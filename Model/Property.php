<?php
App::uses('AppModel', 'Model');
class Property extends AppModel {
	public $primaryKey 	= 'property_id';
	public $name 		= 'Property';
}