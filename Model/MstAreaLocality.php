<?php
App::uses('AppModel', 'Model');
class MstAreaLocality extends AppModel {
	public $primaryKey 	= 'localityCode';
	public $name 		= 'MstAreaLocality';
}