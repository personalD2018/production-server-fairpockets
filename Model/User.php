<?php

// app/Model/User.php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	
   public $belongsTo = array(
        'MstEmployeeDepartment' => array(
            'type' => 'INNER',
            'foreignKey' => 'employee_department_id'
        ),
        'MstEmployeeRole' => array(
            'type' => 'INNER',
            'foreignKey' => 'employee_role_id'
        ),
        'Userextra' => array(
            'className' => 'User',
            'foreignKey' => 'parent_id'
        )
    );
    public $actsAs = array('Tree');
    var $validate = array(
        'mobile' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Mobile already exists.'
            )
        ),
        'email' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Email already exists.'
            )
        ),
        'empId' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Employee ID already exists.'
            )
        ),
        'current_password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Current Password is Wrong'
            )
        ),
        'password1' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Enter New Password'
            )
        ),
        'password2' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Retype Password is not match'
            )
        )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
            );
        }

        return true;
    }

    public function oldPassword($currentPassword) {

        $newHash = Security::hash($currentPassword, 'blowfish', $this->field('password'));
        return $newHash;
    }

    public function currentpassword($user_id) {
        $this->id = $user_id;
        $current = $this->field('password');
        return $current;
    }

    public function getLastQuery() {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        return $lastLog['query'];
    }
}

?>