<?php

class AreaLocalMaster extends Model {

    public $belongsTo = array(
        'AreaCityMaster' => array(
            'foreignKey' => 'cityCode',
            'associatedKey' => 'cityCode',
        )
    );

}

?>