<?php

class AreaStateMaster extends Model {

    public $belongsTo = array(
        'AreaRegionMaster' => array(
            'foreignKey' => 'RegionCode',
            'associatedKey' => 'RegionCode',
        ),
    );

    /* public $hasMany = array(
      'AreaCityMaster' => array(
      'foreignKey' => 'statecode',
      'associatedKey'   => 'statecode',
      )
      ); */
}

?>