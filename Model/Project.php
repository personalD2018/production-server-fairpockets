<?php
App::uses('AppModel', 'Model');
class Project extends AppModel {
	public $primaryKey 	= 'project_id';
	public $name 		= 'Project';
}