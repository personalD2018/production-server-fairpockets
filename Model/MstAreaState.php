<?php
App::uses('AppModel', 'Model');
class MstAreaState extends AppModel {
	public $primaryKey 	= 'stateCode';
	public $name 		= 'MstAreaState';
}