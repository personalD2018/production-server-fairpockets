<?php

class AssignRegionMaster extends Model {

    public $belongsTo = array(
        'AreaRegionMaster' => array(
            'foreignKey' => 'regionCode',
            'associatedKey' => 'regionCode',
        )
    );

    /* public $hasMany = array(
      'AreaStateMaster' => array(
      'foreignKey' => 'RegionCode',
      'associatedKey'   => 'RegionCode',
      )
      ); */
}

?>