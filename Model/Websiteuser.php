<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
App::uses('AuthComponent', 'Controller/Component');

/**
 *
 * User Model - For Individual , Agents , Builders
 *
 */
class Websiteuser extends AppModel {

    public $name = 'websiteusers';
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Name is required.'
            ),
            'string' => array(
                'rule' => '/^[a-zA-Z ]+/',
                'message' => 'Name should have English alphabets only.',
            )
        ),
        'userorgname' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Organization Name is required.'
            ),
            'string' => array(
                'rule' => '/^[a-zA-Z0-9]+/',
                'message' => 'Organization Name should have Alphanumeric Chars only.',
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Email is required.'
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Email format is not valid.'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'The given Email is already in use.'
            )
        ),
        'usermobile' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Mobile number is required.'
            ),
            'numeric' => array(
                'rule' => '/^[0-9]{8,15}$/i',
                'message' => 'Mobile number should have only numerals, min 8 chars.',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'The given Mobile Number is already in use.'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Password is required.'
            )
        )
    );

    public function beforeSave($options = array()) {

        // Generate the crypted password to be stored in database
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password_view'] = $this->data[$this->alias]['password'];
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
            );
        }

        // Mark the user as active
        // Set Account Creation and Modification Dates for the user
        $this->data[$this->alias]['acccreatedate'] = date('Y-m-d H:i:s');
        $this->data[$this->alias]['accmoddate'] = $this->data[$this->alias]['acccreatedate'];
        return true;
    }

    public function oldPassword($currentPassword) {

        $newHash = Security::hash($currentPassword, 'blowfish', $this->field('password'));
        return $newHash;
    }

    public function currentpassword($user_id) {
        $this->id = $user_id;
        $current = $this->field('password');
        return $current;
    }
	
	public function checkUserExists($data) {
	
		//check User role
		if($data['posted_by'] == 'Owner') {
			$userrole = 1;
		} else if($data['posted_by'] == 'Broker') {
			$userrole = 2;
		} else {
			$userrole = 3;
		}
		
		$contact_details = json_decode($data['posted_by_user_contact'][0]);
		
		//pr($contact_details);
		
		if(!empty($contact_details)){
		$user = $this->find('first', array(
			'fields' => array(
				'id'
			),
			'conditions' => array(
				'usermobile' => $contact_details->Mobile
			),
		));
		
		
		$response = ['role' => $userrole];
		
		if(empty($user)) {
			$username = str_replace(' ', '', strtolower($data['posted_by_user_name']));
			$user_data[$this->alias] = [
				'username' => $username,
				'password' => '0',
				'usermobile' => (isset($contact_details->Mobile) ? $contact_details->Mobile : ''),
				'email' => (isset($contact_details->Email) ? $contact_details->Email : ''),
				'useractive' => '0',
				'userrole' => $userrole,
				'acccreatedate' => date('Y-m-d H:i:s'),
				'accmoddate' => date('Y-m-d H:i:s'),				
				'regsitration_token' => md5(date('Y-m-d H:i:s').(isset($contact_details->Email) ? $contact_details->Email : '') . $username),
				'email_verify' => 0,
			];
			
			$user_data = $this->save($user_data);
			$response['id'] = $this->getInsertID();
			//pr($user_data->id);die;
		} else {
			$response['id'] = $user['WebsiteUser']['id'];
		}
		
		}else{
			$response = ['id' => '0','role' => $userrole];
		}
		return $response;
	}
}
