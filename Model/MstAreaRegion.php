<?php
App::uses('AppModel', 'Model');
class MstAreaRegion extends AppModel {
	public $primaryKey 	= 'regionCode';
	public $name 		= 'MstAreaRegion';
}