<?php

class AreaCityMaster extends Model {

    public $belongsTo = array(
        'AreaStateMaster' => array(
            'foreignKey' => 'stateCode',
            'associatedKey' => 'stateCode',
        )
    );

    /* public $hasMany = array(
      'AreaLocalMaster' => array(
      'foreignKey' => 'cityCode',
      'associatedKey'   => 'cityCode',
      )
      ); */
}

?>