<?php
App::uses('AppModel', 'Model');
class MstUserrole extends AppModel {
	public $useTable = 'mst_userrole';
	public $primaryKey = 'userrole_ID';
	public $name = 'MstUserrole';
}