<?php

class AreaRegionMaster extends Model {

    public $belongsTo = array(
        'AreaCountryMaster' => array(
            'foreignKey' => 'countryCode',
            'associatedKey' => 'countryCode',
        )
    );

    /* public $hasMany = array(
      'AreaStateMaster' => array(
      'foreignKey' => 'RegionCode',
      'associatedKey'   => 'RegionCode',
      )
      ); */
}

?>