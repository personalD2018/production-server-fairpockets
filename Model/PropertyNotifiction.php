<?php

class PropertyNotifiction extends Model {

    public $belongsTo = array(
        'PropertyInquiries' => array(
            'foreignKey' => 'inquiry_id',
            'associatedKey' => 'id',
        ),
    );

}

?>