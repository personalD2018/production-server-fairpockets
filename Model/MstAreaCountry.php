<?php
App::uses('AppModel', 'Model');
class MstAreaCountry extends AppModel {
	public $primaryKey = 'countryCode';
	public $name = 'MstAreaCountry';
}