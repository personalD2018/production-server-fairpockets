<?php

class SolrData extends AppModel {
    public $useTable = 'solr_data'; // This model does not use a database table
    
    public function getData($project_id){
        $data_arr = $this->find('all', array(
            'fields' => array('DISTINCT( CONCAT(configure," - ", super_builtup_area, " sqft", " / ", properties_id)) AS flat_spec'),
            'conditions' => array(
                'CONCAT(configure," - ", super_builtup_area, " sqft") IS NOT NULL',
                'Property.project_id = ' . $project_id
                ),
            'order' => array(
                array('configure asc'),
                array('super_builtup_area asc'),
            ),
            'joins' => array(
                array(
                    'table' => 'properties',
                    'alias' => 'Property',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array('Property.property_id = SolrData.properties_id')
                )
            )
        ));
        
        $temp_arr = array();
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                $temp_arr[] = $row[0]['flat_spec'];
            }
        }
        return $temp_arr;
    }
	
	public function getData1($project_id,$planid){
		//$planid = 50;
		//echo '884';
		//echo $planid;die();
        $data_arr = $this->find('all', array(
            'fields' => array('DISTINCT( CONCAT(configure," - ", super_builtup_area, " sqft", " / ", properties_id)) AS flat_spec'),
            'conditions' => array(
                'CONCAT(configure," - ", super_builtup_area, " sqft") IS NOT NULL',
                'PropertyPricingPlan.plan_id = ' . $planid
                ),
            'order' => array(
                array('configure asc'),
                array('super_builtup_area asc'),
            ),
            'joins' => array(
                
				array(
                    'table' => 'property_pricing_plans',
                    'alias' => 'PropertyPricingPlan',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array('PropertyPricingPlan.property_id = SolrData.properties_id')
                )
            )
        ));
        
        $temp_arr = array();
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                $temp_arr[] = $row[0]['flat_spec'];
            }
        }
        return $temp_arr;
    }
	
}

