<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
App::uses('AuthComponent', 'Controller/Component');

/**
 *
 * User Model - For Individual , Agents , Builders
 *
 */
class Contact extends AppModel {

    public $name = 'contacts';
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Name is required.'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Email is required.'
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Email format is not valid.'
            )
        ),
        'mobile' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Mobile number is required.'
            ),
            'numeric' => array(
                'rule' => '/^[0-9]{8,15}$/i',
                'message' => 'Mobile number should have only numerals, min 8 chars.',
            )
        ),
        'subject' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Subject is required.'
            )
        ),
        'message' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Message is required.'
            )
        )
    );

    
}
