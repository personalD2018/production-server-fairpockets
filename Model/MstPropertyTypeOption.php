<?php
App::uses('AppModel', 'Model');
class MstPropertyTypeOption extends AppModel {
	public $primaryKey 	= 'property_option_id';
	public $name 		= 'MstPropertyTypeOption';
}