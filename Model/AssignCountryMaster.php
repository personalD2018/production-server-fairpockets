<?php

class AssignCountryMaster extends Model {

    public $belongsTo = array(
        'AreaCountryMaster' => array(
            'foreignKey' => 'countryCode',
            'associatedKey' => 'countryCode',
        ),
    );
    public $hasmany = array(
        'AreaRegionMaster' => array(
            'foreignKey' => 'regionCode',
            'associatedKey' => 'regionCode',
        ),
    );

}

?>