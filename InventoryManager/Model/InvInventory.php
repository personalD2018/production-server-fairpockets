<?php
class InvInventory extends InventoryManagerAppModel {
    public $name = 'Inventory';
    public $belongsTo = 'Project';
    public function save_data($data_arr){
        try {
            $this->save($data_arr);
            return $this->id;
        } catch (Exception $exc) {
            return false;
        }
    }
    public function getInventoryById($id){
        $data_arr = $this->find('all', array(
            'fields' => array(),
            'conditions' => array('InvInventory.id = ' . $id),
        ));
        return $data_arr;
    }
    public function getInventoriesByUserId($user_id){
        
        $data_arr = $this->find('all', array(
            'fields' => array("InvInventory.*","InvProject.project_name"),
            'conditions' => array("InvWebsiteuser.id = '{$user_id}'"),
            'joins' => array(
                array(
                    'table' => 'projects',
                    'alias' => 'InvProject',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvProject.project_id = InvInventory.project_id")
                ),
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
                )
            )
                    
        ));
        return $data_arr;
    }
    
}

