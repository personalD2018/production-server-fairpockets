<?php


class InvInventoryProperty extends InventoryManagerAppModel {
    public $name = 'InventoryProperty';
    
    public function getInventoryProperty($inventory_id){
        $data_arr = $this->find('all', array(
            'conditions' => array("InvInventoryProperty.inventory_id = '{$inventory_id}'"),
            'order'=>array("InvInventoryProperty.floor_no asc", "InvInventoryProperty.flat_no asc")
        ));
        return $data_arr;
    }
    public function getInventoryPropertyById($id){
        $data_arr = $this->find('all', array(
            'conditions' => array("InvInventoryProperty.id = '{$id}'")
        ));
        return $data_arr;
    }
    public function getCounterByInventoryId($inventory_id){
        $counter = $this->find('count', array(
            'conditions' => array("InvInventoryProperty.inventory_id = '{$inventory_id}'")
        ));
        return $counter;
    }
    public function getCounterByStatus($inventory_id){
        $InvInventoryPropertyStatus = ClassRegistry::init('InvInventoryPropertyStatus');
        $data_arr = $this->find('all', array(
            'fields' => array("count(InvInventoryProperty.id) as counter", "InvInventoryProperty.flat_status"),
            'conditions' => array("InvInventoryProperty.inventory_id = '{$inventory_id}'"),
            'joins' => array(
                array(
                    'table' => 'inventory_property_statuses',
                    'alias' => 'InvInventoryPropertyStatus',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array('InvInventoryProperty.flat_status = InvInventoryPropertyStatus.id')
                )
            ),
            'group'=>array("InvInventoryProperty.flat_status")
        ));
        
        $status_counter_arr = array();
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                if(isset($row['InvInventoryProperty']['flat_status'])){
                    $status_counter_arr[$row['InvInventoryProperty']['flat_status']] = isset($row[0]['counter']) ? intval($row[0]['counter']) : 0;
                }
            }
        }
        $status_arr = $InvInventoryPropertyStatus->find("all");
        $temp_arr = array();
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach($status_arr as $row) {
                
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = array(
                    "status"=>$row['InvInventoryPropertyStatus']['status'],
                    "counter"=>isset($status_counter_arr[$row['InvInventoryPropertyStatus']['id']]) ? intval($status_counter_arr[$row['InvInventoryPropertyStatus']['id']]) : 0   );
            }
        }
        return $temp_arr;
    }
}

