<?php
class InvBuilderBroker extends InventoryManagerAppModel {
    public $name = 'BuilderBroker';
    
    public function getAllBrokers($builder_id, $broker_name, $broker_email, $broker_mobile, $broker_company){
        
        $conditions_arr = array();
        $conditions_arr[] = "InvWebsiteuser.userrole = '2'";
        $conditions_arr[] = "InvWebsiteuser.useractive = '1'";
        
        if(isset($broker_name) && !empty($broker_name)){
            $conditions_arr['OR'][] = "InvWebsiteuser.username like '%{$broker_name}%'";
        }
        if(isset($broker_email) && !empty($broker_email)){
            $conditions_arr['OR'][] = "InvWebsiteuser.email like '%{$broker_email}%'";
        }
        if(isset($broker_company) && !empty($broker_company)){
            $conditions_arr['OR'][] = "InvWebsiteuser.username like '%{$broker_company}%'";
        }
        if(isset($broker_mobile) && !empty($broker_mobile)){
            $conditions_arr['OR'][] = "InvWebsiteuser.usermobile like '%{$broker_mobile}%' ";
        }
        
        
        $data_arr = $this->find('all', array(
            'fields' => array(
                "InvWebsiteuser.id as user_id",
                "InvBuilderBroker.builder_id",
                "InvWebsiteuser.username",
                "InvWebsiteuser.userorgname",
                "InvWebsiteuser.email",
                "InvWebsiteuser.usermobile",
            ),
            'conditions' => $conditions_arr,
            'joins' => array(
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'right',
                    'foreignKey' => false,
                    'conditions' => array('InvWebsiteuser.id = InvBuilderBroker.broker_id')
                )
            )
        ));
        /*
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        echo $lastLog['query']; exit;
        */
        $temp_data = array();
        $user_id_arr = array();
        foreach($data_arr as $row) {
            if($row['InvBuilderBroker']['builder_id'] != $builder_id && !in_array($row['InvWebsiteuser']['user_id'], $user_id_arr)){
                $temp_data[] = $row;
                $user_id_arr[] = $row['InvWebsiteuser']['user_id'];
            }
        }
        return $temp_data;
    }
    
}

