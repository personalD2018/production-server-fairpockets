<?php
class InvWebsiteuser extends InventoryManagerAppModel {
    public $name = 'Websiteuser';
    
    public function getUserDetails($id){
        $data_arr = $this->find('all', array(
            'conditions' => array('InvWebsiteuser.id = ' . $id),
        ));
        return $data_arr;     
    }
}

