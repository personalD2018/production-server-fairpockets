<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
<div class="page-content">
    <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
        <h2>Search brokers from other network</h2>
    </div>
    <div>
        <div class="account-block">
            <div class="add-title-tab">
                <h3>Search </h3>
            </div>

            <div class="add-tab-content detail-block" id="" style="display:block;">
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="add_property_form">
                            <form action="javascript:showSearchList();" class="fpform" id="frm_search"  method="post" accept-charset="utf-8" >
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 margin-bottom">
                                            <div class="form-group select-style">
                                                <label for="broker_name">Name</label>
                                                <input type="text" class="form-control" id="broker_name" name="data[broker_name]" placeholder="Broker name">
                                            </div>	
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 margin-bottom">
                                            <div class="form-group">
                                                <label for="exampleInputcity">Email</label>
                                                <input type="text" class="form-control" id="broker_email" name='data[broker_email]' placeholder="Broker email address">
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 margin-bottom">
                                            <div class="form-group">
                                                <label for="exampleInputsales">Mobile</label>
                                                <input type="text" class="form-control" id="broker_mobile" name="data[broker_mobile]" placeholder="Mobile number" >
                                            </div>	
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 margin-bottom">
                                            <div class="form-group">
                                                <label for="exampleInputsales">Company</label>
                                                <input type="text" class="form-control" id="broker_company" name="data[broker_company]" placeholder="Company name" >
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="account-block text-center submit_area">
                                    <button type="submit" class="btn btn-primary btn-red">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="account-block">
            <div class="add-title-tab">
                <h3>Search result</h3>
                <div id="selected_users" style="display: none;"></div>
            </div>

            <div class="add-tab-content detail-block" id="" style="display:block;">
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="add_property_form">
                            <table id="brokers_search_result" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<?php
    echo $this->Html->script('https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js');
    echo $this->Html->script('https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js');
    echo $this->Html->scriptBlock('
        jQuery(document).ready(function() {
            jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
                var attr = jQuery(this).attr("id");
                if ( jQuery(this).attr("id") === "disable") {          
                    jQuery(this).removeAttr("id");        
                }      
            });
            jQuery( "div#MainMenu a.inventory" ).addClass("active");
            jQuery( "div#MainMenu a.inventory" ).removeClass("collapsed");
            jQuery( "div#demo100" ).addClass("in");
            jQuery( "div#MainMenu a.inventory-management" ).addClass("active");
        });  
    ');
?>
<script type="text/javascript">
jQuery(document).ready(function(){  
    jQuery('form#frm_search').validate({
        rules: {
            "data[broker_name]":{
                require_from_group: [1, ".form-control"]
            },
            "data[broker_email]":{
                require_from_group: [1, ".form-control"]
            },
            "data[broker_mobile]":{
                require_from_group: [1, ".form-control"]
            },
            "data[broker_company]":{
                require_from_group: [1, ".form-control"]
            }
        },
        submitHandler: function(form) {
            // javascript post submit code
            form.submit();
        }
    });	
    var table = jQuery('#brokers_search_result').DataTable({
        order: [[ 1, 'asc' ]],
        "columnDefs": [{
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
            "searchable": false
        }]
    });
    table.clear().draw();
    showSearchList = function(){
        var broker_name = jQuery("#broker_name").val();
        var broker_email = jQuery("#broker_email").val();
        var broker_mobile = jQuery("#broker_mobile").val();
        var broker_company = jQuery("#broker_company").val();
        jQuery.ajax({
            url: "<?php echo Router::url('/accounts/inventory/search-broker-list', true); ?>",
            type: "POST",
            data: {"data[broker_name]" : broker_name, "data[broker_email]":broker_email, "data[broker_mobile]":broker_mobile, "data[broker_company]":broker_company},
            dataType: "json",
            beforeSend: function (xhr) {
                table.clear().draw();
                jQuery("#selected_users").text('');
            },
            success: function(resp_data){
                jQuery.each(resp_data, function(k, v) {
                    table.row.add([
                        "<input type=\"checkbox\" name=\"chk_user_" + v.InvWebsiteuser.user_id + "\" value=\""+v.InvWebsiteuser.user_id+"\" onclick=\"javascript:addUserId("+v.InvWebsiteuser.user_id+")\">",
                        v.InvWebsiteuser.username,
                        v.InvWebsiteuser.usermobile,
                        v.InvWebsiteuser.email
                    ]).draw();
                });
            }
        }).responseText;
    }
    addUserId = function(){
        var user_id = arguments[0];
        if(jQuery('input:checkbox[name="chk_user_'+user_id+'"]').is(':checked')){
            add_selected_user(user_id);
        } else {
            remove_user(user_id);
        }
    }
    remove_user = function(){
        var user_id = arguments[0];
        var current_users_str = jQuery("div#selected_users").text();
        var current_users_arr = new Array();
        if(jQuery.trim(current_users_str) != ""){
            var temp_arr = current_users_str.split(",");
            jQuery.each(temp_arr, function( index, value ) {
                if(parseInt(user_id) != parseInt(value)){
                    current_users_arr.push(parseInt(value));
                }
            });
        }
        if(current_users_arr.length > 0){
            jQuery("div#selected_users").text(current_users_arr.join(','));
        } else {
            jQuery("div#selected_users").text('');
        }
    }
    add_selected_user = function(){
        var user_id = arguments[0];
        var current_users_str = jQuery("div#selected_users").text();
        var current_users_arr = new Array();
        if(jQuery.trim(current_users_str) != ""){
            var temp_arr = current_users_str.split(",");
            jQuery.each(temp_arr, function( index, value ) {
                current_users_arr.push(parseInt(value));
            });
        }
        current_users_arr.push(parseInt(user_id));
        jQuery("div#selected_users").text(current_users_arr.join(','));
    }
    
    
    
    
    
});
</script>