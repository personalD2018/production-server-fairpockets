<div class="page-content">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header">Floor Details</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="account-block">
                <div class="add-title-tab">
                    <h3>Project Info</h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <div class="row">
                        <div class="col-md-4">Project: <?php echo isset($inventory[0]['Project']['project_name']) ? $inventory[0]['Project']['project_name'] : 'N/A';?></div>
                        <div class="col-md-4">Tower: <?php echo isset($inventory[0]['InvInventory']['tower_name']) ? $inventory[0]['InvInventory']['tower_name'] : 'N/A';?></div>
                        <div class="col-md-4">Wing: <?php echo isset($inventory[0]['InvInventory']['wing']) ? $inventory[0]['InvInventory']['wing'] : 'N/A';?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Floors: <?php echo isset($inventory[0]['InvInventory']['floor_start']) ? $inventory[0]['InvInventory']['floor_start'] : '';?> + <?php echo isset($inventory[0]['InvInventory']['floor_end']) ? $inventory[0]['InvInventory']['floor_end'] : 'N/A';?></div>
                        <div class="col-md-4">Flats/Floor: <?php echo isset($inventory[0]['InvInventory']['flats_per_floor']) ? $inventory[0]['InvInventory']['flats_per_floor'] : 'N/A';?></div>
                        <div class="col-md-4">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="account-block">
                <div class="add-title-tab">
                    <h3>Flats </h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <?php
                        echo $this->Form->create(false, array(
                            'url' => array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'saveInventoryFloor'),
                            'id' => 'frm_inventory_second', 
                            'name'=>'frm_inventory_second',
                            'method'=>'POST'
                        ));
                    ?>
                    <input type="hidden" name="inventory_id" value="<?php echo $inventory[0]['InvInventory']['id'];?>">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Flat No.</th>
                                        <th>Area (Sq.Ft.)</th>
                                        <th>BHK</th>
                                        <th>Status</th>
                                        <th style="width:30%;">Position</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i = 1; $i <= intval($inventory[0]['InvInventory']['flats_per_floor']); $i++): ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="input text">
                                                    <input name="data[InvInventoryProperty][flat_no][<?php echo $i;?>]" id="flat_no_<?php echo $i;?>" class="form-control" placeholder="Flat No," maxlength="10" type="text" style="width: 60px;" value="<?php echo str_pad($i, 3, "0", STR_PAD_LEFT);?>">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="data[InvInventoryProperty][flat_area][<?php echo $i;?>]" id="flat_area_<?php echo $i;?>" class="selectpicker bs-select-hidden">
                                                    <option value="">Select</option>
                                                    <?php foreach($areas as $val):?>
                                                    <option value="<?php echo $val;?>"><?php echo $val;?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </td>
                                        
                                        <td>
                                            <div class="form-group">
                                                <select name="data[InvInventoryProperty][flat_bhk][<?php echo $i;?>]" id="flat_bhk_<?php echo $i;?>" class="selectpicker bs-select-hidden">
                                                    <option value="">Select</option>
                                                    <?php foreach($bhks as $val):?>
                                                    <option value="<?php echo $val;?>"><?php echo $val;?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="data[InvInventoryProperty][flat_status][<?php echo $i;?>]" id="flat_status_<?php echo $i;?>" class="selectpicker bs-select-hidden">
                                                    <option value="">Select</option>
                                                    <?php foreach($statuses as $val):?>
                                                    <option value="<?php echo $val['InvInventoryPropertyStatus']['id'];?>"><?php echo $val['InvInventoryPropertyStatus']['status'];?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="data[InvInventoryProperty][flat_position][<?php echo $i;?>][]" id="flat_position_<?php echo $i;?>" class="js-select2-multiple" multiple="multiple">
                                                    <?php foreach($positions as $val):?>
                                                    <option value="<?php echo $val['InvInventoryPropertyPosition']['id'];?>"><?php echo $val['InvInventoryPropertyPosition']['position'];?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endfor;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" id="id_next1" class="btn btn-primary btn-next ">NEXT</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->scriptBlock("jQuery(document).ready(function() {
        jQuery('.js-select2-multiple').multiselect();
        jQuery( 'div#MainMenu a.list-group-item' ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( 'div#MainMenu a.inventory' ).addClass('active');
        jQuery( 'div#MainMenu a.inventory' ).removeClass('collapsed');
        jQuery( 'div#demo100' ).addClass('in');
        jQuery( 'div#MainMenu a.inventory-management' ).addClass('active');
        
        
    });");
?>