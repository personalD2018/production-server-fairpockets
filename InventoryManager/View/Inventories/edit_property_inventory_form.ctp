<form name="frmEditFloorDetails" id="frmEditFloorDetails" method="post" action="javascript:void(0)">
    <input type="hidden" name="data[id]" value="<?php echo $inventory_property[0]['InvInventoryProperty']['id']?>">
<div class="row">
    <div class="col-md-4">Flat No. :</div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="input text">
                <input name="data[flat_no]" id="flat_no" class="form-control" placeholder="Flat No," maxlength="10" type="text" style="width: 60px; border: 1px solid #bdbdbd; border-radius: 0; padding: 6px;" value="<?php echo str_pad($inventory_property[0]['InvInventoryProperty']['flat_no'], 3, "0", STR_PAD_LEFT);?>">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">Flat area :</div>
    <div class="col-md-8">
        <div class="form-group">
            <select name="data[flat_area]" id="flat_area" class="selectpicker bs-select-hidden">
                <option value="">Select</option>
                <?php foreach($areas as $val):?>
                <option value="<?php echo $val;?>" <?php echo $val == $inventory_property[0]['InvInventoryProperty']['flat_area'] ? "selected='selected'" : '';?>><?php echo $val;?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">Flat area :</div>
    <div class="col-md-8">
        <div class="form-group">
            <select name="data[flat_bhk]" id="flat_bhk" class="selectpicker bs-select-hidden">
                <option value="">Select</option>
                <?php foreach($bhks as $val):?>
                <option value="<?php echo $val;?>" <?php echo $val == $inventory_property[0]['InvInventoryProperty']['flat_bhk'] ? "selected='selected'" : '';?>><?php echo $val;?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">Flat status :</div>
    <div class="col-md-8">
        <div class="form-group">
            <select name="data[flat_status]" id="flat_status" class="selectpicker bs-select-hidden">
                <option value="">Select</option>
                <?php foreach($statuses as $key=>$val):?>
                <option value="<?php echo $key;?>" <?php echo $key == $inventory_property[0]['InvInventoryProperty']['flat_status'] ? "selected='selected'" : '';?>><?php echo $val;?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
</div>
<?php
    $sel_positions_arr = isset($inventory_property[0]['InvInventoryProperty']['flat_position']) ? json_decode($inventory_property[0]['InvInventoryProperty']['flat_position'], true) : array();
    if (!is_array($sel_positions_arr) || count($sel_positions_arr) <= 0) {
        $sel_positions_arr = array();
    }
?>
<div class="row">
    <div class="col-md-4">Flat position :</div>
    <div class="col-md-8">
        <div class="form-group">
            <select name="data[flat_position][]" id="flat_position" class="js-select2-multiple" multiple="multiple">
                <?php foreach ($positions as $key=>$val): ?>
                    <option value="<?php echo $key; ?>" <?php echo in_array($key,$sel_positions_arr) ? "selected='selected'" : '';?>><?php echo $val; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('.js-select2-multiple').multiselect();
});
</script>
