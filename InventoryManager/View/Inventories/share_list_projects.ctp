<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Projects</h1>
        </div>
        <div class="col-md-4">
            <div class="input-group custom-search-form">
                <input type="text" id="searchbox" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-yellow" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Locality</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Regd. Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (is_array($projects) && count($projects) > 0) {
                            foreach($projects as $row) {
                    ?>
                    <tr>
					 <?php 
					 //echo $builder_id;
					 $status = $this->Number->shareProjectStatusProjectId($row['InvProject']['project_id'],$builder_id);
						if($status == 'add'){
							$link = 'share-project-info';
						}
						if($status == 'update'){
							$link = 'share-project-info-update';
						}
					 ?>
                        <td><?php echo isset($row['InvProject']['project_name']) ? $row['InvProject']['project_name'] : 'n/a';?></td>
                        <td><?php echo isset($row['InvProject']['locality']) ? $row['InvProject']['locality'] : 'n/a';?></td>
                        <td><?php echo isset($row['InvProject']['city_data']) ? $row['InvProject']['city_data'] : 'n/a';?></td>
                        <td><?php echo isset($row['InvProject']['state_data']) ? $row['InvProject']['state_data'] : 'n/a';?></td>
                        <td><?php echo isset($row['InvProject']['created_date']) ? date("d/m/Y", strtotime($row['InvProject']['created_date'])) : 'n/a';?></td>
                        <td style="text-align: center;"><a href="<?php echo $this->Html->url('/accounts/projects/'.$link.'/' . $row['InvProject']['project_id']);?>" class="btn btn-primary" title="Share"><i class="fa fa-share-alt"></i></a></td>
                    </tr>
                    <?php
                            }
                        }
                    ?>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        
        var dataTable = jQuery('#inventory_list').DataTable({
            "columnDefs": [{
                "targets": [5], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>