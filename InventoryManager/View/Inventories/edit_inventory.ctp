<style type="text/css" title="">
    .form-group input[type='text']{
        border-bottom: 1px solid #ccc !important;
        border-radius: 0!important;
        padding-bottom: 6px;
        padding-top: 7px;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Edit Inventory</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="account-block">

                <div class="add-title-tab">
                    <h3>Inventory Info </h3>
                </div>

                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <?php
                        echo $this->Form->create(false, array(
                            'url' => array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'addInventory'),
                            'id' => 'frm_inventory_first', 
                            'name'=>'frm_inventory_first',
                            'method'=>'POST'
                        ));
                    ?>
                    <input type="hidden" name="mode" value="process">
                    <input type="hidden" name="data[Inventory][id]" value="<?php echo $inventory[0]['InvInventory']['id']?>">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="inventory_project_id" class="label_title">Project name <span class="mand_field">*</span></label>
                                <div class="select-style">
                                    <div class="input select">
                                        <select name="data[Inventory][project_id]" id="inventory_project_id" class="selectpicker bs-select-hidden">
                                            <option value="">Select</option>
                                            <?php foreach($projects as $val):?>
                                            <option value="<?php echo $val['InvProject']['project_id']?>" <?php echo $inventory[0]['Project']['project_id'] == $val['InvProject']['project_id'] ? 'selected="selected"' : ''; ?>><?php echo $val['InvProject']['project_name']?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>		
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="inventory_tower_name" class="label_title">Tower name </label>
                                <div class="input text">
                                    <input name="data[Inventory][tower_name]" id="inventory_tower_name" class="form-control" placeholder="Tower Name" maxlength="50" type="text" value="<?php echo isset($inventory[0]['InvInventory']['tower_name']) ? $inventory[0]['InvInventory']['tower_name'] : ''; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="inventory_wing" class="label_title">Wing (if any)</label>
                                <div class="input text">
                                    <input name="data[Inventory][wing]" id="inventory_wing" class="form-control" placeholder="Wing" maxlength="50" type="text" value="<?php echo isset($inventory[0]['InvInventory']['wing']) ? $inventory[0]['InvInventory']['wing'] : ''; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="project_name" class="label_title">Total floors <span class="mand_field">*</span></label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="select-style">
                                            <div class="input select">
                                                <select name="data[Inventory][floor_start]" id="inventory_floor_start" class="selectpicker bs-select-hidden">
                                                    <option value="G" selected="selected">G</option>
                                                    <?php for ($i = 1; $i <= 30; $i++) :?>
                                                    <option value="<?php echo $i;?>" <?php echo isset($inventory[0]['InvInventory']['floor_start']) && $inventory[0]['InvInventory']['floor_start'] == $i ? 'selected="selected"' : ''; ?>><?php echo $i;?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>		
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input text">
                                            <input name="data[Inventory][floor_end]" id="inventory_floor_end" class="form-control" placeholder="No. of floors" maxlength="2" type="text" value="<?php echo isset($inventory[0]['InvInventory']['floor_end']) ? $inventory[0]['InvInventory']['floor_end'] : ''; ?>">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="inventory_flats_per_floor" class="label_title">Flats per floor <span class="mand_field">*</span></label>
                                <div class="input text"><input name="data[Inventory][flats_per_floor]" id="inventory_flats_per_floor" class="form-control" placeholder="Flats/Floor" maxlength="50" type="text" value="<?php echo isset($inventory[0]['InvInventory']['flats_per_floor']) ? $inventory[0]['InvInventory']['flats_per_floor'] : ''; ?>"></div>
                            </div>
                        </div>
                    </div>
                    <div class="account-block text-center submit_area">
                        <div id="loadingmessage1" style="display:none">
                            <img src="../img/ajax-loader.gif">
                        </div>
                        <button type="submit" id="id_next1" class="btn btn-primary btn-next ">NEXT</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->scriptBlock("jQuery(document).ready(function() {
        jQuery( 'div#MainMenu a.list-group-item' ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( 'div#MainMenu a.inventory' ).addClass('active');
        jQuery( 'div#MainMenu a.inventory' ).removeClass('collapsed');
        jQuery( 'div#demo100' ).addClass('in');
        jQuery( 'div#MainMenu a.inventory-management' ).addClass('active');
        
        jQuery.validator.addMethod('greaterThanStartFloor', function(value, element) {
            var start_floor = jQuery('#inventory_floor_start').val();
            if(parseFloat(value) > 0){
                return true;
            }
        }, 'Please re-check the floor number');
        jQuery('form#frm_inventory_first').validate({
            rules: {
                'data[Inventory][project_id]':{
                    required:true
                },
                'data[Inventory][floor_end]':{
                    required: true,
                    digits:true,
                    greaterThanStartFloor: true
                },
                'data[Inventory][flats_per_floor]':{
                    required: true,
                    digits:true,
                    min:1
                }
            },
            messages:{
                'data[Inventory][project_id]':{
                    required: 'Project not selected'
                },
                'data[Inventory][floor_end]':{
                    required: 'Floor number is required',
                    digits:'Only digits are allowed'
                },
                'data[Inventory][flats_per_floor]':{
                    required: 'Flats per floor is blank',
                    digits:'Only digits are allowed',
                    min:'Minimum should be 1'
                }
            }, 
            submitHandler: function(form) {
                form.submit();
            }
        });
    });");
?>

