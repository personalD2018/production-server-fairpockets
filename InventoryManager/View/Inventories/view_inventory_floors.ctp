<style type="text/css">    
	.panel-group .panel {
		border-radius: 0;
		box-shadow: none;
		border-color: #EEEEEE;
	}
	.panel-default > .panel-heading {
		padding: 0;
		border-radius: 0;
		color: #212121;
		background-color: lightblue;
		border-color: #EEEEEE;
	}
	.panel-title {
		font-size: 14px;
	}
	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}
	.more-less {
		float: right;
		color: #212121;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border-top-color: #EEEEEE;
	}
	.table-wrapper-scroll-x {
		display: block;
		max-height: 100px;
		overflow-y: auto;
		-ms-overflow-style: -ms-autohiding-scrollbar;
	}
	.table-bordered > tbody > tr > td{
		border:none;	
		padding: 0px 0px 0px 0px !important;
	}
	.tabledsn{
		border:0.5px dashed; padding:8px;
		transition: transform .2s; /* Animation */
		margin: 0 auto;
	}
	.tabledsn1{
		border:0.5px solid; padding:8px; font-weight:bold;
	}
	.tabledsn:hover {
		transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
	}
	.table-bordered{
		border:1px solid #000000;
	}
</style>

<?php
	$jsons = '{"flat_no":{"1":"001","2":"002"},"flat_area":{"1":"1215","2":"1620"},"flat_bhk":{"1":"2BHK","2":"2BHK"},"flat_status":{"1":"1","2":"1"},"flat_position":{"1":["1","2"],"2":["2"]}}';
	$flats_per_floor = $inventory[0]['InvInventory']['flats_per_floor'];
	$invjson = $inventory[0]['InvInventory']['template'];
	$decodes = json_decode($invjson,true);
?>
<div class="page-content">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header">Floor Details</h1>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="account-block">
                <div class="add-title-tab">
                    <h3>Flats </h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bs-example">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default" style="width:828px;">                                        
                                        <div>
											<div class="panel-body">
												<?php //foreach($inventory as $inventoryok){
														for($j=0;$j<=1;$j++){
												?>
												<?php $jsonview = $inventory[$j]['InvInventory']['template'];
													$arrr[] =  json_decode($jsonview,true);
												?>
												<div class="detail-title" style="text-align:center;">
													<?php if($inventory[$j]['InvInventory']['wing'] != ''){ ?>
														<a>
															<b><?php echo $inventory[$j]['InvInventory']['wing']; ?></b>								
														</a>
													<?php }else{ ?>
														<a>
															<b><?php echo $inventory[$j]['InvInventory']['tower_name']; ?></b>								
														</a>
													<?php } ?>
												</div>
												<div class="abovetable" style="width:800px; overflow-x:auto;">
													<table class="table table-bordered">                     
														<tbody>
															<tr>
																<td style="padding-top:20px;">
																	<div class="insidetd" style="width:140px;">
																		<div class="tabledsn1">Flat No</div>
																		<div class="tabledsn1">Area</div>
																		<div class="tabledsn1">Config</div>
																		<div class="tabledsn1">Position</div>														
																		<?php foreach($inventory_property_arr as $key=>$row_el): ?>
																			<div class="tabledsn">Floors - <?php echo $key; ?></div>
																		<?php endforeach; ?>
																	</div>
																</td>
																<?php
																	$row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
																	for($i=1; $i<=$inventory[$j]['InvInventory']['flats_per_floor']; $i++){
																?>                                                        
																<td style="padding-top:20px;">
																	<div class="insidetd" style="width:140px;">
																		<div class="tabledsn1">
																			<?php //echo $arrr[$j]['flat_no'][$i]; 																
																			 echo str_pad($arrr[$j]['flat_no'][$i], 3, "0", STR_PAD_LEFT) ?>																
																		</div>
																	
																		<div class="tabledsn1">
																		<?php echo isset($arrr[$j]['flat_area'][$i]) ? $arrr[$j]['flat_area'][$i] . ' Sqft.' : "N/A"; ?>
																		</div>
																		<div class="tabledsn1">
																		<?php echo isset($arrr[$j]['flat_bhk'][$i]) && strlen($arrr[$j]['flat_bhk'][$i]) > 3 ? $arrr[$j]['flat_bhk'][$i] : 'N/A';?>
																		</div>
																		<div class="tabledsn1">
																		<?php 
																			$flat_pos_arr = $arrr[$j]['flat_position'][$i];
																			$temp_pos_arr = array();
																			if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
																				foreach($flat_pos_arr as $pos_el) {
																					$temp_pos_arr[] = $positions[$pos_el];
																				}
																			}
																			if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
																				echo implode(", ",$temp_pos_arr);
																			} else {
																				echo "N/A";
																			}
																		?>
																		</div>
																	
																	
																		<?php foreach($inventory_property_arr as $key=>$row_el){
																		?>
																		<?php 
																		$invid = $inventory_id;
																		$floor_no = $key;
																		$flat_no = $arrr[$j]['flat_no'][$i];
																		$flat_status_id = $this->Number->getStatusInv($invid,$floor_no,$flat_no);
																		
																		if($flat_status_id == 1) { $colorcode = '#dff0d8';}
																		if($flat_status_id == 2) { $colorcode = '#f2dede';}
																		if($flat_status_id == 3) { $colorcode = '#fcf8e3';}
																		if($flat_status_id == 4) { $colorcode = '#d9edf7';}
																		
																		$flat_status =  $statuses[$flat_status_id];
																		$row_classes11 = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
																		?>
																		<div class="tabledsn" style="background-color:<?php echo $colorcode; ?>">
																		<?php echo isset($flat_status) ? $flat_status : "N/A";?></div>
																			
																		<?php 
																		} ?>
																	
																	
																	</div>
																</td>
																<?php } ?>
															</tr>
														</tbody>
													</table>												
												</div>
												<br><br>
												<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
</div>


<?php
echo $this->Html->scriptBlock("jQuery(document).ready(function() {
        jQuery( 'div#MainMenu a.list-group-item' ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( 'div#MainMenu a.inventory' ).addClass('active');
        jQuery( 'div#MainMenu a.inventory' ).removeClass('collapsed');
        jQuery( 'div#demo100' ).addClass('in');
        jQuery( 'div#MainMenu a.inventory-management' ).addClass('active');
    });");
?>
