var ERROR_MESSAGE = {
	USERTYPE : 'Please select are you an Individual or Dealer.',
	NAME : 'Please specify your name.',
	EMAIL : 'Please specify email address.',
	PHONE : 'Please specify your contact number.'
};


var CONTACT_FORM_VALIDATION = {
	ERRORCLASS : 'validation-error',
	RULES : {
		'user_type' : 'required', 
		'name'  	: 'required',
		'email' 	: {
			required : true,
			email   : true
		},
		'phone' 	: {
			required : true
		}
	},
	MESSAGES : {	
		'user_type' : ERROR_MESSAGE.USERTYPE,
		'name'  	: ERROR_MESSAGE.NAME,
		'email' 	: ERROR_MESSAGE.EMAIL,
		'phone'		: ERROR_MESSAGE.PHONE
	}
};


