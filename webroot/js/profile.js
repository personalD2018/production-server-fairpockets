$('#register_form').validate({
	// Specify the validation rules
	rules: {
		'data[Websiteuser][username]': {
			required : true
		},
		// 'data[Websiteuser][userorgname]': {
			// required : true,
			// minlength : 5
		// },
		'data[Websiteuser][usermobile]': {
			required : true,
			number : true,
			minlength : 10,
			maxlength : 10
		},
		'data[Websiteuser][email]': {
			required : true,
			email : true
		}
	},
	
	// Specify the validation error messages
	messages: {
		'data[Websiteuser][username]': {
			required : 'Name is required.'
		},
		// 'data[Websiteuser][userorgname]': {
			// required : 'Organization Name is required.',
			// minlength : 'Min 5 alphabets.'
		// },
		'data[Websiteuser][usermobile]': {
			required : 'Mobile Number is required.',
			number : ' Only Numeric digits allowed.',
			minlength : 'Min 10 numbers.',
			maxlength : 'Max 10 numbers.'
		},
		'data[Websiteuser][email]': {
			required : 'Email is required.',
			email : 'Please enter a valid email address'
		}
	},
	//submitHandler: function(form){
		
		// To remain on same page after submit
		//return true;
		
//}
});