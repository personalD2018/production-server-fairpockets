$(document).ready(function () {
    $(".add_property_form #property-type").change(function () {
        $(".add_property_form #property-type-val").empty();
        $(".add_property_form #property-type-val").append("<option>--Select--</option>");
        if ($(this).val() == 'residential') {

            $(".add_property_form #property-type-val").append("<option>Apartment/Studio Apartment </option><option>Independent Builder Floor</option><option>Independent House/Villa</option><option>Farm House</option>");
        }
        if ($(this).val() == 'commercial') {

            $(".add_property_form #property-type-val").append("<option>Commercial office space</option><option>Office in IT Park </option><option>Commercial shop/showroom</option><option>Commercial/industrial/agricultural Land </option>");
        }
    });
	
	$("#image_preview").on('click', '.icon-delete', function () {
        $(this).closest('.property_image').remove();
    });

    $('#addmore1').click(function () {
        var rowhtml = '<div class="row "><div class="col-sm-3"><div class="form-group"><label for="property-price-before" class="label_title">Name of PLC<span class="mand_field">*</span></label>';
        rowhtml += '<input class="form-control" name="bamount" id="book-amount-before" placeholder="Enter name of plc"><div class="error has-error">Box must be filled out</div></div></div>';
        rowhtml += '<div class="col-sm-3"><div class="form-group"><label for="property-price-before" class="label_title">Type of Charge<span class="mand_field">*</span></label><div class="select-style"><select class="selectpicker bs-select-hidden" name="" id="" title="Select"><option>Mandatory</option><option>Optional</option></select></div></div></div>';
        rowhtml += '<div class=" col-sm-3"><div class="form-group"> <div class="col-sm-12"> <label for="property-price" class="label_title">Amount<span class="mand_field">*</span></label><i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Apartment with most living basics like furniture, AC, TV etc."></i></div>';
        rowhtml += '<div class="col-sm-6"><input class="form-control" type="text" name="sbua"></div>';
        rowhtml += '<div class="col-sm-6"><div class="select-style"><select class="selectpicker" name="sbua_m"><option value="1bhk">Per Sq.ft</option><option value="1bhk">Per Unit</option><option value="1bhk">Acres</option><option value="1bhk">Hactares</option></select></div></div>';
        rowhtml += '<div class="error has-error">Box must be filled out</div></div></div>';
        rowhtml += '<div class="col-sm-2"><div class="form-group"><div class="frowedit"><a href="javascript:void(0)" class="delclick" onclick="delrow(this);"><i class="fa fa-times" aria-hidden="true"></i></a></div></div></div>';
        rowhtml += '</div>';
        $('.rowsec2').append(rowhtml);
        return false;
    });
});

function preview_image(fil)
{
    var total_file = document.getElementById("upload_file").files.length;
	
	var index = $('.gallery-thumb').length;

    for (var i = 0; i < total_file; i++)
    {
	
		$('#image_preview').append('<div class="col-sm-2 property_image"><figure class="gallery-thumb-'+(++index)+'"><img src="'+URL.createObjectURL(event.target.files[i])+'" alt="thumb"><span class="icon icon-delete"><i class="fa fa-trash"></i></span></figure></div>');
    }
}


var regex = /^(.+?)(\d+)$/i;
var cloneProjectBspChargeSpaceIndex = $('.cloneProjectBspChargeSpace').length + 1;
var clonePossessionChargesIndex = $('.clonePossessionCharges').length + 1;



function cloneProjectBspChargeAddRowSpace()
{ //alert('hello');
    cloneProjectBspChargeChildrenSpace = $("#id_cloneProjectBspCharge_tab_space").children().length;
    //maxProjectBspCharge = document.getElementById('id_cloneProjectBspChargeCount').value;
	maxProjectBspCharge = 5;

    if (cloneProjectBspChargeChildrenSpace < maxProjectBspCharge)
    {
        //var validator = $('#id_form_project_pricing').validate();
        if (cloneProjectBspChargeChildrenSpace > 0)
        {
            $("#id_cloneProjectBspCharge_tab_space").children().first().clone()
                    .appendTo("#id_cloneProjectBspCharge_tab_space")
                    .attr("id", "cloneCoreSpace" + cloneProjectBspChargeSpaceIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneProjectBspChargeSpaceIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                       
                        if (id.indexOf("space_mode") >= 0)
                        {
                            this.name = "data[bspchargesspace][space_mode" + cloneProjectBspChargeSpaceIndex + "]";
                        }

                        if (id.indexOf("space_from_floor") >= 0)
                        {
                            this.name = "data[bspchargesspace][space_from_floor" + cloneProjectBspChargeSpaceIndex + "]";
                        }
						
						if (id.indexOf("floor_space_area") >= 0)
                        {
                            this.name = "data[bspchargesspace][floor_space_area" + cloneProjectBspChargeSpaceIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("floor_space_amount") >= 0)
                        {
                            this.name = "data[bspchargesspace][floor_space_amount" + cloneProjectBspChargeSpaceIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 1,
                                messages: {
                                    min: 'Min value allowed is 1.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999,
                                messages: {
                                    max: 'Max value allowed is 999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneProjectBspChargeSpaceIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneProjectBspChargeAddRowSpace').prop('disabled', true);
    }

}

function cloneProjectBspChargeDelRowSpace(vld)
{
    var cloneProjectBspChargeChildrenSpace = $("#id_cloneProjectBspCharge_tab_space").children().length;

    $('#cloneProjectBspChargeDelRowSpace').prop('disabled', false);

    if (cloneProjectBspChargeChildrenSpace > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}




function clonePossessionChargesAddRow()
{ //alert('hello1');
    clonePossessionChargesChildren = $("#id_clonePossessionCharges_tab").children().length;
    //maxProjectBspCharge = document.getElementById('id_cloneProjectBspChargeCount').value;
	maxProjectBspCharge = 5;

    if (clonePossessionChargesChildren < maxProjectBspCharge)
    {
        //var validator = $('#id_form_project_pricing').validate();
        if (clonePossessionChargesChildren > 0)
        {
            $("#id_clonePossessionCharges_tab").children().first().clone()
                    .appendTo("#id_clonePossessionCharges_tab")
                    .attr("id", "cloneCoreSpace" + clonePossessionChargesIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (clonePossessionChargesIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                       
                        

                        if (id.indexOf("possession_from_floor") >= 0)
                        {
                            this.name = "data[possessioncharges][possession_from_floor" + clonePossessionChargesIndex + "]";
                        }
						
						if (id.indexOf("possession_to_floor") >= 0)
                        {
                            this.name = "data[possessioncharges][possession_to_floor" + clonePossessionChargesIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("possession_amount") >= 0)
                        {
                            this.name = "data[possessioncharges][possession_amount" + clonePossessionChargesIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 1,
                                messages: {
                                    min: 'Min value allowed is 1.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999,
                                messages: {
                                    max: 'Max value allowed is 999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            clonePossessionChargesIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneProjectBspChargeAddRowSpace').prop('disabled', true);
    }

}

function clonePossessionChargesDelRow(vld)
{
    var clonePossessionChargesChildren = $("#id_clonePossessionCharges_tab").children().length;

    $('#clonePossessionChargesDelRow').prop('disabled', false);

    if (clonePossessionChargesChildren > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}




