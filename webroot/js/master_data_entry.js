angular.module('masterDataEntryApp', ['datatables'])
.controller('masterDataEntryController', function ($scope, $http, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
	$scope.masterData = {
		moduleName: 'MstAreaCountry',
		data: {}
	};
	$scope.countryList 	= [];
	$scope.regionList	= [];
	$scope.stateList 	= [];
	$scope.cityList 	= [];
	$scope.localityList = [];

	$scope.dtOptions = DTOptionsBuilder.newOptions();

    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2)
    ];

    $scope.addNewEntryForm = function() {
    	$('#modal' + $scope.masterData.moduleName).modal('show');
    	$scope.masterData.data = {};
    };

    $scope.editEntry = function(index) {
		switch($scope.masterData.moduleName) {
			case 'MstAreaCountry':
				$scope.masterData.data = angular.copy($scope.countryList[index][$scope.masterData.moduleName]);
				break;
			case 'MstAreaRegion':
				$scope.masterData.data = angular.copy($scope.regionList[index][$scope.masterData.moduleName]);
				break;
			case 'MstAreaState':
				$scope.masterData.data = angular.copy($scope.stateList[index][$scope.masterData.moduleName]);
				break;
			case 'MstAreaCity':
				$scope.masterData.data = angular.copy($scope.cityList[index][$scope.masterData.moduleName]);
				break;
			case 'MstAreaLocality':
				$scope.masterData.data = angular.copy($scope.localityList[index][$scope.masterData.moduleName]);
				break;
		}
		
		$('#modal' + $scope.masterData.moduleName).modal('show');
		
	};

	$scope.moduleChangeEvent = function(moduleName) {
		switch(moduleName) {
			case 'MstAreaCountry':
				$scope.getAllCountries();
				break;
			case 'MstAreaRegion':
				$scope.getAllRegionsByCountry();
				break;
			case 'MstAreaState':
				$scope.getAllRegionsByCountry();
				$scope.getAllStatesByRegion();
				break;
			case 'MstAreaCity':
				$scope.getAllRegionsByCountry();
				$scope.getAllStatesByRegion();
				$scope.getAllCitiesByState();
				break;
			case 'MstAreaLocality':
				$scope.getAllRegionsByCountry();
				$scope.getAllStatesByRegion();
				$scope.getAllCitiesByState();
				$scope.getAllLocalitiesByCity();
				break;
		}
		$scope.masterData.moduleName = moduleName;
		$scope.masterData.data 		 = {};
	};

	$scope.saveMasterData = function() {
		var url 	= '/admin/accounts/insertMasterData';
		var promise = $http.post(url, angular.copy($scope.masterData));
		promise.then(function(res) {
			switch($scope.masterData.moduleName) {
				case 'MstAreaCountry':
					$scope.getAllCountries();
					$scope.alertMessage = 'New Country was successfully added!';
					break;
				case 'MstAreaRegion':
					$scope.getAllRegionsByCountry();
					$scope.alertMessage = 'New Region was successfully added!';
					break;
				case 'MstAreaState':
					$scope.getAllStatesByRegion();
					$scope.alertMessage = 'New State was successfully added!';
					break;
				case 'MstAreaCity':
					$scope.getAllCitiesByState();
					$scope.alertMessage = 'New City was successfully added!';
					break;
				case 'MstAreaLocality':
					$scope.getAllLocalitiesByCity();
					$scope.alertMessage = 'New Locality was successfully added!';
					break;
			}
			$('#modal' + $scope.masterData.moduleName).modal('hide');
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
			$timeout(function() { $scope.alertMessage = ''; }, 3000);	
			$scope.masterData.data = {};
		}, function(){
			// alert('failed!');
		})
	};

	$scope.getAllCountries = function() {
		var url 	= '/admin/areas/getMstAreaCountry';
		var promise = $http.get(url);
		promise.then(function(res) {
			$scope.countryList = res.data;
		});
		return promise;
	};

	$scope.getAllRegionsByCountry = function() {
		var url 	= '/admin/areas/getAllAreaRegionsByCountry/' + ((typeof countryId == 'undefined') ? '' : countryId);
		var promise = $http.get(url);
		promise.then(function(res) {
			$scope.regionList = res.data;
		});
		return promise;
	};

	$scope.getAllStatesByRegion = function(regionId = '') {
		if(regionId == 'null')
			regionId = '';
		var url 	= '/admin/areas/getAllAreaStatesByRegion/' + regionId;
		var promise = $http.get(url);
		promise.then(function(res) {
			$scope.stateList = res.data;
		});
		return promise;
	};

	$scope.getAllCitiesByState = function(stateId = '') {
		var url 	= '/admin/areas/getAllAreaCitiesByState/' + stateId;
		var promise = $http.get(url);
		promise.then(function(res) {
			$scope.cityList = res.data;
		});
		return promise;
	};
	$scope.getAllLocalitiesByCity = function(cityId = '') {
		var url 	= '/admin/areas/getAllAreaLocalitiesByCity/' + cityId;
		var promise = $http.get(url);
		promise.then(function(res) {
			$scope.localityList = res.data;
		});
		return promise;
	};
	
	$scope.getAllCountries();
	
})