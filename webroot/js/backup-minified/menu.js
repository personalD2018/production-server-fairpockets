$(document).ready(function () {
	$(".list-group-item").click(function() {
	
		if($(this).next().hasClass('dropdown')) {
		
			if(!$(this).hasClass('active')) {
				$('.list-group div.dropdown').removeClass('in');
				$('.list-group div.dropdown').addClass('collapse');
				$(this).removeClass('collapsed');
				$('.list-group .list-group-item').removeClass('active');
				$(this).addClass('active');				
			}
		}
	});
});	