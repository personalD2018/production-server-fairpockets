
// Function to check errors in Gmap section before a form is submitted
function checkGmapErrors()
{
    var errString = 'Please correct the following errors' + '\n';
    var errFound = false;

    if (
            ('' == document.getElementById('locality').value) ||
            ('' == document.getElementById('city_data').value) ||
            ('' == document.getElementById('state_data').value) ||
            ('' == document.getElementById('country_data').value) ||
            ('' == document.getElementById('pincode').value) ||
            ('' == document.getElementById('lat').value) ||
            ('' == document.getElementById('lng').value)
            )
    {
        errFound = true;
        errString = errString + '\n- Please select a locality on map and click it.';

    }

    if (errFound == true) {
        return false; // To support validation plugin
    } else {
        return true; // To support validation plugin
    }
}

$(document).ready(function () {
	
    // This function will be called when a value is selected from city drop-down
    function setLocation()
    {
        // find the dropdown
        var ddl = document.getElementById("id_select_city");

        // find the selected option
        var selectedOption = ddl.options[ddl.selectedIndex];

        // find the attribute value
        document.getElementById("id_search_adm_area_2").value = selectedOption.innerHTML;
        document.getElementById("id_search_adm_area_1").value = selectedOption.parentNode.label;
        //document.getElementById('searchInput').value = "";

        document.getElementById("block").value = "";
        document.getElementById("locality").value = "";
        document.getElementById("city_data").value = "";
        document.getElementById("state_data").value = "";
        document.getElementById("country_data").value = "";
        document.getElementById("pincode").value = "";

    }

    var optionsAuto = {
        radius: 20000

    };


    var iconBase = '/img//';
    var mIcon = iconBase + 'mIcon.png';
    var mHover = iconBase + 'mHover.png';
    var mTick = iconBase + 'mTick.png';
    var last_locality_search = '';

    // Initalization scipt for GMAP
    function initialize()
    {
		
        var displaySuggestions = function (predictions, status) {

            autoCompleteResult = '';

            predictions.forEach(function (prediction) {

                if (status != google.maps.places.PlacesServiceStatus.OK) {
                    alert(status);
                    return;
                }
				
                autoCompleteResult = autoCompleteResult + '<div name=\"' + prediction.place_id + '\" onclick=\"gMapOptionClick(this)\" >' + prediction.description + '</div>'
            });

            if (autoCompleteResult != '')
            {
                document.getElementById("result").innerHTML = autoCompleteResult;
                document.getElementById("result").style.display = 'inline';
            } else
                document.getElementById("result").style.display = 'none';

        };
		var city_value = $("#id_select_city").val();
		var locality_value = $("#searchInput").val();
		var lat_value = $("#lat").val();
		var long_value = $("#lng").val();
		if(lat_value != '')
		{
			var latlng = new google.maps.LatLng(lat_value, long_value);
		}
		else
		{
			var latlng = new google.maps.LatLng(28.5355161, 77.39102649999995);
		}
        
        
        var marker = new google.maps.Marker({
            map: map,
            icon: mIcon,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 13
        });

        marker.setMap(map);

        var input = document.getElementById('searchInput');
        var geocoder = new google.maps.Geocoder();

        var infowindow = new google.maps.InfoWindow();

        // Setting markes to be used
        var currentMarker = null;
        var markerIcon = mIcon;
        var markerIconHover = mHover;
        var markerTick = mTick;
		if(city_value != '')
		{
			var addr = $("#address").val();
		geocoder.geocode({'address': addr}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK)
                {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    marker.setVisible(true);
                    marker.setIcon(markerIcon);

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
		}
        var sel_action = document.getElementById('id_select_city');
        var ser_action = document.getElementById('searchInput');
		
		
        $("#searchInput").keyup(function (event) {

            //alert( $('#searchInput').val() );
            var ddl = document.getElementById("id_select_city");
            var selectedOption = ddl.options[ddl.selectedIndex];
            var temp_search = document.getElementById('searchInput').value + ' ' + selectedOption.innerHTML;
            ;

            if ('' == document.getElementById('searchInput').value)
                document.getElementById("result").style.display = 'none';
            else
            {
                var service = new google.maps.places.AutocompleteService();
                service.getQueryPredictions({input: temp_search}, displaySuggestions);
            }

        });


        // Event Listner for city drop down.
        // gmap will point to selected city from drop dowm
				
        sel_action.addEventListener("change", function () {
			document.getElementById('searchInput').value = "";
			document.getElementById('address').value = "";
            var address = sel_action.options[sel_action.selectedIndex].innerHTML;
            var place = new Array();
            //alert(address);

            setLocation();

            marker.setVisible(false);

            geocoder.geocode({'address': address}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK)
                {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    marker.setVisible(true);
                    marker.setIcon(markerIcon);
                    place = results[0];

                    // Just read city from address component
                    var arrAddress = place.address_components;
                    var temp_city = "";
                    for (ac = 0; ac < arrAddress.length; ac++)
                    {
                        if ((arrAddress[ac].types[0] == "locality") &&
                                (arrAddress[ac].types[1] == "political"))
                            temp_city = arrAddress[ac].long_name
                    }

                    bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), temp_city);
                    infowindow.setContent(place.formatted_address);
                    infowindow.open(map, marker);

                    var newlatlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
                    optionsAuto.location = newlatlng;


                    document.getElementById('lat').value = "";
                    document.getElementById('lng').value = "";
                } else
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        });

        // Event Listner for city drop down.
        // gmap will point to selected city from drop dowm
		
        ser_action.addEventListener("click", function () {

            var address = ser_action.value;
			///alert(address);
            var place = new Array();
            //alert(address);

            if (address == '')
            {
                return; // No need to proceed further.
            }

            if ((last_locality_search == '') || (last_locality_search == address))
            {
                last_locality_search = address;
                return; // No need to proceed further.
            } else
                last_locality_search = address;

            marker.setVisible(false);

            geocoder.geocode({'address': address}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK)
                {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    marker.setVisible(true);
                    marker.setIcon(markerIcon);

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        });

        // Event Listner for mouse dragstart
        // Set correct icon for mouse dragstart
        google.maps.event.addListener(marker, 'dragstart', function () {

            marker.setIcon(markerIconHover);

            document.getElementById('searchInput').value = "";
            document.getElementById("block").value = "";
            document.getElementById("locality").value = "";
            document.getElementById("city_data").value = "";
            document.getElementById("state_data").value = "";
            document.getElementById("country_data").value = "";
            document.getElementById("pincode").value = "";
            document.getElementById('lat').value = "";
            document.getElementById('lng').value = "";


        });

        // Event Listner for mouseout.
        // Set correct icon on mouseout
        google.maps.event.addListener(marker, 'mouseout', function () {

            if ((currentMarker !== null) && (currentMarker !== marker)) {
                marker.setIcon(markerIconHover);
            }

        });

        // Event Listner for mouse dragend
        google.maps.event.addListener(marker, 'dragend', function () {

			geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {

                        marker.setIcon(markerIconHover);

                        // Set current marker
                        currentMarker = marker;

                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);// Set hovered map marker
                    }
                }
            });

        });

        // Event Listner for mouse click
        // Get the different values of address for current position
        // Set correct icon on mouse dragend
        google.maps.event.addListener(marker, 'click', function () {

            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {

                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);

                        marker.setIcon(markerTick);
                        document.getElementById('address').value = results[0].formatted_address;
                        var arrAddress = results[0].address_components;

                        document.getElementById("block").value = "";
                        document.getElementById("locality").value = "";
                        document.getElementById("city_data").value = "";
                        document.getElementById("state_data").value = "";
                        /*document.getElementById("tbUnit9").value = "";*/
                        document.getElementById("country_data").value = "";
                        document.getElementById("pincode").value = "";

                        /*document.getElementById('location').value = "";*/
                        document.getElementById('lat').value = "";
                        document.getElementById('lng').value = "";

                        //Read available address object and try to get different part of address
                        // Bit of hardcoding below for read
                        for (ac = 0; ac < arrAddress.length; ac++)
                        {

                            if ((arrAddress[ac].types[0] == "political") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "sublocality_level_2"))
                                document.getElementById("block").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "sublocality_level_2") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "political"))
                                document.getElementById("block").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "political") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "sublocality_level_1"))
                                document.getElementById("locality").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "sublocality_level_1") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "political"))
                                document.getElementById("locality").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "locality") &&
                                    (arrAddress[ac].types[1] == "political"))
                                document.getElementById("city_data").value = arrAddress[ac].long_name

                            if ((arrAddress[ac].types[0] == "administrative_area_level_1") &&
                                    (arrAddress[ac].types[1] == "political"))
                                document.getElementById("state_data").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "country") &&
                                    (arrAddress[ac].types[1] == "political"))
                                document.getElementById("country_data").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "postal_code"))
                                document.getElementById("pincode").value = arrAddress[ac].long_name;
                        }

                        // Fill different parts of address
                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), document.getElementById("city_data").value);

                        // Check if address is now OK
                        var validator = $("#id_form_project_basic").validate();
                        validator.element("#address");
                    }
                }
            });
        });
    }

    function bindDataToForm(address, lat, lng, city)
    {
        document.getElementById('lat').value = lat;
        document.getElementById('lng').value = lng;

        // Set city drop down based on selected locality
        var mySelObj = document.getElementById("id_select_city");
        var city_found = false;

        for (var i = 0, sL = mySelObj.length; i < sL; i++)
        {
            if (mySelObj.options[i].text == city)
            {
                mySelObj.selectedIndex = i;
                city_found = true;
                break;
            }
        }
    }

    // Clear all set marker icons
    function clearMarkerIcons()
    {
        for (var i = 0; i < map.markers.length; i++)
        {
            map.markers[i].setIcon(markerIcon);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});