function preview_image()
{
    var total_file = document.getElementById("upload_file").files.length;
    for (var i = 0; i < total_file; i++)
    {
        //$('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'><br>");
        $('#image_preview').append('<div class="col-sm-2 thum_ar"><figure class="gallery-thumb"><img src="' + URL.createObjectURL(event.target.files[i]) + '" alt="thumb"><span class="icon icon-fav"><i class="fa fa-star-o"></i></span><span class="icon icon-delete"><i class="fa fa-trash"></i></span><span class="icon icon-loader"><i class="fa fa-spinner fa-spin"></i></span></figure></div>');
        upload_img_del();
    }
}

function upload_img_del() {
    $(".icon-delete").click(function () {
        $(this).closest('.thum_ar').remove();
    });
}
