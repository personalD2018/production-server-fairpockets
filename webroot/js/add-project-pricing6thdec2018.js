
var regex = /^(.+?)(\d+)$/i;
var cloneBankIndex = $('.cloneBank').length + 1;
var cloneCorePlcIndex = $('.cloneCorePlc').length + 1;
var cloneProjectChargeIndex = $('.cloneProjectCharge').length + 1;
var cloneProjectBspChargeIndex = $('.cloneProjectBspCharge').length + 1;
var cloneProjectBspChargePlcIndex = $('.cloneProjectBspChargePlc').length + 1;
var cloneProjectBspChargeSpaceIndex = $('.cloneProjectBspChargeSpace').length + 1;
var cloneOtherPlcIndex = $('.cloneOtherPlc').length + 1;

// Function to check if there are any duplicate Bank or not
function checkBankDuplicate()
{
    var cplc = $('.uniqueBank');

    var cplcArray = $.map(cplc, function (x) {
        return $(x).val();
    });

    cplcArray.sort();
    for (var i = 1; i < cplcArray.length; i++)
    {
        if (cplcArray[i - 1] == cplcArray[i])
            return true; // Found 2 same Bank
    }
    return false;

}

// Function to check if there are any errors in Project Charges or not
function checkPriceProjectChargesErrors(class_x, class_y)
{
    /*
     var cplc = $('.uniqueProjectCharges');
     var cplcamt = $('.uniqueProjectChargesAmt');
     */

    var cplc = $(class_x);
    var cplcamt = $(class_y);

    var cplcArray = $.map(cplc, function (x) {
        return $(x).val();
    });

    var cplcamtArray = $.map(cplcamt, function (x) {
        return $(x).val();
    });

    for (var i = 0; i < cplcArray.length; i++)
    {
        if ((cplcArray[i] == '0') && (cplcamtArray[i] != ''))
            return 3; // Not Selected

        if ((cplcArray[i] != '0') && (cplcamtArray[i] == ''))
            return 2; // Amount is missing

        if ((cplcArray.length > 1) && (cplcArray[i] == '0') && (cplcamtArray[i] == ''))
            return 2; // Amount is missing

    }

    cplcArray.sort();
    for (var i = 1; i < cplcArray.length; i++)
    {
        if (cplcArray[i - 1] == cplcArray[i])
            return 1; // Duplicate
    }

    return 0;

}


// Function to check errors in bank section
function checkBankErrors()
{
    var errString = 'Please correct the following errors' + '\n';
    var errFound = false;

    if (true == checkBankDuplicate())
    {
        errFound = true;
        errString = errString + '\n- Same Bank selected more than once.';
    }

    if (errFound == true)
    {
        alert(errString);
        return true;
    } else
        return false;
}


// Function to check errors in price section
function checkPriceErrors()
{
    var errString = 'Please correct the following errors' + '\n';
    var errFound = false;

    ret_val = checkPriceProjectChargesErrors('.uniqueProjectCharges', '.uniqueProjectChargesAmt');

    if (1 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Duplicate Names of Project Charges selected .';
    } else if (2 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Amount for Project Charges missing.';
    } else if (3 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Name for Project Charges not selected.';
    } else
    {
        ;
    }

    ret_val = checkPriceProjectChargesErrors('.uniqueOtherPlc', '.uniqueOtherPlcAmt');

    if (1 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Duplicate Names of Other Charges selected .';
    } else if (2 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Amount for Other Charges missing.';
    } else if (3 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Name for Other Charges not selected.';
    } else
    {
        ;
    }

    ret_val = checkPriceProjectChargesErrors('.uniqueCorePlc', '.uniqueCorePlcAmt');

    if (1 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Duplicate Names of Additional Charges selected .';
    } else if (2 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Amount for Additional Charges missing.';
    } else if (3 == ret_val)
    {
        errFound = true;
        errString = errString + '\n- Name for Additional Charges not selected.';
    } else
    {
        ;
    }

    if (errFound == true)
    {
        alert(errString);
        return true;
    } else
        return false;
}

// Add Builder New Project 
// Function to add Bank Row
function cloneBankAddRow()
{
    cloneBankChildren = $("#id_cloneBank_tab").children().length;

    if (cloneBankChildren > 0)
    {
        var validator = $('#id_form_project_bank').validate();

        $("#id_cloneBank_tab").children().first().clone()
                .appendTo("#id_cloneBank_tab")
                .attr("id", "cloneBank" + cloneBankIndex)
                .find("*")
                .each(function () {

                    var id = this.id || "";
                    var match = id.match(regex) || [];
                    if (match.length == 3) {
                        this.id = match[1] + (cloneBankIndex);
                    }

                    var cn = this.className;
                    var container
                    if (cn.indexOf("form-group") >= 0)
                    {
                        container = $(this);
                        container.find('label.error').remove();
                    }

                    //Add Bank Name validation rules
                    if (id.indexOf("bank_name") >= 0)
                    {
                        this.name = "data[bank][bank_name" + cloneBankIndex + "]";

                        /*
                         $('[name*=\''+this.name+'\']').rules('add', {
                         required: true,
                         messages: {
                         required: "Bank Name is required"
                         }
                         }
                         );	
                         */

                        container = $(this);
                        container.removeClass('error');
                    }

                });

        cloneBankIndex++;
    } else
        alert(" Nothing to clone");
}

// Add Builder New Project 
// Function to delete Bank Row
function cloneBankDelRow(vld)
{
    var cloneBankChildren = $("#id_cloneBank_tab").children().length;

    if (cloneBankChildren > 1) // Show atleast 1 bank row
    {
        var id = ($(vld).closest('.row')).attr('id');
        var match = id.match(regex) || [];
        var nm = "data[bank][bank_name" + match[2] + "]";

        // TODO : Remove validation rule 
        /*
         $('#id_form_project_bank').validate();
         
         $('form#id_form_project_bank [name*=\''+nm+'\']').rules('add', {
         required: true,
         messages: {
         required: "Bank Name is required"
         }
         }
         );
         
         
         $('#'+nm).rules('remove'); 
         */

        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());

    }

}

// Add Builder New Project 
// Function to add Core Plc Row
function cloneCorePlcAddRow()
{
    cloneCorePlcChildren = $("#id_cloneCorePlc_tab").children().length;
    maxCorePlc = document.getElementById('id_cloneCorePlcCount').value;

    if (cloneCorePlcChildren < maxCorePlc)
    {
        var validator = $('#id_form_project_pricing').validate();

        if (cloneCorePlcChildren > 0)
        {
            $("#id_cloneCorePlc_tab").children().first().clone()
                    .appendTo("#id_cloneCorePlc_tab")
                    .attr("id", "cloneCorePlc" + cloneCorePlcIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneCorePlcIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                        if (id.indexOf("price_core_plc") >= 0)
                        {
                            this.name = "data[addition_charges][price_core_plc" + cloneCorePlcIndex + "]";
                        }

                        if (id.indexOf("price_core_type") >= 0)
                        {
                            this.name = "data[addition_charges][price_core_type" + cloneCorePlcIndex + "]";
                        }

                        if (id.indexOf("price_core_amunit") >= 0)
                        {
                            this.name = "data[addition_charges][price_core_amunit" + cloneCorePlcIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("price_core_amt") >= 0)
                        {
                            this.name = "data[addition_charges][price_core_amt" + cloneCorePlcIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */

                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 0,
                                messages: {
                                    min: 'Min value allowed is 0.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999,
                                messages: {
                                    max: 'Max value allowed is 999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneCorePlcIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneCorePlcAddRow').prop('disabled', true);
    }

}

// Add Builder New Project 
// Function to delete Core PlC Row
function cloneCorePlcDelRow(vld)
{
    var cloneCorePlcChildren = $("#id_cloneCorePlc_tab").children().length;

    $('#id_cloneCorePlcAddRow').prop('disabled', false);

    if (cloneCorePlcChildren > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}

// Add Builder New Project 

// Function to add BSP Charge Row
function cloneProjectBspChargeAddRow()
{
    cloneProjectBspChargeChildren = $("#id_cloneProjectBspCharge_tab").children().length;
    maxProjectBspCharge = document.getElementById('id_cloneProjectBspChargeCount').value;

    if (cloneProjectBspChargeChildren < maxProjectBspCharge)
    {
        var validator = $('#id_form_project_pricing').validate();

        if (cloneProjectBspChargeChildren > 0)
        {
            $("#id_cloneProjectBspCharge_tab").children().first().clone()
                    .appendTo("#id_cloneProjectBspCharge_tab")
                    .attr("id", "cloneCorePlc" + cloneProjectBspChargeIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneProjectBspChargeIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                       
                        if (id.indexOf("from_floor") >= 0)
                        {
                            this.name = "data[bspcharges][from_floor" + cloneProjectBspChargeIndex + "]";
                        }

                        if (id.indexOf("to_floor") >= 0)
                        {
                            this.name = "data[bspcharges][to_floor" + cloneProjectBspChargeIndex + "]";
                        }
						
						if (id.indexOf("unit_bsp") >= 0)
                        {
                            this.name = "data[bspcharges][unit_bsp" + cloneProjectBspChargeIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("price_bsp") >= 0)
                        {
                            this.name = "data[bspcharges][price_bsp" + cloneProjectBspChargeIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 0,
                                messages: {
                                    min: 'Min value allowed is 0.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999999,
                                messages: {
                                    max: 'Max value allowed is 999999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneProjectBspChargeIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneProjectBspChargeAddRow').prop('disabled', true);
    }

}

// Function to delete Project BSP Charge Row
function cloneProjectBspChargeDelRow(vld)
{
    var cloneProjectBspChargeChildren = $("#id_cloneProjectBspCharge_tab").children().length;

    $('#id_cloneProjectBspChargeAddRow').prop('disabled', false);

    if (cloneProjectBspChargeChildren > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}

// Function to add BSP Charge Row PLC
function cloneProjectBspChargeAddRowPlc()
{ //alert('hello');
    cloneProjectBspChargeChildrenPlc = $("#id_cloneProjectBspCharge_tab_plc").children().length;
    //maxProjectBspCharge = document.getElementById('id_cloneProjectBspChargeCount').value;
	maxProjectBspCharge = 50;

    if (cloneProjectBspChargeChildrenPlc < maxProjectBspCharge)
    {
        var validator = $('#id_form_project_pricing').validate();

        if (cloneProjectBspChargeChildrenPlc > 0)
        {
            $("#id_cloneProjectBspCharge_tab_plc").children().first().clone()
                    .appendTo("#id_cloneProjectBspCharge_tab_plc")
                    .attr("id", "cloneCorePlc" + cloneProjectBspChargePlcIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneProjectBspChargePlcIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                       
                        if (id.indexOf("plc_from_floor") >= 0)
                        {
                            this.name = "data[bspchargesplc][plc_from_floor" + cloneProjectBspChargePlcIndex + "]";
                        }

                        if (id.indexOf("plc_to_floor") >= 0)
                        {
                            this.name = "data[bspchargesplc][plc_to_floor" + cloneProjectBspChargePlcIndex + "]";
                        }
						
						if (id.indexOf("per_floor_mode") >= 0)
                        {
                            this.name = "data[bspchargesplc][per_floor_mode" + cloneProjectBspChargePlcIndex + "]";
                        }
						
						if (id.indexOf("unit_plc") >= 0)
                        {
                            this.name = "data[bspchargesplc][unit_plc" + cloneProjectBspChargePlcIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("floor_plc_amount") >= 0)
                        {
                            this.name = "data[bspchargesplc][floor_plc_amount" + cloneProjectBspChargePlcIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 0,
                                messages: {
                                    min: 'Min value allowed is 0.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999,
                                messages: {
                                    max: 'Max value allowed is 999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneProjectBspChargePlcIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneProjectBspChargeAddRowPlc').prop('disabled', true);
    }

}

function cloneProjectBspChargeDelRowPlc(vld)
{
    var cloneProjectBspChargeChildrenPlc = $("#id_cloneProjectBspCharge_tab_plc").children().length;

    $('#id_cloneProjectBspChargeAddRowPlc').prop('disabled', false);

    if (cloneProjectBspChargeChildrenPlc > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}



// Function to add BSP Charge Row PLC
function cloneProjectBspChargeAddRowSpace()
{ alert('hello');
    cloneProjectBspChargeChildrenSpace = $("#id_cloneProjectBspCharge_tab_space").children().length;
    //maxProjectBspCharge = document.getElementById('id_cloneProjectBspChargeCount').value;
	maxProjectBspCharge = 5;

    if (cloneProjectBspChargeChildrenSpace < maxProjectBspCharge)
    {
        var validator = $('#id_form_project_pricing').validate();

        if (cloneProjectBspChargeChildrenSpace > 0)
        {
            $("#id_cloneProjectBspCharge_tab_space").children().first().clone()
                    .appendTo("#id_cloneProjectBspCharge_tab_space")
                    .attr("id", "cloneCoreSpace" + cloneProjectBspChargeSpaceIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneProjectBspChargeSpaceIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                       
                        if (id.indexOf("space_mode") >= 0)
                        {
                            this.name = "data[bspchargesspace][space_mode" + cloneProjectBspChargeSpaceIndex + "]";
                        }

                        if (id.indexOf("space_from_floor") >= 0)
                        {
                            this.name = "data[bspchargesspace][space_from_floor" + cloneProjectBspChargeSpaceIndex + "]";
                        }
						
						if (id.indexOf("floor_space_area") >= 0)
                        {
                            this.name = "data[bspchargesspace][floor_space_area" + cloneProjectBspChargeSpaceIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("floor_space_amount") >= 0)
                        {
                            this.name = "data[bspchargesspace][floor_space_amount" + cloneProjectBspChargeSpaceIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 1,
                                messages: {
                                    min: 'Min value allowed is 1.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999,
                                messages: {
                                    max: 'Max value allowed is 999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneProjectBspChargeSpaceIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneProjectBspChargeAddRowSpace').prop('disabled', true);
    }

}

function cloneProjectBspChargeDelRowSpace(vld)
{
    var cloneProjectBspChargeChildrenSpace = $("#id_cloneProjectBspCharge_tab_space").children().length;

    $('#cloneProjectBspChargeDelRowSpace').prop('disabled', false);

    if (cloneProjectBspChargeChildrenSpace > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}





// Function to add Project Charge Row
function cloneProjectChargeAddRow()
{
    cloneProjectChargeChildren = $("#id_cloneProjectCharge_tab").children().length;
    maxProjectCharge = document.getElementById('id_cloneProjectChargeCount').value;

    if (cloneProjectChargeChildren < maxProjectCharge)
    {
        var validator = $('#id_form_project_pricing').validate();

        if (cloneProjectChargeChildren > 0)
        {
            $("#id_cloneProjectCharge_tab").children().first().clone()
                    .appendTo("#id_cloneProjectCharge_tab")
                    .attr("id", "cloneCorePlc" + cloneProjectChargeIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneProjectChargeIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                        if (id.indexOf("price_pcharge") >= 0)
                        {
                            this.name = "data[charges][price_pcharge" + cloneProjectChargeIndex + "]";
                        }

                        if (id.indexOf("price_pcharge_type") >= 0)
                        {
                            this.name = "data[charges][price_pcharge_type" + cloneProjectChargeIndex + "]";
                        }

                        if (id.indexOf("price_pcharge_amunit") >= 0)
                        {
                            this.name = "data[charges][price_pcharge_amunit" + cloneProjectChargeIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("price_pcharge_amt") >= 0)
                        {
                            this.name = "data[charges][price_pcharge_amt" + cloneProjectChargeIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 0,
                                messages: {
                                    min: 'Min value allowed is 0.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 99999999,
                                messages: {
                                    max: 'Max value allowed is 99999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneProjectChargeIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneProjectChargeAddRow').prop('disabled', true);
    }

}

// Add Builder New Project 
// Function to delete Project Charge Row
function cloneProjectChargeDelRow(vld)
{
    var cloneProjectChargeChildren = $("#id_cloneProjectCharge_tab").children().length;

    $('#id_cloneProjectChargeAddRow').prop('disabled', false);

    if (cloneProjectChargeChildren > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}

// Add Builder New Project 
// Function to add Other Plc Row
function cloneOtherPlcAddRow()
{
    cloneOtherPlcChildren = $("#id_cloneOtherPlc_tab").children().length;
    maxOtherPlc = document.getElementById('id_cloneOtherPlcCount').value;

    if (cloneOtherPlcChildren < maxOtherPlc)
    {
        var validator = $('#id_form_project_pricing').validate();

        if (cloneOtherPlcChildren > 0)
        {
            $("#id_cloneOtherPlc_tab").children().first().clone()
                    .appendTo("#id_cloneOtherPlc_tab")
                    .attr("id", "cloneOtherPlc" + cloneOtherPlcIndex)
                    .find("*")
                    .each(function () {

                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneOtherPlcIndex);
                        }

                        var cn = this.className;
                        var container;
                        if (cn.indexOf("form-group") >= 0)
                        {
                            container = $(this);
                            container.find('label.error').remove();
                        }

                        if (id.indexOf("price_other_plc") >= 0)
                        {
                            this.name = "data[othercharges][price_other_plc" + cloneOtherPlcIndex + "]";
                        }

                        if (id.indexOf("price_other_type") >= 0)
                        {
                            this.name = "data[othercharges][price_other_type" + cloneOtherPlcIndex + "]";
                        }

                        if (id.indexOf("price_other_amunit") >= 0)
                        {
                            this.name = "data[othercharges][price_other_amunit" + cloneOtherPlcIndex + "]";
                        }

                        //Add price core plc validation rules
                        if (id.indexOf("price_other_amt") >= 0)
                        {
                            this.name = "data[othercharges][price_other_amt" + cloneOtherPlcIndex + "]";

                            /*
                             $('[name*=\''+this.name+'\']').rules('add', {
                             required: true,
                             messages: {
                             required: 'Field is required'
                             }
                             }
                             );
                             */
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                number: true,
                                messages: {
                                    number: 'Only Numeric Digits allowed.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                min: 0,
                                messages: {
                                    min: 'Min value allowed is 0.'
                                }
                            }
                            );
                            $('[name*=\'' + this.name + '\']').rules('add', {
                                max: 999999,
                                messages: {
                                    max: 'Max value allowed is 999999.'
                                }
                            }
                            );

                            this.value = "";
                            container = $(this);
                            container.removeClass('error');
                        }

                    });

            cloneOtherPlcIndex++;
        } else
            alert(" Nothing to clone");

    } else
    {
        $('#id_cloneOtherPlcAddRow').prop('disabled', true);
    }

}

// Add Builder New Project 
// Function to delete Other PlC Row
function cloneOtherPlcDelRow(vld)
{

    var cloneOtherPlcChildren = $("#id_cloneOtherPlc_tab").children().length;

    $('#id_cloneOtherPlcAddRow').prop('disabled', false);

    if (cloneOtherPlcChildren > 1) // Show atleast 1 core plc row
    {
        $(vld).closest('.row').remove();
        //alert($(vld).closest('.row').html());
    }

}

