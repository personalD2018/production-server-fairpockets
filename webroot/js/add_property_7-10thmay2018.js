$(document).ready(function () {
    $(".add_property_form #property-type").change(function () {
        $(".add_property_form #property-type-val").empty();
        $(".add_property_form #property-type-val").append("<option>--Select--</option>");
        if ($(this).val() == 'residential') {

            $(".add_property_form #property-type-val").append("<option>Apartment/Studio Apartment </option><option>Independent Builder Floor</option><option>Independent House/Villa</option><option>Farm House</option>");
        }
        if ($(this).val() == 'commercial') {

            $(".add_property_form #property-type-val").append("<option>Commercial office space</option><option>Office in IT Park </option><option>Commercial shop/showroom</option><option>Commercial/industrial/agricultural Land </option>");
        }
    });
	
	$("#image_preview").on('click', '.icon-delete', function () {
        $(this).closest('.property_image').remove();
    });

    $('#addmore1').click(function () {
        var rowhtml = '<div class="row "><div class="col-sm-3"><div class="form-group"><label for="property-price-before" class="label_title">Name of PLC<span class="mand_field">*</span></label>';
        rowhtml += '<input class="form-control" name="bamount" id="book-amount-before" placeholder="Enter name of plc"><div class="error has-error">Box must be filled out</div></div></div>';
        rowhtml += '<div class="col-sm-3"><div class="form-group"><label for="property-price-before" class="label_title">Type of Charge<span class="mand_field">*</span></label><div class="select-style"><select class="selectpicker bs-select-hidden" name="" id="" title="Select"><option>Mandatory</option><option>Optional</option></select></div></div></div>';
        rowhtml += '<div class=" col-sm-3"><div class="form-group"> <div class="col-sm-12"> <label for="property-price" class="label_title">Amount<span class="mand_field">*</span></label><i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Apartment with most living basics like furniture, AC, TV etc."></i></div>';
        rowhtml += '<div class="col-sm-6"><input class="form-control" type="text" name="sbua"></div>';
        rowhtml += '<div class="col-sm-6"><div class="select-style"><select class="selectpicker" name="sbua_m"><option value="1bhk">Per Sq.ft</option><option value="1bhk">Per Unit</option><option value="1bhk">Acres</option><option value="1bhk">Hactares</option></select></div></div>';
        rowhtml += '<div class="error has-error">Box must be filled out</div></div></div>';
        rowhtml += '<div class="col-sm-2"><div class="form-group"><div class="frowedit"><a href="javascript:void(0)" class="delclick" onclick="delrow(this);"><i class="fa fa-times" aria-hidden="true"></i></a></div></div></div>';
        rowhtml += '</div>';
        $('.rowsec2').append(rowhtml);
        return false;
    });
});

function preview_image(fil)
{
    var total_file = document.getElementById("upload_file").files.length;
	
	var index = $('.gallery-thumb').length;

    for (var i = 0; i < total_file; i++)
    {
	
		$('#image_preview').append('<div class="col-sm-2 property_image"><figure class="gallery-thumb-'+(++index)+'"><img src="'+URL.createObjectURL(event.target.files[i])+'" alt="thumb"><span class="icon icon-delete"><i class="fa fa-trash"></i></span></figure></div>');
    }
}