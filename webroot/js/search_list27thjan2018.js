$(window).load(function(){
	$('div').removeClass('divhide');
});

$(document).ready(function() {
	$('.drawer').drawer();
	
	$('#myModal').modal('hide');
	var options = [];
	
	/*$( '.dropdown-menu a' ).on( 'click', function( event ) {
		
		var $target = $( event.currentTarget ),
		val = $target.attr( 'data-value' ),
		$inp = $target.find( 'input' ),
		idx;
		
		if ( ( idx = options.indexOf( val ) ) > -1 ) {
		options.splice( idx, 1 );
		setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
		} else {
		options.push( val );
		setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
		}
		
		$( event.target ).blur();
		
		console.log( options );
		return false;
	});*/
	
	$('.add-title-tab > .add-expand').on('click',function() {
		$(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
	});   
	
	$("#sort-link-filter").click(function(){
		$("#mobile-filter").slideToggle();
	});
	$("#sort-link-sortby").click(function(){
		$("#mobile-sortby").slideToggle();
	});
	
	$('#locality-search').on('keypress keyup', function() {
		var value = $(this).val().toLowerCase();
		if (value != '') {
			
			$('#locality-list > li').each(function () {
				if ($(this).text().toLowerCase().indexOf(value) > -1) {
					$(this).removeClass('displayNone');  
					} else {
					$(this).addClass('displayNone');
				}
			});
			} else {
			$('#locality-lists > li').removeClass('displayNone');
		}
	});	
	
	$('#society-search').on('keypress keyup', function(){
		var value = $(this).val().toLowerCase();
		if (value != '') {
			
			$('#society-list > li').each(function () {
				if ($(this).text().toLowerCase().indexOf(value) > -1) {
					$(this).removeClass('displayNone');  
					} else {
					$(this).addClass('displayNone');
				}
			});
			} else {
			$('#society-list > li').removeClass('displayNone');
		}
	});	
	
	$(".property-type").click(function(){
		$(".property-checklist").slideDown("fast");
	});
	
	if($("#slider").length) {
		jQuery("#slider").slider({delay: 1000, interval: 5000});
	}
	
	var filters = {};
	
	$(document).on('click', 'input[name$="filters"]', function() {
		
		var $box = $(this).attr('name'), $values = [];
		$('input[name='+$box+']').each(function() { 		
			if($(this).is(":checked")) {
				$values.push($(this).val());
			}			
		});
		
		filters[$box] = $values;
		
		if(!$('input[name="search_filters"]').length) {
			var $filters = $('<input/>').attr({
				type: 'hidden',
				name: 'search_filters',
				value: '' 
			});		
			$($filters).appendTo('body');
		}
		
		applyFilters(filters);
	});


	$('[name=go_area_submit]').click(function() {
		filters['area-filters'] = {};
		$('[name^=area_]').each(function(i, v) {
			filters['area-filters'][$(v).attr('name')] = $(v).val();
		});
		applyFilters(filters);
	});
	
	$(document).on('click', '.pagination ul li a', function(e){
		e.preventDefault();
		$(".drawer-overlay").show();
		$('.property-listing').load($(this).attr('href'), function(responseTxt, statusTxt, xhr) {
			$(".drawer-overlay").hide();
		});
	});
	
	$(document).on('click', 'a.cle_fil', function(e){
		e.preventDefault();
		e.stopPropagation();
		var filter = $(this).data('filter');
		if(filter == 'all') {
			filters = {};
			$('.listing-filter [type=checkbox]').attr('checked', false);
			} else if (filter in filters) {
			delete filters[filter];
			$(this).closest('.filter-inner').find(':checkbox').attr('checked', false);
		}
		
		applyFilters(filters);
	});
	
	
	$(document).on('change', 'select[class$="budget"]', function() {
		var $this = $(this);
		var min = $('select[name=min-price]').val() == 0 ? '*' : $('select[name=min-price]').val() * 100000;
		var max = $('select[name=max-price]').val() == 0 ? '*' : $('select[name=max-price]').val() * 100000;
		filters['offer_price'] = min + ' TO ' + max;
		applyFilters(filters);
	});
	
	$('#sort_by').change(function() {
		if($(this).val() != '0') {
			applyFilters(filters);
		}
	});
	//var $wum = $('#user_mobile');
	//Search Details page scripts
	var contact_modal = $('#contact-biulder'), $cf = $('#id_contact_form'), $cfs = $('#id_contact_succ');

	if($cf.length) {
		var validator = $('#contact-form').validate({
			rules			: CONTACT_FORM_VALIDATION.RULES,
			messages		: CONTACT_FORM_VALIDATION.MESSAGES,
			errorClass 		: CONTACT_FORM_VALIDATION.ERRORCLASS,
			errorPlacement	: function(error, element) {
				error.appendTo($(element).parent('.form-group'));
			},
			submitHandler	: function(form) {
				var phno = $('#user_mobile').val();
		alert(phno);
				sendOTP1();
				return false;
			}
		});
	}
	
	contact_modal.on('show.bs.modal', function (e) {
		var elmnt 	= $(e.relatedTarget);
		var modal	= $(this);
		modal.find('.modal-title').text(elmnt.data('title'));
		modal.find('.modal-propid').val(elmnt.data('propid'));
		}).on('hidden.bs.modal', function () {
		validator.resetForm();
		$(this).find("input,textarea,select,button").val('').end()
		.find("input[type=checkbox], input[type=radio]").prop("checked","").end()
		.find('.validation-error').removeClass('validation-error');
		$('#interested').selectpicker('deselectAll');
		$('.message').attr('class', 'message').html('');
	});
});

function applyFilters(filters) {
	$('input[name=search_filters]').val(JSON.stringify(filters));
		
	var url = window.location.href + '&sortby='+$('#sort_by').val()+'&filters=' + JSON.stringify(filters), $overlay = $(".drawer-overlay");
	$.ajax({
		url : url, 
		type : 'get',
		beforeSend : function () {
			$overlay.show();
		},
		complete : function () {
			$overlay.hide();
		},
		success : function(response) {
			$('.property-listing').html(response);
			$('#society-list').html($('.society-list').html());
			$('#locality-list').html($('.locality-list').html());
			
			//Select applied filters
			
			if('society-filters' in filters) {
				$('#society-list li input').each(function() {
					var $item = $(this);
					$(filters['society-filters']).each(function(index, value){
						if($item.val() == value) {
							$item.prop('checked', true);
						}
					});	
				});
			}
			
			if('locality-filters' in filters) {
				$('#locality-list li input').each(function() {
					var $item = $(this);
					$(filters['locality-filters']).each(function(index, value){
						if($item.val() == value) {
							$item.prop('checked', true);
						}
					});	
				});
			}
		}
	});
}

$(document).mouseup(function (e) {
	var container = $(".property-checklist");
	
	if (!container.is(e.target)
	&& container.has(e.target).length === 0)
	{
		container.slideUp("fast");
	}
});

function showlocation() {
	document.getElementById("mylocations").classList.toggle("show");
}

window.onclick = function(event) {
	if (!event.target.matches('.dropbtn')) {
		
		var dropdowns = document.getElementsByClassName("dropdown-content");
		var i;
		for (i = 0; i < dropdowns.length; i++) {
			var openDropdown = dropdowns[i];
			if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
			}
		}
	}
}

function check_uncheck_checkbox1(isChecked) {
	if(isChecked) {
		$('input[name="Residential"]').each(function() { 
			this.checked = true; 
		});
		} else {
		$('input[name="Residential"]').each(function() {
			this.checked = false;
		});
	}
}

function check_uncheck_checkbox2(isChecked) {
	if(isChecked) {
		$('input[name="Commercial"]').each(function() { 
			this.checked = true; 
		});
		} else {
		$('input[name="Commercial"]').each(function() {
			this.checked = false;
		});
	}
}

function showMesssage($data) {
		return '<div class="col-sm-12 status" style="padding: 10px;"><div class="alert alert-'+$data.cls+'">'+$data.msg+'</div></div>';
	}

function verifyOtp1(elem) {
		$(elem).prepend('<i class="fa fa-spinner fa-pulse fa-fw"></i>').attr('disabled', 'disabled');

        $('#id_contact_succ').find('.col-sm-12.status').remove();
        //alert($wum.val());die();
		$.ajax({
			type: 'POST',
			url: '/websiteusers/verifyOtp/',
			data: {
				'id' : $('#user_mobile').val(),
				'otp'	: $('#otp_verify').val()
			},
			dataType :'json',
			success: function(data) {
				//$cfs.html('').hide();
				if(data.type == 'success' ) {
                    $('#id_contact_succ').append(showMesssage({'cls':'success', 'msg':'OTP has been verified, Please wait for registration.'}));
					//$('#id_contact_succ').hide();
					UserRegistrationFormSubmit();
				} else {
                    $(elem).html('Verify').removeAttr('disabled');
                    $('#id_contact_succ').append(showMesssage({'cls':'danger', 'msg':'Invalid OTP, Please try again!.'}));
				}
			}
		});
	}

function sendOTP1() {
	
		//alert('one');
		//alert(phno)
		
		$('#id_contact_form').hide();

		//var phdata = $('#id_contact_form').find('form').serialize();
		
		//alert(phdata);
		

        $('#id_contact_succ').html('').append('<h5>A OTP has been sent on your mobile please verify.</h5><div class="row form-group"><div class="col-sm-12"><a class="col-sm-4 resend-code" onclick="resendOtp(this)">Resend Code</a><input name="otp_verify" class="form-control" placeholder="Verify OTP" maxlength="4" type="text" id="otp_verify" required="required"></div></div><div class="row form-group" style="margin-top: 20px;"><div class="col-sm-12"><button type="button" onclick="verifyOtp1(this)" class="btn btn-primary col-sm-12 btn-red">Verify</button></div></div>').show();
		
		//alert('three');
		
		$.ajax({
			type: 'POST',
			url: '/websiteusers/sendOtp/',
			data: $('#id_contact_form').find('form').serialize(),
			dataType :'json',
			success: function(data) {
				if(data.type != 'success' ) {
					$('#id_contact_succ').html('').hide();
					$('#id_contact_form').show();
				}
			}
		});
		//alert('four');
	}
	
	
//Function : Submit Registration Form using Ajax
	function UserRegistrationFormSubmit() { 
		$.ajax( {
					url: '/leads/saveLead',
					type: 'POST',
					data: $('#contact-form').serialize(),
					dataType: 'json',
					success: function(data) {
						//alert(data);
						//var msg = $('.message');
						if(data.status == 'success') {
							$('#id_contact_form').css('display', 'none');
					temp_str = '<h3 class=\'regsucs\'>Thank You for your interests. </h3>';
					temp_str = temp_str + '<div>';
					temp_str = temp_str + '<p> Email :';
					temp_str = temp_str + data.email
					temp_str = temp_str + '</p><br>';
					temp_str = temp_str + '<p> Contact No :';
					temp_str = temp_str + data.usermobile
					temp_str = temp_str + '</p>';
					temp_str = temp_str + '</div>'
				
					$('#id_contact_succ').html(temp_str);
					$('#id_contact_succ').css('display', 'inline');
					$('#id_contact_succ').css('visibility', 'visible');
							} else {
					$('#id_contact_succ').append(showMesssage({'cls':'success', 'msg':'Please try again.'}));
					$('#id_contact_form').hide();
						}
						
						/*msg.html(data.message);
						
						validator.resetForm();
						contact_modal.find("input,textarea,select,button").val('').end()
						.find("input[type=checkbox], input[type=radio]").prop("checked","").end()
						.find('.validation-error').removeClass('validation-error');
						$('#interested').selectpicker('deselectAll');
						setTimeout(function() {
							$('.message').attr('class', 'message').html('');
						}, 10000);*/
					}
				});
	}