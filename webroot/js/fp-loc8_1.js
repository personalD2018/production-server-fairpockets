
$(document).ready(function () {

    // This function will be called when a value is selected from city drop-down
    function setLocation()
    {
        // find the dropdown
        var ddl = document.getElementById("id_select_city");

        // find the selected option
        var selectedOption = ddl.options[ddl.selectedIndex];

        // find the attribute value
        document.getElementById("id_search_adm_area_2").value = selectedOption.innerHTML;
        document.getElementById("id_search_adm_area_1").value = selectedOption.parentNode.label;

        //alert ( selectedOption.innerHTML );
        //alert ( selectedOption.parentNode.label );

        document.getElementById('searchInput').value = "";

        //Clear all address boxes
        /*
         document.getElementById("tbUnit0").value = "";
         document.getElementById("tbUnit1").value = "";
         document.getElementById("tbUnit2").value = "";
         document.getElementById("tbUnit3").value = "";
         document.getElementById("tbUnit4").value = "";
         */

        document.getElementById("block").value = "";
        document.getElementById("locality").value = "";
        document.getElementById("city_data").value = "";
        document.getElementById("state_data").value = "";
        /*document.getElementById("tbUnit9").value = "";*/
        document.getElementById("country_data").value = "";
        document.getElementById("pincode").value = "";

    }

    var optionsAuto = {
        radius: 20000

    };


    var iconBase = 'http://staging.fairpockets.com/webroot/img//';
    var mIcon = iconBase + 'mIcon.png';
    var mHover = iconBase + 'mHover.png';
    var mTick = iconBase + 'mTick.png';

    // Initalization scipt for GMAP
    function initialize()
    {
        /*var latlng = new google.maps.LatLng(28.5355161,77.39102649999995);*/
        if (document.getElementById('lat').value != "")
            temp_lat = document.getElementById('lat').value;
        else
            temp_lat = 28.5355161;

        if (document.getElementById('lng').value != "")
            temp_lng = document.getElementById('lng').value;
        else
            temp_lng = 77.39102649999995;

        var latlng = new google.maps.LatLng(temp_lat, temp_lng);



        var marker = new google.maps.Marker({
            map: map,
            icon: mIcon,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 13
        });

        marker.setMap(map);

        var input = document.getElementById('searchInput');
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var geocoder = new google.maps.Geocoder();

        var autocomplete = new google.maps.places.Autocomplete(input, optionsAuto);
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();

        // Setting markes to be used
        var currentMarker = null;
        var markerIcon = mIcon;
        var markerIconHover = mHover;
        var markerTick = mTick;

        var sel_action = document.getElementById('id_select_city');

        // Event Listner for city drop down.
        // gmap will point to selected city from drop dowm
        sel_action.addEventListener("change", function () {

            var address = sel_action.options[sel_action.selectedIndex].innerHTML;
            var place = new Array();
            //alert(address);

            setLocation();

            marker.setVisible(false);

            geocoder.geocode({'address': address}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK)
                {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    marker.setVisible(true);
                    marker.setIcon(markerIcon);
                    place = results[0];

                    // Just read city from address component
                    var arrAddress = place.address_components;
                    var temp_city = "";
                    for (ac = 0; ac < arrAddress.length; ac++)
                    {
                        if ((arrAddress[ac].types[0] == "locality") &&
                                (arrAddress[ac].types[1] == "political"))
                            temp_city = arrAddress[ac].long_name
                    }

                    bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), temp_city);
                    infowindow.setContent(place.formatted_address);
                    infowindow.open(map, marker);

                    /*document.getElementById('location').value = "";*/
                    document.getElementById('lat').value = "";
                    document.getElementById('lng').value = "";
                } else
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        });

        // Event Listner for location autofill.
        // gmap will point to selected location via autofill
        // Also city drop down will change appropriately

        autocomplete.addListener('place_changed', function () {

            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport)
            {
                map.fitBounds(place.geometry.viewport);
            } else
            {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            marker.setIcon(markerIcon);

            // Just read city from address component
            var arrAddress = place.address_components;
            var temp_city = "";
            for (ac = 0; ac < arrAddress.length; ac++)
            {
                if ((arrAddress[ac].types[0] == "locality") &&
                        (arrAddress[ac].types[1] == "political"))
                    temp_city = arrAddress[ac].long_name
            }

            bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), temp_city);
            infowindow.setContent(place.formatted_address);
            infowindow.open(map, marker);

            //Clear all address boxes
            /*
             document.getElementById("tbUnit0").value = "";
             document.getElementById("tbUnit1").value = "";
             document.getElementById("tbUnit2").value = "";
             document.getElementById("tbUnit3").value = "";
             document.getElementById("tbUnit4").value = "";
             */

            document.getElementById("block").value = "";
            document.getElementById("locality").value = "";
            document.getElementById("city_data").value = "";
            document.getElementById("state_data").value = "";
            /*document.getElementById("tbUnit9").value = "";*/
            document.getElementById("country_data").value = "";
            document.getElementById("pincode").value = "";

            /* document.getElementById('location').value = ""; */
            document.getElementById('lat').value = "";
            document.getElementById('lng').value = "";

        });



        // Event Listner for mouse dragstart
        // Set correct icon for mouse dragstart
        google.maps.event.addListener(marker, 'dragstart', function () {

            marker.setIcon(markerIconHover);

            document.getElementById('searchInput').value = "";


            //Clear all address boxes
            /*
             document.getElementById("tbUnit0").value = "";
             document.getElementById("tbUnit1").value = "";
             document.getElementById("tbUnit2").value = "";
             document.getElementById("tbUnit3").value = "";
             document.getElementById("tbUnit4").value = "";
             */

            document.getElementById("block").value = "";
            document.getElementById("locality").value = "";
            document.getElementById("city_data").value = "";
            document.getElementById("state_data").value = "";
            /*document.getElementById("tbUnit9").value = "";*/
            document.getElementById("country_data").value = "";
            document.getElementById("pincode").value = "";

            /*document.getElementById('location').value = "";*/
            document.getElementById('lat').value = "";
            document.getElementById('lng').value = "";


        });

        // Event Listner for mouseout.
        // Set correct icon on mouseout
        google.maps.event.addListener(marker, 'mouseout', function () {

            if ((currentMarker !== null) && (currentMarker !== marker)) {
                marker.setIcon(markerIconHover);
            }

        });

        // Event Listner for mouse dragend
        google.maps.event.addListener(marker, 'dragend', function () {

            // Reset market icons
            //clearMarkerIcons();  


            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {

                        marker.setIcon(markerIconHover);

                        // Set current marker
                        currentMarker = marker;

                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);// Set hovered map marker
                    }
                }
            });

        });

        // Event Listner for mouse click
        // Get the different values of address for current position
        // Set correct icon on mouse dragend
        google.maps.event.addListener(marker, 'click', function () {

            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {

                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);

                        marker.setIcon(markerTick);
                        document.getElementById('searchInput').value = results[0].formatted_address;
                        var arrAddress = results[0].address_components;

                        //Clear all address boxes
                        /*
                         document.getElementById("tbUnit0").value = "";
                         document.getElementById("tbUnit1").value = "";
                         document.getElementById("tbUnit2").value = "";
                         document.getElementById("tbUnit3").value = "";
                         document.getElementById("tbUnit4").value = "";
                         */
                        document.getElementById("block").value = "";
                        document.getElementById("locality").value = "";
                        document.getElementById("city_data").value = "";
                        document.getElementById("state_data").value = "";
                        /*document.getElementById("tbUnit9").value = "";*/
                        document.getElementById("country_data").value = "";
                        document.getElementById("pincode").value = "";

                        /*document.getElementById('location').value = "";*/
                        document.getElementById('lat').value = "";
                        document.getElementById('lng').value = "";

                        //Read available address object and try to get different part of address
                        // Bit of hardcoding below for read
                        for (ac = 0; ac < arrAddress.length; ac++)
                        {
                            /*
                             if (arrAddress[ac].types[0] == "street_number")
                             document.getElementById("tbUnit0").value = arrAddress[ac].long_name;
                             
                             if (arrAddress[ac].types[0] == "premise")
                             document.getElementById("tbUnit1").value = arrAddress[ac].long_name;
                             
                             if (arrAddress[ac].types[0] == "route")
                             document.getElementById("tbUnit2").value = arrAddress[ac].long_name;	
                             
                             if ((arrAddress[ac].types[0] == "neighbourhood") &&
                             (arrAddress[ac].types[1] == "political"))
                             document.getElementById("tbUnit3").value = arrAddress[ac].long_name;		
                             
                             if ((arrAddress[ac].types[0] == "political") &&
                             (arrAddress[ac].types[1] == "sublocality") &&
                             (arrAddress[ac].types[2] == "sublocality_level_3"))
                             document.getElementById("tbUnit4").value = arrAddress[ac].long_name;
                             
                             if ((arrAddress[ac].types[0] == "sublocality_level_3") &&
                             (arrAddress[ac].types[1] == "sublocality") &&
                             (arrAddress[ac].types[2] == "political"))
                             document.getElementById("tbUnit4").value = arrAddress[ac].long_name;
                             */

                            if ((arrAddress[ac].types[0] == "political") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "sublocality_level_2"))
                                document.getElementById("block").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "sublocality_level_2") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "political"))
                                document.getElementById("block").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "political") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "sublocality_level_1"))
                                document.getElementById("locality").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "sublocality_level_1") &&
                                    (arrAddress[ac].types[1] == "sublocality") &&
                                    (arrAddress[ac].types[2] == "political"))
                                document.getElementById("locality").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "locality") &&
                                    (arrAddress[ac].types[1] == "political"))
                                document.getElementById("city_data").value = arrAddress[ac].long_name

                            /*if ((arrAddress[ac].types[0] == "administrative_area_level_2") &&
                             (arrAddress[ac].types[1] == "political") )
                             document.getElementById("state_data").value = arrAddress[ac].long_name;*/

                            if ((arrAddress[ac].types[0] == "administrative_area_level_1") &&
                                    (arrAddress[ac].types[1] == "political"))
                                document.getElementById("state_data").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "country") &&
                                    (arrAddress[ac].types[1] == "political"))
                                document.getElementById("country_data").value = arrAddress[ac].long_name;

                            if ((arrAddress[ac].types[0] == "postal_code"))
                                document.getElementById("pincode").value = arrAddress[ac].long_name;
                        }

                        // Fill different parts of address
                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), document.getElementById("city_data").value);
                    }
                }
            });
        });
    }

    function bindDataToForm(address, lat, lng, city)
    {
        /* document.getElementById('location').value = address;*/
        document.getElementById('lat').value = lat;
        document.getElementById('lng').value = lng;

        //Set address in locality 
        //document.getElementById('searchInput').value = address;

        // Set city drop down based on selected locality
        var mySelObj = document.getElementById("id_select_city");
        var city_found = false;

        for (var i = 0, sL = mySelObj.length; i < sL; i++)
        {
            if (mySelObj.options[i].text == city)
            {
                mySelObj.selectedIndex = i;
                city_found = true;
                break;
            }
        }

        /*
         if ( city_found == false )
         mySelObj.selectedIndex = 0;
         */
    }

    // Clear all set marker icons
    function clearMarkerIcons()
    {
        for (var i = 0; i < map.markers.length; i++)
        {
            map.markers[i].setIcon(markerIcon);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});