$(document).ready(function() {
	$('.owl-carousel').owlCarousel({
		loop: true,
		items: 1,
		margin: 10,
		lazyLoad: true,
		nav:true,
		navigation: true,
		navText: [' ', ' '],
		dots: true,
		responsiveClass: true,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:false,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: true
			},
			1000: {
				items: 4,
				nav: true,
			}
		}
		
	});
	
});
