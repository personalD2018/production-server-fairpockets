/*
SQLyog Community v12.5.0 (64 bit)
MySQL - 5.1.73 : Database - fairpockets_prod
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fairpockets_prod` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `fairpockets_prod`;

/*Table structure for table `asst_sell_leads` */

DROP TABLE IF EXISTS `asst_sell_leads`;

CREATE TABLE `asst_sell_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `properties_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_discussion` varchar(255) NOT NULL,
  `call_type` int(11) NOT NULL,
  `doj1` varchar(255) NOT NULL,
  `doj2` varchar(255) NOT NULL,
  `activity` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `properties_id` (`properties_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `asst_sell_leads` */

/*Table structure for table `asst_sell_rents` */

DROP TABLE IF EXISTS `asst_sell_rents`;

CREATE TABLE `asst_sell_rents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) NOT NULL,
  `prop_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `service_start` varchar(255) DEFAULT NULL,
  `service_end` varchar(255) DEFAULT NULL,
  `posted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `prop_id` (`prop_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `asst_sell_rents_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `websiteusers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `asst_sell_rents` */

/*Table structure for table `builder_approval_notifictions` */

DROP TABLE IF EXISTS `builder_approval_notifictions`;

CREATE TABLE `builder_approval_notifictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `builder_id` int(11) NOT NULL,
  `inquiry_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `builder_approval_notifictions` */

insert  into `builder_approval_notifictions`(`id`,`user_id`,`builder_id`,`inquiry_id`,`date`) values 
(1,0,59,14,'2017-12-02 14:08:11'),
(2,0,61,15,'2017-12-02 23:47:17'),
(3,0,62,16,'2017-12-03 13:25:33');

/*Table structure for table `builders` */

DROP TABLE IF EXISTS `builders`;

CREATE TABLE `builders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borg_contact` varchar(255) NOT NULL,
  `borg_landline` varchar(255) NOT NULL,
  `borg_web` varchar(255) NOT NULL,
  `borg_year` int(11) NOT NULL,
  `borg_about` text NOT NULL,
  `borg_mgmt` varchar(255) NOT NULL,
  `searchInput` text NOT NULL,
  `borg_addr` text NOT NULL,
  `city` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `websiteuser_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL DEFAULT '0',
  `block` varchar(255) NOT NULL,
  `locality` varchar(255) NOT NULL,
  `city_data` varchar(255) NOT NULL,
  `state_data` varchar(255) NOT NULL,
  `country_data` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `builders` */

insert  into `builders`(`id`,`borg_contact`,`borg_landline`,`borg_web`,`borg_year`,`borg_about`,`borg_mgmt`,`searchInput`,`borg_addr`,`city`,`status`,`websiteuser_id`,`manager_id`,`block`,`locality`,`city_data`,`state_data`,`country_data`,`pincode`,`lat`,`lng`) values 
(1,'rajnir','7777777777','http://www.facebook.com',6789,'hghhhhhhhh','ddddddddddd','Ayodhya Bypass Rd, K-Sector, Ayodhya Bypass, Bhopal, Madhya Pradesh 462010, India','test',1,3,1,0,'K-Sector','Ayodhya Bypass','Bhopal','Madhya Pradesh','India','462010','23.279960865062602','77.4531270849609'),
(2,'abhimanyu','','http://www.a.c',1,'adsdasdad','','Bhopal, Madhya Pradesh, India','A123 - 1/ ',1,1,10,0,'','','','','','','',''),
(3,'abhimanyu','12345678','http://www.a.c',2000,'ssss ssssssssss ssssss','ffffffff ffffffffff ffffffffff','Durga Market, Dadri Main Rd, Bhangel, Sector - 19 A, Salarpur, Noida, Uttar Pradesh 201304, India','A123 - 1/ first floor',1,4,12,0,'Sector - 19 A','Salarpur','Noida','Uttar Pradesh','India','201304','28.535146','77.39138950000006'),
(4,'pritam singh','778912309','www.abc.co.in',9898,'asdad zzzzzzz','adsasdda   zzzzzzz','Unnamed Road, Maulana Azad National Institute of Technology, Bhopal, Madhya Pradesh 462003, India','B32 industry',1,3,13,43,'','Maulana Azad National Institute of Technology','Bhopal','Madhya Pradesh','India','462003','23.209614572002096','77.3985387670898'),
(5,'Nasir jawed','012045454545','http://www.sdfsdfd.com',1238,'dfgdfgfgfgfdg','dfgfdgd','Dadri Main Rd, Block A, Phase-2, Noida, Uttar Pradesh 201305, India','sdfsdfd dfggdfg - / sdfs 1234556',1,3,18,0,'Block A','Phase-2','Noida','Uttar Pradesh','India','201305','28.53551609999998','77.39514637304683'),
(6,'Ritesh','0120665577','http://www.pnchsheel.com',1234,',jkj clkjfldj flkjfl flkjflkkj flkjf flkjf lkjlkf flkjlkf flkflk flkkjflkj flkjflkjf kjlfk flkjlfkj flkjflkj flkkjlkjlkf fjl;jlkj flkjflkj fljfljlfkjlkkjf lkjjljlkjlkj','kjhkjh dkjhdk dkhd khdkh dkjjhkjhd kjjhkjhd kjhkhkjhd dkhkdjhkj dkh','B-10, Nangloi Sultanpuri Rd, Punjabi Basti, JJ Colony No 3, Nangloi, Delhi, 110041, India','k44, noida',1,3,19,36,'JJ Colony No 3','Nangloi','Delhi','Delhi','India','110041','28.684042','77.06580039999994'),
(7,'bharat','0120678878','www.aliveasset.com',2016,'project','my project','Dadri Main Rd, Sector - 106, Noida, Uttar Pradesh 201304, India','C66 Noida Sector 2',1,2,26,43,'','Sector - 106','Noida','Uttar Pradesh','India','201304','28.5355161','77.39102649999995'),
(8,'bharat','0120678878','www.aliveasset.com',2016,'project','my project','Dadri Main Rd, Sector - 106, Noida, Uttar Pradesh 201304, India','C66 Noida Sector 2',1,2,26,43,'','Sector - 106','Noida','Uttar Pradesh','India','201304','28.5355161','77.39102649999995'),
(9,'Uday','01122222222','www.majak.com',1987,'trust me','i am good','Unnamed Road, Sector B, Govindpura, Bhopal, Madhya Pradesh 462023, India','kjgjhg iuiu ',1,2,28,43,'Sector B','Govindpura','Bhopal','Madhya Pradesh','India','462023','23.2473159658598','77.44626062988277'),
(10,'Roomkey','43573700','www.pdq.com',1999,'hhhyyyppp','hhyytt','D-163, Nehru Nagar, Bijli Nagar, Kewra Bagh, Peer Gate Area, Bhopal, Madhya Pradesh 462001, India','1999,bhopal',1,3,29,43,'Kewra Bagh','Peer Gate Area','Bhopal','Madhya Pradesh','India','462001','23.2599333','77.41261499999996'),
(11,'Pritam Singh','1212121212','preet.com',1000,'preet','preet','Parsik Hill Rd, Sector 30, CBD Belapur, Navi Mumbai, Maharashtra 400614, India','ramprastha',10,3,37,36,'Sector 30','CBD Belapur','Navi Mumbai','Maharashtra','India','400614','19.02639531970622','73.03292406616208'),
(12,'Rajeev','0120-4560666','www.elite-group.co',2010,'Elite Group was born in July 2010 with Flagship Company as HR Oracle Developers Pvt. Ltd. which was a SPV between HR Buildcon Pvt Ltd. and Oracle Real Tech Pvt Ltd. Both these companies have been in real estate sector from quite a long time. ','Vikas Gupta, Uma Shanker, Vinod Bahl, Pramod Bahl','H058, H Block, Sector 63, Noida, Uttar Pradesh 201301, India','H-58, sector 63, Noida, UP',85,3,45,43,'H Block','Sector 63','Noida','Uttar Pradesh','India','201301','28.629556559192018','77.3775478904297'),
(13,'Siddhartha Murari','989989899898','www.sid.com',2001,'fdskjfhdskjfah','kdsfkjasdfkjj','DSF Fast Food, Mumbai, Maharashtra, India','192, CST Rd, Friends Colony, Hallow Pul, Kurla, Mumbai, Maharashtra 400070, India',11,3,59,0,'Hallow Pul','Kurla','Mumbai','Maharashtra','India','400070','19.0759837','72.87765590000004'),
(14,'Siddhartha Murari','989989899898','www.sid.com',2001,'fdskjfhdskjfah','kdsfkjasdfkjj','DSF Fast Food, Mumbai, Maharashtra, India','192, CST Rd, Friends Colony, Hallow Pul, Kurla, Mumbai, Maharashtra 400070, India',11,3,59,0,'Hallow Pul','Kurla','Mumbai','Maharashtra','India','400070','19.0759837','72.87765590000004'),
(15,'Siddhartha Murari','98989898989','www.sid.com',2010,'dsfk adskjfaskdfak;lsdfaklsf askldf','akl sdfjakl;sd fklasdjfk;lasdfk;lasf ;','College Square, Kolkata, West Bengal, India','lathar complax, Newland, College Square, Kolkata, West Bengal 700012, India',5,3,61,0,'','College Square','Kolkata','West Bengal','India','700012','22.572646','88.36389499999996'),
(16,'Sudip Khan','9432993776','www.skhan.com',2012,'zds sdf','as sad','D u m Dum Road, Tanwar Colony, Motijheel, Kolkata, West Bengal, India','lathar complax, Newland, College Square, Kolkata, West Bengal 700012, India',5,3,62,0,'','College Square','Kolkata','West Bengal','India','700012','22.572646','88.36389499999996');

/*Table structure for table `buy_rent_requirements` */

DROP TABLE IF EXISTS `buy_rent_requirements`;

CREATE TABLE `buy_rent_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_reqt` int(11) NOT NULL,
  `prop_type` int(11) NOT NULL,
  `prop_purpose` int(11) NOT NULL,
  `prop_cstat` int(11) NOT NULL,
  `prop_state` int(11) NOT NULL,
  `prop_city` int(11) NOT NULL,
  `prop_locality` varchar(255) NOT NULL,
  `prop_budget` varchar(255) NOT NULL,
  `prop_oth` varchar(255) NOT NULL,
  `prop_rmks` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `posted date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service_start` varchar(255) DEFAULT NULL,
  `service_end` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `buy_rent_requirements_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `websiteusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

/*Data for the table `buy_rent_requirements` */

insert  into `buy_rent_requirements`(`id`,`prop_reqt`,`prop_type`,`prop_purpose`,`prop_cstat`,`prop_state`,`prop_city`,`prop_locality`,`prop_budget`,`prop_oth`,`prop_rmks`,`user_id`,`status`,`posted date`,`service_start`,`service_end`) values 
(2,2,2,1,1,2,16,'jhansi nagar','12 lakhs','','',31,4,'2017-08-26 16:22:11','',''),
(3,1,2,1,1,1,2,'gauhati','2 crore','','wonderful',14,5,'2017-08-26 16:10:39','23-07-2017','23-10-2017'),
(4,1,1,1,1,1,1,'66666','6666666','66','dddddddddddd',31,2,'0000-00-00 00:00:00','',''),
(5,1,1,1,1,1,1,'66666','rrrrrrrr','dd','ddddddd',31,2,'2017-07-23 14:04:24','',''),
(6,1,1,1,1,2,1,'66666','rrrrrrrr','dd','ddddddd',31,2,'2017-07-23 14:14:58','',''),
(7,2,1,1,1,1,2,'66666','rrrrrrrr','dd','ddddddd',31,2,'2017-07-23 16:01:42','23-07-2017','23-10-2017'),
(8,2,1,1,1,1,1,'66666','rrrrrrrr','dd','ddddddd',31,2,'2017-07-23 16:42:57','',''),
(9,1,2,1,1,2,9,'aundh','34 lakhs','','',14,5,'2017-08-25 22:27:38','25-08-2017','25-11-2017'),
(10,2,1,2,2,2,11,'Daulatabad','12 lakhs','','',14,2,'2017-07-25 11:28:48','',''),
(11,1,1,1,1,2,3,'66666','6666666','66','jjjjj',31,4,'2017-08-06 23:37:21','',''),
(12,1,1,1,1,2,1,'66666','rrrrrrrr','66','gggggggg',31,2,'2017-07-25 13:56:30','',''),
(13,1,1,1,1,2,1,'66666','rrrrrrrr','66','gggggggg',31,2,'2017-07-25 13:57:27','',''),
(14,1,1,1,1,2,1,'66666','rrrrrrrr','66','gggggggg',31,2,'2017-07-25 13:58:12','',''),
(15,1,1,1,1,2,1,'66666','rrrrrrrr','66','gggggggg',31,2,'2017-07-25 14:02:10','',''),
(16,1,1,1,1,2,1,'66666','rrrrrrrr','66','gggggggg',31,2,'2017-07-25 14:02:55','',''),
(17,2,1,1,1,2,1,'12','1','','',14,2,'2017-07-26 18:02:36','',''),
(18,2,1,2,1,2,1,'sed jhjhljlkj','there','lkjlkj','lkjlkjlkj',16,2,'2017-09-01 16:48:04','',''),
(19,1,1,1,1,2,0,'ssssssssss','6666666','llllllllll','llllllll',31,3,'2017-09-04 18:35:21','04-09-2017',''),
(20,1,1,1,1,22,0,'ijlkjlkjlkjl','850000','','',42,2,'2017-09-27 00:09:50','',''),
(21,1,1,1,1,23,0,'Sanjay Colony','100000','Not yet','',44,3,'2017-10-05 15:08:19','05-10-2017',''),
(22,2,1,1,1,22,0,'Janak Puri','4500000','','',44,3,'2017-10-05 15:16:16','05-10-2017',''),
(23,1,1,2,1,22,0,'don\'t know','98p','oops','oops',49,2,'2017-10-13 06:16:10','',''),
(24,1,1,1,1,1,1,'dkjfshkjhjk','345345','asdfkaskjfh ahjfkadshfasdj k','k hfkadsfhsdfasjf ajhfjafhkafkjadsfds',59,2,'2017-11-30 21:37:26',NULL,NULL),
(25,1,1,1,1,1,1,'dkjfshkjhjk','345345','asdfkaskjfh ahjfkadshfasdj k','k hfkadsfhsdfasjf ajhfjafhkafkjadsfds',59,2,'2017-11-30 21:39:09',NULL,NULL),
(26,1,1,1,1,1,1,'dfkghdkjfhh','54345345','jhghjghjghhjg','hjggjh',59,2,'2017-11-30 21:41:58',NULL,NULL),
(27,1,1,1,1,1,1,'kjgjgjhghjg','8767876','kjhkjhjbhjbbybuiu','uiybiuyu',59,2,'2017-11-30 21:42:26',NULL,NULL),
(28,1,1,1,1,1,1,'kjgjgjhghjg','8767876','kjhkjhjbhjbbybuiu','uiybiuyu',59,2,'2017-11-30 21:43:57',NULL,NULL),
(29,1,1,1,1,1,1,'kjgjgjhghjg','8767876','kjhkjhjbhjbbybuiu','uiybiuyu',59,2,'2017-11-30 21:44:01',NULL,NULL),
(30,1,1,1,1,1,1,'kjgjgjhghjg','8767876','kjhkjhjbhjbbybuiu','uiybiuyu',59,2,'2017-11-30 21:44:19',NULL,NULL),
(31,1,1,1,1,1,1,'kjhkjhkjh','868767','kjhkjhkjh','kjhh',59,2,'2017-11-30 21:44:36',NULL,NULL),
(32,1,1,1,1,1,1,'kjhkjhkjh','868767','kjhkjhkjh','kjhh',59,2,'2017-11-30 21:46:25',NULL,NULL),
(33,1,1,1,1,1,1,'kjhkjhkjh','868767','kjhkjhkjh','kjhh',59,2,'2017-11-30 21:46:38',NULL,NULL),
(34,1,1,1,1,1,1,'hjghjghjghjg','87677','hjghjghjgh','hjggghj',59,2,'2017-11-30 21:46:55',NULL,NULL),
(35,1,1,1,1,1,1,'hjghjghjghjg','87677','hjghjghjgh','hjggghj',59,2,'2017-11-30 21:48:25',NULL,NULL),
(36,1,1,1,1,1,1,'jghhjjhkjhj','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:48:42',NULL,NULL),
(37,1,1,1,1,1,1,'jghhjjhkjhj','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:49:10',NULL,NULL),
(38,1,1,1,1,1,1,'jghhjjhkjhj','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:49:23',NULL,NULL),
(39,1,1,1,1,1,1,'jghhjjhkjhj','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:49:39',NULL,NULL),
(40,1,1,1,1,1,1,'jghhjjhkjhj','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:50:51',NULL,NULL),
(41,1,1,1,1,1,1,'1','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:51:01',NULL,NULL),
(42,1,1,1,1,1,1,'1','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:53:26',NULL,NULL),
(43,1,1,1,1,1,1,'1','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:53:40',NULL,NULL),
(44,1,1,1,1,1,1,'1','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:53:44',NULL,NULL),
(45,1,1,1,1,1,1,'1','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:53:58',NULL,NULL),
(46,1,1,1,1,1,1,'1','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:57:24',NULL,NULL),
(47,1,1,1,1,1,1,'2','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:57:32',NULL,NULL),
(48,1,1,1,1,1,1,'2','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:57:49',NULL,NULL),
(49,1,1,1,1,1,1,'2','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:57:58',NULL,NULL),
(50,1,1,1,1,1,1,'2','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:58:43',NULL,NULL),
(51,1,1,1,1,1,1,'khkjhkjhk','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:58:56',NULL,NULL),
(52,1,1,1,1,1,1,'khkjhkjhk','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:59:13',NULL,NULL),
(53,1,1,1,1,1,1,'khkjhkjhk','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 21:59:27',NULL,NULL),
(54,1,1,1,1,1,1,'khkjhkjhk','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 22:00:13',NULL,NULL),
(55,1,1,1,1,1,1,'khkjhkjhk','87687687','ugugugug','ugyugyuyugu',59,2,'2017-11-30 22:00:41',NULL,NULL);

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `groups` */

/*Table structure for table `leads` */

DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `interested` varchar(100) NOT NULL,
  `user_type` enum('dealer','individual') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `leads` */

/*Table structure for table `mst_active_inactive` */

DROP TABLE IF EXISTS `mst_active_inactive`;

CREATE TABLE `mst_active_inactive` (
  `IS_Active_ID` int(11) NOT NULL,
  `IS_ACTIVE_VALUE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`IS_Active_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_active_inactive` */

insert  into `mst_active_inactive`(`IS_Active_ID`,`IS_ACTIVE_VALUE`) values 
(0,'Not Active'),
(1,'Active');

/*Table structure for table `mst_admins` */

DROP TABLE IF EXISTS `mst_admins`;

CREATE TABLE `mst_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mst_admins` */

insert  into `mst_admins`(`id`,`username`,`password`,`role`,`created`,`modified`) values 
(1,'admin','$2a$10$3wmK1HHbd0Wt77zZVKGCWeeJtAoNBMNidma/y6QPdgel6it3TU8f.','admin','2017-02-20 11:15:22','2017-02-20 11:15:22'),
(2,'amytymail','$2a$10$.9rrUO1R45si.1QQt90Up.RNNPAn0R.kBHZ4o.GwU5cwhcETgVtQi','admin','2017-09-15 01:37:37','2017-09-15 01:37:39');

/*Table structure for table `mst_amenities` */

DROP TABLE IF EXISTS `mst_amenities`;

CREATE TABLE `mst_amenities` (
  `amenity_id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `alias_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`amenity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `mst_amenities` */

insert  into `mst_amenities`(`amenity_id`,`amenity_name`,`is_active`,`alias_name`) values 
(1,'lift',1,'lift'),
(2,'park',1,'park'),
(3,'Maintenance Staff',1,'staff'),
(4,'visitor_parking',1,'parking'),
(5,'water_storage',1,'waterstorage'),
(6,'feng_shui',1,'vaastu'),
(7,'intercomm_facility',1,'intercomm'),
(8,'security_fire_alarm',1,'fire_alarm'),
(15,'rain water harvesting',1,'rainwater'),
(16,'waste disposal',1,'waste_disposal'),
(17,'private garden/terrace',1,'tarrace'),
(18,'security personnel',1,'security_personal'),
(19,'swimming pool',1,'swimming'),
(20,'ATM',1,'atm'),
(21,'service/goods lift',1,'goods_lift'),
(22,'conference room',1,'conroom'),
(23,'centrally airconditioned',1,'ac'),
(24,'high speed internet',1,'internet'),
(25,'cafeteria/food court',1,'food');

/*Table structure for table `mst_amenity_propertyoption` */

DROP TABLE IF EXISTS `mst_amenity_propertyoption`;

CREATE TABLE `mst_amenity_propertyoption` (
  `amenity_id` int(11) NOT NULL,
  `property_type_id` int(11) NOT NULL,
  PRIMARY KEY (`amenity_id`,`property_type_id`),
  CONSTRAINT `mst_amenity_propertyoption_ibfk_1` FOREIGN KEY (`amenity_id`) REFERENCES `mst_amenities` (`amenity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_amenity_propertyoption` */

insert  into `mst_amenity_propertyoption`(`amenity_id`,`property_type_id`) values 
(1,1),
(1,2),
(1,4),
(1,21),
(1,22),
(1,23),
(1,24),
(1,28),
(1,29),
(2,1),
(2,2),
(2,4),
(2,5),
(2,6),
(3,1),
(3,2),
(3,3),
(3,4),
(3,5),
(3,6),
(3,7),
(3,21),
(3,22),
(3,23),
(3,24),
(3,28),
(3,29),
(4,1),
(4,2),
(4,3),
(4,4),
(4,5),
(4,6),
(4,21),
(4,22),
(4,23),
(4,24),
(4,28),
(4,29),
(5,1),
(5,2),
(5,3),
(5,4),
(5,5),
(5,6),
(5,21),
(5,22),
(5,23),
(5,24),
(5,28),
(5,29),
(6,1),
(6,2),
(6,3),
(6,4),
(6,5),
(6,6),
(6,28),
(6,29),
(7,1),
(7,2),
(7,4),
(8,1),
(8,2),
(8,4),
(8,21),
(8,22),
(8,23),
(8,24),
(8,28),
(8,29),
(15,3),
(15,5),
(15,6),
(15,7),
(15,23),
(15,24),
(15,25),
(15,26),
(15,27),
(16,5),
(16,6),
(16,21),
(16,22),
(16,23),
(16,24),
(16,25),
(16,26),
(16,27),
(16,28),
(16,29),
(17,5),
(17,6),
(18,7),
(18,21),
(18,22),
(18,23),
(18,24),
(18,28),
(18,29),
(19,7),
(20,21),
(20,22),
(20,23),
(20,24),
(21,21),
(21,22),
(21,23),
(21,24),
(22,21),
(22,22),
(23,21),
(23,22),
(24,22),
(25,22);

/*Table structure for table `mst_area_cities` */

DROP TABLE IF EXISTS `mst_area_cities`;

CREATE TABLE `mst_area_cities` (
  `stateCode` int(11) NOT NULL,
  `cityName` varchar(255) NOT NULL,
  `cityCode` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`cityCode`),
  UNIQUE KEY `cityName_UNIQUE` (`cityName`),
  KEY `statecode` (`stateCode`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `mst_area_cities` */

insert  into `mst_area_cities`(`stateCode`,`cityName`,`cityCode`) values 
(1,'Shimla',1),
(3,'Agra',3),
(4,'Thiruvananthapuram',4),
(6,'Kolkata',5),
(5,'Amritsar',8),
(3,'Kanpur',9),
(4,'Chennai',10),
(9,'Mumbai',11),
(9,'Thane',12);

/*Table structure for table `mst_area_countries` */

DROP TABLE IF EXISTS `mst_area_countries`;

CREATE TABLE `mst_area_countries` (
  `countryName` varchar(255) NOT NULL,
  `countryCode` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`countryCode`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `mst_area_countries` */

insert  into `mst_area_countries`(`countryName`,`countryCode`) values 
('India',1),
('USA',2),
('UK',3),
('Canada',4),
('Malaysia',5),
('Sweden',21),
('Australia',22);

/*Table structure for table `mst_area_localities` */

DROP TABLE IF EXISTS `mst_area_localities`;

CREATE TABLE `mst_area_localities` (
  `cityCode` int(11) NOT NULL,
  `localityName` varchar(255) NOT NULL,
  `localityCode` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`localityCode`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mst_area_localities` */

insert  into `mst_area_localities`(`cityCode`,`localityName`,`localityCode`) values 
(4,'Thiruvananthapuram',1),
(5,'North Dum Dum',2),
(5,'Ballygunge',3),
(5,'Salt Lake',4);

/*Table structure for table `mst_area_regions` */

DROP TABLE IF EXISTS `mst_area_regions`;

CREATE TABLE `mst_area_regions` (
  `countryCode` int(11) NOT NULL,
  `regionName` varchar(255) NOT NULL,
  `regionCode` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`regionCode`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `mst_area_regions` */

insert  into `mst_area_regions`(`countryCode`,`regionName`,`regionCode`) values 
(1,'North',1),
(1,'South',2),
(1,'East',3),
(1,'West',4),
(1,'North-East',5),
(1,'South-East',6),
(1,'South-West',7);

/*Table structure for table `mst_area_states` */

DROP TABLE IF EXISTS `mst_area_states`;

CREATE TABLE `mst_area_states` (
  `regionCode` int(11) NOT NULL,
  `stateName` varchar(255) NOT NULL,
  `stateCode` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`stateCode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `mst_area_states` */

insert  into `mst_area_states`(`regionCode`,`stateName`,`stateCode`) values 
(1,'Haryana',1),
(1,'Delhi',2),
(1,'Uttar Pradesh',3),
(2,'Kerala',4),
(4,'Punjab',5),
(3,'West Bengal',6),
(3,'Assam',7),
(3,'Mizoram',8),
(7,'Maharashtra',9),
(4,'California',10);

/*Table structure for table `mst_assign_cities` */

DROP TABLE IF EXISTS `mst_assign_cities`;

CREATE TABLE `mst_assign_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `citycode` int(11) DEFAULT NULL,
  `property_manager` int(11) DEFAULT NULL,
  `property_executive` int(11) DEFAULT NULL,
  `brokerage_manager` int(11) DEFAULT NULL,
  `brokerage_executive` int(11) DEFAULT NULL,
  `research_manager` int(11) DEFAULT NULL,
  `research_executive` int(11) DEFAULT NULL,
  `portfolio_manager` int(11) DEFAULT NULL,
  `portfolio_executive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_assign_cities` */

insert  into `mst_assign_cities`(`id`,`citycode`,`property_manager`,`property_executive`,`brokerage_manager`,`brokerage_executive`,`research_manager`,`research_executive`,`portfolio_manager`,`portfolio_executive`) values 
(1,1,NULL,30,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mst_assign_countries` */

DROP TABLE IF EXISTS `mst_assign_countries`;

CREATE TABLE `mst_assign_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryCode` int(11) DEFAULT NULL,
  `property_manager` int(11) DEFAULT NULL,
  `property_executive` int(11) DEFAULT NULL,
  `brokerage_manager` int(11) DEFAULT NULL,
  `brokerage_executive` int(11) DEFAULT NULL,
  `research_manager` int(11) DEFAULT NULL,
  `research_executive` int(11) DEFAULT NULL,
  `portfolio_manager` int(11) DEFAULT NULL,
  `portfolio_executive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_assign_countries` */

insert  into `mst_assign_countries`(`id`,`countryCode`,`property_manager`,`property_executive`,`brokerage_manager`,`brokerage_executive`,`research_manager`,`research_executive`,`portfolio_manager`,`portfolio_executive`) values 
(1,1,NULL,30,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mst_assign_localities` */

DROP TABLE IF EXISTS `mst_assign_localities`;

CREATE TABLE `mst_assign_localities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `localityCode` int(11) NOT NULL,
  `property_manager` int(11) DEFAULT NULL,
  `property_executive` int(11) DEFAULT NULL,
  `brokerage_manager` int(11) DEFAULT NULL,
  `brokerage_executive` int(11) DEFAULT NULL,
  `research_manager` int(11) DEFAULT NULL,
  `research_executive` int(11) DEFAULT NULL,
  `portfolio_manager` int(11) DEFAULT NULL,
  `portfolio_executive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mst_assign_localities` */

insert  into `mst_assign_localities`(`id`,`localityCode`,`property_manager`,`property_executive`,`brokerage_manager`,`brokerage_executive`,`research_manager`,`research_executive`,`portfolio_manager`,`portfolio_executive`) values 
(1,2,NULL,48,NULL,NULL,NULL,NULL,NULL,NULL),
(3,2,NULL,48,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mst_assign_regions` */

DROP TABLE IF EXISTS `mst_assign_regions`;

CREATE TABLE `mst_assign_regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionCode` int(11) DEFAULT NULL,
  `property_manager` int(11) DEFAULT NULL,
  `property_executive` int(11) DEFAULT NULL,
  `brokerage_manager` int(11) DEFAULT NULL,
  `brokerage_executive` int(11) DEFAULT NULL,
  `research_manager` int(11) DEFAULT NULL,
  `research_executive` int(11) DEFAULT NULL,
  `portfolio_manager` int(11) DEFAULT NULL,
  `portfolio_executive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_assign_regions` */

insert  into `mst_assign_regions`(`id`,`RegionCode`,`property_manager`,`property_executive`,`brokerage_manager`,`brokerage_executive`,`research_manager`,`research_executive`,`portfolio_manager`,`portfolio_executive`) values 
(1,1,NULL,30,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mst_assign_states` */

DROP TABLE IF EXISTS `mst_assign_states`;

CREATE TABLE `mst_assign_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stateCode` int(11) NOT NULL,
  `property_manager` int(11) DEFAULT NULL,
  `property_executive` int(11) DEFAULT NULL,
  `brokerage_manager` int(11) DEFAULT NULL,
  `brokerage_executive` int(11) DEFAULT NULL,
  `research_manager` int(11) DEFAULT NULL,
  `research_executive` int(11) DEFAULT NULL,
  `portfolio_manager` int(11) DEFAULT NULL,
  `portfolio_executive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_assign_states` */

insert  into `mst_assign_states`(`id`,`stateCode`,`property_manager`,`property_executive`,`brokerage_manager`,`brokerage_executive`,`research_manager`,`research_executive`,`portfolio_manager`,`portfolio_executive`) values 
(1,1,NULL,30,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mst_availability` */

DROP TABLE IF EXISTS `mst_availability`;

CREATE TABLE `mst_availability` (
  `availability_id` int(11) NOT NULL,
  `availability_Name` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`availability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_availability` */

insert  into `mst_availability`(`availability_id`,`availability_Name`) values 
(1,'Ready to Move'),
(2,'Under Construction');

/*Table structure for table `mst_banks` */

DROP TABLE IF EXISTS `mst_banks`;

CREATE TABLE `mst_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `mst_banks` */

insert  into `mst_banks`(`id`,`bank_name`) values 
(1,'ICICI Bank'),
(2,'HDFC Bank'),
(3,'Citi Bank'),
(4,'State Bank of India'),
(5,'Punjab National Bank'),
(6,'Bank of Baroda'),
(7,'LIC Housing Finance'),
(8,'HSBC'),
(9,'Axis Bank'),
(10,'India Bulls'),
(11,'Bank of Maharashtra'),
(12,'Punjab & Sind Bank'),
(13,'Indian Bank'),
(14,'PNB Housing Finance'),
(15,'Canara Bank'),
(16,'Corporation Bank'),
(17,'Central Bank of India'),
(18,'IDBI Bank'),
(19,'ING Vysya'),
(20,'Kotak Mahindra'),
(21,'Syndicate Bank'),
(22,'Indian overseas Bank'),
(23,'IndusInd Bank'),
(24,'Uco Bank'),
(25,'Allahabad Bank'),
(26,'Dena Bank'),
(27,'DHFL Home Loan'),
(28,'Bajaj Finance'),
(29,'Andhra Bank'),
(30,'Vijaya Bank'),
(31,'State bank of Travancore'),
(32,'Federal Bank'),
(33,'Bank of India'),
(34,'Union Bank of India');

/*Table structure for table `mst_dealer` */

DROP TABLE IF EXISTS `mst_dealer`;

CREATE TABLE `mst_dealer` (
  `Dealer_id` int(11) NOT NULL AUTO_INCREMENT,
  `Dealer_name` varchar(250) DEFAULT NULL,
  `Mobile_no1` varchar(250) DEFAULT NULL,
  `Mobile_no2` varchar(250) DEFAULT NULL,
  `email_id` varchar(250) DEFAULT NULL,
  `Landline_no` varchar(250) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`Dealer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_dealer` */

/*Table structure for table `mst_employee_departments` */

DROP TABLE IF EXISTS `mst_employee_departments`;

CREATE TABLE `mst_employee_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departmentname` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mst_employee_departments` */

insert  into `mst_employee_departments`(`id`,`departmentname`,`status`) values 
(1,'Property',1),
(2,'Brokerage',1),
(3,'Research',1),
(4,'Protfolio',1);

/*Table structure for table `mst_employee_roles` */

DROP TABLE IF EXISTS `mst_employee_roles`;

CREATE TABLE `mst_employee_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `mst_employee_roles` */

insert  into `mst_employee_roles`(`id`,`role`) values 
(0,'admin'),
(1,'Executive'),
(2,'level1'),
(3,'level2'),
(4,'level3'),
(5,'level4'),
(6,'level5');

/*Table structure for table `mst_feature_propertyoption` */

DROP TABLE IF EXISTS `mst_feature_propertyoption`;

CREATE TABLE `mst_feature_propertyoption` (
  `feature_id` int(11) NOT NULL,
  `property_type_id` int(11) NOT NULL,
  PRIMARY KEY (`feature_id`,`property_type_id`),
  KEY `property_type_id` (`property_type_id`),
  CONSTRAINT `mst_feature_propertyoption_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `mst_features` (`feature_id`),
  CONSTRAINT `mst_feature_propertyoption_ibfk_2` FOREIGN KEY (`property_type_id`) REFERENCES `mst_property_type_options` (`property_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_feature_propertyoption` */

insert  into `mst_feature_propertyoption`(`feature_id`,`property_type_id`) values 
(1,1),
(1,2),
(1,4),
(1,8),
(1,9);

/*Table structure for table `mst_features` */

DROP TABLE IF EXISTS `mst_features`;

CREATE TABLE `mst_features` (
  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `featureName` varchar(255) NOT NULL,
  `active_feature` int(11) NOT NULL DEFAULT '1',
  `Alias_Name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`feature_id`),
  KEY `active_feature` (`active_feature`),
  CONSTRAINT `mst_features_ibfk_1` FOREIGN KEY (`active_feature`) REFERENCES `mst_active_inactive` (`IS_Active_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `mst_features` */

insert  into `mst_features`(`feature_id`,`featureName`,`active_feature`,`Alias_Name`) values 
(1,'Super Builtup Area',1,'sbua'),
(2,'Built Up Area',1,'build_up_area'),
(3,'Carpet Area',1,'carpet_area'),
(4,'bedrooms',1,'bedrooms'),
(5,'bathrooms',1,'bathrooms'),
(6,'balconies',1,'balconies'),
(7,'age',1,'age_of_property'),
(8,'Furnishing',1,'furnishing'),
(9,'Total Floors',1,'total_floors'),
(10,'Property On Floor',1,'on_floor'),
(11,'Reserved Parking',1,'res_parking_1'),
(12,'Other Rooms 1',1,'other_room1'),
(13,'Width Of Facing Road',1,'width_of_fac_road'),
(14,'Overlooking',1,'overlook1'),
(15,'Bachelors Allowed',1,'bachelor_all'),
(16,'Pet Allowed',1,'pet_all'),
(17,'Non Vegetarain',1,'non_veg'),
(18,'Water Source - Supply',1,'watersource1'),
(20,'Plot Area',1,'plot_area'),
(21,'Length of Plot',1,'length_plot'),
(22,'Width Of Plot',1,'width_plot'),
(23,'Floors Allowed For Construction',1,'allowed_floor'),
(24,'maintance',1,NULL),
(25,'Is Boundary Wall Made',1,'boundary_wall'),
(26,'Socity Type',1,'society_type'),
(28,'Facing',1,'facing'),
(30,'Floors in property',1,'in_floor'),
(31,'Type of flooring',1,'type_of_flooring'),
(32,'Power Backup',1,'power_backup'),
(33,'Washrooms',1,'washrooms'),
(34,'Configure',1,'Configure'),
(35,'property_description',1,'property_description'),
(36,'Water Source - Storage',1,'watersource2'),
(37,'Covered Parking',1,'no_covered'),
(38,'Open Parking',1,'no_open'),
(39,'Other Rooms 2',1,'other_room2'),
(40,'Other Rooms 3',1,'other_room3'),
(41,'Other Rooms 4',1,'other_room4'),
(42,'overlook2',1,'overlook2'),
(43,'overlook3',1,'overlook3'),
(44,'overlook4',1,'overlook4'),
(45,'res_parking_2',1,'res_parking_2'),
(46,'res_parking_3',1,'res_parking_3'),
(47,'superbuilt_area_insq\r\n',1,'superbuilt_area_insq'),
(48,'build_up_area_insq',1,'build_up_area_insq'),
(49,'carpet_area_insq',1,'carpet_area_insq'),
(50,'plot_area_insq',1,'plot_area_insq'),
(51,' 60 feet road',1,' 60 feet road'),
(52,'superbuilt_area_insq',1,'superbuilt_area_insq');

/*Table structure for table `mst_list_of_property` */

DROP TABLE IF EXISTS `mst_list_of_property`;

CREATE TABLE `mst_list_of_property` (
  `List_of_property_ID` int(11) NOT NULL,
  `Description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`List_of_property_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_list_of_property` */

insert  into `mst_list_of_property`(`List_of_property_ID`,`Description`) values 
(1,'Sell'),
(2,'Rent'),
(3,'PG');

/*Table structure for table `mst_ownership_type` */

DROP TABLE IF EXISTS `mst_ownership_type`;

CREATE TABLE `mst_ownership_type` (
  `ownership_type_id` int(11) NOT NULL,
  `ownership_type_Name` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`ownership_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_ownership_type` */

insert  into `mst_ownership_type`(`ownership_type_id`,`ownership_type_Name`) values 
(1,'Freehold'),
(2,'leasehold'),
(3,'POWER of attorney');

/*Table structure for table `mst_properties_status` */

DROP TABLE IF EXISTS `mst_properties_status`;

CREATE TABLE `mst_properties_status` (
  `status_id` int(11) NOT NULL,
  `status` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_properties_status` */

insert  into `mst_properties_status`(`status_id`,`status`) values 
(1,'saved'),
(2,'under approvel'),
(3,'approved & active'),
(4,'deactive'),
(5,'DELETE');

/*Table structure for table `mst_property_type_options` */

DROP TABLE IF EXISTS `mst_property_type_options`;

CREATE TABLE `mst_property_type_options` (
  `option_name` varchar(255) NOT NULL,
  `transaction_type` int(11) NOT NULL,
  `property_option_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`property_option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `mst_property_type_options` */

insert  into `mst_property_type_options`(`option_name`,`transaction_type`,`property_option_id`) values 
('Apartment',1,1),
('Studio Appartment',1,2),
('Residential Land',1,3),
('Independent Builder Floor',1,4),
('Independent House',1,5),
('Independent Villa',1,6),
('Farm House',1,7),
('Commercial Office Space',2,8),
('Office In IT Park',2,9),
('Commercial Shop',2,10),
('Commercial Showroom',2,11),
('Commercial Land\r\n',2,12),
('Agricultural Land',2,13),
('Factory',2,14),
('Ware House',2,15),
('Industrial Land',2,16);

/*Table structure for table `mst_transaction_type` */

DROP TABLE IF EXISTS `mst_transaction_type`;

CREATE TABLE `mst_transaction_type` (
  `transaction_type_ID` int(11) NOT NULL,
  `Description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`transaction_type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_transaction_type` */

insert  into `mst_transaction_type`(`transaction_type_ID`,`Description`) values 
(1,'Resale'),
(2,'New Property');

/*Table structure for table `mst_userrole` */

DROP TABLE IF EXISTS `mst_userrole`;

CREATE TABLE `mst_userrole` (
  `userrole_ID` int(11) NOT NULL,
  `Description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`userrole_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_userrole` */

insert  into `mst_userrole`(`userrole_ID`,`Description`) values 
(1,'Owner'),
(2,'Broker'),
(3,'Builder'),
(4,'Admin');

/*Table structure for table `order_histories` */

DROP TABLE IF EXISTS `order_histories`;

CREATE TABLE `order_histories` (
  `service_name` varchar(255) NOT NULL,
  `service_start` varchar(255) DEFAULT NULL,
  `service_end` varchar(255) DEFAULT NULL,
  `Amount` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `order_histories` */

insert  into `order_histories`(`service_name`,`service_start`,`service_end`,`Amount`,`status`,`date`,`id`,`order_id`,`user_id`) values 
('Assisted Sell/Rent','','','',0,'2017-09-27 00:17:01',18,10,42),
('Assisted Sell/Rent','','','',0,'2017-09-27 00:33:06',19,11,42),
('Property Management','','','',0,'2017-09-27 00:36:17',10,12,42),
('Assisted Buy/Rent','05-10-2017','','2500000',3,'2017-10-05 15:07:03',21,13,44),
('Portfolio Management','05-10-2017','','',3,'2017-10-13 05:51:42',22,14,49),
('Property Management','05-10-2017','','1',3,'2017-10-05 15:55:38',11,15,44),
('Assisted Buy/Rent','','','',0,'2017-10-13 06:16:10',23,16,49),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:39:04',24,17,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:39:09',25,18,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:41:58',26,19,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:42:27',27,20,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:43:57',28,21,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:44:01',29,22,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:44:19',30,23,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:44:36',31,24,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:46:25',32,25,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:46:38',33,26,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:46:56',34,27,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:48:25',35,28,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:48:42',36,29,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:49:10',37,30,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:49:23',38,31,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:49:39',39,32,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:50:51',40,33,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:51:02',41,34,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:53:26',42,35,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:53:40',43,36,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:53:44',44,37,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:53:58',45,38,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:57:24',46,39,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:57:32',47,40,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:57:49',48,41,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:57:58',49,42,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:58:43',50,43,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:58:56',51,44,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:59:13',52,45,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 21:59:27',53,46,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 22:00:13',54,47,59),
('Assisted Buy/Rent',NULL,NULL,NULL,NULL,'2017-11-30 22:00:41',55,48,59);

/*Table structure for table `otps` */

DROP TABLE IF EXISTS `otps`;

CREATE TABLE `otps` (
  `otp` varchar(45) DEFAULT NULL,
  `expiry` varchar(45) DEFAULT NULL,
  `id` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `otps` */

insert  into `otps`(`otp`,`expiry`,`id`) values 
('7318','1512092246041','8961112533'),
('8879','1512288074155','9432993776'),
('5245','1512239124528','9674544035'),
('2209','1512533772222','9971992627');

/*Table structure for table `port_services` */

DROP TABLE IF EXISTS `port_services`;

CREATE TABLE `port_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `port_name` varchar(255) NOT NULL,
  `port_prop` varchar(255) NOT NULL,
  `posted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service_start` varchar(255) NOT NULL,
  `service_end` varchar(255) NOT NULL,
  `payment` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) NOT NULL,
  `CAGR` varchar(255) NOT NULL,
  `latest_pdf` varchar(255) NOT NULL,
  `Valuation` varchar(255) NOT NULL,
  `Created_on` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `port_services` */

insert  into `port_services`(`id`,`user_id`,`port_name`,`port_prop`,`posted_date`,`service_start`,`service_end`,`payment`,`payment_status`,`CAGR`,`latest_pdf`,`Valuation`,`Created_on`,`status`) values 
(9,31,'dddddddd','6','2017-08-26 16:19:06','10-08-2017','22-08-2017','53','2','5','','5','08/24/2017',2),
(10,14,'Port-1','7','2017-08-06 21:22:56','','','','','','','','',NULL),
(11,14,'Port-2','8,9','2017-08-09 15:31:22','','',NULL,'','','','','',NULL),
(12,14,'Port-21','\\\"8\\\",\\\"9\\\"','2017-08-22 15:24:43','22-08-2017','22-11-2017','21','','23','upload/portfoliopdf/599bff63aa406.pdf','23','07/31/2017',3),
(13,14,'Port-22','\\\"8\\\",\\\"9\\\"','2017-08-09 17:53:13','','','','','','','','',2),
(14,31,'test','6,10,11,12','2017-08-10 11:56:32','','','5','','5','','44','08/15/2017',4),
(15,14,'Port-23','8,9','2017-08-21 22:48:38','21-08-2017','21-11-2017','320','','345','upload/portfoliopdf/599b15ee1d2a6.pdf','2323343','08/21/2017',3),
(16,16,'Residential','13,14','2017-09-01 17:20:18','','',NULL,'','','','','',NULL),
(17,16,'alpha','13,14','2017-11-06 16:47:25','06-11-2017','','1200','','20','upload/portfoliopdf/5a0043b4ad271.JPG','1200','11/07/2017',4),
(18,31,'test','10,11','2017-09-04 15:45:10','04-09-2017','','45','','','','','',1),
(19,31,'sssssss','10,11','2017-09-04 15:42:47','','',NULL,'','','','','',NULL),
(20,31,'gggggggggggg','11,12','2017-09-04 18:33:04','04-09-2017','','67','','','','','',1),
(21,44,'My Portfolio1','17','2017-10-05 13:22:42','05-10-2017','','2500000','','6','upload/portfoliopdf/59d5e4ca60cf1.doc','2500000','10/05/2017',1),
(22,49,'Lol','18','2017-10-13 05:51:41','','',NULL,'','','','','',NULL);

/*Table structure for table `portfolios` */

DROP TABLE IF EXISTS `portfolios`;

CREATE TABLE `portfolios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_type` int(11) NOT NULL,
  `prop_toption` varchar(255) NOT NULL,
  `prop_proj` varchar(255) NOT NULL,
  `prop_addr` varchar(255) NOT NULL,
  `prop_locality` varchar(255) NOT NULL,
  `prop_city` int(11) NOT NULL,
  `prop_stat` varchar(255) DEFAULT NULL,
  `prop_desc` varchar(255) NOT NULL,
  `prop_area` varchar(255) NOT NULL,
  `prop_cfg` varchar(255) NOT NULL,
  `prop_pprice` varchar(255) NOT NULL,
  `prop_pdate` varchar(255) NOT NULL,
  `prop_mprice` varchar(255) NOT NULL,
  `prop_mrent` varchar(255) NOT NULL,
  `prop_oth` varchar(255) NOT NULL,
  `prop_rsn` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `Updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prop_curr_val` varchar(255) DEFAULT NULL,
  `prop_curr_rent` varchar(255) DEFAULT NULL,
  `prop_5yr_val` varchar(255) DEFAULT NULL,
  `prop_5yr_rent` varchar(255) DEFAULT NULL,
  `prop_advice` varchar(255) DEFAULT NULL,
  `prop_cagr` varchar(255) DEFAULT NULL,
  `prop_state` int(11) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `portfolios` */

insert  into `portfolios`(`id`,`prop_type`,`prop_toption`,`prop_proj`,`prop_addr`,`prop_locality`,`prop_city`,`prop_stat`,`prop_desc`,`prop_area`,`prop_cfg`,`prop_pprice`,`prop_pdate`,`prop_mprice`,`prop_mrent`,`prop_oth`,`prop_rsn`,`user_id`,`Updated_date`,`prop_curr_val`,`prop_curr_rent`,`prop_5yr_val`,`prop_5yr_rent`,`prop_advice`,`prop_cagr`,`prop_state`,`created_date`) values 
(6,2,'27','ss','ffffffffff','ddddd',14,'Self','ffffffff','9','Shared','34343','20 August 2017','45','33','ffffffff','fffffffffff',0,'2017-08-10 22:45:55','3','3','3','3','3','3',2,NULL),
(7,2,'Independent Villa','case green','c34','',12,'2','Independent Villa','23','PentHouse','123123','','232323','','Sell Out','retirment',0,'2017-08-08 22:19:46','','','','','','',NULL,NULL),
(8,2,'25','abb','abb','abb',11,'Rented','abb - bcc -cddd','1233','1RK','123','02 August 2017','123','78','ad','ad',0,'2017-08-10 23:35:23','12','12','12','12','asdf','12',2,NULL),
(9,1,'7','','ffff','ffff',4,'Rented','fffffffffffffffffff','1111111111','5BHK','1111111','17 August 2017','11111111','111111111','dddddddddd','dddddddddddd',0,'2017-08-21 22:38:11','9','9','9','9','9','9',2,NULL),
(10,2,'22','7777777','90','90',17,'Occupied','kkkkkkkkkkkkk','8','None','90','09 August 2017','89','90','78','90',31,'2017-08-09 17:30:57','','','','','','',2,NULL),
(11,1,'Apartment','ss','d','ddddddddddddddddddddd',14,'Rented','ss','4','1RK','4','01 August 2017','3','3','f','fffff',31,'2017-08-09 22:55:55','','','','','','',2,NULL),
(12,1,'Apartment','ss','d','ddddddddddddddddddddd',0,'Rented','ss','4','1RK','4','01 August 2017','3','3','f','fffff',31,'2017-08-09 22:58:47','','','','','','',2,'2017-08-09 22:58:47'),
(13,1,'Apartment','jhgjhgjhg','jhhg jhgjhg jhgj ','kjhkjh',9,'Rented','hfjhjh jhgjhg jgj jhg','87687678','3BHK','987987','12 September 2017','876876','56678','jhgjhf jhhgjg jg jhg','jhgjh jgjh hjgjhg jhgjh jh',16,'2017-09-01 17:11:09','','','','','','',2,'2017-09-01 17:11:09'),
(14,1,'Apartment','kjjk khkjh ','jhgjhg jgjhg jhgjhg jgjgjhgh jhhg','kjkhkjhkjh kjh kjh',9,'Self','jhjhg jhgjhgj jhgjhgj jgjh','876876','3BHK','76786','09 September 2017','8867786','876876','jhgjh jjg jgjhg jhgjhg','jhgjhg hjgjhg jg',16,'2017-09-01 17:16:09','','','','','','',2,'2017-09-01 17:16:08'),
(15,1,'Apartment','','aaaaaaaa','sssssss',16,'Rented','dddddd','123','5BHK','12332','04 September 2017','123123','123123','','',14,'2017-09-02 00:03:33','','','','','','',2,'2017-09-02 00:03:33'),
(16,1,'1','Shokeen Villa','11 A','Ranaji Enclave',90,'Self','egwegrwerg','900','5BHK','50000000','20 September 2016','25000','25000','bwerbwerb','berberbter',42,'2017-09-26 23:57:08','','','','','','',22,'2017-09-26 20:16:29'),
(17,1,'1','Sai Apartment','X-123','Mayur Vihar',90,'Vacant','3BHK available for rent','1200','3BHK','3500000','04 June 2014','4000000','12000','','',44,'2017-10-05 15:02:36','','','','','','',22,'2017-10-05 12:39:49'),
(18,1,'Studio Apartment','individual portfolio','testing address','testing',30,'Rented','testing portfolio description','11111','None','5000','11 October 2017','4800','10','desired action','reason field',49,'2017-10-13 05:44:17','','','','','','',22,'2017-10-13 05:44:17'),
(19,1,'1','emami city','some address','dum dum',3,'Rented','some description','3424','2BHK','324324324','','67776777','6757656','ghfghhgf','jhfgh',59,'2017-11-30 21:02:03',NULL,NULL,NULL,NULL,NULL,NULL,3,'2017-11-30 21:02:03');

/*Table structure for table `portmgmts` */

DROP TABLE IF EXISTS `portmgmts`;

CREATE TABLE `portmgmts` (
  `prop_type` int(11) NOT NULL,
  `prop_toption` varchar(255) NOT NULL,
  `prop_proj` varchar(255) NOT NULL,
  `prop_addr` varchar(255) NOT NULL,
  `prop_locality` varchar(255) NOT NULL,
  `prop_city` int(11) NOT NULL,
  `prop_state` varchar(255) NOT NULL,
  `prop_desc` varchar(255) NOT NULL,
  `prop_area` varchar(255) NOT NULL,
  `prop_cfg` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_Date` varchar(255) DEFAULT NULL,
  `Updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `portmgmts` */

insert  into `portmgmts`(`prop_type`,`prop_toption`,`prop_proj`,`prop_addr`,`prop_locality`,`prop_city`,`prop_state`,`prop_desc`,`prop_area`,`prop_cfg`,`user_id`,`created_Date`,`Updated_date`,`id`) values 
(2,'23','t','ttttttt','tttttt',15,'2','tttttttttt','jjjjjj','Studio',31,'','2017-08-17 12:19:24',1),
(1,'1','ddddddddd','rrrrrrrrrrrrrrrrrr','dddd',16,'2','r','34','5BHK',31,'','2017-08-17 12:27:35',2),
(1,'1','','stud','stud',16,'2','stud','12','None',14,'','2017-08-17 12:59:55',3),
(2,'24','','cn','cm',12,'2','cm','12','None',14,'','2017-08-18 15:17:31',4),
(1,'2','Lol','A.lol','Chennai',17,'2','Lol','10000','PentHouse',14,'','2017-08-19 22:44:24',5),
(1,'1','kjkjh','kjkjh','kjkj',9,'2','kjkjhkjhkjhkjhkjkjkjhkjhkjhkjkjkjkjhkjhkjkjkjhkjhkjhkjkjkjkjkjkj','100','2BHK',16,'','2017-09-01 16:03:42',6),
(2,'22','fhsdfhsdf','hsdfhsdfhsdf','hsdhfsdfhsdf',86,'22','hdsfhsdfhsdfhsd','2500','None',42,'','2017-09-23 16:27:22',7),
(1,'1','jkhjkhkjhkjhkj','M-16, Ground Floor, Outer Circle,','jbjkbkjbkjbkjbkj',90,'22','ghkjhgkjhkjhkjhk','9000','2BHK',42,'','2017-09-27 00:04:20',8),
(1,'6','Hi Rise Apartment','Noida','Sector 50',85,'23','Renovation-Independent House of 2BHK','750','2BHK',44,'','2017-10-05 14:44:25',9),
(1,'6','Individual Project 1','Individual address','individual locality',30,'22','lols','1500','1BHK',49,'','2017-10-13 05:56:22',10),
(1,'1','dxvfvzxvzxcvzx','hkjhkjh','hkjk',5,'6','khkjh','77','None',59,NULL,'2017-11-25 13:45:16',11),
(1,'1','dfhadsfkjhasfhj','askjfhaskjdhfkjlasfh adsfha','dskjfhkjshf kadshfas',11,'9','kadsjfklshdfkds','34534535','6BHK',59,NULL,'2017-11-30 21:31:13',12);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`id`,`user_id`,`title`,`body`,`created`,`modified`) values 
(1,0,'The title','This is the post body.','2017-01-31 16:13:08',NULL),
(2,0,'A title once again','And the post body follows.','2017-01-31 16:13:08',NULL),
(3,0,'Title strikes back','This is really exciting! Not.','2017-01-31 16:13:08',NULL);

/*Table structure for table `project_details` */

DROP TABLE IF EXISTS `project_details`;

CREATE TABLE `project_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(11) NOT NULL,
  `web_link` varchar(255) DEFAULT NULL,
  `num_of_tow` int(11) DEFAULT NULL,
  `land_area` varchar(255) NOT NULL,
  `total_units` varchar(255) DEFAULT NULL,
  `launch_units` varchar(255) DEFAULT NULL,
  `cons_status` varchar(255) DEFAULT NULL,
  `proj_logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_Code` (`project_id`),
  CONSTRAINT `project_details_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `project_details` */

insert  into `project_details`(`id`,`project_id`,`web_link`,`num_of_tow`,`land_area`,`total_units`,`launch_units`,`cons_status`,`proj_logo`) values 
(2,341,'www.sid.com',6,'50',NULL,NULL,NULL,''),
(6,345,'www.sid.com',4,'2',NULL,NULL,NULL,''),
(7,346,'www.sid.com',14,'500',NULL,NULL,NULL,''),
(8,347,'www.projectlink.com',14,'40','50','40','1',''),
(9,348,'www.projectlink.com',6,'4','50','40','1',''),
(10,349,'sadfasdf.com',4,'23','30','20','1',''),
(11,350,'www.projectlink.com',2,'34','6','5','1',''),
(12,351,'www.newtown.com',3,'1','4','3','1',''),
(13,352,'www.weblink.com',1,'1',NULL,NULL,NULL,''),
(19,357,'',8,'6.17','400','200','1','5a24e9e36fb96.jpg'),
(20,358,'',6,'444','8','8','1','5a24fb98d4bd6.PNG'),
(21,359,NULL,NULL,'2800',NULL,NULL,NULL,NULL),
(22,360,'www.link.com',8,'222',NULL,NULL,NULL,'');

/*Table structure for table `project_financers` */

DROP TABLE IF EXISTS `project_financers`;

CREATE TABLE `project_financers` (
  `project_id` bigint(11) NOT NULL,
  `bank` text NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `project_financers` */

insert  into `project_financers`(`project_id`,`bank`,`id`) values 
(349,'{\"bank_name1\":\"16\",\"bank_name2\":\"10\",\"bank_name3\":\"4\"}',1),
(350,'{\"bank_name1\":\"5\",\"bank_name2\":\"20\"}',2),
(351,'{\"bank_name1\":\"5\"}',3),
(353,'null',4),
(353,'null',5),
(355,'{\"bank_name1\":\"4\"}',6),
(356,'{\"bank_name1\":\"2\",\"bank_name2\":\"1\"}',7),
(357,'{\"bank_name1\":\"1\",\"bank_name2\":\"7\",\"bank_name3\":\"9\"}',8),
(358,'{\"bank_name1\":\"1\",\"bank_name2\":\"3\"}',9);

/*Table structure for table `project_financers_old` */

DROP TABLE IF EXISTS `project_financers_old`;

CREATE TABLE `project_financers_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `bank` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `project_financers_old` */

insert  into `project_financers_old`(`id`,`project_id`,`bank`) values 
(1,3,'{\"bank_name1\":\"2\",\"bank_name2\":\"5\",\"bank_name3\":\"4\"}'),
(2,3,'{\"bank_name1\":\"2\",\"bank_name2\":\"5\",\"bank_name3\":\"4\"}'),
(3,4,'{\"bank_name1\":\"3\",\"bank_name2\":\"1\"}'),
(4,7,'{\"bank_name1\":\"1\",\"bank_name2\":\"2\"}'),
(5,10,'{\"bank_name1\":\"3\",\"bank_name2\":\"2\",\"bank_name3\":\"1\"}'),
(6,19,'{\"bank_name1\":\"1\",\"bank_name2\":\"4\"}'),
(7,21,'{\"bank_name1\":\"1\"}'),
(8,24,'{\"bank_name1\":\"1\"}'),
(10,28,'{\"bank_name1\":\"1\"}'),
(12,38,'{\"bank_name1\":\"3\"}'),
(13,42,'{\"bank_name1\":\"9\",\"bank_name2\":\"4\",\"bank_name3\":\"1\",\"bank_name4\":\"5\"}'),
(14,44,'{\"bank_name1\":\"2\"}'),
(15,45,'{\"bank_name1\":\"2\"}');

/*Table structure for table `project_images` */

DROP TABLE IF EXISTS `project_images`;

CREATE TABLE `project_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) NOT NULL,
  `project_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_Code` (`project_id`),
  CONSTRAINT `project_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `project_images` */

insert  into `project_images`(`id`,`image_name`,`project_id`) values 
(14,'upload/project_images/5a250d9c5a843.PNG',358),
(15,'upload/project_images/5a257e141c1d2.PNG',358),
(16,'upload/project_images/5a257ebaf23a0.PNG',358),
(17,'upload/project_images/5a258272e2aac.PNG',357),
(18,'upload/project_images/5a2583eb907d8.jpg',357);

/*Table structure for table `project_office_uses` */

DROP TABLE IF EXISTS `project_office_uses`;

CREATE TABLE `project_office_uses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `office_maxdis` varchar(255) NOT NULL,
  `price_reg_unit` int(11) NOT NULL,
  `office_brkg_fp` varchar(255) NOT NULL,
  `price_brokage_unit` int(11) NOT NULL,
  `office_pay_time` varchar(255) NOT NULL,
  `office_remarks` varchar(255) NOT NULL,
  `office_sales_hdname` varchar(255) NOT NULL,
  `office_sales_hdmobile` varchar(255) NOT NULL,
  `office_sales_hdemail` varchar(255) NOT NULL,
  `office_sales_ctname` varchar(255) NOT NULL,
  `office_sales_ctmobile` varchar(255) NOT NULL,
  `office_sales_ctemail` varchar(255) NOT NULL,
  `office_brochure` varchar(255) NOT NULL,
  `office_rtcard` varchar(255) NOT NULL,
  `project_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_Code` (`project_id`),
  CONSTRAINT `project_office_uses_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `project_office_uses` */

insert  into `project_office_uses`(`id`,`office_maxdis`,`price_reg_unit`,`office_brkg_fp`,`price_brokage_unit`,`office_pay_time`,`office_remarks`,`office_sales_hdname`,`office_sales_hdmobile`,`office_sales_hdemail`,`office_sales_ctname`,`office_sales_ctmobile`,`office_sales_ctemail`,`office_brochure`,`office_rtcard`,`project_id`) values 
(10,'1',1,'111',1,'test 1','test 1','test 1','1232132323','test1@test.com','test1','1234567890','test1@test.com','5a24fbfa98c68.PNG','5a24fbfa98d37.PNG',358),
(11,'7575',1,'787',1,'','','Rajeev','9560095803','rajeevelite2017@gmail.com','Rajeev','9560095803','rajeevelite2017@gmail.com','5a25812847e42.jpg','5a25812847f31.jpg',357);

/*Table structure for table `project_pricings` */

DROP TABLE IF EXISTS `project_pricings`;

CREATE TABLE `project_pricings` (
  `price_bsp` varchar(255) NOT NULL,
  `price_srtax_bsp` varchar(255) NOT NULL,
  `price_srtax_oth` varchar(255) NOT NULL,
  `price_registration` varchar(255) NOT NULL,
  `price_reg_unit` varchar(255) NOT NULL,
  `price_stamp` varchar(255) NOT NULL,
  `price_stm_unit` varchar(255) NOT NULL,
  `project_charges` text NOT NULL,
  `price_bsp_copy` varchar(255) DEFAULT NULL,
  `floor_option` varchar(255) NOT NULL,
  `floor_per_plc_amount` varchar(255) NOT NULL,
  `floor_per_option` varchar(255) NOT NULL,
  `addition_charges` text NOT NULL,
  `other_charges` text NOT NULL,
  `price_sp_offer` varchar(255) NOT NULL,
  `price_remarks` varchar(255) NOT NULL,
  `payment_plan` varchar(255) NOT NULL,
  `project_id` bigint(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `project_Code` (`project_id`),
  CONSTRAINT `project_pricings_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `project_pricings` */

insert  into `project_pricings`(`price_bsp`,`price_srtax_bsp`,`price_srtax_oth`,`price_registration`,`price_reg_unit`,`price_stamp`,`price_stm_unit`,`project_charges`,`price_bsp_copy`,`floor_option`,`floor_per_plc_amount`,`floor_per_option`,`addition_charges`,`other_charges`,`price_sp_offer`,`price_remarks`,`payment_plan`,`project_id`,`id`) values 
('4250','12','12','6','2','1','2','{\"price_pcharge1\":\"1\",\"price_pcharge_type1\":\"1\",\"price_pcharge_amt1\":\"1\",\"price_pcharge_amunit1\":\"2\",\"price_pcharge2\":\"8\",\"price_pcharge_type2\":\"1\",\"price_pcharge_amt2\":\"50\",\"price_pcharge_amunit2\":\"1\",\"price_pcharge3\":\"7\",\"price_pcharge_type3\":\"1\",\"price_pcharge_amt3\":\"150\",\"price_pcharge_amunit3\":\"1\",\"price_pcharge4\":\"6\",\"price_pcharge_type4\":\"1\",\"price_pcharge_amt4\":\"150\",\"price_pcharge_amunit4\":\"1\",\"price_pcharge5\":\"4\",\"price_pcharge_type5\":\"1\",\"price_pcharge_amt5\":\"20000\",\"price_pcharge_amunit5\":\"2\",\"price_pcharge7\":\"5\",\"price_pcharge_type7\":\"1\",\"price_pcharge_amt7\":\"20\",\"price_pcharge_amunit7\":\"1\"}',NULL,'1','','1','{\"price_core_plc1\":\"1\",\"price_core_type1\":\"1\",\"price_core_amt1\":\"1\",\"price_core_amunit1\":\"1\"}','{\"price_other_plc1\":\"0\",\"price_other_type1\":\"1\",\"price_other_amt1\":\"\",\"price_other_amunit1\":\"1\"}','No offer currently','Possession has started','{\"price_payplan1\":\"1\",\"price_payplan2\":\"1\",\"price_payplan3\":\"0\",\"price_payplan4\":\"0\"}',357,7),
('2','2','3','3','2','3','2','{\"price_pcharge1\":\"0\",\"price_pcharge_type1\":\"1\",\"price_pcharge_amt1\":\"\",\"price_pcharge_amunit1\":\"1\"}',NULL,'2','22','1','{\"price_core_plc1\":\"0\",\"price_core_type1\":\"1\",\"price_core_amt1\":\"\",\"price_core_amunit1\":\"1\"}','{\"price_other_plc1\":\"0\",\"price_other_type1\":\"1\",\"price_other_amt1\":\"\",\"price_other_amunit1\":\"1\"}','test 1','test 1','{\"price_payplan1\":\"0\",\"price_payplan2\":\"1\",\"price_payplan3\":\"0\",\"price_payplan4\":\"0\"}',358,8);

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `project_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_type` int(11) DEFAULT NULL,
  `project_highlights` varchar(255) DEFAULT NULL,
  `project_description` text,
  `project_city` int(11) DEFAULT NULL,
  `project_address` text,
  `dtp1` varchar(255) DEFAULT NULL,
  `dtp2` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `block` varchar(255) DEFAULT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `city_data` varchar(255) DEFAULT NULL,
  `state_data` varchar(255) DEFAULT NULL,
  `country_data` varchar(255) DEFAULT NULL,
  `pincode` varchar(255) DEFAULT NULL,
  `current_project` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `identity_Code` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `websiteusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=latin1;

/*Data for the table `projects` */

insert  into `projects`(`project_id`,`user_id`,`project_name`,`project_type`,`project_highlights`,`project_description`,`project_city`,`project_address`,`dtp1`,`dtp2`,`lat`,`lng`,`block`,`locality`,`city_data`,`state_data`,`country_data`,`pincode`,`current_project`,`approved`,`created_date`,`identity_Code`) values 
(341,59,'Emami Luxury',1,'some highlights asdfklasjdfkljaskldfklasf','some desc, some desc, askfjasnfkjjaskfjakldfsjklajdfs',11,'192, CST Rd, Friends Colony, Hallow Pul, Kurla, Mumbai, Maharashtra 400070, India',NULL,NULL,'19.0759837','72.87765590000004','Hallow Pul','Kurla','Mumbai','Maharashtra','India','400070',NULL,1,'2017-12-02 08:51:50',NULL),
(345,59,'some new proj',1,'askdfak sfjkas dfjaskldfjaklsfjklajfklasfj','aks jfkaslfjkadslfjads;lfkjaks;lfj a;skdfkldghkjgdfhgkjdf',5,'GD-178, GD Block, Sector III, Salt Lake City, Kolkata, West Bengal 700106, India',NULL,NULL,'22.578035269089362','88.4157367358398','Sector III','Salt Lake City','Kolkata','West Bengal','India','700106',NULL,1,'2017-12-02 08:55:44',NULL),
(346,59,'kjdfghdkjfkfj',1,'KJHKJH','HJKHKHJK',5,'49, Southern Ave, Lake Range, Kalighat, Kolkata, West Bengal 700029, India',NULL,NULL,'22.5132163','88.34886689999996','Lake Range','Kalighat','Kolkata','West Bengal','India','700029',NULL,1,'2017-12-02 13:16:01',NULL),
(347,59,'jkjkjjjkkjh',1,'kljklkljfkldf','kdfjkdsfgkdsf',5,'lathar complax, Newland, College Square, Kolkata, West Bengal 700012, India','02 December 2017','29 December 2017','22.572646','88.36389499999996','','College Square','Kolkata','West Bengal','India','700012',1,NULL,'2017-12-02 13:20:11',NULL),
(348,59,'demo current project',1,'adkfjdkfgkdfgklj','kdfjgkldsfjgksdflgjsldk fgj',5,'3R-7, GA Block, Sector III, Salt Lake City, Kolkata, West Bengal 700097, India','13 December 2017','07 January 2018','22.571853442653286','88.40457874633785','Sector III','Salt Lake City','Kolkata','West Bengal','India','700097',1,1,'2017-12-02 13:42:05',NULL),
(349,59,'test proj',1,'kjjkhj','akdsfj sdfkj',11,'Dadri Main Rd, Sector - 106, Noida, Uttar Pradesh 201304, India','06 December 2017','27 December 2017','28.5355161','77.39102649999995','','Sector - 106','Noida','Uttar Pradesh','India','201304',1,NULL,'2017-12-02 19:29:30',NULL),
(350,59,'asdwweweweweweweewewe',1,'kjhkjhkjh','jhkjhkjkj',10,'53/70, Ponappa St, Purasaiwakkam, Chennai, Tamil Nadu 600084, India','20 December 2017','19 November 2020','13.084352252422097','80.25647050571297','','Purasaiwakkam','Chennai','Tamil Nadu','India','600084',1,NULL,'2017-12-02 20:14:52',NULL),
(351,61,'Test Current Project',1,'some highlights','some description',5,'Major Arterial Road(S-E), Mahish Bathan, Mohisbathan, Dhapa, Kolkata, West Bengal 700156, India','31 December 2017','01 January 2018','22.572011954487216','88.45041233398433','Mohisbathan','Dhapa','Kolkata','West Bengal','India','700156',1,1,'2017-12-02 23:58:42',NULL),
(352,61,'Test Past Project',1,'some highlights','some desc',5,'Mahishgote 1st Lane, Krishnapur, Kestopur, Kolkata, West Bengal 700059, India',NULL,NULL,'22.585643290146244','88.44268757202144','Krishnapur','Kestopur','Kolkata','West Bengal','India','700059',NULL,1,'2017-12-03 00:01:09',NULL),
(357,45,'Elite Golf Greens',1,'More than 75% open area\r\nEvery building with a drop-off lobby\r\neach flat will be a corner flat\r\nvaastu based & eco friendly project\r\nhigh speed elevators\r\n24 hours water supply\r\n100% power back-up\r\ngrand entrance through lush green belt with splendid foun','Elite Golf Greens is an impressive high rise residential complex of 2/3/4 and 5 BHK apartments that will elevate living to the level of a Golf Resort. Designed for an elite class living experience, these homes come with premises loaded with recreational facilities.\r\nThe layout has been conceived in a way that gives privacy between the residents and visitors. Here, every flat will be a corner flat besides being eco-friendly and Vastu compliant. Elite Golf Greens will bring to you homes with splendid balconies overlooking spectacular greenery, creating a truly opulent and tranquil ambience to come home to.\r\nStrategically located in Sportscity of Noida, this property is created with a vision to surround its residents with opulence and overwhelm them with beauty besides giving hassle-free connectivity to the NCR. It\'s in close proximity to important business hubs, temple, educational institutes, hospitals and well connected to Delhi, Noida City, Greater Noida, Noida Expressway, Ghaziabad by road and metro. This wonderful location and beautiful surroundings truly complement the architectural wonder of Elite Golf Greens.',11,'99-100, 1st Rd, Bandra West, Mumbai, Maharashtra 400050, India','06 February 2013','30 September 2022','19.058136915368163','72.83295956897587','','Bandra West','Mumbai','Maharashtra','India','400050',1,1,'2017-12-04 22:44:28',NULL),
(358,62,'Sudip test1',1,'test 1','test 1',5,'83/A, Baithakkhana, Kolkata, West Bengal 700009, India','29 November 2017','29 December 2017','22.57518215288326','88.37282139160152','','Baithakkhana','Kolkata','West Bengal','India','700009',1,1,'2017-12-04 13:10:54',NULL),
(359,63,'Shri Shidyansh Ved City',NULL,'Mall, Main market, Delhi Highway','Residential land with 3 side open area and having 20ft road and this site is nearby Patanjali',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-06 10:01:31',NULL),
(360,62,'Sudip test 3',2,'dfdfgdgf','dfsfsfsf',5,'111, Narkeldanga Main Rd, Phool Bagan, Kankurgachi, Kolkata, West Bengal 700054, India',NULL,NULL,'22.574072591739416','88.38758427001949','Phool Bagan','Kankurgachi','Kolkata','West Bengal','India','700054',NULL,1,'2017-12-06 23:40:55',NULL),
(361,62,'Sudip test 3',2,'dfdfgdgf','dfsfsfsf',5,'111, Narkeldanga Main Rd, Phool Bagan, Kankurgachi, Kolkata, West Bengal 700054, India',NULL,NULL,'22.574072591739416','88.38758427001949','Phool Bagan','Kankurgachi','Kolkata','West Bengal','India','700054',NULL,NULL,'2017-12-06 23:41:19',NULL);

/*Table structure for table `prop_servicephotos` */

DROP TABLE IF EXISTS `prop_servicephotos`;

CREATE TABLE `prop_servicephotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_name` varchar(255) NOT NULL,
  `service_stage_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `prop_servicephotos` */

insert  into `prop_servicephotos`(`id`,`photo_name`,`service_stage_id`) values 
(1,'upload/propertyserviceimages/59980bc42548d.png',4),
(2,'upload/propertyserviceimages/599864a7b2d85.png',5),
(3,'upload/propertyserviceimages/599864a7b4400.png',5),
(4,'upload/propertyserviceimages/599864c4cf77d.png',6),
(5,'upload/propertyserviceimages/599864c4d0d40.png',6),
(6,'upload/propertyserviceimages/599864c4d1713.png',6),
(7,'upload/propertyserviceimages/599868eeb48cf.gif',7),
(8,'upload/propertyserviceimages/599868eeb5cf4.png',7),
(9,'upload/propertyserviceimages/599ac82ec8d3b.jpg',8),
(10,'upload/propertyserviceimages/599ac82ecaaca.png',8);

/*Table structure for table `prop_services` */

DROP TABLE IF EXISTS `prop_services`;

CREATE TABLE `prop_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `prop_sname` varchar(255) NOT NULL,
  `port_prop` varchar(255) NOT NULL,
  `prop_sdesc` text NOT NULL,
  `services` varchar(255) NOT NULL,
  `posted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service_start` varchar(255) NOT NULL,
  `service_end` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL DEFAULT '1',
  `Created_on` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `prop_services_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `websiteusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `prop_services` */

insert  into `prop_services`(`id`,`user_id`,`prop_sname`,`port_prop`,`prop_sdesc`,`services`,`posted_date`,`service_start`,`service_end`,`payment`,`Created_on`,`status`) values 
(1,31,'ddddd','2','sssssssssss','Repair,Interiors','2017-08-22 21:36:07','18-08-2017','22-08-2017','1','08/14/2017',3),
(2,14,'Pune-Whitewash','3','Whitewash','Others','2017-08-17 13:35:51','','','123','08/17/2017',4),
(3,31,'Pune-whitewash','1','ddddd','Repair,Interiors','2017-08-18 14:50:13','','','1','',0),
(4,14,'All-Comm Prop Pune','4','All services','Repair,Interiors,Utility Bills,Others','2017-11-06 16:32:02','04-09-2017','19-11-2017','123','08/09/2017',4),
(5,14,'From mobile','3','Mobile','Utility Bills,Others','2017-08-21 19:57:36','19-08-2017','19-11-2017','123','08/03/2017',3),
(6,16,'kjkjh','6','klj lklkd dlkjd ldkjd ldkjd ','Repair','2017-09-01 16:04:58','','','1','',0),
(7,16,'kjkjh','6','klj lklkd dlkjd ldkjd ldkjd ','Repair','2017-09-01 16:07:30','','','1','',0),
(8,31,'gg','2','gg','Repair,Interiors','2017-09-04 15:56:07','04-09-2017','','1','09/12/2017',1),
(9,31,'ddddd','2','mmmmmmm','Repair','2017-09-04 18:52:39','04-09-2017','04-09-2017','10','09/19/2017',3),
(10,42,'jhlkjlkjlk','8','ggiugiuhgijkhlkjhlkj','Repair','2017-09-27 00:36:17','','','1','',0),
(11,44,'Repair of house-G','9','Need complete repair work on our property ','Repair','2017-10-05 15:55:38','05-10-2017','','1','10/05/2017',1);

/*Table structure for table `prop_servicestages` */

DROP TABLE IF EXISTS `prop_servicestages`;

CREATE TABLE `prop_servicestages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prop_servicestages_ibfk_1` (`service_id`),
  CONSTRAINT `prop_servicestages_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `prop_services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `prop_servicestages` */

insert  into `prop_servicestages`(`id`,`date`,`status`,`remarks`,`service_id`) values 
(1,'08/25/2017',1,'ongoing',4),
(2,'08/20/2017',1,'repair - ongoing',4),
(3,'08/23/2017',1,'lllllllllll',4),
(4,'12/06/2017',1,'hello..',4),
(5,'08/20/2017',1,'other - ongoing',5),
(6,'08/24/2017',2,'others - closed',5),
(7,'08/29/2017',1,'asdada',5),
(8,'08/22/2017',1,'dddddddddddd',1);

/*Table structure for table `properties` */

DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `property_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `employee_id` int(11) DEFAULT NULL,
  `Employee ID` int(11) DEFAULT NULL,
  `project_id` bigint(11) NOT NULL,
  `transaction_type_id` int(11) DEFAULT NULL COMMENT '1 for sell,2 for rent',
  `offer_price` varchar(255) DEFAULT NULL,
  `offer_price_sqt` int(11) DEFAULT NULL,
  `market_price` varchar(255) DEFAULT NULL,
  `expected_monthly` varchar(255) DEFAULT NULL,
  `market_rent` varchar(255) DEFAULT NULL,
  `deposit` varchar(255) DEFAULT NULL,
  `market_price_sqt` int(11) DEFAULT NULL,
  `maintance_amount` int(11) DEFAULT NULL,
  `purchase_price` varchar(255) DEFAULT NULL,
  `address` text,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `block` varchar(255) DEFAULT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `city_data` varchar(255) DEFAULT NULL,
  `property_city` int(11) NOT NULL,
  `state_data` varchar(255) DEFAULT NULL,
  `country_data` varchar(255) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `description` text,
  `property_type_id` int(11) DEFAULT NULL,
  `possession_date` varchar(255) DEFAULT NULL,
  `booking_amount` varchar(255) DEFAULT NULL,
  `availability_id` int(11) DEFAULT NULL COMMENT '1 ready to move,2 under construction',
  `ownership_type_id` int(11) DEFAULT NULL COMMENT '1:free hold,2:lease hold, 3: Power of attorney',
  `sale_type` int(11) DEFAULT NULL,
  `construction_status` enum('1','2') DEFAULT NULL COMMENT '1:resale,2:rent',
  `features_id` int(11) DEFAULT NULL,
  `amenities_id` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL COMMENT '0:not active,1:active',
  `status_id` int(11) DEFAULT NULL COMMENT '1:saved,2:under approvel,3:approved & active,4:deactive,5:delete',
  `valuation_id` int(11) DEFAULT NULL,
  `approvalDT` date DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Dealer_id_1` int(11) DEFAULT NULL,
  `Dealer_id_2` int(11) DEFAULT NULL,
  `Dealer_id_3` int(11) DEFAULT NULL,
  `Dealer_quoted_Price` varchar(250) DEFAULT NULL,
  `userrole_ID` int(11) DEFAULT NULL,
  `identity_Code` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`property_id`),
  KEY `FK_USERID` (`user_id`),
  KEY `FK_PROP_TYPE_ID` (`property_type_id`),
  KEY `FK_OWNER_TYPE` (`ownership_type_id`),
  KEY `FK_is_active` (`is_active`),
  KEY `status_id` (`status_id`),
  KEY `features_id` (`features_id`),
  KEY `amenities_id` (`amenities_id`),
  KEY `availability_id` (`availability_id`),
  KEY `Dealer_id_1` (`Dealer_id_1`),
  KEY `Dealer_id_2` (`Dealer_id_2`),
  KEY `Dealer_id_3` (`Dealer_id_3`),
  KEY `userrole_ID` (`userrole_ID`),
  KEY `transaction_type_id` (`transaction_type_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `FK_is_active` FOREIGN KEY (`is_active`) REFERENCES `mst_active_inactive` (`IS_Active_ID`),
  CONSTRAINT `FK_OWNER_TYPE` FOREIGN KEY (`ownership_type_id`) REFERENCES `mst_ownership_type` (`ownership_type_id`),
  CONSTRAINT `FK_transaction_type_id` FOREIGN KEY (`transaction_type_id`) REFERENCES `mst_transaction_type` (`transaction_type_ID`),
  CONSTRAINT `FK_USERID` FOREIGN KEY (`user_id`) REFERENCES `websiteusers` (`id`),
  CONSTRAINT `properties_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `mst_properties_status` (`status_id`),
  CONSTRAINT `properties_ibfk_10` FOREIGN KEY (`transaction_type_id`) REFERENCES `mst_property_type_options` (`property_option_id`),
  CONSTRAINT `properties_ibfk_4` FOREIGN KEY (`availability_id`) REFERENCES `mst_availability` (`availability_id`),
  CONSTRAINT `properties_ibfk_5` FOREIGN KEY (`Dealer_id_1`) REFERENCES `mst_dealer` (`Dealer_id`),
  CONSTRAINT `properties_ibfk_6` FOREIGN KEY (`Dealer_id_2`) REFERENCES `mst_dealer` (`Dealer_id`),
  CONSTRAINT `properties_ibfk_7` FOREIGN KEY (`Dealer_id_3`) REFERENCES `mst_dealer` (`Dealer_id`),
  CONSTRAINT `properties_ibfk_8` FOREIGN KEY (`userrole_ID`) REFERENCES `mst_userrole` (`userrole_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27378763549020 DEFAULT CHARSET=latin1;

/*Data for the table `properties` */

insert  into `properties`(`property_id`,`user_id`,`employee_id`,`Employee ID`,`project_id`,`transaction_type_id`,`offer_price`,`offer_price_sqt`,`market_price`,`expected_monthly`,`market_rent`,`deposit`,`market_price_sqt`,`maintance_amount`,`purchase_price`,`address`,`lat`,`lng`,`block`,`locality`,`city_data`,`property_city`,`state_data`,`country_data`,`pincode`,`description`,`property_type_id`,`possession_date`,`booking_amount`,`availability_id`,`ownership_type_id`,`sale_type`,`construction_status`,`features_id`,`amenities_id`,`is_active`,`status_id`,`valuation_id`,`approvalDT`,`created_date`,`Dealer_id_1`,`Dealer_id_2`,`Dealer_id_3`,`Dealer_quoted_Price`,`userrole_ID`,`identity_Code`) values 
(27378763548998,59,NULL,NULL,349,NULL,'200000001',NULL,NULL,'',NULL,'',NULL,10000,NULL,'kjbhkjkhkj',NULL,NULL,NULL,'0','Mumbai',11,NULL,NULL,700065,'new desc 111',1,'3','30000001',NULL,NULL,2,'1',NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-02 22:20:20',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763548999,61,NULL,NULL,351,NULL,'20000',NULL,NULL,'',NULL,'',NULL,2000,NULL,'some house',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700054,'some description of property',1,'6','2100',NULL,NULL,1,'1',NULL,NULL,NULL,3,NULL,NULL,'2017-12-03 00:08:38',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549000,62,NULL,NULL,354,NULL,'1111112',NULL,NULL,'',NULL,'',NULL,1222,NULL,'16/1 Dumdum',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,'sfd fs df',1,'3','123323',NULL,NULL,1,'1',NULL,NULL,NULL,3,NULL,NULL,'2017-12-04 16:38:04',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549001,NULL,NULL,NULL,341,NULL,'2000000',NULL,'2000010','','','',NULL,NULL,NULL,'Deepchand Shopping Centre, 20, Station Rd, Bajaj Wadi, Railway Colony, Santacruz West, Mumbai, Maharashtra 400054, India','19.080526211056103','72.84023371982425','Railway Colony','Santacruz West','Mumbai',11,'Maharashtra','India',400054,'testing',NULL,'6','50000',NULL,NULL,2,'2',NULL,NULL,NULL,2,NULL,NULL,'2017-12-06 22:21:16',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549002,62,NULL,NULL,358,NULL,'1111112',1,NULL,'',NULL,'',NULL,1222,NULL,'16/1 Dumdum',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,'gccghvnj j j',1,'3','123323',NULL,NULL,1,'1',NULL,NULL,NULL,3,NULL,NULL,'2017-12-06 12:04:16',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549003,63,NULL,NULL,359,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'New Delhi','','','','',NULL,0,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-12-06 10:02:07',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549004,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'This Property is nearby by Patanjali.... and this having 3 side open area with 20ft road. total area is 2800Sq. feet. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-06 10:03:30',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549005,0,NULL,NULL,0,NULL,'1960000',NULL,'2520000','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'3','100000',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-06 10:05:55',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549006,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,'2017-12-06 10:06:24',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549007,0,NULL,NULL,0,NULL,'1960000',NULL,'2520000','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'3','100000',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-06 10:06:52',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549008,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'This Property is nearby by Patanjali.... and this having 3 side open area with 20ft road. total area is 2800Sq. feet. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-06 10:06:53',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549009,63,NULL,NULL,359,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'New Delhi','','','','',NULL,0,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-12-06 10:06:54',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549010,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,'2017-12-06 10:07:00',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549011,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,'2017-12-06 22:39:57',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549012,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,'2017-12-06 23:04:10',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549013,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,'2017-12-06 23:07:07',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549014,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,'2017-12-06 23:07:27',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549015,62,NULL,NULL,361,NULL,'1111112',NULL,NULL,'',NULL,'',NULL,1222,NULL,'16/1 Dumdum',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,'sefsdf',1,'3','123323',NULL,NULL,1,'1',NULL,NULL,NULL,1,NULL,NULL,'2017-12-07 01:05:47',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549016,62,NULL,NULL,347,NULL,'2000000',NULL,NULL,'',NULL,'',NULL,NULL,NULL,'123 strt',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,'test descr',2,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-12-07 01:12:53',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549017,62,NULL,NULL,345,NULL,'2000',NULL,NULL,'',NULL,'',NULL,45,NULL,'123ghjg gjg',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,'gdfhgd fhff',2,'','456',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-12-07 01:15:33',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549018,62,NULL,NULL,346,NULL,'55555',NULL,NULL,'',NULL,'',NULL,NULL,NULL,'fhfgh',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,'yhyjtjy',1,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-12-07 01:26:07',NULL,NULL,NULL,NULL,NULL,NULL),
(27378763549019,62,NULL,NULL,347,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'xdvv',NULL,NULL,NULL,NULL,'Kolkata',5,NULL,NULL,700030,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-12-07 02:07:26',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `property_amenities` */

DROP TABLE IF EXISTS `property_amenities`;

CREATE TABLE `property_amenities` (
  `property_id` bigint(11) NOT NULL,
  `amenity_id` int(11) NOT NULL,
  `amenity_value` varchar(250) DEFAULT NULL,
  `unit` varchar(90) DEFAULT NULL,
  `remarks` varchar(90) DEFAULT NULL,
  `property_amenity_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`property_amenity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=592 DEFAULT CHARSET=latin1;

/*Data for the table `property_amenities` */

insert  into `property_amenities`(`property_id`,`amenity_id`,`amenity_value`,`unit`,`remarks`,`property_amenity_id`) values 
(27378763548998,1,'No',NULL,NULL,547),
(27378763548998,2,'No',NULL,NULL,548),
(27378763548998,3,'No',NULL,NULL,549),
(27378763548998,4,'No',NULL,NULL,550),
(27378763548998,5,'No',NULL,NULL,551),
(27378763548998,6,'No',NULL,NULL,552),
(27378763548998,7,'Yes',NULL,NULL,553),
(27378763548998,8,'Yes',NULL,NULL,554),
(27378763548999,1,'No',NULL,NULL,555),
(27378763548999,2,'Yes',NULL,NULL,556),
(27378763548999,3,'No',NULL,NULL,557),
(27378763548999,4,'No',NULL,NULL,558),
(27378763548999,5,'No',NULL,NULL,559),
(27378763548999,6,'Yes',NULL,NULL,560),
(27378763548999,7,'No',NULL,NULL,561),
(27378763548999,8,'Yes',NULL,NULL,562),
(27378763549000,1,'Yes',NULL,NULL,563),
(27378763549000,2,'Yes',NULL,NULL,564),
(27378763549000,3,'No',NULL,NULL,565),
(27378763549000,4,'No',NULL,NULL,566),
(27378763549000,5,'No',NULL,NULL,567),
(27378763549000,6,'No',NULL,NULL,568),
(27378763549000,7,'No',NULL,NULL,569),
(27378763549000,8,'No',NULL,NULL,570),
(27378763549001,1,'Yes',NULL,NULL,571),
(27378763549001,2,'Yes',NULL,NULL,572),
(27378763549001,3,'Yes',NULL,NULL,573),
(27378763549001,4,'No',NULL,NULL,574),
(27378763549001,5,'Yes',NULL,NULL,575),
(27378763549001,6,'Yes',NULL,NULL,576),
(27378763549001,7,'No',NULL,NULL,577),
(27378763549001,8,'Yes',NULL,NULL,578),
(27378763549002,1,'Yes',NULL,NULL,579),
(27378763549002,2,'Yes',NULL,NULL,580),
(27378763549002,3,'Yes',NULL,NULL,581),
(27378763549002,4,'Yes',NULL,NULL,582),
(27378763549002,5,'No',NULL,NULL,583),
(27378763549002,6,'No',NULL,NULL,584),
(27378763549002,7,'No',NULL,NULL,585),
(27378763549002,8,'No',NULL,NULL,586),
(0,3,'Yes',NULL,NULL,587),
(0,4,'Yes',NULL,NULL,588),
(0,5,'Yes',NULL,NULL,589),
(0,6,'Yes',NULL,NULL,590),
(0,15,'Yes',NULL,NULL,591);

/*Table structure for table `property_details` */

DROP TABLE IF EXISTS `property_details`;

CREATE TABLE `property_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) DEFAULT NULL,
  `aggrement_type` varchar(255) DEFAULT NULL,
  `residential_type` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `builder_name` varchar(255) DEFAULT NULL,
  `sublocality_1` varchar(255) DEFAULT NULL,
  `sublocality_2` varchar(255) DEFAULT NULL,
  `super_buil_up_area` varchar(255) DEFAULT NULL,
  `buil_up_area` varchar(255) DEFAULT NULL,
  `carpet_area` varchar(255) DEFAULT NULL,
  `bedrooms` varchar(255) DEFAULT NULL,
  `bathrooms` varchar(255) DEFAULT NULL,
  `balconies` varchar(255) DEFAULT NULL,
  `add_other_room_information` varchar(255) DEFAULT NULL,
  `total_floors` varchar(255) DEFAULT NULL,
  `property_to_floor` varchar(255) DEFAULT NULL,
  `no_of_covered_parking` varchar(255) DEFAULT NULL,
  `covered` varchar(255) DEFAULT NULL,
  `open` varchar(255) DEFAULT NULL,
  `offer_price_all_inclusive` varchar(255) DEFAULT NULL,
  `offer_price_per_sqft` varchar(255) DEFAULT NULL,
  `market_price_for_this_property_per_sqft` varchar(255) DEFAULT NULL,
  `offer_price_for_alive_asset` varchar(255) DEFAULT NULL,
  `offer_price_per_sqft_for_aliveasset` varchar(255) DEFAULT NULL,
  `market_or_fair_price_of_property` varchar(255) DEFAULT NULL,
  `delta` varchar(255) DEFAULT NULL,
  `per_sqft_price_for_alive_asset` varchar(255) DEFAULT NULL,
  `maintenance_annual_1_time_per_unit_per_month` varchar(255) DEFAULT NULL,
  `property_age` varchar(255) DEFAULT NULL,
  `availability` varchar(255) DEFAULT NULL,
  `possession_by` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `ownership` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `lifts` varchar(255) DEFAULT NULL,
  `park` varchar(255) DEFAULT NULL,
  `maintenance_staff` varchar(255) DEFAULT NULL,
  `visitor_parking` varchar(255) DEFAULT NULL,
  `water_storage` varchar(255) DEFAULT NULL,
  `vasstu_compliant` varchar(255) DEFAULT NULL,
  `intercom_facility` varchar(255) DEFAULT NULL,
  `fire_alarm` varchar(255) DEFAULT NULL,
  `centrally_air_conditioned` varchar(255) DEFAULT NULL,
  `piped_gas` varchar(255) DEFAULT NULL,
  `water_purifier` varchar(255) DEFAULT NULL,
  `internet_connectivity` varchar(255) DEFAULT NULL,
  `swimming_pool` varchar(255) DEFAULT NULL,
  `community` varchar(255) DEFAULT NULL,
  `fitness_centre` varchar(255) DEFAULT NULL,
  `security_personnel` varchar(255) DEFAULT NULL,
  `shopping_center` varchar(255) DEFAULT NULL,
  `rain_water_harvesting` varchar(255) DEFAULT NULL,
  `bank_attached_property` varchar(255) DEFAULT NULL,
  `power_backup` varchar(255) DEFAULT NULL,
  `weter_source` varchar(255) DEFAULT NULL,
  `facing` varchar(255) DEFAULT NULL,
  `width_of_facing_road` varchar(255) DEFAULT NULL,
  `type_of_flooring` varchar(255) DEFAULT NULL,
  `furnishing` varchar(255) DEFAULT NULL,
  `in_a_gated_society_description` varchar(1000) DEFAULT NULL,
  `corner_property_description` varchar(1000) DEFAULT NULL,
  `city1` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `mobile_number_1` varchar(255) DEFAULT NULL,
  `mobile_number_2` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `landline_1` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mob_no` varchar(255) DEFAULT NULL,
  `price_quoted` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `name1` varchar(255) DEFAULT NULL,
  `mob_no1` varchar(255) DEFAULT NULL,
  `price_quoted1` varchar(255) DEFAULT NULL,
  `broker_2` varchar(255) DEFAULT NULL,
  `remarks1` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `link` varchar(1000) DEFAULT NULL,
  `Project_Code` bigint(20) DEFAULT NULL,
  `identity_Code` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

/*Data for the table `property_details` */

insert  into `property_details`(`id`,`user_type`,`aggrement_type`,`residential_type`,`city`,`project_name`,`builder_name`,`sublocality_1`,`sublocality_2`,`super_buil_up_area`,`buil_up_area`,`carpet_area`,`bedrooms`,`bathrooms`,`balconies`,`add_other_room_information`,`total_floors`,`property_to_floor`,`no_of_covered_parking`,`covered`,`open`,`offer_price_all_inclusive`,`offer_price_per_sqft`,`market_price_for_this_property_per_sqft`,`offer_price_for_alive_asset`,`offer_price_per_sqft_for_aliveasset`,`market_or_fair_price_of_property`,`delta`,`per_sqft_price_for_alive_asset`,`maintenance_annual_1_time_per_unit_per_month`,`property_age`,`availability`,`possession_by`,`transaction_type`,`ownership`,`images`,`lifts`,`park`,`maintenance_staff`,`visitor_parking`,`water_storage`,`vasstu_compliant`,`intercom_facility`,`fire_alarm`,`centrally_air_conditioned`,`piped_gas`,`water_purifier`,`internet_connectivity`,`swimming_pool`,`community`,`fitness_centre`,`security_personnel`,`shopping_center`,`rain_water_harvesting`,`bank_attached_property`,`power_backup`,`weter_source`,`facing`,`width_of_facing_road`,`type_of_flooring`,`furnishing`,`in_a_gated_society_description`,`corner_property_description`,`city1`,`full_name`,`mobile_number_1`,`mobile_number_2`,`email_id`,`landline_1`,`name`,`mob_no`,`price_quoted`,`remarks`,`name1`,`mob_no1`,`price_quoted1`,`broker_2`,`remarks1`,`status`,`link`,`Project_Code`,`identity_Code`) values 
(1,'Owner','Sell','Apartment','Gurgaon','DLF New Town Heights 2','','Sector 86','','250','','','1','1','','','22','1','','','','9Lac','3600','4292','900000','3600','4175','575','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','Yes','Yes','Yes','','','','','','','','','','','','','','','','','','','Unfurnished','Yes','','Sector 86','Ajay Kumar','9990456687','','Ringing','','Vinod Pardhan','9896355540','4000 to 4500rup/sqft','','','','4000 nto 4200','','As per the confirmation ready to post property on alive asset final price is 9Lac','','http://www.99acres.com/studio-apartment-flat-for-sale-in-dlf-new-town-heights-2-sector-86-gurgaon-250-sq-ft-r4-spid-R16534971?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc1MDUgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgMzk3NDAjNCMgfCA=',256,'17158193003724'),
(2,'Owner','Sell','Apartment','Gurgaon','CHD Avenue 71','','Sector 71','','2350','','','4','4','','','15','15','','','','1.30Cr','5532','5822','13000000','5532','5650','118,0851064','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Sector 71','Priti Joshi','9650400259','','Pritamj1991@gmail.com','','Rajesh Aggarwal/Aadhar Homes','9811750130','5500 to 5800rup/sqft','','Bimlesh Choudhary/Reliable Realtors','9999640915','5500 to 5800rup/sqft','','As per the confirmation he is broker ready to post property on alive asset final price is 1.55CrCr','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2350-sq-ft-r1-spid-I13644807?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDYxNDUgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNDkwNiAjMyN8ICA=',257,'17060143477539'),
(3,'Owner','Sell','Apartment','Gurgaon','CHD Avenue 71','','Sector 71','','2193','','','4','4','3','Servant Room','15','4','','','','1.25Cr','5699','5822','12500000','5700','5650','-50','5600','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','','','','','','','','Yes','','Yes','Yes','','','','Full','Municipal Corporation','West','','Vitrified','Semifurnished','Yes','Yes','Sector 71','Pankaj Kapoor','9958384646','9711158129','Panku_98@me.com','','Rajesh Aggarwal/Aadhar Homes','9811750130','5500 to 5800rup/sqft','','Bimlesh Choudhary/Reliable Realtors','9999640915','5500 to 5800rup/sqft','','As per the confirmation he is broker ready to post property on alive asset final price Is 5600rup/sqft','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2193-sq-ft-spid-H28826091?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDYxNDUgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNDkwNiAjMyN8ICA=',257,'34040668449449'),
(4,'Owner','Sell','Independent House','Gurgaon','Ansal API Esencia','','Sector 67','','2649','2400','','3','3','3','','3','Ground','','','','1.35Cr','5096','6375','13000000','4908','5125','217,4877312','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','Yes','Yes','Yes','','Yes','','','','Yes','','Yes','','','Yes','','','','','Full','','North-East','','Vitrified','Semifurnished','Yes','Yes','Sector 67','Maneesh','7798981399','','maneeshdakru@gmail.com','','Sachin Chawla/Shiv Shakti properties','9818577899','5000 to 5500rup/sqft','','Bimlesh Choudhary/Reliable Realtors','9999640915','4800 to 5200rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com','','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-ansal-api-esencia-sector-67-gurgaon-2649-sq-ft-r5-spid-U19620563?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc0ODkgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgMTUxMDQjNCMgfCA=',258,'43892494555317'),
(5,'Owner','Sell','Apartment','Gurgaon','Raheja Sampada','','Sector-92','','1370','','','3','2','3','','15','9','','','','5900000','4307','4250','5900000','4307','4310','3,430656934','5620','','','Under construction','Dec-17','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Sector-92','Rajeev Goyal','8130119227','','Goyal.rajeev9@gmail.com','','Kamal Nayan/Aadhar Homes','9711139812/','4000 to 4200rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-raheja-sampada-sector-92-gurgaon-1370-sq-ft-r1-spid-F22802783?from_src=XIDPAGE_det&pos=XIDDETVIEW',259,'3269205920431'),
(6,'Owner','Sell','Apartment','Gurgaon','Ireo The Corridors','','Sector 67 A','','1300','','','2','1','2','','19','1','','','','1Cr','7692','8542','10000000','7692','8150','457,6923077','7000','','','Ready to Move','Immediate','Resale','Freehold','','Yes','Yes','','','Yes','','Yes','','','','','','Yes','Yes','Yes','','','','','Full','','','','','','Yes','','Sector 67 A','Yashpal Narang','9811446434','','Ypnagarn@yahoo.co.in','','Aditya Kakar/3CA Inc','9910035800/9250404636','7000 to 7500rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','7200 to 7500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 7000rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-ireo-the-corridors-sector-67-a-gurgaon-1300-sq-ft-r4-spid-M24742635?from_src=XIDPAGE_det&pos=XIDDETVIEW',260,'64152323909762'),
(7,'Owner','Sell','Apartment','Gurgaon','Ireo The Corridors','','Sector 67 A','','1727','','','3','3','2','','19','8','','1 Covered','','1.4Cr','8107','8542','13700000','7933','8150','217,1685003','8800','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','','','Yes','','Yes','','','','','','Yes','Yes','Yes','','','','','Full','','','','','','Yes','','Sector 67 A','Akhil Gandhi','9811173159','9810019712','AKGrajsonsindia.com','','Aditya Kakar/3CA Inc','9910035800/9250404636','7000 to 7500rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','7200 to 7500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 8800rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ireo-the-corridors-sector-67-a-gurgaon-1727-sq-ft-r2-spid-I26816349?from_src=XIDPAGE_det&pos=XIDDETVIEW',260,'78095700419725'),
(8,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR One Phase I','','Sector 84','','1076','','','2','2','','','21','11','','','','4300000','3997','3995','4300000','3996','4066.666667','70,38413879','','','','Under construction','Mar-18','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','','','Sector 95','Mahesh','7838386075','9810249765','Maheshkumar2017@gmail.com','','Saurabh Dadhich/NGLC Realtech Pvt Ltd','9818225352/01244384354','4000 to 4200rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','4000rup/sqft','','As per the confirmation ready to post property on alive asset final price is 43Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-one-phase-1-sector-95-gurgaon-1076-sq-ft-r2-spid-I26999729?from_src=XIDPAGE_det&pos=XIDDETVIEW',261,'28282981131631'),
(9,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR One Phase I','','Sector 95','D 1004,Sector 95 Gurgaon','1265','','','3','2','1','','21','11','','1 Covered','','50Lac','3953','3995','5000000','3953','4066.666667','114,0974967','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','','','','','','','','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','North-East','20-Feet','Vitrified','Semifurnished','Yes','Yes','Sector 95','Niren Gole','9654458916','9873314561','Nirengole@yahoo.co.in','','Saurabh Dadhich/NGLC Realtech Pvt Ltd','9818225352/01244384354','4000 to 4200rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','4000rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-one-phase-1-sector-95-gurgaon-1265-sq-ft-r1-spid-Z24967719?from_src=XIDPAGE_det&pos=XIDDETVIEW',261,'39405484022189'),
(10,'Owner','Sell','Apartment','Gurgaon','DLF Express Greens','','Sector 1A IMT Manesar','','1945','','','3','3','3','Servant Room','20','4','','2 Covered','','80Lac','4114','4122','8800000','4524','4650','125,5784062','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','','','','','','','Yes','Yes','Yes','Yes','Yes','','Yes','','Full','Borewell/Tank','East','32.8-Feet','Vitrified','Unfurnished','Yes','Yes','Sector 1A IMT Manesar','Ajay','9810361867','','ajay1kaliya@gmail.com','','Vishwa Prop','9810796775','4000 to 4500rup/sqft','','','','','busy','As per the confirmation ready to post property on alive asset final price is 88Lac All inclusive','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dlf-express-greens-sector-1a-imt-manesar-gurgaon-1945-sq-ft-r3-spid-R25231316?from_src=XIDPAGE_det&pos=XIDDETVIEW',263,'112858182634525'),
(11,'Owner','Sell','Apartment','Gurgaon','Bestech Park View City','','Sector-48','','4550','','','4','5','3','Study room & Servant Room','15','14','','2 Covered','','3.7Cr','8132','8287','37000000','8132','8350','218,1318681','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','','','','','','Yes','','Sector-48','Avinash Sharma','7988636105','','Ringing','','Aruni Kumar/Koncept Realtors','9718505075','8200 to 8500rup/sqft','','','','','','Ringing','busy','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-bestech-park-view-city-sector-48-gurgaon-4550-sq-ft-spid-C28739541?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IENQMSB8IzE2IyAgfCBTRUFSQ0ggfCA0NTQxNTc3NTMyMTQ0ODkwICMyI3wgIHwgMTk1MDI5MDMsMjgzNzU0NzkgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMyNiMgfCAyIHwjNyMgIHw=',264,'32468834589166'),
(12,'Broker','Sell','Apartment','Gurgaon','DLF Belvedere Tower','','DLF City Phase II','','','2100','','3','3','2','Servant Room','20','2','','1 Covered','','1.75Cr','8334','10200','17500000','8333','8750','416,6666667','','','10 + Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','West','','Marble','Unfurnished','Yes','Yes','DLF City Phase II','Vineet Kejriwal/Kejriwal Realtors Pvt ltd','9873983771','','','','Vineet Kejriwal/Kejriwal Realtors Pvt ltd','9873983771','8500 to 9000rup/sqft','','','','','','As per the confirmation he is broker final price is1.75 Cr','','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-dlf-belvedere-towers-dlf-city-phase-2-gurgaon-2100-sq-ft-r1-spid-P6212693?from_src=XIDPAGE_det&pos=XIDDETVIEW',265,'12866594244131'),
(13,'Owner','Sell','Apartment','Gurgaon','MFG Emerald Hills','','Sector 65','','1850','1650','','3','3','3','Servant Room','3','2','','','1 Open','1.25Cr','6757','7947','12500000','6757','7933.333333','1176,576577','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','Yes','Yes','','','','','','','','','','Yes','Yes','Yes','Yes','Yes','','','Full','Municipal Corporation','South-East','40-Feet','Vitrified','Unfurnished','Yes','','Sector 65','Nikhil Deva','9818102315','','Nikhi.deva@gmail.com','','Mr Rajnish/SRK Residency Pvt Ltd','9810047296/9810009985','7800 to 8000 rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','8000rup/sqft','','As per the confirmation ready to post property on alive asset final price is 1.25Cr','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-emerald-hills-sector-65-gurgaon-1850-sq-ft-r2-spid-C26728729?from_src=XIDPAGE_det&pos=XIDDETVIEW',266,'216801702382107'),
(14,'Owner','Sell','Apartment','Gurgaon','MFG Emerald Hills','','Sector 65','','','1650','','3','3','','','5','5','','','','1.25Cr','7576','7947','12000000','7273','7933.333333','660,6060606','','','','Under construction','Mar-18','Resale','Freehold','','','','Yes','Yes','','','','Yes','','','','','','','','','','','','Full','','','','','Unfurnished','Yes','','Sector 65','Prashant Khanna','9892813828','','off','','Mr Rajnish/SRK Residency Pvt Ltd','9810047296/9810009985','7800 to 8000 rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','8000rup/sqft','','As per the confirmation ready to post property on alive asset final price is 1.20Cr','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-emerald-hills-sector-65-gurgaon-1650-sq-ft-r7-spid-X20158471?from_src=XIDPAGE_det&pos=XIDDETVIEW',266,'275174125299281'),
(15,'Owner','Sell','Apartment','Gurgaon','MFG Emerald Hills','','Sector 65','','','1650','','3','3','','','5','5','','1 Covered','','1.05Cr','6400','7947','10500000','6400','7933.333333','1533,333333','5500','','','Ready to Move','Immediate','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','Semifurnished','','','Sector 65','Surinder','8127124419','','Surinderjoj_julay@yahoo.com','','Mr Rajnish/SRK Residency Pvt Ltd','9810047296/9810009985','7800 to 8000 rup/sqft','','Vikas Singh/AccoladeDevelopers','9811033359','8000rup/sqft','','As per the confirmation ready to post property on alive asset final price is 5500rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-emerald-hills-sector-65-gurgaon-1650-sq-ft-r2-spid-T19315611?from_src=XIDPAGE_det&pos=XIDDETVIEW',266,'118546402377584'),
(16,'Owner','Sell','Apartment','Gurgaon','Ardee City','','Sector 52','','4500','4200','','4','4','3','Servant Room & Pooja Room','3','Ground','','1 Covered','','2.4Cr','5334','5695','22500000','5000','5350','350','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','Yes','','Yes','Yes','','','','','','','','Yes','Yes','Yes','Yes','','','','','','','','','','','','Sector 56','Dr V K Sabharwal','8800982135','','call after some time','','Saurabh Dadhich/NGLC Realtech Pvt Ltd','9818225352/01244384354','5200 to 5500rup/sqft','','','','','','as per the confirmation ready to post porperty on fairpockets.com not ready to share mail Id','','http://www.99acres.com/4-bhk-bedroom-independent-builder-floor-for-sale-in-ardee-city-sector-52-gurgaon-4500-sq-ft-r3-spid-J21531907?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDI0NTcgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNjAzIHwjMyMgIHw=',267,'11531092271461'),
(17,'Owner','Sell','Apartment','Gurgaon','Suncity Essel Tower','','M G Road','','','3800','','5','4','3','Servant Room','17','12','','1 Covered','','2.94Cr','7737','9817','29000000','7632','9500','1868,421053','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Borewell/Tank','North-East','32.8-Feet','Marble','Semifurnished','Yes','','M G Road','Dewaker Saffron Tiger infra','8285813642','','','','Romesh saldi/satyam Estate','9811000085','9500rup/sft','','','','','','As per the confirmation He is broker ready to post property on alive asset final price is 2.90Cr','','http://www.99acres.com/5-bhk-bedroom-apartment-flat-for-sale-in-suncity-essel-tower-mg-road-gurgaon-3800-sq-ft-spid-E28797801?from_src=XIDPAGE_det&pos=XIDDETVIEW',268,'1110026216422'),
(18,'Owner','Sell','Apartment','Gurgaon','Tulip White','','Sector 69','','','220','','1','1','1','','13','1','','','','11.5Lac','5228','5652','1000000','4545','5400','854,5454545','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','Yes','Yes','','','Yes','','','','','','','','','Yes','Yes','','Yes','','Full','','South','65.6-Feet','Vitrified','Furnished','Yes','','Sector 69','Vineet Arora','9810471914','9313773334','Vineetarora12@redifmail.com','','Sachin Chawla/Shiv Shakti properties','9818577899','5400rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 10Lac','','http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-tulip-white-sector-69-gurgaon-220-sq-ft-r2-spid-D24576491?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc4MDkgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgODMyNiAjMyN8ICA=',269,'330039149373421'),
(19,'Owner','Sell','Apartment','Gurgaon','Unitech the Residences','','Secter 33 ','','','1535','','3','4','2','Servant Room','14','Ground','','','','1.04Cr','6800','7225','10400000','6800','7000','200','6800','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 33','K.C Goel','9872668751','','kcgoel1001@Gmail.com','','Pawan Kumar/Rising Sun','8130305216','7000rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 6800rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-the-residences-sector-33-gurgaon-1535-sq-ft-r4-spid-E24434587?from_src=XIDPAGE_det&pos=XIDDETVIEW',270,'205147149809154'),
(20,'Owner','Sell','Apartment','Gurgaon','Unitech the Residences','','Secter 33 ','','','825','','1','1','1','Study room','14','Ground','','','','60Lac','7273','7225','5700000','6909','7000','90,90909091','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 33','Kalpana','8506027022','','Contactkalpanagupta@gmail.com','','Pawan Kumar/Rising Sun','8130305216','7000rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 57 not share mail ID','','http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-unitech-the-residences-sector-33-gurgaon-825-sq-ft-r1-spid-Y27377383?from_src=XIDPAGE_det&pos=XIDDETVIEW',270,'383013539198899'),
(21,'Broker','Sell','Apartment','Gurgaon','DLF Ridgewood Estate','','DLF City Phase 4','','1443','','','3','3','1','','14','5','','1 Covered','','1.3Cr','9010','9690','13000000','9009','9250','240,990991','','','10 + Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','North-East','','Marble','','Yes','','DLF City Phase 4','Anjali/ Intercity Estate','9717831669','','','','Anjali/ Intercity Estate','9717831669','9000 to 9500rup/sqft','','','','','','As per the confirmation he is broker final price is1.3 Cr','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dlf-ridgewood-estate-dlf-city-phase-4-gurgaon-1443-sq-ft-spid-I28522597?from_src=XIDPAGE_det&pos=XIDDETVIEW',271,'59816455545069'),
(22,'Broker','Sell','Apartment','Gurgaon','BPTP Park Serene','','Sector 37D','','1788','','','3','3','3','','20','9','','1 Covered','','62.58Lac','3500','3867','6200000','3468','3650','182,4384787','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','East','78.7-Feet','Vitrified','Semifurnished','Yes','','Sector 37D','Vikas Aggarwal/ I Realty','9810307978','','','','Pawan Kumar/Rising Sun','8130305216','3500 to 3800rup/sqft','','','','','','As per the confirmation he is broker final price is 62Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-bptp-park-serene-sector-37-d-gurgaon-1788-sq-ft-spid-N28574029?from_src=XIDPAGE_det&pos=XIDDETVIEW',272,'380672831148328'),
(23,'Owner','Sell','Apartment','Gurgaon','Raheja Sampada','','Sector-92','','1850','','','4','3','3','','15','1','','1 Covered','','72Lac','3892','4207','7200000','3892','4066.666667','174,7747748','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','East','196.9 Feet','Wood','Unfurnished','Yes','','Sector-92','Nitesh Advani/Nishant realty','9958010090','','','','Kamal Nayan/Aadhar Homes','9711139812/','4000 to 4200rup/sqft','','Navin/Winworld Realty','9650344337','4000rup/sqft','','As per the confirmation he is broker final price is 72Lac','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-raheja-sampada-sector-92-gurgaon-1850-sq-ft-r1-spid-E27619423?from_src=XIDPAGE_det&pos=XIDDETVIEW',259,'401534406571086'),
(24,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc Royal Greens Phase 1','','Sector-92','','','1261','','3','2','','','20','9','','','','57Lac','4521','4802','5700000','3892','4733.333333','841,3333333','','','','Under construction','Dec-17','Resale','Freehold','','Yes','','','','Yes','','','','','','','','','','','','','','','','','','','','','','','Sector 92','Ashok Kumar','9818213835','','ringing','','Mukesh Kumar/Discover Home','9716008599','4800rup/sqft','','Navin/Winworld Realty','9650344337','4600 to 4800rup/sqft','','Ringing','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-royal-greens-phase-1-sector-92-gurgaon-1261-sq-ft-r7-spid-X19482941?from_src=XIDPAGE_det&pos=XIDDETVIEW',273,'365017159300340'),
(25,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc Royal Greens Phase 1','','Sector-92','','1183','','','3','2','2','','4','4','','1 Covered','','58Lac','4903','4802','5500000','4649','4733.333333','84,13637644','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','','','','','','','','Yes','','Yes','Yes','','','','Full','','','','','','','','Sector 92','Ankur Walia','9871492866','','Ankur.walia2008@gmail.com','','Mukesh Kumar/Discover Home','9716008599','4800rup/sqft','','Navin/Winworld Realty','9650344337','4600 to 4800rup/sqft','','As per the confirmation ready to post property on alive asset final price is 55Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-royal-greens-phase-1-sector-92-gurgaon-1183-sq-ft-r1-spid-K27600101?from_src=XIDPAGE_det&pos=XIDDETVIEW',273,'87501998232043'),
(26,'Owner','Sell','Apartment','Gurgaon','Supertech Araville','','Sector-79','','1295','','','2','2','3','','24','15','','','1 Open','59.65Lac','4606','5440','6500000','5019','5066.666667','47,36164736','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','Yes','','','','','Yes','Yes','Yes','','','','','Full','','','','','','Yes','','Sector 79 ','Sandeep Bhateja','9911411099','','Sandeep_bhateja@yahoo.co.in','','Amrish Srivastava/Property station','8010222666','5000rup/sqft','','Aman/Aryan Realty','','5000 to 5200rup/sqft','','As per the confirmation ready to post property on aliveasset final price is 70Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-supertech-araville-sector-79-gurgaon-1295-sq-ft-r2-spid-E26363271?from_src=XIDPAGE_det&pos=XIDDETVIEW',274,'317602316586980'),
(27,'Owner','Sell','Apartment','Gurgaon','Supertech Araville','','Sector-79','','','1530','','2','2','','','24','10','','','','81Lac','5294','5440','8100000','5067','5066.666667','0','5300','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','Yes','','Yes','Yes','','','Yes','','','','Full','','East','','Wood','Furnished','Yes','','Sector 79 ','Sanjay Gupta','9811826258','','Sanjabsp@gmail.com','','Amrish Srivastava/Property station','8010222666','5000rup/sqft','','Aman/Aryan Realty','','5000 to 5200rup/sqft','','As per the confirmation ready to post property on aliveasset final price is 5300rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-supertech-araville-sector-79-gurgaon-1530-sq-ft-r3-spid-J26010747?from_src=XIDPAGE_det&pos=XIDDETVIEW',274,'276342598226333'),
(28,'Broker','Sell','Apartment','Gurgaon','TDI Ourania','','Golf Course Road','','4000','3200','','4','4','3','Servant Room','14','8','','1 Covered','','3.2Cr','8000','9647','32000000','8000','9250','1250','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','North-East','98.4-Feet','Marble','Furnished','Yes','Yes','Golf Course Road','Mr Raj PMC Global','8447456661','','','','Mr Raj PMC Global','8447456661','9000 to 9500rup/sqft','','','','','','As per the confirmation he is broker final price is 3.2Lac','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-tdi-ourania-golf-course-road-gurgaon-4000-sq-ft-r2-spid-D18587417?from_src=XIDPAGE_det&pos=XIDDETVIEW',275,'406787771666609'),
(29,'Broker','Sell','Apartment','Gurgaon','Vatika the Seven Lamps','','Sector 82','','1430','1215','','2','2','2','Study Room','17','11','','1 Covered','','63.46Lac','4438','4717','6300000','4406','4500','94,40559441','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','East','78.7-Feet','Wood','Unfurnished','Yes','Yes','Sector 82','Vikas/Philby Real Estate','9910077853','','','','Vikas/Philby Real Estate','9910077853','4200rup/sqft','','Maninder Singh/Real infra solution','9810802935','4000 to 4200rup/sqft','','As per the confirmation he is broker final price is 63Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-vatika-the-seven-lamps-sector-82-gurgaon-1430-sq-ft-r1-spid-S25439433?from_src=XIDPAGE_det&pos=XIDDETVIEW',276,'46217956399949'),
(30,'Broker','Sell','Apartment','Gurgaon','Tulip Petals','','Sector-89','','1550','','','3','2','3','','12','7','','1 Covered','','62Lac','4000','4460','6200000','4000','4100','100','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','Yes','Yes','Yes','Yes','','','Yes','','Full','','North-East','60-Feet','Vitrified','Semifurnished','Yes','','Sector 89','Paras Rajan','9999989894','','ringing','','Gaurav/JMD Properties','9811119829','4000 to 4200rup/sqft','','','','','','As per the confirmation he is broker final price is 62Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tulip-petals-sector-89-gurgaon-1550-sq-ft-spid-P28666155?from_src=XIDPAGE_det&pos=XIDDETVIEW',277,'141792841388580'),
(31,'Owner','Sell','Apartment','Gurgaon','Town House','','DLF Phase II','','3000','2200','','3','3','1','Servant Room','2','1','','','3 Open','2.5Cr','8334','8882','18000000','6000','8750','2750','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','Yes','','Yes','','Yes','','','','','','','','','','','','','','Partial','Municipal Corporation','North-East','15-Feet','Vitrified','Semifurnished','','Yes','DLF Phase II','Pavnesh Bhandari','9873004078','','pavneshbhandari@redimail.com','','Vineet Kejriwal/Kejriwal Realtors Pvt ltd','9873983771','8500 to 9000rup/sqft','','','','','','as per the confirmation ready to post porperty on fairpockets.com ','','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-dlf-city-phase-2-gurgaon-3000-sq-ft-r1-spid-S26831505?pos=SEARCH',278,'584380161385478'),
(32,'Owner','Sell','Apartment','Gurgaon','Mapsko Casa Bella','','Sector-82','','','1960','','3','3','3','','25','2','','','1 Open','85Lac','4337','4292','8000000','4082','4100','18,36734694','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','Yes','','Sector 82','Madav','9999998384','','madhav.monga@yahoo.com','','Maninder Singh/Real infra solution','9810802935','4000 to 4200rup/sqft','','Vikas/I Realty Propmart','9810307978','4000rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com ','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-mapsko-casa-bella-sector-82-gurgaon-1960-sq-ft-r1-spid-X27405009?pos=SEARCH',279,'625992117729217'),
(33,'Broker','Sell','Apartment','Gurgaon','Tulip Orange','','Sector-70','','1137','','','3','2','2','Study Room','12','5','','1 Covered','','70Lac','6157','6417','7000000','6157','6333.333333','176,7810026','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','Yes','','','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','North-East','36-Feet','Vitrified','Semifurnished','Yes','Yes','Sector-70','Mayank Roof Realtech','9599481837','','','','Maninder Singh/Real infra solution','9810802935','6000 to 6500rup/sqft','','Vikas/I Realty Propmart','9810307978','6500rup/sqft','','As per the confirmation he is broker final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tulip-orange-sector-70-gurgaon-1137-sq-ft-spid-Q26694155?from_src=XIDPAGE_det&pos=XIDDETVIEW',280,'30232349739236'),
(34,'Owner','Sell','Independent House','Gurgaon','Ansal API Esencia','','Sector 67','','2750','','','3','2','3','','3','Ground','','','','1.2Cr','4264','6077','12000000','4364','5750','1386,363636','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','','Yes','','','Yes','','','Full','','West','','Marble','','','','Sector 67','Sashidhar','9971670030','','DRSSN6@YAHOO.CO.IN','','Maninder Singh/Real infra solution','9810802935','5500 to 6000rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 1.2Cr','','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-ansal-api-esencia-sector-67-gurgaon-2750-sq-ft-r2-spid-F23001419?from_src=XIDPAGE_det&pos=XIDDETVIEW',258,'217845689978766'),
(35,'Owner','Sell','Independent House','Gurgaon','Vatika Primrose Floors','','Sector-83','','2100','2000','1500','3','3','2','','2','Ground','','','1 Open','85','4048','5142','8500000','4048','5500','1452,380952','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','Yes','Yes','Yes','Yes','','','','','Yes','','','','','','','','Yes','','Full','Municipal Corporation','Main Road','','','','','','Sector 83','Khurana','9818815206','','Daljeet71@yahoo.com','','Maninder Singh/Real infra solution','9810802935','5000rup/sqft','','Vikas/I Realty Propmart','9810307978','','','As per the confirmation ready to post property on fairpockets.com','Ringing','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-sector-83-gurgaon-2100-sq-ft-r1-spid-T26934365?pos=SEARCH',281,'319081941072275'),
(36,'Owner','Sell','Apartment','Gurgaon','Adani Oyster Grande','','Sector 102','','1889','','','3','3','','','25','8','','','','1.1Cr','5800','5227','11000000','5823','6083.333333','260,146462','6000','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Sector 102','Himanshu Yadav','9716811869','8800584222','Hyadav55@gmail.com','','Danish/M/S JKS Associates','9811067684','6000 to 6500rup/sqft','','Sumit/JMD India','8860056007','5750rup/sqft','','As per the confirmation ready to post property on alive asset final price is 6000rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-adani-oyster-grande-sector-102-gurgaon-1889-sq-ft-r1-spid-Y21118276?from_src=XIDPAGE_det&pos=XIDDETVIEW',282,'222852504626892'),
(37,'Owner','Sell','Apartment','Gurgaon','Adani Oyster Grande','','Sector 102','','1889','','','3','3','','','25','3','','','','1.1Cr','5824','5227','11000000','5823','6083.333333','260,146462','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Sector 102','Kaushik Dey','9560777228','','','','Danish/M/S JKS Associates','9811067684','6000 to 6500rup/sqft','','Sumit/JMD India','8860056007','5750rup/sqft','','ringing','Ringing','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-adani-oyster-grande-sector-102-gurgaon-1889-sq-ft-r6-spid-V11884109?from_src=XIDPAGE_det&pos=XIDDETVIEW',282,'133268698282170'),
(38,'Broker','Sell','Apartment','Gurgaon','Adani Oyster Grande','','Sector 102','','1689','','','3','3','3','','25','6','','1 Covered','','92.9Lac','5500','5227','9200000','5447','6083.333333','636,3232682','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','Yes','Yes','','','','Yes','','','','','','Yes','Yes','Yes','Yes','','','','Full','','','','','','Yes','','Sector 102','Sumit/JMD India','8860056007','7988138733','','','Danish/M/S JKS Associates','9811067684','6000 to 6500rup/sqft','','Sumit/JMD India','8860056007','5750rup/sqft','','As per the confirmation he is broker final price is 92Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-adani-oyster-grande-sector-102-gurgaon-1689-sq-ft-spid-D28175722?from_src=XIDPAGE_det&pos=XIDDETVIEW',282,'745155349395372'),
(39,'Owner','Sell','Apartment','Gurgaon','Emaar MGF Gurgaon Greens','','Sector 102','','1650','','','3','4','','Servant Room','15','7','','1 Covered','','75Lac','4546','4802','7300000','4424','4600','175,7575758','4800','','','Under construction','Aug-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','Park/Garden','','','','Yes','','Sector 102','Sumit Sengal','9416015405','','sumit.sehgal@rediffmail.com','','Sunil/Sai Angan Real Estate','9811951929','4500 to 4800rup/sqft','','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','4500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 4800rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-gurgaon-greens-sector-102-gurgaon-1650-sq-ft-r3-spid-Q25454287?pos=SEARCH',283,'255713260380398'),
(40,'Owner','Sell','Apartment','Gurgaon','Emaar MGF Gurgaon Greens','','Sector 102','','1650','','','3','3','','','15','7','','','','72.6Lac','4400','4802','7200000','4364','4600','236,3636364','','','','Immediate','','','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sector 102','Kunal Aggarwal','9810747466','','Kaushal_53@yahoo.com','','Sunil/Sai Angan Real Estate','9811951929','4500 to 4800rup/sqft','','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','4500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 72Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-gurgaon-greens-sector-102-gurgaon-1650-sq-ft-r6-spid-O23077297?from_src=XIDPAGE_det&pos=XIDDETVIEW',283,'571916656488133'),
(41,'Broker','Sell','Apartment','Gurgaon','Emaar MGF Gurgaon Greens','','Sector 102','','1650','','','3','4','3','Servant Room','15','8','','','','70Lac','4243','4802','7000000','4242','4600','357,5757576','','','','Under construction','Aug-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','','Vitrified','Semifurnished','Yes','Yes','Sector 102','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','9910005952','','','Sunil/Sai Angan Real Estate','9811951929','4500 to 4800rup/sqft','','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','4500rup/sqft','','As per the confirmation he is broker final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-gurgaon-greens-sector-102-gurgaon-1650-sq-ft-spid-X28287035?from_src=XIDPAGE_det&pos=XIDDETVIEW',283,'470561654376177'),
(42,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','','2036','','3','4','','','25','11','','','','88Lac','4323','4420','8800000','4323','4830','507','4323','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','','','Sector 89','Dakshit Bajaj','9891281969','','Daksh.bajaj@gmail.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com ','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-2036-sq-ft-r5-spid-A21576645?pos=SEARCH',284,'608656397587250'),
(43,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','2070','','','3','4','3','','25','17','','','','95Lac','4590','4420','9108000','4400','4830','430','4400','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','East','','Vitrified','Unfurnished','Yes','Yes','Sector 89','Sanjeev','9212101560','','Sanarora69@yahoo.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 4400rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-2070-sq-ft-spid-B28609167?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'767835770524074'),
(44,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','1660','','','2','3','3','Study & Servant Room','25','6','','','','70Lac','4217','4420','7000000','4217','4830','613,1325301','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','','','','','','','','Yes','','Yes','','','','','Full','','','','','','','','Sector 89','Rajeev','9899992736','','Rajeev.Doval@timegroup.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','Aa per the confirmation ready to post property final price is 70Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1660-sq-ft-r1-spid-E17191420?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'240484394437500'),
(45,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','','1910','','3','3','3','','25','3','','','','80.2Lac','4199','4420','7640000','4000','4830','830','4000','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sector 89','Sundaram','9818678679','','Sundarams@hotmail.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 4000rup/sqft.','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1910-sq-ft-r5-spid-B22521271?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'634502411899401'),
(46,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','','1957','','3','4','3','Servant Room','25','11','','','','85Lac','4344','4420','8219000','4200','4830','630','4200','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','','','','','Yes','','Yes','','','','','Full','','Park/Garden','','','Unfurnished','Yes','','Sector 89','Harish Goel','9873080301','9582842488','ringng','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','Hi Is investeras per the confirmation ready to post property on alive asset final price is 4200rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1957-sq-ft-spid-J28457565?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'633023801566024'),
(47,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','','1910','','2','2','2','','25','8','','','','85Lac','4451','4420','8404000','4400','4830','430','4400','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','','','','','Yes','','Yes','','','','','Full','','','','','','Yes','','Sector 89','Vikas','9810779966','','Vikas.Garg89@yahoo.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per th confirmation ready to post property on alive asset final price is 4400rup/sqft','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1910-sq-ft-r5-spid-N18769013?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'297776283149427'),
(48,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','','2236','','3','3','','','25','6','','','','90Lac','4026','4420','9167000','4100','4830','730','4100','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sector 89','Anupam Singh','9999249441','','Anupam@321@gmail.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per the confirmation ready to post property on alive asset said we already pay 28Lac rup final price is 4100rup/sqft','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-2236-sq-ft-r3-spid-M22198391?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'507133287523074'),
(49,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','1660','','','2','3','2','Study Room','25','7','','','','63.08Lac','3800','4420','6300000','3795','4830','1034,819277','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','East','','Vitrified','Unfurnished','Yes','','Sector 89','Ankush Vashisht','8510934351','','off','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 63Lac','no contact','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1660-sq-ft-r1-spid-M26622436?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'668765659220580'),
(50,'Broker','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','1297','','','2','2','2','Pooja Room','25','11','','1 Covered','','46.69Lac','3600','4420','4770000','3678','4830','1152,28219','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','East','60-Feet','Wood','Unfurnished','Yes','','Sector 89','Kamal Nayan/Aadhar Homes','9711139812','7042000546','ringng','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per the confirmation he is broker final price is 46Lac + extra charges','no contact','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1297-sq-ft-spid-E28895099?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'818722738360748'),
(51,'Broker','Sell','Apartment','Gurgaon','3C Greensopolis','','Sector-89','','1957','','','3','4','3','Servant Room','25','12','','1 Covered','','78.28Lac','4000','4420','7828000','4000','4830','830','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','','','','','Yes','','Sector 89','Akshay Tanwar/Astitva Realtor','7982453536','9999963532','Aksh.tanwar07@gmail.com','','Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft','','Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft','','As per the confirmation he is broker final price is 4000rup/sqft','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1957-sq-ft-r2-spid-U26763721?from_src=XIDPAGE_det&pos=XIDDETVIEW',284,'29846210621586'),
(52,'Owner','Sell','Apartment','Gurgaon','Moonsoon Breeze Phase 1','','Sector 78','','1657','','','3','2','3','Pooja Room','13','4','','','','80Lac','4828','4845','7300000','4406','4600','194,4477972','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','','Yes','','Yes','','Full','Borewell/Tank','East','','','','Yes','','Sector 78 ','Puneet','9958823193','','Puneet_kotnala@yahoo.com','','Mohit Yadav / Resale Experts','9728209209','4500rup/sqft','','Navin/Winworld Realty Services','9650344337','4500 to 4800rup/sqft','','As per the confirmation ready to post property on alive asset final price is 73Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-monsoon-breeze-sector-78-gurgaon-1657-sq-ft-r2-spid-G26733439?from_src=XIDPAGE_det&pos=XIDDETVIEW',285,'744435272904098'),
(53,'Owner','Sell','Apartment','Gurgaon','Unitech South City 2','','Sector 50','','','1139','','1','1','2','','2','2','','','1 Open','65Lac','5706','6715','6450000','5663','6400','737,1378402','','','10 + years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','','Yes','','Yes','','Partial','Municipal Corporation','West','','Marble','Furnished','Yes','','Sector 50','Anirudh Rathore','9168178663','','rathore.anirudh@gmail.com','','Devender Rajora','9891414040','6400rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 65Lac','','http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-unitech-south-city-2-sector-50-gurgaon-1139-sq-ft-spid-C28832157?from_src=XIDPAGE_det&pos=XIDDETVIEW',286,'493500401611438'),
(54,'Owner','Sell','Apartment','Gurgaon','Indiabulls Centrum Park','','Sectar-103','','1481','','','2','3','2','Study Room','19','1','','1 Covered','','67Lac','4523','4377','6368300','4300','4350','50','4300','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','North-East','','Marble','Unfurnished','Yes','Yes','Sector 103','Saurabh Bhargava','9810808088','','Saurabh.bhargav.82@gmal.com','','Mukesh Kumar','9716008599','4200 to 4500rup/sqft','','Dhruv Kohli/Grow More Estate','9899238283/9811290924','4200rup/sqft','','As per the confirmation ready to post property on alive asset final price is 4300rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-indiabulls-centrum-park-sector-103-gurgaon-1481-sq-ft-r3-spid-B25158838?from_src=XIDPAGE_det&pos=XIDDETVIEW',287,'194856486839884'),
(55,'Owner','Sell','Apartment','Gurgaon','The Legend','','Secter 57 ','','3200','','','4','4','3','','12','Ground','','','','2.3Cr','7188','7310','22000000','6875','7266.666667','391,6666667','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','North','','','','Yes','','Sector 57','Raminder','9818787445','9810046201','Ovation1980@gmail.com','','Karan/Rayland Realty','9899971363','7000rup/sqft','','Rajat Arora/Surendra properties','9810499330','7300 to 7500rup/sqft','','As per the confirmation ready to post property on alive asset final price is 2.20Cr','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-the-legend-sector-57-gurgaon-3200-sq-ft-r2-spid-W23647522?from_src=XIDPAGE_det&pos=XIDDETVIEW',288,'565366565697455'),
(56,'Broker','Sell','Apartment','Gurgaon','The Legend','','Secter 57 ','','5500','','','5','5','3','','12','12','','2 Covered','','3.5Cr','6364','7310','35000000','6364','7266.666667','903,030303','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','','','Yes','','','Full','Municipal Corporation','East','','','Semifurnished','Yes','Yes','Sector 57','Rajat Arora/Surendra properties','9810499330','','','','Karan/Rayland Realty','9899971363','7000rup/sqft','','Rajat Arora/Surendra properties','9810499330','7300 to 7500rup/sqft','','As per the confirmation he is broker final price is 3.5Cr','','http://www.99acres.com/5-bhk-bedroom-apartment-flat-for-sale-in-the-legend-sector-57-gurgaon-5500-sq-ft-spid-F28747369?from_src=XIDPAGE_det&pos=XIDDETVIEW',288,'12845414220948'),
(57,'Owner','Sell','Apartment','Gurgaon','Piedmont Taksila Heights','','Secter 37C','','1848','','','4','4','3','','15','4','','','1 Open','75Lac','4059','4505','7500000','4058','4233.333333','174,8917749','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','','','','','Yes','','','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','','North-East','30 Feet','','','Yes','','Secter 37C','Deol','8587934185','','Udaideolal@gmail.com','','Surender Sharma/All india Properties','9350068161','4000 to 4500rup/sqft','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','4200rup/sqft','','As per the confirmation ready to post property on alive asset final price is 75Lac','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-piedmont-taksila-heights-sector-37-c-gurgaon-1848-sq-ft-r1-spid-O27933180?from_src=XIDPAGE_det&pos=XIDDETVIEW',289,'607104415292855'),
(58,'Owner','Sell','Apartment','Gurgaon','Spire Woods','','Secter 103','','1851','','','3','3','3','','13','8','','1 Covered','','70Lac','3782','3910','7000000','3782','4000','218,2603998','','','','Ready to Move','Immediate','Resale','Freehold','','Yes','Yes','Yes','','','Yes','Yes','Yes','','','','','','Yes','Yes','','','','','Full','Municipal Corporation','North-East','','','Semifurnished','Yes','','Sector 103','Anil','9990603128','','anilatri2810@gmail.com','','Mukesh Kumar','9716008599','4000rup/sqft','','','','','','As per the confirmation ready to post property final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spire-woods-sector-103-gurgaon-1851-sq-ft-r1-spid-X26062429?from_src=XIDPAGE_det&pos=XIDDETVIEW',290,'709015683704971'),
(59,'Owner','Sell','Apartment','Gurgaon','Godrej 101','','Secter 79 ','','1262','','','2','2','3','','15','Ground','','','1 Open','45Lac','3566','5482','4500000','3566','5250','1684,231379','','','','Under construction','Aug-19','Resale','Freehold','','','Yes','','','','','','Yes','','','','','Yes','Yes','Yes','','','Yes','','Full','Municipal Corporation','North-East','45-Feet','Vitrified','Furnished','Yes','','Sector 79 ','Atul','8527695502','','atultaxak@yahoo.com','','Maninder Singh/Real infra solution','9810802935','5000 to 5500rup/sqft','','Vikas/I Realty Propmart','9810307978','','','as per the confirmation ready to post porperty on fairpockets.com.','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-godrej-101-sector-79-gurgaon-1262-sq-ft-spid-X29058843?from_src=XIDPAGE_det&pos=XIDDETVIEW',291,'530886081343364'),
(60,'Owner','Sell','Apartment','Gurgaon','Supertech Hues','','Secter 68 ','','1180','1000','','2','2','3','','29','12','','','','62Lac','5255','6417','6200000','5254','6266.666667','1012,429379','','','','Under construction','Jul-18','Resale','Freehold','','Yes','Yes','Yes','','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','East','','Marble','Furnished','Yes','','Sector 68','Ankit Kaushik','9899804990','','ankitkaushikca@yahoo.com','','Vivik','9711178607','6000 to 6300rup/sqft','','Madhav Mishra/Winworld realty','9650544334','6500','','as per the confirmation ready to post porperty on fairpockets.com.','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-supertech-hues-sector-68-gurgaon-1180-sq-ft-r2-spid-Z27487263?pos=SEARCH',292,'499027288635921'),
(61,'Owner','Sell','Apartment','Gurgaon','Godrej Summit','','Secter 104','','1447','','','2','2','3','','19','4','','','','76.03Lac','5254','5227','7600000','5252','5550','297,7539737','5100','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','','','Yes','','','','Yes','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','Marble','Unfurnished','Yes','','Sector-104','Vineet Arora','9810644424','','Vineetarora_r@yahoo.in','','Manmohan Gupta/Brick ty Brick','8048094664','5000rup/sqft','','Vijay Khurana/Finmart Realty','9990508289','5550','','As per the confirmation ready to post property final price is 5100rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-godrej-summit-sector-104-gurgaon-1447-sq-ft-r3-spid-P26141335?from_src=XIDPAGE_det&pos=XIDDETVIEW',293,'890075429299806'),
(62,'Owner','Sell','Apartment','Gurgaon','Silverglades the Melia','','Sohna Road','','1350','','','2','2','2','Study Room','15','14','','1 Covered','','67Lac','4888','4802','6200000','4593','4800','207,4074074','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','Semifurnished','Yes','','Sohna','Dapinder Singh','9818365376','','Dapisingh@gmail.com','','Saurabh Dadhich/NGLC Realtech','9818225352/01244384354','4800rup/sqft','','','','','','As per the confirmation ready to post property final price is 62Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-silverglades-the-melia-sohna-gurgaon-1350-sq-ft-r1-spid-H19157905?from_src=XIDPAGE_det&pos=XIDDETVIEW',294,'475132054177731'),
(63,'Owner','Sell','Apartment','Gurgaon','Tashee Capital Gateway','','Sectar-111','','','1990','','3','3','3','','18','3','','1 Covered','','90Lac','4562','4462','8600000','4322','4433.333333','111,7252931','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','Unfurnished','Yes','','Sector 111','Radhika Haldia','9167980516','','Radseh@gmail.com','','Vijay Khurana/FinMart realty','9990508289','4300 to 4500rup/sqft','','J.S.K Homes','9810955811','4500rup/sqft','','As per the confirmation ready to post property final price is 86Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tashee-capital-gateway-sector-111-gurgaon-1990-sq-ft-r1-spid-L28115068?from_src=XIDPAGE_det&pos=XIDDETVIEW',295,'926982598680560'),
(64,'Owner','Sell','Apartment','Gurgaon','Tashee Capital Gateway','','Sectar-111','','1422','','','2','2','','','18','4','','1 Covered','1 Open','55.5Lac','4286','4462','5500000','3868','4433.333333','565,5414909','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','','','Yes','','Full','','','','','','Yes','Yes','Sector 111','Machael Khanna','9810156505','','KhannaMachael@gmail.com','','Vijay Khurana/FinMart realty','9990508289','4300 to 4500rup/sqft','','J.S.K Homes','9810955811','4500rup/sqft','','As per the confirmation ready to post property final price is 55Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-tashee-capital-gateway-sector-111-gurgaon-1295-sq-ft-r4-spid-I15343653?from_src=XIDPAGE_det&pos=XIDDETVIEW',295,'655204421578025'),
(65,'Owner','Sell','Apartment','Gurgaon','Era Cosmocity','','Sectar-103','','1368','','','2','2','3','','28','12','','1 Covered','','45Lac','3290','3910','4500000','3289','3520','230,5263158','2700','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','Yes','Yes','Yes','Yes','Yes','','Yes','','','','','','','','Yes','','Full','','East','196.9-Feet','Vitrified','Semifurnished','Yes','Yes','Sector 103','Gaurav','9818010727','','Gaurav.pahuja1983@gmail.com','','Mukesh Kumar','9716008599','3200 to 3500rup/sqft','','Dhruv Kohli/Grow More Estate','9899238283/9811290924','3500rup/sqft','','As per the confirmation ready to post property final price is 2700rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-era-cosmocity-sector-103-gurgaon-1368-sq-ft-r2-spid-S16710101?pos=SEARCH',296,'457978140562591'),
(66,'Owner','Sell','Apartment','Gurgaon','Era Cosmocity','','Sectar-103','','1828','','','3','3','3','','28','6','','','','64Lac','3502','3910','6400000','3501','3520','18,9059081','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','','Yes','Yes','Yes','Yes','Yes','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','Vitrified','','Yes','','Sector 103','Harmeet','7259030410','','babarai13@yahoo.co.in','','Mukesh Kumar','9716008599','3200 to 3500rup/sqft','','Dhruv Kohli/Grow More Estate','9899238283/9811290924','3500rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com ','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-era-cosmocity-sector-103-gurgaon-1828-sq-ft-r6-spid-C21239046?from_src=XIDPAGE_det&pos=XIDDETVIEW',296,'298081152207020'),
(67,'Broker','Sell','Apartment','Gurgaon','Satya the Hermitage','','Sectar-103','','2605','2500','','4','4','','Servant Room','14','6','','','','95Lac','3647','4165','9500000','3647','3933.333333','286,5003199','','','1 to 5 Years','Immediate','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','','Yes','Yes','','Yes','','Full','','','','','Unfurnished','Yes','','Sector 103','Mr ANKUSH/ Fair Investment Solutions','9810723221','','','','Manmohan Gupta/Brick ty Brick','8048094664','4000rup/sqft','','Vijay Khurana/Finmart Realty','9990508289','3800 to 4000rup/sqft','','As per the confirmation He is broker ready to post property','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-satya-the-hermitage-sector-103-gurgaon-2605-sq-ft-r4-spid-X21519213?from_src=XIDPAGE_det&pos=XIDDETVIEW',297,'96778381070277'),
(68,'Owner','Sell','Apartment','Gurgaon','Unitech Sunbeeze','','Sectar-69','','1807','','','4','4','2','Servant Room','14','10','','1 Covered','','72.5Lac','4012','4165','10000000','5534','5600','65,96568899','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','','Yes','Yes','Yes','','','','','Yes','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','','','','Yes','','Sector 69','Amit Bhambri','9811560165','','amit.bhambri7@gmail.com','','Navin/Winworld realty','9650344337','4000rup/sqft Basic price','','','','','','as per the confirmation ready to post porperty on fairpockets.com.','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-unitech-sunbreeze-sector-69-gurgaon-1807-sq-ft-r2-spid-F26835425?from_src=XIDPAGE_det&pos=XIDDETVIEW',298,'941407895842381'),
(69,'Owner','Sell','Apartment','Gurgaon','Earth Elacasa','','Sectar-107','','2285','','','3','4','3','Servant Room & Study Room','25','12','','1 Covered','','72.5Lac','4049','4240','7250000','3173','4266.666667','1093,800146','3900','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','Yes','Yes','','','','','Yes','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 107','Munish','9650028466','','said call after some time','','Saurabh Dadhich/NGLC Realtech','9818225352','4000 to 4300rup/sqft','','Nitin/Priyanka Housing','8030408435','4500rup/sqft','','As per the confirmation ready to post property final price is 3900rup/sqft its Basic price','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-earth-elacasa-sector-107-gurgaon-2285-sq-ft-spid-N28363477?from_src=XIDPAGE_det&pos=XIDDETVIEW',299,'301839226523039'),
(70,'Owner','Sell','Apartment','Gurgaon','Eldeco Accolade','','Sohna Road','','','1290','','2','2','','','19','4','','','','55.47Lac','4300','3952','5547000','4300','4300','0','4100','','','Under construction','Jun-18','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Sohna Road','Surjeet','9810229526','','Surjeet.s28@gmail.com','','Aman/Aryan Realty Infra','8447755103','4000 to 4500rup/sqft','','Ankur Aggarwal/Aggarwal realter','9991000585','4200 to 4500rup/sqft','','As per the confirmation readt to post property price is 4100rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-eldeco-accolade-sohna-gurgaon-1290-sq-ft-r3-spid-N20762323?pos=SEARCH',300,'35757136266018'),
(71,'Owner','Sell','Apartment','Gurgaon','Eldeco Accolade','','Sohna Road','','1269','','','2','2','3','','19','8','','','','54Lac','4256','3952','5400000','4255','4300','44,68085106','4500','','','Under construction','Jan-18','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sohna Road','Ashok Bansal','9355157288','','Mhd9355157288@yahoo.com','','Aman/Aryan Realty Infra','8447755103','4000 to 4500rup/sqft','','Ankur Aggarwal/Aggarwal realter','9991000585','4200 to 4500rup/sqft','','As per the confirmation ready to post property final price is 4500rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-eldeco-accolade-sohna-gurgaon-1269-sq-ft-r1-spid-M23405894?pos=SEARCH',300,'681723989493660'),
(72,'Broker','Sell','Apartment','Gurgaon','Pareena Mi Casa','','Sectar-68','','1245','','','2','2','3','','36','6','','1 Covered','','65Lac','5221','5482','6500000','5221','5800','579,1164659','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','Yes','','Yes','Yes','Yes','','Yes','','Full','','','','','','Yes','','Sector 68','Varsha Aggarwal','8010942646','','','','Varsha Aggarwal','8010942646','5000 to 5500rup/sqft','','','','','','As per the confirmation He is broker ready to post property.','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-mi-casa-sector-68-gurgaon-1245-sq-ft-spid-K28621671?from_src=XIDPAGE_det&pos=XIDDETVIEW',301,'441652695567393'),
(73,'Owner','Sell','Apartment','Gurgaon','Pareena Mi Casa','','Sectar-68','','1245','','','2','2','3','','36','28','','1 Covered','','68Lac','5462','5482','6600000','5301','5800','498,7951807','','','','Under construction','Feb-20','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','Yes','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','Main Road','','','','Yes','','Sector 68','Sam','7838555499','','ringing','','Varsha Aggarwal','8010942646','5000 to 5500rup/sqft','','','','','','As per the confirmation ready to post property final price is 66Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-mi-casa-sector-68-gurgaon-1245-sq-ft-r2-spid-B25242150?from_src=XIDPAGE_det&pos=XIDDETVIEW',301,'136151402706688'),
(74,'Owner','Sell','Apartment','Gurgaon','Pareena Mi Casa','','Sectar-68','','','1250','','2','2','','','36','3','','','','68.75Lac','5500','5482','6875000','5500','5800','300','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','Yes','','Yes','Yes','Yes','','Yes','','Full','','','','','','Yes','','Sector 68','Kumar Abhishek','9986417778','','ringing','','Varsha Aggarwal','8010942646','5000 to 5500rup/sqft','','','','','','said all after some time','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-mi-casa-sector-68-gurgaon-1250-sq-ft-r4-spid-X19731689?from_src=XIDPAGE_det&pos=XIDDETVIEW',301,'820982099310658'),
(75,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas','','Sectar-70','','1560','','','3','3','3','','15','Ground','','1 Covered','','75Lac','4808','4802','7000000','4487','4833.333333','346,1538462','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','Yes','','Yes','Yes','Yes','','Yes','','Full','','North-east','150-Feet','Wood','Furnished','Yes','Yes','Sector 70','Rajesh Gupta','9810385892','','Grajeshg20000@hotmail.com','','Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price','','Varsha Aggarwal/Connections ','8010942646','5000rup/sqft','','As per the confirmation ready to post property Final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-r3-spid-D22735909?from_src=XIDPAGE_det&pos=XIDDETVIEW',302,'715071195063730'),
(76,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas','','Sectar-70','','1560','','','3','3','3','','15','12','','','','65Lac','4166','4802','6500000','4167','4833.333333','666,6666667','4000','','','Under construction','Dec-17','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sector 70','Ashish','9711204574','','Soodashish1@gmail.com','','Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price','','Varsha Aggarwal/Connections ','8010942646','5000rup/sqft','','As per the confirmation ready to post property Final price is 4000rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-r2-spid-K27033103?from_src=XIDPAGE_det&pos=XIDDETVIEW',302,'1.093515021345409e15'),
(77,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas','','Sectar-70','','1560','','','3','4','3','Servant Room','15','7','','1 Covered','','78Lac','5000','4802','7500000','4808','4833.333333','25,64102564','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','','','','','','Yes','Yes','Yes','','Yes','','Full','Borewell/Tank','East','','','','Yes','Yes','Sector 70','Ajay','9972163600','','Giriajay_giri@redif.com','','Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price','','Varsha Aggarwal/Connections ','8010942646','5000rup/sqft','','As per the confirmation ready to post property Final price is 75Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-spid-F29046745?from_src=XIDPAGE_det&pos=XIDDETVIEW',302,'230746837581970'),
(78,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas','','Sectar-70','','1560','','','3','3','3','Servant Room','15','6','','','','79Lac','5065','4802','7300000','4679','4833.333333','153,8461538','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','Yes','','Full','','East','','','','Yes','','Sector 70','Monica','9818492066','','Spacesmonica@gmail.com','','Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price','','Varsha Aggarwal/Connections ','8010942646','5000rup/sqft','','As per the confirmation ready to post property final price is 73Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-r2-spid-W24541533?from_src=XIDPAGE_det&pos=XIDDETVIEW',302,'948535632010837'),
(79,'Broker','Sell','Apartment','Gurgaon','Unitech Vistas','','Sectar-70','','1535','','','3','4','3','Servant Room','15','4','','1 Covered','','62Lac','4040','4802','6200000','4039','4833.333333','794,2453855','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','Yes','','Full','','Park/Garden','','','','Yes','','Sector 70','Varsha Aggarwal/Connections ','8010942646','','','','Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price','','Varsha Aggarwal/Connections ','8010942646','5000rup/sqft','','As per the confirmation Hi is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1535-sq-ft-spid-K28621307?from_src=XIDPAGE_det&pos=XIDDETVIEW',302,'906225738853292'),
(80,'Broker','Sell','Apartment','Gurgaon','Unitech Vistas','','Sectar-70','','1560','','','3','4','3','Servant Room','15','7','','1 Covered','','56.1Lac','3597','4802','5610000','3596','4833.333333','1237,179487','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','','Yes','Yes','','','','Full','','Park/Garden','','','','Yes','','Sohna Road','Dhrub/Premiux Capital','8375029981','','','','Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price','','Varsha Aggarwal/Connections ','8010942646','5000rup/sqft','','As per the confirmation Hi is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-spid-D28947423?from_src=XIDPAGE_det&pos=XIDDETVIEW',302,'56224893420693'),
(81,'Owner','Sell','Apartment','Gurgaon','CHD Resortico','','Sohna Road','','709','','','1','1','1','','15','7','','1 Covered','','29Lac','4091','3952','2700000','3808','4100','291,819464','','','','Under construction','Mar-20','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','Yes','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','','','','Unfurnished','Yes','','Sohna Road','Kaushal Gupta','9999975277','','kaushalguptahere@gmail.com','','Anand Garg/Konexionz Real Estate','9811364000/9811944603','4000 to 4200rup/sqft','','','','','','As per the confirmation ready to post property final price is 27Lac','','http://www.99acres.com/1-bhk-bedroom-serviced-apartment-flat-for-sale-in-chd-resortico-sohna-road-gurgaon-709-sq-ft-r3-spid-J25485131?from_src=XIDPAGE_det&pos=XIDDETVIEW',303,'764853772539503'),
(82,'Owner','Sell','Apartment','Gurgaon','Raheja Maheshwara','','Sohna Road','','1198','','','2','2','2','','13','11','','1 Covered','','35.64Lac','2975','2890','3500000','2922','3250','328,4641068','','','','Under construction','Dec-20','Resale','Freehold','','Yes','Yes','','Yes','','','','Yes','','','','','','','Yes','Yes','','','','Full','','','','','','Yes','','Sohna Road','Tarun Sharma','9582636815','','off','','Ankur Aggarwal/Aggarwal realter','9991000585','3000 to 3500rup/sqft','','','','','','As per confirmation ready to post property final price is 35Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-raheja-maheshwara-sohna-gurgaon-1198-sq-ft-r1-spid-X27758191?pos=SEARCH',304,'390528656490006'),
(83,'Owner','Sell','Apartment','Gurgaon','Eros Wembley Estate','','Sohna Road','','1376','','','3','3','2','','16','9','','1 Covered','','1.2Cr','8721','8755','11700000','8503','8510','7,093023256','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','Yes','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','south','','Ceramic','Furnished','Yes','','Sohna Road','Amit Kumar Dubey','9845090110','','off','','Ajay Varma/MD Real Estate','8813944769','8000 to 9000rup/sqft','','','','','','as per the confirmation ready to post porperty on fairpockets.com  not share mail.id','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-eros-wembley-estate-rosewood-gurgaon-1376-sq-ft-r1-spid-S26387827?from_src=XIDPAGE_det&pos=XIDDETVIEW',305,'1.2994409662247e15'),
(84,'Broker','Sell','Apartment','Gurgaon','ILD Greens','','Sector-37C','','','1790','','3','3','3','Pooja Room','21','8','','1 Covered','','56.5lac','3157','4037','5650000','3156','3833.333333','676,9087523','','','','Under construction','By 2018','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','','Yes','','Full','','North-east','','Marble','Unfurnished','Yes','','Secter 37C','Saurabh Goswami/Reliable Realtors','9873400634','','','','Surender Sharma/All india Properties','9350068161','3500 to 4000rup/sqft','','Saurabh Goswami/Reliable Realtors','9873400634','4000rup/sqft','','As per the confirmation He is broker ready to post property on Fairpockets.com','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ild-greens-sector-37-c-gurgaon-1790-sq-ft-spid-G28998875?from_src=XIDPAGE_det&pos=XIDDETVIEW',306,'292203708961456'),
(85,'Broker','Sell','Apartment','Gurgaon','ILD Greens','','Sector-37C','','2311','2211','','4','4','3','Pooja room & Servant Room','21','9','','3 Covered','1 Open','71lac','3073','4037','7100000','3072','3833.333333','761,0702438','','','','Immediate','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','','Yes','','Full','Borewell/Tank','North-east','80-Feet','Vitrified','','Yes','Yes','Secter 37C','Surender Sharma/All india Properties','9350068161','','','','Surender Sharma/All india Properties','9350068161','3500 to 4000rup/sqft','','Saurabh Goswami/Reliable Realtors','9873400634','4000rup/sqft','','As per the confirmation He is broker ready to post property on Fairpockets.com','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-ild-greens-sector-37-c-gurgaon-2311-sq-ft-r2-spid-Q15906483?from_src=XIDPAGE_det&pos=XIDDETVIEW',306,'915241845899743'),
(86,'Owner','Sell','Apartment','Gurgaon','Dhoot time Residency','','Sector-63','','1708','','','3','3','3','Servant Room','20','16','','1 Covered','','1.21Cr','7100','7267','12100000','7084','7100','15,69086651','7000','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','West','','Marble','Semifurnished','Yes','','Secter 63','Keshav','8800253408','','arorakeshav95@gmail.com','','Mr Raj PMC Global','8447456661','7000rup/sqft','','Rohit/Fundus Praedium','9810520002','7000 to 7200rup/sqft','','As per the confirmation ready to post property final price is 7000rup/sqft BSP','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dhoot-time-residency-sector-63-gurgaon-1708-sq-ft-spid-P28956837?from_src=XIDPAGE_det&pos=XIDDETVIEW',307,'263133439018990'),
(87,'Owner','Sell','Apartment','Gurgaon','Dhoot time Residency','','Sector-63','','1708','','','3','3','3','Study Room','20','6','','','','1.2LCr','7026','7267','12100000','6800','7100','300','6800','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','West','','Marble','Unfurnished','Yes','','Secter 63','Manoj','9810020483','','Manny111968@gmail.com','','Mr Raj PMC Global','8447456661','7000rup/sqft','','Rohit/Fundus Praedium','9810520002','7000 to 7200rup/sqft','','As per the confirmation ready to post property final price is 6800rup/sqft BSP','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dhoot-time-residency-sector-63-gurgaon-1708-sq-ft-r3-spid-C24001461?from_src=XIDPAGE_det&pos=XIDDETVIEW',307,'275521359024548'),
(88,'Broker','Sell','Apartment','Gurgaon','Dhoot time Residency','','Sector-63','','1642','','','3','3','3','Servant Room','20','13','','','','98.52Lac','6000','7267','9852000','6000','7100','1100','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','Yes','','','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','West','','Marble','Unfurnished','Yes','','Secter 63','Mr Raj PMC Global','8447456661','','','','Mr Raj PMC Global','8447456661','7000rup/sqft','','Rohit/Fundus Praedium','9810520002','7000 to 7200rup/sqft','','As per the confirmation he is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dhoot-time-residency-sector-63-gurgaon-1642-sq-ft-r2-spid-F19275443?from_src=XIDPAGE_det&pos=XIDDETVIEW',307,'585682946574940'),
(89,'Owner','Sell','Apartment','Gurgaon','Ramprastha City Skyz','','Sector-37D','','','1750','','3','3','3','','20','4','','1 Covered','','71Lac','4058','4207','7000000','4000','4066.666667','66,66666667','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Secter 37D','AK Datta','9818682267','','Dr_AkDatta@yahoo.co.in','','Surender Sharma/All india Properties','9350068161','4000 to 4200rup/sqft','','Saurabh Goswami/Reliable Realtors','9873400634','4000rup/sqft','','As per the confirmation ready to post property final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ramprastha-city-skyz-sector-37-d-gurgaon-1750-sq-ft-r5-spid-H23877926?pos=SEARCH',308,'320897961016776'),
(90,'Owner','Sell','Apartment','Gurgaon','Spaze Privy the Address','','Sector-93','','1998','','','3','4','2','Servant Room & Study Room','20','10','','2 Covered','','75.92Lac','3800','4037','7592000','3800','3825','25,2002002','','','','Under construction','Mar-17','Resale','Freehold','','Yes','Yes','Yes','','Yes','Yes','Yes','','','','','','','Yes','Yes','','Yes','','','Full','Municipal Corporation','Park/Garden','','','','Yes','Yes','Sector 72','Kingshuk Basu','','','','','Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic','','Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft','','No contect','No contect','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1998-sq-ft-r2-spid-N26031049?pos=SEARCH',309,'1.64093589456424e15'),
(91,'Owner','Sell','Apartment','Gurgaon','Spaze Privy the Address','','Sector-93','','1998','','','3','4','2','Servant Room & Study Room','15','Ground','','2 Covered','','74Lac','3704','4037','7400000','3704','3825','121,2962963','','','','Immediate','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','Yes','Sector 72','S Taneja','9810375239','','Tanejask@yahoo.com','','Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic','','Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft','','As per the confirmation ready to post property final price is 74Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1998-sq-ft-r5-spid-U24215273?pos=SEARCH',309,'1.804802374744365e15'),
(92,'Owner','Sell','Apartment','Gurgaon','Spaze Privy the Address','','Sector-93','','1697','','','3','3','3','','15','4','','','','62Lac','3653','4037','6200000','3654','3825','171,4938126','3600','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','Marble','Semifurnished','Yes','','Sector 72','Deepakurv','9871595075','','Deepakvarshney23@gamil.com','','Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic','','Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft','','As per the confirmation ready to post property final price is 3600rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1697-sq-ft-spid-D28428147?pos=SEARCH',309,'379489397183922'),
(93,'Broker','Sell','Apartment','Gurgaon','Spaze Privy the Address','','Sector-93','','1697','','','3','4','2','','15','7','','1 Covered','','52.61Lac','3100','4037','5261000','3100','3825','724,8232174','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','North-east','60-Feet','Vitrified','Unfurnished','Yes','','Sector 72','Amrish Srivastava/Property station','8010222666','','','','Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic','','Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft','','As per the confirmation he is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1697-sq-ft-spid-F28942203?pos=SEARCH',309,'136492691906801'),
(94,'Broker','Sell','Apartment','Gurgaon','Spaze Privy the Address','','Sector-93','','1998','','','3','4','3','servant room','19','10','','','','62Lac','3104','4037','6200000','3103','3825','721,8968969','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 72','Bimlesh Choudhary/Reliable Realtors','9999640915','','','','Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic','','Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft','','As per the confirmation he is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1998-sq-ft-r3-spid-H20210189?pos=SEARCH',309,'1.422668597484051e15'),
(95,'Broker','Sell','Apartment','Gurgaon','Sare Petioles','','Sector-92','','2093','','','4','4','3','servant room','20','6','','1 Covered','','67Lac','3202','4420','6700000','3201','4100','898,8533206','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','Yes','Yes','','Yes','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-east','60-Feet','Vitrified','','Yes','','Sector 92','Dinesh Kumar/Welcome Home','8510888888','','','','Dinesh Kumar/Welcome Home','8510888888','4000 to 4200rup/sqft','','','','','','As per the confirmation He is broker ready to post property','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sare-petioles-sector-92-gurgaon-2093-sq-ft-spid-H29087649?from_src=XIDPAGE_det&pos=XIDDETVIEW',310,'1.02196673558389e15'),
(96,'Owner','Sell','Apartment','Gurgaon','Avalon Rangoli','','Sector 24 Dharuhera','','1300','','','2','2','3','','12','11','','1 Covered','','30Lac','2308','2465','3000000','2308','2500','192,3076923','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 24 Dharuhera','Upesh Sharma','9911405053','','Upeshsha@gmail.com','','Mohit Yadav / Resale Experts','9728209209','2200 to 2500rup/sqft','','Shiv Rohilla/Shivsan Buildwell Pvt Ltd','9212706175/8930939992','2200 to 2400rup/sqft','','As per the confirmation ready to post property final price is 30Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-avalon-rangoli-sector-24-dharuhera-1300-sq-ft-spid-P29046957?from_src=XIDPAGE_det&pos=XIDDETVIEW',311,'804807034024251'),
(97,'Owner','Sell','Apartment','Gurgaon','Avalon Rangoli','','Sector 24 Dharuhera','','1300','','','2','2','3','','12','7','','1 Covered','','30.55Lac','2350','2465','3055000','2350','2500','150','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','','','','Yes','','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 24 Dharuhera','Raj','9810980437','','Rajkumarkansal95@gmail.com','','Mohit Yadav / Resale Experts','9728209209','2200 to 2500rup/sqft','','Shiv Rohilla/Shivsan Buildwell Pvt Ltd','9212706175/8930939992','2200 to 2400rup/sqft','','As per the confirmation ready to post property final price is 30.5Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-avalon-rangoli-sector-24-dharuhera-1300-sq-ft-r3-spid-Y21898625?from_src=XIDPAGE_det&pos=XIDDETVIEW',311,'935506789014495'),
(98,'Broker','Sell','Apartment','Gurgaon','Avalon Rangoli','','Sector 24 Dharuhera','','1300','','','2','2','3','','12','5','','1 Covered','','27.5Lac','2116','2465','2750000','2115','2500','384,6153846','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','North','24-Feet','Vitrified','Semifurnished','Yes','Yes','Sector 24 Dharuhera','Mohit Yadav / Resale Experts','9728209209','','','','Mohit Yadav / Resale Experts','9728209209','2200 to 2500rup/sqft','','Shiv Rohilla/Shivsan Buildwell Pvt Ltd','9212706175/8930939992','2200 to 2400rup/sqft','','As per the confirmation he is broker ready to post property','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-avalon-rangoli-sector-24-dharuhera-1300-sq-ft-spid-U28496089?from_src=XIDPAGE_det&pos=XIDDETVIEW',311,'284265763867034'),
(99,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali','','Sector 1','','2500','2400','','4','4','3','','10','9','','1 Covered','','75','3000','3315','7500000','3000','3350','350','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','','','Yes','','','Full','Borewell/Tank','East','120-Feet','Marble','','Yes','','Sector 1','Ashwani Sharma','7087696046','','aks33noida@gmail.com','','Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft','','','','','','As per the confirmation ready to post property final price is 74Lac','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2500-sq-ft-r1-spid-T28164344?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTMyMjY5MjMwNDQ2NTU1ICMyI3wgIHwgMjg2MTc5ODEsMjgzNzU0NzkgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMyNiMgfCAyIHwjNyMgIHw=',312,'568385278517537'),
(100,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali','','Sector 1','','2500','','','4','4','2','Servant Room','10','4','','1 Covered','','82Lac','3280','3315','8000000','3200','3350','150','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','','','','','','Full','Municipal Corporation','','','','','Yes','','Sector 1','R.N Khan','9463888308','','ringing','','Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft','','','','','','As per the confirmation ready to post property on alive asset final price is 80Lac not share Mail ID','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2500-sq-ft-r2-spid-M26722915?from_src=XIDPAGE_det&pos=XIDDETVIEW',312,'2.000431463818404e15'),
(101,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali','','Sector 1','','2700','','','4','4','2','','10','10','','1 Covered','','75','2778','3315','7400000','2741','3350','609,2592593','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','','','Yes','','','Full','Municipal Corporation','East','120-Feet','Marble','Furnished','Yes','Yes','Sector 1','Ashwani Sharma','7087696046','','aks33noida@gmail.com','','Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft','','','','','','As per the confirmation ready to post property final price is 74Lac','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2700-sq-ft-spid-E28981929?from_src=XIDPAGE_det&pos=XIDDETVIEW',312,'213447417766631'),
(102,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali','','Sector 1','','','2750','','4','4','','','10','7','','1 Covered','','85Lac','3091','3315','8500000','3091','3350','259,0909091','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','','Full','Municipal Corporation','East','120-Feet','Marble','Furnished','Yes','Yes','Sector 1','Abhishek Gupta','9779744123','','Abhishekgupta701@gmail.com','','Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft','','','','','','As per the confirmation ready to post property final price is 83.5Lac','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2750-sq-ft-r4-spid-B7262905?from_src=XIDPAGE_det&pos=XIDDETVIEW',312,'1.128842985877865e15'),
(103,'Broker','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali','','Sector 1','','2500','','','3','3','3','Servant Room & Pooja Room','10','3','','1 Covered','','65Lac','2600','3315','6500000','2600','3350','750','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','','Full','Borewell/Tank','North-east','100-Feet','Marble','Semifurnished','Yes','','Sector 1','Manoj Chauhan/shri Shani Properties','9990855998','','','','Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft','','','','','','As per the confirmation He is broker ready to post property final price is 65lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2500-sq-ft-r3-spid-I25449905?from_src=XIDPAGE_det&pos=XIDDETVIEW',312,'891279644614948'),
(104,'Owner','Sell','Apartment','Gurgaon','vipul world','','Sector 48','','1800','1450','','3','3','2','study room','3','3','','','','82','4555','5482','8200000','4556','5100','544,4444444','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','Yes','Yes','Yes','','Yes','','','','','','','','','','Yes','Yes','','','Full','','','','Vitrified','Furnished','Yes','','Sector 48','Vivek Tomer','9599784990','','vivektomer86@gmail.com','','Sayta Pratap/invester Planner','9999515707','5000 to 5200rup/sqft All inclucive','','Narender/Neha Estate','9999996375','5200rup/sqft','','As per the confirmation ready to post property final price is 82Lac.','','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-vipul-world-sector-48-gurgaon-1800-sq-ft-spid-F27908780?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDI0NjggfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNzA1IHwjMyMgIHw=',313,'1.046732803498769e15'),
(105,'Owner','Sell','Apartment','Gurgaon','vipul world','','Sector 48','','1800','1450','','3','3','1','','4','3','','1 Covered','1 Open','82','5240','5482','8200000','4556','5100','544,4444444','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','Yes','Yes','','','','','','','','','','','','','','Full','','','','Vitrified','','Yes','','Sector 48','Vivek','9958055701','','vivektomer86@gmail.com','','Sayta Pratap/invester Planner','9999515707','5000 to 5200rup/sqft All inclucive','','Narender/Neha Estate','9999996375','5200rup/sqft','','call back','','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-vipul-world-sector-48-gurgaon-1450-sq-ft-spid-I28193098?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDI0NjggfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNzA1IHwjMyMgIHw=',313,'440263174661786'),
(106,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills','','Sector 77','','','1260','','2','2','2','','18','8','','','','56Lac','4445','4462','5600000','4444','5333.333333','888,8888889','4500','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','','Yes','Yes','','Yes','Yes','','','','','','Yes','Yes','Yes','','','','Full','','','','Vitrified','','Yes','','Sector 77','Gyanendra','9811009732','','Gyanendra_vn@yahoo.co.in','','Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive','','Amrish Srivastava/Property station','8010222666','5500rup/sqft','','As per the confirmation ready to post property final price is 4500rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1260-sq-ft-r7-spid-N15007765?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTM5MjE3NDczMzYxODI1ICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA=',314,'1.159831583770995e15'),
(107,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills','','Sector 77','','1735','','','3','3','3','','18','3','','1 Covered','','81.55Lac','4701','4462','3155000','1818','5333.333333','3514,889529','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','Yes','','Full','','west','','Vitrified','Semifurnished','Yes','','Sector 77','Nitish Sharma','','','','','Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive','','Amrish Srivastava/Property station','8010222666','5500rup/sqft','','no contact','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1735-sq-ft-r7-spid-Z19929203?from_src=XIDPAGE_det&pos=XIDDETVIEW',314,'191302237841477'),
(108,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills','','Sector 77','','1515','','','3','2','2','','18','6','','','','70.45Lac','4651','4462','7045000','4650','5333.333333','683,1683168','4600','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','Yes','','Full','','','','','Furnished','Yes','','Sector 77','Nishi Gangneja','9971460456','9810897899 Mandeep Kumar','mkazab11@gmail.com','','Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive','','Amrish Srivastava/Property station','8010222666','5500rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com.','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1515-sq-ft-r4-spid-V18188333?from_src=XIDPAGE_det&pos=XIDDETVIEW',314,'1.777264600388395e15'),
(109,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills','','Sector 77','','1735','','','3','3','3','Pooja Room','18','16','','1 Covered','','72','4510','4462','7200000','4150','5333.333333','1183,477426','4500','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','Yes','','Full','','','','marble','Furnished','Yes','','Sector 77','Pankaj','9717868466','','Pankaj.sinha@itchotels.in','','Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive','','Amrish Srivastava/Property station','8010222666','5500rup/sqft','','As per the confirmation ready to post property final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1735-sq-ft-r5-spid-E15954065?from_src=XIDPAGE_det&pos=XIDDETVIEW',314,'1.788017194198863e15'),
(110,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills','','Sector 77','','','1260','','2','2','3','Pooja Room','18','4','','1 Covered','','58Lac','4604','4462','5800000','4603','5333.333333','730,1587302','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','','Yes','','Full','Borewell/Tank','Park/Garden','100-Feet','marble','Unfurnished','Yes','','Sector 77','Niraj','9999850678','','nkc_indian@yahoo.co.in','','Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive','','Amrish Srivastava/Property station','8010222666','5500rup/sqft','','As per the confirmation ready to post property final price is 55Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1260-sq-ft-r3-spid-D25714021?from_src=XIDPAGE_det&pos=XIDDETVIEW',314,'1.372752443266662e15'),
(111,'Broker','Sell','Apartment','Gurgaon','Umang Winter Hills','','Sector 77','','','1260','','2','2','2','','18','7','','','','50.4Lac','4000','4462','5040000','4000','5333.333333','1333,333333','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','Yes','','','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','Semifurnished','Yes','','Sector 77','Himanshu / DC Jain Real Estate Services','9873999789','','','','Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive','','Amrish Srivastava/Property station','8010222666','5500rup/sqft','','As per the confirmation he is broker ready to post property','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1260-sq-ft-r4-spid-L14289335?from_src=XIDPAGE_det&pos=XIDDETVIEW',314,'1.463685576061156e15'),
(112,'Owner','Sell','Apartment','Gurgaon','BPTP terra','','Sector-37D','','','1691','','3','3','','','23','10','','','','84.55Lac','5000','5567','8455000','5000','5100','100','','','','Under construction','Dec-17','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sector-37D','Deb','9899211161','','ringing','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','5000 to 5200rup/sqft','','','','','','','Ringing','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-bptp-terra-sector-37-d-gurgaon-1691-sq-ft-r4-spid-X23702360?from_src=XIDPAGE_det&pos=XIDDETVIEW',315,'932060768426426'),
(113,'Owner','Sell','Apartment','Gurgaon','Pareena Coban Residences','','Sector 99A','','','1997','','3','3','3','','20','10','','','','1Cr','5008','5100','10000000','5008','5100','92,4887331','4900','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 99A','Kunal Tiwari','9829404933','','Tiwari.kt@gmail.com','','Manmohan Gupta/Brick ty Brick','8048094664','5000 to 5200rup/sqft','','','','','','As per the confirmation ready to post final price is 4900rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-pareena-coban-residences-sector-99-a-gurgaon-1997-sq-ft-r7-spid-G21173222?from_src=XIDPAGE_det&pos=XIDDETVIEW',316,'231738900487717'),
(114,'Owner','Sell','Apartment','Gurgaon','Puri Emerald Bay','','Sector-104','','2450','','','3','4','3','Servant Room','31','26','','2 Covered','','1.6Cr','6531','7820','16000000','6531','7666.666667','1136,054422','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','North','492.1Feet','marble','Semifurnished','Yes','Yes','Sector-104','Satish Singh','','','','','Manmohan Gupta/Brick ty Brick','8048094664','7500 to 8000rup/sqft','','Vijay Khurana/Finmart Realty','9990508289','7500rup/sqft','','No contect','No contect','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-puri-emerald-bay-sector-104-gurgaon-2450-sq-ft-r1-spid-F27600095?from_src=XIDPAGE_det&pos=XIDDETVIEW',317,'622340821183338'),
(115,'Owner','Sell','Apartment','Gurgaon','Forte Point the Olive Spire','','Sector-70A','','1388','','','2','2','2','','12','8','','1 Covered','','70Lac','5044','5270','7000000','5043','5250','206,7723343','','','','Immediate','Dec-18','Resale','Freehold','','Yes','Yes','Yes','','','','','Yes','Yes','Yes','','','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 70A','Subhra Saha','9136041875','','Subhra_saha@yahoo.com','','Amrish Srivastava/Property station','8010222666','5000 to 5500rup/sqft','','','','','','As per the confirmation ready to post property final price is 70Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-forte-point-the-olive-spire-sector-70-a-gurgaon-1388-sq-ft-r2-spid-E27248469?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NTU2NjM2OTExMjQyICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA=',318,'111800849655260'),
(116,'Owner','Sell','Apartment','Gurgaon','Paras Dews','','Sector-106','','','1760','','3','4','3','Servant & study room','24','12','','1 Covered','','90Lac','5133','5227','9000000','5114','5175','61,36363636','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','','','Yes','Yes','Yes','','Full','Municipal Corporation','Park/Garden','','Vitrified','Semifurnished','Yes','','Sector-106','Ayush','8178819634','9953480099','busy','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ','','Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft','','DNE','Ringing','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1760-sq-ft-spid-Q29066279?from_src=XIDPAGE_det&pos=XIDDETVIEW',319,'1.003936648025823e15'),
(117,'Owner','Sell','Apartment','Gurgaon','Paras Dews','','Sector-106','','','1665','','3','3','3','','24','9','','1 Covered','','75Lac','4504','5227','7500000','4505','5175','670,4954955','4500','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','','Yes','','','','','','','','','','Yes','','Yes','Yes','Yes','','Full','','','','Wood','Semifurnished','Yes','','Sector-106','Sanjeev','9310578160','','Sanjeevsharn77@gmail.com','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ','','Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft','','As per the confirmation ready to post final price is 4500rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1665-sq-ft-r5-spid-U21342584?from_src=XIDPAGE_det&pos=XIDDETVIEW',319,'1681259020607'),
(118,'Owner','Sell','Apartment','Gurgaon','Paras Dews','','Sector-106','','','1665','','3','3','3','','24','12','','','','83.25Lac','5000','5227','8325000','5000','5175','175','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector-106','Manish Jain','9167008956','','busy','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ','','Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft','','As per the confirmation said final price is 1 cr not ready to share all info on call.','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1665-sq-ft-r3-spid-U20169105?from_src=XIDPAGE_det&pos=XIDDETVIEW',319,'1.70512228180524e15'),
(119,'Broker','Sell','Apartment','Gurgaon','Paras Dews','','Sector-106','','1760','','','3','4','3','Study & Servant Room','24','11','','1 Covered','','80.96Lac','4600','5227','8096000','4600','5175','575','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','','east','196.9 Feet','marble','Semifurnished','Yes','','Sector-106','Naveen/Winworld Realty','9650344337','','','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ','','Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft','','As per the confirmation He is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1760-sq-ft-r4-spid-Q22807771?from_src=XIDPAGE_det&pos=XIDDETVIEW',319,'1.391646454015376e15'),
(120,'Broker','Sell','Apartment','Gurgaon','Paras Dews','','Sector-106','','1900','','','3','4','3','Study & Servant Room','24','14','','1 Covered','','87.66Lac','4614','5227','8766000','4614','5175','561,3157895','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','east','','Vitrified','Semifurnished','Yes','','Sector-106','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','','','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ','','Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft','','As per the confirmation He is broker ready to post property','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1900-sq-ft-r3-spid-L25241478?from_src=XIDPAGE_det&pos=XIDDETVIEW',319,'1.814636906164498e15'),
(121,'Owner','Sell','Apartment','Gurgaon','Vatika the Seven Lamps','','Sector 82','','2158','','','3','4','3','Servant & study room','17','14','','2 Covered','','1.07Cr','4960','4717','8700000','4032','4825','793,489342','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','Yes','Yes','Yes','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','East','78.7-Feet','Wood','Unfurnished','Yes','Yes','Sector 82','Vinyas','8130805553','','ringing','','Maninder Singh/Real infra solution','9810802935','4600 to 5000rup/sqft','','Vikas/I Realty Propmart','9810307978','4700 to 5000rup/sqft','','ringing','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-vatika-the-seven-lamps-sector-82-gurgaon-2158-sq-ft-r2-spid-O26830133?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc0OTAgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgMzM1MTYjNCMgfCA=',276,'22279645085266'),
(122,'Owner','Sell','Apartment','Gurgaon','Landmark tower','','Sector 51','','','1050','','2','2','','','6','2','','','','57','5428','6332','5600000','5333','6000','666,6666667','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','','Yes','','Sector 51','Bharti','9990545000','','amit.leocar@gmail.com','','Arvind/PSN Realtech','9971087673','6000rup/sqft','','','','','','as per the confirmation ready to post porperty on fairpockets.com.','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-landmark-towers-sector-51-gurgaon-1050-sq-ft-r4-spid-X20457169?from_src=XIDPAGE_det&pos=XIDDETVIEW',320,'1.960302343826142e15'),
(123,'Owner','Sell','Apartment','Gurgaon','Vatika Emilia Floors','','Sector 82','','1500','','','2','2','2','','4','Ground','','','1 Open','66Lac','4400','5227','6600000','4400','4825','425','','','0 to 1 Year','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','Full','','','','','Unfurnished','Yes','','Sector 82','Sandeep Saras','9934012239','','off','','Maninder Singh/Real infra solution','9810802935','4600 to 5000rup/sqft','','Vikas/I Realty Propmart','9810307978','4700 to 5000rup/sqft','','Off','out of service','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-vatika-emilia-floors-sector-82-gurgaon-1500-sq-ft-r1-spid-R28050552?from_src=XIDPAGE_det&pos=XIDDETVIEW',321,'2.370769056709672e15'),
(124,'Broker','Sell','Apartment','Gurgaon','BPTP Freedom Park Life','','Sector 57','','225','220','','1','1','1','','19','2','','','1 Open','13Lac','5778','6757','1300000','5778','6250','472,2222222','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','','','Yes','','Full','Municipal Corporation','North-East','','Cement','Unfurnished','Yes','','Sector 57','Shilpa/Global Estate','9990752121','','','','Shilpa/Global Estate','9990752121','6000 to 6500rup/sqft','','','','','','As per the confirmation He is broker ready to post property on Fairpockets.com13Lac','','http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-bptp-freedom-park-life-sector-57-gurgaon-225-sq-ft-spid-R29075877?from_src=XIDPAGE_det&pos=XIDDETVIEW',322,'970467666057854'),
(125,'Owner','Sell','Apartment','Gurgaon','Ansal Heights 86','','Sector 86','','1690','1500','','3','2','1','','14','10','','2 Covered','','56.18Lac','3325','3187','5618000','3324','3400','75,73964497','3200','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','Yes','','','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','Pool/Club','','','','Yes','','Sector 86','Amit','9466935388','','amit.n.kambhoj@gmail.com','','Gaurav/JMD Properties','9811119829','3000rup/sqft','','Paras Rajan/Right Concepte','9999989894','2800 to 3000rup/sqft','','As per the ready to post property on Fairpockets.com final price is  3200rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ansal-heights-86-sector-86-gurgaon-1690-sq-ft-r1-spid-K27968344?from_src=XIDPAGE_det&pos=XIDDETVIEW',323,'184904374888728'),
(126,'Owner','Sell','Apartment','Gurgaon','Ansal Heights 86','','Sector 86','','1690','','','3','2','1','','14','9','','1 Covered','','55.75Lac','3299','3187','5575000','3299','3400','101,183432','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','Yes','','','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','','Vitrified','Semifurnished','Yes','Yes','Sector 86','Ashi','8076927218','','ringing','','Gaurav/JMD Properties','9811119829','3000rup/sqft','','Paras Rajan/Right Concepte','9999989894','2800 to 3000rup/sqft','','ringing','out of service','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ansal-heights-86-sector-86-gurgaon-1690-sq-ft-spid-B28489995?from_src=XIDPAGE_det&pos=XIDDETVIEW',323,'515116773251671'),
(127,'Owner','Sell','Apartment','Gurgaon','AWHO Sispal Vihar','','Sector 49','','300','250','','1','1','1','','13','1','','','','15Lac','5000','7565','1400000','4667','5100','433,3333333','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','','Yes','Yes','Yes','','','Yes','','','','','','','','','','Yes','','','Full','','North','','Marble','Furnished','Yes','Yes','Sector 49','Raj Kumar','9910798793','','32.gurgaom@gmail.com','','Arvind/PSN Realtech','9971087673','5000 to 5200rup/sqft All inclucive','','Surender Yadav/Jai Realty','9990171415','5000rup/sqft','','As per the ready to post property on Fairpockets.com final price is  14Lac','','http://www.99acres.com/studio-apartment-flat-for-sale-in-awho-sispal-vihar-sector-49-gurgaon-300-sq-ft-r1-spid-V24649575?from_src=XIDPAGE_det&pos=XIDDETVIEW',324,'2.032436552124541e15'),
(128,'Owner','Sell','Apartment','Gurgaon','BPTP Astaire Gardens','','Sector 70 A','','1690','','','3','3','','','2','Ground','','','','82.5Lac','4882','5270','8200000','4852','4866.666667','14,59566075','','','','Under construction','Dec-17','Resale','Freehold','','','Yes','Yes','Yes','Yes','Yes','','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','','Vitrified','Unfurnished','Yes','','Sector 70 A','Deepak','9716299555','','D_rally1978@hotmail.com','','Saurabh Goswami/Reliable Realtors','9873400634','4800 to 5000rup/sqft','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','4800rup/sqft','','As per the ready to post property on Fairpockets.com final price is  82Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-bptp-astaire-gardens-sector-70-a-gurgaon-1690-sq-ft-r3-spid-H21660851?from_src=XIDPAGE_det&pos=XIDDETVIEW',325,'926614411827115'),
(129,'Broker','Sell','Apartment','Gurgaon','Bestech City','','Manesar','','975','','','2','2','3','','14','3','','','','33Lac','3384','3485','3200000','3282','3300','17,94871795','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','','','','','','Full','','','','Vitrified','Semifurnished','Yes','','Manesar','Shiv Rohilla/Shivsan Buildwell','9212706175','','','','Shiv Rohilla/Shivsan Buildwell','9212706175','3200 to 3500rup/sqft','','Anil Kumar/Goodwill propertys','9001797735','3200rup/sqft','','As per the confirmation He is broker ready to post property on live asset 32Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-bestech-city-dharuheda-gurgaon-975-sq-ft-r4-spid-I24497945?from_src=XIDPAGE_det&pos=XIDDETVIEW',326,'1.078010817975365e15'),
(130,'Broker','Sell','Apartment','Gurgaon','Bestech City','','Manesar','','975','','','2','2','2','','14','6','','1 Covered','','31.5Lac','3231','3485','3100000','3179','3300','120,5128205','','','5 to 10 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','North-East','200-Feet','Vitrified','Semifurnished','Yes','','Manesar','Anil Kumar/Goodwill propertys','9001797735','','','','Shiv Rohilla/Shivsan Buildwell','9212706175','3200 to 3500rup/sqft','','Anil Kumar/Goodwill propertys','9001797735','3200rup/sqft','','As per the confirmation He is broker ready to post property on live asset 31Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-bestech-city-dharuheda-gurgaon-975-sq-ft-spid-K28801227?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IFNIIHwgWSB8IzE1IyAgfCBQRCM2IyB8IDggfCMxIyAgfCBPICMxI3wgIHwgODA5MCB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA=',326,'2.608559249470011e15'),
(131,'Broker','Sell','Apartment','Gurgaon','Shree vardhman Flora','','Sector-90','','1875','','','3','3','3','Servant Room','14','9','','','','65.63Lac','3500','3910','6500000','3467','3700','233,3333333','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','','','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','Yes','','','Full','','','','Vitrified','Semifurnished','Yes','','Sector-90','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','','','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','3500 to 4000rup/sqft','','Amrish Srivastava/Property station','8010222666','3500rup/sqft to 3800rup/sqft','','As per the confirmation He is broker ready to post property on live asset 65Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-shree-vardhman-flora-sector-90-gurgaon-1875-sq-ft-r4-spid-Y24279661?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgQSMyIyB8IDc0MzQgfCBSIHwgIHwgMSMzIyB8IEEgfCMyOSMgIHwgODMzMiAjMyN8ICA=',327,'1.931707130538942e15'),
(132,'Broker','Sell','Apartment','Gurgaon','Shree vardhman Flora','','Sector-90','','1300','','','2','2','2','Study Room','14','8','','1 Covered','','45.55Lac','3504','3910','4500000','3462','3700','238,4615385','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','','','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','Yes','','','Full','Municipal Corporation','North','75-Feet','Vitrified','','Yes','','Sector-90','Amrish Srivastava/Property station','8010222666','','','','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','3500 to 4000rup/sqft','','Amrish Srivastava/Property station','8010222666','3500rup/sqft to 3800rup/sqft','','As per the confirmation He is broker ready to post property on live asset 45Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-shree-vardhman-flora-sector-90-gurgaon-1300-sq-ft-r2-spid-E27439867?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgQSMyIyB8IDc0MzQgfCBSIHwgIHwgMSMzIyB8IEEgfCMyOSMgIHwgODMzMiAjMyN8ICA=',327,'1.786192223623996e15'),
(133,'Owner','Sell','Apartment','Gurgaon','The Fernhill','','Sector 91','','1877','','','3','3','3','','13','6','','1 Covered','','60','3197','3145','5000000','2664','2900','236,1747469','','','','Under construction','Jul-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','Yes','','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 91','Ananda Dharmapal','9811565874','','','','Yogesh Kumar/Equator realtors','9717744497','2800 to 3000rup/sqft','','Karambir/Philby real Estate','9910050953','3000rup/sqft','','ringing','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-the-fernhill-sector-91-gurgaon-1877-sq-ft-r4-spid-C27247623?from_src=XIDPAGE_det&pos=XIDDETVIEW',328,'432265948541370'),
(134,'Owner','Sell','Apartment','Gurgaon','Mapsko Casa Bella','','Sector-82','','','1960','','3','3','3','','25','2','','','1 Open','85Lac','4337','4292','8000000','4082','4100','18,36734694','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','','','','','','','','','','','','','','','','','','','','','','','','','','Yes','','Sector 82','Madav','9999998384','','madhav.monga@yahoo.com','','Maninder Singh/Real infra solution','9810802935','4000 to 4200rup/sqft','','Vikas/I Realty Propmart','9810307978','4000rup/sqft','','as per the confirmation ready to post porperty on fairpockets.com ','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-mapsko-casa-bella-sector-82-gurgaon-1960-sq-ft-r1-spid-X27405009?pos=SEARCH',279,'2.143670459241185e15'),
(135,'Broker','Sell','Apartment','Gurgaon','Tulip Orange','','Sector-70','','1137','','','3','2','2','Study Room','12','5','','1 Covered','','70Lac','6157','6417','7000000','6157','6333.333333','176,7810026','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','','','Yes','','','','Yes','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','North-East','36-Feet','Vitrified','Semifurnished','Yes','Yes','Sector-70','Mayank Roof Realtech','9599481837','','','','Maninder Singh/Real infra solution','9810802935','6000 to 6500rup/sqft','','Vikas/I Realty Propmart','9810307978','6500rup/sqft','','As per the confirmation he is broker final price is 70Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tulip-orange-sector-70-gurgaon-1137-sq-ft-spid-Q26694155?from_src=XIDPAGE_det&pos=XIDDETVIEW',280,'1.312742300372642e15'),
(136,'Owner','Sell','Apartment','Gurgaon','Anand Raj Madelia','','Manesar','','','1772','','4','4','3','','14','10','','','','60Lac','3387','4377','6000000','3386','3250','-136,0045147','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','','Yes','','','Yes','','','','','','','','Yes','Yes','Yes','','','Full','','','','','','Yes','','Manesar','Roman Hashmi','9319134412','','','','Manoj  Chauhan/Shri Shani Properties','9990855998','3000 to 3500rup/sqft','said some dispute going on','','','','','As per the confirmation Manesar Secter M2  some types of legal disputes gpong on so cant sale the properties','','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-anant-raj-madelia-manesar-gurgaon-1772-sq-ft-r2-spid-U27429941?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NTU2NjM2OTExMjQyICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA=',329,'85335049205718'),
(137,'Owner','Sell','Apartment','Gurgaon','Pareena Coban Residences','','Sector 99A','','','1997','','3','3','3','','20','10','','','','1Cr','5008','5100','10000000','5008','5100','92,4887331','4900','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 99A','Kunal Tiwari','9829404933','','Tiwari.kt@gmail.com','','Manmohan Gupta/Brick ty Brick','8048094664','5000 to 5200rup/sqft','','','','','','As per the confirmation ready to post final price is 4900rup/sqft','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-pareena-coban-residences-sector-99-a-gurgaon-1997-sq-ft-r7-spid-G21173222?from_src=XIDPAGE_det&pos=XIDDETVIEW',316,'1.960125561056809e15'),
(138,'Owner','Sell','Apartment','Gurgaon','Pareena Coban Residences','','Sector 99A','','','1550','','2','2','','','20','6','','1 Covered','','80Lac','5162','5100','8000000','5161','5100','-61,29032258','5000','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','Unfurnished','Yes','','Sector 99A','A S Mazumdar','9540736726','','Asmazumdar@gmail.com','','Manmohan Gupta/Brick ty Brick','8048094664','5000 to 5200rup/sqft','','','','','','As per the confirmation ready to post final price is 5000rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-coban-residences-sector-99-a-gurgaon-1550-sq-ft-r7-spid-W20126029?from_src=XIDPAGE_det&pos=XIDDETVIEW',316,'1.261556190125711e15'),
(139,'Owner','Sell','Apartment','Gurgaon','Puri Emerald Bay','','Sector-104','','','1550','','2','2','','','31','14','','','','1.2Cr','7742','7820','12000000','7742','7666.666667','-75,2688172','7250','','','Under construction','Dec-17','Resale','Freehold','','Yes','','','','Yes','','','Yes','','','','','','','','','','','','Full','','','','','Unfurnished','Yes','','Sector-104','Divyanshu','9457125121','','Divyanshugoel2005@yahoo.co.in','','Manmohan Gupta/Brick ty Brick','8048094664','7500 to 8000rup/sqft','','Vijay Khurana/Finmart Realty','9990508289','7500rup/sqft','','As per confirmation ready to post property final price is 7250 to 7400rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-puri-emerald-bay-sector-104-gurgaon-1550-sq-ft-r7-spid-C17096737?from_src=XIDPAGE_det&pos=XIDDETVIEW',317,'387267996159106'),
(140,'Owner','Sell','Apartment','Gurgaon','Forte Point the Olive Spire','','Sector-70A','','1388','','','2','2','2','','12','8','','1 Covered','','70Lac','5044','5270','7000000','5043','5250','206,7723343','','','','Under construction','Dec-18','Resale','Freehold','','Yes','Yes','Yes','','','','','Yes','Yes','Yes','','','','Yes','Yes','Yes','Yes','Yes','','Full','','','','','','Yes','','Sector 70A','Subhra Saha','9136041875','','Subhra_saha@yahoo.com','','Amrish Srivastava/Property station','8010222666','5000 to 5500rup/sqft','','','','','','As per the confirmation ready to post property final price is 70Lac','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-forte-point-the-olive-spire-sector-70-a-gurgaon-1388-sq-ft-r2-spid-E27248469?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NTU2NjM2OTExMjQyICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA=',318,'934707016807916'),
(141,'Owner','Sell','Apartment','Gurgaon','Ninex City','','Sector 76','','1952','','','3','4','3','Servant Room','12','7','','1 Covered','','88Lac','4509','4335','8800000','4508','4250','-258,1967213','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','Yes','Yes','Yes','Yes','Yes','Yes','','Full','Borewell/Tank','West','54-Feet','Vitrified','Semifurnished','Yes','','Sector 76','R S Chauhan','8802340684','','busy','','Amrish Srivastava/Property station','8010222666','4000 to 4500rup/sqft','','','','','','As per the confirmation ready to post final price is 85Lac','','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ninex-city-sector-76-gurgaon-1952-sq-ft-r3-spid-F25562417?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NzUwMzgzNDk4OTYxICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA=',330,'684270577822744'),
(142,'Owner','Sell','Apartment','Gurgaon','Parkwood Westend','','Sector-92','','1200','','','2','2','3','Study room','14','2','','1 Covered','','45Lac','3250','3315','4500000','3750','3500','-250','3100','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','','','','','','Yes','Yes','Yes','Yes','Yes','','Full','Municipal Corporation','East','196.9 Feet','Vitrified','Semifurnished','Yes','Yes','Sector-92','Ashutosh Gupta','9910108279','','Ashutosh.gupta19@gmail.com','','Dinesh Kumar/Welcome Home','8510888888','3300rup/sqft to 3700rup/sqft','','','','','','As per the confirmation ready to post property final price is 3100rup/sqft','','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-parkwood-westend-sector-92-gurgaon-1200-sq-ft-r1-spid-W27582047?from_src=XIDPAGE_det&pos=XIDDETVIEW',331,'601437968678552'),
(143,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1776','','','3','3','3','Servant Room','23','8','','1 Covered','','65','4043','3910','6200000','3491','3750','259,009009','3490','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','','','196.9 Feet','','Semifurnished','Yes','Yes','Sector 92','Manju Goel','9953681243','','Manujgoel@gmail.com','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','As per the confirmation ready to post property.','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1608-sq-ft-r4-spid-D20311591?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'946437694909464'),
(144,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1596','1400','1300','3','3','3','Servant Room','23','3','','1 Covered','','62','3884','3910','6200000','3885','3750','-134,7117794','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','50- Feet','Vitrified','','Yes','','Sector 92','Siddharth','9717239595','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1596-sq-ft-r2-spid-L28939297?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'30914290962381'),
(145,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','','1261','','3','3','','','23','12','','1 Covered','','47','3728','3910','4700000','3727','3750','22,79936558','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','','','','','','Yes','','Sector 92','Vineet','9990001696','9990001646','call after some time for mail Id','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','As per the confirmation ready to post property.','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1261-sq-ft-r4-spid-X22884073?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'201432031519301'),
(146,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1870','','','4','5','3','Servant Room','23','2','','1 Covered','','75','4010','3910','7500000','4011','3750','-260,6951872','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','','','','','Yes','Yes','Sector 92','Aman Sehra','9911272119','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','Busy','','https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1870-sq-ft-spid-S30966513?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'920075211883980'),
(147,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1851','','','3','4','3','Servant Room','23','17','','1 Covered','','68','3674','3910','6800000','3674','3750','76,31010265','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','','','','','','Yes','','Sector 92','Satyam','8745002200','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','Ringing','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1851-sq-ft-spid-X30732761?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'1.054101312971029e15'),
(148,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1870','','','4','5','3','Servant Room','23','9','','1 Covered','','65.45','3500','3910','','0','3750','3750','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','','Park/Garden','','','Unfurnished','Yes','','Sector 92','Rupak Kumar','9810866676','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','Ringing','','https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1870-sq-ft-spid-R31219339?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'2.508323497031154e15'),
(149,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1660','','','3','3','3','','23','18','','1 Covered','','59','3542','3910','','0','3750','3750','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','East','','Vitrified','','Yes','','Sector 92','Vinay','8860630818','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','DNE','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1666-sq-ft-spid-N31246345?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'404543950206695'),
(150,'Broker','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1666','','','3','3','2','Servant Room','23','2','','1 Covered','','65','3902','3910','','0','3750','3750','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','32.8-Feet','Vitrified','Semifurnished','Yes','','Sector 92','Peeyush Goyal/Pathways Consultion','9582532628','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1666-sq-ft-r5-spid-D25883275?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'460968617014996'),
(151,'Broker','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92','','1666','1665','1664','3','4','3','Servant Room','23','2','','1 Covered','','63','3782','3910','6300000','3782','3750','-31,51260504','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','','Yes','','','Yes','','','','','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','60-Feet','Vitrified','Semifurnished','Yes','','Sector 92','Kamal/Aadhar Homes','7042000546','','','','Kamal/Aadhar Homes','7042000546','3750','','Peeyush Goyal/Pathways Consultion','9582532628','3750','','','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1666-sq-ft-spid-K30982103?from_src=XIDPAGE_det&pos=XIDDETVIEW',332,'1.09028658822074e15'),
(152,'Owner','Sell','Apartment','Gurgaon','CHD Avenue 71','CHD Developers','Sector 71','','2193','','','4','4','3','Servant Room','15','14','','1 Covered','','1.2','5472','5567','11500000','5244','5500','256,0419517','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','','','','','','Yes','','Sector 71','Sumita','9899694840','','Sumitakukreja@gmail.com','','Harish Mahna/Chaahat Homes','9560621391','5500','','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','5500','','As per the confermation ready to post','','https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2193-sq-ft-r1-spid-E30620557?pos=SEARCH',257,'1.020182049073169e15'),
(153,'Broker','Sell','Apartment','Gurgaon','CHD Avenue 71','CHD Developers','Sector 71','','2350','','','4','4','3','Servant Room','15','9','','1 Covered','','1.2','5107','5567','12000000','5106','5500','393,6170213','','','1 to 5 Years','Ready to Move','','Resale','Freehold','','Yes','Yes','Yes','Yes','','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','Park/Garden','196.9 Feet','Wood','','Yes','','Sector 71','Harish Mahna/Chaahat Homes','9560621391','9560621351','','','Harish Mahna/Chaahat Homes','9560621391','5500','','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','5500','','','','https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2350-sq-ft-spid-M30984785?pos=SEARCH',257,'1.820286549698912e15'),
(154,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95','','2212','','','4','4','3','Pooja room & Servant Room','21','17','','1 Covered','','94','4250','4845','9400000','4250','4500','250,4520796','','','','Immediate','','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','','Park/Garden','','Vitrified','Unfurnished','Yes','Yes','Sector 95','Jason Ford','9811811420','','','','Surender Sharma/Simply One Associates','9910780177','','','','','','','busy','','https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-2212-sq-ft-r5-spid-M26597752?from_src=XIDPAGE_det&pos=XIDDETVIEW',333,'2.953752027450802e15'),
(155,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95','','1598','','','3','3','3','Servant Room','21','8','','1 Covered','','70','4381','4845','7000000','4380','4500','119,5244055','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','Park/Garden','','Vitrified','Unfurnished','Yes','','Sector 95','Gautam Kaushik','9910734422','','','','Surender Sharma/Simply One Associates','9910780177','','','','','','','busy','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1598-sq-ft-r6-spid-W25543221?from_src=XIDPAGE_det&pos=XIDDETVIEW',333,'3.079373991867955e15'),
(156,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95','','1598','','','3','4','3','Servant Room','21','1','','1 Covered','','58','3630','4845','5800000','3630','4500','870,4630788','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','Municipal Corporation','North-East','','Vitrified','Semifurnished','Yes','','Sector 95','Manoj','7536862905','','','','Surender Sharma/Simply One Associates','9910780177','','','','','','','Ringing','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1598-sq-ft-spid-O31474319?from_src=XIDPAGE_det&pos=XIDDETVIEW',333,'226481306443507'),
(157,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95','','','2034','','3','3','3','','21','3','','1 Covered','','85','4723','4845','8000000','3933','4500','566,8633235','','','','Under construction','Dec-17','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','','','','','Unfurnished','Yes','','Sector 95','Rahul','9818033323','','aryanRahulin@gmail.com','','Surender Sharma/Simply One Associates','9910780177','','','','','','','As per the confermation ready to post','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1800-sq-ft-r2-spid-Q28821715?from_src=XIDPAGE_det&pos=XIDDETVIEW',333,'1.282934520332108e15'),
(158,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95','','','1598','','3','4','3','Servant Room','21','12','','1 Covered','','63.92','4000','4845','6392000','4000','4500','500','','','','Under construction','Mar-18','Resale','Freehold','','Yes','Yes','Yes','Yes','Yes','','Yes','Yes','','','','','Yes','Yes','Yes','Yes','','Yes','','Full','','','','Vitrified','Semifurnished','Yes','','Sector 95','Neetu Arora','7838867389','','','','Surender Sharma/Simply One Associates','9910780177','','','','','','','Ringing','','https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1598-sq-ft-r9-spid-D9500205?from_src=XIDPAGE_det&pos=XIDDETVIEW',333,'2.580340497546558e15');

/*Table structure for table `property_features` */

DROP TABLE IF EXISTS `property_features`;

CREATE TABLE `property_features` (
  `properties_id` bigint(11) NOT NULL,
  `features_id` int(11) NOT NULL,
  `features_Value` varchar(90) DEFAULT NULL,
  `Unit` varchar(90) DEFAULT NULL,
  `Remarks` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`properties_id`,`features_id`),
  KEY `features_id` (`features_id`),
  CONSTRAINT `property_features_ibfk_4` FOREIGN KEY (`features_id`) REFERENCES `mst_features` (`feature_id`),
  CONSTRAINT `property_features_ibfk_3` FOREIGN KEY (`properties_id`) REFERENCES `properties` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `property_features` */

insert  into `property_features`(`properties_id`,`features_id`,`features_Value`,`Unit`,`Remarks`) values 
(27378763549002,50,'22',NULL,NULL),
(27378763549002,52,'1222',NULL,NULL);

/*Table structure for table `property_inquiries` */

DROP TABLE IF EXISTS `property_inquiries`;

CREATE TABLE `property_inquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `Assisted` varchar(255) NOT NULL,
  `Immediate` int(11) NOT NULL,
  `Portfolio` int(11) NOT NULL,
  `properties_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `properties_id` (`properties_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `property_inquiries` */

/*Table structure for table `property_notifictions` */

DROP TABLE IF EXISTS `property_notifictions`;

CREATE TABLE `property_notifictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `property_id` bigint(11) NOT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `inquiry_id` (`inquiry_id`),
  KEY `properties_id` (`property_id`),
  CONSTRAINT `property_notifictions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `websiteusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `property_notifictions` */

insert  into `property_notifictions`(`id`,`user_id`,`property_id`,`inquiry_id`,`date`) values 
(1,59,27378763548993,NULL,NULL),
(2,59,27378763548997,NULL,NULL),
(3,59,27378763548998,NULL,NULL),
(4,59,27378763548998,NULL,NULL),
(5,61,27378763548999,NULL,NULL),
(6,62,27378763549000,NULL,NULL),
(7,62,27378763549000,NULL,NULL),
(8,62,27378763549000,NULL,NULL),
(9,62,27378763549000,NULL,NULL),
(10,62,27378763549000,NULL,NULL),
(11,62,27378763549000,NULL,NULL),
(12,62,27378763549000,NULL,NULL),
(13,62,27378763549000,NULL,NULL),
(14,62,27378763549000,NULL,NULL),
(15,62,27378763549000,NULL,NULL),
(16,44,27378763549001,NULL,NULL),
(17,62,27378763549000,NULL,NULL),
(18,62,27378763549000,NULL,NULL),
(19,62,27378763549002,NULL,NULL),
(20,62,27378763549002,NULL,NULL),
(21,63,0,NULL,NULL),
(22,63,0,NULL,NULL);

/*Table structure for table `property_pics` */

DROP TABLE IF EXISTS `property_pics`;

CREATE TABLE `property_pics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` bigint(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `pic` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `property_pics` */

/*Table structure for table `reasearchreport_pics` */

DROP TABLE IF EXISTS `reasearchreport_pics`;

CREATE TABLE `reasearchreport_pics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `pic` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `reasearchreport_pics` */

insert  into `reasearchreport_pics`(`id`,`report_id`,`is_active`,`pic`) values 
(1,21,1,'upload/reportimages/5a003aaa87ecc.JPG'),
(2,22,1,'upload/reportimages/5a1c9f329e83e.jpg');

/*Table structure for table `research_reports` */

DROP TABLE IF EXISTS `research_reports`;

CREATE TABLE `research_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(255) NOT NULL,
  `type_of_project` int(11) NOT NULL,
  `plot_area` varchar(255) NOT NULL,
  `no_of_floors` int(11) NOT NULL,
  `doj1` varchar(255) NOT NULL,
  `doj2` varchar(255) NOT NULL,
  `land_holding` varchar(255) NOT NULL,
  `project_status` int(11) NOT NULL,
  `no_of_towers` int(11) NOT NULL,
  `unit_size` varchar(255) NOT NULL,
  `no_of_unit` varchar(255) NOT NULL,
  `area_livability` varchar(255) NOT NULL,
  `dentisy` varchar(255) NOT NULL,
  `carpet_area` varchar(255) NOT NULL,
  `about_project` text NOT NULL,
  `about_locality` text NOT NULL,
  `about_builder` text NOT NULL,
  `city` int(11) DEFAULT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `block` varchar(255) NOT NULL,
  `locality` varchar(255) NOT NULL,
  `city_data` varchar(255) NOT NULL,
  `state_data` varchar(255) NOT NULL,
  `country_data` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `unit` text NOT NULL,
  `reportpdf` varchar(255) NOT NULL,
  `report_price` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `research_reports` */

insert  into `research_reports`(`id`,`report_name`,`type_of_project`,`plot_area`,`no_of_floors`,`doj1`,`doj2`,`land_holding`,`project_status`,`no_of_towers`,`unit_size`,`no_of_unit`,`area_livability`,`dentisy`,`carpet_area`,`about_project`,`about_locality`,`about_builder`,`city`,`lat`,`lng`,`block`,`locality`,`city_data`,`state_data`,`country_data`,`pincode`,`unit`,`reportpdf`,`report_price`) values 
(17,'Aims Golf Avenue',1,'5 ',19,'06/01/2010','12/01/2016','Aims Promoters Pvt Ltd.',1,7,'1035 to 2350','750','1 to 2 years','150','68% of Super area','Aims Golf Avenue is a part of an integrated township, Aims Golf City conceptualized by Aims Group. The\r\nproject is located in the plush locale of Sector-75, Noida with close proximity to various residential and\r\ncommercial hubs. This project has 2BHK, 3BHK and 4BHK apartments with servant room along with modern\r\nday features.','Sector-75, Noida has emerged as one of the most prominent\r\nresidential hubs on account of systematic development of\r\ninfrastructure and of commercial real estate in the vicinity.\r\nSector-75 is situated right next to sector-50, which is considered as\r\na premium location because of the presence of high end housing\r\nprojects.','Aims Group is a diversified group with major\r\nstake in Real Estate Development. They\r\nbelieve in green urban solutions and support\r\nthe social, economic, environmental, cultural\r\nand educational health of the regions for\r\nwhich they work.',3,'','','','','','','','','{\"unittype\":[\"2BHK, 3BHK, 4BHK \"],\"totalunit\":[\"750\"],\"areabaseprice\":[\"4500\"]}','Aims Golf Avenue 1-final.pdf','0'),
(18,'Sudip test',1,'2000',5,'11/30/2017','02/24/2018','12/3',1,3,'1200','5','555','45','5455','Very good projects','as ds dfvxc xc c','asdw ',1,'','','','','','','','','{\"unittype\":[\"sq ft\"],\"totalunit\":[\"1200\"],\"areabaseprice\":[\"5000\"]}','DSC00402.JPG','1200'),
(19,'Sudip test',1,'2000',5,'11/30/2017','02/24/2018','12/3',1,3,'1200','5','555','45','5455','dssd asdas d','as ds ','asdw ',1,'','','','','','','','','{\"unittype\":[\"sq ft\"],\"totalunit\":[\"1200\"],\"areabaseprice\":[\"5000\"]}','DSC00402.JPG','1200'),
(20,'Sudip test',1,'2000',5,'11/30/2017','02/24/2018','12/3',1,3,'1200','5','555','45','5455','dssd asdas d','as ds ','asdw ',1,'','','','','','','','','{\"unittype\":[\"sq ft\"],\"totalunit\":[\"1200\"],\"areabaseprice\":[\"5000\"]}','DSC00402.JPG','1200'),
(21,'Sudip test',1,'2000',5,'11/30/2017','02/24/2018','12/3',1,3,'1200','5','555','45','5455','dssd asdas d','as ds ','asdw ',1,'','','','','','','','','{\"unittype\":[\"sq ft\"],\"totalunit\":[\"1200\"],\"areabaseprice\":[\"5000\"]}','DSC00402.JPG','1200'),
(22,'khjkjkh',1,'89',7,'11/29/2017','11/30/2017','798',1,1,'89','89','89','898','899','jhjgjh','kjhj','hkhkk',3,'','','','','','','','','{\"unittype\":[\"78\"],\"totalunit\":[\"8799\"],\"areabaseprice\":[\"878878\"]}','issues-fairpockets.xls','78878787');

/*Table structure for table `service_notifictions` */

DROP TABLE IF EXISTS `service_notifictions`;

CREATE TABLE `service_notifictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `service_notifictions` */

insert  into `service_notifictions`(`id`,`user_id`,`service`,`date`) values 
(1,46,'Buy/Rent Requirment Services','2017-07-20 14:35:50'),
(2,46,'Buy/Rent Requirment Services','2017-07-20 15:16:56'),
(3,46,'Buy/Rent Requirment Services','2017-07-22 14:47:02'),
(4,46,'Buy/Rent Requirment Services','2017-07-23 14:04:24'),
(5,46,'Buy/Rent Requirment Services','2017-07-23 14:14:59'),
(6,46,'Buy/Rent Requirment Services','2017-07-23 14:15:41'),
(7,46,'Buy/Rent Requirment Services','2017-07-23 16:42:57'),
(8,46,'Buy/Rent Requirment Services','2017-07-25 00:30:16'),
(9,46,'Buy/Rent Requirment Services','2017-07-25 00:40:00'),
(10,46,'Sell/Rent Requirement Services','2017-07-25 00:49:51'),
(11,46,'Buy/Rent Requirment Services','2017-07-25 11:12:01'),
(12,46,'Buy/Rent Requirment Services','2017-07-25 13:56:30'),
(13,46,'Buy/Rent Requirment Services','2017-07-25 13:57:27'),
(14,46,'Buy/Rent Requirment Services','2017-07-25 13:58:12'),
(15,46,'Buy/Rent Requirment Services','2017-07-25 14:02:10'),
(16,46,'Buy/Rent Requirment Services','2017-07-25 14:02:55'),
(17,46,'Sell/Rent Requirement Services','2017-07-25 14:15:30'),
(18,46,'Sell/Rent Requirement Services','2017-07-25 15:25:22'),
(19,46,'Sell/Rent Requirement Services','2017-07-25 15:27:43'),
(20,46,'Sell/Rent Requirement Services','2017-07-25 17:40:53'),
(21,46,'Sell/Rent Requirement Services','2017-07-26 18:01:40'),
(22,46,'Buy/Rent Requirment Services','2017-07-26 18:02:36'),
(23,46,'Buy/Rent Requirment Services','2017-09-01 16:48:06'),
(24,46,'Buy/Rent Requirment Services','2017-09-04 18:34:15'),
(25,46,'Buy/Rent Requirement Services','2017-09-27 00:09:51'),
(26,46,'Buy/Rent Requirement Services','2017-10-05 15:07:03'),
(27,46,'Buy/Rent Requirement Services','2017-10-05 15:15:03'),
(28,46,'Buy/Rent Requirement Services','2017-10-13 06:16:10');

/*Table structure for table `temp_properties_amenities` */

DROP TABLE IF EXISTS `temp_properties_amenities`;

CREATE TABLE `temp_properties_amenities` (
  `properties_id` int(11) NOT NULL,
  `amenities_name` varchar(90) DEFAULT NULL,
  `amenities_Value` varchar(90) DEFAULT NULL,
  `Unit` varchar(90) DEFAULT NULL,
  `Remarks` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `temp_properties_amenities` */

insert  into `temp_properties_amenities`(`properties_id`,`amenities_name`,`amenities_Value`,`Unit`,`Remarks`) values 
(1,'lift','1',NULL,NULL),
(1,'park','0',NULL,NULL),
(1,'staff','0',NULL,NULL),
(1,'parking','0',NULL,NULL),
(1,'waterstorage','0',NULL,NULL),
(1,'vaastu','0',NULL,NULL),
(1,'intercomm','1',NULL,NULL),
(1,'fire_alarm','1',NULL,NULL),
(2,'lift','0',NULL,NULL),
(2,'park','0',NULL,NULL),
(2,'staff','1',NULL,NULL),
(2,'parking','1',NULL,NULL),
(2,'waterstorage','0',NULL,NULL),
(2,'vaastu','0',NULL,NULL),
(2,'intercomm','0',NULL,NULL),
(2,'fire_alarm','0',NULL,NULL),
(3,'staff','1',NULL,NULL),
(3,'security_personal','0',NULL,NULL),
(3,'swimming','0',NULL,NULL),
(3,'rainwater','1',NULL,NULL),
(4,'staff','0',NULL,NULL),
(4,'waterstorage','0',NULL,NULL),
(4,'rainwater','0',NULL,NULL),
(5,'lift','0',NULL,NULL),
(5,'park','0',NULL,NULL),
(5,'staff','0',NULL,NULL),
(5,'parking','0',NULL,NULL),
(5,'waterstorage','0',NULL,NULL),
(5,'vaastu','0',NULL,NULL),
(5,'intercomm','0',NULL,NULL),
(5,'fire_alarm','0',NULL,NULL),
(8,'park','0',NULL,NULL),
(8,'staff','0',NULL,NULL),
(8,'parking','0',NULL,NULL),
(8,'waterstorage','0',NULL,NULL),
(8,'vaastu','0',NULL,NULL),
(8,'waste_disposal','0',NULL,NULL),
(8,'tarrace','0',NULL,NULL),
(8,'rainwater','0',NULL,NULL),
(13,'lift','0',NULL,NULL),
(13,'staff','0',NULL,NULL),
(13,'parking','0',NULL,NULL),
(13,'waterstorage','1',NULL,NULL),
(13,'fire_alarm','0',NULL,NULL),
(13,'waste_disposal','0',NULL,NULL),
(13,'security_personal','1',NULL,NULL),
(13,'goods_lift','1',NULL,NULL),
(13,'atm','1',NULL,NULL),
(13,'rainwater','0',NULL,NULL),
(14,'waste_disposal','0',NULL,NULL),
(14,'rainwater','0',NULL,NULL),
(15,'lift','0',NULL,NULL),
(15,'staff','0',NULL,NULL),
(15,'parking','0',NULL,NULL),
(15,'waterstorage','0',NULL,NULL),
(15,'fire_alarm','0',NULL,NULL),
(15,'waste_disposal','0',NULL,NULL),
(15,'security_personal','1',NULL,NULL),
(15,'goods_lift','0',NULL,NULL),
(15,'conroom','0',NULL,NULL),
(15,'atm','0',NULL,NULL),
(15,'ac','0',NULL,NULL),
(16,'lift','0',NULL,NULL),
(16,'staff','0',NULL,NULL),
(16,'parking','0',NULL,NULL),
(16,'waterstorage','0',NULL,NULL),
(16,'vaastu','1',NULL,NULL),
(16,'fire_alarm','0',NULL,NULL),
(16,'waste_disposal','0',NULL,NULL),
(16,'security_personal','1',NULL,NULL),
(17,'lift','0',NULL,NULL),
(17,'staff','0',NULL,NULL),
(17,'parking','0',NULL,NULL),
(17,'waterstorage','0',NULL,NULL),
(17,'vaastu','0',NULL,NULL),
(17,'fire_alarm','1',NULL,NULL),
(17,'waste_disposal','0',NULL,NULL),
(17,'security_personal','0',NULL,NULL),
(22,'lift','0',NULL,NULL),
(22,'park','1',NULL,NULL),
(22,'staff','1',NULL,NULL),
(22,'parking','1',NULL,NULL),
(22,'waterstorage','0',NULL,NULL),
(22,'vaastu','0',NULL,NULL),
(22,'intercomm','0',NULL,NULL),
(22,'fire_alarm','1',NULL,NULL),
(23,'lift','1',NULL,NULL),
(23,'park','0',NULL,NULL),
(23,'staff','0',NULL,NULL),
(23,'parking','0',NULL,NULL),
(23,'waterstorage','0',NULL,NULL),
(23,'vaastu','0',NULL,NULL),
(23,'intercomm','0',NULL,NULL),
(23,'fire_alarm','1',NULL,NULL),
(24,'lift','0',NULL,NULL),
(24,'park','1',NULL,NULL),
(24,'staff','0',NULL,NULL),
(24,'parking','0',NULL,NULL),
(24,'waterstorage','0',NULL,NULL),
(24,'vaastu','0',NULL,NULL),
(24,'intercomm','0',NULL,NULL),
(24,'fire_alarm','1',NULL,NULL),
(25,'lift','1',NULL,NULL),
(25,'park','0',NULL,NULL),
(25,'staff','0',NULL,NULL),
(25,'parking','0',NULL,NULL),
(25,'waterstorage','0',NULL,NULL),
(25,'vaastu','0',NULL,NULL),
(25,'intercomm','1',NULL,NULL),
(25,'fire_alarm','1',NULL,NULL),
(26,'lift','0',NULL,NULL),
(26,'staff','1',NULL,NULL),
(26,'parking','1',NULL,NULL),
(26,'waterstorage','0',NULL,NULL),
(26,'fire_alarm','0',NULL,NULL),
(26,'waste_disposal','0',NULL,NULL),
(26,'security_personal','0',NULL,NULL),
(26,'goods_lift','1',NULL,NULL),
(26,'atm','0',NULL,NULL),
(26,'rainwater','1',NULL,NULL),
(29,'lift','1',NULL,NULL),
(29,'park','1',NULL,NULL),
(29,'staff','1',NULL,NULL),
(29,'parking','1',NULL,NULL),
(29,'waterstorage','0',NULL,NULL),
(29,'vaastu','0',NULL,NULL),
(29,'intercomm','0',NULL,NULL),
(29,'fire_alarm','0',NULL,NULL),
(31,'lift','1',NULL,NULL),
(31,'park','0',NULL,NULL),
(31,'staff','1',NULL,NULL),
(31,'parking','1',NULL,NULL),
(31,'waterstorage','0',NULL,NULL),
(31,'vaastu','0',NULL,NULL),
(31,'intercomm','0',NULL,NULL),
(31,'fire_alarm','0',NULL,NULL),
(32,'lift','0',NULL,NULL),
(32,'park','0',NULL,NULL),
(32,'staff','0',NULL,NULL),
(32,'parking','0',NULL,NULL),
(32,'waterstorage','0',NULL,NULL),
(32,'vaastu','0',NULL,NULL),
(32,'intercomm','1',NULL,NULL),
(32,'fire_alarm','1',NULL,NULL),
(34,'lift','0',NULL,NULL),
(34,'park','0',NULL,NULL),
(34,'staff','1',NULL,NULL),
(34,'parking','1',NULL,NULL),
(34,'waterstorage','1',NULL,NULL),
(34,'vaastu','1',NULL,NULL),
(34,'intercomm','0',NULL,NULL),
(34,'fire_alarm','0',NULL,NULL),
(38,'lift','0',NULL,NULL),
(38,'staff','1',NULL,NULL),
(38,'parking','0',NULL,NULL),
(38,'waterstorage','0',NULL,NULL),
(38,'vaastu','0',NULL,NULL),
(38,'fire_alarm','0',NULL,NULL),
(38,'waste_disposal','1',NULL,NULL),
(38,'security_personal','0',NULL,NULL),
(39,'lift','0',NULL,NULL),
(39,'park','0',NULL,NULL),
(39,'staff','0',NULL,NULL),
(39,'parking','0',NULL,NULL),
(39,'waterstorage','0',NULL,NULL),
(39,'vaastu','0',NULL,NULL),
(39,'intercomm','0',NULL,NULL),
(39,'fire_alarm','0',NULL,NULL),
(40,'lift','1',NULL,NULL),
(40,'park','1',NULL,NULL),
(40,'staff','1',NULL,NULL),
(40,'parking','1',NULL,NULL),
(40,'waterstorage','0',NULL,NULL),
(40,'vaastu','1',NULL,NULL),
(40,'intercomm','1',NULL,NULL),
(40,'fire_alarm','1',NULL,NULL),
(41,'lift','1',NULL,NULL),
(41,'staff','1',NULL,NULL),
(41,'parking','1',NULL,NULL),
(41,'waterstorage','1',NULL,NULL),
(41,'fire_alarm','1',NULL,NULL),
(41,'waste_disposal','1',NULL,NULL),
(41,'security_personal','1',NULL,NULL),
(41,'goods_lift','1',NULL,NULL),
(41,'conroom','1',NULL,NULL),
(41,'atm','1',NULL,NULL),
(41,'ac','1',NULL,NULL),
(41,'internet','1',NULL,NULL),
(41,'food','1',NULL,NULL),
(42,'park','0',NULL,NULL),
(42,'staff','0',NULL,NULL),
(42,'parking','0',NULL,NULL),
(42,'waterstorage','0',NULL,NULL),
(42,'vaastu','0',NULL,NULL),
(42,'waste_disposal','0',NULL,NULL),
(42,'tarrace','1',NULL,NULL),
(42,'rainwater','1',NULL,NULL),
(45,'lift','0',NULL,NULL),
(45,'park','0',NULL,NULL),
(45,'staff','0',NULL,NULL),
(45,'parking','0',NULL,NULL),
(45,'waterstorage','0',NULL,NULL),
(45,'vaastu','0',NULL,NULL),
(45,'intercomm','0',NULL,NULL),
(45,'fire_alarm','0',NULL,NULL),
(50,'lift','1',NULL,NULL),
(50,'park','1',NULL,NULL),
(50,'staff','0',NULL,NULL),
(50,'parking','0',NULL,NULL),
(50,'waterstorage','0',NULL,NULL),
(50,'vaastu','0',NULL,NULL),
(50,'intercomm','0',NULL,NULL),
(50,'fire_alarm','0',NULL,NULL),
(52,'waste_disposal','1',NULL,NULL),
(52,'rainwater','1',NULL,NULL),
(57,'lift','0',NULL,NULL),
(57,'park','0',NULL,NULL),
(57,'staff','0',NULL,NULL),
(57,'parking','0',NULL,NULL),
(57,'waterstorage','0',NULL,NULL),
(57,'vaastu','0',NULL,NULL),
(57,'intercomm','1',NULL,NULL),
(57,'fire_alarm','1',NULL,NULL),
(61,'staff','0',NULL,NULL),
(61,'security_personal','0',NULL,NULL),
(61,'swimming','0',NULL,NULL),
(61,'rainwater','0',NULL,NULL),
(62,'lift','0',NULL,NULL),
(62,'park','0',NULL,NULL),
(62,'staff','0',NULL,NULL),
(62,'parking','0',NULL,NULL),
(62,'waterstorage','0',NULL,NULL),
(62,'vaastu','0',NULL,NULL),
(62,'intercomm','0',NULL,NULL),
(62,'fire_alarm','0',NULL,NULL),
(63,'lift','1',NULL,NULL),
(63,'park','1',NULL,NULL),
(63,'staff','1',NULL,NULL),
(63,'parking','1',NULL,NULL),
(63,'waterstorage','1',NULL,NULL),
(63,'vaastu','1',NULL,NULL),
(63,'intercomm','1',NULL,NULL),
(63,'fire_alarm','1',NULL,NULL),
(69,'lift','1',NULL,NULL),
(69,'park','1',NULL,NULL),
(69,'staff','1',NULL,NULL),
(69,'parking','1',NULL,NULL),
(69,'waterstorage','1',NULL,NULL),
(69,'vaastu','1',NULL,NULL),
(69,'intercomm','1',NULL,NULL),
(69,'fire_alarm','1',NULL,NULL),
(71,'lift','1',NULL,NULL),
(71,'park','1',NULL,NULL),
(71,'staff','1',NULL,NULL),
(71,'parking','1',NULL,NULL),
(71,'waterstorage','0',NULL,NULL),
(71,'vaastu','0',NULL,NULL),
(71,'intercomm','1',NULL,NULL),
(71,'fire_alarm','0',NULL,NULL),
(72,'park','1',NULL,NULL),
(72,'staff','1',NULL,NULL),
(72,'parking','0',NULL,NULL),
(72,'waterstorage','1',NULL,NULL),
(72,'vaastu','1',NULL,NULL),
(72,'waste_disposal','1',NULL,NULL),
(72,'tarrace','1',NULL,NULL),
(72,'rainwater','0',NULL,NULL),
(73,'lift','1',NULL,NULL),
(73,'park','1',NULL,NULL),
(73,'staff','1',NULL,NULL),
(73,'parking','0',NULL,NULL),
(73,'waterstorage','1',NULL,NULL),
(73,'vaastu','0',NULL,NULL),
(73,'intercomm','1',NULL,NULL),
(73,'fire_alarm','0',NULL,NULL),
(75,'waste_disposal','0',NULL,NULL),
(75,'rainwater','0',NULL,NULL),
(77,'park','0',NULL,NULL),
(77,'staff','1',NULL,NULL),
(77,'parking','1',NULL,NULL),
(77,'waterstorage','1',NULL,NULL),
(77,'vaastu','0',NULL,NULL),
(77,'waste_disposal','0',NULL,NULL),
(77,'tarrace','0',NULL,NULL),
(77,'rainwater','0',NULL,NULL);

/*Table structure for table `temp_properties_features` */

DROP TABLE IF EXISTS `temp_properties_features`;

CREATE TABLE `temp_properties_features` (
  `properties_id` int(11) NOT NULL,
  `features_name` varchar(90) DEFAULT NULL,
  `features_Value` varchar(90) DEFAULT NULL,
  `Unit` varchar(90) DEFAULT NULL,
  `Remarks` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `temp_properties_features` */

insert  into `temp_properties_features`(`properties_id`,`features_name`,`features_Value`,`Unit`,`Remarks`) values 
(1,'sbua','678',NULL,NULL),
(1,'build_up_area','66',NULL,NULL),
(1,'carpet_area','66',NULL,NULL),
(1,'bedrooms','1',NULL,NULL),
(1,'bathrooms','1',NULL,NULL),
(1,'Configure','1BHK',NULL,NULL),
(1,'balconies','1',NULL,NULL),
(1,'age_of_property','65',NULL,NULL),
(1,'total_floors','15',NULL,NULL),
(1,'on_floor','17',NULL,NULL),
(1,'res_parking_2','1',NULL,NULL),
(1,'res_parking_3','1',NULL,NULL),
(1,'no_covered','2',NULL,NULL),
(1,'no_open','3',NULL,NULL),
(1,'other_room1','0',NULL,NULL),
(1,'other_room2','1',NULL,NULL),
(1,'other_room3','0',NULL,NULL),
(1,'other_room4','0',NULL,NULL),
(1,'property_description','gggggggggggggg',NULL,NULL),
(1,'superbuilt_area_insq','678',NULL,NULL),
(1,'build_up_area_insq','594',NULL,NULL),
(1,'carpet_area_insq','71879.28',NULL,NULL),
(1,'furnishing','furnished',NULL,NULL),
(1,'width_of_fac_road','341',NULL,NULL),
(1,'watersource1','1',NULL,NULL),
(1,'watersource2','0',NULL,NULL),
(1,'overlook1','0',NULL,NULL),
(1,'overlook2','0',NULL,NULL),
(1,'overlook3','0',NULL,NULL),
(1,'overlook4','0',NULL,NULL),
(2,'sbua','99',NULL,NULL),
(2,'build_up_area','50',NULL,NULL),
(2,'carpet_area','45',NULL,NULL),
(2,'bedrooms','2',NULL,NULL),
(2,'bathrooms','3',NULL,NULL),
(2,'Configure','6BHK',NULL,NULL),
(2,'balconies','2',NULL,NULL),
(2,'age_of_property','1',NULL,NULL),
(2,'total_floors','7',NULL,NULL),
(2,'on_floor','basement',NULL,NULL),
(2,'res_parking_2','1',NULL,NULL),
(2,'res_parking_3','1',NULL,NULL),
(2,'no_covered','1',NULL,NULL),
(2,'no_open','2',NULL,NULL),
(2,'other_room1','1',NULL,NULL),
(2,'other_room2','1',NULL,NULL),
(2,'other_room3','0',NULL,NULL),
(2,'other_room4','0',NULL,NULL),
(2,'property_description','Apartment',NULL,NULL),
(2,'superbuilt_area_insq','99',NULL,NULL),
(2,'build_up_area_insq','50',NULL,NULL),
(2,'carpet_area_insq','45',NULL,NULL),
(3,'furnishing','unfurnished',NULL,NULL),
(3,'society_type','gated society',NULL,NULL),
(3,NULL,NULL,NULL,NULL),
(3,'width_of_fac_road','34',NULL,NULL),
(3,'watersource1','0',NULL,NULL),
(3,'watersource2','1',NULL,NULL),
(3,'build_up_area','1111',NULL,NULL),
(3,'carpet_area','2222',NULL,NULL),
(3,'plot_area','3333',NULL,NULL),
(3,'bedrooms','4',NULL,NULL),
(3,'bathrooms','3',NULL,NULL),
(3,'Configure','2BHK',NULL,NULL),
(3,'balconies','2',NULL,NULL),
(3,'age_of_property','12',NULL,NULL),
(3,'in_floor','7',NULL,NULL),
(3,'other_room1','0',NULL,NULL),
(3,'other_room2','0',NULL,NULL),
(3,'other_room3','1',NULL,NULL),
(3,'other_room4','1',NULL,NULL),
(3,'property_description','Farm House ',NULL,NULL),
(3,'plot_area_insq','3333',NULL,NULL),
(3,'build_up_area_insq','1111',NULL,NULL),
(3,'carpet_area_insq','2222',NULL,NULL),
(4,'society_type','gated society',NULL,NULL),
(4,NULL,NULL,NULL,NULL),
(4,'watersource1','1',NULL,NULL),
(4,'watersource2','1',NULL,NULL),
(4,'boundary_wall','2',NULL,NULL),
(4,'plot_area','45',NULL,NULL),
(4,'length_plot','23',NULL,NULL),
(4,'width_plot','24',NULL,NULL),
(4,'allowed_floor','5',NULL,NULL),
(4,'property_description','Residential Land ',NULL,NULL),
(4,'plot_area_insq','32401.8',NULL,NULL),
(5,'power_backup','partial',NULL,NULL),
(5,'furnishing','semi-furnished',NULL,NULL),
(5,'width_of_fac_road','23',NULL,NULL),
(5,'watersource1','1',NULL,NULL),
(5,'watersource2','0',NULL,NULL),
(5,'sbua','988989',NULL,NULL),
(5,'build_up_area','787878',NULL,NULL),
(5,'carpet_area','655656',NULL,NULL),
(5,'bedrooms','2',NULL,NULL),
(5,'bathrooms','2',NULL,NULL),
(5,'Configure','2BHK',NULL,NULL),
(5,'balconies','3',NULL,NULL),
(5,'age_of_property','56',NULL,NULL),
(5,'in_floor','4',NULL,NULL),
(5,'res_parking_1','1',NULL,NULL),
(5,'no_covered',NULL,NULL,NULL),
(5,'no_open',NULL,NULL,NULL),
(5,'other_room1','1',NULL,NULL),
(5,'other_room2','0',NULL,NULL),
(5,'other_room3','0',NULL,NULL),
(5,'other_room4','1',NULL,NULL),
(5,'property_description','Independent Builder Floor ',NULL,NULL),
(5,'superbuilt_area_insq','988989',NULL,NULL),
(5,'build_up_area_insq','787878',NULL,NULL),
(5,'carpet_area_insq','655656',NULL,NULL),
(6,'build_up_area','12',NULL,NULL),
(6,'carpet_area','12',NULL,NULL),
(6,'plot_area','12',NULL,NULL),
(6,'bedrooms','1',NULL,NULL),
(6,'bathrooms','1',NULL,NULL),
(6,'Configure','None',NULL,NULL),
(6,'balconies','2',NULL,NULL),
(6,'age_of_property',NULL,NULL,NULL),
(6,'in_floor','4',NULL,NULL),
(6,'res_parking_3','1',NULL,NULL),
(6,'no_covered',NULL,NULL,NULL),
(6,'no_open','12',NULL,NULL),
(6,'other_room1','0',NULL,NULL),
(6,'other_room2','0',NULL,NULL),
(6,'other_room3','1',NULL,NULL),
(6,'other_room4','0',NULL,NULL),
(6,'property_description','Independent House',NULL,NULL),
(6,'plot_area_insq','12',NULL,NULL),
(6,'build_up_area_insq','12',NULL,NULL),
(6,'carpet_area_insq','12',NULL,NULL),
(7,NULL,NULL,NULL,NULL),
(8,'null',NULL,NULL,NULL),
(9,NULL,NULL,NULL,NULL),
(10,'sbua','1234',NULL,NULL),
(10,'build_up_area','1234',NULL,NULL),
(10,'carpet_area','1234',NULL,NULL),
(10,'washrooms','7',NULL,NULL),
(10,'age_of_property','1',NULL,NULL),
(10,'total_floors','14',NULL,NULL),
(10,'res_parking_1','1',NULL,NULL),
(10,'no_covered',NULL,NULL,NULL),
(10,'no_open',NULL,NULL,NULL),
(10,'property_description','Commercial office space',NULL,NULL),
(10,'superbuilt_area_insq','1234',NULL,NULL),
(10,'build_up_area_insq','1234',NULL,NULL),
(10,'carpet_area_insq','1234',NULL,NULL),
(11,'sbua','6',NULL,NULL),
(11,'build_up_area','6',NULL,NULL),
(11,'carpet_area','6',NULL,NULL),
(11,'washrooms','2',NULL,NULL),
(11,'age_of_property','6',NULL,NULL),
(11,'total_floors','6',NULL,NULL),
(11,'on_floor',NULL,NULL,NULL),
(11,'res_parking_1','1',NULL,NULL),
(11,'no_covered','6',NULL,NULL),
(11,'no_open',NULL,NULL,NULL),
(11,'property_description','Office in IT Park ',NULL,NULL),
(11,'superbuilt_area_insq','6',NULL,NULL),
(11,'build_up_area_insq','6',NULL,NULL),
(11,'carpet_area_insq','6',NULL,NULL),
(12,'build_up_area','4545',NULL,NULL),
(12,'plot_area','4545',NULL,NULL),
(12,'washrooms','9+',NULL,NULL),
(12,'balconies','4',NULL,NULL),
(12,'age_of_property',NULL,NULL,NULL),
(12,'in_floor','4',NULL,NULL),
(12,'property_description','WareHouse',NULL,NULL),
(12,'plot_area_insq','4545',NULL,NULL),
(12,'build_up_area_insq','4545',NULL,NULL),
(13,'type_of_flooring','Granite',NULL,NULL),
(13,'power_backup','none',NULL,NULL),
(13,'facing','ne',NULL,NULL),
(13,'width_of_fac_road','33',NULL,NULL),
(13,'build_up_area','77777',NULL,NULL),
(13,'carpet_area','77777777',NULL,NULL),
(13,'plot_area','77777777',NULL,NULL),
(13,'washrooms','Shared',NULL,NULL),
(13,'balconies','1',NULL,NULL),
(13,'in_floor','12',NULL,NULL),
(13,'res_parking_2','1',NULL,NULL),
(13,'no_covered','14',NULL,NULL),
(13,'no_open',NULL,NULL,NULL),
(13,'property_description','Commercial shop\\\\/showroom ',NULL,NULL),
(13,'plot_area_insq','77777777',NULL,NULL),
(13,'build_up_area_insq','77777',NULL,NULL),
(13,'carpet_area_insq','77777777',NULL,NULL),
(14,'width_of_fac_road','23',NULL,NULL),
(14,'watersource1','1',NULL,NULL),
(14,'watersource2','1',NULL,NULL),
(14,'boundary_wall','1',NULL,NULL),
(14,'plot_area','24',NULL,NULL),
(14,'length_plot','56',NULL,NULL),
(14,'width_plot','56',NULL,NULL),
(14,'property_description','Commercial\\\\/industrial\\\\/agr icultural Land ',NULL,NULL),
(14,'plot_area_insq','57609.12',NULL,NULL),
(15,'power_backup','partial',NULL,NULL),
(15,'furnishing','furnished',NULL,NULL),
(15,'facing','North-West',NULL,NULL),
(15,'width_of_fac_road','34',NULL,NULL),
(15,'sbua','341',NULL,NULL),
(15,'build_up_area','441',NULL,NULL),
(15,'carpet_area',NULL,NULL,NULL),
(15,'washrooms','9+',NULL,NULL),
(15,'age_of_property',NULL,NULL,NULL),
(15,'total_floors','14',NULL,NULL),
(15,'on_floor','12',NULL,NULL),
(15,'res_parking_1','1',NULL,NULL),
(15,'no_covered',NULL,NULL,NULL),
(15,'no_open',NULL,NULL,NULL),
(15,'property_description','tffff',NULL,NULL),
(15,'superbuilt_area_insq','341',NULL,NULL),
(15,'build_up_area_insq','441',NULL,NULL),
(16,'build_up_area','1234',NULL,NULL),
(16,'carpet_area','1234',NULL,NULL),
(16,'plot_area','1234',NULL,NULL),
(16,'washrooms','Shared',NULL,NULL),
(16,'balconies','1',NULL,NULL),
(16,'age_of_property','12',NULL,NULL),
(16,'in_floor','1',NULL,NULL),
(16,'property_description','Factory\\\\/warehouse ',NULL,NULL),
(16,'plot_area_insq','1234',NULL,NULL),
(16,'build_up_area_insq','1234',NULL,NULL),
(16,'carpet_area_insq','1234',NULL,NULL),
(16,'type_of_flooring','Wood',NULL,NULL),
(16,'facing','North-East',NULL,NULL),
(16,'width_of_fac_road','56',NULL,NULL),
(16,'watersource1','0',NULL,NULL),
(16,'watersource2','1',NULL,NULL),
(17,'build_up_area',NULL,NULL,NULL),
(17,'carpet_area','441',NULL,NULL),
(17,'plot_area','45',NULL,NULL),
(17,'washrooms','9',NULL,NULL),
(17,'balconies','2',NULL,NULL),
(17,'age_of_property',NULL,NULL,NULL),
(17,'in_floor','13',NULL,NULL),
(17,'property_description','cccccccc',NULL,NULL),
(17,'plot_area_insq','45',NULL,NULL),
(17,'carpet_area_insq','441',NULL,NULL),
(17,'type_of_flooring','vitrified',NULL,NULL),
(17,'facing','North',NULL,NULL),
(17,'width_of_fac_road',NULL,NULL,NULL),
(17,'watersource1','0',NULL,NULL),
(17,'watersource2','0',NULL,NULL),
(18,'build_up_area',NULL,NULL,NULL),
(18,'carpet_area',NULL,NULL,NULL),
(18,'plot_area','1234',NULL,NULL),
(18,'bedrooms','2',NULL,NULL),
(18,'bathrooms','2',NULL,NULL),
(18,'Configure','PentHouse',NULL,NULL),
(18,'balconies','2',NULL,NULL),
(18,'age_of_property',NULL,NULL,NULL),
(18,'in_floor',NULL,NULL,NULL),
(18,'other_room1','0',NULL,NULL),
(18,'other_room2','0',NULL,NULL),
(18,'other_room3','1',NULL,NULL),
(18,'other_room4','0',NULL,NULL),
(18,'property_description','farm house',NULL,NULL),
(18,'plot_area_insq','1234',NULL,NULL),
(19,'plot_area','1234',NULL,NULL),
(19,'length_plot',NULL,NULL,NULL),
(19,'width_plot',NULL,NULL,NULL),
(19,'allowed_floor',NULL,NULL,NULL),
(19,'property_description','land land',NULL,NULL),
(19,'plot_area_insq','1234',NULL,NULL),
(20,NULL,NULL,NULL,NULL),
(21,NULL,NULL,NULL,NULL),
(22,'sbua','341',NULL,NULL),
(22,'build_up_area','441',NULL,NULL),
(22,'carpet_area','441',NULL,NULL),
(22,'bedrooms','3',NULL,NULL),
(22,'bathrooms','2',NULL,NULL),
(22,'Configure','Shared',NULL,NULL),
(22,'balconies','1',NULL,NULL),
(22,'age_of_property','45',NULL,NULL),
(22,'total_floors','5',NULL,NULL),
(22,'on_floor','basement',NULL,NULL),
(22,'res_parking_2','1',NULL,NULL),
(22,'res_parking_3','1',NULL,NULL),
(22,'no_covered','5',NULL,NULL),
(22,'no_open','6',NULL,NULL),
(22,'other_room1','1',NULL,NULL),
(22,'other_room2','1',NULL,NULL),
(22,'other_room3','0',NULL,NULL),
(22,'other_room4','0',NULL,NULL),
(22,'property_description','dddddd',NULL,NULL),
(22,'superbuilt_area_insq','341',NULL,NULL),
(22,'build_up_area_insq','441',NULL,NULL),
(22,'carpet_area_insq','441',NULL,NULL),
(22,'furnishing','furnished',NULL,NULL),
(22,'width_of_fac_road','34',NULL,NULL),
(22,'watersource1','0',NULL,NULL),
(22,'watersource2','1',NULL,NULL),
(22,'overlook1','0',NULL,NULL),
(22,'overlook2','1',NULL,NULL),
(22,'overlook3','0',NULL,NULL),
(22,'overlook4','0',NULL,NULL),
(22,'bachelor_all','2',NULL,NULL),
(22,'pet_all','1',NULL,NULL),
(22,'non_veg','1',NULL,NULL),
(23,'sbua','341',NULL,NULL),
(23,'build_up_area','441',NULL,NULL),
(23,'carpet_area',NULL,NULL,NULL),
(23,'bedrooms','1',NULL,NULL),
(23,'bathrooms','2',NULL,NULL),
(23,'Configure','2BHK',NULL,NULL),
(23,'balconies','2',NULL,NULL),
(23,'age_of_property',NULL,NULL,NULL),
(23,'total_floors','14',NULL,NULL),
(23,'on_floor','Ground',NULL,NULL),
(23,'no_covered',NULL,NULL,NULL),
(23,'no_open',NULL,NULL,NULL),
(23,'other_room1','0',NULL,NULL),
(23,'other_room2','0',NULL,NULL),
(23,'other_room3','0',NULL,NULL),
(23,'other_room4','0',NULL,NULL),
(23,'property_description','ddddddd',NULL,NULL),
(23,'superbuilt_area_insq','341',NULL,NULL),
(23,'build_up_area_insq','441',NULL,NULL),
(23,'furnishing','furnished',NULL,NULL),
(23,'width_of_fac_road','455',NULL,NULL),
(23,'watersource1','0',NULL,NULL),
(23,'watersource2','0',NULL,NULL),
(23,'overlook1','0',NULL,NULL),
(23,'overlook2','0',NULL,NULL),
(23,'overlook3','0',NULL,NULL),
(23,'overlook4','0',NULL,NULL),
(23,'bachelor_all','2',NULL,NULL),
(23,'pet_all','2',NULL,NULL),
(23,'non_veg','2',NULL,NULL),
(24,'sbua','341',NULL,NULL),
(24,'build_up_area','43',NULL,NULL),
(24,'carpet_area','43',NULL,NULL),
(24,'bedrooms','1',NULL,NULL),
(24,'bathrooms','1',NULL,NULL),
(24,'Configure','None',NULL,NULL),
(24,'balconies','2',NULL,NULL),
(24,'age_of_property','45',NULL,NULL),
(24,'total_floors','15',NULL,NULL),
(24,'on_floor','Ground',NULL,NULL),
(24,'res_parking_2','1',NULL,NULL),
(24,'res_parking_3','1',NULL,NULL),
(24,'no_covered','12',NULL,NULL),
(24,'no_open','12',NULL,NULL),
(24,'other_room1','0',NULL,NULL),
(24,'other_room2','1',NULL,NULL),
(24,'other_room3','1',NULL,NULL),
(24,'other_room4','0',NULL,NULL),
(24,'property_description','rrrrrrrr',NULL,NULL),
(24,'superbuilt_area_insq','341',NULL,NULL),
(24,'build_up_area_insq','43',NULL,NULL),
(24,'carpet_area_insq','387',NULL,NULL),
(24,'furnishing','semi-furnished',NULL,NULL),
(24,'width_of_fac_road','34',NULL,NULL),
(24,'watersource1','1',NULL,NULL),
(24,'watersource2','1',NULL,NULL),
(24,'overlook1','1',NULL,NULL),
(24,'overlook2','0',NULL,NULL),
(24,'overlook3','0',NULL,NULL),
(24,'overlook4','0',NULL,NULL),
(24,'bachelor_all','1',NULL,NULL),
(25,'sbua','2345',NULL,NULL),
(25,'build_up_area',NULL,NULL,NULL),
(25,'carpet_area',NULL,NULL,NULL),
(25,'bedrooms','2',NULL,NULL),
(25,'bathrooms','1',NULL,NULL),
(25,'Configure','3BHK',NULL,NULL),
(25,'balconies','2',NULL,NULL),
(25,'age_of_property',NULL,NULL,NULL),
(25,'total_floors','16',NULL,NULL),
(25,'on_floor','14',NULL,NULL),
(25,'res_parking_2','1',NULL,NULL),
(25,'res_parking_3','1',NULL,NULL),
(25,'no_covered','3',NULL,NULL),
(25,'no_open','3',NULL,NULL),
(25,'other_room1','1',NULL,NULL),
(25,'other_room2','1',NULL,NULL),
(25,'other_room3','1',NULL,NULL),
(25,'other_room4','1',NULL,NULL),
(25,'property_description','Studio apartment',NULL,NULL),
(25,'superbuilt_area_insq','1055250',NULL,NULL),
(26,'build_up_area',NULL,NULL,NULL),
(26,'carpet_area',NULL,NULL,NULL),
(26,'plot_area','9990',NULL,NULL),
(26,'washrooms','None',NULL,NULL),
(26,'balconies','1',NULL,NULL),
(26,'in_floor',NULL,NULL,NULL),
(26,'res_parking_1','1',NULL,NULL),
(26,'no_covered',NULL,NULL,NULL),
(26,'no_open',NULL,NULL,NULL),
(26,'property_description','Commercial Shop',NULL,NULL),
(26,'plot_area_insq','9990',NULL,NULL),
(26,'type_of_flooring',NULL,NULL,NULL),
(26,'power_backup','full',NULL,NULL),
(26,'facing','South-East',NULL,NULL),
(26,'width_of_fac_road',NULL,NULL,NULL),
(27,NULL,NULL,NULL,NULL),
(28,NULL,NULL,NULL,NULL),
(29,'sbua','1000',NULL,NULL),
(29,'build_up_area','900',NULL,NULL),
(29,'carpet_area',NULL,NULL,NULL),
(29,'bedrooms','2',NULL,NULL),
(29,'bathrooms','2',NULL,NULL),
(29,'Configure','2BHK',NULL,NULL),
(29,'balconies','2',NULL,NULL),
(29,'age_of_property',NULL,NULL,NULL),
(29,'total_floors',NULL,NULL,NULL),
(29,'on_floor',NULL,NULL,NULL),
(29,'res_parking_1','1',NULL,NULL),
(29,'no_covered',NULL,NULL,NULL),
(29,'no_open',NULL,NULL,NULL),
(29,'other_room1','1',NULL,NULL),
(29,'other_room2','0',NULL,NULL),
(29,'other_room3','0',NULL,NULL),
(29,'other_room4','0',NULL,NULL),
(29,'property_description','kjhkjhz kh xlkjx ',NULL,NULL),
(29,'superbuilt_area_insq','1000',NULL,NULL),
(29,'build_up_area_insq','900',NULL,NULL),
(29,'furnishing','furnished',NULL,NULL),
(29,'width_of_fac_road',NULL,NULL,NULL),
(29,'watersource1','1',NULL,NULL),
(29,'watersource2','0',NULL,NULL),
(29,'overlook1','1',NULL,NULL),
(29,'overlook2','0',NULL,NULL),
(29,'overlook3','0',NULL,NULL),
(29,'overlook4','0',NULL,NULL),
(30,NULL,NULL,NULL,NULL),
(31,'sbua','5656',NULL,NULL),
(31,'build_up_area',NULL,NULL,NULL),
(31,'carpet_area',NULL,NULL,NULL),
(31,'bedrooms','2',NULL,NULL),
(31,'bathrooms','1',NULL,NULL),
(31,'Configure','1RK',NULL,NULL),
(31,'balconies','1',NULL,NULL),
(31,'age_of_property',NULL,NULL,NULL),
(31,'total_floors',NULL,NULL,NULL),
(31,'on_floor',NULL,NULL,NULL),
(31,'res_parking_2','1',NULL,NULL),
(31,'no_covered','3',NULL,NULL),
(31,'no_open',NULL,NULL,NULL),
(31,'other_room1','0',NULL,NULL),
(31,'other_room2','0',NULL,NULL),
(31,'other_room3','1',NULL,NULL),
(31,'other_room4','1',NULL,NULL),
(31,'property_description','studio aparatment',NULL,NULL),
(31,'superbuilt_area_insq','5656',NULL,NULL),
(31,'furnishing',NULL,NULL,NULL),
(31,'width_of_fac_road','78',NULL,NULL),
(31,'watersource1','0',NULL,NULL),
(31,'watersource2','0',NULL,NULL),
(31,'overlook1','0',NULL,NULL),
(31,'overlook2','1',NULL,NULL),
(31,'overlook3','1',NULL,NULL),
(31,'overlook4','0',NULL,NULL),
(32,'sbua','1',NULL,NULL),
(32,'build_up_area','1',NULL,NULL),
(32,'carpet_area','1',NULL,NULL),
(32,'bedrooms','1',NULL,NULL),
(32,'bathrooms','1',NULL,NULL),
(32,'Configure','6BHK',NULL,NULL),
(32,'balconies','3',NULL,NULL),
(32,'age_of_property',NULL,NULL,NULL),
(32,'total_floors',NULL,NULL,NULL),
(32,'on_floor',NULL,NULL,NULL),
(32,'no_covered',NULL,NULL,NULL),
(32,'no_open',NULL,NULL,NULL),
(32,'other_room1','0',NULL,NULL),
(32,'other_room2','0',NULL,NULL),
(32,'other_room3','0',NULL,NULL),
(32,'other_room4','0',NULL,NULL),
(32,'property_description','apartment on rent',NULL,NULL),
(32,'superbuilt_area_insq','1',NULL,NULL),
(32,'build_up_area_insq','1',NULL,NULL),
(32,'carpet_area_insq','1',NULL,NULL),
(33,NULL,NULL,NULL,NULL),
(34,'sbua','9',NULL,NULL),
(34,'build_up_area',NULL,NULL,NULL),
(34,'carpet_area',NULL,NULL,NULL),
(34,'bedrooms','4',NULL,NULL),
(34,'bathrooms','3',NULL,NULL),
(34,'Configure','PentHouse',NULL,NULL),
(34,'balconies','3',NULL,NULL),
(34,'age_of_property',NULL,NULL,NULL),
(34,'in_floor',NULL,NULL,NULL),
(34,'res_parking_2','1',NULL,NULL),
(34,'res_parking_3','1',NULL,NULL),
(34,'no_covered','5',NULL,NULL),
(34,'no_open','12',NULL,NULL),
(34,'other_room1','0',NULL,NULL),
(34,'other_room2','0',NULL,NULL),
(34,'other_room3','1',NULL,NULL),
(34,'other_room4','1',NULL,NULL),
(34,'property_description','Independent builder floor',NULL,NULL),
(34,'superbuilt_area_insq','9',NULL,NULL),
(34,'power_backup',NULL,NULL,NULL),
(34,'furnishing',NULL,NULL,NULL),
(34,'width_of_fac_road',NULL,NULL,NULL),
(34,'watersource1','0',NULL,NULL),
(34,'watersource2','0',NULL,NULL),
(35,NULL,NULL,NULL,NULL),
(36,NULL,NULL,NULL,NULL),
(37,NULL,NULL,NULL,NULL),
(38,'build_up_area','1234',NULL,NULL),
(38,'carpet_area','5678',NULL,NULL),
(38,'plot_area','91234',NULL,NULL),
(38,'washrooms','9+',NULL,NULL),
(38,'balconies','4',NULL,NULL),
(38,'age_of_property','23',NULL,NULL),
(38,'in_floor','14',NULL,NULL),
(38,'property_description','Factory',NULL,NULL),
(38,'plot_area_insq','91234',NULL,NULL),
(38,'build_up_area_insq','1234',NULL,NULL),
(38,'carpet_area_insq','5678',NULL,NULL),
(38,'type_of_flooring','IPSFinish',NULL,NULL),
(38,'facing','South-East',NULL,NULL),
(38,'width_of_fac_road','78.90',NULL,NULL),
(38,'watersource1','0',NULL,NULL),
(38,'watersource2','1',NULL,NULL),
(39,'sbua','33',NULL,NULL),
(39,'build_up_area',NULL,NULL,NULL),
(39,'carpet_area',NULL,NULL,NULL),
(39,'bedrooms','4',NULL,NULL),
(39,'bathrooms','3',NULL,NULL),
(39,'Configure','Shared',NULL,NULL),
(39,'balconies','2',NULL,NULL),
(39,'age_of_property',NULL,NULL,NULL),
(39,'total_floors',NULL,NULL,NULL),
(39,'on_floor',NULL,NULL,NULL),
(39,'no_covered',NULL,NULL,NULL),
(39,'no_open',NULL,NULL,NULL),
(39,'other_room1','0',NULL,NULL),
(39,'other_room2','0',NULL,NULL),
(39,'other_room3','0',NULL,NULL),
(39,'other_room4','0',NULL,NULL),
(39,'property_description','jjjjjjjjjjjj',NULL,NULL),
(39,'superbuilt_area_insq','33',NULL,NULL),
(39,'furnishing',NULL,NULL,NULL),
(39,'width_of_fac_road',NULL,NULL,NULL),
(39,'watersource1','0',NULL,NULL),
(39,'watersource2','0',NULL,NULL),
(39,'overlook1','0',NULL,NULL),
(39,'overlook2','0',NULL,NULL),
(39,'overlook3','0',NULL,NULL),
(39,'overlook4','0',NULL,NULL),
(40,'sbua','4',NULL,NULL),
(40,'build_up_area','3',NULL,NULL),
(40,'carpet_area','2',NULL,NULL),
(40,'bedrooms','4',NULL,NULL),
(40,'bathrooms','2',NULL,NULL),
(40,'Configure','3BHK',NULL,NULL),
(40,'balconies','2',NULL,NULL),
(40,'age_of_property',NULL,NULL,NULL),
(40,'total_floors',NULL,NULL,NULL),
(40,'on_floor','14',NULL,NULL),
(40,'res_parking_1','1',NULL,NULL),
(40,'no_covered',NULL,NULL,NULL),
(40,'no_open',NULL,NULL,NULL),
(40,'other_room1','1',NULL,NULL),
(40,'other_room2','0',NULL,NULL),
(40,'other_room3','1',NULL,NULL),
(40,'other_room4','0',NULL,NULL),
(40,'property_description','New Apartment',NULL,NULL),
(40,'superbuilt_area_insq','4',NULL,NULL),
(40,'build_up_area_insq','3',NULL,NULL),
(40,'carpet_area_insq','2',NULL,NULL),
(41,'sbua','34',NULL,NULL),
(41,'build_up_area','34',NULL,NULL),
(41,'carpet_area','34',NULL,NULL),
(41,'washrooms','Shared',NULL,NULL),
(41,'age_of_property','12',NULL,NULL),
(41,'total_floors','15',NULL,NULL),
(41,'on_floor','Lower Ground',NULL,NULL),
(41,'res_parking_2','1',NULL,NULL),
(41,'no_covered','16',NULL,NULL),
(41,'no_open',NULL,NULL,NULL),
(41,'property_description','Office in IT Park',NULL,NULL),
(41,'superbuilt_area_insq','34',NULL,NULL),
(41,'build_up_area_insq','34',NULL,NULL),
(41,'carpet_area_insq','34',NULL,NULL),
(41,'power_backup','partial',NULL,NULL),
(41,'furnishing','semi-furnished',NULL,NULL),
(41,'width_of_fac_road','56',NULL,NULL),
(42,'build_up_area',NULL,NULL,NULL),
(42,'carpet_area',NULL,NULL,NULL),
(42,'plot_area','1',NULL,NULL),
(42,'bedrooms','2',NULL,NULL),
(42,'bathrooms','1',NULL,NULL),
(42,'Configure','Studio',NULL,NULL),
(42,'balconies','1',NULL,NULL),
(42,'age_of_property',NULL,NULL,NULL),
(42,'in_floor',NULL,NULL,NULL),
(42,'no_covered',NULL,NULL,NULL),
(42,'no_open',NULL,NULL,NULL),
(42,'other_room1','0',NULL,NULL),
(42,'other_room2','0',NULL,NULL),
(42,'other_room3','0',NULL,NULL),
(42,'other_room4','0',NULL,NULL),
(42,'property_description','Villa',NULL,NULL),
(42,'plot_area_insq','1',NULL,NULL),
(42,'power_backup','full',NULL,NULL),
(42,'furnishing',NULL,NULL,NULL),
(42,'width_of_fac_road','67',NULL,NULL),
(42,'watersource1','0',NULL,NULL),
(42,'watersource2','1',NULL,NULL),
(43,NULL,NULL,NULL,NULL),
(44,NULL,NULL,NULL,NULL),
(45,'sbua','1000',NULL,NULL),
(45,'build_up_area',NULL,NULL,NULL),
(45,'carpet_area',NULL,NULL,NULL),
(45,'bedrooms','2',NULL,NULL),
(45,'bathrooms','2',NULL,NULL),
(45,'Configure','2BHK',NULL,NULL),
(45,'balconies','3',NULL,NULL),
(45,'age_of_property',NULL,NULL,NULL),
(45,'total_floors',NULL,NULL,NULL),
(45,'on_floor',NULL,NULL,NULL),
(45,'res_parking_1','1',NULL,NULL),
(45,'no_covered',NULL,NULL,NULL),
(45,'no_open',NULL,NULL,NULL),
(45,'other_room1','0',NULL,NULL),
(45,'other_room2','0',NULL,NULL),
(45,'other_room3','0',NULL,NULL),
(45,'other_room4','0',NULL,NULL),
(45,'property_description','kjhjkh kjkj khkj kjh',NULL,NULL),
(45,'superbuilt_area_insq','1000',NULL,NULL),
(45,'furnishing',NULL,NULL,NULL),
(45,'width_of_fac_road',NULL,NULL,NULL),
(45,'watersource1','0',NULL,NULL),
(45,'watersource2','0',NULL,NULL),
(45,'overlook1','0',NULL,NULL),
(45,'overlook2','0',NULL,NULL),
(45,'overlook3','0',NULL,NULL),
(45,'overlook4','0',NULL,NULL),
(46,'sbua','1000',NULL,NULL),
(46,'build_up_area',NULL,NULL,NULL),
(46,'carpet_area',NULL,NULL,NULL),
(46,'bedrooms','2',NULL,NULL),
(46,'bathrooms','2',NULL,NULL),
(46,'Configure','2BHK',NULL,NULL),
(46,'balconies','2',NULL,NULL),
(46,'age_of_property',NULL,NULL,NULL),
(46,'total_floors',NULL,NULL,NULL),
(46,'on_floor',NULL,NULL,NULL),
(46,'res_parking_3','1',NULL,NULL),
(46,'no_covered',NULL,NULL,NULL),
(46,'no_open','4',NULL,NULL),
(46,'other_room1','0',NULL,NULL),
(46,'other_room2','0',NULL,NULL),
(46,'other_room3','0',NULL,NULL),
(46,'other_room4','0',NULL,NULL),
(46,'property_description','lkjlkj khkw wkhw wkjhw wkjhhw wkjhw wkjhw wkjhw wkjhw ',NULL,NULL),
(46,'superbuilt_area_insq','1000',NULL,NULL),
(47,NULL,NULL,NULL,NULL),
(48,NULL,NULL,NULL,NULL),
(49,NULL,NULL,NULL,NULL),
(50,'sbua','33',NULL,NULL),
(50,'build_up_area','43',NULL,NULL),
(50,'carpet_area','441',NULL,NULL),
(50,'bedrooms','2',NULL,NULL),
(50,'bathrooms','4',NULL,NULL),
(50,'Configure','Shared',NULL,NULL),
(50,'balconies','2',NULL,NULL),
(50,'age_of_property','34',NULL,NULL),
(50,'total_floors','14',NULL,NULL),
(50,'on_floor','Lower Ground',NULL,NULL),
(50,'res_parking_1','1',NULL,NULL),
(50,'no_covered',NULL,NULL,NULL),
(50,'no_open',NULL,NULL,NULL),
(50,'other_room1','0',NULL,NULL),
(50,'other_room2','1',NULL,NULL),
(50,'other_room3','1',NULL,NULL),
(50,'other_room4','0',NULL,NULL),
(50,'property_description','ddddddd',NULL,NULL),
(50,'superbuilt_area_insq','33',NULL,NULL),
(50,'build_up_area_insq','43',NULL,NULL),
(50,'carpet_area_insq','441',NULL,NULL),
(50,'furnishing',NULL,NULL,NULL),
(50,'width_of_fac_road','34',NULL,NULL),
(50,'watersource1','0',NULL,NULL),
(50,'watersource2','0',NULL,NULL),
(50,'overlook1','0',NULL,NULL),
(50,'overlook2','0',NULL,NULL),
(50,'overlook3','0',NULL,NULL),
(50,'overlook4','0',NULL,NULL),
(51,NULL,NULL,NULL,NULL),
(52,'plot_area','23',NULL,NULL),
(52,'length_plot',NULL,NULL,NULL),
(52,'width_plot',NULL,NULL,NULL),
(52,'property_description','agriculture land',NULL,NULL),
(52,'plot_area_insq','23',NULL,NULL),
(52,'width_of_fac_road','23',NULL,NULL),
(52,'watersource1','1',NULL,NULL),
(52,'watersource2','1',NULL,NULL),
(52,'boundary_wall','2',NULL,NULL),
(53,NULL,NULL,NULL,NULL),
(54,NULL,NULL,NULL,NULL),
(55,NULL,NULL,NULL,NULL),
(56,NULL,NULL,NULL,NULL),
(57,'sbua','341',NULL,NULL),
(57,'build_up_area','3',NULL,NULL),
(57,'carpet_area','441',NULL,NULL),
(57,'bedrooms','2',NULL,NULL),
(57,'bathrooms','3',NULL,NULL),
(57,'Configure','None',NULL,NULL),
(57,'balconies','2',NULL,NULL),
(57,'age_of_property','45',NULL,NULL),
(57,'total_floors','6',NULL,NULL),
(57,'on_floor','Ground',NULL,NULL),
(57,'res_parking_1','1',NULL,NULL),
(57,'no_covered',NULL,NULL,NULL),
(57,'no_open',NULL,NULL,NULL),
(57,'other_room1','0',NULL,NULL),
(57,'other_room2','1',NULL,NULL),
(57,'other_room3','0',NULL,NULL),
(57,'other_room4','0',NULL,NULL),
(57,'property_description','jjjjjjjjjjjj',NULL,NULL),
(57,'superbuilt_area_insq','341',NULL,NULL),
(57,'build_up_area_insq','3',NULL,NULL),
(57,'carpet_area_insq','441',NULL,NULL),
(57,'furnishing','furnished',NULL,NULL),
(57,'width_of_fac_road','455',NULL,NULL),
(57,'watersource1','1',NULL,NULL),
(57,'watersource2','1',NULL,NULL),
(57,'overlook1','1',NULL,NULL),
(57,'overlook2','1',NULL,NULL),
(57,'overlook3','0',NULL,NULL),
(57,'overlook4','0',NULL,NULL),
(58,NULL,NULL,NULL,NULL),
(59,'sbua','341',NULL,NULL),
(59,'build_up_area','441',NULL,NULL),
(59,'carpet_area','441',NULL,NULL),
(59,'bedrooms','1',NULL,NULL),
(59,'bathrooms','1',NULL,NULL),
(59,'Configure','Shared',NULL,NULL),
(59,'balconies','3',NULL,NULL),
(59,'age_of_property','45',NULL,NULL),
(59,'in_floor','15',NULL,NULL),
(59,'no_covered',NULL,NULL,NULL),
(59,'no_open',NULL,NULL,NULL),
(59,'other_room1','0',NULL,NULL),
(59,'other_room2','0',NULL,NULL),
(59,'other_room3','0',NULL,NULL),
(59,'other_room4','0',NULL,NULL),
(59,'property_description','sssssssss',NULL,NULL),
(59,'superbuilt_area_insq','341',NULL,NULL),
(59,'build_up_area_insq','441',NULL,NULL),
(59,'carpet_area_insq','441',NULL,NULL),
(60,'sbua','341',NULL,NULL),
(60,'build_up_area','43',NULL,NULL),
(60,'carpet_area','43',NULL,NULL),
(60,'bedrooms','3',NULL,NULL),
(60,'bathrooms','1',NULL,NULL),
(60,'Configure','None',NULL,NULL),
(60,'balconies','2',NULL,NULL),
(60,'age_of_property','45',NULL,NULL),
(60,'total_floors','3',NULL,NULL),
(60,'on_floor','12',NULL,NULL),
(60,'no_covered',NULL,NULL,NULL),
(60,'no_open',NULL,NULL,NULL),
(60,'other_room1','0',NULL,NULL),
(60,'other_room2','0',NULL,NULL),
(60,'other_room3','0',NULL,NULL),
(60,'other_room4','0',NULL,NULL),
(60,'property_description','hhhhhhh',NULL,NULL),
(60,'superbuilt_area_insq','341',NULL,NULL),
(60,'build_up_area_insq','43',NULL,NULL),
(60,'carpet_area_insq','43',NULL,NULL),
(61,'build_up_area',NULL,NULL,NULL),
(61,'carpet_area',NULL,NULL,NULL),
(61,'plot_area','12',NULL,NULL),
(61,'bedrooms','2',NULL,NULL),
(61,'bathrooms','2',NULL,NULL),
(61,'Configure','6BHK',NULL,NULL),
(61,'balconies','3',NULL,NULL),
(61,'age_of_property',NULL,NULL,NULL),
(61,'in_floor',NULL,NULL,NULL),
(61,'other_room1','0',NULL,NULL),
(61,'other_room2','0',NULL,NULL),
(61,'other_room3','0',NULL,NULL),
(61,'other_room4','0',NULL,NULL),
(61,'property_description','farm house',NULL,NULL),
(61,'plot_area_insq','12',NULL,NULL),
(61,'furnishing',NULL,NULL,NULL),
(61,'society_type',NULL,NULL,NULL),
(61,'width_of_fac_road',NULL,NULL,NULL),
(61,'watersource1','0',NULL,NULL),
(61,'watersource2','0',NULL,NULL),
(62,'sbua','4444',NULL,NULL),
(62,'build_up_area',NULL,NULL,NULL),
(62,'carpet_area',NULL,NULL,NULL),
(62,'bedrooms','2',NULL,NULL),
(62,'bathrooms','2',NULL,NULL),
(62,'Configure','1BHK',NULL,NULL),
(62,'balconies','2',NULL,NULL),
(62,'age_of_property',NULL,NULL,NULL),
(62,'total_floors',NULL,NULL,NULL),
(62,'on_floor',NULL,NULL,NULL),
(62,'no_covered',NULL,NULL,NULL),
(62,'no_open',NULL,NULL,NULL),
(62,'other_room1','0',NULL,NULL),
(62,'other_room2','0',NULL,NULL),
(62,'other_room3','0',NULL,NULL),
(62,'other_room4','0',NULL,NULL),
(62,'property_description','iuykh kjhkjh kjhkjh',NULL,NULL),
(62,'superbuilt_area_insq','4444',NULL,NULL),
(62,'furnishing',NULL,NULL,NULL),
(62,'width_of_fac_road',NULL,NULL,NULL),
(62,'watersource1','0',NULL,NULL),
(62,'watersource2','0',NULL,NULL),
(62,'overlook1','0',NULL,NULL),
(62,'overlook2','0',NULL,NULL),
(62,'overlook3','0',NULL,NULL),
(62,'overlook4','0',NULL,NULL),
(63,'sbua','65654',NULL,NULL),
(63,'build_up_area','6544',NULL,NULL),
(63,'carpet_area','5435',NULL,NULL),
(63,'bedrooms','2',NULL,NULL),
(63,'bathrooms','3',NULL,NULL),
(63,'Configure','3BHK',NULL,NULL),
(63,'balconies','2',NULL,NULL),
(63,'age_of_property',NULL,NULL,NULL),
(63,'total_floors','4',NULL,NULL),
(63,'on_floor','6',NULL,NULL),
(63,'res_parking_2','1',NULL,NULL),
(63,'no_covered','5',NULL,NULL),
(63,'no_open',NULL,NULL,NULL),
(63,'other_room1','1',NULL,NULL),
(63,'other_room2','0',NULL,NULL),
(63,'other_room3','0',NULL,NULL),
(63,'other_room4','0',NULL,NULL),
(63,'property_description','hgfhgf hfhggfhg hgfhgfhgfhhf jhgjhhgj jgjhgjhg',NULL,NULL),
(63,'superbuilt_area_insq','65654',NULL,NULL),
(63,'build_up_area_insq','6544',NULL,NULL),
(63,'carpet_area_insq','5435',NULL,NULL),
(63,'furnishing','furnished',NULL,NULL),
(63,'width_of_fac_road','654',NULL,NULL),
(63,'watersource1','1',NULL,NULL),
(63,'watersource2','1',NULL,NULL),
(63,'overlook1','1',NULL,NULL),
(63,'overlook2','1',NULL,NULL),
(63,'overlook3','0',NULL,NULL),
(63,'overlook4','0',NULL,NULL),
(63,'bachelor_all','1',NULL,NULL),
(63,'pet_all','1',NULL,NULL),
(64,NULL,NULL,NULL,NULL),
(65,NULL,NULL,NULL,NULL),
(66,'sbua','2000',NULL,NULL),
(66,'build_up_area',NULL,NULL,NULL),
(66,'carpet_area',NULL,NULL,NULL),
(66,'bedrooms','2',NULL,NULL),
(66,'bathrooms','2',NULL,NULL),
(66,'Configure','1RK',NULL,NULL),
(66,'balconies','2',NULL,NULL),
(66,'age_of_property',NULL,NULL,NULL),
(66,'total_floors','40+',NULL,NULL),
(66,'on_floor','40+',NULL,NULL),
(66,'res_parking_1','1',NULL,NULL),
(66,'no_covered',NULL,NULL,NULL),
(66,'no_open',NULL,NULL,NULL),
(66,'other_room1','1',NULL,NULL),
(66,'other_room2','0',NULL,NULL),
(66,'other_room3','0',NULL,NULL),
(66,'other_room4','0',NULL,NULL),
(66,'property_description','bdsfbsdfbsd',NULL,NULL),
(66,'superbuilt_area_insq','2000',NULL,NULL),
(67,NULL,NULL,NULL,NULL),
(68,NULL,NULL,NULL,NULL),
(69,'sbua','25000',NULL,NULL),
(69,'build_up_area','2500',NULL,NULL),
(69,'carpet_area','2300',NULL,NULL),
(69,'bedrooms','2',NULL,NULL),
(69,'bathrooms','2',NULL,NULL),
(69,'Configure','2BHK',NULL,NULL),
(69,'balconies','2',NULL,NULL),
(69,'age_of_property','23',NULL,NULL),
(69,'total_floors','9',NULL,NULL),
(69,'on_floor','3',NULL,NULL),
(69,'res_parking_1','1',NULL,NULL),
(69,'no_covered',NULL,NULL,NULL),
(69,'no_open',NULL,NULL,NULL),
(69,'other_room1','1',NULL,NULL),
(69,'other_room2','0',NULL,NULL),
(69,'other_room3','0',NULL,NULL),
(69,'other_room4','0',NULL,NULL),
(69,'property_description','gwergwergwer',NULL,NULL),
(69,'superbuilt_area_insq','25000',NULL,NULL),
(69,'build_up_area_insq','2500',NULL,NULL),
(69,'carpet_area_insq','2300',NULL,NULL),
(69,'furnishing',NULL,NULL,NULL),
(69,'width_of_fac_road',NULL,NULL,NULL),
(69,'watersource1','0',NULL,NULL),
(69,'watersource2','0',NULL,NULL),
(69,'overlook1','1',NULL,NULL),
(69,'overlook2','1',NULL,NULL),
(69,'overlook3','0',NULL,NULL),
(69,'overlook4','0',NULL,NULL),
(69,'bachelor_all','1',NULL,NULL),
(69,'pet_all','2',NULL,NULL),
(69,'non_veg','1',NULL,NULL),
(70,NULL,NULL,NULL,NULL),
(71,'sbua','1400',NULL,NULL),
(71,'build_up_area','1200',NULL,NULL),
(71,'carpet_area','900',NULL,NULL),
(71,'bedrooms','3',NULL,NULL),
(71,'bathrooms','2',NULL,NULL),
(71,'Configure','3BHK',NULL,NULL),
(71,'balconies','2',NULL,NULL),
(71,'age_of_property','5',NULL,NULL),
(71,'total_floors','4',NULL,NULL),
(71,'on_floor','3',NULL,NULL),
(71,'res_parking_2','1',NULL,NULL),
(71,'no_covered','1',NULL,NULL),
(71,'no_open',NULL,NULL,NULL),
(71,'other_room1','0',NULL,NULL),
(71,'other_room2','1',NULL,NULL),
(71,'other_room3','0',NULL,NULL),
(71,'other_room4','0',NULL,NULL),
(71,'property_description','3BHK flat nearby park',NULL,NULL),
(71,' 60 feet road',NULL,NULL,NULL),
(71,'superbuilt_area_insq','1400',NULL,NULL),
(71,'build_up_area_insq','1200',NULL,NULL),
(71,'carpet_area_insq','900',NULL,NULL),
(71,'furnishing','unfurnished',NULL,NULL),
(71,'width_of_fac_road','50',NULL,NULL),
(71,'watersource1','1',NULL,NULL),
(71,'watersource2','0',NULL,NULL),
(71,'overlook1','1',NULL,NULL),
(71,'overlook2','1',NULL,NULL),
(71,'overlook3','0',NULL,NULL),
(71,'overlook4','0',NULL,NULL),
(71,'bachelor_all','1',NULL,NULL),
(71,'pet_all','2',NULL,NULL),
(71,'non_veg','1',NULL,NULL),
(72,'build_up_area','2000',NULL,NULL),
(72,'carpet_area','1700',NULL,NULL),
(72,'plot_area','1400',NULL,NULL),
(72,'bedrooms','4',NULL,NULL),
(72,'bathrooms','3',NULL,NULL),
(72,'Configure','4BHK',NULL,NULL),
(72,'balconies','2',NULL,NULL),
(72,'age_of_property','2',NULL,NULL),
(72,'in_floor','2',NULL,NULL),
(72,'res_parking_2','1',NULL,NULL),
(72,'res_parking_3','1',NULL,NULL),
(72,'no_covered','3',NULL,NULL),
(72,'no_open','2',NULL,NULL),
(72,'other_room1','0',NULL,NULL),
(72,'other_room2','1',NULL,NULL),
(72,'other_room3','1',NULL,NULL),
(72,'other_room4','0',NULL,NULL),
(72,'property_description','4BHK house',NULL,NULL),
(72,'plot_area_insq','1400',NULL,NULL),
(72,'build_up_area_insq','2000',NULL,NULL),
(72,'carpet_area_insq','1700',NULL,NULL),
(72,'power_backup','full',NULL,NULL),
(72,'furnishing','unfurnished',NULL,NULL),
(72,'width_of_fac_road','100',NULL,NULL),
(72,'watersource1','1',NULL,NULL),
(72,'watersource2','0',NULL,NULL),
(73,'sbua','1500',NULL,NULL),
(73,'build_up_area','1300',NULL,NULL),
(73,'carpet_area','1100',NULL,NULL),
(73,'bedrooms','2',NULL,NULL),
(73,'bathrooms','2',NULL,NULL),
(73,'Configure','2BHK',NULL,NULL),
(73,'balconies','2',NULL,NULL),
(73,'age_of_property','3',NULL,NULL),
(73,'total_floors','10',NULL,NULL),
(73,'on_floor','2',NULL,NULL),
(73,'res_parking_3','1',NULL,NULL),
(73,'no_covered',NULL,NULL,NULL),
(73,'no_open','1',NULL,NULL),
(73,'other_room1','0',NULL,NULL),
(73,'other_room2','0',NULL,NULL),
(73,'other_room3','1',NULL,NULL),
(73,'other_room4','0',NULL,NULL),
(73,'property_description','2BHK flat ready to move it',NULL,NULL),
(73,'superbuilt_area_insq','1500',NULL,NULL),
(73,'build_up_area_insq','1300',NULL,NULL),
(73,'carpet_area_insq','1100',NULL,NULL),
(73,'furnishing','furnished',NULL,NULL),
(73,'width_of_fac_road','200',NULL,NULL),
(73,'watersource1','1',NULL,NULL),
(73,'watersource2','1',NULL,NULL),
(73,'overlook1','1',NULL,NULL),
(73,'overlook2','1',NULL,NULL),
(73,'overlook3','1',NULL,NULL),
(73,'overlook4','1',NULL,NULL),
(73,'bachelor_all','2',NULL,NULL),
(73,'pet_all','2',NULL,NULL),
(73,'non_veg','1',NULL,NULL),
(74,NULL,NULL,NULL,NULL),
(75,'plot_area','12',NULL,NULL),
(75,'length_plot','12',NULL,NULL),
(75,'width_plot',NULL,NULL,NULL),
(75,'property_description','12121212',NULL,NULL),
(75,'plot_area_insq','12',NULL,NULL),
(75,'width_of_fac_road',NULL,NULL,NULL),
(75,'watersource1','0',NULL,NULL),
(75,'watersource2','0',NULL,NULL),
(76,NULL,NULL,NULL,NULL),
(77,'build_up_area','25000',NULL,NULL),
(77,'carpet_area','20000',NULL,NULL),
(77,'plot_area','30000',NULL,NULL),
(77,'bedrooms','2',NULL,NULL),
(77,'bathrooms','1',NULL,NULL),
(77,'Configure','2BHK',NULL,NULL),
(77,'balconies','1',NULL,NULL),
(77,'age_of_property','10',NULL,NULL),
(77,'in_floor','4',NULL,NULL),
(77,'res_parking_3','1',NULL,NULL),
(77,'no_covered',NULL,NULL,NULL),
(77,'no_open','3',NULL,NULL),
(77,'other_room1','0',NULL,NULL),
(77,'other_room2','1',NULL,NULL),
(77,'other_room3','0',NULL,NULL),
(77,'other_room4','0',NULL,NULL),
(77,'property_description','Other description',NULL,NULL),
(77,'plot_area_insq','30000',NULL,NULL),
(77,'build_up_area_insq','25000',NULL,NULL),
(77,'carpet_area_insq','20000',NULL,NULL),
(77,'power_backup','none',NULL,NULL),
(77,'furnishing','furnished',NULL,NULL),
(77,'width_of_fac_road','20',NULL,NULL),
(77,'watersource1','1',NULL,NULL),
(77,'watersource2','0',NULL,NULL),
(78,NULL,NULL,NULL,NULL);

/*Table structure for table `uploaded_property_details` */

DROP TABLE IF EXISTS `uploaded_property_details`;

CREATE TABLE `uploaded_property_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) DEFAULT NULL,
  `aggrement_type` varchar(255) DEFAULT NULL,
  `residential_type` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `builder_name` varchar(255) DEFAULT NULL,
  `sublocality_1` varchar(255) DEFAULT NULL,
  `sublocality_2` varchar(255) DEFAULT NULL,
  `super_build_up_area` varchar(255) DEFAULT NULL,
  `build_up_area` varchar(255) DEFAULT NULL,
  `carpet_area` varchar(255) DEFAULT NULL,
  `bedrooms` varchar(255) DEFAULT NULL,
  `bathrooms` varchar(255) DEFAULT NULL,
  `balconies` varchar(255) DEFAULT NULL,
  `add_other_room_information` varchar(255) DEFAULT NULL,
  `total_floors` varchar(255) DEFAULT NULL,
  `property_to_floor` varchar(255) DEFAULT NULL,
  `no_of_covered_parking` varchar(255) DEFAULT NULL,
  `covered` varchar(255) DEFAULT NULL,
  `open` varchar(255) DEFAULT NULL,
  `offer_price_all_inclusive` varchar(255) DEFAULT NULL,
  `offer_price_per_sqft` varchar(255) DEFAULT NULL,
  `market_price_for_this_property_per_sqft` varchar(255) DEFAULT NULL,
  `offer_price_for_alive_asset` varchar(255) DEFAULT NULL,
  `offer_price_per_sqft_for_aliveasset` varchar(255) DEFAULT NULL,
  `market_or_fair_price_of_property` varchar(255) DEFAULT NULL,
  `delta` varchar(255) DEFAULT NULL,
  `per_sqft_price_for_alive_asset` varchar(255) DEFAULT NULL,
  `maintenance_annual_1_time_per_unit_per_month` varchar(255) DEFAULT NULL,
  `property_age` varchar(255) DEFAULT NULL,
  `availability` varchar(255) DEFAULT NULL,
  `possession_by` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `ownership` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `lifts` varchar(255) DEFAULT NULL,
  `park` varchar(255) DEFAULT NULL,
  `maintenance_staff` varchar(255) DEFAULT NULL,
  `visitor_parking` varchar(255) DEFAULT NULL,
  `water_storage` varchar(255) DEFAULT NULL,
  `vasstu_compliant` varchar(255) DEFAULT NULL,
  `intercom_facility` varchar(255) DEFAULT NULL,
  `fire_alarm` varchar(255) DEFAULT NULL,
  `centrally_air_conditioned` varchar(255) DEFAULT NULL,
  `piped_gas` varchar(255) DEFAULT NULL,
  `water_purifier` varchar(255) DEFAULT NULL,
  `internet_connectivity` varchar(255) DEFAULT NULL,
  `swimming_pool` varchar(255) DEFAULT NULL,
  `community` varchar(255) DEFAULT NULL,
  `fitness_centre` varchar(255) DEFAULT NULL,
  `security_personnel` varchar(255) DEFAULT NULL,
  `shopping_center` varchar(255) DEFAULT NULL,
  `rain_water_harvesting` varchar(255) DEFAULT NULL,
  `bank_attached_property` varchar(255) DEFAULT NULL,
  `power_backup` varchar(255) DEFAULT NULL,
  `weter_source` varchar(255) DEFAULT NULL,
  `facing` varchar(255) DEFAULT NULL,
  `width_of_facing_road` varchar(255) DEFAULT NULL,
  `type_of_flooring` varchar(255) DEFAULT NULL,
  `furnishing` varchar(255) DEFAULT NULL,
  `in_a_gated_society_description` varchar(1000) DEFAULT NULL,
  `corner_property_description` varchar(1000) DEFAULT NULL,
  `city1` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `mobile_number_1` varchar(255) DEFAULT NULL,
  `mobile_number_2` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `landline_1` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mob_no` varchar(255) DEFAULT NULL,
  `price_quoted` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `name1` varchar(255) DEFAULT NULL,
  `mob_no1` varchar(255) DEFAULT NULL,
  `price_quoted1` varchar(255) DEFAULT NULL,
  `broker_2` varchar(255) DEFAULT NULL,
  `remarks1` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `link` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=latin1;

/*Data for the table `uploaded_property_details` */

insert  into `uploaded_property_details`(`id`,`user_type`,`aggrement_type`,`residential_type`,`city`,`project_name`,`builder_name`,`sublocality_1`,`sublocality_2`,`super_build_up_area`,`build_up_area`,`carpet_area`,`bedrooms`,`bathrooms`,`balconies`,`add_other_room_information`,`total_floors`,`property_to_floor`,`no_of_covered_parking`,`covered`,`open`,`offer_price_all_inclusive`,`offer_price_per_sqft`,`market_price_for_this_property_per_sqft`,`offer_price_for_alive_asset`,`offer_price_per_sqft_for_aliveasset`,`market_or_fair_price_of_property`,`delta`,`per_sqft_price_for_alive_asset`,`maintenance_annual_1_time_per_unit_per_month`,`property_age`,`availability`,`possession_by`,`transaction_type`,`ownership`,`images`,`lifts`,`park`,`maintenance_staff`,`visitor_parking`,`water_storage`,`vasstu_compliant`,`intercom_facility`,`fire_alarm`,`centrally_air_conditioned`,`piped_gas`,`water_purifier`,`internet_connectivity`,`swimming_pool`,`community`,`fitness_centre`,`security_personnel`,`shopping_center`,`rain_water_harvesting`,`bank_attached_property`,`power_backup`,`weter_source`,`facing`,`width_of_facing_road`,`type_of_flooring`,`furnishing`,`in_a_gated_society_description`,`corner_property_description`,`city1`,`full_name`,`mobile_number_1`,`mobile_number_2`,`email_id`,`landline_1`,`name`,`mob_no`,`price_quoted`,`remarks`,`name1`,`mob_no1`,`price_quoted1`,`broker_2`,`remarks1`,`status`,`link`) values 
(1,'Owner','Sell','Apartment','Gurgaon','DLF New Town Heights 2',NULL,'Sector 86',NULL,'250',NULL,NULL,'1','1',NULL,NULL,'22','1',NULL,NULL,NULL,'9Lac','3600','4292','900000','3600','4175','575',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 86','Ajay Kumar','9990456687',NULL,'Ringing',NULL,'Vinod Pardhan','9896355540','4000 to 4500rup/sqft',NULL,NULL,NULL,'4000 nto 4200',NULL,'As per the confirmation ready to post property on alive asset final price is 9Lac',NULL,'http://www.99acres.com/studio-apartment-flat-for-sale-in-dlf-new-town-heights-2-sector-86-gurgaon-250-sq-ft-r4-spid-R16534971?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc1MDUgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgMzk3NDAjNCMgfCA='),
(2,'Owner','Sell','Apartment','Gurgaon','CHD Avenue 71',NULL,'Sector 71',NULL,'2350',NULL,NULL,'4','4',NULL,NULL,'15','15',NULL,NULL,NULL,'1.30Cr','5532','5822','13000000','5532','5650','118.0851063829787',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 71','Priti Joshi','9650400259',NULL,'Pritamj1991@gmail.com',NULL,'Rajesh Aggarwal/Aadhar Homes','9811750130','5500 to 5800rup/sqft',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','5500 to 5800rup/sqft',NULL,'As per the confirmation he is broker ready to post property on alive asset final price is 1.55CrCr',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2350-sq-ft-r1-spid-I13644807?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDYxNDUgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNDkwNiAjMyN8ICA='),
(3,'Owner','Sell','Apartment','Gurgaon','CHD Avenue 71',NULL,'Sector 71',NULL,'2193',NULL,NULL,'4','4','3','Servant Room','15','4',NULL,NULL,NULL,'1.25Cr','5699','5822','12500000','5700','5650','-50','5600',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Yes','Yes',NULL,NULL,NULL,'Full','Municipal Corporation','West',NULL,'Vitrified','Semifurnished','Yes','Yes','Sector 71','Pankaj Kapoor','9958384646','9711158129','Panku_98@me.com',NULL,'Rajesh Aggarwal/Aadhar Homes','9811750130','5500 to 5800rup/sqft',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','5500 to 5800rup/sqft',NULL,'As per the confirmation he is broker ready to post property on alive asset final price Is 5600rup/sqft',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2193-sq-ft-spid-H28826091?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDYxNDUgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNDkwNiAjMyN8ICA='),
(4,'Owner','Sell','Independent House','Gurgaon','Ansal API Esencia',NULL,'Sector 67',NULL,'2649','2400',NULL,'3','3','3',NULL,'3','Ground',NULL,NULL,NULL,'1.35Cr','5096','6375','13000000','4908','5125','217.4877312193285',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Full',NULL,'North-East',NULL,'Vitrified','Semifurnished','Yes','Yes','Sector 67','Maneesh','7798981399',NULL,'maneeshdakru@gmail.com',NULL,'Sachin Chawla/Shiv Shakti properties','9818577899','5000 to 5500rup/sqft',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','4800 to 5200rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com',NULL,'http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-ansal-api-esencia-sector-67-gurgaon-2649-sq-ft-r5-spid-U19620563?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc0ODkgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgMTUxMDQjNCMgfCA='),
(5,'Owner','Sell','Apartment','Gurgaon','Raheja Sampada',NULL,'Sector-92',NULL,'1370',NULL,NULL,'3','2','3',NULL,'15','9',NULL,NULL,NULL,'5900000','4307','4250','5900000','4307','4310','3.430656934306171','5620',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector-92','Rajeev Goyal','8130119227',NULL,'Goyal.rajeev9@gmail.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4000 to 4200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-raheja-sampada-sector-92-gurgaon-1370-sq-ft-r1-spid-F22802783?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(6,'Owner','Sell','Apartment','Gurgaon','Ireo The Corridors',NULL,'Sector 67 A',NULL,'1300',NULL,NULL,'2','1','2',NULL,'19','1',NULL,NULL,NULL,'1Cr','7692','8542','10000000','7692','8150','457.6923076923076','7000',NULL,NULL,'Ready to Move','Immediate','Resale','Freehold',NULL,'Yes','Yes',NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 67 A','Yashpal Narang','9811446434',NULL,'Ypnagarn@yahoo.co.in',NULL,'Aditya Kakar/3CA Inc','9910035800/9250404636','7000 to 7500rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','7200 to 7500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 7000rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-ireo-the-corridors-sector-67-a-gurgaon-1300-sq-ft-r4-spid-M24742635?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(7,'Owner','Sell','Apartment','Gurgaon','Ireo The Corridors',NULL,'Sector 67 A',NULL,'1727',NULL,NULL,'3','3','2',NULL,'19','8',NULL,'1 Covered',NULL,'1.4Cr','8107','8542','13700000','7933','8150','217.1685002895192','8800',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes',NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 67 A','Akhil Gandhi','9811173159','9810019712','AKGrajsonsindia.com',NULL,'Aditya Kakar/3CA Inc','9910035800/9250404636','7000 to 7500rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','7200 to 7500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 8800rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ireo-the-corridors-sector-67-a-gurgaon-1727-sq-ft-r2-spid-I26816349?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(8,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR One Phase I',NULL,'Sector 84',NULL,'1076',NULL,NULL,'2','2',NULL,NULL,'21','11',NULL,NULL,NULL,'4300000','3997','3995','4300000','3996','4066.666666666667','70.38413878562551',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 95','Mahesh','7838386075','9810249765','Maheshkumar2017@gmail.com',NULL,'Saurabh Dadhich/NGLC Realtech Pvt Ltd','9818225352/01244384354','4000 to 4200rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','4000rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 43Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-one-phase-1-sector-95-gurgaon-1076-sq-ft-r2-spid-I26999729?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(9,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR One Phase I',NULL,'Sector 95','D 1004,Sector 95 Gurgaon','1265',NULL,NULL,'3','2','1',NULL,'21','11',NULL,'1 Covered',NULL,'50Lac','3953','3995','5000000','3953','4066.666666666667','114.097496706192',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,'North-East','20-Feet','Vitrified','Semifurnished','Yes','Yes','Sector 95','Niren Gole','9654458916','9873314561','Nirengole@yahoo.co.in',NULL,'Saurabh Dadhich/NGLC Realtech Pvt Ltd','9818225352/01244384354','4000 to 4200rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','4000rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-one-phase-1-sector-95-gurgaon-1265-sq-ft-r1-spid-Z24967719?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(10,'Owner','Sell','Apartment','Gurgaon','DLF Express Greens',NULL,'Sector 1A IMT Manesar',NULL,'1945',NULL,NULL,'3','3','3','Servant Room','20','4',NULL,'2 Covered',NULL,'80Lac','4114','4122','8800000','4524','4650','125.5784061696659',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Borewell/Tank','East','32.8-Feet','Vitrified','Unfurnished','Yes','Yes','Sector 1A IMT Manesar','Ajay','9810361867',NULL,'ajay1kaliya@gmail.com',NULL,'Vishwa Prop','9810796775','4000 to 4500rup/sqft',NULL,NULL,NULL,NULL,'busy','As per the confirmation ready to post property on alive asset final price is 88Lac All inclusive',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dlf-express-greens-sector-1a-imt-manesar-gurgaon-1945-sq-ft-r3-spid-R25231316?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(11,'Owner','Sell','Apartment','Gurgaon','Bestech Park View City',NULL,'Sector-48',NULL,'4550',NULL,NULL,'4','5','3','Study room & Servant Room','15','14',NULL,'2 Covered',NULL,'3.7Cr','8132','8287','37000000','8132','8350','218.131868131868',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector-48','Avinash Sharma','7988636105',NULL,'Ringing',NULL,'Aruni Kumar/Koncept Realtors','9718505075','8200 to 8500rup/sqft',NULL,NULL,NULL,NULL,NULL,'Ringing','busy','http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-bestech-park-view-city-sector-48-gurgaon-4550-sq-ft-spid-C28739541?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IENQMSB8IzE2IyAgfCBTRUFSQ0ggfCA0NTQxNTc3NTMyMTQ0ODkwICMyI3wgIHwgMTk1MDI5MDMsMjgzNzU0NzkgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMyNiMgfCAyIHwjNyMgIHw='),
(12,'Broker','Sell','Apartment','Gurgaon','DLF Belvedere Tower',NULL,'DLF City Phase II',NULL,NULL,'2100',NULL,'3','3','2','Servant Room','20','2',NULL,'1 Covered',NULL,'1.75Cr','8334','10200','17500000','8333','8750','416.6666666666661',NULL,NULL,'10 + Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,'West',NULL,'Marble','Unfurnished','Yes','Yes','DLF City Phase II','Vineet Kejriwal/Kejriwal Realtors Pvt ltd','9873983771',NULL,NULL,NULL,'Vineet Kejriwal/Kejriwal Realtors Pvt ltd','9873983771','8500 to 9000rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation he is broker final price is1.75 Cr',NULL,'http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-dlf-belvedere-towers-dlf-city-phase-2-gurgaon-2100-sq-ft-r1-spid-P6212693?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(13,'Owner','Sell','Apartment','Gurgaon','MFG Emerald Hills',NULL,'Sector 65',NULL,'1850','1650',NULL,'3','3','3','Servant Room','3','2',NULL,NULL,'1 Open','1.25Cr','6757','7947','12500000','6757','7933.333333333333','1176.576576576576',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,NULL,'Full','Municipal Corporation','South-East','40-Feet','Vitrified','Unfurnished','Yes',NULL,'Sector 65','Nikhil Deva','9818102315',NULL,'Nikhi.deva@gmail.com',NULL,'Mr Rajnish/SRK Residency Pvt Ltd','9810047296/9810009985','7800 to 8000 rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','8000rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 1.25Cr',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-emerald-hills-sector-65-gurgaon-1850-sq-ft-r2-spid-C26728729?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(14,'Owner','Sell','Apartment','Gurgaon','MFG Emerald Hills',NULL,'Sector 65',NULL,NULL,'1650',NULL,'3','3',NULL,NULL,'5','5',NULL,NULL,NULL,'1.25Cr','7576','7947','12000000','7273','7933.333333333333','660.6060606060601',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,NULL,NULL,'Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 65','Prashant Khanna','9892813828',NULL,'off',NULL,'Mr Rajnish/SRK Residency Pvt Ltd','9810047296/9810009985','7800 to 8000 rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','8000rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 1.20Cr',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-emerald-hills-sector-65-gurgaon-1650-sq-ft-r7-spid-X20158471?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(15,'Owner','Sell','Apartment','Gurgaon','MFG Emerald Hills',NULL,'Sector 65',NULL,NULL,'1650',NULL,'3','3',NULL,NULL,'5','5',NULL,'1 Covered',NULL,'1.05Cr','6400','7947','10500000','6400','7933.333333333333','1533.333333333333','5500',NULL,NULL,'Ready to Move','Immediate','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,'Semifurnished',NULL,NULL,'Sector 65','Surinder','8127124419',NULL,'Surinderjoj_julay@yahoo.com',NULL,'Mr Rajnish/SRK Residency Pvt Ltd','9810047296/9810009985','7800 to 8000 rup/sqft',NULL,'Vikas Singh/AccoladeDevelopers','9811033359','8000rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 5500rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-emerald-hills-sector-65-gurgaon-1650-sq-ft-r2-spid-T19315611?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(16,'Owner','Sell','Apartment','Gurgaon','Ardee City',NULL,'Sector 52',NULL,'4500','4200',NULL,'4','4','3','Servant Room & Pooja Room','3','Ground',NULL,'1 Covered',NULL,'2.4Cr','5334','5695','22500000','5000','5350','350',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 56','Dr V K Sabharwal','8800982135',NULL,'call after some time',NULL,'Saurabh Dadhich/NGLC Realtech Pvt Ltd','9818225352/01244384354','5200 to 5500rup/sqft',NULL,NULL,NULL,NULL,NULL,'as per the confirmation ready to post porperty on fairpockets.com not ready to share mail Id',NULL,'http://www.99acres.com/4-bhk-bedroom-independent-builder-floor-for-sale-in-ardee-city-sector-52-gurgaon-4500-sq-ft-r3-spid-J21531907?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDI0NTcgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNjAzIHwjMyMgIHw='),
(17,'Owner','Sell','Apartment','Gurgaon','Suncity Essel Tower',NULL,'M G Road',NULL,NULL,'3800',NULL,'5','4','3','Servant Room','17','12',NULL,'1 Covered',NULL,'2.94Cr','7737','9817','29000000','7632','9500','1868.421052631579',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Borewell/Tank','North-East','32.8-Feet','Marble','Semifurnished','Yes',NULL,'M G Road','Dewaker Saffron Tiger infra','8285813642',NULL,NULL,NULL,'Romesh saldi/satyam Estate','9811000085','9500rup/sft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation He is broker ready to post property on alive asset final price is 2.90Cr',NULL,'http://www.99acres.com/5-bhk-bedroom-apartment-flat-for-sale-in-suncity-essel-tower-mg-road-gurgaon-3800-sq-ft-spid-E28797801?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(18,'Owner','Sell','Apartment','Gurgaon','Tulip White',NULL,'Sector 69',NULL,NULL,'220',NULL,'1','1','1',NULL,'13','1',NULL,NULL,NULL,'11.5Lac','5228','5652','1000000','4545','5400','854.545454545455',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'South','65.6-Feet','Vitrified','Furnished','Yes',NULL,'Sector 69','Vineet Arora','9810471914','9313773334','Vineetarora12@redifmail.com',NULL,'Sachin Chawla/Shiv Shakti properties','9818577899','5400rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 10Lac',NULL,'http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-tulip-white-sector-69-gurgaon-220-sq-ft-r2-spid-D24576491?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc4MDkgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgODMyNiAjMyN8ICA='),
(19,'Owner','Sell','Apartment','Gurgaon','Unitech the Residences',NULL,'Secter 33 ',NULL,NULL,'1535',NULL,'3','4','2','Servant Room','14','Ground',NULL,NULL,NULL,'1.04Cr','6800','7225','10400000','6800','7000','200','6800',NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 33','K.C Goel','9872668751',NULL,'kcgoel1001@Gmail.com',NULL,'Pawan Kumar/Rising Sun','8130305216','7000rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 6800rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-the-residences-sector-33-gurgaon-1535-sq-ft-r4-spid-E24434587?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(20,'Owner','Sell','Apartment','Gurgaon','Unitech the Residences',NULL,'Secter 33 ',NULL,NULL,'825',NULL,'1','1','1','Study room','14','Ground',NULL,NULL,NULL,'60Lac','7273','7225','5700000','6909','7000','90.90909090909099',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 33','Kalpana','8506027022',NULL,'Contactkalpanagupta@gmail.com',NULL,'Pawan Kumar/Rising Sun','8130305216','7000rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 57 not share mail ID',NULL,'http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-unitech-the-residences-sector-33-gurgaon-825-sq-ft-r1-spid-Y27377383?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(21,'Broker','Sell','Apartment','Gurgaon','DLF Ridgewood Estate',NULL,'DLF City Phase 4',NULL,'1443',NULL,NULL,'3','3','1',NULL,'14','5',NULL,'1 Covered',NULL,'1.3Cr','9010','9690','13000000','9009','9250','240.9909909909911',NULL,NULL,'10 + Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','North-East',NULL,'Marble',NULL,'Yes',NULL,'DLF City Phase 4','Anjali/ Intercity Estate','9717831669',NULL,NULL,NULL,'Anjali/ Intercity Estate','9717831669','9000 to 9500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation he is broker final price is1.3 Cr',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dlf-ridgewood-estate-dlf-city-phase-4-gurgaon-1443-sq-ft-spid-I28522597?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(22,'Broker','Sell','Apartment','Gurgaon','BPTP Park Serene',NULL,'Sector 37D',NULL,'1788',NULL,NULL,'3','3','3',NULL,'20','9',NULL,'1 Covered',NULL,'62.58Lac','3500','3867','6200000','3468','3650','182.4384787472036',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','East','78.7-Feet','Vitrified','Semifurnished','Yes',NULL,'Sector 37D','Vikas Aggarwal/ I Realty','9810307978',NULL,NULL,NULL,'Pawan Kumar/Rising Sun','8130305216','3500 to 3800rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation he is broker final price is 62Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-bptp-park-serene-sector-37-d-gurgaon-1788-sq-ft-spid-N28574029?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(23,'Owner','Sell','Apartment','Gurgaon','Raheja Sampada',NULL,'Sector-92',NULL,'1850',NULL,NULL,'4','3','3',NULL,'15','1',NULL,'1 Covered',NULL,'72Lac','3892','4207','7200000','3892','4066.666666666667','174.7747747747744',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','East','196.9 Feet','Wood','Unfurnished','Yes',NULL,'Sector-92','Nitesh Advani/Nishant realty','9958010090',NULL,NULL,NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4000 to 4200rup/sqft',NULL,'Navin/Winworld Realty','9650344337','4000rup/sqft',NULL,'As per the confirmation he is broker final price is 72Lac',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-raheja-sampada-sector-92-gurgaon-1850-sq-ft-r1-spid-E27619423?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(24,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc Royal Greens Phase 1',NULL,'Sector-92',NULL,NULL,'1261',NULL,'3','2',NULL,NULL,'20','9',NULL,NULL,NULL,'57Lac','4521','4802','5700000','3892','4733.333333333333','841.333333333333',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 92','Ashok Kumar','9818213835',NULL,'ringing',NULL,'Mukesh Kumar/Discover Home','9716008599','4800rup/sqft',NULL,'Navin/Winworld Realty','9650344337','4600 to 4800rup/sqft',NULL,'Ringing',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-royal-greens-phase-1-sector-92-gurgaon-1261-sq-ft-r7-spid-X19482941?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(25,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc Royal Greens Phase 1',NULL,'Sector-92',NULL,'1183',NULL,NULL,'3','2','2',NULL,'4','4',NULL,'1 Covered',NULL,'58Lac','4903','4802','5500000','4649','4733.333333333333','84.13637644406845',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Yes','Yes',NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 92','Ankur Walia','9871492866',NULL,'Ankur.walia2008@gmail.com',NULL,'Mukesh Kumar/Discover Home','9716008599','4800rup/sqft',NULL,'Navin/Winworld Realty','9650344337','4600 to 4800rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 55Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-royal-greens-phase-1-sector-92-gurgaon-1183-sq-ft-r1-spid-K27600101?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(26,'Owner','Sell','Apartment','Gurgaon','Supertech Araville',NULL,'Sector-79',NULL,'1295',NULL,NULL,'2','2','3',NULL,'24','15',NULL,NULL,'1 Open','59.65Lac','4606','5440','6500000','5019','5066.666666666667','47.36164736164756',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 79 ','Sandeep Bhateja','9911411099',NULL,'Sandeep_bhateja@yahoo.co.in',NULL,'Amrish Srivastava/Property station','8010222666','5000rup/sqft',NULL,'Aman/Aryan Realty',NULL,'5000 to 5200rup/sqft',NULL,'As per the confirmation ready to post property on aliveasset final price is 70Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-supertech-araville-sector-79-gurgaon-1295-sq-ft-r2-spid-E26363271?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(27,'Owner','Sell','Apartment','Gurgaon','Supertech Araville',NULL,'Sector-79',NULL,NULL,'1530',NULL,'2','2',NULL,NULL,'24','10',NULL,NULL,NULL,'81Lac','5294','5440','8100000','5067','5066.666666666667','0','5300',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes',NULL,'Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,'Full',NULL,'East',NULL,'Wood','Furnished','Yes',NULL,'Sector 79 ','Sanjay Gupta','9811826258',NULL,'Sanjabsp@gmail.com',NULL,'Amrish Srivastava/Property station','8010222666','5000rup/sqft',NULL,'Aman/Aryan Realty',NULL,'5000 to 5200rup/sqft',NULL,'As per the confirmation ready to post property on aliveasset final price is 5300rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-supertech-araville-sector-79-gurgaon-1530-sq-ft-r3-spid-J26010747?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(28,'Broker','Sell','Apartment','Gurgaon','TDI Ourania',NULL,'Golf Course Road',NULL,'4000','3200',NULL,'4','4','3','Servant Room','14','8',NULL,'1 Covered',NULL,'3.2Cr','8000','9647','32000000','8000','9250','1250',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,'North-East','98.4-Feet','Marble','Furnished','Yes','Yes','Golf Course Road','Mr Raj PMC Global','8447456661',NULL,NULL,NULL,'Mr Raj PMC Global','8447456661','9000 to 9500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation he is broker final price is 3.2Lac',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-tdi-ourania-golf-course-road-gurgaon-4000-sq-ft-r2-spid-D18587417?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(29,'Broker','Sell','Apartment','Gurgaon','Vatika the Seven Lamps',NULL,'Sector 82',NULL,'1430','1215',NULL,'2','2','2','Study Room','17','11',NULL,'1 Covered',NULL,'63.46Lac','4438','4717','6300000','4406','4500','94.40559440559446',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','East','78.7-Feet','Wood','Unfurnished','Yes','Yes','Sector 82','Vikas/Philby Real Estate','9910077853',NULL,NULL,NULL,'Vikas/Philby Real Estate','9910077853','4200rup/sqft',NULL,'Maninder Singh/Real infra solution','9810802935','4000 to 4200rup/sqft',NULL,'As per the confirmation he is broker final price is 63Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-vatika-the-seven-lamps-sector-82-gurgaon-1430-sq-ft-r1-spid-S25439433?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(30,'Broker','Sell','Apartment','Gurgaon','Tulip Petals',NULL,'Sector-89',NULL,'1550',NULL,NULL,'3','2','3',NULL,'12','7',NULL,'1 Covered',NULL,'62Lac','4000','4460','6200000','4000','4100','100',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,NULL,'Yes',NULL,'Full',NULL,'North-East','60-Feet','Vitrified','Semifurnished','Yes',NULL,'Sector 89','Paras Rajan','9999989894',NULL,'ringing',NULL,'Gaurav/JMD Properties','9811119829','4000 to 4200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation he is broker final price is 62Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tulip-petals-sector-89-gurgaon-1550-sq-ft-spid-P28666155?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(31,'Owner','Sell','Apartment','Gurgaon','Town House',NULL,'DLF Phase II',NULL,'3000','2200',NULL,'3','3','1','Servant Room','2','1',NULL,NULL,'3 Open','2.5Cr','8334','8882','18000000','6000','8750','2750',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes',NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Partial','Municipal Corporation','North-East','15-Feet','Vitrified','Semifurnished',NULL,'Yes','DLF Phase II','Pavnesh Bhandari','9873004078',NULL,'pavneshbhandari@redimail.com',NULL,'Vineet Kejriwal/Kejriwal Realtors Pvt ltd','9873983771','8500 to 9000rup/sqft',NULL,NULL,NULL,NULL,NULL,'as per the confirmation ready to post porperty on fairpockets.com ',NULL,'http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-dlf-city-phase-2-gurgaon-3000-sq-ft-r1-spid-S26831505?pos=SEARCH'),
(32,'Owner','Sell','Apartment','Gurgaon','Mapsko Casa Bella',NULL,'Sector-82',NULL,NULL,'1960',NULL,'3','3','3',NULL,'25','2',NULL,NULL,'1 Open','85Lac','4337','4292','8000000','4082','4100','18.36734693877543',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 82','Madav','9999998384',NULL,'madhav.monga@yahoo.com',NULL,'Maninder Singh/Real infra solution','9810802935','4000 to 4200rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978','4000rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com ',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-mapsko-casa-bella-sector-82-gurgaon-1960-sq-ft-r1-spid-X27405009?pos=SEARCH'),
(33,'Broker','Sell','Apartment','Gurgaon','Tulip Orange',NULL,'Sector-70',NULL,'1137',NULL,NULL,'3','2','2','Study Room','12','5',NULL,'1 Covered',NULL,'70Lac','6157','6417','7000000','6157','6333.333333333333','176.7810026385223',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','North-East','36-Feet','Vitrified','Semifurnished','Yes','Yes','Sector-70','Mayank Roof Realtech','9599481837',NULL,NULL,NULL,'Maninder Singh/Real infra solution','9810802935','6000 to 6500rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978','6500rup/sqft',NULL,'As per the confirmation he is broker final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tulip-orange-sector-70-gurgaon-1137-sq-ft-spid-Q26694155?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(34,'Owner','Sell','Independent House','Gurgaon','Ansal API Esencia',NULL,'Sector 67',NULL,'2750',NULL,NULL,'3','2','3',NULL,'3','Ground',NULL,NULL,NULL,'1.2Cr','4264','6077','12000000','4364','5750','1386.363636363636',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,'Full',NULL,'West',NULL,'Marble',NULL,NULL,NULL,'Sector 67','Sashidhar','9971670030',NULL,'DRSSN6@YAHOO.CO.IN',NULL,'Maninder Singh/Real infra solution','9810802935','5500 to 6000rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 1.2Cr',NULL,'http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-ansal-api-esencia-sector-67-gurgaon-2750-sq-ft-r2-spid-F23001419?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(35,'Owner','Sell','Independent House','Gurgaon','Vatika Primrose Floors',NULL,'Sector-83',NULL,'2100','2000','1500','3','3','2',NULL,'2','Ground',NULL,NULL,'1 Open','85','4048','5142','8500000','4048','5500','1452.380952380952',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Full','Municipal Corporation','Main Road',NULL,NULL,NULL,NULL,NULL,'Sector 83','Khurana','9818815206',NULL,'Daljeet71@yahoo.com',NULL,'Maninder Singh/Real infra solution','9810802935','5000rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978',NULL,NULL,'As per the confirmation ready to post property on fairpockets.com','Ringing','http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-sector-83-gurgaon-2100-sq-ft-r1-spid-T26934365?pos=SEARCH'),
(36,'Owner','Sell','Apartment','Gurgaon','Adani Oyster Grande',NULL,'Sector 102',NULL,'1889',NULL,NULL,'3','3',NULL,NULL,'25','8',NULL,NULL,NULL,'1.1Cr','5800','5227','11000000','5823','6083.333333333333','260.1464619728249','6000',NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 102','Himanshu Yadav','9716811869','8800584222','Hyadav55@gmail.com',NULL,'Danish/M/S JKS Associates','9811067684','6000 to 6500rup/sqft',NULL,'Sumit/JMD India','8860056007','5750rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 6000rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-adani-oyster-grande-sector-102-gurgaon-1889-sq-ft-r1-spid-Y21118276?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(37,'Owner','Sell','Apartment','Gurgaon','Adani Oyster Grande',NULL,'Sector 102',NULL,'1889',NULL,NULL,'3','3',NULL,NULL,'25','3',NULL,NULL,NULL,'1.1Cr','5824','5227','11000000','5823','6083.333333333333','260.1464619728249',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 102','Kaushik Dey','9560777228',NULL,NULL,NULL,'Danish/M/S JKS Associates','9811067684','6000 to 6500rup/sqft',NULL,'Sumit/JMD India','8860056007','5750rup/sqft',NULL,'ringing','Ringing','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-adani-oyster-grande-sector-102-gurgaon-1889-sq-ft-r6-spid-V11884109?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(38,'Broker','Sell','Apartment','Gurgaon','Adani Oyster Grande',NULL,'Sector 102',NULL,'1689',NULL,NULL,'3','3','3',NULL,'25','6',NULL,'1 Covered',NULL,'92.9Lac','5500','5227','9200000','5447','6083.333333333333','636.3232682060388',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 102','Sumit/JMD India','8860056007','7988138733',NULL,NULL,'Danish/M/S JKS Associates','9811067684','6000 to 6500rup/sqft',NULL,'Sumit/JMD India','8860056007','5750rup/sqft',NULL,'As per the confirmation he is broker final price is 92Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-adani-oyster-grande-sector-102-gurgaon-1689-sq-ft-spid-D28175722?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(39,'Owner','Sell','Apartment','Gurgaon','Emaar MGF Gurgaon Greens',NULL,'Sector 102',NULL,'1650',NULL,NULL,'3','4',NULL,'Servant Room','15','7',NULL,'1 Covered',NULL,'75Lac','4546','4802','7300000','4424','4600','175.757575757576','4800',NULL,NULL,'Under construction','Aug-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','Park/Garden',NULL,NULL,NULL,'Yes',NULL,'Sector 102','Sumit Sengal','9416015405',NULL,'sumit.sehgal@rediffmail.com',NULL,'Sunil/Sai Angan Real Estate','9811951929','4500 to 4800rup/sqft',NULL,'Deepak Thareja/Schon Realtors PVT Ltd','9910006482','4500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 4800rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-gurgaon-greens-sector-102-gurgaon-1650-sq-ft-r3-spid-Q25454287?pos=SEARCH'),
(40,'Owner','Sell','Apartment','Gurgaon','Emaar MGF Gurgaon Greens',NULL,'Sector 102',NULL,'1650',NULL,NULL,'3','3',NULL,NULL,'15','7',NULL,NULL,NULL,'72.6Lac','4400','4802','7200000','4364','4600','236.363636363636',NULL,NULL,NULL,'Immediate',NULL,NULL,'Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 102','Kunal Aggarwal','9810747466',NULL,'Kaushal_53@yahoo.com',NULL,'Sunil/Sai Angan Real Estate','9811951929','4500 to 4800rup/sqft',NULL,'Deepak Thareja/Schon Realtors PVT Ltd','9910006482','4500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 72Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-gurgaon-greens-sector-102-gurgaon-1650-sq-ft-r6-spid-O23077297?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(41,'Broker','Sell','Apartment','Gurgaon','Emaar MGF Gurgaon Greens',NULL,'Sector 102',NULL,'1650',NULL,NULL,'3','4','3','Servant Room','15','8',NULL,NULL,NULL,'70Lac','4243','4802','7000000','4242','4600','357.575757575758',NULL,NULL,NULL,'Under construction','Aug-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East',NULL,'Vitrified','Semifurnished','Yes','Yes','Sector 102','Deepak Thareja/Schon Realtors PVT Ltd','9910006482','9910005952',NULL,NULL,'Sunil/Sai Angan Real Estate','9811951929','4500 to 4800rup/sqft',NULL,'Deepak Thareja/Schon Realtors PVT Ltd','9910006482','4500rup/sqft',NULL,'As per the confirmation he is broker final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-emaar-mgf-gurgaon-greens-sector-102-gurgaon-1650-sq-ft-spid-X28287035?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(42,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,NULL,'2036',NULL,'3','4',NULL,NULL,'25','11',NULL,NULL,NULL,'88Lac','4323','4420','8800000','4323','4830','507','4323',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 89','Dakshit Bajaj','9891281969',NULL,'Daksh.bajaj@gmail.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com ',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-2036-sq-ft-r5-spid-A21576645?pos=SEARCH'),
(43,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,'2070',NULL,NULL,'3','4','3',NULL,'25','17',NULL,NULL,NULL,'95Lac','4590','4420','9108000','4400','4830','430','4400',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','East',NULL,'Vitrified','Unfurnished','Yes','Yes','Sector 89','Sanjeev','9212101560',NULL,'Sanarora69@yahoo.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 4400rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-2070-sq-ft-spid-B28609167?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(44,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,'1660',NULL,NULL,'2','3','3','Study & Servant Room','25','6',NULL,NULL,NULL,'70Lac','4217','4420','7000000','4217','4830','613.1325301204815',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sector 89','Rajeev','9899992736',NULL,'Rajeev.Doval@timegroup.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'Aa per the confirmation ready to post property final price is 70Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1660-sq-ft-r1-spid-E17191420?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(45,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,NULL,'1910',NULL,'3','3','3',NULL,'25','3',NULL,NULL,NULL,'80.2Lac','4199','4420','7640000','4000','4830','830','4000',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 89','Sundaram','9818678679',NULL,'Sundarams@hotmail.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 4000rup/sqft.',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1910-sq-ft-r5-spid-B22521271?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(46,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,NULL,'1957',NULL,'3','4','3','Servant Room','25','11',NULL,NULL,NULL,'85Lac','4344','4420','8219000','4200','4830','630','4200',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,'Full',NULL,'Park/Garden',NULL,NULL,'Unfurnished','Yes',NULL,'Sector 89','Harish Goel','9873080301','9582842488','ringng',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'Hi Is investeras per the confirmation ready to post property on alive asset final price is 4200rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1957-sq-ft-spid-J28457565?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(47,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,NULL,'1910',NULL,'2','2','2',NULL,'25','8',NULL,NULL,NULL,'85Lac','4451','4420','8404000','4400','4830','430','4400',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 89','Vikas','9810779966',NULL,'Vikas.Garg89@yahoo.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per th confirmation ready to post property on alive asset final price is 4400rup/sqft','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1910-sq-ft-r5-spid-N18769013?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(48,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,NULL,'2236',NULL,'3','3',NULL,NULL,'25','6',NULL,NULL,NULL,'90Lac','4026','4420','9167000','4100','4830','730','4100',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 89','Anupam Singh','9999249441',NULL,'Anupam@321@gmail.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset said we already pay 28Lac rup final price is 4100rup/sqft','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-2236-sq-ft-r3-spid-M22198391?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(49,'Owner','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,'1660',NULL,NULL,'2','3','2','Study Room','25','7',NULL,NULL,NULL,'63.08Lac','3800','4420','6300000','3795','4830','1034.819277108434',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,'East',NULL,'Vitrified','Unfurnished','Yes',NULL,'Sector 89','Ankush Vashisht','8510934351',NULL,'off',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 63Lac','no contact','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1660-sq-ft-r1-spid-M26622436?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(50,'Broker','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,'1297',NULL,NULL,'2','2','2','Pooja Room','25','11',NULL,'1 Covered',NULL,'46.69Lac','3600','4420','4770000','3678','4830','1152.282189668466',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','East','60-Feet','Wood','Unfurnished','Yes',NULL,'Sector 89','Kamal Nayan/Aadhar Homes','9711139812','7042000546','ringng',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per the confirmation he is broker final price is 46Lac + extra charges','no contact','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1297-sq-ft-spid-E28895099?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(51,'Broker','Sell','Apartment','Gurgaon','3C Greensopolis',NULL,'Sector-89',NULL,'1957',NULL,NULL,'3','4','3','Servant Room','25','12',NULL,'1 Covered',NULL,'78.28Lac','4000','4420','7828000','4000','4830','830',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation',NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 89','Akshay Tanwar/Astitva Realtor','7982453536','9999963532','Aksh.tanwar07@gmail.com',NULL,'Kamal Nayan/Aadhar Homes','9711139812/','4200 to 4500rup/sqft',NULL,'Akshay Tanwar/Astitva Realtor','7982453536','4500rup/sqft',NULL,'As per the confirmation he is broker final price is 4000rup/sqft','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-3c-greenopolis-sector-89-gurgaon-1957-sq-ft-r2-spid-U26763721?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(52,'Owner','Sell','Apartment','Gurgaon','Moonsoon Breeze Phase 1',NULL,'Sector 78',NULL,'1657',NULL,NULL,'3','2','3','Pooja Room','13','4',NULL,NULL,NULL,'80Lac','4828','4845','7300000','4406','4600','194.4477972238983',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Full','Borewell/Tank','East',NULL,NULL,NULL,'Yes',NULL,'Sector 78 ','Puneet','9958823193',NULL,'Puneet_kotnala@yahoo.com',NULL,'Mohit Yadav / Resale Experts','9728209209','4500rup/sqft',NULL,'Navin/Winworld Realty Services','9650344337','4500 to 4800rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 73Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-monsoon-breeze-sector-78-gurgaon-1657-sq-ft-r2-spid-G26733439?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(53,'Owner','Sell','Apartment','Gurgaon','Unitech South City 2',NULL,'Sector 50',NULL,NULL,'1139',NULL,'1','1','2',NULL,'2','2',NULL,NULL,'1 Open','65Lac','5706','6715','6450000','5663','6400','737.1378402107111',NULL,NULL,'10 + years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Partial','Municipal Corporation','West',NULL,'Marble','Furnished','Yes',NULL,'Sector 50','Anirudh Rathore','9168178663',NULL,'rathore.anirudh@gmail.com',NULL,'Devender Rajora','9891414040','6400rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 65Lac',NULL,'http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-unitech-south-city-2-sector-50-gurgaon-1139-sq-ft-spid-C28832157?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(54,'Owner','Sell','Apartment','Gurgaon','Indiabulls Centrum Park',NULL,'Sectar-103',NULL,'1481',NULL,NULL,'2','3','2','Study Room','19','1',NULL,'1 Covered',NULL,'67Lac','4523','4377','6368300','4300','4350','50','4300',NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,'North-East',NULL,'Marble','Unfurnished','Yes','Yes','Sector 103','Saurabh Bhargava','9810808088',NULL,'Saurabh.bhargav.82@gmal.com',NULL,'Mukesh Kumar','9716008599','4200 to 4500rup/sqft',NULL,'Dhruv Kohli/Grow More Estate','9899238283/9811290924','4200rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 4300rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-indiabulls-centrum-park-sector-103-gurgaon-1481-sq-ft-r3-spid-B25158838?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(55,'Owner','Sell','Apartment','Gurgaon','The Legend',NULL,'Secter 57 ',NULL,'3200',NULL,NULL,'4','4','3',NULL,'12','Ground',NULL,NULL,NULL,'2.3Cr','7188','7310','22000000','6875','7266.666666666667','391.666666666667',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,'North',NULL,NULL,NULL,'Yes',NULL,'Sector 57','Raminder','9818787445','9810046201','Ovation1980@gmail.com',NULL,'Karan/Rayland Realty','9899971363','7000rup/sqft',NULL,'Rajat Arora/Surendra properties','9810499330','7300 to 7500rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 2.20Cr',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-the-legend-sector-57-gurgaon-3200-sq-ft-r2-spid-W23647522?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(56,'Broker','Sell','Apartment','Gurgaon','The Legend',NULL,'Secter 57 ',NULL,'5500',NULL,NULL,'5','5','3',NULL,'12','12',NULL,'2 Covered',NULL,'3.5Cr','6364','7310','35000000','6364','7266.666666666667','903.030303030303',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes',NULL,NULL,'Yes',NULL,NULL,'Full','Municipal Corporation','East',NULL,NULL,'Semifurnished','Yes','Yes','Sector 57','Rajat Arora/Surendra properties','9810499330',NULL,NULL,NULL,'Karan/Rayland Realty','9899971363','7000rup/sqft',NULL,'Rajat Arora/Surendra properties','9810499330','7300 to 7500rup/sqft',NULL,'As per the confirmation he is broker final price is 3.5Cr',NULL,'http://www.99acres.com/5-bhk-bedroom-apartment-flat-for-sale-in-the-legend-sector-57-gurgaon-5500-sq-ft-spid-F28747369?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(57,'Owner','Sell','Apartment','Gurgaon','Piedmont Taksila Heights',NULL,'Secter 37C',NULL,'1848',NULL,NULL,'4','4','3',NULL,'15','4',NULL,NULL,'1 Open','75Lac','4059','4505','7500000','4058','4233.333333333333','174.8917748917747',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes',NULL,NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'North-East','30 Feet',NULL,NULL,'Yes',NULL,'Secter 37C','Deol','8587934185',NULL,'Udaideolal@gmail.com',NULL,'Surender Sharma/All india Properties','9350068161','4000 to 4500rup/sqft',NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','4200rup/sqft',NULL,'As per the confirmation ready to post property on alive asset final price is 75Lac',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-piedmont-taksila-heights-sector-37-c-gurgaon-1848-sq-ft-r1-spid-O27933180?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(58,'Owner','Sell','Apartment','Gurgaon','Spire Woods',NULL,'Secter 103',NULL,'1851',NULL,NULL,'3','3','3',NULL,'13','8',NULL,'1 Covered',NULL,'70Lac','3782','3910','7000000','3782','4000','218.2603997839005',NULL,NULL,NULL,'Ready to Move','Immediate','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Full','Municipal Corporation','North-East',NULL,NULL,'Semifurnished','Yes',NULL,'Sector 103','Anil','9990603128',NULL,'anilatri2810@gmail.com',NULL,'Mukesh Kumar','9716008599','4000rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spire-woods-sector-103-gurgaon-1851-sq-ft-r1-spid-X26062429?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(59,'Owner','Sell','Apartment','Gurgaon','Godrej 101',NULL,'Secter 79 ',NULL,'1262',NULL,NULL,'2','2','3',NULL,'15','Ground',NULL,NULL,'1 Open','45Lac','3566','5482','4500000','3566','5250','1684.231378763867',NULL,NULL,NULL,'Under construction','Aug-19','Resale','Freehold',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,'Full','Municipal Corporation','North-East','45-Feet','Vitrified','Furnished','Yes',NULL,'Sector 79 ','Atul','8527695502',NULL,'atultaxak@yahoo.com',NULL,'Maninder Singh/Real infra solution','9810802935','5000 to 5500rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978',NULL,NULL,'as per the confirmation ready to post porperty on fairpockets.com.',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-godrej-101-sector-79-gurgaon-1262-sq-ft-spid-X29058843?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(60,'Owner','Sell','Apartment','Gurgaon','Supertech Hues',NULL,'Secter 68 ',NULL,'1180','1000',NULL,'2','2','3',NULL,'29','12',NULL,NULL,NULL,'62Lac','5255','6417','6200000','5254','6266.666666666667','1012.429378531074',NULL,NULL,NULL,'Under construction','Jul-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','East',NULL,'Marble','Furnished','Yes',NULL,'Sector 68','Ankit Kaushik','9899804990',NULL,'ankitkaushikca@yahoo.com',NULL,'Vivik','9711178607','6000 to 6300rup/sqft',NULL,'Madhav Mishra/Winworld realty','9650544334','6500',NULL,'as per the confirmation ready to post porperty on fairpockets.com.',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-supertech-hues-sector-68-gurgaon-1180-sq-ft-r2-spid-Z27487263?pos=SEARCH'),
(61,'Owner','Sell','Apartment','Gurgaon','Godrej Summit',NULL,'Secter 104',NULL,'1447',NULL,NULL,'2','2','3',NULL,'19','4',NULL,NULL,NULL,'76.03Lac','5254','5227','7600000','5252','5550','297.7539737387697','5100',NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,'Marble','Unfurnished','Yes',NULL,'Sector-104','Vineet Arora','9810644424',NULL,'Vineetarora_r@yahoo.in',NULL,'Manmohan Gupta/Brick ty Brick','8048094664','5000rup/sqft',NULL,'Vijay Khurana/Finmart Realty','9990508289','5550',NULL,'As per the confirmation ready to post property final price is 5100rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-godrej-summit-sector-104-gurgaon-1447-sq-ft-r3-spid-P26141335?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(62,'Owner','Sell','Apartment','Gurgaon','Silverglades the Melia',NULL,'Sohna Road',NULL,'1350',NULL,NULL,'2','2','2','Study Room','15','14',NULL,'1 Covered',NULL,'67Lac','4888','4802','6200000','4593','4800','207.4074074074078',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,'Semifurnished','Yes',NULL,'Sohna','Dapinder Singh','9818365376',NULL,'Dapisingh@gmail.com',NULL,'Saurabh Dadhich/NGLC Realtech','9818225352/01244384354','4800rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 62Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-silverglades-the-melia-sohna-gurgaon-1350-sq-ft-r1-spid-H19157905?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(63,'Owner','Sell','Apartment','Gurgaon','Tashee Capital Gateway',NULL,'Sectar-111',NULL,NULL,'1990',NULL,'3','3','3',NULL,'18','3',NULL,'1 Covered',NULL,'90Lac','4562','4462','8600000','4322','4433.333333333333','111.7252931323283',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 111','Radhika Haldia','9167980516',NULL,'Radseh@gmail.com',NULL,'Vijay Khurana/FinMart realty','9990508289','4300 to 4500rup/sqft',NULL,'J.S.K Homes','9810955811','4500rup/sqft',NULL,'As per the confirmation ready to post property final price is 86Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tashee-capital-gateway-sector-111-gurgaon-1990-sq-ft-r1-spid-L28115068?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(64,'Owner','Sell','Apartment','Gurgaon','Tashee Capital Gateway',NULL,'Sectar-111',NULL,'1422',NULL,NULL,'2','2',NULL,NULL,'18','4',NULL,'1 Covered','1 Open','55.5Lac','4286','4462','5500000','3868','4433.333333333333','565.5414908579464',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Sector 111','Machael Khanna','9810156505',NULL,'KhannaMachael@gmail.com',NULL,'Vijay Khurana/FinMart realty','9990508289','4300 to 4500rup/sqft',NULL,'J.S.K Homes','9810955811','4500rup/sqft',NULL,'As per the confirmation ready to post property final price is 55Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-tashee-capital-gateway-sector-111-gurgaon-1295-sq-ft-r4-spid-I15343653?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(65,'Owner','Sell','Apartment','Gurgaon','Era Cosmocity',NULL,'Sectar-103',NULL,'1368',NULL,NULL,'2','2','3',NULL,'28','12',NULL,'1 Covered',NULL,'45Lac','3290','3910','4500000','3289','3520','230.5263157894738','2700',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Full',NULL,'East','196.9-Feet','Vitrified','Semifurnished','Yes','Yes','Sector 103','Gaurav','9818010727',NULL,'Gaurav.pahuja1983@gmail.com',NULL,'Mukesh Kumar','9716008599','3200 to 3500rup/sqft',NULL,'Dhruv Kohli/Grow More Estate','9899238283/9811290924','3500rup/sqft',NULL,'As per the confirmation ready to post property final price is 2700rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-era-cosmocity-sector-103-gurgaon-1368-sq-ft-r2-spid-S16710101?pos=SEARCH'),
(66,'Owner','Sell','Apartment','Gurgaon','Era Cosmocity',NULL,'Sectar-103',NULL,'1828',NULL,NULL,'3','3','3',NULL,'28','6',NULL,NULL,NULL,'64Lac','3502','3910','6400000','3501','3520','18.9059080962802',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,'Vitrified',NULL,'Yes',NULL,'Sector 103','Harmeet','7259030410',NULL,'babarai13@yahoo.co.in',NULL,'Mukesh Kumar','9716008599','3200 to 3500rup/sqft',NULL,'Dhruv Kohli/Grow More Estate','9899238283/9811290924','3500rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com ',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-era-cosmocity-sector-103-gurgaon-1828-sq-ft-r6-spid-C21239046?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(67,'Broker','Sell','Apartment','Gurgaon','Satya the Hermitage',NULL,'Sectar-103',NULL,'2605','2500',NULL,'4','4',NULL,'Servant Room','14','6',NULL,NULL,NULL,'95Lac','3647','4165','9500000','3647','3933.333333333333','286.5003198976328',NULL,NULL,'1 to 5 Years','Immediate',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,'Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 103','Mr ANKUSH/ Fair Investment Solutions','9810723221',NULL,NULL,NULL,'Manmohan Gupta/Brick ty Brick','8048094664','4000rup/sqft',NULL,'Vijay Khurana/Finmart Realty','9990508289','3800 to 4000rup/sqft',NULL,'As per the confirmation He is broker ready to post property',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-satya-the-hermitage-sector-103-gurgaon-2605-sq-ft-r4-spid-X21519213?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(68,'Owner','Sell','Apartment','Gurgaon','Unitech Sunbeeze',NULL,'Sectar-69',NULL,'1807',NULL,NULL,'4','4','2','Servant Room','14','10',NULL,'1 Covered',NULL,'72.5Lac','4012','4165','10000000','5534','5600','65.96568898727128',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East',NULL,NULL,NULL,'Yes',NULL,'Sector 69','Amit Bhambri','9811560165',NULL,'amit.bhambri7@gmail.com',NULL,'Navin/Winworld realty','9650344337','4000rup/sqft Basic price',NULL,NULL,NULL,NULL,NULL,'as per the confirmation ready to post porperty on fairpockets.com.',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-unitech-sunbreeze-sector-69-gurgaon-1807-sq-ft-r2-spid-F26835425?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(69,'Owner','Sell','Apartment','Gurgaon','Earth Elacasa',NULL,'Sectar-107',NULL,'2285',NULL,NULL,'3','4','3','Servant Room & Study Room','25','12',NULL,'1 Covered',NULL,'72.5Lac','4049','4240','7250000','3173','4266.666666666667','1093.800145878921','3900',NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 107','Munish','9650028466',NULL,'said call after some time',NULL,'Saurabh Dadhich/NGLC Realtech','9818225352','4000 to 4300rup/sqft',NULL,'Nitin/Priyanka Housing','8030408435','4500rup/sqft',NULL,'As per the confirmation ready to post property final price is 3900rup/sqft its Basic price',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-earth-elacasa-sector-107-gurgaon-2285-sq-ft-spid-N28363477?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(70,'Owner','Sell','Apartment','Gurgaon','Eldeco Accolade',NULL,'Sohna Road',NULL,NULL,'1290',NULL,'2','2',NULL,NULL,'19','4',NULL,NULL,NULL,'55.47Lac','4300','3952','5547000','4300','4300','0','4100',NULL,NULL,'Under construction','Jun-18','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Sohna Road','Surjeet','9810229526',NULL,'Surjeet.s28@gmail.com',NULL,'Aman/Aryan Realty Infra','8447755103','4000 to 4500rup/sqft',NULL,'Ankur Aggarwal/Aggarwal realter','9991000585','4200 to 4500rup/sqft',NULL,'As per the confirmation readt to post property price is 4100rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-eldeco-accolade-sohna-gurgaon-1290-sq-ft-r3-spid-N20762323?pos=SEARCH'),
(71,'Owner','Sell','Apartment','Gurgaon','Eldeco Accolade',NULL,'Sohna Road',NULL,'1269',NULL,NULL,'2','2','3',NULL,'19','8',NULL,NULL,NULL,'54Lac','4256','3952','5400000','4255','4300','44.68085106383023','4500',NULL,NULL,'Under construction','Jan-18','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sohna Road','Ashok Bansal','9355157288',NULL,'Mhd9355157288@yahoo.com',NULL,'Aman/Aryan Realty Infra','8447755103','4000 to 4500rup/sqft',NULL,'Ankur Aggarwal/Aggarwal realter','9991000585','4200 to 4500rup/sqft',NULL,'As per the confirmation ready to post property final price is 4500rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-eldeco-accolade-sohna-gurgaon-1269-sq-ft-r1-spid-M23405894?pos=SEARCH'),
(72,'Broker','Sell','Apartment','Gurgaon','Pareena Mi Casa',NULL,'Sectar-68',NULL,'1245',NULL,NULL,'2','2','3',NULL,'36','6',NULL,'1 Covered',NULL,'65Lac','5221','5482','6500000','5221','5800','579.1164658634534',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 68','Varsha Aggarwal','8010942646',NULL,NULL,NULL,'Varsha Aggarwal','8010942646','5000 to 5500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation He is broker ready to post property.',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-mi-casa-sector-68-gurgaon-1245-sq-ft-spid-K28621671?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(73,'Owner','Sell','Apartment','Gurgaon','Pareena Mi Casa',NULL,'Sectar-68',NULL,'1245',NULL,NULL,'2','2','3',NULL,'36','28',NULL,'1 Covered',NULL,'68Lac','5462','5482','6600000','5301','5800','498.7951807228919',NULL,NULL,NULL,'Under construction','Feb-20','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','Main Road',NULL,NULL,NULL,'Yes',NULL,'Sector 68','Sam','7838555499',NULL,'ringing',NULL,'Varsha Aggarwal','8010942646','5000 to 5500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 66Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-mi-casa-sector-68-gurgaon-1245-sq-ft-r2-spid-B25242150?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(74,'Owner','Sell','Apartment','Gurgaon','Pareena Mi Casa',NULL,'Sectar-68',NULL,NULL,'1250',NULL,'2','2',NULL,NULL,'36','3',NULL,NULL,NULL,'68.75Lac','5500','5482','6875000','5500','5800','300',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 68','Kumar Abhishek','9986417778',NULL,'ringing',NULL,'Varsha Aggarwal','8010942646','5000 to 5500rup/sqft',NULL,NULL,NULL,NULL,NULL,'said all after some time',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-mi-casa-sector-68-gurgaon-1250-sq-ft-r4-spid-X19731689?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(75,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas',NULL,'Sectar-70',NULL,'1560',NULL,NULL,'3','3','3',NULL,'15','Ground',NULL,'1 Covered',NULL,'75Lac','4808','4802','7000000','4487','4833.333333333333','346.1538461538457',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'North-east','150-Feet','Wood','Furnished','Yes','Yes','Sector 70','Rajesh Gupta','9810385892',NULL,'Grajeshg20000@hotmail.com',NULL,'Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price',NULL,'Varsha Aggarwal/Connections ','8010942646','5000rup/sqft',NULL,'As per the confirmation ready to post property Final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-r3-spid-D22735909?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(76,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas',NULL,'Sectar-70',NULL,'1560',NULL,NULL,'3','3','3',NULL,'15','12',NULL,NULL,NULL,'65Lac','4166','4802','6500000','4167','4833.333333333333','666.6666666666661','4000',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 70','Ashish','9711204574',NULL,'Soodashish1@gmail.com',NULL,'Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price',NULL,'Varsha Aggarwal/Connections ','8010942646','5000rup/sqft',NULL,'As per the confirmation ready to post property Final price is 4000rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-r2-spid-K27033103?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(77,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas',NULL,'Sectar-70',NULL,'1560',NULL,NULL,'3','4','3','Servant Room','15','7',NULL,'1 Covered',NULL,'78Lac','5000','4802','7500000','4808','4833.333333333333','25.64102564102541',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Borewell/Tank','East',NULL,NULL,NULL,'Yes','Yes','Sector 70','Ajay','9972163600',NULL,'Giriajay_giri@redif.com',NULL,'Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price',NULL,'Varsha Aggarwal/Connections ','8010942646','5000rup/sqft',NULL,'As per the confirmation ready to post property Final price is 75Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-spid-F29046745?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(78,'Owner','Sell','Apartment','Gurgaon','Unitech Vistas',NULL,'Sectar-70',NULL,'1560',NULL,NULL,'3','3','3','Servant Room','15','6',NULL,NULL,NULL,'79Lac','5065','4802','7300000','4679','4833.333333333333','153.8461538461534',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'East',NULL,NULL,NULL,'Yes',NULL,'Sector 70','Monica','9818492066',NULL,'Spacesmonica@gmail.com',NULL,'Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price',NULL,'Varsha Aggarwal/Connections ','8010942646','5000rup/sqft',NULL,'As per the confirmation ready to post property final price is 73Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-r2-spid-W24541533?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(79,'Broker','Sell','Apartment','Gurgaon','Unitech Vistas',NULL,'Sectar-70',NULL,'1535',NULL,NULL,'3','4','3','Servant Room','15','4',NULL,'1 Covered',NULL,'62Lac','4040','4802','6200000','4039','4833.333333333333','794.245385450597',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'Park/Garden',NULL,NULL,NULL,'Yes',NULL,'Sector 70','Varsha Aggarwal/Connections ','8010942646',NULL,NULL,NULL,'Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price',NULL,'Varsha Aggarwal/Connections ','8010942646','5000rup/sqft',NULL,'As per the confirmation Hi is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1535-sq-ft-spid-K28621307?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(80,'Broker','Sell','Apartment','Gurgaon','Unitech Vistas',NULL,'Sectar-70',NULL,'1560',NULL,NULL,'3','4','3','Servant Room','15','7',NULL,'1 Covered',NULL,'56.1Lac','3597','4802','5610000','3596','4833.333333333333','1237.179487179487',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,NULL,NULL,'Full',NULL,'Park/Garden',NULL,NULL,NULL,'Yes',NULL,'Sohna Road','Dhrub/Premiux Capital','8375029981',NULL,NULL,NULL,'Dhrub/Premiux Capital','8375029981','4500,5000up/sqft Basic price',NULL,'Varsha Aggarwal/Connections ','8010942646','5000rup/sqft',NULL,'As per the confirmation Hi is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-unitech-vistas-sector-70-gurgaon-1560-sq-ft-spid-D28947423?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(81,'Owner','Sell','Apartment','Gurgaon','CHD Resortico',NULL,'Sohna Road',NULL,'709',NULL,NULL,'1','1','1',NULL,'15','7',NULL,'1 Covered',NULL,'29Lac','4091','3952','2700000','3808','4100','291.8194640338506',NULL,NULL,NULL,'Under construction','Mar-20','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation',NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sohna Road','Kaushal Gupta','9999975277',NULL,'kaushalguptahere@gmail.com',NULL,'Anand Garg/Konexionz Real Estate','9811364000/9811944603','4000 to 4200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 27Lac',NULL,'http://www.99acres.com/1-bhk-bedroom-serviced-apartment-flat-for-sale-in-chd-resortico-sohna-road-gurgaon-709-sq-ft-r3-spid-J25485131?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(82,'Owner','Sell','Apartment','Gurgaon','Raheja Maheshwara',NULL,'Sohna Road',NULL,'1198',NULL,NULL,'2','2','2',NULL,'13','11',NULL,'1 Covered',NULL,'35.64Lac','2975','2890','3500000','2922','3250','328.4641068447413',NULL,NULL,NULL,'Under construction','Dec-20','Resale','Freehold',NULL,'Yes','Yes',NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sohna Road','Tarun Sharma','9582636815',NULL,'off',NULL,'Ankur Aggarwal/Aggarwal realter','9991000585','3000 to 3500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per confirmation ready to post property final price is 35Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-raheja-maheshwara-sohna-gurgaon-1198-sq-ft-r1-spid-X27758191?pos=SEARCH'),
(83,'Owner','Sell','Apartment','Gurgaon','Eros Wembley Estate',NULL,'Sohna Road',NULL,'1376',NULL,NULL,'3','3','2',NULL,'16','9',NULL,'1 Covered',NULL,'1.2Cr','8721','8755','11700000','8503','8510','7.093023255814842',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','south',NULL,'Ceramic','Furnished','Yes',NULL,'Sohna Road','Amit Kumar Dubey','9845090110',NULL,'off',NULL,'Ajay Varma/MD Real Estate','8813944769','8000 to 9000rup/sqft',NULL,NULL,NULL,NULL,NULL,'as per the confirmation ready to post porperty on fairpockets.com  not share mail.id',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-eros-wembley-estate-rosewood-gurgaon-1376-sq-ft-r1-spid-S26387827?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(84,'Broker','Sell','Apartment','Gurgaon','ILD Greens',NULL,'Sector-37C',NULL,NULL,'1790',NULL,'3','3','3','Pooja Room','21','8',NULL,'1 Covered',NULL,'56.5lac','3157','4037','5650000','3156','3833.333333333333','676.9087523277467',NULL,NULL,NULL,'Under construction','By 2018','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'North-east',NULL,'Marble','Unfurnished','Yes',NULL,'Secter 37C','Saurabh Goswami/Reliable Realtors','9873400634',NULL,NULL,NULL,'Surender Sharma/All india Properties','9350068161','3500 to 4000rup/sqft',NULL,'Saurabh Goswami/Reliable Realtors','9873400634','4000rup/sqft',NULL,'As per the confirmation He is broker ready to post property on Fairpockets.com',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ild-greens-sector-37-c-gurgaon-1790-sq-ft-spid-G28998875?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(85,'Broker','Sell','Apartment','Gurgaon','ILD Greens',NULL,'Sector-37C',NULL,'2311','2211',NULL,'4','4','3','Pooja room & Servant Room','21','9',NULL,'3 Covered','1 Open','71lac','3073','4037','7100000','3072','3833.333333333333','761.0702437617197',NULL,NULL,NULL,'Immediate',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Borewell/Tank','North-east','80-Feet','Vitrified',NULL,'Yes','Yes','Secter 37C','Surender Sharma/All india Properties','9350068161',NULL,NULL,NULL,'Surender Sharma/All india Properties','9350068161','3500 to 4000rup/sqft',NULL,'Saurabh Goswami/Reliable Realtors','9873400634','4000rup/sqft',NULL,'As per the confirmation He is broker ready to post property on Fairpockets.com',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-ild-greens-sector-37-c-gurgaon-2311-sq-ft-r2-spid-Q15906483?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(86,'Owner','Sell','Apartment','Gurgaon','Dhoot time Residency',NULL,'Sector-63',NULL,'1708',NULL,NULL,'3','3','3','Servant Room','20','16',NULL,'1 Covered',NULL,'1.21Cr','7100','7267','12100000','7084','7100','15.6908665105384','7000',NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','West',NULL,'Marble','Semifurnished','Yes',NULL,'Secter 63','Keshav','8800253408',NULL,'arorakeshav95@gmail.com',NULL,'Mr Raj PMC Global','8447456661','7000rup/sqft',NULL,'Rohit/Fundus Praedium','9810520002','7000 to 7200rup/sqft',NULL,'As per the confirmation ready to post property final price is 7000rup/sqft BSP',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dhoot-time-residency-sector-63-gurgaon-1708-sq-ft-spid-P28956837?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(87,'Owner','Sell','Apartment','Gurgaon','Dhoot time Residency',NULL,'Sector-63',NULL,'1708',NULL,NULL,'3','3','3','Study Room','20','6',NULL,NULL,NULL,'1.2LCr','7026','7267','12100000','6800','7100','300','6800',NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','West',NULL,'Marble','Unfurnished','Yes',NULL,'Secter 63','Manoj','9810020483',NULL,'Manny111968@gmail.com',NULL,'Mr Raj PMC Global','8447456661','7000rup/sqft',NULL,'Rohit/Fundus Praedium','9810520002','7000 to 7200rup/sqft',NULL,'As per the confirmation ready to post property final price is 6800rup/sqft BSP',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dhoot-time-residency-sector-63-gurgaon-1708-sq-ft-r3-spid-C24001461?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(88,'Broker','Sell','Apartment','Gurgaon','Dhoot time Residency',NULL,'Sector-63',NULL,'1642',NULL,NULL,'3','3','3','Servant Room','20','13',NULL,NULL,NULL,'98.52Lac','6000','7267','9852000','6000','7100','1100',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','West',NULL,'Marble','Unfurnished','Yes',NULL,'Secter 63','Mr Raj PMC Global','8447456661',NULL,NULL,NULL,'Mr Raj PMC Global','8447456661','7000rup/sqft',NULL,'Rohit/Fundus Praedium','9810520002','7000 to 7200rup/sqft',NULL,'As per the confirmation he is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-dhoot-time-residency-sector-63-gurgaon-1642-sq-ft-r2-spid-F19275443?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(89,'Owner','Sell','Apartment','Gurgaon','Ramprastha City Skyz',NULL,'Sector-37D',NULL,NULL,'1750',NULL,'3','3','3',NULL,'20','4',NULL,'1 Covered',NULL,'71Lac','4058','4207','7000000','4000','4066.666666666667','66.66666666666652',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Secter 37D','AK Datta','9818682267',NULL,'Dr_AkDatta@yahoo.co.in',NULL,'Surender Sharma/All india Properties','9350068161','4000 to 4200rup/sqft',NULL,'Saurabh Goswami/Reliable Realtors','9873400634','4000rup/sqft',NULL,'As per the confirmation ready to post property final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ramprastha-city-skyz-sector-37-d-gurgaon-1750-sq-ft-r5-spid-H23877926?pos=SEARCH'),
(90,'Owner','Sell','Apartment','Gurgaon','Spaze Privy the Address',NULL,'Sector-93',NULL,'1998',NULL,NULL,'3','4','2','Servant Room & Study Room','20','10',NULL,'2 Covered',NULL,'75.92Lac','3800','4037','7592000','3800','3825','25.20020020020002',NULL,NULL,NULL,'Under construction','Mar-17','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,'Yes',NULL,NULL,'Full','Municipal Corporation','Park/Garden',NULL,NULL,NULL,'Yes','Yes','Sector 72','Kingshuk Basu',NULL,NULL,NULL,NULL,'Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft',NULL,'No contect','No contect','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1998-sq-ft-r2-spid-N26031049?pos=SEARCH'),
(91,'Owner','Sell','Apartment','Gurgaon','Spaze Privy the Address',NULL,'Sector-93',NULL,'1998',NULL,NULL,'3','4','2','Servant Room & Study Room','15','Ground',NULL,'2 Covered',NULL,'74Lac','3704','4037','7400000','3704','3825','121.2962962962961',NULL,NULL,NULL,'Immediate',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Sector 72','S Taneja','9810375239',NULL,'Tanejask@yahoo.com',NULL,'Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft',NULL,'As per the confirmation ready to post property final price is 74Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1998-sq-ft-r5-spid-U24215273?pos=SEARCH'),
(92,'Owner','Sell','Apartment','Gurgaon','Spaze Privy the Address',NULL,'Sector-93',NULL,'1697',NULL,NULL,'3','3','3',NULL,'15','4',NULL,NULL,NULL,'62Lac','3653','4037','6200000','3654','3825','171.4938126104889','3600',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,'Marble','Semifurnished','Yes',NULL,'Sector 72','Deepakurv','9871595075',NULL,'Deepakvarshney23@gamil.com',NULL,'Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft',NULL,'As per the confirmation ready to post property final price is 3600rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1697-sq-ft-spid-D28428147?pos=SEARCH'),
(93,'Broker','Sell','Apartment','Gurgaon','Spaze Privy the Address',NULL,'Sector-93',NULL,'1697',NULL,NULL,'3','4','2',NULL,'15','7',NULL,'1 Covered',NULL,'52.61Lac','3100','4037','5261000','3100','3825','724.8232174425457',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','North-east','60-Feet','Vitrified','Unfurnished','Yes',NULL,'Sector 72','Amrish Srivastava/Property station','8010222666',NULL,NULL,NULL,'Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft',NULL,'As per the confirmation he is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1697-sq-ft-spid-F28942203?pos=SEARCH'),
(94,'Broker','Sell','Apartment','Gurgaon','Spaze Privy the Address',NULL,'Sector-93',NULL,'1998',NULL,NULL,'3','4','3','servant room','19','10',NULL,NULL,NULL,'62Lac','3104','4037','6200000','3103','3825','721.896896896897',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 72','Bimlesh Choudhary/Reliable Realtors','9999640915',NULL,NULL,NULL,'Amrish Srivastava/Property station','8010222666','3800 to 4000rup/sqft Basic',NULL,'Bimlesh Choudhary/Reliable Realtors','9999640915','3500 to 4000rup/sqft',NULL,'As per the confirmation he is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-spaze-privvy-the-address-sector-93-gurgaon-1998-sq-ft-r3-spid-H20210189?pos=SEARCH'),
(95,'Broker','Sell','Apartment','Gurgaon','Sare Petioles',NULL,'Sector-92',NULL,'2093',NULL,NULL,'4','4','3','servant room','20','6',NULL,'1 Covered',NULL,'67Lac','3202','4420','6700000','3201','4100','898.8533205924509',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-east','60-Feet','Vitrified',NULL,'Yes',NULL,'Sector 92','Dinesh Kumar/Welcome Home','8510888888',NULL,NULL,NULL,'Dinesh Kumar/Welcome Home','8510888888','4000 to 4200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation He is broker ready to post property',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sare-petioles-sector-92-gurgaon-2093-sq-ft-spid-H29087649?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(96,'Owner','Sell','Apartment','Gurgaon','Avalon Rangoli',NULL,'Sector 24 Dharuhera',NULL,'1300',NULL,NULL,'2','2','3',NULL,'12','11',NULL,'1 Covered',NULL,'30Lac','2308','2465','3000000','2308','2500','192.3076923076924',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 24 Dharuhera','Upesh Sharma','9911405053',NULL,'Upeshsha@gmail.com',NULL,'Mohit Yadav / Resale Experts','9728209209','2200 to 2500rup/sqft',NULL,'Shiv Rohilla/Shivsan Buildwell Pvt Ltd','9212706175/8930939992','2200 to 2400rup/sqft',NULL,'As per the confirmation ready to post property final price is 30Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-avalon-rangoli-sector-24-dharuhera-1300-sq-ft-spid-P29046957?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(97,'Owner','Sell','Apartment','Gurgaon','Avalon Rangoli',NULL,'Sector 24 Dharuhera',NULL,'1300',NULL,NULL,'2','2','3',NULL,'12','7',NULL,'1 Covered',NULL,'30.55Lac','2350','2465','3055000','2350','2500','150',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 24 Dharuhera','Raj','9810980437',NULL,'Rajkumarkansal95@gmail.com',NULL,'Mohit Yadav / Resale Experts','9728209209','2200 to 2500rup/sqft',NULL,'Shiv Rohilla/Shivsan Buildwell Pvt Ltd','9212706175/8930939992','2200 to 2400rup/sqft',NULL,'As per the confirmation ready to post property final price is 30.5Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-avalon-rangoli-sector-24-dharuhera-1300-sq-ft-r3-spid-Y21898625?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(98,'Broker','Sell','Apartment','Gurgaon','Avalon Rangoli',NULL,'Sector 24 Dharuhera',NULL,'1300',NULL,NULL,'2','2','3',NULL,'12','5',NULL,'1 Covered',NULL,'27.5Lac','2116','2465','2750000','2115','2500','384.6153846153848',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','North','24-Feet','Vitrified','Semifurnished','Yes','Yes','Sector 24 Dharuhera','Mohit Yadav / Resale Experts','9728209209',NULL,NULL,NULL,'Mohit Yadav / Resale Experts','9728209209','2200 to 2500rup/sqft',NULL,'Shiv Rohilla/Shivsan Buildwell Pvt Ltd','9212706175/8930939992','2200 to 2400rup/sqft',NULL,'As per the confirmation he is broker ready to post property',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-avalon-rangoli-sector-24-dharuhera-1300-sq-ft-spid-U28496089?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(99,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali',NULL,'Sector 1',NULL,'2500','2400',NULL,'4','4','3',NULL,'10','9',NULL,'1 Covered',NULL,'75','3000','3315','7500000','3000','3350','350',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,NULL,'Full','Borewell/Tank','East','120-Feet','Marble',NULL,'Yes',NULL,'Sector 1','Ashwani Sharma','7087696046',NULL,'aks33noida@gmail.com',NULL,'Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 74Lac',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2500-sq-ft-r1-spid-T28164344?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTMyMjY5MjMwNDQ2NTU1ICMyI3wgIHwgMjg2MTc5ODEsMjgzNzU0NzkgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMyNiMgfCAyIHwjNyMgIHw='),
(100,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali',NULL,'Sector 1',NULL,'2500',NULL,NULL,'4','4','2','Servant Room','10','4',NULL,'1 Covered',NULL,'82Lac','3280','3315','8000000','3200','3350','150',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full','Municipal Corporation',NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 1','R.N Khan','9463888308',NULL,'ringing',NULL,'Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property on alive asset final price is 80Lac not share Mail ID',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2500-sq-ft-r2-spid-M26722915?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(101,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali',NULL,'Sector 1',NULL,'2700',NULL,NULL,'4','4','2',NULL,'10','10',NULL,'1 Covered',NULL,'75','2778','3315','7400000','2741','3350','609.2592592592591',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,NULL,'Full','Municipal Corporation','East','120-Feet','Marble','Furnished','Yes','Yes','Sector 1','Ashwani Sharma','7087696046',NULL,'aks33noida@gmail.com',NULL,'Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 74Lac',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2700-sq-ft-spid-E28981929?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(102,'Owner','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali',NULL,'Sector 1',NULL,NULL,'2750',NULL,'4','4',NULL,NULL,'10','7',NULL,'1 Covered',NULL,'85Lac','3091','3315','8500000','3091','3350','259.090909090909',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Full','Municipal Corporation','East','120-Feet','Marble','Furnished','Yes','Yes','Sector 1','Abhishek Gupta','9779744123',NULL,'Abhishekgupta701@gmail.com',NULL,'Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 83.5Lac',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2750-sq-ft-r4-spid-B7262905?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(103,'Broker','Sell','Apartment','Gurgaon','HSIIDC Sidco Aravali',NULL,'Sector 1',NULL,'2500',NULL,NULL,'3','3','3','Servant Room & Pooja Room','10','3',NULL,'1 Covered',NULL,'65Lac','2600','3315','6500000','2600','3350','750',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Full','Borewell/Tank','North-east','100-Feet','Marble','Semifurnished','Yes',NULL,'Sector 1','Manoj Chauhan/shri Shani Properties','9990855998',NULL,NULL,NULL,'Manoj Chauhan/shri Shani Properties','9990855998','3200 to 3500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation He is broker ready to post property final price is 65lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-hsiidc-sidco-aravali-sector-1-imt-manesar-gurgaon-2500-sq-ft-r3-spid-I25449905?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(104,'Owner','Sell','Apartment','Gurgaon','vipul world',NULL,'Sector 48',NULL,'1800','1450',NULL,'3','3','2','study room','3','3',NULL,NULL,NULL,'82','4555','5482','8200000','4556','5100','544.4444444444443',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,NULL,'Full',NULL,NULL,NULL,'Vitrified','Furnished','Yes',NULL,'Sector 48','Vivek Tomer','9599784990',NULL,'vivektomer86@gmail.com',NULL,'Sayta Pratap/invester Planner','9999515707','5000 to 5200rup/sqft All inclucive',NULL,'Narender/Neha Estate','9999996375','5200rup/sqft',NULL,'As per the confirmation ready to post property final price is 82Lac.',NULL,'http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-vipul-world-sector-48-gurgaon-1800-sq-ft-spid-F27908780?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDI0NjggfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNzA1IHwjMyMgIHw='),
(105,'Owner','Sell','Apartment','Gurgaon','vipul world',NULL,'Sector 48',NULL,'1800','1450',NULL,'3','3','1',NULL,'4','3',NULL,'1 Covered','1 Open','82','5240','5482','8200000','4556','5100','544.4444444444443',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,'Vitrified',NULL,'Yes',NULL,'Sector 48','Vivek','9958055701',NULL,'vivektomer86@gmail.com',NULL,'Sayta Pratap/invester Planner','9999515707','5000 to 5200rup/sqft All inclucive',NULL,'Narender/Neha Estate','9999996375','5200rup/sqft',NULL,'call back',NULL,'http://www.99acres.com/3-bhk-bedroom-independent-builder-floor-for-sale-in-vipul-world-sector-48-gurgaon-1450-sq-ft-spid-I28193098?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDI0NjggfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgNzA1IHwjMyMgIHw='),
(106,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills',NULL,'Sector 77',NULL,NULL,'1260',NULL,'2','2','2',NULL,'18','8',NULL,NULL,NULL,'56Lac','4445','4462','5600000','4444','5333.333333333333','888.8888888888887','4500',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes',NULL,'Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Full',NULL,NULL,NULL,'Vitrified',NULL,'Yes',NULL,'Sector 77','Gyanendra','9811009732',NULL,'Gyanendra_vn@yahoo.co.in',NULL,'Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive',NULL,'Amrish Srivastava/Property station','8010222666','5500rup/sqft',NULL,'As per the confirmation ready to post property final price is 4500rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1260-sq-ft-r7-spid-N15007765?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTM5MjE3NDczMzYxODI1ICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA='),
(107,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills',NULL,'Sector 77',NULL,'1735',NULL,NULL,'3','3','3',NULL,'18','3',NULL,'1 Covered',NULL,'81.55Lac','4701','4462','3155000','1818','5333.333333333333','3514.889529298751',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'west',NULL,'Vitrified','Semifurnished','Yes',NULL,'Sector 77','Nitish Sharma',NULL,NULL,NULL,NULL,'Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive',NULL,'Amrish Srivastava/Property station','8010222666','5500rup/sqft',NULL,'no contact','no contact','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1735-sq-ft-r7-spid-Z19929203?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(108,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills',NULL,'Sector 77',NULL,'1515',NULL,NULL,'3','2','2',NULL,'18','6',NULL,NULL,NULL,'70.45Lac','4651','4462','7045000','4650','5333.333333333333','683.1683168316831','4600',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,'Furnished','Yes',NULL,'Sector 77','Nishi Gangneja','9971460456','9810897899 Mandeep Kumar','mkazab11@gmail.com',NULL,'Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive',NULL,'Amrish Srivastava/Property station','8010222666','5500rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com.',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1515-sq-ft-r4-spid-V18188333?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(109,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills',NULL,'Sector 77',NULL,'1735',NULL,NULL,'3','3','3','Pooja Room','18','16',NULL,'1 Covered',NULL,'72','4510','4462','7200000','4150','5333.333333333333','1183.477425552353','4500',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,'marble','Furnished','Yes',NULL,'Sector 77','Pankaj','9717868466',NULL,'Pankaj.sinha@itchotels.in',NULL,'Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive',NULL,'Amrish Srivastava/Property station','8010222666','5500rup/sqft',NULL,'As per the confirmation ready to post property final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1735-sq-ft-r5-spid-E15954065?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(110,'Owner','Sell','Apartment','Gurgaon','Umang Winter Hills',NULL,'Sector 77',NULL,NULL,'1260',NULL,'2','2','3','Pooja Room','18','4',NULL,'1 Covered',NULL,'58Lac','4604','4462','5800000','4603','5333.333333333333','730.1587301587297',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Borewell/Tank','Park/Garden','100-Feet','marble','Unfurnished','Yes',NULL,'Sector 77','Niraj','9999850678',NULL,'nkc_indian@yahoo.co.in',NULL,'Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive',NULL,'Amrish Srivastava/Property station','8010222666','5500rup/sqft',NULL,'As per the confirmation ready to post property final price is 55Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1260-sq-ft-r3-spid-D25714021?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(111,'Broker','Sell','Apartment','Gurgaon','Umang Winter Hills',NULL,'Sector 77',NULL,NULL,'1260',NULL,'2','2','2',NULL,'18','7',NULL,NULL,NULL,'50.4Lac','4000','4462','5040000','4000','5333.333333333333','1333.333333333333',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,'Semifurnished','Yes',NULL,'Sector 77','Himanshu / DC Jain Real Estate Services','9873999789',NULL,NULL,NULL,'Himanshu / DC Jain Real Estate Services','9873999789','5000 to 5500rup/sqft All inclucive',NULL,'Amrish Srivastava/Property station','8010222666','5500rup/sqft',NULL,'As per the confirmation he is broker ready to post property',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-umang-winter-hills-sector-77-gurgaon-1260-sq-ft-r4-spid-L14289335?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(112,'Owner','Sell','Apartment','Gurgaon','BPTP terra',NULL,'Sector-37D',NULL,NULL,'1691',NULL,'3','3',NULL,NULL,'23','10',NULL,NULL,NULL,'84.55Lac','5000','5567','8455000','5000','5100','100',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector-37D','Deb','9899211161',NULL,'ringing',NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','5000 to 5200rup/sqft',NULL,NULL,NULL,NULL,NULL,NULL,'Ringing','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-bptp-terra-sector-37-d-gurgaon-1691-sq-ft-r4-spid-X23702360?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(113,'Owner','Sell','Apartment','Gurgaon','Pareena Coban Residences',NULL,'Sector 99A',NULL,NULL,'1997',NULL,'3','3','3',NULL,'20','10',NULL,NULL,NULL,'1Cr','5008','5100','10000000','5008','5100','92.48873309964983','4900',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 99A','Kunal Tiwari','9829404933',NULL,'Tiwari.kt@gmail.com',NULL,'Manmohan Gupta/Brick ty Brick','8048094664','5000 to 5200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post final price is 4900rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-pareena-coban-residences-sector-99-a-gurgaon-1997-sq-ft-r7-spid-G21173222?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(114,'Owner','Sell','Apartment','Gurgaon','Puri Emerald Bay',NULL,'Sector-104',NULL,'2450',NULL,NULL,'3','4','3','Servant Room','31','26',NULL,'2 Covered',NULL,'1.6Cr','6531','7820','16000000','6531','7666.666666666667','1136.054421768707',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','North','492.1Feet','marble','Semifurnished','Yes','Yes','Sector-104','Satish Singh',NULL,NULL,NULL,NULL,'Manmohan Gupta/Brick ty Brick','8048094664','7500 to 8000rup/sqft',NULL,'Vijay Khurana/Finmart Realty','9990508289','7500rup/sqft',NULL,'No contect','No contect','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-puri-emerald-bay-sector-104-gurgaon-2450-sq-ft-r1-spid-F27600095?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(115,'Owner','Sell','Apartment','Gurgaon','Forte Point the Olive Spire',NULL,'Sector-70A',NULL,'1388',NULL,NULL,'2','2','2',NULL,'12','8',NULL,'1 Covered',NULL,'70Lac','5044','5270','7000000','5043','5250','206.772334293948',NULL,NULL,NULL,'Immediate','Dec-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 70A','Subhra Saha','9136041875',NULL,'Subhra_saha@yahoo.com',NULL,'Amrish Srivastava/Property station','8010222666','5000 to 5500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 70Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-forte-point-the-olive-spire-sector-70-a-gurgaon-1388-sq-ft-r2-spid-E27248469?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NTU2NjM2OTExMjQyICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA='),
(116,'Owner','Sell','Apartment','Gurgaon','Paras Dews',NULL,'Sector-106',NULL,NULL,'1760',NULL,'3','4','3','Servant & study room','24','12',NULL,'1 Covered',NULL,'90Lac','5133','5227','9000000','5114','5175','61.36363636363603',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Full','Municipal Corporation','Park/Garden',NULL,'Vitrified','Semifurnished','Yes',NULL,'Sector-106','Ayush','8178819634','9953480099','busy',NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ',NULL,'Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft',NULL,'DNE','Ringing','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1760-sq-ft-spid-Q29066279?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(117,'Owner','Sell','Apartment','Gurgaon','Paras Dews',NULL,'Sector-106',NULL,NULL,'1665',NULL,'3','3','3',NULL,'24','9',NULL,'1 Covered',NULL,'75Lac','4504','5227','7500000','4505','5175','670.4954954954956','4500',NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes',NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,'Wood','Semifurnished','Yes',NULL,'Sector-106','Sanjeev','9310578160',NULL,'Sanjeevsharn77@gmail.com',NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ',NULL,'Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft',NULL,'As per the confirmation ready to post final price is 4500rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1665-sq-ft-r5-spid-U21342584?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(118,'Owner','Sell','Apartment','Gurgaon','Paras Dews',NULL,'Sector-106',NULL,NULL,'1665',NULL,'3','3','3',NULL,'24','12',NULL,NULL,NULL,'83.25Lac','5000','5227','8325000','5000','5175','175',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector-106','Manish Jain','9167008956',NULL,'busy',NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ',NULL,'Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft',NULL,'As per the confirmation said final price is 1 cr not ready to share all info on call.',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1665-sq-ft-r3-spid-U20169105?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(119,'Broker','Sell','Apartment','Gurgaon','Paras Dews',NULL,'Sector-106',NULL,'1760',NULL,NULL,'3','4','3','Study & Servant Room','24','11',NULL,'1 Covered',NULL,'80.96Lac','4600','5227','8096000','4600','5175','575',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,'east','196.9 Feet','marble','Semifurnished','Yes',NULL,'Sector-106','Naveen/Winworld Realty','9650344337',NULL,NULL,NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ',NULL,'Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft',NULL,'As per the confirmation He is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1760-sq-ft-r4-spid-Q22807771?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(120,'Broker','Sell','Apartment','Gurgaon','Paras Dews',NULL,'Sector-106',NULL,'1900',NULL,NULL,'3','4','3','Study & Servant Room','24','14',NULL,'1 Covered',NULL,'87.66Lac','4614','5227','8766000','4614','5175','561.3157894736842',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','east',NULL,'Vitrified','Semifurnished','Yes',NULL,'Sector-106','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352',NULL,NULL,NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352','5000 to 5200rup/sqft ',NULL,'Naveen/Winworld Realty','9650344337','5000 to 5500rup/sqft',NULL,'As per the confirmation He is broker ready to post property',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-paras-dews-sector-106-gurgaon-1900-sq-ft-r3-spid-L25241478?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(121,'Owner','Sell','Apartment','Gurgaon','Vatika the Seven Lamps',NULL,'Sector 82',NULL,'2158',NULL,NULL,'3','4','3','Servant & study room','17','14',NULL,'2 Covered',NULL,'1.07Cr','4960','4717','8700000','4032','4825','793.4893419833179',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','East','78.7-Feet','Wood','Unfurnished','Yes','Yes','Sector 82','Vinyas','8130805553',NULL,'ringing',NULL,'Maninder Singh/Real infra solution','9810802935','4600 to 5000rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978','4700 to 5000rup/sqft',NULL,'ringing',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-vatika-the-seven-lamps-sector-82-gurgaon-2158-sq-ft-r2-spid-O26830133?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgTyMyIyB8IDc0OTAgfCBSIHwgIHwgMSMzIyB8IE8gfCMyOSMgIHwgMzM1MTYjNCMgfCA='),
(122,'Owner','Sell','Apartment','Gurgaon','Landmark tower',NULL,'Sector 51',NULL,NULL,'1050',NULL,'2','2',NULL,NULL,'6','2',NULL,NULL,NULL,'57','5428','6332','5600000','5333','6000','666.666666666667',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 51','Bharti','9990545000',NULL,'amit.leocar@gmail.com',NULL,'Arvind/PSN Realtech','9971087673','6000rup/sqft',NULL,NULL,NULL,NULL,NULL,'as per the confirmation ready to post porperty on fairpockets.com.',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-landmark-towers-sector-51-gurgaon-1050-sq-ft-r4-spid-X20457169?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(123,'Owner','Sell','Apartment','Gurgaon','Vatika Emilia Floors',NULL,'Sector 82',NULL,'1500',NULL,NULL,'2','2','2',NULL,'4','Ground',NULL,NULL,'1 Open','66Lac','4400','5227','6600000','4400','4825','425',NULL,NULL,'0 to 1 Year','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 82','Sandeep Saras','9934012239',NULL,'off',NULL,'Maninder Singh/Real infra solution','9810802935','4600 to 5000rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978','4700 to 5000rup/sqft',NULL,'Off','out of service','http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-vatika-emilia-floors-sector-82-gurgaon-1500-sq-ft-r1-spid-R28050552?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(124,'Broker','Sell','Apartment','Gurgaon','BPTP Freedom Park Life',NULL,'Sector 57',NULL,'225','220',NULL,'1','1','1',NULL,'19','2',NULL,NULL,'1 Open','13Lac','5778','6757','1300000','5778','6250','472.2222222222226',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,'Full','Municipal Corporation','North-East',NULL,'Cement','Unfurnished','Yes',NULL,'Sector 57','Shilpa/Global Estate','9990752121',NULL,NULL,NULL,'Shilpa/Global Estate','9990752121','6000 to 6500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation He is broker ready to post property on Fairpockets.com13Lac',NULL,'http://www.99acres.com/1-bhk-bedroom-apartment-flat-for-sale-in-bptp-freedom-park-life-sector-57-gurgaon-225-sq-ft-spid-R29075877?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(125,'Owner','Sell','Apartment','Gurgaon','Ansal Heights 86',NULL,'Sector 86',NULL,'1690','1500',NULL,'3','2','1',NULL,'14','10',NULL,'2 Covered',NULL,'56.18Lac','3325','3187','5618000','3324','3400','75.73964497041425','3200',NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','Pool/Club',NULL,NULL,NULL,'Yes',NULL,'Sector 86','Amit','9466935388',NULL,'amit.n.kambhoj@gmail.com',NULL,'Gaurav/JMD Properties','9811119829','3000rup/sqft',NULL,'Paras Rajan/Right Concepte','9999989894','2800 to 3000rup/sqft',NULL,'As per the ready to post property on Fairpockets.com final price is  3200rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ansal-heights-86-sector-86-gurgaon-1690-sq-ft-r1-spid-K27968344?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(126,'Owner','Sell','Apartment','Gurgaon','Ansal Heights 86',NULL,'Sector 86',NULL,'1690',NULL,NULL,'3','2','1',NULL,'14','9',NULL,'1 Covered',NULL,'55.75Lac','3299','3187','5575000','3299','3400','101.1834319526629',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East',NULL,'Vitrified','Semifurnished','Yes','Yes','Sector 86','Ashi','8076927218',NULL,'ringing',NULL,'Gaurav/JMD Properties','9811119829','3000rup/sqft',NULL,'Paras Rajan/Right Concepte','9999989894','2800 to 3000rup/sqft',NULL,'ringing','out of service','http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ansal-heights-86-sector-86-gurgaon-1690-sq-ft-spid-B28489995?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(127,'Owner','Sell','Apartment','Gurgaon','AWHO Sispal Vihar',NULL,'Sector 49',NULL,'300','250',NULL,'1','1','1',NULL,'13','1',NULL,NULL,NULL,'15Lac','5000','7565','1400000','4667','5100','433.333333333333',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,NULL,'Full',NULL,'North',NULL,'Marble','Furnished','Yes','Yes','Sector 49','Raj Kumar','9910798793',NULL,'32.gurgaom@gmail.com',NULL,'Arvind/PSN Realtech','9971087673','5000 to 5200rup/sqft All inclucive',NULL,'Surender Yadav/Jai Realty','9990171415','5000rup/sqft',NULL,'As per the ready to post property on Fairpockets.com final price is  14Lac',NULL,'http://www.99acres.com/studio-apartment-flat-for-sale-in-awho-sispal-vihar-sector-49-gurgaon-300-sq-ft-r1-spid-V24649575?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(128,'Owner','Sell','Apartment','Gurgaon','BPTP Astaire Gardens',NULL,'Sector 70 A',NULL,'1690',NULL,NULL,'3','3',NULL,NULL,'2','Ground',NULL,NULL,NULL,'82.5Lac','4882','5270','8200000','4852','4866.666666666667','14.59566074950726',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East',NULL,'Vitrified','Unfurnished','Yes',NULL,'Sector 70 A','Deepak','9716299555',NULL,'D_rally1978@hotmail.com',NULL,'Saurabh Goswami/Reliable Realtors','9873400634','4800 to 5000rup/sqft',NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','4800rup/sqft',NULL,'As per the ready to post property on Fairpockets.com final price is  82Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-bptp-astaire-gardens-sector-70-a-gurgaon-1690-sq-ft-r3-spid-H21660851?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(129,'Broker','Sell','Apartment','Gurgaon','Bestech City',NULL,'Manesar',NULL,'975',NULL,NULL,'2','2','3',NULL,'14','3',NULL,NULL,NULL,'33Lac','3384','3485','3200000','3282','3300','17.94871794871779',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,'Vitrified','Semifurnished','Yes',NULL,'Manesar','Shiv Rohilla/Shivsan Buildwell','9212706175',NULL,NULL,NULL,'Shiv Rohilla/Shivsan Buildwell','9212706175','3200 to 3500rup/sqft',NULL,'Anil Kumar/Goodwill propertys','9001797735','3200rup/sqft',NULL,'As per the confirmation He is broker ready to post property on live asset 32Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-bestech-city-dharuheda-gurgaon-975-sq-ft-r4-spid-I24497945?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(130,'Broker','Sell','Apartment','Gurgaon','Bestech City',NULL,'Manesar',NULL,'975',NULL,NULL,'2','2','2',NULL,'14','6',NULL,'1 Covered',NULL,'31.5Lac','3231','3485','3100000','3179','3300','120.5128205128203',NULL,NULL,'5 to 10 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','North-East','200-Feet','Vitrified','Semifurnished','Yes',NULL,'Manesar','Anil Kumar/Goodwill propertys','9001797735',NULL,NULL,NULL,'Shiv Rohilla/Shivsan Buildwell','9212706175','3200 to 3500rup/sqft',NULL,'Anil Kumar/Goodwill propertys','9001797735','3200rup/sqft',NULL,'As per the confirmation He is broker ready to post property on live asset 31Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-bestech-city-dharuheda-gurgaon-975-sq-ft-spid-K28801227?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IFNIIHwgWSB8IzE1IyAgfCBQRCM2IyB8IDggfCMxIyAgfCBPICMxI3wgIHwgODA5MCB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA='),
(131,'Broker','Sell','Apartment','Gurgaon','Shree vardhman Flora',NULL,'Sector-90',NULL,'1875',NULL,NULL,'3','3','3','Servant Room','14','9',NULL,NULL,NULL,'65.63Lac','3500','3910','6500000','3467','3700','233.3333333333335',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,NULL,'Full',NULL,NULL,NULL,'Vitrified','Semifurnished','Yes',NULL,'Sector-90','Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354',NULL,NULL,NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','3500 to 4000rup/sqft',NULL,'Amrish Srivastava/Property station','8010222666','3500rup/sqft to 3800rup/sqft',NULL,'As per the confirmation He is broker ready to post property on live asset 65Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-shree-vardhman-flora-sector-90-gurgaon-1875-sq-ft-r4-spid-Y24279661?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgQSMyIyB8IDc0MzQgfCBSIHwgIHwgMSMzIyB8IEEgfCMyOSMgIHwgODMzMiAjMyN8ICA='),
(132,'Broker','Sell','Apartment','Gurgaon','Shree vardhman Flora',NULL,'Sector-90',NULL,'1300',NULL,NULL,'2','2','2','Study Room','14','8',NULL,'1 Covered',NULL,'45.55Lac','3504','3910','4500000','3462','3700','238.4615384615386',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,NULL,'Full','Municipal Corporation','North','75-Feet','Vitrified',NULL,'Yes',NULL,'Sector-90','Amrish Srivastava/Property station','8010222666',NULL,NULL,NULL,'Saurabh Dadhich/NGLC Realtech pvt ltd','9818225352/01244384354','3500 to 4000rup/sqft',NULL,'Amrish Srivastava/Property station','8010222666','3500rup/sqft to 3800rup/sqft',NULL,'As per the confirmation He is broker ready to post property on live asset 45Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-shree-vardhman-flora-sector-90-gurgaon-1300-sq-ft-r2-spid-E27439867?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzMyIyAgfCA4ICMxI3wgIHwgQSMyIyB8IDc0MzQgfCBSIHwgIHwgMSMzIyB8IEEgfCMyOSMgIHwgODMzMiAjMyN8ICA='),
(133,'Owner','Sell','Apartment','Gurgaon','The Fernhill',NULL,'Sector 91',NULL,'1877',NULL,NULL,'3','3','3',NULL,'13','6',NULL,'1 Covered',NULL,'60','3197','3145','5000000','2664','2900','236.174746936601',NULL,NULL,NULL,'Under construction','Jul-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes',NULL,'Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 91','Ananda Dharmapal','9811565874',NULL,NULL,NULL,'Yogesh Kumar/Equator realtors','9717744497','2800 to 3000rup/sqft',NULL,'Karambir/Philby real Estate','9910050953','3000rup/sqft',NULL,'ringing',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-the-fernhill-sector-91-gurgaon-1877-sq-ft-r4-spid-C27247623?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(134,'Owner','Sell','Apartment','Gurgaon','Mapsko Casa Bella',NULL,'Sector-82',NULL,NULL,'1960',NULL,'3','3','3',NULL,'25','2',NULL,NULL,'1 Open','85Lac','4337','4292','8000000','4082','4100','18.36734693877543',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 82','Madav','9999998384',NULL,'madhav.monga@yahoo.com',NULL,'Maninder Singh/Real infra solution','9810802935','4000 to 4200rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978','4000rup/sqft',NULL,'as per the confirmation ready to post porperty on fairpockets.com ',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-mapsko-casa-bella-sector-82-gurgaon-1960-sq-ft-r1-spid-X27405009?pos=SEARCH'),
(135,'Broker','Sell','Apartment','Gurgaon','Tulip Orange',NULL,'Sector-70',NULL,'1137',NULL,NULL,'3','2','2','Study Room','12','5',NULL,'1 Covered',NULL,'70Lac','6157','6417','7000000','6157','6333.333333333333','176.7810026385223',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','North-East','36-Feet','Vitrified','Semifurnished','Yes','Yes','Sector-70','Mayank Roof Realtech','9599481837',NULL,NULL,NULL,'Maninder Singh/Real infra solution','9810802935','6000 to 6500rup/sqft',NULL,'Vikas/I Realty Propmart','9810307978','6500rup/sqft',NULL,'As per the confirmation he is broker final price is 70Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-tulip-orange-sector-70-gurgaon-1137-sq-ft-spid-Q26694155?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(136,'Owner','Sell','Apartment','Gurgaon','Anand Raj Madelia',NULL,'Manesar',NULL,NULL,'1772',NULL,'4','4','3',NULL,'14','10',NULL,NULL,NULL,'60Lac','3387','4377','6000000','3386','3250','-136.0045146726861',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Manesar','Roman Hashmi','9319134412',NULL,NULL,NULL,'Manoj  Chauhan/Shri Shani Properties','9990855998','3000 to 3500rup/sqft','said some dispute going on',NULL,NULL,NULL,NULL,'As per the confirmation Manesar Secter M2  some types of legal disputes gpong on so cant sale the properties',NULL,'http://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-anant-raj-madelia-manesar-gurgaon-1772-sq-ft-r2-spid-U27429941?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NTU2NjM2OTExMjQyICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA='),
(137,'Owner','Sell','Apartment','Gurgaon','Pareena Coban Residences',NULL,'Sector 99A',NULL,NULL,'1997',NULL,'3','3','3',NULL,'20','10',NULL,NULL,NULL,'1Cr','5008','5100','10000000','5008','5100','92.48873309964983','4900',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 99A','Kunal Tiwari','9829404933',NULL,'Tiwari.kt@gmail.com',NULL,'Manmohan Gupta/Brick ty Brick','8048094664','5000 to 5200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post final price is 4900rup/sqft',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-pareena-coban-residences-sector-99-a-gurgaon-1997-sq-ft-r7-spid-G21173222?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(138,'Owner','Sell','Apartment','Gurgaon','Pareena Coban Residences',NULL,'Sector 99A',NULL,NULL,'1550',NULL,'2','2',NULL,NULL,'20','6',NULL,'1 Covered',NULL,'80Lac','5162','5100','8000000','5161','5100','-61.2903225806449','5000',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 99A','A S Mazumdar','9540736726',NULL,'Asmazumdar@gmail.com',NULL,'Manmohan Gupta/Brick ty Brick','8048094664','5000 to 5200rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post final price is 5000rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-pareena-coban-residences-sector-99-a-gurgaon-1550-sq-ft-r7-spid-W20126029?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(139,'Owner','Sell','Apartment','Gurgaon','Puri Emerald Bay',NULL,'Sector-104',NULL,NULL,'1550',NULL,'2','2',NULL,NULL,'31','14',NULL,NULL,NULL,'1.2Cr','7742','7820','12000000','7742','7666.666666666667','-75.26881720430083','7250',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes',NULL,NULL,NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector-104','Divyanshu','9457125121',NULL,'Divyanshugoel2005@yahoo.co.in',NULL,'Manmohan Gupta/Brick ty Brick','8048094664','7500 to 8000rup/sqft',NULL,'Vijay Khurana/Finmart Realty','9990508289','7500rup/sqft',NULL,'As per confirmation ready to post property final price is 7250 to 7400rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-puri-emerald-bay-sector-104-gurgaon-1550-sq-ft-r7-spid-C17096737?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(140,'Owner','Sell','Apartment','Gurgaon','Forte Point the Olive Spire',NULL,'Sector-70A',NULL,'1388',NULL,NULL,'2','2','2',NULL,'12','8',NULL,'1 Covered',NULL,'70Lac','5044','5270','7000000','5043','5250','206.772334293948',NULL,NULL,NULL,'Under construction','Dec-18','Resale','Freehold',NULL,'Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 70A','Subhra Saha','9136041875',NULL,'Subhra_saha@yahoo.com',NULL,'Amrish Srivastava/Property station','8010222666','5000 to 5500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 70Lac',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-forte-point-the-olive-spire-sector-70-a-gurgaon-1388-sq-ft-r2-spid-E27248469?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NTU2NjM2OTExMjQyICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA='),
(141,'Owner','Sell','Apartment','Gurgaon','Ninex City',NULL,'Sector 76',NULL,'1952',NULL,NULL,'3','4','3','Servant Room','12','7',NULL,'1 Covered',NULL,'88Lac','4509','4335','8800000','4508','4250','-258.1967213114758',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes','Yes',NULL,'Full','Borewell/Tank','West','54-Feet','Vitrified','Semifurnished','Yes',NULL,'Sector 76','R S Chauhan','8802340684',NULL,'busy',NULL,'Amrish Srivastava/Property station','8010222666','4000 to 4500rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post final price is 85Lac',NULL,'http://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-ninex-city-sector-76-gurgaon-1952-sq-ft-r3-spid-F25562417?pos=SEARCH&sid=UiB8IFFTIHwgUyB8IzcjICB8IEhQIHwgWSB8IzE1IyAgfCBTRUFSQ0ggfCA2NTQ1NzUwMzgzNDk4OTYxICMyI3wgIHwgMjgyNDE1NDQsMjg4MzIxMjEgfCAgfCA4ICMxI3wgIHwgTyMzIyB8IFIgfCAgfCAxICMyI3wgIHwgTyMzNSMgfCA='),
(142,'Owner','Sell','Apartment','Gurgaon','Parkwood Westend',NULL,'Sector-92',NULL,'1200',NULL,NULL,'2','2','3','Study room','14','2',NULL,'1 Covered',NULL,'45Lac','3250','3315','4500000','3750','3500','-250','3100',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes','Yes','Yes','Yes',NULL,NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Full','Municipal Corporation','East','196.9 Feet','Vitrified','Semifurnished','Yes','Yes','Sector-92','Ashutosh Gupta','9910108279',NULL,'Ashutosh.gupta19@gmail.com',NULL,'Dinesh Kumar/Welcome Home','8510888888','3300rup/sqft to 3700rup/sqft',NULL,NULL,NULL,NULL,NULL,'As per the confirmation ready to post property final price is 3100rup/sqft',NULL,'http://www.99acres.com/2-bhk-bedroom-apartment-flat-for-sale-in-parkwood-westend-sector-92-gurgaon-1200-sq-ft-r1-spid-W27582047?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(143,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1776',NULL,NULL,'3','3','3','Servant Room','23','8',NULL,'1 Covered',NULL,'65','4043','3910','6200000','3491','3750','259.0090090090089','3490',NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,'196.9 Feet',NULL,'Semifurnished','Yes','Yes','Sector 92','Manju Goel','9953681243',NULL,'Manujgoel@gmail.com',NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,'As per the confirmation ready to post property.',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1608-sq-ft-r4-spid-D20311591?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(144,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1596','1400','1300','3','3','3','Servant Room','23','3',NULL,'1 Covered',NULL,'62','3884','3910','6200000','3885','3750','-134.7117794486217',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East','50- Feet','Vitrified',NULL,'Yes',NULL,'Sector 92','Siddharth','9717239595',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,NULL,NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1596-sq-ft-r2-spid-L28939297?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(145,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,NULL,'1261',NULL,'3','3',NULL,NULL,'23','12',NULL,'1 Covered',NULL,'47','3728','3910','4700000','3727','3750','22.79936558287091',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 92','Vineet','9990001696','9990001646','call after some time for mail Id',NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,'As per the confirmation ready to post property.',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1261-sq-ft-r4-spid-X22884073?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(146,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1870',NULL,NULL,'4','5','3','Servant Room','23','2',NULL,'1 Covered',NULL,'75','4010','3910','7500000','4011','3750','-260.6951871657752',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation',NULL,NULL,NULL,NULL,'Yes','Yes','Sector 92','Aman Sehra','9911272119',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,'Busy',NULL,'https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1870-sq-ft-spid-S30966513?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(147,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1851',NULL,NULL,'3','4','3','Servant Room','23','17',NULL,'1 Covered',NULL,'68','3674','3910','6800000','3674','3750','76.31010264721772',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 92','Satyam','8745002200',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,'Ringing',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1851-sq-ft-spid-X30732761?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(148,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1870',NULL,NULL,'4','5','3','Servant Room','23','9',NULL,'1 Covered',NULL,'65.45','3500','3910',NULL,'0','3750','3750',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'Park/Garden',NULL,NULL,'Unfurnished','Yes',NULL,'Sector 92','Rupak Kumar','9810866676',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,'Ringing',NULL,'https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1870-sq-ft-spid-R31219339?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(149,'Owner','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1660',NULL,NULL,'3','3','3',NULL,'23','18',NULL,'1 Covered',NULL,'59','3542','3910',NULL,'0','3750','3750',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','East',NULL,'Vitrified',NULL,'Yes',NULL,'Sector 92','Vinay','8860630818',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,'DNE',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1666-sq-ft-spid-N31246345?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(150,'Broker','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1666',NULL,NULL,'3','3','2','Servant Room','23','2',NULL,'1 Covered',NULL,'65','3902','3910',NULL,'0','3750','3750',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East','32.8-Feet','Vitrified','Semifurnished','Yes',NULL,'Sector 92','Peeyush Goyal/Pathways Consultion','9582532628',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,NULL,NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1666-sq-ft-r5-spid-D25883275?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(151,'Broker','Sell','Apartment','Gurgaon','Sare Crescent Parc','Sare Homes','Sector-92',NULL,'1666','1665','1664','3','4','3','Servant Room','23','2',NULL,'1 Covered',NULL,'63','3782','3910','6300000','3782','3750','-31.51260504201673',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,NULL,'Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East','60-Feet','Vitrified','Semifurnished','Yes',NULL,'Sector 92','Kamal/Aadhar Homes','7042000546',NULL,NULL,NULL,'Kamal/Aadhar Homes','7042000546','3750',NULL,'Peeyush Goyal/Pathways Consultion','9582532628','3750',NULL,NULL,NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sare-crescent-parc-sector-92-gurgaon-1666-sq-ft-spid-K30982103?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(152,'Owner','Sell','Apartment','Gurgaon','CHD Avenue 71','CHD Developers','Sector 71',NULL,'2193',NULL,NULL,'4','4','3','Servant Room','15','14',NULL,'1 Covered',NULL,'1.2','5472','5567','11500000','5244','5500','256.0419516643869',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,NULL,'Yes',NULL,'Sector 71','Sumita','9899694840',NULL,'Sumitakukreja@gmail.com',NULL,'Harish Mahna/Chaahat Homes','9560621391','5500',NULL,'Deepak Thareja/Schon Realtors PVT Ltd','9910006482','5500',NULL,'As per the confermation ready to post',NULL,'https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2193-sq-ft-r1-spid-E30620557?pos=SEARCH'),
(153,'Broker','Sell','Apartment','Gurgaon','CHD Avenue 71','CHD Developers','Sector 71',NULL,'2350',NULL,NULL,'4','4','3','Servant Room','15','9',NULL,'1 Covered',NULL,'1.2','5107','5567','12000000','5106','5500','393.6170212765956',NULL,NULL,'1 to 5 Years','Ready to Move',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes',NULL,NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','Park/Garden','196.9 Feet','Wood',NULL,'Yes',NULL,'Sector 71','Harish Mahna/Chaahat Homes','9560621391','9560621351',NULL,NULL,'Harish Mahna/Chaahat Homes','9560621391','5500',NULL,'Deepak Thareja/Schon Realtors PVT Ltd','9910006482','5500',NULL,NULL,NULL,'https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-chd-avenue-71-sector-71-gurgaon-2350-sq-ft-spid-M30984785?pos=SEARCH'),
(154,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95',NULL,'2212',NULL,NULL,'4','4','3','Pooja room & Servant Room','21','17',NULL,'1 Covered',NULL,'94','4250','4845','9400000','4250','4500','250.4520795660037',NULL,NULL,NULL,'Immediate',NULL,'Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,'Park/Garden',NULL,'Vitrified','Unfurnished','Yes','Yes','Sector 95','Jason Ford','9811811420',NULL,NULL,NULL,'Surender Sharma/Simply One Associates','9910780177',NULL,NULL,NULL,NULL,NULL,NULL,'busy',NULL,'https://www.99acres.com/4-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-2212-sq-ft-r5-spid-M26597752?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(155,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95',NULL,'1598',NULL,NULL,'3','3','3','Servant Room','21','8',NULL,'1 Covered',NULL,'70','4381','4845','7000000','4380','4500','119.5244055068833',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','Park/Garden',NULL,'Vitrified','Unfurnished','Yes',NULL,'Sector 95','Gautam Kaushik','9910734422',NULL,NULL,NULL,'Surender Sharma/Simply One Associates','9910780177',NULL,NULL,NULL,NULL,NULL,NULL,'busy',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1598-sq-ft-r6-spid-W25543221?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(156,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95',NULL,'1598',NULL,NULL,'3','4','3','Servant Room','21','1',NULL,'1 Covered',NULL,'58','3630','4845','5800000','3630','4500','870.4630788485606',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full','Municipal Corporation','North-East',NULL,'Vitrified','Semifurnished','Yes',NULL,'Sector 95','Manoj','7536862905',NULL,NULL,NULL,'Surender Sharma/Simply One Associates','9910780177',NULL,NULL,NULL,NULL,NULL,NULL,'Ringing',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1598-sq-ft-spid-O31474319?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(157,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95',NULL,NULL,'2034',NULL,'3','3','3',NULL,'21','3',NULL,'1 Covered',NULL,'85','4723','4845','8000000','3933','4500','566.8633235004918',NULL,NULL,NULL,'Under construction','Dec-17','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,NULL,'Unfurnished','Yes',NULL,'Sector 95','Rahul','9818033323',NULL,'aryanRahulin@gmail.com',NULL,'Surender Sharma/Simply One Associates','9910780177',NULL,NULL,NULL,NULL,NULL,NULL,'As per the confermation ready to post',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1800-sq-ft-r2-spid-Q28821715?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(158,'Owner','Sell','Apartment','Gurgaon','Sidhartha NCR Greens','Sidhartha Group Builders','Sector 95',NULL,NULL,'1598',NULL,'3','4','3','Servant Room','21','12',NULL,'1 Covered',NULL,'63.92','4000','4845','6392000','4000','4500','500',NULL,NULL,NULL,'Under construction','Mar-18','Resale','Freehold',NULL,'Yes','Yes','Yes','Yes','Yes',NULL,'Yes','Yes',NULL,NULL,NULL,NULL,'Yes','Yes','Yes','Yes',NULL,'Yes',NULL,'Full',NULL,NULL,NULL,'Vitrified','Semifurnished','Yes',NULL,'Sector 95','Neetu Arora','7838867389',NULL,NULL,NULL,'Surender Sharma/Simply One Associates','9910780177',NULL,NULL,NULL,NULL,NULL,NULL,'Ringing',NULL,'https://www.99acres.com/3-bhk-bedroom-apartment-flat-for-sale-in-sidhartha-ncr-greens-sector-95-gurgaon-1598-sq-ft-r9-spid-D9500205?from_src=XIDPAGE_det&pos=XIDDETVIEW'),
(159,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(160,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(161,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(162,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(163,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(164,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(165,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(166,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(167,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(168,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(169,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(170,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(171,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(172,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(173,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(174,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(175,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(176,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(177,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(178,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(179,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(180,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(181,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(182,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(183,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(184,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(185,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(186,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(187,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(188,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(189,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(190,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(191,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(192,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(193,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(194,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(195,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(196,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(197,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(198,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(199,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(200,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(201,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(202,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(203,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(204,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(205,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(206,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(207,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(208,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(209,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(210,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(211,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(212,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(213,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(214,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(215,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(216,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(217,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(218,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(219,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(220,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(221,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(222,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(223,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(224,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(225,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(226,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(227,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(228,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(229,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(230,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(231,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(232,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(233,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(234,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(235,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(236,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(237,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(238,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(239,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(240,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(241,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(242,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(243,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(244,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(245,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(246,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(247,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(248,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(249,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(250,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(251,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(252,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(253,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(255,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(256,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(257,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(258,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(259,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(260,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(261,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(262,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(263,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(264,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(265,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(266,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(267,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(268,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(269,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(270,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(271,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(272,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(273,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(274,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(275,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(276,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(277,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(278,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(279,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(280,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(281,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(282,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(283,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(284,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(285,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(286,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(287,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(288,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(289,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(290,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(291,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(292,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(293,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(294,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(295,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(296,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(297,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(298,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(299,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(300,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(301,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(302,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(303,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(304,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(305,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(306,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(307,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(308,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(309,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(310,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(311,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(312,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(313,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(314,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(315,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(316,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(317,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(318,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(319,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(320,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(321,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(322,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(323,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(324,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(325,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(326,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(327,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(328,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(329,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(330,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(331,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(332,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(333,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(334,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(335,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(336,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(337,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(338,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empId` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `is_valid` int(11) NOT NULL,
  `doj` varchar(255) NOT NULL,
  `employee_department_id` varchar(255) NOT NULL,
  `employee_role_id` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`empId`,`username`,`password`,`profile_pic`,`mobile`,`address`,`email`,`parent_id`,`lft`,`rght`,`is_valid`,`doj`,`employee_department_id`,`employee_role_id`,`created`,`modified`) values 
(8,'101','admin','$2a$10$3wmK1HHbd0Wt77zZVKGCWeeJtAoNBMNidma/y6QPdgel6it3TU8f.','58cbfc8611ce9.jpg','1234123400','def road','admin@fairpockets.com',0,1,2,1,'02/26/2017','1','0','2017-03-17 11:08:56','2017-10-05 05:11:25');

/*Table structure for table `websiteusers` */

DROP TABLE IF EXISTS `websiteusers`;

CREATE TABLE `websiteusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(100) NOT NULL,
  `usermobile` varchar(15) NOT NULL,
  `userorgname` varchar(100) DEFAULT NULL,
  `useractive` int(11) DEFAULT NULL,
  `userrole` int(1) DEFAULT NULL,
  `acccreatedate` datetime DEFAULT NULL,
  `accmoddate` datetime DEFAULT NULL,
  `userlastlogin` datetime DEFAULT NULL,
  `regsitration_token` varchar(255) NOT NULL,
  `email_verify` int(11) NOT NULL DEFAULT '0',
  `otp_verify` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_userrole` (`userrole`),
  CONSTRAINT `FK_userrole` FOREIGN KEY (`userrole`) REFERENCES `mst_userrole` (`userrole_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Data for the table `websiteusers` */

insert  into `websiteusers`(`id`,`username`,`password`,`email`,`usermobile`,`userorgname`,`useractive`,`userrole`,`acccreatedate`,`accmoddate`,`userlastlogin`,`regsitration_token`,`email_verify`,`otp_verify`) values 
(0,'ritesh','$2a$10$2IuDO.tHM07zTdDlMOE/WuA4JTOz0IyVAqbzMD/fgc6Lo2B9v1qti','riteshanand00@gmail.com','9710000000','abcdf',1,3,'2017-10-20 22:26:22','2017-10-20 22:26:22',NULL,'4d70a0974ad8204cf67cc75cdfa2864e',1,0),
(1,'Owner','$2a$10$RCDSp2LG8qzDiG46EJTg8.jq2zmh/CBS5J8S4JE5NjrFi0d4tKPLS','rajniofficial@gmail.com','777777777777777','',1,3,'2017-04-14 01:40:34','2017-04-14 01:40:34',NULL,'31c47c8c84da865ad085272b6da1bad4',1,0),
(2,'Broker','$2a$10$EqiU8yDBTJ5SGr7jvBZAd.FH2kBlMpQAFlU5BJ/ty789cXaFUXX6u','nasir.jawed1@aliveasset.com','9899614666','nasirbuilder',1,3,'2017-04-10 02:14:24','2017-04-10 02:14:24',NULL,'170a8bef90f72240d220532d6ed2d28d',0,0),
(3,'Builder','$2a$10$ycYuE7fndy1mnWZnbCc.feNF5r2qz7gqWSLKrw4S3Z7LQ9W/pGopG','fairpocketstech@gmail.com','9898989898','',1,1,'2017-04-11 02:31:26','2017-04-11 02:31:26',NULL,'e3ef84e367ad243bb0a688a4abc3e1a3',1,0),
(9,'nasirjawed','$2a$10$EqiU8yDBTJ5SGr7jvBZAd.FH2kBlMpQAFlU5BJ/ty789cXaFUXX6u','nasir.jawed@aliveasset.com','98909090','',1,1,'2017-04-11 03:08:35','2017-04-11 03:08:35',NULL,'0420f7a97149db526571b7f936f24355',0,0),
(13,'augment internet','$2a$10$EqiU8yDBTJ5SGr7jvBZAd.FH2kBlMpQAFlU5BJ/ty789cXaFUXX6u','augmentinternet1@gmail.com','12341298','UNITECH',1,3,'2017-07-28 20:36:12','2017-07-28 20:36:12',NULL,'81dc457efd9bf9bdca811d6a0da387c5',1,0),
(14,'Augment Internet','$2a$10$NzXV.oJhD/cvMVWJsVesbeKn6BOUJmh1wetHk4ZNKJySaUljvIGn.','augmentinternet@gmail.com','67676767','bjp india party',1,1,'2017-08-31 12:23:56','2017-08-31 12:23:56',NULL,'428d8d60b3d8587d7dea8ae8f80ee61c',1,0),
(15,'lol','$2a$10$g9xPlvVa7g.kZBmqJMHVt.zK7RMNbElcX1UG7hTYb.3EL/8JWrRwO','lol@yahoo.com','9090909090','',0,1,'2017-04-16 01:37:04','2017-04-16 01:37:04',NULL,'6acbeef3e0f613d5e8b988f64dfa33eb',1,0),
(16,'ritesh','$2a$10$tsW/gIa3EYUYJilmp.NXi.QgoDXpz4biXrUWavuB1sg8eDsekHf4e','riteshanand02@gmail.com','9717006821','',1,1,'2017-10-13 19:54:18','2017-10-13 19:54:18',NULL,'69b335a446fa0d3952c151eaa99e9ac7',1,0),
(17,'ritesh','$2a$10$6JiNPHQ7K/6ljI0rWXV6Teq.iQk0W/NgSljP/eASnk1hjISCXgAcq','ritesh.anand@gmail.com','900000000','',0,1,'2017-04-18 00:22:54','2017-04-18 00:22:54',NULL,'d857d07aec816aa179f2c8e44877a305',0,0),
(18,'Nasir Jawed','$2a$10$Y8fUQTWEhUo.BTcjb4GixO6nuPWq3LQl/K96kYn1UGSlI7IDsfL4C','nasir.jawed@fairpockets.com','9811111111','Alive Asset',1,3,'2017-04-18 03:53:07','2017-04-18 03:53:07',NULL,'4762110aa7c5579da0f456cec719d439',1,0),
(19,'asssasd','$2a$10$KwhkZrv/1NHFDNcuE4HGZefoEBXaceqqrS9Y55JfAhF88UU59WdZa','sada@gnvn.com','45456456565','Saicosys',0,1,NULL,NULL,NULL,'adw3r234sfrw',0,0),
(20,'Roomy','$2a$10$GasqP.iRfIpjXMAp.mCShOvp7pvpYymdr46pHeDGQkaknS4lZdS7.','srumki@gmail.com','931396259500000','',1,1,'2017-07-20 11:03:05','2017-07-20 11:03:05',NULL,'56545e0470150c20d6c1908061579d58',1,0),
(21,'bharat','$2a$10$Uh6QwSnZscr2ShkZyJHJhe64EyGU/jz4gWxNyGUmqDcbMANiDkyUm','bharat.sharma6feb@gmail.com','9650965993','',1,1,'2017-04-25 00:08:36','2017-04-25 00:08:36',NULL,'7542c3fcae4f28b85a2bfbe8ae183d70',1,0),
(22,'Ashwani','$2a$10$GPZ8fFN2j8AJDVB9oOc6z.wCPh4OSbPhiaJFydRjuud60weHuOVsq','ashwani.kumar@aliveasset.com','9871854733','',0,1,'2017-04-18 04:51:58','2017-04-18 04:51:58',NULL,'bb9fd2bb2d759c63bb98def68f2f1cc9',0,0),
(23,'bharat','$2a$10$O3h8bTRa1kZ8GZJ6awHei.46.Rv7BH0Z0DA5jIH/TnoKf2gf8VpGG','bharat.sharma@fairpockets.com','8920203622','',0,1,'2017-04-18 04:57:01','2017-04-18 04:57:01',NULL,'8563d07176e33c1de24ead657c252664',0,0),
(24,'xyz','$2a$10$f/EYo4YdEvHXEVUNtxU9mep9xDnWintcAi03oGXzMuh/J9jXiAbs2','rumki.sengupta@aliveasset.com','9999988888','khaya',0,3,'2017-04-18 05:45:44','2017-04-18 05:45:44',NULL,'520b6f730102e2cf2b3b8379646ea17a',0,0),
(25,'Vikas','$2a$10$YIY68Vd8eaL5.GozrLw8z.qsowFkaREAVWV9bTokuCN37Rw6aF6ca','thefairpockets@gmail.com','9560685484','propequity.com',1,2,'2017-04-18 06:00:37','2017-04-18 06:00:37',NULL,'0ea7d9c1fb409be980a3813f4099fbc9',1,0),
(26,'bharat','$2a$10$tJENlvGnL8k3FvOF/z0owePljlZSsboylS/uPwQF5Fj1.AwWDlT8u','bharat.sharma6feb@outlook.com','9650445661','aliveasset',1,3,'2017-05-01 23:53:54','2017-05-01 23:53:54',NULL,'4f840b71feb6e447357c7f7b28526d7a',1,0),
(27,'Rumki','$2a$10$9x8b.G/ps/wtpCTLvDgFmutd4DN/WEdpMlKDPP3bgj5raN1jHuPDq','ritesh.anand@aliveasset.com','9999888877','Alive Asset',0,3,'2017-04-25 00:21:23','2017-04-25 00:21:23',NULL,'c844ba7948156a930018800012e5aa01',0,0),
(28,'kjkjh','$2a$10$9Uhw/nYATb3lIzqnHkb5.O2bNztkEAig/Bxsuwi0dGq3B2MhebpVe','udaywebdesigner@gmail.com','9717006822','kjhkjh',1,3,'2017-04-30 03:59:43','2017-04-30 03:59:43',NULL,'c286c829c7f1f4e903eabe72a9965716',1,0),
(29,'xyz','$2a$10$72aQv6DopBc3EIFzBmLn/.wQf.Vn6OOMHh6Qmgj.3PxfoxEqDfwMy','srumki@outlook.com','9999999999','tomato',1,3,'2017-05-01 01:04:56','2017-05-01 01:04:56',NULL,'af3c0ce11c5394e6b9847ccc0750d168',1,0),
(30,'akai','$2a$10$ifTgPhbTX7hepIQxunTGdug0pb76qe6BQM..Tgsl9J5tgTVF5cfx2','rooms@outlook.com','88888888888','okooko',0,3,'2017-05-17 23:33:01','2017-05-17 23:33:01',NULL,'2101854ab825ce848f3edd876b28005b',0,0),
(31,'rajni','$2a$10$BobGiTQbhPU1BL4vXq1i3OoDSAWFyHIaxPyh5qSSfApN1lM40K8xe','rajniphp@gmail.com','976433567899','',1,1,'2017-05-22 23:41:46','2017-05-22 23:41:46',NULL,'d2e8be725dc1bb924b1197ed0458e029',1,0),
(32,'Test','$2a$10$ENKLtaClI0bGAytHDKa7I.C6tHA8ZMaPsDLBQNLwU/llJYz/sDFFW','Test@test.com','4236499236','TestOrg',0,2,'2017-06-20 11:13:10','2017-06-20 11:13:10',NULL,'9fb2462c54b2ec2821bf3c2e548a98cd',0,0),
(33,'N S','$2a$10$hln1peWRQTPSa2usIV1bcuOtjpvy485YRzPfjsXbOyvL/HBWwr1ZO','nneetis@gmail.com','96506914953','',1,1,'2017-06-20 11:25:12','2017-06-20 11:25:12',NULL,'9e53301207e2736c5526f6dfc1a5be30',1,0),
(34,'test','$2a$10$6vurm1Yj2qC.KHwBcUUThOb6JOA4wc6zOTHxsWUuGarpd0OexWcLK','admin@gmail.com','33333333333','',0,1,'2017-07-11 14:41:27','2017-07-11 14:41:27',NULL,'5092410dfdf3b20d0aa7c4dac41aabea',0,0),
(35,'Ritesh','$2a$10$ZLzo6ArJAqHuyPNChmgPdeqwo7vU34QX.173R9vh/HBZ9SP0JeKfm','verma_ritesh@hotmail.com','9879879877','',0,1,'2017-07-24 16:01:22','2017-07-24 16:01:22',NULL,'f1dac679ff093274e4e1a6ea0e0f0113',0,0),
(36,'kj','$2a$10$yqguNbzE7nG7ml/VTusLb.SOm/rI.hVM32iH14wjEECd.mHVvzOYi','ritesh@gmail.com','9879809877','',0,1,'2017-07-24 16:20:25','2017-07-24 16:20:25',NULL,'642199bb0c80a2d231855a5e4483bb4f',0,0),
(37,'Test Augment','$2a$10$7vTZ7B90nm86OnfzHgzVqOxlNeYy9q8GOqalomtlyGBjUYWMkzFwC','test.augmentinternet@gmail.com','1234678901','Augment',1,3,'2017-08-02 16:51:06','2017-08-02 16:51:06',NULL,'b8ab9a72983ead4c8c8a477531567cb0',1,0),
(38,'Thulu Singh','$2a$10$QguqAafmqpz3DICqBGDpLelGWRtXHFq2Lj45dgVuImfhCrQFnLyqy','Thulu@gmail.com','9871312909','',0,1,'2017-08-17 22:01:34','2017-08-17 22:01:34',NULL,'40cc6d897487702e1860cabe1f808579',0,0),
(39,'abcd','$2a$10$MYQX/j4pJNyUFZeOW7RqeeOUDoFqN0RSksfrYTmiVnik7lTz.03Oi','abcd@gmail.com','1234567890','',0,1,'2017-08-19 22:48:32','2017-08-19 22:48:32',NULL,'151a0b0ed9e09a2e71581cddd01b2dc4',0,0),
(40,'Amit','$2a$10$j6nnOazG0jZY2PzPoVGape15ZugcjMt17aDvAnysrDm01IVW/srhi','amit@saicosys.com','9999134288','',1,1,'2017-10-11 05:30:03','2017-10-11 05:30:03',NULL,'38d3149a300b536ced39907386e92282',1,0),
(41,'Ritesh ','$2a$10$NRCN2wocYgsxV/qM5nStPOBve7QoNPtVNh70ew2s3JWny8CELdtWm','info@aliveasset.com','9717006021','abc pvt lts',1,2,'2017-09-02 11:18:03','2017-09-02 11:18:03',NULL,'8f83e1a29d58a2955b97bb4231aa2120',1,0),
(42,'Amit Shokeen','$2a$10$.9rrUO1R45si.1QQt90Up.RNNPAn0R.kBHZ4o.GwU5cwhcETgVtQi','amitshokeen.dev@gmail.com','9999134277','',1,1,'2017-09-26 20:43:44','2017-09-26 20:43:44',NULL,'5f2bad36f87262bffd978ca517d4c24a',1,0),
(43,'Gurpreet','$2a$10$dZpP03Iu28h9dAidfLs40uZse.dC7dndkveYwGWbAf0FVriYZOOje','gurpreet@saicosys.com','9999994259','',1,1,'2017-10-17 15:12:08','2017-10-17 15:12:08',NULL,'6071999142bbbf7552d032bf02541d6f',1,0),
(44,'Ganesh Pandey','$2a$10$/79kW4RY0e/mw2yoSFVPc.QgH8/1YOkSASgN24RJst8kbOGB1fkUa','gannyguy123@yahoo.ca','7838848812','',1,1,'2017-12-04 15:50:04','2017-12-04 15:50:04',NULL,'cc3dcfba44ed8a838bc713be60a56041',1,0),
(45,'Rajeev','$2a$10$DD1jA92KmQRkh38yuvwvs.3WtHg.RXETjkmHpWZG8Wiwul.bR/SMC','rajeevelite2017@gmail.com','9560095803','GolfFreen Mansions Pvt Ltd.',1,3,'2017-10-09 11:14:36','2017-10-09 11:14:36',NULL,'983b02c36d49ac51207573ce05069243',1,0),
(46,'Ganesh Pandey','$2a$10$42nU4Q.1LHSAaKW1Z3yEMeSYFLttYGF4yGLYl5CtLP3DMOu6Tg48C','gannyguy@gmail.com','7838848813','Abc Pvt Ltd',1,2,'2017-10-11 11:03:23','2017-10-11 11:03:23',NULL,'d2914645276f88dbbe29be3cc7f9e240',1,0),
(47,'Gurpreet','$2a$10$VXNHie2FuvKmZlirdpQ9reIKp1hSmAcQAJBRnJPnRKOy2fZvHoY8m','info@saicosys.com','9999999123','Saicosys',0,2,'2017-10-12 03:14:33','2017-10-12 03:14:33',NULL,'022268f1135cf7f87a0c05625795afb2',0,0),
(48,'Sandeep','$2a$10$.5rWOyAcRGYnR0BbCCeoDu6kb5TMv0eG8cS2bZAKcVkdSZJitxUOG','sandeepk@saicosys.com','9874562130','Saicosys',0,3,'2017-10-12 03:19:49','2017-10-12 03:19:49',NULL,'79a65eecd62e10733c6194d5d65c8ceb',0,0),
(49,'Guru Individual','$2a$10$RK8Ow1lhngokMKddBut4auwqNG/P8ZxUJnoINCdEK7HTXuo9ppPBe','guru@yopmail.com','1234567899','',1,1,'2017-10-13 02:31:48','2017-10-13 02:31:48',NULL,'2330fd34b11c59807f4ec5dec6547fe2',1,0),
(50,'chandra','$2a$10$5VJkXOKngRVGaOcHrgmmb.EilLIo5I6NuGWLycI/UAGh.T.oYvBte','chndra87@gmail.com','8745911738','',0,1,'2017-10-18 14:37:57','2017-10-18 14:37:57',NULL,'c759862f04931b80061f492fc4ae1556',0,0),
(51,'chandra','$2a$10$gRsHHUPva2GOh4mC8RsBiOQlVROk6pjOje1Zs9N2.8nIpKN33dD3e','chndrak87@gmail.com','8083334498','',1,1,'2017-10-18 14:46:18','2017-10-18 14:46:18',NULL,'f8f6db45ad2ee0eec2faee047f4a5c99',1,0),
(56,'gurpreet','$2a$10$BSVtSQQ1FjZ9I7D99kJdduBpOiLmubcGnnQCGsSmmWfd/l4VPvQim','guru222@yopmail.com','8800122210','',0,1,'2017-10-21 18:16:11','2017-10-21 18:16:11',NULL,'44d8af7ad154b86611eb5cfb041532f3',0,0),
(57,'Sid Murari','$2a$10$W76mnaTi8bobfS0cTV7VNuCWS/ii87cfN05dWxBhuImg.2xTUPvk6','s3@gmail.com','9674544031',NULL,1,1,'2017-11-19 20:22:46','2017-11-19 20:22:46',NULL,'135f55344495a109da5f648dd2b4f971',1,0),
(58,'Skjdsfhadsj fhdskf','$2a$10$T5OEm6IXOzv0xm1IfOmH3Oqj53cGsznXqKLLO1itRFymsXaYBhazm','s2@gmail.com','9674544032',NULL,1,1,'2017-11-22 01:08:17','2017-11-22 01:08:17',NULL,'fdc2e26f68994554b106f99f2e2e9e12',1,0),
(59,'Siddhartha Murari','$2a$10$seQN5YJFWg.Ow5lYh3zsiOluFjWkTDTfWjm7WbRb1Qynq2RWuhDB.','s1@gmail.com','9674544031','some famous org',1,3,'2017-12-02 08:40:23','2017-12-02 08:40:23',NULL,'281b7e538244dbe671db93e32f8a517f',1,0),
(60,'Sid Murari','$2a$10$zAia0Y3HekOZMd7dOuYfteqXBbIvFfOBXW/2gyfFz5RDjIZzeCXo6','s4@gmail.com','8961112539',NULL,NULL,1,'2017-12-01 06:57:49','2017-12-01 06:57:49',NULL,'f1c4f395b9e330053865db519353d7f9',0,0),
(61,'Sid Builder','$2a$10$IMcR6jMfGIOPkec6mrU1peFDSaLYIbhMQzxNJxp8/.K.2eMDiE0b2','sidart101@gmail.com','9674544035','sid builders pvt. ltd.',1,3,'2017-12-02 23:45:51','2017-12-02 23:45:51',NULL,'94e359929ca3b6d411082b60c4ad4566',1,0),
(62,'skhan','$2a$10$//WnpKmEGRBzcRG4YR4SeO0Ut6E0JHDfd/xMRPhjwcTsaqOoFPSFS','sudip.btec@gmail.com','9432993776','khan orgg',1,3,'2017-12-06 21:29:05','2017-12-06 21:29:05',NULL,'2c2b095eb74ee18802850c3f628efcd4',1,0),
(63,'Varun','$2a$10$dkcSDw224qarW5OUKtH3hOtHaAvBPOhBHmO7nnTgrYq1PMN7hfHEW','varunchoudhary_2007@yahoo.co.in','9971992627',NULL,1,1,'2017-12-06 09:36:31','2017-12-06 09:36:31',NULL,'b8c4e840ba13d51ee2829033e7641df2',1,0);

/*Table structure for table `widgets` */

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `part_no` varchar(12) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `widgets` */

/* Procedure structure for procedure `USP_FEATURE_COL_ROW` */

/*!50003 DROP PROCEDURE IF EXISTS  `USP_FEATURE_COL_ROW` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `USP_FEATURE_COL_ROW`( properties_ID INT,STR_features VARCHAR(200))
BEGIN
  
SELECT DISTINCT properties_ID,
  SUBSTRING_INDEX(SUBSTRING_INDEX(STR_features, ',', numbers.n), ',', -1) 
  NAME
FROM (
   SELECT @rownum := @rownum + 1 AS n
   FROM  `properties`
   CROSS JOIN (SELECT @rownum := 0) r
) numbers ;
END */$$
DELIMITER ;

/*Table structure for table `v_amenity_property_options` */

DROP TABLE IF EXISTS `v_amenity_property_options`;

/*!50001 DROP VIEW IF EXISTS `v_amenity_property_options` */;
/*!50001 DROP TABLE IF EXISTS `v_amenity_property_options` */;

/*!50001 CREATE TABLE  `v_amenity_property_options`(
 `amenity_id` int(11) ,
 `amenity_name` varchar(255) ,
 `propertyOptionId` longblob ,
 `is_active` int(11) 
)*/;

/*Table structure for table `v_feature_property_options` */

DROP TABLE IF EXISTS `v_feature_property_options`;

/*!50001 DROP VIEW IF EXISTS `v_feature_property_options` */;
/*!50001 DROP TABLE IF EXISTS `v_feature_property_options` */;

/*!50001 CREATE TABLE  `v_feature_property_options`(
 `feature_id` int(11) ,
 `featurename` varchar(255) ,
 `propertyOptionId` longblob ,
 `active_feature` int(11) 
)*/;

/*Table structure for table `v_geo_location_hierarchi` */

DROP TABLE IF EXISTS `v_geo_location_hierarchi`;

/*!50001 DROP VIEW IF EXISTS `v_geo_location_hierarchi` */;
/*!50001 DROP TABLE IF EXISTS `v_geo_location_hierarchi` */;

/*!50001 CREATE TABLE  `v_geo_location_hierarchi`(
 `citycode` int(11) ,
 `cityName` varchar(255) ,
 `stateCode` int(11) ,
 `stateName` varchar(255) ,
 `regionCode` int(11) ,
 `regionName` varchar(255) ,
 `countryCode` int(11) ,
 `countryName` varchar(255) 
)*/;

/*Table structure for table `v_properties_amenitie` */

DROP TABLE IF EXISTS `v_properties_amenitie`;

/*!50001 DROP VIEW IF EXISTS `v_properties_amenitie` */;
/*!50001 DROP TABLE IF EXISTS `v_properties_amenitie` */;

/*!50001 CREATE TABLE  `v_properties_amenitie`(
 `property_id` bigint(11) ,
 `amenities` longtext 
)*/;

/*Table structure for table `v_properties_features` */

DROP TABLE IF EXISTS `v_properties_features`;

/*!50001 DROP VIEW IF EXISTS `v_properties_features` */;
/*!50001 DROP TABLE IF EXISTS `v_properties_features` */;

/*!50001 CREATE TABLE  `v_properties_features`(
 `property_id` bigint(11) ,
 `features` longtext 
)*/;

/*Table structure for table `v_property` */

DROP TABLE IF EXISTS `v_property`;

/*!50001 DROP VIEW IF EXISTS `v_property` */;
/*!50001 DROP TABLE IF EXISTS `v_property` */;

/*!50001 CREATE TABLE  `v_property`(
 `property_id` bigint(11) ,
 `user_id` int(11) ,
 `employee_id` int(11) ,
 `Employee ID` int(11) ,
 `project_id` bigint(11) ,
 `transaction_type_id` int(11) ,
 `offer_price` varchar(255) ,
 `offer_price_sqt` int(11) ,
 `market_price` varchar(255) ,
 `expected_monthly` varchar(255) ,
 `market_rent` varchar(255) ,
 `deposit` varchar(255) ,
 `market_price_sqt` int(11) ,
 `maintance_amount` int(11) ,
 `purchase_price` varchar(255) ,
 `address` text ,
 `lat` varchar(255) ,
 `lng` varchar(255) ,
 `block` varchar(255) ,
 `locality` varchar(255) ,
 `city_data` varchar(255) ,
 `property_city` int(11) ,
 `state_data` varchar(255) ,
 `country_data` varchar(255) ,
 `pincode` int(11) ,
 `description` text ,
 `property_type_id` int(11) ,
 `possession_date` varchar(255) ,
 `booking_amount` varchar(255) ,
 `availability_id` int(11) ,
 `ownership_type_id` int(11) ,
 `sale_type` int(11) ,
 `construction_status` enum('1','2') ,
 `features_id` int(11) ,
 `amenities_id` int(11) ,
 `is_active` int(11) ,
 `status_id` int(11) ,
 `valuation_id` int(11) ,
 `approvalDT` date ,
 `created_date` timestamp ,
 `Dealer_id_1` int(11) ,
 `Dealer_id_2` int(11) ,
 `Dealer_id_3` int(11) ,
 `Dealer_quoted_Price` varchar(250) ,
 `userrole_ID` int(11) ,
 `identity_Code` varchar(90) ,
 `features` longtext ,
 `amenities` longtext 
)*/;

/*Table structure for table `v_temp_properties_features` */

DROP TABLE IF EXISTS `v_temp_properties_features`;

/*!50001 DROP VIEW IF EXISTS `v_temp_properties_features` */;
/*!50001 DROP TABLE IF EXISTS `v_temp_properties_features` */;

/*!50001 CREATE TABLE  `v_temp_properties_features`(
 `properties_id` int(11) ,
 `features_name` varchar(90) ,
 `features_Value` varchar(90) ,
 `Unit` varchar(90) 
)*/;

/*View structure for view v_amenity_property_options */

/*!50001 DROP TABLE IF EXISTS `v_amenity_property_options` */;
/*!50001 DROP VIEW IF EXISTS `v_amenity_property_options` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_amenity_property_options` AS select `ap`.`amenity_id` AS `amenity_id`,`am`.`amenity_name` AS `amenity_name`,group_concat(`ap`.`property_type_id` separator ',') AS `propertyOptionId`,`am`.`is_active` AS `is_active` from (`mst_amenity_propertyoption` `ap` join `mst_amenities` `am` on((`ap`.`amenity_id` = `am`.`amenity_id`))) group by `ap`.`amenity_id`,`am`.`amenity_name` */;

/*View structure for view v_feature_property_options */

/*!50001 DROP TABLE IF EXISTS `v_feature_property_options` */;
/*!50001 DROP VIEW IF EXISTS `v_feature_property_options` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_feature_property_options` AS select `ap`.`feature_id` AS `feature_id`,`am`.`featureName` AS `featurename`,group_concat(`ap`.`property_type_id` separator ',') AS `propertyOptionId`,`am`.`active_feature` AS `active_feature` from (`mst_feature_propertyoption` `ap` join `mst_features` `am` on((`ap`.`feature_id` = `am`.`feature_id`))) group by `ap`.`feature_id`,`am`.`featureName` */;

/*View structure for view v_geo_location_hierarchi */

/*!50001 DROP TABLE IF EXISTS `v_geo_location_hierarchi` */;
/*!50001 DROP VIEW IF EXISTS `v_geo_location_hierarchi` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_geo_location_hierarchi` AS select `acm`.`cityCode` AS `citycode`,`acm`.`cityName` AS `cityName`,`acm`.`stateCode` AS `stateCode`,`asm`.`stateName` AS `stateName`,`arm`.`regionCode` AS `regionCode`,`arm`.`regionName` AS `regionName`,`acom`.`countryCode` AS `countryCode`,`acom`.`countryName` AS `countryName` from (((`mst_area_cities` `acm` join `mst_area_states` `asm` on((`acm`.`stateCode` = `asm`.`stateCode`))) join `mst_area_regions` `arm` on((`asm`.`regionCode` = `arm`.`regionCode`))) join `mst_area_countries` `acom` on((`arm`.`countryCode` = `acom`.`countryCode`))) */;

/*View structure for view v_properties_amenitie */

/*!50001 DROP TABLE IF EXISTS `v_properties_amenitie` */;
/*!50001 DROP VIEW IF EXISTS `v_properties_amenitie` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_properties_amenitie` AS select `pa`.`property_id` AS `property_id`,group_concat(concat('"',`ma`.`alias_name`,'":"',`pa`.`amenity_value`,'"') separator ',') AS `amenities` from (`property_amenities` `pa` join `mst_amenities` `ma` on((`pa`.`property_amenity_id` = `ma`.`amenity_id`))) group by `pa`.`property_id` */;

/*View structure for view v_properties_features */

/*!50001 DROP TABLE IF EXISTS `v_properties_features` */;
/*!50001 DROP VIEW IF EXISTS `v_properties_features` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_properties_features` AS select `pf`.`properties_id` AS `property_id`,group_concat(concat('"',`mf`.`Alias_Name`,'":"',`pf`.`features_Value`,'"') separator ',') AS `features` from (`property_features` `pf` join `mst_features` `mf` on((`pf`.`features_id` = `mf`.`feature_id`))) group by `pf`.`properties_id` */;

/*View structure for view v_property */

/*!50001 DROP TABLE IF EXISTS `v_property` */;
/*!50001 DROP VIEW IF EXISTS `v_property` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_property` AS select `p`.`property_id` AS `property_id`,`p`.`user_id` AS `user_id`,`p`.`employee_id` AS `employee_id`,`p`.`Employee ID` AS `Employee ID`,`p`.`project_id` AS `project_id`,`p`.`transaction_type_id` AS `transaction_type_id`,`p`.`offer_price` AS `offer_price`,`p`.`offer_price_sqt` AS `offer_price_sqt`,`p`.`market_price` AS `market_price`,`p`.`expected_monthly` AS `expected_monthly`,`p`.`market_rent` AS `market_rent`,`p`.`deposit` AS `deposit`,`p`.`market_price_sqt` AS `market_price_sqt`,`p`.`maintance_amount` AS `maintance_amount`,`p`.`purchase_price` AS `purchase_price`,`p`.`address` AS `address`,`p`.`lat` AS `lat`,`p`.`lng` AS `lng`,`p`.`block` AS `block`,`p`.`locality` AS `locality`,`p`.`city_data` AS `city_data`,`p`.`property_city` AS `property_city`,`p`.`state_data` AS `state_data`,`p`.`country_data` AS `country_data`,`p`.`pincode` AS `pincode`,`p`.`description` AS `description`,`p`.`property_type_id` AS `property_type_id`,`p`.`possession_date` AS `possession_date`,`p`.`booking_amount` AS `booking_amount`,`p`.`availability_id` AS `availability_id`,`p`.`ownership_type_id` AS `ownership_type_id`,`p`.`sale_type` AS `sale_type`,`p`.`construction_status` AS `construction_status`,`p`.`features_id` AS `features_id`,`p`.`amenities_id` AS `amenities_id`,`p`.`is_active` AS `is_active`,`p`.`status_id` AS `status_id`,`p`.`valuation_id` AS `valuation_id`,`p`.`approvalDT` AS `approvalDT`,`p`.`created_date` AS `created_date`,`p`.`Dealer_id_1` AS `Dealer_id_1`,`p`.`Dealer_id_2` AS `Dealer_id_2`,`p`.`Dealer_id_3` AS `Dealer_id_3`,`p`.`Dealer_quoted_Price` AS `Dealer_quoted_Price`,`p`.`userrole_ID` AS `userrole_ID`,`p`.`identity_Code` AS `identity_Code`,`vpf`.`features` AS `features`,`vpa`.`amenities` AS `amenities` from ((`properties` `p` left join `v_properties_features` `vpf` on((`p`.`property_id` = `vpf`.`property_id`))) left join `v_properties_amenitie` `vpa` on((`p`.`property_id` = `vpa`.`property_id`))) group by `p`.`property_id` */;

/*View structure for view v_temp_properties_features */

/*!50001 DROP TABLE IF EXISTS `v_temp_properties_features` */;
/*!50001 DROP VIEW IF EXISTS `v_temp_properties_features` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_temp_properties_features` AS select `tpf`.`properties_id` AS `properties_id`,`tpf`.`features_name` AS `features_name`,`tpf`.`features_Value` AS `features_Value`,(case `tpf`.`features_name` when 'sbua' then (select `temp_properties_features`.`features_Value` from `temp_properties_features` where ((`temp_properties_features`.`properties_id` = `tpf`.`properties_id`) and (`temp_properties_features`.`features_name` = 'sbua_m'))) when 'build_up_area' then (select `temp_properties_features`.`features_Value` from `temp_properties_features` where ((`temp_properties_features`.`properties_id` = `tpf`.`properties_id`) and (`temp_properties_features`.`features_name` = 'build_up_sq'))) when 'carpet_area' then (select `temp_properties_features`.`features_Value` from `temp_properties_features` where ((`temp_properties_features`.`properties_id` = `tpf`.`properties_id`) and (`temp_properties_features`.`features_name` = 'carpet_area_sq'))) when 'width_of_fac_road' then (select `temp_properties_features`.`features_Value` from `temp_properties_features` where ((`temp_properties_features`.`properties_id` = `tpf`.`properties_id`) and (`temp_properties_features`.`features_name` = 'width_of_sq'))) when 'plot_area' then (select `temp_properties_features`.`features_Value` from `temp_properties_features` where ((`temp_properties_features`.`properties_id` = `tpf`.`properties_id`) and (`temp_properties_features`.`features_name` = 'plot_area_sq'))) end) AS `Unit` from `temp_properties_features` `tpf` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
