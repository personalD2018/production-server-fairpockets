<?php

App::uses('CakeEmail', 'Network/Email');
class AccountsController extends AppController {

    public $uses = array(
        'Builder', 
        'Websiteuser', 
        'BuilderApprovalNotifiction', 
        'User', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState', 
        'MstAreaCountry', 
        'MstAreaRegion', 
        'MstAreaState', 
        'MstAreaCity', 
        'MstAreaLocality',
        'BuyRentRequirement', 
        'ServiceNotifiction', 
        'Property', 
        'AsstSellRent', 
        'AsstSellLead', 
        'ResearchReport', 
        'ReasearchreportPic', 
        'Portfolio', 
        'PortService', 
        'MstPropertyTypeOption', 
        'Portmgmt', 
        'PropService', 
        'PropServicestage', 
        'PropServicephoto', 
        'OrderHistory', 
		'PropertyFeature',
        'UploadedPropertyDetail',
		'MstUserrole',
		'Lead',
		'Freeactivation',
		'Project','add_sales_employees',
		'InventoryManager.InvProject',
        'InventoryManager.InvProperty',
        'InventoryManager.InvInventory',
        'InventoryManager.InvInventoryPropertyStatus',
        'InventoryManager.InvInventoryPropertyPosition',
        'InventoryManager.InvWebsiteuser',
        'InventoryManager.InvInventoryProperty');
    var $helpers = array('Html', 'Form','Number');
    public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('towerblock','wingblock','inventorySummeryReport','inventory1','deleteSalesEmployees','edit_broker_employees1','price_calculator_list_brokers','edit_sales_employees1','share_projects_inventory_view','share_projects_inventory','share_project_to_sales_employees','my_projects','edit_sales_employees','broker_employees_lists','sales_employees_lists','sales_employees_lists_broker','add_sales_employees','add_sales_employees_broker','AsstBuyRent_EditRequirement', 'admin_delete_reports_pic', 'research_detail', 'research_report', 'Portfolio_adminViewReportDetail', 'editportfolio', 'Porperty_ViewService', 'Property_ServiceDetails', 'admin_insertMasterData');
		
		$builderServicesStatus = $this->Freeactivation->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
		$this->set('builderServicesStatus', $MessageBoardAppusersAll);
	}
	
	public function getState() {
        $region_list = $this->MstAreaRegion->find('list', [ 
            'conditions' => [ 'MstAreaRegion.countryCode' => 1 ],
            'fields'     => [ 'MstAreaRegion.regionCode' ]
        ]);
        $sql = sprintf("SELECT MstAreaState.* FROM mst_area_states MstAreaState WHERE (SELECT COUNT(*) FROM mst_area_cities mc WHERE MstAreaState.stateCode = mc.stateCode) > 0 AND MstAreaState.regionCode IN (%s) ORDER BY MstAreaState.stateName ASC", implode(',', $region_list));
        $stateList = $this->MstAreaState->query($sql);
        $states = [];
        foreach ($stateList as $state) {
            $states[$state['MstAreaState']['stateCode']] = $state['MstAreaState']['stateName'];
        }
        return $states;
    }

    public function dashboard() {
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            $this->set('status', $builder['0']['Builder']['status']);
        }
        $this->layout = "account";
        /* buider approvel state : 0:not fill,1:save,2:submit/pending,3:approved 4.rejected */
        //	pr($this->Session->read('Auth.Websiteuser')); die;


        if ($this->Session->read('Auth.Websiteuser.userrole') == '3') {
            $builder = $this->Builder->find('all', array(
                'fields' => array(
                    'id',
                    'status',
                ),
                'conditions' => array(
                    'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
            if (!empty($builder) && $builder['0']['Builder']['status'] == '3') {
                
            } else {
                $this->redirect('builderapproval');
            }
        }
		
		if ($this->Session->read('Auth.Websiteuser.userrole') == '3') {
			$totalProjects = $this->Project->find('count', array(
					'conditions' => array(
						'user_id' => $this->Session->read('Auth.Websiteuser.id')
					)
			));
			if(!empty($totalProjects)){$this->set('totalProjects', $totalProjects);}
		}
		
		$totalProperty = $this->Property->find('count', array(
					'conditions' => array(
						'user_id' => $this->Session->read('Auth.Websiteuser.id')
					)
			));
			if(!empty($totalProperty)){$this->set('totalProperty', $totalProperty);}
			
		$totalresponses = $this->Lead->find('count', array(
			'joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.property_id = Lead.property_id',
						'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')))),
						'limit' => 12));
		if(!empty($totalresponses)){$this->set('totalresponses', $totalresponses);}
		
    }

    public function builderapproval() {

        $this->layout = "account";
        if ($this->request->is('post') || $this->request->is('put')) {

            $locality_find = $this->MstAssignLocality->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_localities',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_localities.localityCode = MstAssignLocality.localityCode'
                        )
                    )
                ),
                'conditions' => array(
                    'localityName' => $this->request->data['Builder']['locality']
                )
            ));

            $city_find = $this->MstAssignCity->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_cities',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_cities.cityCode = MstAssignCity.cityCode'
                        )
                    )
                ),
                'conditions' => array(
                    'MstAssignCity.cityCode' => $this->request->data['Builder']['city']
                )
            ));

            $state_find = $this->MstAssignState->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_states',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_states.stateCode = MstAssignState.stateCode'
                        )
                    )
                ),
                'conditions' => array(
                    'mst_area_states.stateName' => $this->request->data['Builder']['state_data']
                )
            ));

            $country_find = $this->MstAssignCountry->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_countries',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_countries .countryCode = MstAssignCountry.countryCode'
                        )
                    )
                ),
                'conditions' => array(
                    'mst_area_countries.countryName' => $this->request->data['Builder']['country_data']
                )
            ));

            if (!empty($locality_find['0']['MstAssignLocality']['research_manager'])) {
                $key = $locality_find['0']['MstAssignLocality']['research_manager'];
            } else if (!empty($city_find['0']['MstAssignCity']['research_manager'])) {
                $key = $city_find['0']['MstAssignCity']['research_manager'];
            } else if (!empty($state_find['0']['MstAssignState']['research_manager'])) {
                $key = $state_find['0']['MstAssignState']['research_manager'];
            } else if (!empty($country_find['0']['MstAssignCountry']['research_manager'])) {
                $key = $country_find['0']['MstAssignCountry']['research_manager'];
            } else {
                $key = 0;
            }

            if (!empty($this->request->data['Builder']['buider_id'])) {

                $this->Builder->id = $this->request->data['Builder']['buider_id'];
            }

            $this->request->data ['Builder']['websiteuser_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data ['Builder']['manager_id'] = $key;

            $this->request->data ['Builder']['status'] = $this->request->data['save'];

            if ($this->Builder->save($this->request->data)) {
                if ($this->request->data['save'] == '1') {

                    $this->Session->setFlash('Profile has been saved Sucessfully', 'default', array('class' => 'green'));
                } else {
                    //sent notifiction after saving
                    $this->request->data ['BuilderApprovalNotifiction']['user_id'] = $key;
                    $this->request->data ['BuilderApprovalNotifiction']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
                    $this->request->data ['BuilderApprovalNotifiction']['inquiry_id'] = $this->Builder->id;
                    $this->BuilderApprovalNotifiction->save($this->request->data);
                    $this->redirect('builderapproval');
                }
                $this->redirect(array('action' => 'builderapproval'));
            }
        }

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $builder_Status = "New";
        if (!empty($builder)) {
            $this->request->data = $builder[0];
            $this->set('buider_id', $builder['0']['Builder']['id']);
            if ($builder['0']['Builder']['status'] == 1) {
                $builder_Status = "Saved";
            } else if ($builder['0']['Builder']['status'] == 2) {
                $builder_Status = "Approval Request Pending";
            } else if ($builder['0']['Builder']['status'] == 3) {
                $builder_Status = "Approved";
            } else if ($builder['0']['Builder']['status'] == 4) {
                $builder_Status = "Rejected";
            } else {
                $builder_Status = "New";
            }
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
        /* buider approvel state : 0:not fill,1:save,2:submit/pending,3:approved 4.rejected */

        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);
        $this->set('builder_Status', $builder_Status);
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] == '2') {
                $this->render('builder_submit');
            }
        }
    }

    public function admin_builderIndex() {
        $user_id = $this->Session->read('Auth.User.id');
        $employee = $this->User->children($user_id);
        $teamarray = '';
        foreach ($employee as $team) {

            $teamarray .= $team['User']['id'];
            $teamarray .= ',';
        }
        $trim_data = rtrim($teamarray, ',');
        $trim_data_array = explode(",", $trim_data);
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id == 0) {
            $builder = $this->Builder->find('all', array(
                'conditions' => array(
                    'Builder.status !=' => '1'
                )
            ));
        } else {
            $builder = $this->Builder->find('all', array(
                'conditions' => array(
                    'Builder.status !=' => '1',
                    'Builder.manager_id' => $trim_data_array
                )
            ));
        }


        $this->set('builder', $builder);
    }

    public function admin_viewBuilder($id = null) {
        $this->Builder->id = $id;
        if (!$this->Builder->exists()) {
            throw new NotFoundException(__('Invalid Builder'));
        }
        $builder_data = $this->Builder->findById($id);
        $this->set('builder_data', $builder_data);
    }

    public function admin_editBuilder($id = null) {

        $this->Builder->id = $id;
        if (!$this->Builder->exists()) {
            throw new NotFoundException(__('Invalid Builder'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            //find email of bulider
            $builder_email = $this->Websiteuser->find('list', array(
                'fields' => array(
                    'email',
                ),
                'conditions' => array(
                    'id' => $this->request->data['Builder']['websiteuser_id']
                )
            ));
            if(!empty($builder_email)) {
                if ($this->request->data['Builder']['status'] == '3') {
                    
					$html = 'Dear ' . $this->request->data ['Builder']['borg_contact'];
					$html .= '<p>Our team has verified your details and find you a builder of repute. We are happy to welcome your
					projects on our website.</p>';
					$html .= '<p>Please click on this link to proceed to posting your projects</p>';
					$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot .'">Click Now</a></p>';
					$html .= '<p>In case of any difficulty please call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope our association is mutually beneficial.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
                        
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->emailFormat('html');
                    /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
                    $Email->to($builder_email[$this->request->data['Builder']['websiteuser_id']]);
                    $Email->subject('Verification successful');
                    $Email->send($html);
                } else {
					$html = 'Dear ' . $this->request->data ['Builder']['borg_contact'];
					$html .= '<p>Our team was not able to satisfactorily verify your credentials and so we will not be able to list projects on our website for now.</p>';
					$html .= '<p>We however request you to be in touch for any possible future engagements.</p>';
					$html .= '<p>In case of any doubts or clarifications please feel free to call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope to work with you in future.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
                        
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->emailFormat('html');
                    /* $Email->from(array('Support@FairPockets.com' => 'FairPockets'));
                     */ $Email->to($builder_email[$this->request->data['Builder']['websiteuser_id']]);
                    $Email->subject('Verification failed');
                    $Email->send($html);
                }
            }
            if ($this->Builder->save($this->request->data)) {
                $this->Session->setFlash('The Builder has been Updated', 'default', array('class' => 'green'));
            }
        } else {
            $builder_data = $this->Builder->findById($id);
            $this->request->data = $builder_data;
        }
    }

    public function change_profile() {
        $id = $this->Session->read('Auth.Websiteuser.id');
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            $this->set('status', $builder['0']['Builder']['status']);
			$this->set('borg_logo', $builder['0']['Builder']['borg_logo']);
        }
        $this->layout = "account";
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Websiteuser->id = $id;

            if ($this->Websiteuser->save($this->request->data)) {
				
				$borg_logo = $this->request->data['Websiteuser']['borg_logo']['name'];
				$target_dir = 'upload/builder_logo/';
				if (!empty($borg_logo)) { 
					$tmprary_name = $this->request->data ['Websiteuser']['borg_logo']['tmp_name'];
					$temp = explode(".", $borg_logo);
					$newfilename = uniqid() . '.' . end($temp);
					if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
						$builderlogo = $newfilename;
					}
				} else {
					if($builder['0']['Builder']['borg_logo'] !=''){
						$builderlogo = $builder['0']['Builder']['borg_logo'];
					}else{
						$builderlogo = 'logo-default';
					}
				}
				$this->Builder->query("UPDATE builders SET borg_logo='".$builderlogo."' WHERE websiteuser_id='".$this->Session->read('Auth.Websiteuser.id')."'");
				
				
                $this->Session->setFlash('Profile has been Updated Successfully', 'default', array('class' => 'green'));
            }
        } else {
            $this->request->data = $this->Websiteuser->findById($id);
        }
    }

    public function AsstBuyRent_AddRequirement() {
        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['BuyRentRequirement']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data['BuyRentRequirement']['status'] = $this->request->data['save'];
            if ($this->BuyRentRequirement->save($this->request->data)) {

                $lastinsertId = $this->BuyRentRequirement->getLastInsertId();
                $this->request->data['OrderHistory']['service_name'] = 'Assisted Buy/Rent';
                $this->request->data['OrderHistory']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
                $this->request->data['OrderHistory']['id'] = $lastinsertId;
                $this->OrderHistory->save($this->request->data);

                $user = $this->User->find('all', array(
                    'fields' => array(
                        'id',
                        'parent_id',
                    )
                ));
                $user = Set::extract('/User/.', $user);

                $locality_find = $this->MstAssignLocality->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_localities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_localities.localityCode = MstAssignLocality.localityCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'localityName' => $this->request->data['BuyRentRequirement']['prop_locality']
                    )
                ));
                
                // print_r($locality_find); exit;
                $city_find = $this->MstAssignCity->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_cities.cityCode = MstAssignCity.cityCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'MstAssignCity.cityCode' => $this->request->data['BuyRentRequirement']['prop_city']
                    )
                ));

                $state_find = $this->MstAssignState->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_states',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_states.stateCode = MstAssignState.stateCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'mst_area_states.stateCode' => $this->request->data['BuyRentRequirement']['prop_state']
                    )
                ));

                $country_find = $this->MstAssignCountry->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_countries',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_countries.countryCode = MstAssignCountry.countryCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'mst_area_countries.countryCode' => '1'
                    )
                ));


                if (!empty($locality_find['MstAreaLocality']['research_executive'])) {
                    $key = $locality_find['MstAssignLocality']['research_executive'];
                } else if (!empty($city_find['MstAreaCity']['research_executive'])) {
                    $key = $city_find['MstAssignCity']['research_executive'];
                } else if (!empty($state_find['MstAreaState']['research_executive'])) {
                    $key = $state_find['MstAssignState']['research_executive'];
                } else {

                    $key = $country_find['0']['MstAssignCountry']['research_executive'];
                }

                // print $key; exit;
                // Added by s khan 25-12-2017
               $key= $this->Session->read('Auth.Websiteuser.id');
                $this->request->data ['ServiceNotifiction']['user_id'] = $key;
                $this->request->data ['ServiceNotifiction']['service'] = "Buy/Rent Requirement Services";

                $this->ServiceNotifiction->save($this->request->data);
                if ($this->request->data['save'] == '2') {
                    $this->view = "servicesubmit";
                } else {
                    $this->Session->setFlash('Requirement has been added Sucessfully', 'default', array('class' => 'green'));
                    unset($this->request->data);
                }
            } else {
                $this->Session->setFlash('Requirement has not been send', 'default', array('class' => 'red'));
            }
        }

        $this->layout = "account";
        $region_list = $this->MstAreaRegion->find('list', [ 
            'conditions' => [ 'MstAreaRegion.countryCode' => 1 ],
            'fields'     => [ 'MstAreaRegion.regionCode' ]
        ]);
        $sql = sprintf("SELECT MstAreaState.* FROM mst_area_states MstAreaState WHERE (SELECT COUNT(*) FROM mst_area_cities mc WHERE MstAreaState.stateCode = mc.stateCode) > 0 AND MstAreaState.regionCode IN (%s) ORDER BY MstAreaState.stateName ASC", implode(',', $region_list));
        $state_master = $this->MstAreaState->query($sql);
        $stateList = [];
        foreach ($state_master as $value) {
            $stateList[$value['MstAreaState']['stateCode']] = $value['MstAreaState']['stateName'];
        }
        /*$state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'stateCode',
                'stateName',
            )
        ));*/
        $this->set('state_master', $stateList);
    }

    public function AsstBuyRent_EditRequirement($id) {
        $this->BuyRentRequirement->id = $id;
        if (!$this->BuyRentRequirement->exists()) {
            throw new NotFoundException(__('Invalid Requirement'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['BuyRentRequirement']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data['BuyRentRequirement']['status'] = $this->request->data['save'];
            if ($this->BuyRentRequirement->save($this->request->data)) {
                if ($this->request->data['save'] == '2') {
                    $this->view = "servicesubmit";
                }
                $this->Session->setFlash('Requirement has been Updated Sucessfully', 'default', array('class' => 'green'));
            } else {
                $this->Session->setFlash('Requirement has not been Updated', 'default', array('class' => 'red'));
            }
        }

        $BuyRentRequirement_data = $this->BuyRentRequirement->findById($id);
        $this->request->data = $BuyRentRequirement_data;

        $this->layout = "account";
        $state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'stateCode',
                'stateName',
            )
        ));
        $this->set('state_master', $state_master);
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            ),
            'conditions' => array(
                'statecode' => $BuyRentRequirement_data['BuyRentRequirement']['prop_state']
            )
        ));

        $this->set('city_master', $city_master);
    }

    public function AsstSellRent_View_Lead($prpid = Null) {
        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'AsstSellLead.name LIKE' => $this->request->data['search'] . '%',
                        'AsstSellLead.price_discussion LIKE' => $this->request->data['search'] . '%',
                    ),
                    'AsstSellLead.property_id' => $prpid
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('AsstSellLead.property_id' => $prpid),
                'limit' => 5
            );
        }
        $AsstSellLead = $this->Paginator->paginate('AsstSellLead');


        $this->set('AsstSellLead', $AsstSellLead);

        $this->Property->id = $prpid;
        if (!$this->Property->exists()) {
            throw new NotFoundException(__('Invalid Lead'));
        }
        $proj = $this->Property->findById($prpid);

        $merge_data = '';

        $propertyoption = $this->MstPropertyTypeOptions->find('all', array(
            'conditions' => array(
                ' 	option_code' => $proj['Property']['property_type']
            )
        ));

        $buysell = $this->AsstSellRent->find('all', array(
            'conditions' => array(
                ' 	prop_id' => $proj['Property']['id']
            )
        ));

        $merge_data = $proj;

        if (!empty($propertyoption['0'])) {
            $merge_data = array_merge($merge_data, $propertyoption['0']);
        }

        if (!empty($buysell['0'])) {
            $merge_data = array_merge($merge_data, $buysell['0']);
        }

        $this->set('data', $merge_data);

        $this->layout = "account";
    }

    public function AsstBuyRent_View_Requirement() {
        $this->layout = "account";


        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'MstAreaCity.cityName LIKE' => $this->request->data['search'] . '%',
                        'MstAreaState.stateName LIKE' => $this->request->data['search'] . '%',
                        'BuyRentRequirement.prop_locality LIKE' => $this->request->data['search'] . '%',
                        'BuyRentRequirement.prop_budget LIKE' => $this->request->data['search'] . '%'
                    ),
                    'BuyRentRequirement.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'joins' => array(
                    array(
                        'alias' => 'MstAreaCity',
                        'table' => 'mst_area_cities',
                        'type' => 'Left',
                        'conditions' => '`MstAreaCity`.`cityCode` =`BuyRentRequirement`.`prop_city`'
                    ),
                    array(
                        'alias' => 'MstAreaState',
                        'table' => 'mst_area_states',
                        'type' => 'Left',
                        'conditions' => '`MstAreaState`.`stateCode` =`BuyRentRequirement`.`prop_state`'
                    )
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('BuyRentRequirement.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
        }
        $requirement = $this->Paginator->paginate('BuyRentRequirement');
        foreach ($requirement as $proj) {

            $city = $this->MstAreaCity->find('all', array(
                'conditions' => array(
                    'cityCode' => $proj['BuyRentRequirement']['prop_city']
                )
            ));

            $merge_data = $proj;

            if (!empty($city['0'])) {
                $merge_data = array_merge($merge_data, $city['0']);
            }
            $allprojdata[] = $merge_data;
        }
        $this->set(compact('allprojdata'));
    }

    public function AsstSellRent_View_Response() {
        $this->layout = "account";


        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'locality LIKE' => $this->request->data['search'] . '%',
                        'city_data LIKE' => $this->request->data['search'] . '%',
                        'state_data LIKE' => $this->request->data['search'] . '%'
                    ),
                    'AsstSellRent.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'joins' => array(
                    array(
                        'alias' => 'AsstSellRent',
                        'table' => 'asst_sell_rents',
                        'type' => 'Inner',
                        'conditions' => '`Property`.`id` =`AsstSellRent`.`prop_id`'
                    )
                ),
                'limit' => 5
            );
        } else {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'AsstSellRent.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'joins' => array(
                    array(
                        'alias' => 'AsstSellRent',
                        'table' => 'asst_sell_rents',
                        'type' => 'Left',
                        'conditions' => '`Property`.`property_id` =`AsstSellRent`.`prop_id`'
                    )
                ),
                'limit' => 5
            );
        }
        $sellrequirement = $this->Paginator->paginate('Property');
        $merge_data = '';
        foreach ($sellrequirement as $proj) {

            $propertyoption = $this->MstPropertyTypeOptions->find('all', array(
                'conditions' => array(
                    ' 	option_code' => $proj['Property']['property_type']
                )
            ));

            $buysell = $this->AsstSellRent->find('all', array(
                'conditions' => array(
                    ' 	prop_id' => $proj['Property']['property_id']
                )
            ));

            $merge_data = $proj;

            if (!empty($propertyoption['0'])) {
                $merge_data = array_merge($merge_data, $propertyoption['0']);
            }

            if (!empty($buysell['0'])) {
                $merge_data = array_merge($merge_data, $buysell['0']);
            }
            $allprojdata[] = $merge_data;
        }

        $this->set(compact('allprojdata'));

        $this->layout = "account";
    }

    public function order_history() {

        $order = $this->OrderHistory->find('all', array(
            'conditions' => array(
                'user_id' => $this->Session->read('Auth.Websiteuser.id')
            ),
            'order' => 'order_id DESC'
        ));

        $this->set(compact('order'));
        $this->layout = "account";
    }

    public function asstSellRent_servicerequest() {

        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['AsstSellRent']['user_id'] = $this->Session->read('Auth.Websiteuser.id');

            if ($this->AsstSellRent->save($this->request->data)) {

                $lastinsertId = $this->AsstSellRent->getLastInsertId();
                $this->request->data['OrderHistory']['service_name'] = 'Assisted Sell/Rent';
                $this->request->data['OrderHistory']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
                $this->request->data['OrderHistory']['id'] = $lastinsertId;
                $this->OrderHistory->save($this->request->data);

                $propertydetails = $this->Property->findByproperty_id($this->request->data['AsstSellRent']['prop_id']);
				//print_r($propertydetails);exit;
                $user = $this->User->find('all', array(
                    'fields' => array(
                        'id',
                        'parent_id',
                    )
                ));
                $user = Set::extract('/User/.', $user);


                $locality_find = $this->MstAssignLocality->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_localities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_localities.localityCode = MstAssignLocality.localityCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'localityName' => $propertydetails['Property']['locality']
                    )
                ));

                $city_find = $this->MstAssignCity->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_cities.cityCode = MstAssignCity.cityCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'MstAssignCity.cityCode' => $propertydetails['Property']['property_city']
                    )
                ));

                $state_find = $this->MstAssignState->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_states',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_states.stateCode = MstAssignState.stateCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'stateName' => $propertydetails['Property']['state_data']
                    )
                ));

                $country_find = $this->MstAssignCountry->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'mst_area_countries',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'mst_area_countries.countryCode = MstAssignCountry.countryCode'
                            )
                        )
                    ),
                    'conditions' => array(
                        'mst_area_countries.countryName' => $propertydetails['Property']['country_data']
                    )
                ));

                if (!empty($locality_find['MstAreaLocality']['research_executive'])) {
                    $key = $locality_find['MstAssignLocality']['research_executive'];
                } else if (!empty($city_find['MstAreaCity']['research_executive'])) {
                    $key = $city_find['MstAssignCity']['research_executive'];
                } else if (!empty($state_find['MstAreaState']['research_executive'])) {
                    $key = $state_find['MstAssignState']['research_executive'];
                } else {

                    $key = $country_find['0']['MstAssignCountry']['research_executive'];
                }

                $this->request->data ['ServiceNotifiction']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
                $this->request->data ['ServiceNotifiction']['service'] = "Sell/Rent Requirement Services";

                $this->ServiceNotifiction->save($this->request->data);


                $this->Session->setFlash('Requirement has been added Sucessfully', 'default', array('class' => 'green'));
            } else {
                $this->Session->setFlash('Requirement has not been send', 'default', array('class' => 'red'));
            }
        }


        $this->layout = "account";
        $AsstSellRent_data = $this->AsstSellRent->find('list', array(
            'fields' => array(
                'prop_id'
            )
        ));

        $properties = $this->Property->find('all', array(
            'conditions' => array(
                'status_id' => 3,
                'user_id' => $this->Session->read('Auth.Websiteuser.id'),
                'NOT' => array("Property.property_id" => $AsstSellRent_data)
            )
        ));
		
		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $properties[0]['Property']['property_id']
                )
            ));
		$all_property_features = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
		}
		$propertyoption = $this->MstPropertyTypeOption->find('first', array(
                'conditions' => array(
                    'property_option_id' => $properties[0]['Property']['property_type_id']
                )
            )); 
		//echo '<pre>';print_r($propertyoption);
		$this->set('all_property_features',$all_property_features);
		$this->set('propertyoption',$propertyoption);
        $this->set(compact('properties'));
    }

    public function admin_buy_rent_service() {
        $buysellservice = $this->BuyRentRequirement->find('all');
        $merge_data = '';
        foreach ($buysellservice as $proj) {

            $city = $this->MstAreaCity->find('all', array(
                'conditions' => array(
                    'cityCode' => $proj['BuyRentRequirement']['prop_city']
                )
            ));

            $merge_data = $proj;

            if (!empty($city['0'])) {
                $merge_data = array_merge($merge_data, $city['0']);
            }
            $allprojdata[] = $merge_data;
        }

        $this->set(compact('allprojdata'));
    }

    public function admin_viewBuyrent($id) {
        $proj = $this->BuyRentRequirement->findById($id);

        $city = $this->MstAreaCity->find('all', array(
            'conditions' => array(
                'cityCode' => $proj['BuyRentRequirement']['prop_city']
            )
        ));

        $merge_data = $proj;

        if (!empty($city['0'])) {
            $merge_data = array_merge($merge_data, $city['0']);
        }
        $data = $merge_data;

        $this->set(compact('data'));

        $this->BuyRentRequirement->id = $id;
        $this->OrderHistory->id = $id;
        if (!$this->BuyRentRequirement->exists()) {
            throw new NotFoundException(__('Invalid Requirement'));
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {



            if ($this->request->data['BuyRentRequirement']['status'] == '3') {
                $this->request->data['BuyRentRequirement']['service_start'] = date("d-m-Y");
                $this->request->data['OrderHistory']['service_start'] = date("d-m-Y");
            }

            if ($this->request->data['BuyRentRequirement']['status'] == '5') {
                $this->request->data['BuyRentRequirement']['service_end'] = date("d-m-Y");
                $this->request->data['OrderHistory']['service_end'] = date("d-m-Y");
            }

            $this->request->data['OrderHistory']['status'] = $this->request->data['BuyRentRequirement']['status'];
            $this->OrderHistory->save($this->request->data);


            if ($this->BuyRentRequirement->save($this->request->data)) {

                if ($proj['BuyRentRequirement']['prop_type'] == '1') {
                    $prop_type = 'Residential';
                } else {
                    $prop_type = 'Commercial';
                }

                if ($proj['BuyRentRequirement']['prop_reqt'] == '1') {
                    $prop_req = 'Rent';
                } else {
                    $prop_req = 'Buy';
                }

                $userdata = $this->Websiteuser->find('all', array(
                    'conditions' => array(
                        'Websiteuser.id' => $proj['BuyRentRequirement']['user_id']
                    )
                ));


                if ($this->request->data['BuyRentRequirement']['status'] == '3') {

                    $line = "New " . $prop_type . "  advisory request accepted";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>Thank you for soliciting our assisted  ' . $prop_req . '  for your ' . $prop_type .'</p>';
					$html .= '<p>Our team will contact you soon to understand your expectations in detail.</p>';
					$html .= '<p>Here are the contact details on which we will try to reach you: +91-99109 16878</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }
                if ($this->request->data['BuyRentRequirement']['status'] == '4') {
                    $line = "New " . $prop_type . " Service advisory request rejected";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>We will not be able to provide assisted ' . $prop_req . ' for your ' . $prop_type . ' as of now.</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope to help with your other properties.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }
                if ($this->request->data['BuyRentRequirement']['status'] == '5') {
                    $line = $prop_type . " Service advisory request closed";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>Your requested service ' . $prop_req . ' for your ' . $prop_type . ' is closed now.</p>';
					$html .= '<p>We thank you again for this opportunity to serve you.</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope to help with your other properties.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }

                $Email = new CakeEmail();
                $Email->config('smtp');
				$Email->emailFormat('html');
                /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
                $Email->to($userdata['0']['Websiteuser']['email']);
                $Email->subject($line);
                $Email->send($html);



                $this->Session->setFlash('Requirement has been Updated Sucessfully', 'default', array('class' => 'green'));
            } else {
                $this->Session->setFlash('Requirement has not been Updated', 'default', array('class' => 'red'));
            }
        }
    }

    public function admin_viewSellrent($id) {
        $data = $this->Property->find('all', array(
            'fields' => array('Property.*', 'AsstSellRent.*'),
            'joins' => array(
                array(
                    'alias' => 'AsstSellRent',
                    'table' => 'asst_sell_rents',
                    'type' => 'Inner',
                    'conditions' => '`Property`.`id` =`AsstSellRent`.`prop_id`'
                )
            ),
            'conditions' => array(
                'AsstSellRent.id' => $id
            )
        ));


        $this->set('data', $data[0]);

        $this->AsstSellRent->id = $id;
        $this->OrderHistory->id = $id;
        if (!$this->AsstSellRent->exists()) {
            throw new NotFoundException(__('Invalid Requirement'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->request->data['AsstSellRent']['status'] == '3') {
                $this->request->data['AsstSellRent']['service_start'] = date("d-m-Y");
                $this->request->data['OrderHistory']['service_start'] = date("d-m-Y");
            }

            if ($this->request->data['AsstSellRent']['status'] == '5') {
                $this->request->data['AsstSellRent']['service_end'] = date("d-m-Y");
                $this->request->data['OrderHistory']['service_end'] = date("d-m-Y");
            }

            $this->request->data['OrderHistory']['status'] = $this->request->data['AsstSellRent']['status'];
            $this->OrderHistory->save($this->request->data);

            if ($this->AsstSellRent->save($this->request->data)) {

                $project_feature = json_decode($data['0']['Property']['features'], 'true');
                if (!empty($project_feature['Configure'])) {
                    $option = $project_feature['Configure'];
                } else {
                    if ($data['0']['MstPropertyTypeOptions']['transaction_type'] == '1') {
                        $option = "Residential Property";
                    } else {
                        $option = "Commercial Property";
                    }
                }

                $option .= " for ";
                if ($data['0']['Property']['transaction_type'] == '1') {
                    $option .= "Sale";
                } else {
                    $option .= "Rent";
                }
                $option .= " in ";

                $option .= $data['0']['Property']['city_data'];
                $option .= " , ";
                $option .= $data['0']['Property']['state_data'];

                if ($data['0']['AsstSellRent']['prop_type'] == '1') {
                    $prop_req = 'RentOut';
                } else {
                    $prop_req = 'Sell';
                }

                $userdata = $this->Websiteuser->find('all', array(
                    'conditions' => array(
                        'Websiteuser.id' => $data['0']['AsstSellRent']['user_id']
                    )
                ));


                if ($this->request->data['AsstSellRent']['status'] == '3') {

                    $line = "New " . $prop_req . "  advisory request accepted";
                   
				    $html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>Thank you for soliciting our assisted  ' . $option . '  for your ' . $prop_req . '.</p>';
					$html .= '<p>Our team will contact you soon to understand your expectations in detail.</p>';
					$html .= '<p>Here are the contact details on which we will try to reach you: +91-99109 16878</p>';
					$html .= '<p>We hope to help with your other properties.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
				   
                }
                if ($this->request->data['AsstSellRent']['status'] == '4') {
                    $line = "New " . $prop_req . " Service advisory request rejected";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>We will not be able to provide assisted ' . $option . ' for your ' . $prop_req . ' as of now.</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope to help with your other properties.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }
                if ($this->request->data['AsstSellRent']['status'] == '5') {
                    $line = $prop_req . " Service advisory request closed";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>Your requested service ' . $option . ' for your ' . $prop_req . ' is closed now.</p>';
					$html .= '<p>We thank you again for this opportunity to serve you</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope to help with your other properties.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }

                $Email = new CakeEmail();
                $Email->config('smtp');
				$Email->emailFormat('html');
                /* 	$Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
                $Email->to($userdata['0']['Websiteuser']['email']);
                $Email->subject($line);
                $Email->send($html);



                $this->Session->setFlash('Requirement has been Updated Sucessfully', 'default', array('class' => 'green'));
            } else {
                $this->Session->setFlash('Requirement has not been Updated', 'default', array('class' => 'red'));
            }
        }
    }

    public function admin_editBuyrent($id) {
        $this->BuyRentRequirement->id = $id;
        if (!$this->BuyRentRequirement->exists()) {
            throw new NotFoundException(__('Invalid Requirement'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['BuyRentRequirement']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data['BuyRentRequirement']['status'] = $this->request->data['save'];
            if ($this->BuyRentRequirement->save($this->request->data)) {

                $this->Session->setFlash('Requirement has been Updated Sucessfully', 'default', array('class' => 'green'));
            } else {
                $this->Session->setFlash('Requirement has not been Updated', 'default', array('class' => 'red'));
            }
        } else {
            $BuyRentRequirement_data = $this->BuyRentRequirement->findById($id);
            $this->request->data = $BuyRentRequirement_data;
        }

        $state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'stateCode',
                'stateName',
            )
        ));
        $this->set('state_master', $state_master);
    }

    public function admin_sell_rent_service() {
        $sell_rent = $this->Property->find('all', array(
            'fields' => array('Property.*', 'AsstSellRent.*'),
            'joins' => array(
                array(
                    'alias' => 'AsstSellRent',
                    'table' => 'asst_sell_rents',
                    'type' => 'Inner',
                    'conditions' => '`Property`.`property_id` =`AsstSellRent`.`prop_id`'
                )
            )
        ));

        $this->set('sell_rent', $sell_rent);
    }

    public function admin_viewsellrentleads($id) {
        $AsstSellLead = $this->AsstSellLead->find('all', array(
            'conditions' => array(
                'property_id' => $id
            )
        ));
        $this->set('AsstSellLead', $AsstSellLead);
    }

    public function admin_addsellrentleads($id) {
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->AsstSellLead->save($this->request->data)) {
                $this->Session->setFlash('Lead has been saved Successfully!!!', 'default', array('class' => 'green'));
            }
        }
        $this->set('property_id', $id);
    }

    public function admin_editsellrentleads($id) {
        $this->AsstSellLead->id = $id;
        if (!$this->AsstSellLead->exists()) {
            throw new NotFoundException(__('Invalid Lead'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->AsstSellLead->save($this->request->data)) {
                $this->Session->setFlash('Lead has been Update Successfully!!!', 'default', array('class' => 'green'));
            }
        }
        $AsstSellLead_data = $this->AsstSellLead->findById($id);
        $this->request->data = $AsstSellLead_data;
    }

    public function admin_addreport() {
        $target_dir = 'upload/reportimages/';
        $target_dirpdf = 'upload/reportpdf/';
        if ($this->request->is('post')) {

            $unit['unittype'] = $this->request->data['unittype'];
            $unit['totalunit'] = $this->request->data['totalunit'];
            $unit['areabaseprice'] = $this->request->data['areabaseprice'];
            $unitformat = json_encode($unit);
            if (!empty($this->request->data['ResearchReport']['reportpdf'])) {

                $name = $this->request->data['ResearchReport']['reportpdf']['name'];

                if (!empty($name)) {
                    // Check For Empty Values 
                    $tmprary_name = $this->request->data['ResearchReport']['reportpdf']['tmp_name'];
                    $temp = explode(".", $name);
                    $newfilename = uniqid() . '.' . end($temp);
                    move_uploaded_file($tmprary_name, $target_dirpdf . $newfilename);
                }
            }
            $this->request->data['ResearchReport']['unit'] = $unitformat;
            $this->request->data['ResearchReport']['reportpdf'] = $this->request->data['ResearchReport']['reportpdf']['name'];
            if ($this->ResearchReport->save($this->request->data)) {
                $lastinsertId = $this->ResearchReport->getLastInsertId();
                if (!empty($this->request->data['ResearchReport']['reportpics'])) {
                    foreach ($this->request->data['ResearchReport']['reportpics'] as $result) {

                        $name = $result['name'];

                        if (!empty($name)) {
                            // Check For Empty Values 
                            $tmprary_name = $result['tmp_name'];
                            $temp = explode(".", $name);
                            $newfilename = uniqid() . '.' . end($temp);
                            if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {

                                $this->ReasearchreportPic->create();
                                $this->ReasearchreportPic->save(
                                        array(
                                            "report_id" => $lastinsertId,
                                            "project_id" => $lastinsertId,
                                            "pic" => $target_dir . $newfilename,
                                        )
                                );
                            }
                        }
                    }
                }


                $this->Session->setFlash('The Report has been saved Successfully!!!', 'default', array('class' => 'green'));

                unset($this->request->data);
            }
        }
        $city = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city', $city);
    }

    public function admin_viewreport() {
        $researchreport = $this->ResearchReport->find('all');
        $this->set('researchreport', $researchreport);
    }

    public function admin_editResearchReport($id) {
        $this->ResearchReport->id = $id;
        if (!$this->ResearchReport->exists()) {
            throw new NotFoundException(__('Invalid Research Report'));
        }
        $target_dir = 'upload/reportimages/';
        $target_dirpdf = 'upload/reportpdf/';
        if ($this->request->is('post') || $this->request->is('put')) {

            $unit['unittype'] = $this->request->data['unittype'];
            $unit['totalunit'] = $this->request->data['totalunit'];
            $unit['areabaseprice'] = $this->request->data['areabaseprice'];
            $unitformat = json_encode($unit);

            if (!empty($this->request->data['ResearchReport']['reportpdf'])) {

                $name = $this->request->data['ResearchReport']['reportpdf']['name'];

                if (!empty($name)) {
                    // Check For Empty Values 
                    $tmprary_name = $this->request->data['ResearchReport']['reportpdf']['tmp_name'];
                    $temp = explode(".", $name);
                    $newfilename = uniqid() . '.' . end($temp);
                    move_uploaded_file($tmprary_name, $target_dirpdf . $newfilename);
                }
            }
            $this->request->data['ResearchReport']['unit'] = $unitformat;
            if (!empty($this->request->data['ResearchReport']['reportpdf']['name'])) {
                $this->request->data['ResearchReport']['reportpdf'] = $newfilename;
            } else {
                $this->request->data['ResearchReport']['reportpdf'] = $this->request->data['ResearchReport']['report_pdf1'];
            }
            if ($this->ResearchReport->save($this->request->data)) {

                if (!empty($this->request->data['ResearchReport']['reportpics'])) {
                    foreach ($this->request->data['ResearchReport']['reportpics'] as $result) {

                        $name = $result['name'];

                        if (!empty($name)) {
                            // Check For Empty Values 
                            $tmprary_name = $result['tmp_name'];
                            $temp = explode(".", $name);
                            $newfilename = uniqid() . '.' . end($temp);
                            if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {

                                $this->ReasearchreportPic->create();
                                $this->ReasearchreportPic->save(
                                        array(
                                            "report_id" => $id,
                                            "pic" => $target_dir . $newfilename,
                                        )
                                );
                            }
                        }
                    }
                }


                $this->Session->setFlash('The Report has been updated Successfully!!!', 'default', array('class' => 'green'));
            }
        }
        $reportpics = $this->ReasearchreportPic->find('all', array(
            'conditions' => array(
                'report_id' => $id
            )
        ));

        $ResearchReport_data = $this->ResearchReport->findById($id);
        $this->request->data = $ResearchReport_data;
        $this->set('reportpics', $reportpics);
        $city = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city', $city);
    }

    public function admin_delete_reports_pic($id) {
        $this->ReasearchreportPic->delete($id);
        die;
    }

    public function research_detail($id) {
        $Reasearchreport = $this->ResearchReport->find('all', array(
            'conditions' => array(
                'id' => $id
            )
        ));
		if(!empty($Reasearchreport)){
			$this->set('Reasearchreport', $Reasearchreport['0']);
			$this->layout = "research";
			$reportpics = $this->ReasearchreportPic->find('all', array(
				'conditions' => array(
					'report_id' => $id
				)
			));
			$this->set('reportpics', $reportpics);
		}else{
			throw new NotFoundException('Could not find that post');
		}
    }

    public function research_report() {

		$this->set('title', 'Research Reports');
        if ($this->request->is('post')) {
            $searchreportdata = $this->ResearchReport->find('all', array(
                'conditions' => array(
                    'OR' => array(
                        'ResearchReport.about_builder LIKE' => $this->request->data['proname'] . '%',
                        'ResearchReport.about_project LIKE' => $this->request->data['proname'] . '%',
                        'ResearchReport.locality LIKE' => $this->request->data['proname'] . '%',
                        'ResearchReport.city_data LIKE' => $this->request->data['proname'] . '%',
                        'ResearchReport.state_data LIKE' => $this->request->data['proname'] . '%'
                    ),
                    'ResearchReport.city' => $this->request->data['city']
                )
            ));
        } else {
            $searchreportdata = $this->ResearchReport->find('all');
        }

        $allrepotdata = '';
        foreach ($searchreportdata as $data) {

            $reportpic = $this->ReasearchreportPic->find('all', array(
                'conditions' => array(
                    'report_id' => $data['ResearchReport']['id']
                )
            ));


            $merge_data = $data;
            if (!empty($reportpic['0'])) {
                $merge_data = array_merge($merge_data, $reportpic['0']);
            }
            $allrepotdata[] = $merge_data;
        }
        $this->set('searchreportdata', $allrepotdata);
        $this->layout = "research";
        $city = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city', $city);
    }

    public function portfolio_addProperty() {
        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['Portfolio']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data['Portfolio']['created_date'] = date("Y-m-d H:i:s");
            if ($this->Portfolio->save($this->request->data)) {

                $page = "1";
                $this->view = "portfolio";
                $this->set('page', $page);
            }
        }
        $regionList = $this->MstAreaRegion->find('list', [
            'fields' => ['regionCode'],
            'conditions' => [ 'countryCode' => 1 ]
        ]);
       /* $state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'stateCode',
                'stateName',
            ),
            'conditions' => [ 'regionCode' => $regionList ]
        ));*/
        $state_master = $this->getState();
        $this->set('state_master', $state_master);
        $this->layout = "account";
    }

    public function Portfolio_ViewProperty() {
        $this->layout = "account";


        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'MstAreaCity.cityName LIKE' => $this->request->data['search'] . '%',
                        'MstAreaState.stateName LIKE' => $this->request->data['search'] . '%',
                        'Portfolio.prop_locality LIKE' => $this->request->data['search'] . '%',
                        'Portfolio.prop_addr LIKE' => $this->request->data['search'] . '%',
                        'Portfolio.prop_cfg LIKE' => $this->request->data['search'] . '%',
                        'Portfolio.prop_mprice LIKE' => $this->request->data['search'] . '%'
                    ),
                    'Portfolio.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'joins' => array(
                    array(
                        'alias' => 'MstAreaCity',
                        'table' => 'mst_area_cities',
                        'type' => 'Left',
                        'conditions' => '`MstAreaCity`.`cityCode` =`Portfolio`.`prop_city`'
                    ),
                    array(
                        'alias' => 'MstAreaState',
                        'table' => 'mst_area_states',
                        'type' => 'Left',
                        'conditions' => '`MstAreaState`.`stateCode` =`Portfolio`.`prop_state`'
                    )
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('Portfolio.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
        }
        $requirement = $this->Paginator->paginate('Portfolio');
        foreach ($requirement as $proj) {

            $city = $this->MstAreaCity->find('all', array(
                'conditions' => array(
                    'MstAreaCity.cityCode' => $proj['Portfolio']['prop_city']
                )
            ));


            $propertyoption = $this->MstPropertyTypeOption->find('all', array(
                'conditions' => array(
                    'MstPropertyTypeOption.property_option_id' => $proj['Portfolio']['prop_toption']
                )
            ));


            $merge_data = $proj;

            if (!empty($city['0'])) {
                $merge_data = array_merge($merge_data, $city['0']);
            }

            if (!empty($propertyoption['0'])) {
                $merge_data = array_merge($merge_data, $propertyoption['0']);
            }
            $allprojdata[] = $merge_data;
        }

        $this->set(compact('allprojdata'));
    }

    public function portfolio_editProperty($id) {
        $this->Portfolio->id = $id;
        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['Portfolio']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            if ($this->Portfolio->save($this->request->data)) {

                $page = "2";
                $this->view = "portfolio";
                $this->set('page', $page);
            }
        }
        $state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'stateCode',
                'stateName',
            )
        ));
        $this->set('state_master', $state_master);

        $this->layout = "account";

        $portfolio_data = $this->Portfolio->findById($id);
        $this->request->data = $portfolio_data;

        $city_master = $this->MstAreaCity->find('list', array(
            'conditions' => array(
                'stateCode' => $portfolio_data['Portfolio']['prop_state']
            ),
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);

        $propertyoption = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $portfolio_data['Portfolio']['prop_type']
            )
        ));
        $this->set('propertyoption', $propertyoption);
    }

    public function Portfolio_ServiceRequest() {

        if ($this->request->is('post') || $this->request->is('put')) {

            $proop = $this->request->data['PortService']['port_prop'];

            $this->request->data['PortService']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data['PortService']['port_prop'] = implode(",", $proop);

            if ($this->PortService->save($this->request->data)) {

                $lastinsertId = $this->PortService->getLastInsertId();
                $this->request->data['OrderHistory']['service_name'] = 'Portfolio Management';
                $this->request->data['OrderHistory']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
                $this->request->data['OrderHistory']['id'] = $lastinsertId;
                $this->OrderHistory->save($this->request->data);
                $page = "3";
                $this->view = "portfolio";
                $this->set('page', $page);
            }
        }


        $Portfolio = $this->Portfolio->find('all', array(
            'conditions' => array(
                'Portfolio.user_id' => $this->Session->read('Auth.Websiteuser.id')
            ),
            'fields' => array('Portfolio.*', 'MstAreaCity.*'),
            'joins' => array(
                array(
                    'alias' => 'MstAreaCity',
                    'table' => 'mst_area_cities',
                    'type' => 'Left',
                    'conditions' => '`MstAreaCity`.`cityCode` =`Portfolio`.`prop_city`'
                ),
                array(
                    'alias' => 'MstAreaState',
                    'table' => 'mst_area_states',
                    'type' => 'Left',
                    'conditions' => '`MstAreaState`.`stateCode` =`Portfolio`.`prop_state`'
                )
            )
        ));
        $this->set('Portfolio', $Portfolio);

        $this->layout = "account";
    }

    public function Portfolio_ViewReport() {
        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'PortService.port_name LIKE' => $this->request->data['search'] . '%'
                    ),
                    'PortService.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('PortService.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
        }
        $requirement = $this->Paginator->paginate('PortService');

        $this->set('requirement', $requirement);
        $this->layout = "account";
    }

    public function Portfolio_ViewReportDetail($id) {
        $report_detail = $this->PortService->find('all', array(
            'conditions' => array(
                'PortService.id' => $id
            )
        ));
        $this->set('report_detail', $report_detail[0]);
        $this->layout = "account";

        $condat = explode(',', $report_detail['0']['PortService']['port_prop']);
        $properties = $this->Portfolio->find('all', array(
            'conditions' => array('Portfolio.id' => $condat),
            'fields' => array('Portfolio.*', 'MstAreaCity.*'),
            'joins' => array(
                array(
                    'alias' => 'MstAreaCity',
                    'table' => 'area_city_masters',
                    'type' => 'Left',
                    'conditions' => '`MstAreaCity`.`cityCode` =`Portfolio`.`prop_city`'
                ),
                array(
                    'alias' => 'MstAreaState',
                    'table' => 'mst_area_states',
                    'type' => 'Left',
                    'conditions' => '`MstAreaState`.`stateCode` =`Portfolio`.`prop_state`'
                )
            )
        ));

        $this->set('properties', $properties);
    }

    public function Portfolio_adminViewReportDetail($id) {
        $report_detail = $this->PortService->find('all', array(
            'conditions' => array(
                'PortService.id' => $id
            )
        ));
        $this->set('report_detail', $report_detail[0]);
        $this->layout = "account";

        $condat = explode(',', $report_detail['0']['PortService']['port_prop']);
        $properties = $this->Portfolio->find('all', array(
            'conditions' => array('Portfolio.id' => $condat),
            'fields' => array('Portfolio.*', 'MstAreaCity.*'),
            'joins' => array(
                array(
                    'alias' => 'MstAreaCity',
                    'table' => 'mst_area_cities',
                    'type' => 'Left',
                    'conditions' => '`MstAreaCity`.`cityCode` =`Portfolio`.`prop_city`'
                ),
                array(
                    'alias' => 'MstAreaState',
                    'table' => 'mst_area_states',
                    'type' => 'Left',
                    'conditions' => '`MstAreaState`.`stateCode` =`Portfolio`.`prop_state`'
                )
            )
        ));

        $this->set('properties', $properties);
    }

    public function admin_view_portfolio() {
        $PortService = $this->PortService->find('all');
        $this->set('PortService', $PortService);
    }

    public function editportfolio($id) {
        $this->Portfolio->id = $id;
        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['Portfolio']['user_id'] = $this->Session->read('Auth.Websiteuser.id');

            if ($this->Portfolio->save($this->request->data)) {

                $page = "2";
                $this->view = "portfolio";
                $this->set('page', $page);
            }
        }
        $state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'id',
                'statename',
            )
        ));

        $this->set('state_master', $state_master);
        $this->layout = "account";

        $portfolio_data = $this->Portfolio->findById($id);
        $city_master = $this->MstAreaCity->find('list', array(
            'conditions' => array(
                'stateCode' => $portfolio_data['Portfolio']['prop_state']
            ),
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);

        $this->request->data = $portfolio_data;

        $propertyoption = $this->MstPropertyTypeOptions->find('list', array(
            'fields' => array(
                'option_code',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type' => $portfolio_data['Portfolio']['prop_type']
            )
        ));
        $this->set('propertyoption', $propertyoption);
    }

    public function admin_edit_portfolio($id) {
        $this->PortService->id = $id;
        $this->OrderHistory->id = $id;
        $PortService = $this->PortService->findById($id);

        $target_dirpdf = 'upload/portfoliopdf/';
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->request->data['PortService']['status'] == '1') {
                $this->request->data['PortService']['service_start'] = date("d-m-Y");
                $this->request->data['OrderHistory']['service_start'] = date("d-m-Y");
                $this->request->data['OrderHistory']['status'] = '3';
            }

            if (!empty($this->request->data['PortService']['latest_pdf'])) {

                $name = $this->request->data['PortService']['latest_pdf']['name'];

                if (!empty($name)) {
                    // Check For Empty Values 
                    $tmprary_name = $this->request->data['PortService']['latest_pdf']['tmp_name'];
                    $temp = explode(".", $name);
                    $newfilename = uniqid() . '.' . end($temp);
                    move_uploaded_file($tmprary_name, $target_dirpdf . $newfilename);
                    $this->request->data['PortService']['latest_pdf'] = $target_dirpdf . $newfilename;
                } else {
                    $this->request->data['PortService']['latest_pdf'] = $this->request->data['PortService']['latest_pdf1'];
                }
            }
            if ($this->PortService->save($this->request->data)) {
                $this->request->data['OrderHistory']['Amount'] = $this->request->data['PortService']['payment'];
                $userdata = $this->Websiteuser->find('all', array(
                    'conditions' => array(
                        'Websiteuser.id' => $PortService['PortService']['user_id']
                    )
                ));


                if ($this->request->data['PortService']['status'] == '1') {

                    $line = "New " . $PortService['PortService']['port_name'] . "  portfolio request accepted";
                    $html = 'Dear ' . $userdata['0']['Websiteuser']['username'] . ',
						
						Thank you for soliciting our assisted  ' . $PortService['PortService']['port_name'] . ' . Service.
						Our team will contact you soon to understand your expectations in detail.
						Here are the contact details on which we will try to reach you:
						Phone: 9899614666
						Regards,
						
						FairPockets Team';
                }
                if ($this->request->data['PortService']['status'] == '2') {

                    $this->request->data['OrderHistory']['status'] = '4';
                    $line = "New " . $PortService['PortService']['port_name'] . "  Portfolio rejected";
                    $html = 'Dear ' . $userdata['0']['Websiteuser']['username'] . ',
						We will not be able to provide assisted ' . $PortService['PortService']['port_name'] . ' Portfolio Service as of now.
						For any support needs please reply to this mail or call our helpline: +91-99109 16878
						We hope to help with your other properties,
						Regards,
						
						FairPockets Team';
                }

                if ($this->request->data['PortService']['status'] == '4') {

                    $this->request->data['OrderHistory']['status'] = '6';
                    $line = "Pay for porfolio management services";
                    $html = 'Dear ' . $userdata['0']['Websiteuser']['username'] . ',
						We have reviewed your property details and are happy to manage your portfolio ' . $PortService['PortService']['port_name'] . ' package and  The charges for the same would be Rs ' . $this->request->data['PortService']['payment'] . ' /-
						
						Please proceed the below steps to complete payment for services : <br>
						1. Log In to your accounts in the Fairpockets <br>
						2. Click on the the Portfolio link in the left side <br>
						3. Then Click View Portfolio Services <br>
						4. Now you will see "Pay Now" button for the new portfolio service and click on this button <br>
						For any support needs please reply to this mail or call our helpline: +91-99109 16878
						We thank you again for this opportunity to serve you,
						
						
						Regards,
						
						FairPockets Team';
                }


                if ($this->request->data['PortService']['status'] == '3') {
                    $this->request->data['OrderHistory']['status'] = '5';
                    $this->request->data['OrderHistory']['service_end'] = date("d-m-Y");
                    $this->request->data['PortService']['service_end'] = date("d-m-Y");
                    $this->PortService->save($this->request->data);
                    $line = "New Portfolio Valuation uploaded";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>We have complete the valuation of your portfolio <name> and the report is now available here:</p>';
					$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot . 'accounts/Portfolio_ViewReportDetail/' .$id.'">Click to see property</a></p>';
					$html .= '<p>We thank you again for this opportunity to serve you</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We thank you again for this opportunity to serve you.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }
                $this->OrderHistory->save($this->request->data);
                $Email = new CakeEmail();
                $Email->config('smtp');
				$Email->emailFormat('html');
                /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
                $Email->to($userdata['0']['Websiteuser']['email']);
                $Email->subject($line);
                $Email->send($html);





                $this->Session->setFlash('The Portfolio Service has been update Successfully!!!', 'default', array('class' => 'green'));
            }
        }

        $PortService = $this->PortService->findById($id);
        $this->request->data = $PortService;
        $this->set('PortService', $PortService);
    }

    public function Property_AddProperty() {

        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['Portmgmt']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            
            if(isset($this->request->data['Portfolio']))
                $this->request->data['Portmgmt']['prop_city'] = $this->request->data['Portfolio']['prop_city'];    
            $this->request->data['Portmgmt']['created_date'] = date("Y-m-d H:i:s");
            // print_r($this->request->data); exit;
            if ($this->Portmgmt->save($this->request->data)) {
                $page = "1";
                $this->view = "propertiesmgt";
                $this->set('page', $page);
            }
        }
        $region_list = $this->MstAreaRegion->find('list', [ 
            'conditions' => [ 'MstAreaRegion.countryCode' => 1 ],
            'fields'     => [ 'MstAreaRegion.regionCode' ]
        ]);

        $sql = sprintf("SELECT MstAreaState.* FROM mst_area_states MstAreaState WHERE (SELECT COUNT(*) FROM mst_area_cities mc WHERE MstAreaState.stateCode = mc.stateCode) > 0 AND MstAreaState.regionCode IN (%s) ORDER BY MstAreaState.stateName ASC", implode(',', $region_list));
        $state_master = $this->MstAreaState->query($sql);
        $stateList = [];
        foreach ($state_master as $value) {
            $stateList[$value['MstAreaState']['stateCode']] = $value['MstAreaState']['stateName'];
        }
        /*$state_master = $this->MstAreaState->find('list', array(
            'conditions' => [ 'MstAreaState.regionCode' => $region_list ],
            'fields'     => [ 'MstAreaState.stateCode', 'MstAreaState.stateName'],
            'order'      => [ 'MstAreaState.stateName' => 'ASC']
        ));*/
        $this->set('state_master', $stateList);
        $this->layout = "account";
        unset($this->request->data);
    }

    public function Property_ViewProperty() {
        $this->layout = "account";

        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'MstAreaCity.cityName LIKE' => $this->request->data['search'] . '%',
                        'MstAreaState.stateName LIKE' => $this->request->data['search'] . '%',
                        'Portmgmt.prop_locality LIKE' => $this->request->data['search'] . '%',
                        'Portmgmt.prop_addr LIKE' => $this->request->data['search'] . '%'),
                    'Portmgmt.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'joins' => array(
                    array(
                        'alias' => 'MstAreaCity',
                        'table' => 'mst_area_cities',
                        'type' => 'Left',
                        'conditions' => '`MstAreaCity`.`cityCode` =`Portmgmt`.`prop_city`'
                    ),
                    array(
                        'alias' => 'MstAreaState',
                        'table' => 'mst_area_states',
                        'type' => 'Left',
                        'conditions' => '`MstAreaState`.`stateCode` =`Portmgmt`.`prop_state`'
                    )
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('Portmgmt.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
        }
        $requirement = $this->Paginator->paginate('Portmgmt');
        foreach ($requirement as $proj) {

            $city = $this->MstAreaCity->find('all', array(
                'conditions' => array(
                    'MstAreaCity.cityCode' => $proj['Portmgmt']['prop_city']
                )
            ));


            $propertyoption = $this->MstPropertyTypeOption->find('all', array(
                'conditions' => array(
                    'MstPropertyTypeOption.property_option_id' => $proj['Portmgmt']['prop_toption']
                )
            ));


            $merge_data = $proj;

            if (!empty($city['0'])) {
                $merge_data = array_merge($merge_data, $city['0']);
            }

            if (!empty($propertyoption['0'])) {
                $merge_data = array_merge($merge_data, $propertyoption['0']);
            }
            $allprojdata[] = $merge_data;
        }

        $this->set(compact('allprojdata'));
    }

    public function property_editProperty($id) {
        $this->Portmgmt->id = $id;
        if ($this->request->is('post') || $this->request->is('put')) {

            $this->request->data['Portmgmt']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            if ($this->Portmgmt->save($this->request->data)) {

                $page = "2";
                $this->view = "propertiesmgt";
                $this->set('page', $page);
            }
        }
        $state_master = $this->MstAreaState->find('list', array(
            'fields' => array(
                'stateCode',
                'stateName',
            )
        ));
        $this->set('state_master', $state_master);

        $this->layout = "account";

        $portfolio_data = $this->Portmgmt->findById($id);
        $this->request->data = $portfolio_data;

        $city_master = $this->MstAreaCity->find('list', array(
            'conditions' => array(
                'stateCode' => $portfolio_data['Portmgmt']['prop_state']
            ),
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);

        $propertyoption = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $portfolio_data['Portmgmt']['prop_type']
            )
        ));
        $this->set('propertyoption', $propertyoption);
    }

    public function property_ServiceRequest() {
        if ($this->request->is('post') || $this->request->is('put')) {

            $services = $this->request->data['PropService']['services'];

            $this->request->data['PropService']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            $this->request->data['PropService']['services'] = implode(",", $services);

            if ($this->PropService->save($this->request->data)) {
                $lastinsertId = $this->PropService->getLastInsertId();
                $this->request->data['OrderHistory']['service_name'] = 'Property Management';
                $this->request->data['OrderHistory']['id'] = $lastinsertId;
                $this->request->data['OrderHistory']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
                $this->OrderHistory->save($this->request->data);
                $page = "3";
                $this->view = "propertiesmgt";
                $this->set('page', $page);
            }
        }


        $Portmgmt = $this->Portmgmt->find('all', array(
            'conditions' => array(
                'Portmgmt.user_id' => $this->Session->read('Auth.Websiteuser.id')
            ),
            'fields' => array('Portmgmt.*', 'MstAreaCity.*'),
            'joins' => array(
                array(
                    'alias' => 'MstAreaCity',
                    'table' => 'mst_area_cities',
                    'type' => 'Left',
                    'conditions' => '`MstAreaCity`.`cityCode` =`Portmgmt`.`prop_city`'
                ),
                array(
                    'alias' => 'MstAreaState',
                    'table' => 'mst_area_states',
                    'type' => 'Left',
                    'conditions' => '`MstAreaState`.`stateCode` =`Portmgmt`.`prop_state`'
                )
            )
        ));
        $this->set('Portmgmt', $Portmgmt);

        $this->layout = "account";
    }

    public function Porperty_ViewService() {
        if (!empty($this->request->data['search'])) {

            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'PropService.prop_sname LIKE' => $this->request->data['search'] . '%'
                    ),
                    'PropService.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('PropService.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
        }
        $requirement = $this->Paginator->paginate('PropService');

        $this->set('requirement', $requirement);
        $this->layout = "account";
    }

    public function Property_ServiceDetails($id) {
        $report_detail = $this->PropService->find('all', array(
            'conditions' => array(
                'PropService.id' => $id
            )
        ));

        $this->layout = "account";

        $this->set('report_detail', $report_detail[0]);
        $service['servicedetail'] = $this->PropServicestage->find('all', array(
            'conditions' => array(
                'id' => $id
            )
        ));
        $mergedata = array();
        foreach ($service['servicedetail'] as $photo_id) {
            $servicephotos['Servicephotos'] = $this->PropServicephoto->find('all', array(
                'conditions' => array(
                    'service_stage_id' => $photo_id['PropServicestage']['id']
                )
            ));

            if (!empty($servicephotos['Servicephotos'][0])) {
                $mergedata[] = array_merge($photo_id, $servicephotos);
            } else {
                $mergedata[] = $photo_id;
            }
        }


        $this->set('report_service_detail', $mergedata);
    }

    public function admin_property_service_request() {
        $PropService = $this->PropService->find('all');
        $this->set('PropService', $PropService);
    }

    public function admin_edit_property_service($id) {
        $this->PropService->id = $id;
        $this->OrderHistory->id = $id;
        $PropService = $this->PropService->findById($id);

        $target_dirpdf = 'upload/portfoliopdf/';
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['OrderHistory']['Amount'] = $this->request->data['PropService']['payment'];
            if ($this->request->data['PropService']['status'] == '1') {
                $this->request->data['PropService']['service_start'] = date("d-m-Y");
                $this->request->data['OrderHistory']['service_start'] = date("d-m-Y");
                $this->request->data['OrderHistory']['status'] = '3';
            }


            if ($this->PropService->save($this->request->data)) {

                $userdata = $this->Websiteuser->find('all', array(
                    'conditions' => array(
                        'Websiteuser.id' => $PropService['PropService']['user_id']
                    )
                ));


                if ($this->request->data['PropService']['status'] == '1') {

                    $line = "New " . $PropService['PropService']['prop_sname'] . "  Property Service request accepted";
                    $html = 'Dear ' . $userdata['0']['Websiteuser']['username'] . ',
						
						Thank you for soliciting our assisted  ' . $PropService['PropService']['prop_sname'] . ' . Service.
						Our team will contact you soon to understand your expectations in detail.
						Here are the contact details on which we will try to reach you:
						Phone: 9899614666
						Regards,
						
						FairPockets Team';
                }
                if ($this->request->data['PropService']['status'] == '2') {
                    $this->request->data['OrderHistory']['status'] = '4';
                    $line = "New " . $PropService['PropService']['prop_sname'] . "  Property Service rejected";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>We will not be able to provide assisted ' . $PropService['PropService']['prop_sname'] . ' Property Service as of now.</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We hope to help with your other properties</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }

                if ($this->request->data['PropService']['status'] == '4') {
                    $this->request->data['OrderHistory']['status'] = '6';

                    $line = "Pay for Property management services";
                    
					
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>We have reviewed your property details and are happy to manage your Property Service ' . $PropService['PropService']['prop_sname'] . ' package and  The charges for the same would be Rs ' . $this->request->data['PropService']['payment'] . ' /-</p>';
					$html .= '<p>Please proceed to complete payment for services on following link: </p>';
					$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot . 'accounts/Portfolio_ViewReportDetail/' .$id.'">Click to see property </a></p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We thank you again for this opportunity to serve you.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }


                if ($this->request->data['PropService']['status'] == '3') {

                    $this->request->data['PropService']['service_end'] = date("d-m-Y");
                    $this->request->data['OrderHistory']['service_end'] = date("d-m-Y");

                    $this->request->data['OrderHistory']['status'] = '5';
                    $this->PropService->save($this->request->data);
                    $line = "New Property Valuation uploaded";
                    
					$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
					$html .= '<p>We have complete the valuation of your Property ' . $PropService['PropService']['prop_sname'] . ' and the report is now available here:</p>';
					$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot . 'accounts/Portfolio_ViewReportDetail/' .$id.'">Click to see property </a></p>';
					$html .= '<p>We thank you again for this opportunity to serve you</p>';
					$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
					$html .= '<p>We thank you again for this opportunity to serve you.</p>';
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                }

                $this->OrderHistory->save($this->request->data);
                $Email = new CakeEmail();
                $Email->config('smtp');
				$Email->emailFormat('html');
                /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
                $Email->to($userdata['0']['Websiteuser']['email']);
                $Email->subject($line);
                $Email->send($html);

                $this->Session->setFlash('The Property Service has been update Successfully!!!', 'default', array('class' => 'green'));
            }
        }

        $PropService = $this->PropService->findById($id);
        $this->request->data = $PropService;
        $this->set('PropService', $PropService);
    }

    public function admin_addservicedetail($id) {
        $target_dir = 'upload/propertyserviceimages/';
        if ($this->request->is('post')) {
            $this->request->data['PropServicestage']['id'] = $id;
            if ($this->PropServicestage->save($this->request->data)) {
                $lastinsertId = $this->PropServicestage->getLastInsertId();
                if (!empty($this->request->data['PropServicestage']['servicepics'])) {
                    foreach ($this->request->data['PropServicestage']['servicepics'] as $result) {

                        $name = $result['name'];

                        if (!empty($name)) {
                            // Check For Empty Values 
                            $tmprary_name = $result['tmp_name'];
                            $temp = explode(".", $name);
                            $newfilename = uniqid() . '.' . end($temp);
                            if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {

                                $this->PropServicephoto->create();
                                $this->PropServicephoto->save(
                                        array(
                                            "service_stage_id" => $lastinsertId,
                                            "photo_name" => $target_dir . $newfilename,
                                        )
                                );
                            }
                        }
                    }
                }


                $this->Session->setFlash('The Property Serivce has been saved Successfully!!!', 'default', array('class' => 'green'));

                unset($this->request->data);
            }
        }
    }

    public function admin_propertyDataUpload() {
    	$this->set('title', 'Upload Property Details');
        $this->layout = "admin";
    }


    /*
    CREATE TABLE `uploaded_property_details` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `user_type` varchar(255) DEFAULT NULL,
      `aggrement_type` varchar(255) DEFAULT NULL,
      `residential_type` varchar(255) DEFAULT NULL,
      `city` varchar(255) DEFAULT NULL,
      `project_name` varchar(255) DEFAULT NULL,
      `builder_name` varchar(255) DEFAULT NULL,
      `sublocality_1` varchar(255) DEFAULT NULL,
      `sublocality_2` varchar(255) DEFAULT NULL,
      `super_build_up_area` varchar(255) DEFAULT NULL,
      `build_up_area` varchar(255) DEFAULT NULL,
      `carpet_area` varchar(255) DEFAULT NULL,
      `bedrooms` varchar(255) DEFAULT NULL,
      `bathrooms` varchar(255) DEFAULT NULL,
      `balconies` varchar(255) DEFAULT NULL,
      `add_other_room_information` varchar(255) DEFAULT NULL,
      `total_floors` varchar(255) DEFAULT NULL,
      `property_to_floor` varchar(255) DEFAULT NULL,
      `no_of_covered_parking` varchar(255) DEFAULT NULL,
      `covered` varchar(255) DEFAULT NULL,
      `open` varchar(255) DEFAULT NULL,
      `offer_price_all_inclusive` varchar(255) DEFAULT NULL,
      `offer_price_per_sqft` varchar(255) DEFAULT NULL,
      `market_price_for_this_property_per_sqft` varchar(255) DEFAULT NULL,
      `offer_price_for_alive_asset` varchar(255) DEFAULT NULL,
      `offer_price_per_sqft_for_aliveasset` varchar(255) DEFAULT NULL,
      `market_or_fair_price_of_property` varchar(255) DEFAULT NULL,
      `delta` varchar(255) DEFAULT NULL,
      `per_sqft_price_for_alive_asset` varchar(255) DEFAULT NULL,
      `maintenance_annual_1_time_per_unit_per_month` varchar(255) DEFAULT NULL,
      `property_age` varchar(255) DEFAULT NULL,
      `availability` varchar(255) DEFAULT NULL,
      `possession_by` varchar(255) DEFAULT NULL,
      `transaction_type` varchar(255) DEFAULT NULL,
      `ownership` varchar(255) DEFAULT NULL,
      `images` varchar(255) DEFAULT NULL,
      `lifts` varchar(255) DEFAULT NULL,
      `park` varchar(255) DEFAULT NULL,
      `maintenance_staff` varchar(255) DEFAULT NULL,
      `visitor_parking` varchar(255) DEFAULT NULL,
      `water_storage` varchar(255) DEFAULT NULL,
      `vasstu_compliant` varchar(255) DEFAULT NULL,
      `intercom_facility` varchar(255) DEFAULT NULL,
      `fire_alarm` varchar(255) DEFAULT NULL,
      `centrally_air_conditioned` varchar(255) DEFAULT NULL,
      `piped_gas` varchar(255) DEFAULT NULL,
      `water_purifier` varchar(255) DEFAULT NULL,
      `internet_connectivity` varchar(255) DEFAULT NULL,
      `swimming_pool` varchar(255) DEFAULT NULL,
      `community` varchar(255) DEFAULT NULL,
      `fitness_centre` varchar(255) DEFAULT NULL,
      `security_personnel` varchar(255) DEFAULT NULL,
      `shopping_center` varchar(255) DEFAULT NULL,
      `rain_water_harvesting` varchar(255) DEFAULT NULL,
      `bank_attached_property` varchar(255) DEFAULT NULL,
      `power_backup` varchar(255) DEFAULT NULL,
      `weter_source` varchar(255) DEFAULT NULL,
      `facing` varchar(255) DEFAULT NULL,
      `width_of_facing_road` varchar(255) DEFAULT NULL,
      `type_of_flooring` varchar(255) DEFAULT NULL,
      `furnishing` varchar(255) DEFAULT NULL,
      `in_a_gated_society_description` varchar(1000) DEFAULT NULL,
      `corner_property_description` varchar(1000) DEFAULT NULL,
      `city1` varchar(255) DEFAULT NULL,
      `full_name` varchar(255) DEFAULT NULL,
      `mobile_number_1` varchar(255) DEFAULT NULL,
      `mobile_number_2` varchar(255) DEFAULT NULL,
      `email_id` varchar(255) DEFAULT NULL,
      `landline_1` varchar(255) DEFAULT NULL,
      `name` varchar(255) DEFAULT NULL,
      `mob_no` varchar(255) DEFAULT NULL,
      `price_quoted` varchar(255) DEFAULT NULL,
      `remarks` varchar(255) DEFAULT NULL,
      `name1` varchar(255) DEFAULT NULL,
      `mob_no1` varchar(255) DEFAULT NULL,
      `price_quoted1` varchar(255) DEFAULT NULL,
      `broker_2` varchar(255) DEFAULT NULL,
      `remarks1` varchar(255) DEFAULT NULL,
      `status` varchar(255) DEFAULT NULL,
      `link` varchar(1000) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    */
    public function admin_upload_property_details() {

       if ($this->request->is('post')) {
	   
	   		$userroledata = $this->MstUserrole->find('all');
			$userrole_arr = array();
			if(!empty($userroledata))
			{
				foreach($userroledata as $userrole_val)
				{
					$userrole_arr[$userrole_val['MstUserrole']['userrole_ID']] = strtoupper($userrole_val['MstUserrole']['Description']);
				}
			}
            //print 'asdf';
            require ROOT . '/vendors/phpoffice/phpexcel/Classes/PHPExcel.php';
           //print '111';
          // exit;
            $objPHPExcel = new PHPExcel();
            $filename = ROOT. DS . 'documents'. DS . $this->request->data['file']['name'];
            $file = $this->request->data['file']['tmp_name']; 

            $rows = [];
            $fileNameParts = explode('.', $filename);
            $ext = strtolower(end($fileNameParts));
            if($ext == 'xlsx') {
                $document = PHPExcel_IOFactory::load($file);    
                $rows = $document->getSheet(0)->toArray();
            } else if($ext == 'csv') {
                $file_handle = fopen($file, 'r');
                while (!feof($file_handle) ) {
                    $rows[] = fgetcsv($file_handle, 1024);
                }
                fclose($file_handle);
            } else {
                http_response_code(400);
                exit;
            }
//echo '<pre>';print_r($rows);exit;

            $start_row = 0;
            $col_index_to_skip = [0];
            foreach($rows as $row) {
                if($row[0] !== NULL) {
                    $col_index = 0;
                    foreach ($row as $col) {
                        if(empty($col) || is_null($col))
                            $col_index_to_skip[] = $col_index;
                        $col_index++;
                    }
                    $start_row++;
                    break;
                }
                $start_row++;
            }
			//array_push($col_index_to_skip,25,33,34,71,80,83);
//echo '<pre>';print_r($col_index_to_skip);
            $rows = array_slice($rows, $start_row);

            $columns = [
                "user_type",
                "aggrement_type",
                "residential_type",
                "city",
                "project_name",
                "builder_name",
                "sublocality_1",
                "sublocality_2",
                "super_build_up_area",
                "build_up_area",
                "carpet_area",
                "bedrooms",
                "bathrooms",
                "balconies",
                "add_other_room_information",
                "total_floors",
                "property_to_floor",
                "no_of_covered_parking",
                "covered",
                "open",
                "offer_price_all_inclusive",
                "offer_price_per_sqft",
				"market_or_fair_price_of_property",
				"market_price_for_this_property_per_sqft",
				"discount",
				"delta",
				"maintenance_annual_1_time_per_unit_per_month",                                                  
                "property_age",
                "availability",
                "possession_by",
                "transaction_type",
                "ownership",
				"covered1",
                "open1",
                "images",
                "lifts",
                "park",
                "maintenance_staff",
                "visitor_parking",
                "water_storage",
                "vasstu_compliant",
                "intercom_facility",
                "fire_alarm",
                "centrally_air_conditioned",
                "piped_gas",
                "water_purifier",
                "internet_connectivity",
                "swimming_pool",
                "community",
                "fitness_centre",
                "security_personnel",
                "shopping_center",
                "rain_water_harvesting",
                "bank_attached_property",
                "power_backup",
                "weter_source",
                "facing",
                "width_of_facing_road",
                "type_of_flooring",
                "furnishing",
                "in_a_gated_society_description",
                "corner_property_description",
                "city1",
                "full_name",
                "mobile_number_1",
                "mobile_number_2",
                "email_id",
                "landline_1",
                "name",
                "mob_no",
                "price_quoted",
                "remarks",
                "name1",
                "mob_no1",
                "price_quoted1",
                "broker_2",
                "remarks1",
                "status",
				"per_sqft_price_for_alive_asset",
				"offer_price_for_alive_asset",
                "offer_price_per_sqft_for_aliveasset",
                "link"
            ];
            $result = [];
            $rowInsertSuccessCount = 0;
            $rowInsertFailedCount = 0;
            // print 'ok'; exit;
            // print json_encode($rows); exit;
			$sql_trunc = "TRUNCATE TABLE uploaded_property_details";
				$this->UploadedPropertyDetail->query($sql_trunc);
				
            foreach ($rows as $index => $row) {
                $values = [];
                $i = 0;
                foreach ($row as $key => $col) {

                    if(!in_array($key, $col_index_to_skip)) {
                        $values[$columns[$i]] = $col;
                        $i++;     
                    }   

                    if($i == 79)
                        break;
                }
				//echo '<pre>';print_r($values);
				//exit;
				
                $this->UploadedPropertyDetail->create();
                $this->UploadedPropertyDetail->save($values);

                if($this->UploadedPropertyDetail->getAffectedRows() == 1) {
                    // $result[] = $this->PropertyDetailUpload->find([ 'id' => $this->PropertyDetail->id ]);
                    $rowInsertSuccessCount++;
                }
                else{
                    // $result[] = false;
                    $rowInsertFailedCount++;
                }
                $this->UploadedPropertyDetail->clear();
				
				$user_type_ = strtoupper($values['user_type']);
				if(in_array($user_type_,$userrole_arr))
				{
					$userrole_id = array_search($user_type_, $userrole_arr);
				}
				 //Save New User
				 $user_data = array();
				 $user_data['username'] = $values['full_name'];
				 $user_data['email'] = $values['email_id'];
				 $user_data['usermobile'] = $values['mobile_number_1'];
				 $user_data['password'] = '111111';
				 $user_data['userrole'] = $userrole_id;
				 $user_data['useractive'] = '1';
				//echo '<pre>';print_r($user_data);
				if ($this->Websiteuser->save($user_data)) {
					echo '1';						
				}
				$this->Websiteuser->clear();
            }
			$this->UploadedPropertyDetail->sProcedure();
			/*$ch = curl_init();
			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "http://49.50.76.151:8983/solr/farpkt_solr_data_prod/dataimport?command=full-import&clean=true&commit=true");
			curl_setopt($ch, CURLOPT_HEADER, 0);			
			// grab URL and pass it to the browser
			curl_exec($ch);			
			// close cURL resource, and free up system resources
			curl_close($ch);*/
			
            // header('Content-Type: application/json');
            print json_encode([ 'success' => $rowInsertSuccessCount, 'failed' => $rowInsertFailedCount] );
            exit;
        }
    }

    public function admin_masterDataEntry() {
        $this->set('title', 'Upload Master Data');
        $this->layout = "masterData";
        // $this->layout = false;
        // $this->render('/Accounts/admin_masterDataEntry');
    }

    public function admin_insertMasterData() {
        $requestBody    = json_decode(file_get_contents('php://input'), TRUE);        
        $moduleName     = $requestBody['moduleName'];
        $data           = [ $moduleName => $requestBody['data']];
        $this->$moduleName->save($data);
        $status = '';
        if($this->$moduleName->getAffectedRows() == 1) {
            $status = 'success';
        }
        else{
            $status = 'failed';
        }
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $this->response->body(json_encode([ 
            'moduleName' => $requestBody['moduleName'], 
            'status' => $status
        ]));
        return $this->response;
    }
	
	public function make_payment($id)
	{
		$PortService = $this->PortService->findById($id);
		if(!empty($PortService))
		{
		//echo '<pre>'; print_r($PortService);
		$userdata = $this->Websiteuser->find('all', array(
                    'conditions' => array(
                        'Websiteuser.id' => $PortService['PortService']['user_id']
                    )
                ));
		//echo '<pre>'; print_r($userdata);
		$name = $userdata[0]['Websiteuser']['username'];
		$amount = $PortService['PortService']['payment'];
		$email = $userdata[0]['Websiteuser']['email'];
		$product_info = $PortService['PortService']['port_name'];
		// Merchant key here as provided by Payu
		//$MERCHANT_KEY = "rjQUPktU";
		$MERCHANT_KEY = "gtKFFx";
		// Merchant Salt as provided by Payu
		//$SALT = "e5iIg1jwi8";
		$SALT = "eCwWELxi";
		// End point - change to https://secure.payu.in for LIVE mode
		$PAYU_BASE_URL = "https://test.payu.in";
		
		$action = '';
		
		$posted = array();
		
		 // Generate random transaction id
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		
		$posted['key'] = $MERCHANT_KEY;
		$posted['txnid'] = $txnid;
		$posted['amount'] = $amount;
		$posted['productinfo'] = $product_info;
		$posted['firstname'] = $name;
		$posted['email'] = $email;
		
		$hash = '';
		// Hash Sequence
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		
		  /*if(
				  empty($posted['key'])
				  || empty($posted['txnid'])
				  || empty($posted['amount'])
				  || empty($posted['firstname'])
				  || empty($posted['email'])
				  || empty($posted['phone'])
				  || empty($posted['productinfo'])
				  || empty($posted['surl'])
				  || empty($posted['furl'])
				  || empty($posted['service_provider'])
		  ) 
		  {
			$formError = 1;
		  } 
		  else 
		  {*/
			//$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
			$hashVarsSeq = explode('|', $hashSequence);
			$hash_string = '';	
			foreach($hashVarsSeq as $hash_var) 
			{
			  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
			  $hash_string .= '|';
			}
		
			$hash_string .= $SALT;
		
		//echo $hash_string;
			$hash = strtolower(hash('sha512', $hash_string));
			$action = $PAYU_BASE_URL . '/_payment';
		  
		//echo $action;
		$this->layout = "account";
		$this->set('action', $action);
		$this->set('MERCHANT_KEY', $MERCHANT_KEY);
		$this->set('hash', $hash);
		$this->set('txnid', $txnid);
		$this->set('txnid', $txnid);
		$this->set('userdata', $userdata[0]['Websiteuser']);
		$this->set('PortService', $PortService['PortService']);
		}
		else
		{
			$this->redirect('Portfolio_ViewReport');
		}
		$this->view = 'fp_pgform';
	}
	public function payment_success($id='')
	{
	}
	
	public function payment_failure($id='')
	{
	}
	
	
	public function add_sales_employees() {
		
		
		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		
		//echo '<pre>'; print_r($this->Session->read('Auth'));die();
		//echo $this->Session->read('Auth.Websiteuser.email');die();
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			);
			//echo '<pre>'; print_r($getBuilderIdArray);die(); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
			$getBrokerId =	$this->Session->read('Auth.Websiteuser.id');
			$userCreatedBy = 'Broker';		
			
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');			
			$getBrokerId =	0;
			$userCreatedBy = 'Builder';	
		}
		
		
        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile']))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{				
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $getBuilderId;
				$this->request->data['SalesUser']['broker_id'] = $getBrokerId;
				$this->request->data['SalesUser']['user_type'] = 'Sales';
				$this->request->data['SalesUser']['user_created_by'] = $userCreatedBy;
				//echo '<pre>'; print_r($this->request->data);die();
				if ($this->SalesUser->save($this->request->data['SalesUser'])) {
					$lastinsertId = $this->SalesUser->getLastInsertId();
					for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						//$projectName[0]['Project']['project_name'];
						//($projectName);
						//();
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Sales';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					
					
						$Email = new CakeEmail();
						$Email->viewVars(array(
								'user_email' => $this->request->data['SalesUser']['email'],
								'user_name' => $this->request->data['SalesUser']['name'],
								'user_mobile' => $this->request->data['SalesUser']['mobile'],
								'user_password' => $this->request->data['SalesUser']['password']));
						//$Email->config('smtp');
						
						//$mailtest = ''.$this->request->data['SalesUser']['email'].'';
						//echo $mailtest; die();
						
						$Email->template('android_app_sales','leads');
						$Email->emailFormat('html');
						$Email->subject('Welcome To Fair Pockets Partner App');
						$Email->to($this->request->data['SalesUser']['email']);
						$Email->cc('digpalsingh884@gmail.com');
						$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));
						//$Email->send();
						
						
						$output = $Email->send();
                            if ($output) {
                                // Password set
                                $this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
                            } else {
                                // Not able to set password	
                                $this->Session->setFlash('Unable to send Email , please check email.', 'default', array('class' => 'red'));
                            }
					
					
				}
			}
        }
		
		
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $getBuilderId
				)
			)
		);
		
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
        unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
    }
	
	
	public function edit_sales_employees1($sales_id){
		
		$this->layout = "account";
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		 $this->SalesUser->id = $sales_id;
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);
			//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
            if ($this->SalesUser->save($this->request->data)) {
				$this->ProjectsAndroidApp->deleteAll(array(
					'ProjectsAndroidApp.sales_or_broker_employee_id' => $sales_id				
					), false);
				for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $sales_id;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Sales';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
				
                $this->Session->setFlash('Updated Successfully.', 'default', array('class' => 'green'));
				$this->redirect('price_calculator_list_employees');
            }
        }
		
		
		$this->request->data = $this->SalesUser->findById($sales_id);
		$allsalesUsers = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'id' => $sales_id
                )
            ));
		//echo '<pre>'; print_r($allsalesUsers);die();
		$this->request->data['bspcharges'] = json_decode($allsalesUsers['0']['SalesUser']['projects'], 'true');
		
		$this->loadModel('Project');
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
		
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	
	public function edit_sales_employees($sales_id){
		
		$this->layout = "account";
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		 $this->SalesUser->id = $sales_id;
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);
			//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
            if ($this->SalesUser->save($this->request->data)) {
                $this->Session->setFlash('Updated Successfully.', 'default', array('class' => 'green'));
				$this->redirect('sales_employees_lists');
            }
        }
		
		
		$this->request->data = $this->SalesUser->findById($sales_id);
		$allsalesUsers = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'id' => $sales_id
                )
            ));
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	public function sales_employees_lists() {
		$this->loadModel('SalesUser');
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
		}
		
        if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['search'] . '%',
                        'SalesUser.email LIKE' => $this->request->data['search'] . '%',
                    ),
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'sales',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'conditions' => array(
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'sales',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
		}
		$allsalesUsers = $this->Paginator->paginate('SalesUser');
		//echo '<pre>'; print_r($allsalesUsers);die();
        //$this->set('allsalesUsers');
		$this->set('allsalesUsers', $allsalesUsers);
        $this->layout = 'account';
    }
	
	
	
	public function sales_employees_lists_csv() {
				$this->loadModel('SalesUser');

			$builder = $this->Builder->find('all', array(
				'conditions' => array(
					'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
							)
						));
			if (!empty($builder['0']['Builder']['status'])) {
				if ($builder['0']['Builder']['status'] != 3) {
						$this->redirect('/accounts/dashboard');
					}
				$this->set('status', $builder['0']['Builder']['status']);
			}
			$data = $this->SalesUser->find('all',
				array(
					'fields' => array('name','email','mobile','created'),
						'conditions' => array(
							//'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
								'builder_id' => 45,
									'user_type' => 'Sales',
										'user_created_by' => 'Builder'
							))
			);
			$headers = array(
				'SalesUser'=>array(
					'name' => 'Name',
						'email' => 'Email',
							'mobile' => 'Mobile',
								'created' => 'Created'
					)
			);
			array_unshift($data,$headers);
			$this->set(compact('data')); 
			$this->layout = 'account';
			}
	
	
	public function price_calculator_list_employees() {
		$this->loadModel('SalesUser');
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }		
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
		}
		
        if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['search'] . '%',
                        'SalesUser.email LIKE' => $this->request->data['search'] . '%',
                    ),
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'sales',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'conditions' => array(
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'sales',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
		}
		$allsalesUsers = $this->Paginator->paginate('SalesUser');
		//echo '<pre>'; print_r($allsalesUsers);die();
        //$this->set('allsalesUsers');
		$this->set('allsalesUsers', $allsalesUsers);
        $this->layout = 'account';
    }
	
	
	
	/*public function add_broker_employees() {
		
		//echo $this->Session->read('Auth.Websiteuser.userorgname');
	//Configure::write('debug', 2);
	//Configure::read('debug', 2);		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		//App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
		//$password = "123456";
		//$hasher = new BlowfishPasswordHasher();
		//echo $hasher->hash($password);
		
		
		//$hash_pwd = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		//echo $hash_pwd; 
		
        if ($this->request->is('post') || $this->request->is('put')) {
			
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile'],
			"email"=>$this->request->data['SalesUser']['email']))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{
					//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['SalesUser']['broker_id'] = 0;
				$this->request->data['SalesUser']['user_type'] = 'Broker';
				$this->request->data['SalesUser']['user_created_by'] = 'Builder';
				//$this->request->data['SalesUser']['company'] = $this->request->data['SalesUser']['company'];
				//echo '<pre>'; print_r($this->request->data);die();
				if ($this->SalesUser->save($this->request->data['SalesUser'])) {
					$lastinsertId = $this->SalesUser->getLastInsertId();
					for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						
						//$view = new View($this);
						//$Custom = $view->loadHelper('Number');
						//$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						// $projectName[0]['Project']['project_name'];
						//($projectName);
						//();
						$view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'builder';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					$this->request->data['Websiteuser']['userrole'] = 2;
						$this->request->data['Websiteuser']['username'] = $this->request->data['SalesUser']['name'];
						$this->request->data['Websiteuser']['userorgname'] = $this->request->data['SalesUser']['company'];
						$this->request->data['Websiteuser']['email'] = $this->request->data['SalesUser']['email'];
						$this->request->data['Websiteuser']['usermobile'] = $this->request->data['SalesUser']['mobile'];
						$this->request->data['Websiteuser']['password'] = $password;
						$this->request->data['Websiteuser']['regsitration_token'] = md5(date('Y-m-d H:i:s') . $this->request->data['SalesUser']['email'] . $this->request->data['SalesUser']['mobile']);
						
						$this->Websiteuser->save($this->request->data['Websiteuser']);
					$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					
					
					$Email = new CakeEmail();
						$Email->viewVars(array(
								'user_email' => $this->request->data['SalesUser']['email'],
								'builder_org_name' => $this->Session->read('Auth.Websiteuser.userorgname'),
								'user_name' => $this->request->data['SalesUser']['name'],
								'user_mobile' => $this->request->data['SalesUser']['mobile'],
								'user_password' => $this->request->data['SalesUser']['password']));
						$Email->config('smtp');
						
						$Email->template('android_app_brokers','leads');
						$Email->emailFormat('html');
						$Email->subject('Welcome To FairPockets and our Partner App');
						$Email->to($this->request->data['SalesUser']['email']);
						$Email->cc('digpalsingh884@gmail.com');
						$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));
						$Email->send();
				}
			}
        }
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
        unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
		
    }*/
	
	
	public function add_broker_employees() {
		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');	
		$this->loadModel('BuilderBroker');	
//$allbuilders = $this->BuilderBroker->find('all');
//echo '<pre>'; print_r($allbuilders);		
		
        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile'],
			"email"=>$this->request->data['SalesUser']['email']))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				//$this->request->data['SalesUser']['broker_id'] = 0;
				$this->request->data['SalesUser']['user_type'] = 'Broker';
				$this->request->data['SalesUser']['user_created_by'] = 'Builder';
				
				$this->request->data['Websiteuser']['userrole'] = 2;
				$this->request->data['Websiteuser']['username'] = $this->request->data['SalesUser']['name'];
				$this->request->data['Websiteuser']['userorgname'] = $this->request->data['SalesUser']['company'];
				$this->request->data['Websiteuser']['email'] = $this->request->data['SalesUser']['email'];
				$this->request->data['Websiteuser']['usermobile'] = $this->request->data['SalesUser']['mobile'];
				$this->request->data['Websiteuser']['password'] = $password;
				$this->request->data['Websiteuser']['regsitration_token'] = md5(date('Y-m-d H:i:s') . $this->request->data['SalesUser']['email'] . $this->request->data['SalesUser']['mobile']);
				//echo '<pre>'; print_r($this->request->data['Websiteuser']);die();
				if($this->Websiteuser->save($this->request->data['Websiteuser'])){
					$lastinsertIdwu = $this->Websiteuser->getLastInsertId();
					$this->request->data['SalesUser']['broker_id'] = $lastinsertIdwu;
					if ($this->SalesUser->save($this->request->data['SalesUser'])) {
						$lastinsertId = $this->SalesUser->getLastInsertId();
						
						$this->request->data['BuilderBroker']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['BuilderBroker']['broker_id'] = $lastinsertId;
						$this->request->data['BuilderBroker']['projects'] = json_encode($this->request->data['bspcharges'], true);
						$this->request->data['BuilderBroker']['created_at'] = date("Y-m-d H:i:s");
						
						$this->BuilderBroker->save($this->request->data['BuilderBroker']);
						
						for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
							$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
							$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
							 $view = new View($this);
							$Custom = $view->loadHelper('Number');
							$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
							
							$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
							$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
							$this->request->data['ProjectsAndroidApp']['status'] = 1;
							$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'builder';
							$this->request->data['ProjectsAndroidApp']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
							$this->request->data['ProjectsAndroidApp']['websiteuser_id'] = $lastinsertIdwu;
							$this->ProjectsAndroidApp->create(false);
							//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
							$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
						}
					}
					$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					
					$Email = new CakeEmail();
						$Email->viewVars(array(
								'user_email' => $this->request->data['SalesUser']['email'],
								'builder_org_name' => $this->Session->read('Auth.Websiteuser.userorgname'),
								'user_name' => $this->request->data['SalesUser']['name'],
								'user_mobile' => $this->request->data['SalesUser']['mobile'],
								'user_password' => $this->request->data['SalesUser']['password']));
						$Email->config('smtp');
						
						$Email->template('android_app_brokers','leads');
						$Email->emailFormat('html');
						$Email->subject('Welcome To FairPockets and our Partner App');
						$Email->to($this->request->data['SalesUser']['email']);
						$Email->cc('digpalsingh884@gmail.com');
						$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));
						$Email->send();
					
					
				}else{
					$this->Session->setFlash('Failed, Email/Mobile no already exist in our database.', 'default', array('class' => 'red'));
				}
				
				
				
			}
        }
		
		
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
        unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
		
    }
	
	
	
	public function edit_broker_employees($sales_id){
		
		$this->layout = "account";
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('BuilderBroker');		
		 $this->SalesUser->id = $sales_id;
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);
			//echo '<pre>'; print_r($this->request->data [broker_websiteuser_id]);die();
			$broker_websiteuser_id = $this->request->data ['broker_websiteuser_id'];
            if ($this->SalesUser->save($this->request->data)) {
				$this->ProjectsAndroidApp->deleteAll(array(
					'ProjectsAndroidApp.sales_or_broker_employee_id' => $sales_id				
					), false);
				for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $sales_id;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->request->data['ProjectsAndroidApp']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['ProjectsAndroidApp']['websiteuser_id'] = $broker_websiteuser_id;
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
				
                $this->Session->setFlash('Updated Successfully.', 'default', array('class' => 'green'));
            }
			$this->BuilderBroker->deleteAll(array(
					'BuilderBroker.broker_id' => $sales_id,
					'BuilderBroker.builder_id' => $this->Session->read('Auth.Websiteuser.id')
					), false);
					
			$this->request->data['BuilderBroker']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
			$this->request->data['BuilderBroker']['broker_id'] = $sales_id;
			$this->request->data['BuilderBroker']['projects'] = json_encode($this->request->data['bspcharges'], true);
			$this->request->data['BuilderBroker']['created_at'] = date("Y-m-d H:i:s");
			
			$this->BuilderBroker->save($this->request->data['BuilderBroker']);
        }
		
		
		$this->request->data = $this->SalesUser->findById($sales_id);
		//echo '<pre>'; print_r($this->request->data);//die();
		/*$allsalesUsers = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'id' => $sales_id
                )
            ));*/
		
		$allsalesUsers = $this->BuilderBroker->find('all', array(
                'conditions' => array(
                    'broker_id' => $sales_id,
					'builder_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
		
		//echo '<pre>'; print_r($allsalesUsers);die();
		//$this->request->data['bspcharges'] = json_decode($allsalesUsers['0']['BuilderBroker']['projects'], 'true');
		$this->request->data['bspcharges'] = json_decode($this->request->data['SalesUser']['projects'], 'true');
		
		$this->loadModel('Project');
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
		
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	
	public function edit_broker_employees1($sales_id){
		
		$this->layout = "account";
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('BuilderBroker');		
		 $this->SalesUser->id = $sales_id;
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);
			//echo '<pre>'; print_r($this->request->data [broker_websiteuser_id]);die();
			$broker_websiteuser_id = $this->request->data ['broker_websiteuser_id'];
            if ($this->SalesUser->save($this->request->data)) {
				$this->ProjectsAndroidApp->deleteAll(array(
					'ProjectsAndroidApp.sales_or_broker_employee_id' => $sales_id				
					), false);
				for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $sales_id;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->request->data['ProjectsAndroidApp']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['ProjectsAndroidApp']['websiteuser_id'] = $broker_websiteuser_id;
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
				
                $this->Session->setFlash('Updated Successfully.', 'default', array('class' => 'green'));
            }
			$this->BuilderBroker->deleteAll(array(
					'BuilderBroker.broker_id' => $sales_id,
					'BuilderBroker.builder_id' => $this->Session->read('Auth.Websiteuser.id')
					), false);
					
			$this->request->data['BuilderBroker']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
			$this->request->data['BuilderBroker']['broker_id'] = $sales_id;
			$this->request->data['BuilderBroker']['projects'] = json_encode($this->request->data['bspcharges'], true);
			$this->request->data['BuilderBroker']['created_at'] = date("Y-m-d H:i:s");
			
			$this->BuilderBroker->save($this->request->data['BuilderBroker']);
        }
		
		
		$this->request->data = $this->SalesUser->findById($sales_id);
		//echo '<pre>'; print_r($this->request->data);//die();
		/*$allsalesUsers = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'id' => $sales_id
                )
            ));*/
		
		$allsalesUsers = $this->BuilderBroker->find('all', array(
                'conditions' => array(
                    'broker_id' => $sales_id,
					'builder_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
		
		//echo '<pre>'; print_r($allsalesUsers);die();
		//$this->request->data['bspcharges'] = json_decode($allsalesUsers['0']['BuilderBroker']['projects'], 'true');
		$this->request->data['bspcharges'] = json_decode($this->request->data['SalesUser']['projects'], 'true');
		
		$this->loadModel('Project');
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
		
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	public function clone_broker_employees($email_id){
		
		$this->layout = "account";
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('BuilderBroker');
		
		 $salesIdArray = $this->SalesUser->find('all', array(
            'fields' => array(
                'id',
				'builder_id',
				'broker_id'
            ),
            'conditions' => array(
                'email' =>$email_id
            )
        ));
		//print_r($salesIdArray);
		
		//echo $salesIdArray[0]['SalesUser']['id'];
		 //die();
		//echo $this->Session->read('Auth.Websiteuser.id');
		//$this->SalesUser
		 $this->SalesUser->id = $salesIdArray[0]['SalesUser']['id'];
		if ($this->request->is('post') || $this->request->is('put')) {
			//$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);
			//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
           // if ($this->SalesUser->save($this->request->data)) {
				$conditions1 = array(
				'BuilderBroker.broker_id' => $salesIdArray[0]['SalesUser']['id'],
				'BuilderBroker.builder_id' => $this->Session->read('Auth.Websiteuser.id')
				);
				if($this->BuilderBroker->hasAny($conditions1)){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{				
				for( $i=1; $i <= count($this->request->data['bspcharges'])/3; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $salesIdArray[0]['SalesUser']['id'];
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['ProjectsAndroidApp']['websiteuser_id'] = $salesIdArray[0]['SalesUser']['broker_id'];
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'builder';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					
					$this->request->data['BuilderBroker']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
					$this->request->data['BuilderBroker']['broker_id'] = $salesIdArray[0]['SalesUser']['id'];
					$this->request->data['BuilderBroker']['projects'] = json_encode($this->request->data['bspcharges'], true);
					$this->request->data['BuilderBroker']['created_at'] = date("Y-m-d H:i:s");
					//echo '<pre>'; print_r($this->request->data['BuilderBroker']);die();
					$this->BuilderBroker->save($this->request->data['BuilderBroker']);
				
                $this->Session->setFlash('Added in your network Successfully.', 'default', array('class' => 'green'));
           // }
		   
		   }
        }
		$sales_id = $salesIdArray[0]['SalesUser']['id'];
		
		$this->request->data = $this->SalesUser->findById($sales_id);
		/*$allsalesUsers = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'id' => $sales_id
                )
            ));
		//echo '<pre>'; print_r($allsalesUsers);die();
		$this->request->data['bspcharges'] = json_decode($allsalesUsers['0']['SalesUser']['projects'], 'true');*/
		
		$this->loadModel('Project');
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
		
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	
	public function broker_employees_lists() {
		$this->loadModel('SalesUser');
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
		if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['search'] . '%',
                        'SalesUser.email LIKE' => $this->request->data['search'] . '%',
                    ),
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'Broker',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'conditions' => array(
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'Broker',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
		}
		$allsalesUsers = $this->Paginator->paginate('SalesUser');
		
        /*$allsalesUsers = $this->SalesUser->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'user_type' => 'broker'
            )
        ));*/
        //$this->set('allsalesUsers');
		$this->set('allsalesUsers', $allsalesUsers);
        $this->layout = 'account';
    }
	
	
	
	public function price_calculator_list_brokers() {
		$this->loadModel('SalesUser');
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
        if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['search'] . '%',
                        'SalesUser.email LIKE' => $this->request->data['search'] . '%',
                    ),
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'Broker',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'conditions' => array(
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'user_type' => 'Broker',
					'user_created_by' => 'Builder'
				),
				'order' => 'created DESC',
                'limit' => 8
            );
		}
		$allsalesUsers = $this->Paginator->paginate('SalesUser');
        //$this->set('allsalesUsers');
		$this->set('allsalesUsers', $allsalesUsers);
        $this->layout = 'account';
    }
	
	
	public function deleteSalesEmployees($sales_employees_id = null){
        $this->autoRender = false;
        //echo '884'; die();
		$this->loadModel('SalesUser');
        # delete inventory properties
        //if($this->SalesUser->deleteAll(array('SalesUser.id' => $sales_employees_id))){
		//$this->Session->setFlash('Sales Employee deleted!');
        //$this->redirect('/accounts/sales_employees_lists');
		//$this->redirect(array('controller' => 'accounts', 'action' => 'sales_employees_lists'));		
			
		//}
        //exit;
		//print_r($_POST);die();
		if(!empty($_POST))
		{	
			if($this->SalesUser->deleteAll(array('SalesUser.id' => $_POST['pid'])))
				{
					print 1;          
					exit();
				}else {
					print 0;
				}
		}		
		die;
        # delete inventory
        //$this->InvInventory->deleteAll(array("InvInventory.id"=>$inventory_id), false);
        
        
    }
	
	public function deleteBrokerEmployees($broker_employees_id = null){
        $this->autoRender = false;
        $this->loadModel('SalesUser');
        # delete inventory properties
        $this->SalesUser->deleteAll(array('SalesUser.id' => $broker_employees_id), false);
        
        # delete inventory
        //$this->InvInventory->deleteAll(array("InvInventory.id"=>$inventory_id), false);
        $this->Session->setFlash('Broker deleted!');
        return $this->redirect('/accounts/broker_employees_lists');
        
    }
	
	
	public function share_projects_inventory($project_id) {
		$this->loadModel('InvProjectsHistory');
		$this->loadModel('InvProjects');
		$this->loadModel('Project');
		$getInvProjectsArray = $this->InvProjects->find('all',
			array(
				  'conditions'=>array(
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'), 'project_id' => $project_id
				)
			)
		);
        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>';print_r($this->request->data);die();
			
			if(isset($getInvProjectsArray) && !empty($getInvProjectsArray)){
				//echo 'yes';
				
				$this->InvProjectsHistory->deleteAll(array(
					'InvProjectsHistory.builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'InvProjectsHistory.project_id' => $project_id
					), false);
				
				for( $i=1; $i <= count($this->request->data['invprojects'])/5; $i++ ){
					$this->request->data['InvProjectsHistory']['inv_projects_id'] = $getInvProjectsArray[0]['InvProjects']['id'];
					$this->request->data['InvProjectsHistory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
					$this->request->data['InvProjectsHistory']['project_id'] = $project_id;
					$this->request->data['InvProjectsHistory']['tower_no'] =  $this->request->data['invprojects']['tower'.$i.''];
					$this->request->data['InvProjectsHistory']['wing'] =  $this->request->data['invprojects']['wing'.$i.''];
					$this->request->data['InvProjectsHistory']['total_floor_from'] =  $this->request->data['invprojects']['total_floor_from'.$i.''];
					$this->request->data['InvProjectsHistory']['total_floor_to'] =  $this->request->data['invprojects']['total_floor_to'.$i.''];
					$this->request->data['InvProjectsHistory']['flats_per_floor'] =  $this->request->data['invprojects']['flats_per_floor'.$i.''];
					
					$this->InvProjectsHistory->create(false);
					$this->InvProjectsHistory->save($this->request->data['InvProjectsHistory']);
					//$this->request->data['invprojects'] = json_encode($getInvProjectsArray[0]['InvProjects']['proj_inv_json'], true);
					
					//$this->InvProjects->updateAll(array('proj_inv_json'=>json_encode($this->request->data['invprojects'], true))
										//,array('id'=>2));
										$projectjsonArr = json_encode($this->request->data['invprojects'], true);
					$this->InvProjects->updateAll(array("InvProjects.proj_inv_json" => "'{$projectjsonArr}'"), array("InvProjects.id" => $getInvProjectsArray[0]['InvProjects']['id']));
					//$this->redirect(array('controller' => 'accounts', 'action' => 'share_projects_inventory_view'));		
				}
				$this->Session->setFlash("Project's Inventory Configured Successfully", "default", array("class" => "green"));
				$this->redirect(array('controller' => 'accounts', 'action' => 'share_projects_inventory_view'));				
			}else{	
//echo 'no';			
				$this->request->data['InvProjects']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['InvProjects']['project_id'] = $project_id;
				$this->request->data['InvProjects']['proj_inv_json'] = json_encode($this->request->data['invprojects'], true);
				$this->request->data['InvProjects']['status'] = 1;
				if ($this->InvProjects->save($this->request->data)) {
					$lastinsertIdInvProjects = $this->InvProjects->getLastInsertId();
					for( $i=1; $i <= count($this->request->data['invprojects'])/5; $i++ ){
						$this->request->data['InvProjectsHistory']['inv_projects_id'] = $lastinsertIdInvProjects;
						$this->request->data['InvProjectsHistory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['InvProjectsHistory']['project_id'] = $project_id;
						$this->request->data['InvProjectsHistory']['tower_no'] =  $this->request->data['invprojects']['tower'.$i.''];
						$this->request->data['InvProjectsHistory']['wing'] =  $this->request->data['invprojects']['wing'.$i.''];
						$this->request->data['InvProjectsHistory']['total_floor_from'] =  $this->request->data['invprojects']['total_floor_from'.$i.''];
						$this->request->data['InvProjectsHistory']['total_floor_to'] =  $this->request->data['invprojects']['total_floor_to'.$i.''];
						$this->request->data['InvProjectsHistory']['flats_per_floor'] =  $this->request->data['invprojects']['flats_per_floor'.$i.''];
						
						$this->InvProjectsHistory->create(false);
						$this->InvProjectsHistory->save($this->request->data['InvProjectsHistory']);
					}
					$this->Session->setFlash("Project's Inventory Configured Successfully", "default", array("class" => "green"));
					$this->redirect(array('controller' => 'accounts', 'action' => 'share_projects_inventory_view'));
					//die();
				}
			}
		}
		
		$getInvProjectsArray1 = $this->InvProjects->find('all',
			array(
				  'conditions'=>array(
					'builder_id' => $this->Session->read('Auth.Websiteuser.id'), 'project_id' => $project_id
				)
			)
		);
		//$invprojects1data = 
		$this->request->data['invprojects'] = json_decode($getInvProjectsArray1[0]['InvProjects']['proj_inv_json'], true);
		//echo '<pre>'; print_r($this->request->data);die();
		//$this->set('getProjects1', $getProjects1);
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
       // unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		$this->set('project_id', $project_id);
		
    }
	
	public function share_projects_inventory_view() {
		$this->layout = 'account';
        //$builder_id = CakeSession::read('Auth.Websiteuser.id');

$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }		
        /*$projects1 = $this->Project->find('list',array(
		'fields' => array('DISTINCT(Property.project_id)'),
		'joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.project_id = Project.project_id',
						'Project.user_id' => $this->Session->read('Auth.Websiteuser.id'))))
		));*/		
		$ProjectsHaveProp = $this->Property->find('all',array(
			'fields'=>array('DISTINCT(project_id)'),
			'conditions'=>array('user_id'=>$this->Session->read('Auth.Websiteuser.id'))
		));
		$ProjectsHavePropArr = Set::extract('/Property/project_id', $ProjectsHaveProp);	
		//echo '<pre>'; print_r($ProjectsHavePropArr);die();
		$comma_separated = implode(",", $ProjectsHavePropArr);					
			$arr=explode(",",$comma_separated);
		//print_r($arr);
		
		
		 if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
                'conditions' => array(
					'OR' => array(
                        'Project.project_name LIKE' => $this->request->data['search'] . '%',
                        'Project.locality LIKE' => $this->request->data['search'] . '%',
                    ),
					'project_id' => $arr
				),
				'order' => 'created_date DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'conditions' => array(
					'project_id' => $arr
				),
				'order' => 'created_date DESC',
                'limit' => 8
            );
		}
		$projects = $this->Paginator->paginate('Project');
		/*$projects = $this->Project->find('all',array(
		'conditions' => array("Project.project_id"=>$arr)));*/
		
		//echo '<pre>'; print_r($projects);die();
		
        //$projects = $this->InvProject->getProjectByUserId($this->Session->read('Auth.Websiteuser.id'));
		//echo '<pre>'; print_r($projects);die();
        $this->set('projects', $projects);
		//echo $builder_id;
		$this->set('builder_id', $builder_id);
		
	}
	
	public function getTowerListByProjectId() {
        $propertyoption = array();
			$this->loadModel('InvProjectsHistory');
        if (isset($this->request['data']['project_id'])) {

            $propertyoption = $this->InvProjectsHistory->find('list', array(
                'fields' => array(
                    //'project_id',
                    'tower_no'
                ),
                'conditions' => array(
                    'builder_id' =>$this->Session->read('Auth.Websiteuser.id'),
					'project_id' => $this->request['data']['project_id'],
                )
            ));			
			}

        header('Content-Type: application/json');
        echo json_encode($propertyoption);
        exit();
    }
	
	public function getwingsByProjectIdTowerName() {
        $propertyoption = array();
			$this->loadModel('InvProjectsHistory');
			 //print_r($this->request['data']); die();
        if (isset($this->request['data']['project_id'])) {

            $propertyoption = $this->InvProjectsHistory->find('list', array(
                'fields' => array(
                    //'project_id',
                    'wing'
                ),
                'conditions' => array(
                    'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'project_id' => $this->request['data']['project_id'],
					'tower_no' => $this->request['data']['tower_name'],
                )
            ));			
			}
//print_r($propertyoption); die();
        header('Content-Type: application/json');
        echo json_encode($propertyoption);
        exit();
    }
	
	
	public function getinvDetailsByProjectIdTowerNameWing() {
		$this->autoRendor = false;
        $propertyoption = array();
		$this->loadModel('InvProjectsHistory');
		// print_r($this->request['data']); die();
        if (isset($this->request['data']['project_id'])) {
            $propertyoption = $this->InvProjectsHistory->find('all', array(
                'fields' => array(
                    //'project_id',
                    'total_floor_from',
					'total_floor_to',
					'flats_per_floor'
                ),
                'conditions' => array(
                    'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
					'project_id' => $this->request['data']['project_id'],
					'tower_no' => $this->request['data']['tower_name'],
					'wing' => $this->request['data']['wing_name']
                )
            ));			
		}
        header('Content-Type: application/json');		
		echo json_encode(array(
						 'total_floor_from' => $propertyoption[0]['InvProjectsHistory']['total_floor_from'],
						 'total_floor_to' => $propertyoption[0]['InvProjectsHistory']['total_floor_to'],
						 'flats_per_floor' => $propertyoption[0]['InvProjectsHistory']['flats_per_floor']
					 ));
		
        exit();
    }
	
	
	public function myProjects(){
		$this->loadModel('ShareProjectsHistory');
		$this->loadModel('SalesUser');
		
		$getBrokerIdBySalesUserTable = $this->SalesUser->find('all', array(                
                'conditions' => array(
                    'broker_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
			
			//echo '<pre>'; print_r($getBrokerIdBySalesUserTable);
			
			//echo $getBrokerIdBySalesUserTable[0]['SalesUser']['id'];
			
			//echo $this->Session->read('Auth.Websiteuser.id');	
		$sharedProjectsArray = $this->ShareProjectsHistory->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'builder_broker_share_projects',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'builder_broker_share_projects.share_project_history_id = ShareProjectsHistory.id',
							'builder_broker_share_projects.status =1'
                        )
                    )
                ),
                'conditions' => array(
                    'builder_broker_share_projects.broker_id' => $getBrokerIdBySalesUserTable[0]['SalesUser']['id']
                )
            ));
		
		//echo '<pre>'; print_r($sharedProjectsArray); die();
		
		$this->set('sharedProjectsArray', $sharedProjectsArray);
        $this->layout = 'account';
	}
	
	
	public function myProjectsView($project_id){
		$this->loadModel('ShareProjectsHistory');
		$this->loadModel('SalesUser');
		
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                //'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		//echo '<pre>'; print_r($ShareProjectsHistoryData);
		
		$project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
		
		$this->layout = 'account';
	}
	
	public function myInventories(){
		$this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
		
		$this->loadModel('SalesUser');
		
		$getBrokerIdBySalesUserTable = $this->SalesUser->find('all', array(                
                'conditions' => array(
                    'broker_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
			
			
		$this->loadModel('BuilderBrokerShareInventory');
		
		
		$ShareInvectoriesHistoryData = $this->BuilderBrokerShareInventory->find('all', array(
            'conditions' => array(
                'broker_id' => $getBrokerIdBySalesUserTable[0]['SalesUser']['id'],
				'status' => 1,
            )
        ));
		
		//echo '<pre>'; print_r($ShareInvectoriesHistoryData);
		
        $teamarray = '';
        foreach ($ShareInvectoriesHistoryData as $team) {

            $teamarray .= $team['BuilderBrokerShareInventory']['inventory_id'];
            $teamarray .= ',';
        }
        $trim_data = rtrim($teamarray, ',');
        $trim_data_array = explode(",", $trim_data);
		
		//print_r($trim_data_array);die();
		
		
		
		
		$user_id =45;
		$data_arr = $this->InvInventory->find('all', array(
            'fields' => array("InvInventory.*","InvProject.project_name"),
            'conditions' => array(/*"InvWebsiteuser.id = '{$user_id}'",*/
									"InvInventory.id"=>$trim_data_array),
            'joins' => array(
                array(
                    'table' => 'projects',
                    'alias' => 'InvProject',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvProject.project_id = InvInventory.project_id")
                ),
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
                )
            )
                    
        ));
        //return $data_arr;
		//echo '<pre>'; print_r($data_arr);
		$this->set('inventories',$data_arr);
		
	}
	
	
	
	public function inventoriesHistoryBuilderPanel(){
		$this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
		
		$this->loadModel('SalesUser');
		
		$getBrokerIdBySalesUserTable = $this->SalesUser->find('all', array(                
                'conditions' => array(
                    'broker_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
			
			
		$this->loadModel('BuilderBrokerShareInventory');
		
		
		$ShareInvectoriesHistoryData = $this->BuilderBrokerShareInventory->find('all', array(
			//'fields' => array('DISTINCT BuilderBrokerShareInventory.broker_id,BuilderBrokerShareInventory.broker_id'),
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'status' => 1,
            )
        ));
		
		//echo '<pre>'; print_r($ShareInvectoriesHistoryData);
		$this->set('ShareInvectoriesHistoryData',$ShareInvectoriesHistoryData);
		
        $teamarray = '';
        foreach ($ShareInvectoriesHistoryData as $team) {

            $teamarray .= $team['BuilderBrokerShareInventory']['inventory_id'];
            $teamarray .= ',';
        }
        $trim_data = rtrim($teamarray, ',');
        $trim_data_array = explode(",", $trim_data);
		
		//print_r($trim_data_array);die();
		
		
		
		
		$user_id =$this->Session->read('Auth.Websiteuser.id');
		$data_arr = $this->InvInventory->find('all', array(
            'fields' => array("InvInventory.*","InvProject.project_name","InvBuilderBrokerShareInventory.broker_id"),
            'conditions' => array("InvWebsiteuser.id = '{$user_id}'"),
            'joins' => array(
                array(
                    'table' => 'projects',
                    'alias' => 'InvProject',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvProject.project_id = InvInventory.project_id")
                ),
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
                ),
                array(
                    'table' => 'builder_broker_share_inventories',
                    'alias' => 'InvBuilderBrokerShareInventory',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvBuilderBrokerShareInventory.inventory_id = InvInventory.id",
											"InvBuilderBrokerShareInventory.builder_id = '{$user_id}'",
											"InvBuilderBrokerShareInventory.status"=>1)
                )
            )
                    
        ));
        //return $data_arr;
		//echo '<pre>'; print_r($data_arr);die();
		$this->set('inventories',$data_arr);		
	}
	
	
	
	public function inventoriesHistorySalesBuilderPanel(){
		$this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
		
		$this->loadModel('SalesUser');
		
		$getBrokerIdBySalesUserTable = $this->SalesUser->find('all', array(                
                'conditions' => array(
                    'broker_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
			
			
		$this->loadModel('BuilderSalesShareInventory');
		
		
		$ShareInvectoriesHistoryData = $this->BuilderSalesShareInventory->find('all', array(
			//'fields' => array('DISTINCT BuilderBrokerShareInventory.broker_id,BuilderBrokerShareInventory.broker_id'),
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'status' => 1,
            )
        ));
		
		//echo '<pre>'; print_r($ShareInvectoriesHistoryData);
		$this->set('ShareInvectoriesHistoryData',$ShareInvectoriesHistoryData);
		
        $teamarray = '';
        foreach ($ShareInvectoriesHistoryData as $team) {

            $teamarray .= $team['BuilderSalesShareInventory']['inventory_id'];
            $teamarray .= ',';
        }
        $trim_data = rtrim($teamarray, ',');
        $trim_data_array = explode(",", $trim_data);
		
		//print_r($trim_data_array);die();
		
		
		
		
		$user_id =$this->Session->read('Auth.Websiteuser.id');
		
		
		if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(					
					'fields' => array("InvInventory.*","InvProject.project_name","InvBuilderSalesShareInventory.sales_id"),
					'conditions' => array("InvWebsiteuser.id = '{$user_id}'",
						'OR' => array(
							'InvInventory.tower_name LIKE' => $this->request->data['search'] . '%'
							//'InvProject.project_name LIKE' => $this->request->data['search'] . '%',
						)
					),					
					'joins' => array(
						array(
							'table' => 'projects',
							'alias' => 'InvProject',
							'type' => 'inner',
							'foreignKey' => false,
							'conditions' => array("InvProject.project_id = InvInventory.project_id")
						),
						array(
							'table' => 'websiteusers',
							'alias' => 'InvWebsiteuser',
							'type' => 'inner',
							'foreignKey' => false,
							'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
						),
						array(
							'table' => 'builder_sales_share_inventories',
							'alias' => 'InvBuilderSalesShareInventory',
							'type' => 'inner',
							'foreignKey' => false,
							'conditions' => array("InvBuilderSalesShareInventory.inventory_id = InvInventory.id",
													"InvBuilderSalesShareInventory.builder_id = '{$user_id}'",
													"InvBuilderSalesShareInventory.status"=>1)
						)
					),
				'order' => 'created DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'fields' => array("InvInventory.*","InvProject.project_name","InvBuilderSalesShareInventory.sales_id"),
				'conditions' => array("InvWebsiteuser.id = '{$user_id}'"),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					),
					array(
						'table' => 'builder_sales_share_inventories',
						'alias' => 'InvBuilderSalesShareInventory',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvBuilderSalesShareInventory.inventory_id = InvInventory.id",
												"InvBuilderSalesShareInventory.builder_id = '{$user_id}'",
												"InvBuilderSalesShareInventory.status"=>1)
					)
				),
				'order' => 'created DESC',
                'limit' => 8
            );
		}
		$data_arr = $this->Paginator->paginate('InvInventory');
		
		
		/*$data_arr = $this->InvInventory->find('all', array(
            'fields' => array("InvInventory.*","InvProject.project_name","InvBuilderSalesShareInventory.sales_id"),
            'conditions' => array("InvWebsiteuser.id = '{$user_id}'"),
            'joins' => array(
                array(
                    'table' => 'projects',
                    'alias' => 'InvProject',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvProject.project_id = InvInventory.project_id")
                ),
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
                ),
                array(
                    'table' => 'builder_sales_share_inventories',
                    'alias' => 'InvBuilderSalesShareInventory',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvBuilderSalesShareInventory.inventory_id = InvInventory.id",
											"InvBuilderSalesShareInventory.builder_id = '{$user_id}'",
											"InvBuilderSalesShareInventory.status"=>1)
                )
            )
                    
        ));*/
        //return $data_arr;
		//echo '<pre>'; print_r($data_arr);die();
		$this->set('inventories',$data_arr);		
	}
	
	
	
	
	public function myInventoriesView($inventory_id){
		
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');

        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;


        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $inventory_property = $this->InvInventoryProperty->getInventoryProperty($inventory_id);
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $temp_arr = array();
        if (is_array($inventory_property) && count($inventory_property) > 0) {
            foreach ($inventory_property as $property) {
                $temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
            }
        }
        $inventory_property_arr = $temp_arr;
        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
        $this->set('inventory_property', $inventory_property);
        $this->set('inventory_property_arr', $inventory_property_arr);
        
        # summarry
        $flat_counter = $this->InvInventoryProperty->getCounterByInventoryId($inventory_id);
        $flat_count_by_status = $this->InvInventoryProperty->getCounterByStatus($inventory_id);
        
        $this->set('flat_counter', $flat_counter);
        $this->set('flat_count_by_status', $flat_count_by_status);
	}
	
	public function sales_employees_lists_broker() {
		$this->loadModel('SalesUser');
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
        $allsalesUsers = $this->SalesUser->find('all', array(
            'conditions' => array(
                'broker_id' => $this->Session->read('Auth.Websiteuser.id'),
				'user_type' => 'Broker_sales',
				'user_created_by' => 'Broker'
            )
        ));
        //$this->set('allsalesUsers');
		$this->set('allsalesUsers', $allsalesUsers);
        $this->layout = 'account';
    }
	
	public function builder_lists_broker(){
			$this->layout = "account";
			$this->loadModel('SalesUser');
			//echo $this->Session->read('Auth.Websiteuser.id');
			$getbrokerIdByBuilderId = $this->SalesUser->find('all',array(
				'fields'=> array('id'),
				'conditions'=> array(
					'broker_id'=>$this->Session->read('Auth.Websiteuser.id')
					)
						)
							);
		//echo '<pre>'; print_r($getbrokerIdByBuilderId);die();
		$buildersListArray= $this->Websiteuser->find('all', array(
			'fields'=> array('id','userorgname','acccreatedate'),
			'joins' => array(array('table' => 'builder_brokers',
                        'alias' => 'BuilderBroker',
                        'type' => 'INNER',
                        'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $getbrokerIdByBuilderId[0]['SalesUser']['id'])))));
		//echo '<pre>'; print_r($buildersDropdown);die();

		$this->set('buildersListArray', $buildersListArray);
			
		}
		
		
		
		public function add_sales_employees_broker() {
			//echo $this->Session->read('Auth.Websiteuser.id');
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('BrokerSales');
        if ($this->request->is('post') || $this->request->is('put')) {
			
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile'],
			"email"=>$this->request->data['SalesUser']['email'],
			"user_created_by"=>'Broker'))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{				
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				
				$getbuilderIdByBrokerId = $this->SalesUser->find('all',array(
							'fields'=> array('builder_id'),
							'conditions'=> array(
								'broker_id'=>$this->Session->read('Auth.Websiteuser.id'),
								'user_type'=>'Broker'
								)
									)
										);
				//echo '<pre>'; print_r($getbuilderIdByBrokerId);die();
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $getbuilderIdByBrokerId[0]['SalesUser']['builder_id'];
				$this->request->data['SalesUser']['broker_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['SalesUser']['share_projects_check'] = $this->request->data['SalesUser']['share_check'];
				$this->request->data['SalesUser']['user_type'] = 'Broker_sales';
				$this->request->data['SalesUser']['user_created_by'] = 'Broker';
				//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
				if ($this->SalesUser->save($this->request->data['SalesUser'])) {
					$lastinsertId = $this->SalesUser->getLastInsertId();
					
						$getbrokerIdByBuilderId = $this->SalesUser->find('all',array(
							'fields'=> array('id'),
							'conditions'=> array(
								'broker_id'=>$this->Session->read('Auth.Websiteuser.id')
								)
									)
										);
					
						//$this->request->data['BrokerSales']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['BrokerSales']['broker_id'] = $getbrokerIdByBuilderId[0]['SalesUser']['id'];
						$this->request->data['BrokerSales']['sales_id'] = $lastinsertId;						
						$this->request->data['BrokerSales']['projects'] = json_encode($this->request->data['bspcharges'], true);
						$this->request->data['BrokerSales']['status'] = 1;
						//$this->request->data['BrokerSales']['created_at'] = date("Y-m-d H:i:s");
						
						$this->BrokerSales->save($this->request->data['BrokerSales']);
					
					
					for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name'];
						if($this->request->data['bspcharges']['proj_disc'.$i.''] == ''){
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  0;
							
						}else{
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						}
						
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					if($this->request->data['SalesUser']['email']!=''){
						$Email = new CakeEmail();
						$Email->viewVars(array(
								'user_email' => $this->request->data['SalesUser']['email'],
								'user_name' => $this->request->data['SalesUser']['name'],
								'user_mobile' => $this->request->data['SalesUser']['mobile'],
								'user_password' => $this->request->data['SalesUser']['password']));					
						
						$Email->template('android_app_sales','leads');
						$Email->emailFormat('html');
						$Email->subject('Welcome To Fair Pockets Partner App');
						$Email->to($this->request->data['SalesUser']['email']);
						$Email->cc('digpalsingh884@gmail.com');
						$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));						
						
						$output = $Email->send();
							if ($output) {
								// Password set
								$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
								$this->redirect('sales_employees_lists_broker');
							} else {
								// Not able to set password	
								$this->Session->setFlash('Unable to send Email , please check email.', 'default', array('class' => 'red'));
							}
					}else{
						$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					}
				}
			}
        }
		
		$getbrokerIdByBuilderId = $this->SalesUser->find('all',
										array(
										'fields'=> array('id'),
										'conditions'=> array('broker_id'=>$this->Session->read('Auth.Websiteuser.id'))
										)
										);
										
										//echo '<pre>'; print_r($getbrokerIdByBuilderId);
		
		$buildersDropdown = $this->Websiteuser->find('list', array(
			'fields'=> array('id','userorgname'),
			'joins' => array(array('table' => 'builder_brokers',
                        'alias' => 'BuilderBroker',
                        'type' => 'INNER',
                        'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $getbrokerIdByBuilderId[0]['SalesUser']['id'])))));
		//echo '<pre>'; print_r($buildersDropdown);

		$this->set('buildersDropdown', $buildersDropdown);		
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
        unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
    }
		
		
		
		public function add_sales_employees_broker1() {
		//echo $this->Session->read('Auth.Websiteuser.id');
		$this->loadModel('SalesUser');
		//$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('BrokerSales');
        if ($this->request->is('post') || $this->request->is('put')) {
			
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile'],
			"email"=>$this->request->data['SalesUser']['email'],
			"user_created_by"=>'Broker'))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{				
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				
				$getbuilderIdByBrokerId = $this->SalesUser->find('all',array(
							'fields'=> array('builder_id'),
							'conditions'=> array(
								'broker_id'=>$this->Session->read('Auth.Websiteuser.id'),
								'user_type'=>'Broker'
								)
									)
										);
				//echo '<pre>'; print_r($getbuilderIdByBrokerId);die();
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $getbuilderIdByBrokerId[0]['SalesUser']['builder_id'];
				$this->request->data['SalesUser']['broker_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['SalesUser']['share_projects_check'] = $this->request->data['SalesUser']['share_check'];
				$this->request->data['SalesUser']['user_type'] = 'Broker_sales';
				$this->request->data['SalesUser']['user_created_by'] = 'Broker';
				//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
				if ($this->SalesUser->save($this->request->data['SalesUser'])) {
					$lastinsertId = $this->SalesUser->getLastInsertId();
					
						$getbrokerIdByBuilderId = $this->SalesUser->find('all',array(
							'fields'=> array('id'),
							'conditions'=> array(
								'broker_id'=>$this->Session->read('Auth.Websiteuser.id')
								)
									)
										);
					
						//$this->request->data['BrokerSales']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['BrokerSales']['broker_id'] = $getbrokerIdByBuilderId[0]['SalesUser']['id'];
						$this->request->data['BrokerSales']['sales_id'] = $lastinsertId;						
						$this->request->data['BrokerSales']['projects'] = json_encode($this->request->data['bspcharges'], true);
						$this->request->data['BrokerSales']['status'] = 1;
						//$this->request->data['BrokerSales']['created_at'] = date("Y-m-d H:i:s");
						
						$this->BrokerSales->save($this->request->data['BrokerSales']);
						
						$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					
					/*for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name'];
						if($this->request->data['bspcharges']['proj_disc'.$i.''] == ''){
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  0;
							
						}else{
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						}
						
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}*/
					/*if($this->request->data['SalesUser']['email']!=''){
						$Email = new CakeEmail();
						$Email->viewVars(array(
								'user_email' => $this->request->data['SalesUser']['email'],
								'user_name' => $this->request->data['SalesUser']['name'],
								'user_mobile' => $this->request->data['SalesUser']['mobile'],
								'user_password' => $this->request->data['SalesUser']['password']));					
						
						$Email->template('android_app_sales','leads');
						$Email->emailFormat('html');
						$Email->subject('Welcome To Fair Pockets Partner App');
						$Email->to($this->request->data['SalesUser']['email']);
						$Email->cc('digpalsingh884@gmail.com');
						$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));						
						
						$output = $Email->send();
							if ($output) {
								// Password set
								$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
								$this->redirect('sales_employees_lists_broker');
							} else {
								// Not able to set password	
								$this->Session->setFlash('Unable to send Email , please check email.', 'default', array('class' => 'red'));
							}
					}else{
						$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					}*/
				}
			}
        }
		
		/*$getbrokerIdByBuilderId = $this->SalesUser->find('all',
										array(
										'fields'=> array('id'),
										'conditions'=> array('broker_id'=>$this->Session->read('Auth.Websiteuser.id'))
										)
										);
										
										//echo '<pre>'; print_r($getbrokerIdByBuilderId);
		
		$buildersDropdown = $this->Websiteuser->find('list', array(
			'fields'=> array('id','userorgname'),
			'joins' => array(array('table' => 'builder_brokers',
                        'alias' => 'BuilderBroker',
                        'type' => 'INNER',
                        'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $getbrokerIdByBuilderId[0]['SalesUser']['id'])))));
		//echo '<pre>'; print_r($buildersDropdown);

		$this->set('buildersDropdown', $buildersDropdown);		
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		*/
		
        $this->layout = "account";
        unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
    }
	
	public function getbuilderprojects() {
        $propertyoption = array();
			$this->loadModel('ProjectsAndroidApp');
			//echo '<pre>'; print_r($this->request['data']);die();
        if (isset($this->request['data']['builder_id'])) {

            $propertyoption = $this->ProjectsAndroidApp->find('list', array(
                'fields' => array(
                    'project_id',
                    'project_name',
                ),
                'conditions' => array(
                    'builder_id' => $this->request['data']['builder_id'],
					'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id'),
                )
            ));
			//echo $this->request['data']['builder_id'].','.$this->Session->read('Auth.Websiteuser.id');
			//echo '<pre>'; print_r($propertyoption);die();
			
			
			}

        header('Content-Type: application/json');
        echo json_encode($propertyoption);
        exit();
    }
	
	public function getbuilderprojectsdiscunit() {
        $propertyoption = array();
			$this->loadModel('ProjectsAndroidApp');
			//echo '<pre>'; print_r($this->request['data']);die();
        if (isset($this->request['data']['project_id'])) {

            $discUnitOption = $this->ProjectsAndroidApp->find('all', array(
                'fields' => array(
                    'proj_discount_unit'
                ),
                'conditions' => array(
					'project_id' => $this->request['data']['project_id'],
                    'builder_id' => $this->request['data']['sel_builder_id'],
					'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id'),
                )
            ));
			//echo $discUnitOption[0]['ProjectsAndroidApp']['proj_discount_unit']; die();
			if($discUnitOption[0]['ProjectsAndroidApp']['proj_discount_unit'] == 1){
				$discUnitOption = array('1' => 'Amount');
			}elseif($discUnitOption[0]['ProjectsAndroidApp']['proj_discount_unit'] == 2){
				$discUnitOption = array('2' => 'Percentage');
			}elseif($discUnitOption[0]['ProjectsAndroidApp']['proj_discount_unit'] == 3){
				$discUnitOption = array('3' => 'Sq Ft');
			}else{
				$discUnitOption = array('1' => 'Amount','2' => 'Percentage','3' => 'Sq Ft');
			}
			
			//echo $this->request['data']['builder_id'].','.$this->Session->read('Auth.Websiteuser.id');
			//echo '<pre>'; print_r($propertyoption);die();
			
			
			}

        header('Content-Type: application/json');
        echo json_encode($discUnitOption);
        exit();
    }
	
	public function share_inventory_to_brokers_sales($inventory_id){
       
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
		$this->set('inventory_id', $inventory_id);
		//die();
		$this->loadModel('SalesUser');
		   $BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'broker_id' => $this->Session->read('Auth.Websiteuser.id'),
						'user_type' => 'Broker_sales',
						'user_created_by' => 'Broker'
					)
				));
			//echo '<pre>'; print_r($BuilderSalesList); die();	
		$this->set('BuilderSalesList', $BuilderSalesList);
		$this->loadModel('BrokerSalesShareInventory');
		$this->loadModel('ShareBrokersalesInventoryHistory');
		
		$inventoryArray = $this->ShareBrokersalesInventoryHistory->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'inventory_id' => $inventory_id
					)
				));
				//echo '<pre>'; print_r($inventoryArray); die();
		
		if ($this->request->is('post')) {
			//$this->BuilderBrokerShareInventory->query("Delete from `builder_broker_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistory']['project_id'] . "");
		//echo '<pre>'; print_r($this->request->data); die();
		
				
				//echo '<pre>'; print_r($inventoryArray); die();
				$salesJson = json_encode($this->request->data['shareinv1'], true);
				//echo $brokersJson; die();
				if(!empty($inventoryArray)){
					//$brokersJson = json_encode($this->request->data['shareprojects1'], true);
					$this->ShareBrokersalesInventoryHistory->updateAll(
						array(
							'assign_sales'=>"'{$salesJson}'"
						),
						array(
							'builder_id'=>$this->Session->read('Auth.Websiteuser.id'),
							'inventory_id'=>$inventory_id,
						));
					
					//
					
				}else{
					$this->request->data['ShareBrokersalesInventoryHistory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
					$this->request->data['ShareBrokersalesInventoryHistory']['inventory_id'] = $inventory_id;
					$this->request->data['ShareBrokersalesInventoryHistory']['assign_sales'] = $salesJson;
					$this->request->data['ShareBrokersalesInventoryHistory']['status'] = 1;
					//}			
					//echo '<pre>'; print_r($this->request->data['ShareInventoryHistory']); die();
					$this->ShareBrokersalesInventoryHistory->create();
					$this->ShareBrokersalesInventoryHistory->save($this->request->data['ShareBrokersalesInventoryHistory']);
					
				}
		
			//echo $builder_id;
				//echo $inventory_id;
				//die();
		
		$this->BrokerSalesShareInventory->query("Delete from `broker_sales_share_inventories` WHERE `builder_id` = " . $builder_id . " and `inventory_id` = " . $inventory_id . "");
			
			for ($i = 0; $i <= count($this->request->data['shareinv1'])/2-1; $i++) {
				
				$this->request->data['BrokerSalesShareInventory']['share_inventory_history_id'] = $inventory_id;
				$this->request->data['BrokerSalesShareInventory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['BrokerSalesShareInventory']['inventory_id'] = $inventory_id;
				$this->request->data['BrokerSalesShareInventory']['sales_id'] = $this->request->data['shareinv1']['sales_id'.$i.''];
				$this->request->data['BrokerSalesShareInventory']['status'] = $this->request->data['shareinv1']['sales_check'.$i.''];
					//}
			//echo '<pre>'; print_r($this->request->data['BrokerSalesShareInventory']);die();
			$this->BrokerSalesShareInventory->create();
			$this->BrokerSalesShareInventory->save($this->request->data['BrokerSalesShareInventory']);	

			
				
			}
					
		}
		$this->request->data['shareinv1'] = json_decode($inventoryArray[0]['ShareBrokersalesInventoryHistory']['assign_sales'], true);
	}
	
	
	public function edit_sales_employees_broker_panel($sales_id){
		
		$this->layout = "account";
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		
		$this->ProjectsAndroidApp->deleteAll(array
								('ProjectsAndroidApp.sales_or_broker_employee_id'=>$sales_id),
								false);
		
		
		 $this->SalesUser->id = $sales_id;
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);
			//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
            if ($this->SalesUser->save($this->request->data)) {
				//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
				for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
						//$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
						
						
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $sales_id;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['builder_id'] = NULL;
						$this->request->data['ProjectsAndroidApp']['websiteuser_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
				
                $this->Session->setFlash('Updated Successfully.', 'default', array('class' => 'green'));
            }
        }
		
		$getbrokerIdByBuilderId = $this->SalesUser->find('all',
							array(
							'fields'=> array('id'),
							'conditions'=> array('broker_id'=>$this->Session->read('Auth.Websiteuser.id'))
							)
							);
		
		$buildersDropdown = $this->Websiteuser->find('list', array(
			'fields'=> array('id','userorgname'),
			'joins' => array(array('table' => 'builder_brokers',
                        'alias' => 'BuilderBroker',
                        'type' => 'INNER',
                        'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $getbrokerIdByBuilderId[0]['SalesUser']['id'])))));
		//echo '<pre>'; print_r($buildersDropdown);

		$this->set('buildersDropdown', $buildersDropdown);
		
		$this->request->data = $this->SalesUser->findById($sales_id);
		$allsalesUsers = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'id' => $sales_id
                )
            ));
		//echo '<pre>'; print_r($allsalesUsers);die();
		$this->request->data['bspcharges'] = json_decode($allsalesUsers['0']['SalesUser']['projects'], 'true');
		
		$this->loadModel('Project');
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		 $this->set('getProjects', $getProjects);
		
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            //$this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	public function check_broker_exist() {
		//$this->layout = false;
			//$this->autoRender = false;
		//die();
		$this->loadModel('SalesUser');
        header('Content-Type: application/json');
        if ($this->request->is('ajax')) {
			//print_r( $this->request->query['data']);die();
          // echo $this->request->query['data']['SalesUser']['email']; die();
			$data = $this->SalesUser->find('all', array(
                'conditions' => array(
                    'email' => $this->request->query['data']['SalesUser']['email'],
					'builder_id !=' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
            //print_r($data); die();
            if($data) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        } else {
            echo json_encode(false);
        }
		
		//$emlsid = 884;
		//$this->set('emlsid', $emlsid);
        
        die;
    }
	
	
	
	
	public function share_projects_form() {
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			);
			//echo '<pre>'; print_r($getBuilderIdArray);die(); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
			$getBrokerId =	$this->Session->read('Auth.Websiteuser.id');
			$userCreatedBy = 'Broker';			
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');			
			$getBrokerId =	0;
			$userCreatedBy = 'Builder';
		}
		
        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);//die();
			
            				
				//$projecturl = json_encode($this->request->data['bspcharges'], true);
					//$this->redirect('share_projects_form_builder_to_sales/'.$projecturl);
				
			
        }
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $getBuilderId
				)
			)
		);
		
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
        unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
    }
	
	public function share_projects_form_builder_to_sales($json) {
		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');		
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			);
			//echo '<pre>'; print_r($getBuilderIdArray);die(); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
			$getBrokerId =	$this->Session->read('Auth.Websiteuser.id');
			$userCreatedBy = 'Broker';			
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');			
			$getBrokerId =	0;
			$userCreatedBy = 'Builder';
		}	
			//echo '<pre>'; print_r($this->request->data);die();
		   $this->loadModel('SalesUser');
		   if($this->request->data['submitaction'] == 'sales'){
			$BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'builder_id' => $getBuilderId,
						'user_type' => 'sales',
						'projects' => NULL
					)
				));   
		   }
		   
		   if($this->request->data['submitaction'] == 'broker'){
			$BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'builder_id' => $getBuilderId,
						'user_type' => 'Broker',
						'projects' => 'null'
					)
				));   
		   }  
		   
			//echo '<pre>'; print_r($BuilderSalesList); die();	
			$this->set('BuilderSalesList', $BuilderSalesList);
			
			if(!isset($this->request->data['bspcharges']) && $this->request->data['bspcharges'] == ''){
				$this->redirect('share_projects_form');
			}
			$projectjson = json_encode($this->request->data['bspcharges'], true);
			//echo $projecturl;
			$this->set('projectjson', $projectjson);
			
			$json ='{"from_project1":"701","proj_disc1":"8","to_floor1":"1","from_project2":"701","proj_disc2":"12","to_floor2":"1"}';
			//echo $json;
			//$projectarr = json_decode($json);
			//echo '<pre>';print_r($projectarr);
       
		
        $this->layout = "account";
        //unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	public function share_projects_form_builder_to_sales_assign($json) {		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');			
        if ($this->request->is('post') || $this->request->is('put')) {
				
			for( $i=0; $i < count($this->request->data['check_list']); $i++ ){				
				$json = $this->request->data['projectjson'];				
				$projectarr = json_decode($json,true);				
				$this->SalesUser->updateAll(array("SalesUser.projects" => "'{$json}'"),
				array("SalesUser.id" => $this->request->data['check_list'][$i]));
				$allsalesUsers = $this->SalesUser->find('all', array(
						'conditions' => array(
							'id' => $this->request->data['check_list'][$i]
						)
					));					
				if($allsalesUsers[0]['SalesUser']['user_type'] == 'Sales'){					
					for( $j=1; $j <= count($projectarr)/3; $j++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $this->request->data['check_list'][$i];
						$this->request->data['ProjectsAndroidApp']['project_id'] = $projectarr['from_project'.$j.''];						
						$view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($projectarr['from_project'.$j.'']);						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name']; 
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $projectarr['proj_disc'.$j.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $projectarr['to_floor'.$j.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Sales';
						$this->ProjectsAndroidApp->create(false);						
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					$Email = new CakeEmail();
					$Email->viewVars(array(
							'user_email' => $allsalesUsers[0]['SalesUser']['email'],
							'user_name' => $allsalesUsers[0]['SalesUser']['name'],
							'user_mobile' => $allsalesUsers[0]['SalesUser']['mobile'],
							'user_password' => $allsalesUsers[0]['SalesUser']['password']));						
					$Email->template('android_app_sales','leads');
					$Email->emailFormat('html');
					$Email->subject('Welcome To Fair Pockets Partner App');
					$Email->to($allsalesUsers[0]['SalesUser']['email']);
					$Email->cc('digpalsingh884@gmail.com');
					$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));
					$Email->send();
					
				}
				
				if($allsalesUsers[0]['SalesUser']['user_type'] == 'Broker'){					
					for( $j=1; $j <= count($projectarr)/3; $j++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $this->request->data['check_list'][$i];
						$this->request->data['ProjectsAndroidApp']['project_id'] = $projectarr['from_project'.$j.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($projectarr['from_project'.$j.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name'];  
						$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $projectarr['proj_disc'.$i.''];
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $projectarr['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'builder';
						$this->request->data['ProjectsAndroidApp']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['ProjectsAndroidApp']['websiteuser_id'] = $allsalesUsers[0]['SalesUser']['broker_id'];
						$this->ProjectsAndroidApp->create(false);
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					$Email = new CakeEmail();
					$Email->viewVars(array(
							'user_email' => $allsalesUsers[0]['SalesUser']['email'],
							'builder_org_name' => $this->Session->read('Auth.Websiteuser.userorgname'),
							'user_name' => $allsalesUsers[0]['SalesUser']['name'],
							'user_mobile' => $allsalesUsers[0]['SalesUser']['mobile'],
							'user_password' => $allsalesUsers[0]['SalesUser']['password']));						
					$Email->template('android_app_brokers','leads');
					$Email->emailFormat('html');
					$Email->subject('Welcome To Fair Pockets Partner App');
					$Email->to($allsalesUsers[0]['SalesUser']['email']);
					$Email->cc('digpalsingh884@gmail.com');
					$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));
					$Email->send();
					
				}
			}
			
				$this->Session->setFlash('Project assigned successfull.', 'default', array('class' => 'green'));
				$this->redirect('sales_employees_lists');	
			
        }
		
        $this->layout = "account";
        unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	
	public function add_sales_employees1() {
		
		$this->loadModel('SalesUser');
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');			
			$getBrokerId =	0;
			$userCreatedBy = 'Builder';	
		}
		
		
        if ($this->request->is('post') || $this->request->is('put')) {
			
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile']))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{				
				//$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $getBuilderId;
				$this->request->data['SalesUser']['broker_id'] = $getBrokerId;
				$this->request->data['SalesUser']['user_type'] = 'Sales';
				$this->request->data['SalesUser']['user_created_by'] = $userCreatedBy;
				//echo '<pre>'; print_r($this->request->data);die();
				if ($this->SalesUser->save($this->request->data['SalesUser'])) {
						$lastinsertId = $this->SalesUser->getLastInsertId();						
						$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
						//$this->redirect('sales_employees_lists');
						$this->redirect(array('controller' => 'accounts', 'action' => 'sales_employees_lists'));
				}
			}
        }
		
        $this->layout = "account";
        unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
    }
	
	
	public function add_broker_employees1() {
		
		$this->loadModel('SalesUser');	
		$this->loadModel('BuilderBroker');
		
        if ($this->request->is('post') || $this->request->is('put')) {
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile'],
			"email"=>$this->request->data['SalesUser']['email']))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				//$this->request->data['SalesUser']['broker_id'] = 0;
				$this->request->data['SalesUser']['user_type'] = 'Broker';
				$this->request->data['SalesUser']['user_created_by'] = 'Builder';
				
				$this->request->data['Websiteuser']['userrole'] = 2;
				$this->request->data['Websiteuser']['username'] = $this->request->data['SalesUser']['name'];
				$this->request->data['Websiteuser']['userorgname'] = $this->request->data['SalesUser']['company'];
				$this->request->data['Websiteuser']['email'] = $this->request->data['SalesUser']['email'];
				$this->request->data['Websiteuser']['usermobile'] = $this->request->data['SalesUser']['mobile'];
				$this->request->data['Websiteuser']['password'] = $password;
				$this->request->data['Websiteuser']['regsitration_token'] = md5(date('Y-m-d H:i:s') . $this->request->data['SalesUser']['email'] . $this->request->data['SalesUser']['mobile']);
				//echo '<pre>'; print_r($this->request->data['Websiteuser']);die();
				if($this->Websiteuser->save($this->request->data['Websiteuser'])){
					$lastinsertIdwu = $this->Websiteuser->getLastInsertId();
					$this->request->data['SalesUser']['broker_id'] = $lastinsertIdwu;
					if ($this->SalesUser->save($this->request->data['SalesUser'])) {
						$lastinsertId = $this->SalesUser->getLastInsertId();
						
						$this->request->data['BuilderBroker']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['BuilderBroker']['broker_id'] = $lastinsertId;
						$this->request->data['BuilderBroker']['projects'] = json_encode($this->request->data['bspcharges'], true);
						$this->request->data['BuilderBroker']['created_at'] = date("Y-m-d H:i:s");
						
						$this->BuilderBroker->save($this->request->data['BuilderBroker']);
						
					}
					$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					
				}else{
					$this->Session->setFlash('Failed, Email/Mobile no already exist in our database.', 'default', array('class' => 'red'));
				}
			}
        }
		
				
        $this->layout = "account";
        unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
		
    }
	
	
	public function share_projects_form_broker_panel() {
		
		echo $this->Session->read('Auth.Websiteuser.id');
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('BrokerSales');
        if ($this->request->is('post') || $this->request->is('put')) {
			
            if($this->SalesUser->hasAny(array("mobile"=>$this->request->data['SalesUser']['mobile'],
			"email"=>$this->request->data['SalesUser']['email'],
			"user_created_by"=>'Broker'))){
					$this->Session->setFlash('User Already Exist, Please try Again', 'default', array('class' => 'green'));				
			}else{				
				$this->request->data['SalesUser']['projects'] = json_encode($this->request->data['bspcharges'], true);			
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$password = '';
				for ($i = 0; $i < 6; $i++) {
					$password .= $characters[rand(0, strlen($characters) - 1)];
				}
				
				$getbuilderIdByBrokerId = $this->SalesUser->find('all',array(
							'fields'=> array('builder_id'),
							'conditions'=> array(
								'broker_id'=>$this->Session->read('Auth.Websiteuser.id'),
								'user_type'=>'Broker'
								)
									)
										);
				//echo '<pre>'; print_r($getbuilderIdByBrokerId);die();
				$this->request->data['SalesUser']['password'] = $password;
				$this->request->data['SalesUser']['builder_id'] = $getbuilderIdByBrokerId[0]['SalesUser']['builder_id'];
				$this->request->data['SalesUser']['broker_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['SalesUser']['share_projects_check'] = $this->request->data['SalesUser']['share_check'];
				$this->request->data['SalesUser']['user_type'] = 'Broker_sales';
				$this->request->data['SalesUser']['user_created_by'] = 'Broker';
				//echo '<pre>'; print_r($this->request->data['SalesUser']);die();
				if ($this->SalesUser->save($this->request->data['SalesUser'])) {
					$lastinsertId = $this->SalesUser->getLastInsertId();
					
						$getbrokerIdByBuilderId = $this->SalesUser->find('all',array(
							'fields'=> array('id'),
							'conditions'=> array(
								'broker_id'=>$this->Session->read('Auth.Websiteuser.id')
								)
									)
										);
					
						//$this->request->data['BrokerSales']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['BrokerSales']['broker_id'] = $getbrokerIdByBuilderId[0]['SalesUser']['id'];
						$this->request->data['BrokerSales']['sales_id'] = $lastinsertId;						
						$this->request->data['BrokerSales']['projects'] = json_encode($this->request->data['bspcharges'], true);
						$this->request->data['BrokerSales']['status'] = 1;
						//$this->request->data['BrokerSales']['created_at'] = date("Y-m-d H:i:s");
						
						$this->BrokerSales->save($this->request->data['BrokerSales']);
					
					
					for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $lastinsertId;
						$this->request->data['ProjectsAndroidApp']['project_id'] = $this->request->data['bspcharges']['from_project'.$i.''];
						 $view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name'];
						if($this->request->data['bspcharges']['proj_disc'.$i.''] == ''){
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  0;
							
						}else{
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $this->request->data['bspcharges']['proj_disc'.$i.''];
						}
						
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $this->request->data['bspcharges']['to_floor'.$i.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->ProjectsAndroidApp->create(false);
						//echo '<pre>'; print_r($this->request->data['ProjectsAndroidApp']);die();
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					if($this->request->data['SalesUser']['email']!=''){
						$Email = new CakeEmail();
						$Email->viewVars(array(
								'user_email' => $this->request->data['SalesUser']['email'],
								'user_name' => $this->request->data['SalesUser']['name'],
								'user_mobile' => $this->request->data['SalesUser']['mobile'],
								'user_password' => $this->request->data['SalesUser']['password']));					
						
						$Email->template('android_app_sales','leads');
						$Email->emailFormat('html');
						$Email->subject('Welcome To Fair Pockets Partner App');
						$Email->to($this->request->data['SalesUser']['email']);
						$Email->cc('digpalsingh884@gmail.com');
						$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));						
						
						$output = $Email->send();
							if ($output) {
								// Password set
								$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
								$this->redirect('sales_employees_lists_broker');
							} else {
								// Not able to set password	
								$this->Session->setFlash('Unable to send Email , please check email.', 'default', array('class' => 'red'));
							}
					}else{
						$this->Session->setFlash('User Created Successfully.', 'default', array('class' => 'green'));
					}
				}
			}
        }
		
		$getbrokerIdByBuilderId = $this->SalesUser->find('all',
										array(
										'fields'=> array('id'),
										'conditions'=> array('broker_id'=>$this->Session->read('Auth.Websiteuser.id'))
										)
										);
										
										//echo '<pre>'; print_r($getbrokerIdByBuilderId);
		
		$buildersDropdown = $this->Websiteuser->find('list', array(
			'fields'=> array('id','userorgname'),
			'joins' => array(array('table' => 'builder_brokers',
                        'alias' => 'BuilderBroker',
                        'type' => 'INNER',
                        'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $getbrokerIdByBuilderId[0]['SalesUser']['id'])))));
		//echo '<pre>'; print_r($buildersDropdown);

		$this->set('buildersDropdown', $buildersDropdown);		
		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);
		$this->set('getProjects', $getProjects);
		
        $this->layout = "account";
        unset($this->request->data);
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }			
		
    }
	
	
	public function share_projects_form_broker_to_sales($json) {
		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');		
		//echo $this->Session->read('Auth.Websiteuser.userrole');
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			);
			//echo '<pre>'; print_r($getBuilderIdArray);die(); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
			$getBrokerId =	$this->Session->read('Auth.Websiteuser.id');
			$userCreatedBy = 'Broker';			
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');			
			$getBrokerId =	0;
			$userCreatedBy = 'Builder';
		}	
			//echo '<pre>'; print_r($this->request->data);
		   $this->loadModel('SalesUser');
		   //if($this->request->data['submitaction'] == 'sales'){		
					
			$BuilderSalesList = $this->SalesUser->find('all', array(
            'conditions' => array(
                'broker_id' => $this->Session->read('Auth.Websiteuser.id'),
				'projects' => 'null',
				'user_type' => 'Broker_sales',
				'user_created_by' => 'Broker'
				)
			));
			
		   //} 
		   
			//echo '<pre>'; print_r($BuilderSalesList); die();	
			$this->set('BuilderSalesList', $BuilderSalesList);
			
			/*if(!isset($this->request->data['bspcharges']) && $this->request->data['bspcharges'] == ''){
				$this->redirect('share_projects_form');
			}*/
			$projectjson = json_encode($this->request->data['bspcharges'], true);
			//echo $projecturl;
			$this->set('projectjson', $projectjson);
			
			$json ='{"from_project1":"701","proj_disc1":"8","to_floor1":"1","from_project2":"701","proj_disc2":"12","to_floor2":"1"}';
			//echo $json;
			//$projectarr = json_decode($json);
			//echo '<pre>';print_r($projectarr);
       
		
        $this->layout = "account";
        //unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	
	public function share_projects_form_broker_to_sales_assign($json) {		
		$this->loadModel('SalesUser');
		$this->loadModel('Project');
		$this->loadModel('ProjectsAndroidApp');			
        if ($this->request->is('post') || $this->request->is('put')) {	
         //echo '<pre>'; print_r($this->request->data);die();		
			for( $i=0; $i < count($this->request->data['check_list']); $i++ ){				
				$json = $this->request->data['projectjson'];				
				$projectarr = json_decode($json,true);				
				$this->SalesUser->updateAll(array("SalesUser.projects" => "'{$json}'"),
				array("SalesUser.id" => $this->request->data['check_list'][$i]));
					
				$allsalesUsers = $this->SalesUser->find('all', array(
						'conditions' => array(
							'id' => $this->request->data['check_list'][$i]
						)
					));				
				//if($allsalesUsers[0]['SalesUser']['user_type'] == 'Broker_sales'){					
					for( $j=1; $j <= count($projectarr)/3; $j++ ){
						$this->request->data['ProjectsAndroidApp']['sales_or_broker_employee_id'] = $this->request->data['check_list'][$i];
						$this->request->data['ProjectsAndroidApp']['project_id'] = $projectarr['from_project'.$j.''];						
						$view = new View($this);
						$Custom = $view->loadHelper('Number');
						$projectName = $Custom->getProjectNameByProjectId($projectarr['from_project'.$j.'']);						
						$this->request->data['ProjectsAndroidApp']['project_name'] = $projectName[0]['Project']['project_name'];
						if($projectarr['proj_disc'.$j.''] == ''){
							$this->request->data['ProjectsAndroidApp']['proj_discount'] = 0;
						}else{
							$this->request->data['ProjectsAndroidApp']['proj_discount'] =  $projectarr['proj_disc'.$j.''];
						}
						
						
						$this->request->data['ProjectsAndroidApp']['proj_discount_unit'] =  $projectarr['to_floor'.$j.''];
						$this->request->data['ProjectsAndroidApp']['status'] = 1;
						$this->request->data['ProjectsAndroidApp']['post_created_by'] = 'Broker';
						$this->ProjectsAndroidApp->create(false);						
						$this->ProjectsAndroidApp->save($this->request->data['ProjectsAndroidApp']);
					}
					
					
				//}	
				//echo '<pre>'; print_r($allsalesUsers);
				$Email = new CakeEmail();
					$Email->viewVars(array(
							'user_email' => $allsalesUsers[0]['SalesUser']['email'],
							'user_name' => $allsalesUsers[0]['SalesUser']['name'],
							'user_mobile' => $allsalesUsers[0]['SalesUser']['mobile'],
							'user_password' => $allsalesUsers[0]['SalesUser']['password']));						
					$Email->template('android_app_sales','leads');
					$Email->emailFormat('html');
					$Email->subject('Welcome To Fair Pockets Partner App');
					$Email->to($allsalesUsers[0]['SalesUser']['email']);
					$Email->cc('digpalsingh884@gmail.com');
					$Email->from(array('Support@FairPockets.com' => 'FairPockets Users'));
					$Email->send();
				
			}
			//die();
				$this->Session->setFlash('Project assigned successfull.', 'default', array('class' => 'green'));
				$this->redirect('sales_employees_lists_broker');	
			
        }
		
        $this->layout = "account";
        unset($this->request->data);
		
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $getBuilderId
            )
        ));
        if (!empty($builder)) {
            $this->request->data = $builder[0];            
            $this->set('status', $builder['0']['Builder']['status']);
        } else {
            $builder_id = 0;
            $this->set('buider_id', $builder_id);
        }
		
	}
	
	//--new admin activation module start 8-8-2018----- 	
	public function admin_app_activation() {	
		$this->layout = "admin";
		$this->loadModel('Websiteuser');		
		$usersData1 = $this->Websiteuser->find('all', array(           
			'conditions' => array(
				'userrole' => '3'
			)
		));
		$usersData= $this->Websiteuser->find('all', array(
		'fields'=> array('Websiteuser.*','Freeactivation.date_created',
		'Freeactivation.servicetype', 'Freeactivation.expiry_date'),
		'joins' => array(array('table' => 'freeactivations',
			'alias' => 'Freeactivation',
			'type' => 'INNER',
			'conditions' => array('Freeactivation.builder_id = Websiteuser.id',
			'Websiteuser.userrole' => 3))
				))
			);	
		$this->set('builderData', $usersData);
		$this->set('builderData1', $usersData1);		
		$this->loadModel('freeactivation');
		$freeactivation = $this->freeactivation->find('all');
		$this->set('Freeactivation', $freeactivation);		
    } 		
	//--new admin activation module end 8-8-2018-----
	
	public function admin_activation_list() {
		$this->loadModel('freeactivation');
        $PropService = $this->freeactivation->find('all');
		$this->set('PropService', $PropService);
	}
		
	public function admin_builderApproveAction($id){
        if (!empty($id)) { 
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->loadModel('freeactivation');
				$this->freeactivation->updateAll(
					array(
						'status'=>$this->request->data['freeactivation']['status']
						 ),
					array(
						'id'=>$id
					));
				$this->Session->setFlash('Builder approved Succesfully!!', 'default', array('class' => 'green'));
				return $this->redirect(array('action' => 'activation_list'));
			}
        } 
    }
		//--------------------------ADD/UPDATE Paid activation start -------------------------
		public function admin_paid_activation($id) {        
			$this->loadModel('freeactivation');
			$PortService = $this->freeactivation->findById($id);
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->loadModel('freeactivation');
				$inputdata=$this->request->data['freeactivation']['duration_of_free_trial'];
				$creation_date = date("d-m-Y");
				$creation_date1 = "+".$inputdata." day";
				$creation_date_conver_sttotime = strtotime($creation_date);
				$date_after_n_days = strtotime($creation_date1, $creation_date_conver_sttotime);
				$date_after_n_days;
				$date_after_n_days1= date('d-m-Y',$date_after_n_days);
				$this->request->data['freeactivation']['expiry_date'] = $date_after_n_days1;
				if ($this->freeactivation->save($this->request->data)){
					$this->Session->setFlash('The Portfolio Service has been update Successfully!!!', 'default', array('class' => 'green'));
				}
			}
			$freeactivation = $this->freeactivation->findByBuilderId($id);
			$this->set('builder_id',$this->params['pass'][0]);
			$this->request->data = $freeactivation;
			$this->set('freeactivation', $freeactivation);
		}
	//--------------------ADD/UPDATE paid activation End--------------------
		
	//--------------------------ADD/UPDATE activation Start -------------------------
		public function admin_free_activation($id) {        
			$this->loadModel('freeactivation');
			$PortService = $this->freeactivation->findById($id);
			if($this->request->is('post') || $this->request->is('put')) {
				$this->loadModel('freeactivation');
				$inputdata=$this->request->data['freeactivation']['duration_of_free_trial'];
				$creation_date = date("d-m-Y");
				$creation_date1 = "+".$inputdata." day";
				$creation_date_conver_sttotime = strtotime($creation_date);
				$date_after_n_days = strtotime($creation_date1, $creation_date_conver_sttotime);
				$date_after_n_days;
				$date_after_n_days1= date('d-m-Y',$date_after_n_days);
				$this->request->data['freeactivation']['expiry_date'] = $date_after_n_days1;
				if ($this->freeactivation->save($this->request->data)) {
					$this->Session->setFlash('The Portfolio Service has been update Successfully!!!', 'default', array('class' => 'green'));
				}
			
			}
			$freeactivation = $this->freeactivation->findByBuilderId($id);
			$this->set('builder_id',$this->params['pass'][0]);
			$this->request->data = $freeactivation;
			$this->set('freeactivation', $freeactivation);
		}
	//--------------------ADD/UPDATE free activation End--------------------
	
	//--------------------------Edit activation Start -------------------------
		public function admin_edit_activation($id) {        
			$this->loadModel('freeactivation');
			$PortService = $this->freeactivation->findById($id);
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->freeactivation->save($this->request->data)) {
					$this->Session->setFlash('The Portfolio Service has been update Successfully!!!', 'default', array('class' => 'green'));
				}
			}
			$freeactivation = $this->freeactivation->findById($id);
			$this->request->data = $freeactivation;
			$this->set('freeactivation', $freeactivation);
    	}
	//--------------------Edit activation End--------------------
		


	public function inventory1() {
		$this->loadModel('SalesUser');
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
		}
		
        /*if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
				'fields' => array("InvInventory.*","InvProject.project_name"),
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['search'] . '%',
                        'SalesUser.email LIKE' => $this->request->data['search'] . '%',
                    ),
					"InvWebsiteuser.id = '{$getBuilderId}'"
				),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'order' => 'created_at DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'fields' => array("InvInventory.*","InvProject.project_name"),
				'conditions' => array("InvWebsiteuser.id = '{$getBuilderId}'"),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'order' => 'created_at DESC',
                'limit' => 8
            );
		}*/
		
						
				
			$this->InvInventory->bindModel(array(
				'belongsTo' => array(
					'Project' => array(
					  'foreignKey' => false,
					  'type' => 'INNER',
					  'fields' => array('project_name'),
					  'conditions' => array('Project.project_id = InvInventory.project_id')
					),					
					'Websiteuser' => array(
					  'foreignKey' => false,
					  'type' => 'INNER',
					  'fields' => array('id'),
					  'conditions' => array('Websiteuser.id = Project.user_id')
					)
				  )
						), false);
				
				
		
		$keyword = "";
        $scriteria = "";
        if (isset($this->request->params["named"]['keyword'])) {
            $this->request->data['InvInventory']['keyword'] = trim($this->request->params["named"]['keyword']);
        }
        if (isset($this->request->params["named"]['scriteria'])) {
            $this->request->data['InvInventory']['scriteria'] = trim($this->request->params["named"]['scriteria']);
        }
		$conditions = array();	
		if (empty($this->request->data)) {
            $conditions = array('Websiteuser.id' => $getBuilderId);
        }
        else {	
	//echo 'one';
			 //$conditions = array('Websiteuser.id' => $getBuilderId);
			 $conditions = array("Websiteuser.id" => $getBuilderId);
            if (isset($this->request->data['InvInventory']['scriteria']) && $this->request->data['InvInventory']['keyword'] != '') {
				
			    $keyword = trim($this->request->data['InvInventory']['keyword']);
                $scriteria = trim($this->request->data['InvInventory']['scriteria']);
                switch ($scriteria) {					
                    case 'project_name':
                        $conditions["Project.project_name like "] = "%" . $keyword . "%";
                        break;
					case 'tower_name':
                        $conditions["InvInventory.tower_name like "] = "%" . $keyword . "%";
                        break;
                }
				print_r($conditions);
            }
            /* if(isset($this->request->data['Employee']['date_from']) and $this->request->data['Employee']['date_from']!='')
              {
              $otherfilterConditions .= "date_from:" . $this->request->data['Employee']['date_from'];
              $conditions['Date(Employee.created) >='] = $this->request->data['Employee']['date_from'];
              }

              if(isset($this->request->data['Employee']['date_to']) and $this->request->data['Employee']['date_to']!='')
              {
              $otherfilterConditions .= "date_to:" . $this->request->data['Employee']['date_to'];
              $conditions['Date(Employee.created) <='] = $this->request->data['Employee']['date_to'];
              } */
        }
		//print_r($conditions);
		$this->paginate = array('limit' => 10, 'order' => array('InvInventory.created_at' => 'desc'));
		$this->set('keyword', $keyword);
		$this->set('scriteria', $scriteria);
		$inventories	=	$this->paginate('InvInventory',$conditions);
		//echo '<pre>'; print_r($inventories); die();
		//$inventories = $this->Paginator->paginate('InvInventory');
		$this->set('inventories', $inventories);
		
        
        
		
		if ($this->RequestHandler->isAjax()) {
            $this->layout = "ajax";
            $this->viewPath = 'Elements' . DS . 'inventory';
            $this->render('invList');
        }
		$this->layout = 'account';
		
    }
	
	//--------------Inventory Summery Report Start on 24-8-2018------
	
	
	public function inventorySummeryReport() {
		$this->loadModel('SalesUser');
		$this->loadModel('AppClientLead');
		
		$userid_count = $this->AppClientLead->find('count',array(
		'conditions' => array('user_id'=>2)
		));
		
		//echo '<pre>'; print_r($userid_count);die();
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
			$getBuilderIdArray = $this->SalesUser->find('first',
				array('fields'=> array(
						'builder_id'			
					),
					  'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
					)
				)
			); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
		}
		
		if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
		}
		//echo $getBuilderId;die();
		//$getBuilderId = 45;
		//print_r($this->request->data);
        if (!empty($this->request->data)) {
				
				$this->loadModel('Project');
       			$cond=array('OR'=>array('Project.project_name LIKE' => $this->request->data['name'] .'%',
									'Project.project_name LIKE' => $this->request->data['project_name'] .'%',
									'Project.city_data LIKE' => $this->request->data['tower'] .'%'/*,
									'SalesUser.created LIKE' => $this->request->data['project'] .'%',
									'SalesUser.created LIKE' => $this->request->data['tower'] .'%'*/
							)  );
				$list = $this->Project->find('all',array('conditions'=>$cond));
				echo "<pre>";print_r($cond);
				echo "<pre>";print_r($list);
			/*$this->Paginator->settings = array(
				'fields' => array("InvInventory.*","InvProject.project_name"),
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['name'] . '%',
                        'SalesUser.email LIKE' => $this->request->data[''] . '%',
                    ),
					"InvWebsiteuser.id = '{$getBuilderId}'"
				),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'order' => 'created_at DESC',
                'limit' => 8
            ); */
	    }else{
			
			$this->Paginator->settings = array(
                'fields' => array("InvInventory.*","InvProject.project_name"),
				'conditions' => array("InvWebsiteuser.id = '{$getBuilderId}'"),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'order' => 'created_at DESC',
                'limit' => 8
            );
		}
		
						
				
			/*$this->InvInventory->bindModel(array(
				'belongsTo' => array(
					'Project' => array(
					  'foreignKey' => false,
					  'type' => 'INNER',
					  'fields' => array('project_name'),
					  'conditions' => array('Project.project_id = InvInventory.project_id')
					),					
					'Websiteuser' => array(
					  'foreignKey' => false,
					  'type' => 'INNER',
					  'fields' => array('id'),
					  'conditions' => array('Websiteuser.id = Project.user_id')
					)
				  )
						), false);*/
				
		
			
		
		
		
		$keyword = "";
        $scriteria = "";
        if (isset($this->request->params["named"]['keyword'])) {
            $this->request->data['SalesUser']['keyword'] = trim($this->request->params["named"]['keyword']);
        }
        if (isset($this->request->params["named"]['scriteria'])) {
            $this->request->data['SalesUser']['scriteria'] = trim($this->request->params["named"]['scriteria']);
        }
		$conditions = array();	
		if (empty($this->request->data)) {
            $conditions = array('SalesUser.builder_id' => $getBuilderId, 'SalesUser.user_type'=> 'Sales');
        }
        else {	
	//echo 'one';
			 //$conditions = array('Websiteuser.id' => $getBuilderId);
			 $conditions = array('SalesUser.builder_id' => $getBuilderId, 'SalesUser.user_type'=> 'Sales');
            if (isset($this->request->data['SalesUser']['scriteria']) && $this->request->data['SalesUser']['keyword'] != '') {
				
			    $keyword = trim($this->request->data['SalesUser']['keyword']);
                $scriteria = trim($this->request->data['SalesUser']['scriteria']);
                switch ($scriteria) {					
                    case 'project_name':
                        $conditions["SalesUser.project_name like "] = "%" . $keyword . "%";
                        break;
					case 'tower_name':
                        $conditions["SalesUser.tower_name like "] = "%" . $keyword . "%";
                        break;
                }
				print_r($conditions);
            }
            /* if(isset($this->request->data['Employee']['date_from']) and $this->request->data['Employee']['date_from']!='')
              {
              $otherfilterConditions .= "date_from:" . $this->request->data['Employee']['date_from'];
              $conditions['Date(Employee.created) >='] = $this->request->data['Employee']['date_from'];
              }

              if(isset($this->request->data['Employee']['date_to']) and $this->request->data['Employee']['date_to']!='')
              {
              $otherfilterConditions .= "date_to:" . $this->request->data['Employee']['date_to'];
              $conditions['Date(Employee.created) <='] = $this->request->data['Employee']['date_to'];
              } */
        }
		//print_r($conditions);
		
		//echo $getBuilderId;
		$allproject=$this->Project->find('list',
					array(
						'fields'=>array('project_id','project_name'),
						'conditions'=>array('user_id'=>$getBuilderId)
						));
												
				//echo "<pre>";print_r($allproject);die();
				$this->set('allproject', $allproject);
		
		
		$this->paginate = array('limit' => 10, 'order' => array('SalesUser.created' => 'desc'));
		$this->set('keyword', $keyword);
		$this->set('scriteria', $scriteria);
		$salesuserArray	=	$this->paginate('SalesUser',$conditions);
		//echo '<pre>'; print_r($salesuserArray); die();
		//$inventories = $this->Paginator->paginate('InvInventory');
		//$this->set('inventories', $inventories);
		$this->set('salesuserArray', $salesuserArray);
		$this->set('userid_count', $userid_count);
		$this->set('list', $list);
		
		
		
        
        
		
		if ($this->RequestHandler->isAjax()) {
            $this->layout = "ajax";
            $this->viewPath = 'Elements' . DS . 'inventory';
            $this->render('invList');
        }
		$this->layout = 'account';
		
		//$this->loadModel('InvProjects');
		$invProjectsArray = $this->InvInventory->find('all', array(
            'fields' => array("DISTINCT(InvProject.project_id) AS project_id"),
            'conditions' => array("InvWebsiteuser.id = '{$getBuilderId}'"),
            'joins' => array(
                array(
                    'table' => 'projects',
                    'alias' => 'InvProject',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvProject.project_id = InvInventory.project_id")
                ),
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
                )
            )
                    
        ));
		//echo '<pre>'; print_r($invProjectsArray); die();
		$invArr = Set::extract('/InvProject/project_id/.', $invProjectsArray);
		//echo '<pre>'; print_r($invArr); die();
		
		$allproject=$this->Project->find('list',
									array(
										'fields'=>array('project_id','project_name'),
										'conditions'=>array('project_id'=>$invArr)
										));
										
										
		
										
		//echo "<pre>";print_r($allproject);die();
		$this->set('allproject', $allproject);
		
		
		
		/*$data_arr = $this->InvProperty->find('all', array(
            'fields' => array(''),
            'conditions' => array('InvProperty.user_id' => 45,
			'InvProperty.project_id' => 701)
        ));
		
		if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                $area_arr[] = $row['PropertyFeature'][0]['features_Value'];
            }
        }
		echo '<pre>'; print_r($area_arr);die();*/
		
		//echo "<pre>";print_r($data_arr);die();
		
		
		
    }
	
	//----------------------Inventory Summary Report End on 24-8-2018----------------------
	
	
	//-------Tower Blog Start--------------			
		public function towerblock($projectid){
			$this->loadModel('Inventorie');
			$alltower = $this->Inventorie->find('all', array(
							 'fields' => array(
							'id','tower_name'								
							),
							'conditions' => array(
								//'current_project' => 0,
								'project_id' =>$projectid,
								'tower_name !=' => ''
							)
				));				
			$this->view = 'towerblock';
			$this->layout = 'ajax';				
			$this->set('alltower', $alltower);
		}			
	//-------Tower Blog End--------------
			
			
	//-----Wing Blog Start------			
	public function wingblock($projectid){
		$towerarray = $this->request->data['check_list'];
		$toweridStrings = implode(',',$towerarray);
		
		$this->set('toweridStrings', $toweridStrings);
		
		$this->loadModel('Inventorie');
		$allwing = $this->Inventorie->find('all', array(
						 'fields' => array(
						'id','wing'								
						),
						'conditions' => array(
							//'current_project' => 0,
							'id' =>$towerarray,
							'wing !=' => ''
						)
			));				
		$this->view = 'wingblock';
		$this->layout = 'ajax';
		
		$this->set('allwing', $allwing);
	}			
	//------Wing Blog End---------
			
			
			
			//-----Area Block Start------			
			public function areablock($projectid){	
                if(isset($this->request->data['check_list']) && !empty($this->request->data['check_list'])){			
					$invIdsArr = $this->request->data['check_list'];
				}
				if(isset($this->request->data['check_list_tower']) && !empty($this->request->data['check_list_tower'])){			
					$invIdsArr = explode(" ",$this->request->data['check_list_tower']);
				}
				//echo '<pre>'; print_r($invIdsArr); die();
				$this->set('invIdsArrJson',json_encode($invIdsArr));					
					$this->loadModel('Inventory_propertie');
					$invArea = $this->Inventory_propertie->find('all', array(
						'fields' => array(
							'DISTINCT(flat_area)'
						),
							'conditions' => array(
							   'inventory_id' => $invIdsArr
							)
						));
					$this->view = 'areablock';
				    $this->layout = 'ajax';
				$this->set('invArea', $invArea);
			}			
			//------Area Block End---------
			
			
			//-----Status Block Start------			
			public function statusblock($projectid){				
				$invArray = json_decode($this->request->data['invIdsArrJson'],true);
				$this->set('selectedInvArrJson',$this->request->data['invIdsArrJson']);				
				$areaArray = $this->request->data['check_list_area'];
				$this->set('selectedAreaArrJson',json_encode($this->request->data['check_list_area']));
				
				$this->loadModel('Inventory_propertie');
				$allstatus = $this->Inventory_propertie->find('all', array(
					'fields' => array(
						'DISTINCT(flat_status)'
					),
						'conditions' => array(
						   'inventory_id' => $invArray,
						   'flat_area' => $areaArray
						)
					));
				$this->view = 'statusblock';
				$this->layout = 'ajax';
				
				$status_arr = $this->InvInventoryPropertyStatus->getStatuses();
				if (is_array($status_arr) && count($status_arr) > 0) {
					foreach ($status_arr as $row) {
						$temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
					}
				}
				$status_arr = $temp_arr;
				
				$this->set('statuses', $status_arr);				
				$this->set('allstatus', $allstatus);
				$this->set('invArea', $invArea);
			}			
			//------Status Block End---------
			
			
			
			//------------------Calculator sales Employee Start ----------------
		  
		public function calculatorSalesEmployees() {
			$this->loadModel('SalesUser');
			$this->loadModel('AppClientLeadsHistory');
			
			$builder = $this->Builder->find('all', array(
					'conditions' => array(
					'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
			if (!empty($builder['0']['Builder']['status'])) {
				if ($builder['0']['Builder']['status'] != 3) {
						$this->redirect('/accounts/dashboard');
					}
						$this->set('status', $builder['0']['Builder']['status']);
			}
			
			$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
			
			$this->SalesUser->bindModel(array(
				'hasMany' => array(
				'AppClientLeadsHistory' => array(
					//'fields'=>array('AppClientLeadsHistory.pdf_link')
					'fields' => array('pdf_link')
					//'conditions' =>array('AppClientLeadsHistory.sales_user_id' => 161)
					)
				)
			));
			$allsalesuser = $this->SalesUser->find('all',array(
			'fields'=>array('id','name','mobile','email'),
			'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales'),
			'order' => array('created' => 'DESC')
			));
		
			if (!empty($_GET['data'])) {				
			//echo '<pre>';print_r($_GET['data']);
				if($_GET['data']['name'] == '' && $_GET['data']['keyword1'] == '' && $_GET['data']['keyword2'] == '')
				{
					
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link')
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
					'fields'=>array('id','name','mobile','email'),
					'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales'),
					'order' => array('created' => 'DESC')
					));
					
				}
				
				if($_GET['data']['name'] == '' && $_GET['data']['keyword1'] != '' && $_GET['data']['keyword2'] == '')
				{
					//echo '<pre>';print_r($_GET['data']);
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link'),
							'conditions' =>array(
							'AppClientLeadsHistory.created >=' => $_GET['data']['keyword1']
								)
							  )
							)
						)
					);
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales'
					),'order' => array('created' => 'DESC')));
					
					//echo '<pre>';print_r($allsalesuser);die();
					
				}
				
				if($_GET['data']['name'] == '' && $_GET['data']['keyword1'] == '' &&	$_GET['data']['keyword2'] != '')
				{
					
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link'),
							'conditions' =>array(
							'AppClientLeadsHistory.created <=' => $_GET['data']['keyword'])
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales'
					),'order' => array('created' => 'DESC')));
					
				}
				
				if($_GET['data']['name'] != '' && $_GET['data']['keyword1'] == '' &&	$_GET['data']['keyword2'] == '')
				{
					//echo '<pre>';print_r($_GET);die();
					
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link')
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales',
						'SalesUser.name LIKE' => $_GET['data']['name'] .'%'
					),'order' => array('created' => 'DESC')));
					
					//echo '<pre>';print_r($allsalesuser);die();
					
				}
				
				if($_GET['data']['name'] != '' && $_GET['data']['keyword1'] != '' &&	$_GET['data']['keyword2'] == '')
				{
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link'),
							'conditions' =>array(
							'AppClientLeadsHistory.created >=' => $_GET['data']['keyword1'])
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales',
						'SalesUser.name LIKE' => $_GET['data']['name'] .'%'
					),'order' => array('created' => 'DESC')));
					
				}
				
				if($_GET['data']['name'] != '' && $_GET['data']['keyword1'] == '' &&	$_GET['data']['keyword2'] != '')
				{
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link'),
							'conditions' =>array(
							'AppClientLeadsHistory.created <=' => $_GET['data']['keyword2'])
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales',
						'SalesUser.name LIKE' => $_GET['data']['name'] .'%'
					),'order' => array('created' => 'DESC')));
					
				}
				
				if($_GET['data']['name'] != '' && $_GET['data']['keyword1'] != '' &&	$_GET['data']['keyword2'] != '')
				{
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link'),
							'conditions' =>array(
							'AppClientLeadsHistory.created between ? and ?' => array($_GET['data']['keyword1'],
							$_GET['data']['keyword2']))
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales',
						'SalesUser.name LIKE' => $_GET['data']['name'] .'%'
					),'order' => array('created' => 'DESC')));
					
				}
				
				
				if($_GET['data']['name'] == '' && $_GET['data']['keyword1'] != '' &&	$_GET['data']['keyword2'] != '')
				{
					$this->SalesUser->bindModel(array(
						'hasMany' => array(
						'AppClientLeadsHistory' => array(
							'fields' => array('pdf_link'),
							'conditions' =>array(
							'AppClientLeadsHistory.created between ? and ?' => array($_GET['data']['keyword1'],
							$_GET['data']['keyword2']))
							)
						)
					));
					$allsalesuser = $this->SalesUser->find('all',array(
						'fields'=>array('id','name','mobile','email'),
						'conditions' => array('SalesUser.builder_id' => $getBuilderId,'user_type'=>'Sales'
					),'order' => array('created' => 'DESC')));
					
				}

			}
			$this->set('list', $allsalesuser);
			$this->layout = 'account';
		}
			  
		  //------------------Calculator sales Employee End--------------
		  
		  
		   //------------------Calculator ChannelPartner Start ----------------
		  
		  public function calculatorChannelPartner() {
		$this->loadModel('SalesUser');
			$builder = $this->Builder->find('all', array(
				'conditions' => array(
							'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
						)
				));
				
	if (!empty($builder['0']['Builder']['status'])) {
		if ($builder['0']['Builder']['status'] != 3) {
						$this->redirect('/accounts/dashboard');
							}
								$this->set('status', $builder['0']['Builder']['status']);
					}
					
	if($this->Session->read('Auth.Websiteuser.userrole') == '2'){			
		$getBuilderIdArray = $this->SalesUser->find('first',
			array('fields'=> array(
				'builder_id'			
				),
					'conditions'=>array(
						'SalesUser.email' => $this->Session->read('Auth.Websiteuser.email')
							)
						)
					); 			
			$getBuilderId =	$getBuilderIdArray['SalesUser']['builder_id'];
		}
		
	//---------------Search start---------
	if (!empty($this->request->data)) {
		if (empty($this->request->data['name']) && !empty($this->request->data['keyword1']) || !empty($this->request->data['keyword2'])  ){
								$this->request->data['name']='.';
				}
			$cond=array('OR'=>array('SalesUser.name LIKE' => $this->request->data['name'] .'%',
			'SalesUser.created between ? and ?' => array($this->request->data['keyword1'], $this->request->data['keyword2']
									)
								) 
						);
			$list = $this->SalesUser->find('all',array('conditions'=>$cond));
	}
  //-------------search end --------

	if($this->Session->read('Auth.Websiteuser.userrole') == '3'){
		$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
			//$getBuilderId=45;
				$allbroker=$this->SalesUser->find('all',
					array(
						'fields'=>array('id','name','mobile','email'),
			'conditions'=>array('builder_id'=>$getBuilderId,'user_type'=>'Broker')
		));
	}
	
	$this->InvInventory->bindModel(array(
		'belongsTo' => array(
				'Project' => array(
					'foreignKey' => false,
						'type' => 'INNER',
							'fields' => array('project_name'),
								'conditions' => array('Project.project_id = InvInventory.project_id')
									),					
										'Websiteuser' => array(
							'foreignKey' => false,
						'type' => 'INNER',
					'fields' => array('id'),
				'conditions' => array('Websiteuser.id = Project.user_id')
			)
		)
	), false);
	
	$keyword = "";
	$scriteria = "";
	if (isset($this->request->params["named"]['keyword'])) {
		$this->request->data['InvInventory']['keyword'] = trim($this->request->params["named"]['keyword']);
	}
	if (isset($this->request->params["named"]['scriteria'])) {
		$this->request->data['InvInventory']['scriteria'] = trim($this->request->params["named"]['scriteria']);
	}
	$conditions = array();	
	if (empty($this->request->data)) {
		$conditions = array('Websiteuser.id' => $getBuilderId);
	}
	else {
		$conditions = array("Websiteuser.id" => $getBuilderId);
			if (isset($this->request->data['InvInventory']['scriteria']) && $this->request->data['InvInventory']['keyword'] != '') {

				$keyword = trim($this->request->data['InvInventory']['keyword']);
					$scriteria = trim($this->request->data['InvInventory']['scriteria']);
						switch ($scriteria) {					
							case 'project_name':
								$conditions["Project.project_name like "] = "%" . $keyword . "%";
						break;
					case 'tower_name':
				$conditions["InvInventory.tower_name like "] = "%" . $keyword . "%";
			break;
			}
		print_r($conditions);
		}
	}
	$this->paginate = array('limit' => 10, 'order' => array('InvInventory.created_at' => 'desc'));
		$this->set('keyword', $keyword);
			$this->set('scriteria', $scriteria);
				$inventories	=	$this->paginate('InvInventory',$conditions);
					$this->set('inventories', $inventories);
						$this->set('brokerlist', $allbroker);
							$this->set('slist', $list);
							
	if ($this->RequestHandler->isAjax()) {
		$this->layout = "ajax";
			$this->viewPath = 'Elements' . DS . 'inventory';
				$this->render('invList');
	}
	$this->layout = 'account';
	}
	
		  
     //------------------Calculator ChannelPartner Employee End--------------
  
  
  
  
    //------------salesPersonDetails Start here ----------------------
  
     public function salesPersonDetails($salesuserid){
				$builder = $this->Builder->find('all', array(
				'conditions' => array(
					'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			));
			if (!empty($builder)) {
				//$this->request->data = $builder[0];            
				$this->set('status', $builder['0']['Builder']['status']);
			} else {
				$builder_id = 0;
				$this->set('buider_id', $builder_id);
			}
				//------search start------
//echo $this->params['pass'][1];
//echo $this->params['pass'][2];

				$dateStartFormated = date_format(date_create($this->params['pass'][1]),"Y-m-d H:i:s");
			 $dateEndFormated = date_format(date_create($this->params['pass'][2]),"Y-m-d H:i:s");
			 
			 //echo $dateStartFormated.'-'.$dateEndFormated;
			if (!empty($this->request->data)) {
				$this->loadModel('AppClientLead');
					$this->loadModel('AppClientLeadsHistory');
						$this->AppClientLeadsHistory->bindModel(array(
								'belongsTo' => array(
									'AppClientLead' => array(
										'foreignKey	' => false,
										'type' => 'LEFT',
										'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
										'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id')
									)
								)
							), false);
							
							$list = $this->AppClientLeadsHistory->find('all', 
							array('conditions' => array('OR'=>array('AppClientLeadsHistory.name LIKE' => $this->request->data['sname'] .'%'	),
							'AppClientLeadsHistory.user_id'=>$salesuserid)));
				}
	           //----------search end-----
					
				$this->loadModel('AppClientLead');
				$this->loadModel('AppClientLeadsHistory');
				$this->AppClientLeadsHistory->bindModel(array(
					'belongsTo' => array(
						'AppClientLead' => array(
							'foreignKey' => false,
							'type' => 'LEFT',
							'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
							'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id'),
						)
					)
						), false);
						
				$leadshistoriesdata = $this->AppClientLeadsHistory->find('all', 
				array('conditions' => array('AppClientLeadsHistory.user_id'=>$salesuserid,
				'AppClientLeadsHistory.created between ? and ?' => array($dateStartFormated, $dateEndFormated)
				)));
				
					if($this->params['pass'][0] && empty($this->params['pass'][1]) || empty($this->params['pass'][2]) ){
						
				   $this->AppClientLeadsHistory->bindModel(array(
					'belongsTo' => array(
						'AppClientLead' => array(
							'foreignKey' => false,
							'type' => 'LEFT',
							'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
							'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id'),
						)
					)
						), false);
						
				$leadshistoriesdata = $this->AppClientLeadsHistory->find('all', 
				array('conditions' => array('AppClientLeadsHistory.user_id'=>$salesuserid
				)));
				
					}
						$this->layout = 'account';
						$this->set('leadshistoriesdata', $leadshistoriesdata);
						$this->set('salesuserid', $salesuserid);
						$this->set('slist', $list);
			 } 
  
        //------------ salesPersonDetails End Here -----------------------
	  
	  
	  
	    //------------brokerPersonDetails Start here ----------------------
	  
	  public function brokerPersonDetails($brokerid){
		//-------search start---
	if (!empty($this->request->data)) {
	$this->loadModel('AppClientLead');
		$this->loadModel('AppClientLeadsHistory');
			$this->AppClientLeadsHistory->bindModel(array(
			'belongsTo' => array(
				'AppClientLead' => array(
					'foreignKey' => false,
					'type' => 'LEFT',
					'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
					'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id'),
				)
			)
				), false);
				
				$list = $this->AppClientLeadsHistory->find('all', 
	array('conditions' => array('OR'=>array('AppClientLeadsHistory.name LIKE' => $this->request->data['sname'] .'%'	),'AppClientLeadsHistory.user_id'=>$brokerid)));
				}
	   //------search end ---------
	   
		$this->loadModel('AppClientLead');
		$this->loadModel('AppClientLeadsHistory');
		$this->AppClientLeadsHistory->bindModel(array(
						'belongsTo' => array(
							'AppClientLead' => array(
								'foreignKey' => false,
								'type' => 'LEFT',
								'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
								'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id'),
							)
						)
			), false);
			$leadshistoriesdata = $this->AppClientLeadsHistory->find('all', 
			array('conditions' => array('AppClientLeadsHistory.user_id'=>$brokerid)));
			//echo '<pre>'; print_r($leadshistoriesdata); die();
			$this->set('leadshistoriesdata', $leadshistoriesdata);
			$this->set('brokerid', $brokerid);
			$this->set('slist', $list);
				$this->layout = 'account';
	} 
	  
	      //---------------brokerPersonDetails End here--------------
	  
  
         //--------- Broker List CSV file download Start ------------------
  
         public function channel_client_lists_csv($brokerid) {
			//echo $brokerid;die();
			$this->loadModel('SalesUser');

			$builder = $this->Builder->find('all', array(
			'conditions' => array(
			'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
			)
			));
			
			if (!empty($builder['0']['Builder']['status'])) {
			if ($builder['0']['Builder']['status'] != 3) {
			$this->redirect('/accounts/dashboard');
			}
			$this->set('status', $builder['0']['Builder']['status']);
			}
			//print_r($this->Session->read('Auth.Websiteuser.id'));
				$this->loadModel('AppClientLead');
				$this->loadModel('AppClientLeadsHistory');
				
				$this->AppClientLeadsHistory->bindModel(array(
								'belongsTo' => array(
									'AppClientLead' => array(
										'foreignKey' => false,
										'type' => 'LEFT',
										'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
										'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id'),
									)
								)
					), false);
					$brokerdetails = $this->AppClientLeadsHistory->find('all', 	
					array('fields'=>array('AppClientLeadsHistory.name','AppClientLeadsHistory.pdf_link', 'AppClientLead.email','AppClientLead.mobile'),'conditions' => array('AppClientLeadsHistory.user_id'=>$brokerid)));
													
				$headers = array(
					'data'=>array(
					'name' => 'Name',
					'pdf Link'=>'Pdf Link',
					'email' => 'Email',
					'mobile' => 'Mobile',
					
					)
				);
				
				$i = 0;
				$result=array();
				foreach($brokerdetails as $data)
				{
					$result[$i]['data'] = $data['AppClientLeadsHistory'];
					$result[$i]['data']['email'] = $data['AppClientLead']['email'];
					$result[$i]['data']['mobile'] = $data['AppClientLead']['mobile'];
					$i++;
				}
				array_unshift($result,$headers);
				$this->set(compact('result')); 
				//echo '<pre>'; print_r($result); die();

				$this->layout = 'account';
			}
  //------------ Broker List CSV file download End-------------
			
	//---------sales Person Details CreatedBy Broker Start -----------
		  
			 public function brokerPersonDetailsCreatedByBroker($brokerid){
				$getBuilderId = $this->Session->read('Auth.Websiteuser.id');
					$this->loadModel('app_client_leads');
							$this->loadModel('app_client_leads_histories');
								$this->loadModel('sales_users');
				$leadshistoriesdata=$this->sales_users->find('all',
						array(
							'fields'=>array('sales_users.*'),
									'conditions'=>array('sales_users.broker_id' =>$brokerid ,'sales_users.user_type'=>'Broker_sales') 
						)
					);
				   $client_id = $this->request->params['pass'];
				$this->layout = 'account';
				$this->set('leadshistoriesdata', $leadshistoriesdata);
				$this->set('client_id', $client_id); 
		  }
		  //----------sales Person Details CreatedBy Broker End--------------	

		
		//--------- Sales Employees Details Lists CSV file download Start ------------------

			public function sales_employees_details_lists_csv($brokerid) {
			//echo $brokerid;
			$this->loadModel('SalesUser');

			$builder = $this->Builder->find('all', array(
			'conditions' => array(
			'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
			)
			));

			if (!empty($builder['0']['Builder']['status'])) {
			if ($builder['0']['Builder']['status'] != 3) {
			$this->redirect('/accounts/dashboard');
			}
			$this->set('status', $builder['0']['Builder']['status']);
			}
				$this->loadModel('AppClientLead');
				$this->loadModel('AppClientLeadsHistory');
				$this->AppClientLeadsHistory->bindModel(array(
					'belongsTo' => array(
						'AppClientLead' => array(
							'foreignKey' => false,
							'type' => 'LEFT',
							'fields' => array('AppClientLead.id', 'AppClientLead.email','AppClientLead.mobile'),
							'conditions' => array('AppClientLeadsHistory.client_id = AppClientLead.id'),
						)
					)
						), false);
						
				$salesPersonDetailsdata = $this->AppClientLeadsHistory->find('all', 
				array('fields'=>array('AppClientLeadsHistory.name','AppClientLead.email','AppClientLead.mobile','AppClientLeadsHistory.pdf_link'),'conditions' => array('AppClientLeadsHistory.user_id'=>$brokerid)));
			
				$headers = array(
					'data'=>array(
					'name' => 'Name',
					'pdf' => 'Pdf',
					'email' => 'Email',
					'mobile' => 'Mobile'					
					)
				);			
					$i = 0;
				$result=array();
				foreach($salesPersonDetailsdata as $data)
				{
					$result[$i]['data'] = $data['AppClientLeadsHistory'];
					$result[$i]['data']['email'] = $data['AppClientLead']['email'];
					$result[$i]['data']['mobile'] = $data['AppClientLead']['mobile'];
					$i++;
				}
				
				array_unshift($result,$headers);
				$this->set(compact('result')); 
				//echo '<pre>'; print_r($result); die();

				$this->layout = 'account';
			}
			//------------ Sales Employees Details Lists CSV file download Start CSV file download End---------
			
			
			//------------channel partner lists CSV Start Here----------

			public function channel_partner_lists_csv() {
			$this->loadModel('SalesUser');
			$builder = $this->Builder->find('all', array(
			'conditions' => array(
			'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
			)
			));
			if (!empty($builder['0']['Builder']['status'])) {
				if ($builder['0']['Builder']['status'] != 3) {
					$this->redirect('/accounts/dashboard');
						}
					$this->set('status', $builder['0']['Builder']['status']);
			}
			$data = $this->SalesUser->find('all',
				array(
					'fields' => array('name','email','mobile','created'),
						'conditions' => array(
							//'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
								'builder_id' => 45,
									'user_type' => 'Broker',
										'user_created_by' => 'Builder'
						))
			);
			$headers = array(
				'SalesUser'=>array(
				'name' => 'Name',
				'email' => 'Email',
				'mobile' => 'Mobile',
				'created' => 'Created'
				)
			);
			array_unshift($data,$headers);
			$this->set(compact('data'));
			$this->layout = 'account';
			}
			//---------------channel partner lists CSV END Here----------
			
			
			//--------- Broker Person Details Created By Broker CSV file download Start ------------------

			public function brokerPersonDetailsCreatedByBrokerCsv($brokerid) {
			//echo $brokerid;die();
			$this->loadModel('SalesUser');
			$builder = $this->Builder->find('all', array(
				'conditions' => array(
					'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			));

			if (!empty($builder['0']['Builder']['status'])) {
				if ($builder['0']['Builder']['status'] != 3) {
					$this->redirect('/accounts/dashboard');
						}
						$this->set('status', $builder['0']['Builder']['status']);
			}
			$this->loadModel('sales_users');
				$leadshistoriesdata=$this->sales_users->find('all',
				array(
					'fields'=>array('sales_users.name','sales_users.email','sales_users.mobile'),
					'conditions'=>array('sales_users.broker_id' =>$brokerid ,'sales_users.user_type'=>'Broker_sales') 
						)
					);		
				$headers = array(
					'sales_users'=>array(
					'name' => 'Name',
					'email' => 'Email',
					'mobile' => 'Mobile',
					
					)
				);
				array_unshift($leadshistoriesdata,$headers);
				$this->set(compact('leadshistoriesdata')); 
				$this->layout = 'account';
			}
		//------------ Broker Person Details Created By Broker CSV file download End-------------

}
?>