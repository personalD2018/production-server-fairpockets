<?php
	
App::uses('AppController', 'Controller');
class TestsController extends AppController {
	
	public $name = 'Tests'; //Controller name
	public $uses = array('Solr'); //Model name
	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('index', 'get');
	}
	
	public function index(){
		$cond = "active:1";
		$sort = "timestamp desc";
		
		$params = array(
			'conditions' =>array(
				'solr_query' => $cond
			),
			'order' => $sort,
		'offset' => 0,
		'limit' => 1
		);
		
		$result = $this->Solr->find('all', $params);
		$this->layout = 'ajax';
		//header('Content-Type: application/json; charset=utf-8'); /* To send response in json format */
		echo json_encode($result);
		die;
	}
	
	public function get() {
		
	}
}	