<?php
	
	/**
		* Static content controller.
		*
		* This file will render views from views/pages/
		*
		* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
		* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		*
		* Licensed under The MIT License
		* For full copyright and license information, please see the LICENSE.txt
		* Redistributions of files must retain the above copyright notice.
		*
		* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.Controller
		* @since         CakePHP(tm) v 0.2.9
		* @license       http://www.opensource.org/licenses/mit-license.php MIT License
	*/
	App::uses('AppController', 'Controller');
	App::uses('CakeEmail', 'Network/Email');
	$title = "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties 11";
	/**
		* Static content controller
		*
		* Override this controller by placing a copy in controllers directory of an application
		*
		* @package       app.Controller
		* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
	*/
	class PagesController extends AppController {
		
		/**
			* This controller does not use a model
			*
			* @var array
		*/
		public $uses = array(
        'Property', 
        'MstAreaState', 
        'MstPropertyTypeOption', 
        'FeatureMasters', 
        'PropertyFeatures', 
        'AmentieMaster', 
        'PropertyPic', 
        'MstAreaRegion','MstAreaState','MstAreaCity', 'MstAreaLocality', 
        'PropertyInquirie', 
        'User', 
        'PropertyNotifiction', 
        'MstAreaCountry', 
        'Project', 
        'Builder', 
        'ProjectDetails', 
        'Bank', 
        'ProjectFinancer', 
        'ProjectPricing', 
        'ProjectOfficeUse', 
        'Builder', 
        'Websiteuser', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState', 
        'FeaturePropertyOptionView', 
        'AmenityPropertyOptionView', 
        'PropertyAmenity',
		'PropertyFeature', 
        'MstAmenity', 
		'MstFeature',
        'VAmenityPropertyOption', 
        'VFeaturePropertyOption',
        'FeatureAminityPropertyOptionView',
		'Lead');
		var $helpers = array("Number");
		public $components = array('Paginator','Propertydata');
		
		/**
		* Displays a view
		* @return void
		* @throws ForbiddenException When a directory traversal attempt.
		* @throws NotFoundException When the view file could not be found
		* or MissingViewException in debug mode.
		*/
				
		public function beforeFilter() {
			parent::beforeFilter();
			// Allow users to register and logout.
			$this->Auth->allow('near','display', 'emi_calc', 'rentvbuy_calc', 'about_us', 'our_team', 'faq', 'research_reports', 'advisory', 'property_management', 'portfolio_management', 'career', 'contact', 'feedback', 'pap', 'tac', 'pricing', 'search_list', 'search_list_detail', 'recentProperties','builder_property_details','getbspoptions','serverinfo','calculator');
		}
		
		public function serverinfo() {
			
			phpinfo();
			
		}
		
		public function display() {
			
			$this->set('status', 3);
			$this->set('remember_me_user', $this->Cookie->read('remember_me_user'));
			$this->layout = "home";
			$path = func_get_args();
			
			$count = count($path);
			if (!$count) {
				return $this->redirect('/');
			}
			if (in_array('..', $path, true) || in_array('.', $path, true)) {
				throw new ForbiddenException();
			}
			$page = $subpage = $title_for_layout = null;
			
			if (!empty($path[0])) {
				$page = $path[0];
			}
			if (!empty($path[1])) {
				$subpage = $path[1];
			}
			if (!empty($path[$count - 1])) {
				//$title = "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties";
					}
			
			$this->set(compact('page', 'subpage', 'title'));
			
			try {
				$this->render(implode('/', $path));
				} catch (MissingViewException $e) {
				if (Configure::read('debug')) {
					throw $e;
				}
				throw new NotFoundException();
			}
		}
		
		/*public function getcity() {

			$this->loadModel('MstAreaCity');
			$city = $this->MstAreaCity->find('all', array(
				'fields' => array(
					'cityName'
				)
			));
			echo '884';print_r($city);die();
			return $city;
			$this->set('city', $city);

		}*/
		
		public function search_list() {
		
			//$this->near();die;
			
			$this->layout = "search";
			include_once(ROOT . DS . "vendors".DS."autoload.php");
			$config = $this->getSolrconfig();
			$client = new Solarium\Client($config);

			// this executes the query and returns the result
			//$resultset = $client->select($query);

			// get a select query instance
			$query = $client->createSelect();
			// set a query (all prices starting from 12)
			$limit = 20;
			$start = 1;
			$page = 0;
			
			if(isset($this->params->query['page']) && $this->params->query['page'] >= 1) {
				$page = ($this->params->query['page'] - 1) * 10; 
				$start = $this->params->query['page'];
				unset($this->params->query['page']);
			}
			
			$this->loadModel('MstAreaCity');
			$city = $this->MstAreaCity->find('list', array(
				'fields' => array(
					'cityCode',
					'cityName',
				)
			));
			//$title ='';
			//Add filters for PROPERTY Name
			if(isset($this->params->query['property_name']) && $this->params->query['property_name']) {
				$search_for = $this->params->query['property_name'];
				
				if($index = $this->stristrarray($city, $search_for)) {
					$title = "Property for ".$search_for;
					$query->setQuery($search_for .', '. $city[$index]);
					//$query->createFilterQuery('property_name')->setQuery('property_name:'.$search_for.'');
					//$query->createFilterQuery('city')->setQuery('city:'.$city[$index].'');
				} else {
					//Add filter for Default location
					if(isset($this->params->query['default_location']) && $this->params->query['default_location']) {
						$default_location = $this->params->query['default_location'];
						$query->createFilterQuery('city')->setQuery('city:'.$default_location.'');
						$query->setQuery($search_for);
						$title = "Property for ".$default_location;
					}else{
						$query->setQuery($search_for);
						$title = "Property ".$search_for;
					}
				}
			}else{
				//Add filter for Default location
				if(isset($this->params->query['default_location']) && $this->params->query['default_location']) {
					$default_location = $this->params->query['default_location'];
					$query->createFilterQuery('city')->setQuery('city:'.$default_location.'');
					$title = "Property for ".$default_location;
				}
			}
			
			//Add filters for TRANSACTION TYPE
			if(isset($this->params->query['transaction_type'])) {
				$transaction_type = $this->params->query['transaction_type'];
				if($transaction_type == 'buy') {
					$transaction_type = 'sell';
				}
				$title .= " / Property for ".$transaction_type;
				if($transaction_type == 'posted_by') {
				$query->createFilterQuery('transaction_type')->setQuery('posted_by:Builder');
				}else{
				$query->createFilterQuery('transaction_type')->setQuery('transaction_type:'.$transaction_type.'');
				}
			}
			
			//Add filters for PROPERTY TYPE
			if(isset($this->params->query['property_type'])) {
				$property_type_query = 'property_type:('.implode(' OR ', $this->params->query['property_type']).')';
				$query->createFilterQuery('property_type')->setQuery($property_type_query);
				$title .= " / Property for ".$property_type_query;
			}
			
			//Filter data based on applied filters
			
			if($this->request->is('ajax')) {
				if(isset($this->params->query['filters'])) {
					$filters = $this->createFilters($this->params->query['filters']);
					foreach($filters as $field => $value){
						$query->createFilterQuery($field)->setQuery($field.':'.$value);
					}
					//$filters = json_decode($this->params->query['filters']);
				
					//pr($filters);die;
				}
				
				$this->layout = "ajax";
				$this->view = 'search_list_ajax';
			}
			if(isset($this->params->query['sortby']))
			{
			if($this->params->query['sortby'] == 0)
			{
				$this->params->query['sortby'] = 'score';
			}
			}
			if(isset($this->params->query['sortby']) && $this->params->query['sortby'] != 'score') {
				$sortby = $this->params->query['sortby'];
				// sort the results by price ascending
				$options = array();
				$options = array(
					'date'				=> array('post_date', 'DESC'),
					'discount' 			=> array('discount_percent', 'DESC'),
					'offer_price_low' 	=> array('offer_price', 'ASC'),
					'offer_price_high' 	=> array('offer_price', 'DESC'),
					'offer_price_sqft_low' 	=> array('offer_price_sqft', 'ASC'),
					'offer_price_sqft_high' => array('offer_price_sqft', 'DESC')
				);
				$query->addSort($options[$sortby][0], $options[$sortby][1]);
			}
			
			$query->addSort('score', $query::SORT_DESC);
			
			// get the facetset component
			$facetSet = $query->getFacetSet();
			
			// create a facet field instance and set options
			$facetSet->createFacetField('Society')->setField('project_name');
			$facetSet->createFacetField('Locality')->setField('sublocality1');
			//$facetSet->setLimit(-1);
			
			$query->setStart($page)->setRows($limit);
			
			// this executes the query and returns the result
			try{
				$result = $client->select($query);
			/*
				$prop = [];
				
				$this->loadModel('NewProperty');
				$this->loadModel('WebsiteUser');
					
				foreach($result as $data) {
					
					//For user add into website user table
					$user = $this->WebsiteUser->checkUserExists($data);
					// Insert properties into database on the basis on user phone no validation.
					
					
					
					$prop[] = [
							'user_id' 			=> $user['id'],
							'employee_id' 		=> '0',
							'posted_by' 		=> $user['role'],
							'project_id' 		=> $data['id'],
							'posted_by' 		=> $data['posted_by'],
							'transaction_type' 	=> $data['transaction_type'],
							'property_type' 	=> $data['property_type'],
							'city' 				=> $data['city'],
							'project_name' 		=> $data['project_name'],
							'super_builtup_area' => $data['super_builtup_area'],
							'builtup_area' 		=> $data['builtup_area'],
							'configure' 		=> $data['configure'],
							'total_floors' 		=> $data['total_floors'],
							'on_floor' 			=> $data['on_floor'],
							'covered_parking' 	=> $data['covered_parking'],
							'offer_price' 		=> $data['offer_price'],
							'offer_price_sqft' 	=> $data['offer_price_sqft'],
							'market_price' 		=> $data['market_price'],
							'market_price_sqft' => $data['market_price_sqft'],
							'discount' 			=> $data['discount'],
							'discount_sqft' 	=> $data['discount_sqft'],
							'discount_percent' 	=> $data['discount_percent'],
							'availability' 		=> $data['availability'],
							'sale_type' 		=> $data['sale_type'],
							'posession_date' 	=> $data['posession_date'],
							'amenities' 		=> json_encode($data['amenities']),
							'power_backup' 		=> $data['power_backup'],
							'water_source' 		=> $data['water_source'],
							'facing' 			=> $data['facing'],
							'flooring_type' 	=> $data['flooring_type'],
							'furnishings' 		=> $data['furnishings'],
							'posted_by_user_name' 		=> $data['posted_by_user_name'],
							'posted_by_user_contact' 	=> json_encode($data['posted_by_user_contact']),
							'score' 			=> $data['score'],
							'post_date' 		=> $data['post_date'],
							'sublocality1' 		=> $data['sublocality1'],
							'sublocality2' 		=> $data['sublocality2'],
							'sublocality3' 		=> $data['sublocality3'],
							'age_of_property' 		=> $data['age_of_property'],
						];
						
						//break;
					}
					
					//pr($prop);die;
				$this->NewProperty->saveMany($prop);
				$sss = $this->NewProperty->getInsertID();
				echo '++++'.$sss;
				die;*/
			} catch(Exception $e){  //catch exception
				echo 'Message: ' .$e->getMessage();
			}
				//echo '<pre>';print_r($result);die();
			$this->set(compact('result', 'limit', 'start', 'title'));
			
		}

		private function stristrarray($array, $str){
			//This array will hold the indexes of every
			//element that contains our substring.
			//$indexes = array();
			//$str = '';
			//$v = '';
			foreach($array as $k => $v){
				//If stristr, add the index to our
				//$indexes array.
				if($v){
				if(stristr($str, $v)){
					return $k;
				}
				}
			}
			return false;
		}
		
		private function createFilters() {
			$filters = json_decode($this->params->query['filters']);
			$sim_fil = [
				'sublocality1'	=> 'locality-filters', 
				'project_name' 	=> 'society-filters', 
				'sale_type' 	=> 'sale-type-filters', 
				'furnishings' 	=> 'furnishing-filters', 
				'amenities' 	=> 'amenities-filters',
				'configure'		=> 'bedroom-filters',
				'posted_by'		=> 'posted-by-filters',
				'availability'  => 'availability-filters',
				'age_of_property' 	=> 'age_of_property-filters',
				'posession_date' 	=> 'posession_date-filters'
			];
			
			$fil = [];
			foreach($filters as $name => $values) {
				if(count($values)) {
					if($key = array_search($name, $sim_fil)) {
						$fil[$key] = '("'.implode('" OR "', $values).'")';
					} else {
						$fil[$name] = '[' . $values . ']';
					}
				}
			}
			
			return $fil;
		}
		
		public function recentProperties($location) {
			$this->layout = "ajax";
			
			
			include_once(ROOT . DS . "vendors".DS."autoload.php");
			$config = $this->getSolrconfig();
			
			$client = new Solarium\Client($config);
			
			// get a select query instance
			$query = $client->createSelect();
			
			// set a query (all prices starting from 12)
			$limit = 10;
			$page = 0;
				
			$query->createFilterQuery('city')->setQuery('city:'.$location.'');
			$query->setStart($page)->setRows($limit);
			
			// this executes the query and returns the result
			try{
				$result = $client->select($query);
			}
			//catch exception
			catch(Exception $e){
				echo 'Message: ' .$e->getMessage();
			}	
			
			//pr($result);die;
			$this->set(compact('result'));
		}

		public function near() {
			$this->layout = false;
			//$this->autoRender = false;
			include_once(ROOT . DS . "vendors".DS."autoload.php");
			$config = $this->getSolrconfig();
			
			$client = new Solarium\Client($config);	
			// get a morelikethis query instance
			$query = $client->createMoreLikeThis();

			$query->setQuery('id:PUN_1');
			$query->setMltFields('manu,cat');
			$query->setMinimumDocumentFrequency(1);
			$query->setMinimumTermFrequency(1);
			$query->createFilterQuery('stock')->setQuery('inStock:true');
			$query->setInterestingTerms('details');
			$query->setMatchInclude(true);

			// this executes the query and returns the result
			$resultset = $client->select($query);

			pr($resultset);die;

			// display the total number of MLT documents found by solr
			echo 'Number of MLT matches found: '.$resultset->getNumFound().'<br/><br/>';
			echo '<b>Listing of matched docs:</b>';

			// show MLT documents using the resultset iterator
			foreach ($resultset as $document) {

				echo '<hr/><table>';

				// the documents are also iterable, to get all fields
				foreach ($document as $field => $value) {
					// this converts multivalue fields to a comma-separated string
					if (is_array($value)) {
						$value = implode(', ', $value);
					}

					echo '<tr><th>' . $field . '</th><td>' . $value . '</td></tr>';
				}

				echo '</table>';
			}

		}
		
		public function search_list_detail($property_id = null) {
			
			
				$this->layout = "home";
			$id = base64_decode($property_id);
        $propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);
		//echo $id;
		
		//$all_property_features = '';
		//$all_property_features_unit = '';

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();
		
		//$all_property_amenities = '';
		//$all_property_amenities_unit = '';

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    ' 	localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);



        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		
		//echo '<pre>'; print_r($propertyImages);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        if (!empty($projectdetail['0'])) {
            $this->set('projectdetail', $projectdetail['0']);
        }
        if (!empty($project['0'])) {
            $this->set('project', $project['0']);
            $this->set('propertyImages', $propertyImages);
        }
        $this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
			
		}
		
		public function builder_property_details($property_id = null) {
				$this->layout = "home";
			$id = base64_decode($property_id);
			
			$propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);die();
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    'localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_financer = $this->ProjectFinancer->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_pricing = $this->ProjectPricing->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $Websiteuser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'id' => $propertydetails['Property']['user_id']
            )
        ));
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $propertydetails['Property']['user_id']
            )
        ));
        $pastproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'user_id' => $propertydetails['Property']['user_id'],
                'approved' => '1',
                'current_project' => '0'
            )
        ));
        $pastprojectdata = (implode($pastproject, ','));
		
		
		$this->loadModel('ProjectImage');
		$proj_image = $this->ProjectImage->find('all', array(
                'conditions' => array(
                    'project_id' => $propertydetails['Property']['project_id']
                )
            ));
			
			$this->set('proj_image', $proj_image);
			
		$this->loadModel('ProjectOfficeUse');
		$proj_office = $this->ProjectOfficeUse->find('all', array(
                'conditions' => array(
                    'project_id' => $propertydetails['Property']['project_id']
                )
            ));
			
			$this->set('proj_office', $proj_office);
		//echo '<pre>'; print_r($proj_office);
		
        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
            ));
		$all_property_features = array();
		$all_property_features_unit = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
			$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
		}
		
		
        $this->set('pastprojectdata', $pastprojectdata);
        $this->set('Websiteuser', $Websiteuser['0']);
        if(!empty($builder))
		{
        	$this->set('builder', $builder['0']);
		}
		else
		{
			$this->set('builder', $builder);
		}
		if(!empty($project_pricing))
		{
        	$this->set('project_pricing', $project_pricing['0']);
		}
		else
		{
			$this->set('project_pricing', $project_pricing);
		}
        if(!empty($project_financer))
		{
        	$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
		}
		else
		{
			$this->set('project_financer', $project_financer);
		}
		
        
		if(!empty($projectdetail))
		{
        	$this->set('projectdetail', $projectdetail['0']);
		}
		else
		{
			$this->set('projectdetail', $projectdetail);
		}
        
        $this->set('propertyImages', $propertyImages);
		if(!empty($propertyoption))
		{
        	$this->set('propertyoption', $propertyoption['0']);
		}
		else
		{
			$this->set('propertyoption', $propertyoption);
		}
        $this->set('propertydetails', $propertydetails);
		
        $this->set('propertyfeaturesunit', $all_property_features_unit);
        $this->set('project', $project['0']);
		
		
        //$this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
		
		$this->loadModel('ProjectBspDetail');
		$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$propertydetails['Property']['project_id']."");			
		//echo '<pre>';print_r($bsp_limit);
        $this->set('bsp_limit', $bsp_limit );		
	   
			
		}
		
		
		
		public function calculator($property_id = null) {
				$this->layout = "home";
				$id = base64_decode($property_id);			
				$propertydetails = $this->Property->findByPropertyId($id);
				
				$propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);
				foreach($propertyFeatureData as $property_val)
				{
					$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
					$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
				}
				$this->set('all_property_features', $all_property_features);
				$this->set('all_property_features_unit', $all_property_features_unit);
	  

       

				$project = $this->Project->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));
				$projectdetail = $this->ProjectDetails->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));

				$project_financer = $this->ProjectFinancer->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));

				$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));				
						
				
				$property_features = $this->PropertyFeature->find('all', array(
						'conditions' => array(
							'properties_id' => $id
						)
					));
				$all_property_features = array();
				$all_property_features_unit = array();
				foreach($property_features as $property_val)
				{
					$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
					$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
				}
		
		
				
				if(!empty($project_pricing))
				{
					$this->set('project_pricing', $project_pricing['0']);
				}
				else
				{
					$this->set('project_pricing', $project_pricing);
				}
				if(!empty($project_financer))
				{
					$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
				}
				else
				{
					$this->set('project_financer', $project_financer);
				}
				
				
				if(!empty($projectdetail))
				{
					$this->set('projectdetail', $projectdetail['0']);
				}
				else
				{
					$this->set('projectdetail', $projectdetail);
				}
				
				$this->set('propertydetails', $propertydetails);
				
				$this->set('propertyfeaturesunit', $all_property_features_unit);
				$this->set('project', $project['0']);
				
				$this->set('propertydetails', $propertydetails);
				
				$this->loadModel('ProjectBspDetail');
				$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$propertydetails['Property']['project_id']."");			
				//echo '<pre>';print_r($bsp_limit);
				$this->set('bsp_limit', $bsp_limit );

		
				
				/*$gOfferPrice = $propertydetails['Property']['offer_price'];
				$gServiceTax = 0;
				if (!empty($all_property_features['47'])) {
					$gSuperBuiltUpArea = $all_property_features['47'];
				} else if(!empty($all_property_features['50'])) {
					$gSuperBuiltUpArea = $all_property_features['50'];
				}
				else{
					$gSuperBuiltUpArea = '';
				}
				// Super Built up area ( in sq ft )
				//echo '<pre>'; print_r($project_pricing);
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}*/

				$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];

						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}

						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];
					}
				}
				//echo '884'; echo '<pre>'; print_r($project_charges); die();
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Sinking Fund";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				$gaProjectChargesLen = count($gaProjectCharges);
				//print 
				echo '<pre>'; print_r($gaProjectCharges);//die();
				print json_encode($gaProjectCharges); die();
				
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				// Project Pricing - Floor PLC
				$gaTotalFloors = $projectdetail['ProjectDetails']['num_of_tow']; // Total number of floor in the tower of property
				if(!empty($project_pricing))
				{
				if ($project_pricing['ProjectPricing']['floor_per_plc_amount'] != '') {
					$gaFloorPLC = array(
						// [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
						array($project_pricing['ProjectPricing']['floor_option'], $project_pricing['ProjectPricing']['floor_per_plc_amount'], $project_pricing['ProjectPricing']['floor_per_option'])
							//array( 2 , 10 , 1 )
					);
				} else {
					$gaFloorPLC = array(
						// [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
						array($project_pricing['ProjectPricing']['floor_option'], 0, $project_pricing['ProjectPricing']['floor_per_option'])
							//array( 2 , 10 , 1 )
					);
				}
				}
				else
				{
					 $gaFloorPLC = array();
				}

				/*
				  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
				  pr($gaProjectCharges);
				  echo "<br>"."gaStampDuty"."<br>";
				  pr($gaStampDuty);
				  echo "<br>"."gaRegistration"."<br>";
				  pr($gaRegistration);
				  echo "<br>"."gaFloorPLC"."<br>";
				  pr($gaFloorPLC);
				 */


				$gaMandatoryCharges = '';
				$gaOptionalCharges = '';
				$gaOptionalCount = 0;
				$ProjectCharges = 0;
				$gaFloorPLCCharges = 0;

				if(!empty($gaFloorPLC))
				{
				if ($gaFloorPLC[0][1] > 0) {
					$gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

					if ($gaFloorPLC[0][0] == 2) {
						// BSP w.r.t Ground floor
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					} else {
						// BSP w.r.t TOP floor;
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					}

					if ($gaFloorPLC[0][2] == 2) {
						// sub option
						$gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
					}

					$gaMandatoryCharges = $gaMandatoryCharges . "
									<div class='col-sm-12'>
										<label for='' class='checkbox-custom-label'>
								";
					$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
							'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC1[0][1] . "&nbsp;/ sq ft</span>
									";
					$gaMandatoryCharges = $gaMandatoryCharges . "
										</label>  
									</div>
							";
				}
				}
				$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
				$gOfferPrice = $gOfferPrice + $BaseCharge;
				$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

				//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
				//echo "<br><br>BaseCharge - ".$BaseCharge;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				// Project Charges
				$ProjectCharges = 0;

				echo '<pre>'; print_r($gaProjectCharges);

				for ($row = 0; $row < $gaProjectChargesLen; $row++) {
					//echo "<br><br>".$gaMandatoryCharges;
					if (1 == $gaProjectCharges[$row][1]) {
						// Mandatory to include the charges

						$gaMandatoryCharges = $gaMandatoryCharges . "
										<div class='col-sm-12'>
											<label for='' class='checkbox-custom-label'>
								";

						//Calculate Charges

						$MandatoryChargeAmt = $gaProjectCharges[$row][2];
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
									";
						}

						$gaMandatoryCharges = $gaMandatoryCharges . "
											</label>  
										</div>
								";

						//echo "<br><br>	ProjectCharges - ".$ProjectCharges;
					} else {
						// Optional to include the charges
						$gaOptionalCount = $gaOptionalCount + 1;
						$tempid = "optional" . $gaOptionalCount;

						$gaOptionalCharges = $gaOptionalCharges . "
										<div class='col-sm-12'>
											
								";

						//Calculate Charges
						$OptionalChargeAmt = $gaProjectCharges[$row][2];
						$tempChargeAmt = 0;
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

							// Display Charge in Optional Section
							$gaOptionalCharges = $gaOptionalCharges . "
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$tempChargeAmt = $OptionalChargeAmt;

							// Display Charge in Mandatory Section
							$gaOptionalCharges = $gaOptionalCharges . "
									
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
									";
						}

						$gaOptionalCharges = $gaOptionalCharges . "
											
											</label>  
										</div>
								";
					}

					//echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
				}

				$gProjectCharges_a = $ProjectCharges;

				$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
				$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

				//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
				//echo "<br><br>gServiceTax - ".$gServiceTax;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				//Stamp Duty
				$StampDuty = 0;
				if(!empty($gaStampDuty)){
				if (2 == $gaStampDuty[0][0]) {
					// Percentage
					$StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
				} else {
					// Flat Charges
					$StampDuty = $gaStampDuty[0][1];
				}
				}
				else{
				$StampDuty = '';
				}
				$gOfferPrice = $gOfferPrice + $StampDuty;

				//echo "<br><br>StampDuty - ".$StampDuty;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				//Registration Charges
				$Registration = 0;
				if(!empty($gaRegistration)){
				if (2 == $gaRegistration[0][0]) {
					// Percentage
					$Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
				} else {
					// Flat Charges
					$Registration = $gaRegistration[0][1];
				}
				}
				else{
				$Registration = '';
				}
				$gOfferPrice = $gOfferPrice + $Registration;
				
				echo $gaMandatoryCharges;

				//echo "<br><br>Registration - ".$Registration;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
	   
			
		}
		
		/*public function search_list_detail($property_id = null) {
			$this->layout = "home";
			//echo $property_id;die;
			if($property_id != null) {
				
				
				$config = $this->getSolrconfig();
				
				include_once(ROOT . DS . "vendors".DS."autoload.php");	
				$client = new Solarium\Client($config);
				
				
				$query = $client->createSelect();
				
				
				// get a select query instance
				$query = $client->createSelect();

				
				// set a query (all prices starting from 12)
				$search_for = "id:".base64_decode($property_id);

					$this->loadModel('PropertyPic');
					$propertyImages = $this->PropertyPic->find('list', array(
						'fields' => array(
							'id',
							'pic',
						),
						'conditions' => array(
							'property_id' => base64_decode($property_id)
						)
					));
					$this->set('propertyImages', $propertyImages);
				
				
				// add a query and morelikethis settings (using fluent interface)
				$query->setQuery($search_for)
					  ->getMoreLikeThis()
					  ->setFields('project_name,builder_name,sublocality1,sublocality2,sublocality3')
					  ->setMinimumDocumentFrequency(1)
					  ->setMinimumTermFrequency(1);

				// this executes the query and returns the result
				
				
				try{
				
					$result = $client->select($query);
					$similar = $result->getMoreLikeThis(); 
				
					//$result = $client->select($query);
					
				}
				//catch exception
				catch(Exception $e){
					echo 'Message: ' .$e->getMessage();
				}	
				$this->set(compact('result', 'similar'));
				//$this->set('result', $resultset);
			}
		}*/
		
		public function emi_calc() {
			$this->layout = "home";
			$title = "Calculate easy installment EMI with our calculator";
			$this->set('title', $title);
		}
		
		public function rentvbuy_calc() {
			$this->layout = "home";
			$title = "Calculate rent vs buy analysis with our calculator";
			$this->set('title', $title);
		}
		
		public function about_us() {
			//ini_set('max_execution_time', 600);
			//phpinfo();
			$this->layout = "search";
			//$title = " ";
			//$this->set('title', $title);
		}
		
		public function our_team() {
			$this->layout = "home";
			//$title = "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties";
			//$this->set('title', $title);
		}
		
		public function faq() {
			$this->layout = "home";
			$title = "Fairpockets FAQ always help to assest you";
			$this->set('title', $title);
		}
		
		public function research_reports() {
			$this->layout = "home";
			$title = "Fairpockets | Find the property reports with assessment";
			$this->set('title', $title);
		}
		
		public function advisory() {
			$this->layout = "search";
			$title = "Fairpockets | Searching your prospective buyer or seller in the fastest possible time";
			$this->set('title', $title);
		}
		
		public function property_management() {
			$this->layout = "search";
			$title = "Fairpockets | Get your property professionally managed by us";
			$this->set('title', $title);
		}
		
		public function portfolio_management() {
			$this->layout = "search";
			$title = "Fairpockets | Portfolio management service process";
			$this->set('title', $title);
		}
		
		public function career() {
			//header('Content-Type: text/plain; charset=utf-8');
			$this->loadModel('Career');
			$this->layout = "search";
			$title = "Fairpockets | Make your career with India's No.1 Real estate company";
			$this->set('title', $title);
			if(!empty($this->request->data)){
				//print_r($_FILES);die();
				$target_dir = 'upload/career_images/';				
				//echo $target_dir; print_r($this->request->data);die();				
				if (!empty($this->request->data['Career']['resume_path'])) {
					$name = $this->request->data['Career']['resume_path']['name'];
					
					if (!empty($name)) {
						// Check For Empty Values 
						$tmprary_name = $this->request->data['Career']['resume_path']['tmp_name'];
						$temp = explode(".", $name);
						$newfilename = uniqid() . '.' . end($temp);
						if(move_uploaded_file($tmprary_name, $target_dir . $newfilename)){
							$this->request->data['Career']['status'] = 1;
							$this->request->data['Career']['resume_path'] = $name;
							if ($this->Career->save($this->request->data)) {				
								$this->Session->setFlash('Thanks for contacting Us, We will get back to you shortly.', 'default', array('class' => 'green'));
								unset($this->request->data);
							}
						}else{
							$this->Session->setFlash('Document file not uploded due to big size,please use small size document file.', 'default', array('class' => 'green'));
						}
					}
				}				
			}
		}
		
		public function contact() {
			
			$this->loadModel('Contact');
			$this->layout = "search";
			$title = "Fairpockets | Feel free to contact us";
			//print_r($this->request->data);die();
			if(!empty($this->request->data)){
				
				//print_r($this->request->data);die();
				if ($this->Contact->validates()) {//this ensures Model vaidation is applied along with error messages coming from model
					if ($this->Contact->save($this->request->data)) {				
						$this->Session->setFlash('Thanks for contacting Us, We will get back to you shortly.', 'default', array('class' => 'green'));

						//Sending email
						$message = 'There is a request from ' . $this->request->data['Contact']['name'];
						$message .= '<p>Email: ' . $this->request->data['Contact']['email'] . '</p>';
						$message .= '<p>Mobile: ' . $this->request->data['Contact']['mobile'] . '</p>';
						$message .= '<p>Subject: ' . $this->request->data['Contact']['subject'] . '</p>';
						$message .= '<p>Message: ' . $this->request->data['Contact']['message'] . '</p>';
                        
                        $Email = new CakeEmail();
                        $Email->config('smtp');
                        $Email->from(array('Support@FairPockets.com' => 'FairPockets Contact')); 
                        $Email->to('info@fairpockets.com');
                        $Email->emailFormat('html');
                        $Email->subject('Contact us request');
                        $Email->send($message);
						//End Sending email
						unset($this->request->data['Contact']);
					} 
				}
			}			
			$this->set('title', $title);
		}
		
		public function feedback() {
			//phpinfo();
			$this->layout = "search";
			$title = "Fairpockets | Share your valuable feedbacks with us";
			$this->set('title', $title);
			
			$this->loadModel('Feedback');
			
			if(!empty($this->request->data)){				
				//echo $target_dir; print_r($this->request->data);die();				
					//$this->request->data['Feedback']['name'] = $this->request->data['Feedbacks']['name'];
					//$this->request->data['Feedback']['email'] = $this->request->data['Feedbacks']['email'];
					//$this->request->data['Feedback']['feedback_comments'] = $this->request->data['Feedbacks']['feedback'];
					$this->request->data['Feedback']['status'] = 1;
					if ($this->Feedback->save($this->request->data)) {				
						$this->Session->setFlash('Thanks for Feedback, We will get back to you shortly.', 'default', array('class' => 'green'));
						unset($this->request->data);
				}				
			}
		}
		
		public function pap() {
			$this->layout = "home";
			$title = "Fairpockets | Privacy and Policy";
			$this->set('title', $title);
		}
		
		public function tac() {
			$this->layout = "home";
			$title = "Fairpockets | Terms & Conditions";
			$this->set('title', $title);
		}
		
		public function pricing() {
			$this->layout = "search";
			$title = "Fairpockets | Buy Our Service";
			$this->set('title', $title);
		}
		
		public function fp_pgform() {
			$this->layout = "account";
			$title = "Fairpockets | User Dashboard";
			$this->set('title', $title);
		}
		
		public function fp_pgsuccess() {
			$this->layout = "account";
			$title = "Fairpockets | User Dashboard";
			$this->set('title', $title);
		}
		
		public function fp_pgfailure() {
			$this->layout = "account";
			$title = "Fairpockets | User Dashboard";
			$this->set('title', $title);
		}
		
		public function getbspoptions() {
			$this->autoRender = false;
			//print_r($this->request->data);die();
			$this->loadModel('ProjectBspDetail');
			$bspoptionid = $this->request->data['id'];
			$projectid = $this->request->data['projectid'];
			$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$bspoptionid." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
			return $bsp_price[0]['project_bsp_details']['bsp_charges'];
			$this->layout = "ajax";
			exit();
		}
		
	}
