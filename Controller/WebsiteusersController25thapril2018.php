<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class WebsiteusersController extends AppController {

    public $uses = array('Websiteuser', 'Builder', 'Otp');

    public function beforeFilter() {
        parent::beforeFilter();

        $this->Auth->allow('index', 'register', 'forgotpass', 'emailVerify', 'checkField', 'sendOtp', 'sendOtpLeads', 'verifyOtp', 'sendemailtest');

        // Setting User Email to be used for search criteria in Auth Component
    }

    public function index() {
        $this->redirect('/');
    }

    // Function : Logout... and direct to homepage
    public function logout() {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }

    public function loggedout() {
        //do nothing
    }
    public function checkField() {
        header('Content-Type: application/json');
        if ($this->request->is('ajax')) {
            $data = $this->Websiteuser->find('first', [
                'conditions' => [
                    $this->request->query['data']['Websiteuser']
                ]
            ]);
            
            if($data) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        } else {
            echo json_encode(false);
        }
        
        die;
    }
    public function sendOtp() {
        $randNo = rand(1000, 9999);
        $mobileNo = $this->request->data['Websiteuser']['usermobile'];
        $milliseconds = round(microtime(true) * 1000) + (10 * 60 * 1000);
        $this->Otp->save([
            'Otp' => [ 'otp' => $randNo, 'expiry' => $milliseconds, 'id' => $mobileNo ]
        ]);
        if($this->Otp->getAffectedRows() == 1) {
            $this->sendSMS($mobileNo, $randNo);            
        }  else {
            echo json_encode(['message' => 'Invalid request!', 'type' => 'error']);
        }
        die;
    }
	
	public function sendOtpLeads() {
        $randNo = rand(1000, 9999);
		//echo '<pre>'; print_r($this->request->data);die();
        $mobileNo = $this->request->data['phone'];
        $milliseconds = round(microtime(true) * 1000) + (10 * 60 * 1000);
        $this->Otp->save([
            'Otp' => [ 'otp' => $randNo, 'expiry' => $milliseconds, 'id' => $mobileNo ]
        ]);
        if($this->Otp->getAffectedRows() == 1) {
            $this->sendSMS($mobileNo, $randNo);            
        }  else {
            echo json_encode(['message' => 'Invalid request!', 'type' => 'error']);
        }
        die;
    }

    public function sendSMS($mobileNo, $randNo) {



        $authKey = "178586A0aldZIg59dbed56";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $mobileNo;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "FPOCKE";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Your FairPockets' OTP is " . $randNo);

        //Define route 
        $route = "99";


        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        // $url="http://api.msg91.com/api/sendhttp.php";
        $url = 'https://control.msg91.com/api/sendotp.php?authkey='. $authKey .'&mobile='. $mobileNo .'&message=Your OTP is '. $randNo .'&sender='. $senderId .'&otp=' . $randNo;

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_POST => true,
            // CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            // echo 'error:' . curl_error($ch);
            return false;
        }

        curl_close($ch);

        // echo $output;
        return true;

    }

    // Function : Authenticate user for login
    public function login() {

        if ($this->request->is('ajax')) {
            if (!empty($this->data)) {
                if (!empty($this->request->data)) {
                    // Check if User data is valid for login
                    if ($this->Websiteuser->validates()) {
                        //print_r($this->request->data); 
                        //exit;
                        //$log = $this->User->getDataSource()->getLog(false, false);       
                        //debug($log);

                        if ($this->Auth->login()) {

                            // did they select the remember me checkbox?
                            if ($this->request->data['Websiteuser']['remember_me'] == 1) {


                                $this->Cookie->write('remember_me_user', $this->request->data['Websiteuser'], true, '2 weeks');
                            } else {
                                $this->Cookie->delete('remember_me_user');
                            }
                            // Authentication Successfull

                            $response = array();
                            $response['status'] = 'success';
                            $response['message'] = 'The User authentication is successful.';
                            echo json_encode($response);
                            die;
                        } else {
                            // Authentication Failed

                            $response = array();
                            $response['status'] = 'error';
                            $response['message'] = 'The User authentication has failed.';
                            echo json_encode($response);
                            die;
                        }
                    }
                }
            }
        } else {
            $this->redirect('/');
        }
    }

    public function verifyOtp() {
        header('Content-Type: application/json');
        //print_r($this->request->data);die();
        $otp = $this->Otp->find('first', [
            'conditions' => $this->request->data
        ]);
       // print_r($otp); die();
        if(!empty($otp)) {
            $milliseconds = round(microtime(true) * 1000);
            if($otp['Otp']['expiry'] > $milliseconds) {
                print json_encode([ 'type' => 'success' ]);
            } else {
                print json_encode([ 'type' => 'failure' ]);
            }
        } else {
            print json_encode([ 'type' => 'failure' ]);
        }
        exit;
    }

    // Function : 
	/*
	* Add user for registration
	* TODO : Send Mail to User on Successful registration
	* TODO : Generate TOKEN based on email to be used for first time login
	*/
    public function register() {

        if ($this->request->is('ajax')) {

            if (!empty($this->data)) {
                if (!empty($this->request->data)) {
                    // Check if User data is valid for registration
                    if ($this->Websiteuser->validates()) {
                        $this->request->data['Websiteuser']['regsitration_token'] = md5(date('Y-m-d H:i:s') . $this->request->data['Websiteuser']['email'] . $this->request->data['Websiteuser']['usermobile']);
                        //Save New User
                        if ($this->Websiteuser->save($this->request->data)) {
							//$this->sendemailtest($this->request->data);
                            $response = array();
                            $response['status'] = 'success';
                            $response['message'] = 'The User has been saved.';
                            $response['data'] = $this->data;
                            echo json_encode($response);
                            die;
							//$this->sendemailtest($this->request->data);
                        } else {
                            // Not able to save new user data

                            $response = array();
                            $User = $this->Websiteuser->invalidFields();
                            $response['status'] = 'error';
                            $response['message'] = 'The User could not be saved. Please, try again.';
                            $response['userData'] = compact('User');
                            echo json_encode($response);
                            die;
                        }
                    }
                }
            }
        }
    }
	
	/*
	* Send Email To User On Registration	
	*/
	
	public function sendemailtest(){
		if ($this->request->is('ajax')) {
			//$this->request->data['Websiteuser']['regsitration_token'] = $this->request->data['Websiteuser']['usermobile'];
			//echo '<pre>'; print_r($this->request->data);die();
			$message = 'Dear ' . $this->request->data['Websiteuser']['username'];
								$message .= '<p>Thank You for signing up with FairPockets, We bring your reality investment to life</p>';
								$message .= '<p>Please Click on following link to verify your email</p>';
								$message .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot . 'emailVerify/' . $this->request->data['Websiteuser']['usermobile'].'">Click to verify email</a></p>';
								$message .= '<br/>';			  
								$message .= '<p>Regards,</p>';
								$message .= '<p><strong>FairPockets Team<strong></p>';							
								
								$Email = new CakeEmail();
								$Email->config('smtp');
								$Email->to($this->request->data['Websiteuser']['email']);
								$Email->emailFormat('html');
								$Email->subject('Verify Your Email');
								$Email->send($message);
								die();
							
		}		
	}

	/*
	* Email verification
	* Update verify email data into database
	*/
    public function emailVerify($token) {
        $this->layout = "home";
        $get_token = $this->Websiteuser->find('all', array(
            'fields' => array(
                'id',
            ),
            'conditions' => array(
                'usermobile' => $token
            )
        ));
        if (!empty($get_token)) {
            $this->Websiteuser->query("UPDATE `websiteusers` SET `useractive` = '1', `email_verify`='1' WHERE `id` = " . $get_token['0']['Websiteuser']['id'] . "");
        } else {
            $this->redirect('/');
        }
    }
	
	/*
	* Forgot password
	* Sending Email to user
	* Todo : Send mail to user about new password
	* We don't need a view for this action
	*/
    public function forgotpass() {
        if ($this->request->is('ajax')) {
            if (!empty($this->data)) {
                if (!empty($this->request->data)) {
                    //Check if User data is valid
                    if ($this->Websiteuser->validates()) {
					    //Get the user email id for which the password needs to be reset
                        $user_email = $this->request->data['Websiteuser']['email'];
						//Get user account info from DB
                        $check_email = $this->Websiteuser->findByEmail($user_email);
						//Set & Send password only if user is active
                        if ((!empty($check_email)) && ( $check_email['Websiteuser']['useractive'] )) {
                            $this->Websiteuser->id = $check_email['Websiteuser']['id'];
                            $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
                            $new_password = '';
                            for ($i = 0; $i < 6; $i++) {
                                $new_password .= $characters[rand(0, strlen($characters) - 1)];
                            }
                            $this->request->data['Websiteuser']['password'] = $new_password;
						    $this->Websiteuser->save($this->request->data);
                            /* Sending Email to user */
                            $email = $check_email['Websiteuser']['email'];
							
                            $html = 'Hi ' . $check_email['Websiteuser']['username'];
							$html .= '<p>Welcome back to FairPockets,We bring your realty investment to life.</p>';
							$html .= '<p>Your Password has been reset Successfully</p>';
							$html .= '<p>Your Login id : ' . $check_email['Websiteuser']['email'].'</p>';
							$html .= '<p>Your Password is : ' . $new_password.'</p>';
							$html .= '<p>Please login using the following link :</p>';
							$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot.'">Login Now</a></p>';
							$html .= '<br/>';			  
							$html .= '<p>Regards,</p>';
							$html .= '<p><strong>FairPockets Team<strong></p>';
							
                            $Email = new CakeEmail();
                            $Email->config('smtp');
							$Email->emailFormat('html');
                            /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
                            $Email->to($email);
                            $Email->subject('Change your Password-FairPockets');

                            $output = $Email->send($html);
                            if ($output) {
                                // Password set
                                $response = array();
                                $response['status'] = 'success';
                                $response['message'] = 'Password Reset.';
                                echo json_encode($response);
                                die;
                            } else {
                                // Not able to set password	
                                $response = array();
                                $response['status'] = 'success';
                                $response['message'] = 'Unable to reset.';
                                echo json_encode($response);
                                die;
                            }
                        } else {
                            // User not found 
                            $response = array();
                            $response['status'] = 'error';
                            $response['message'] = 'This user does not exists in our records.';
                            echo json_encode($response);
                            die;
                        }
                    }else{
						$response['status'] = 'error';
						$response['message'] = 'This user does not exists in our records.';
						echo json_encode($response);
						die;
					}
                }
            }
        }
    }

	/*
	* Change password
	* Sending Email to user
	* Todo : Send mail to user about new password
	* We don't need a view for this action
	*/
    public function changePassword() {

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            $this->set('status', $builder['0']['Builder']['status']);
        }
        $this->layout = "account";
        $user_id = $this->Session->read('Auth.Websiteuser.id');
        if ($this->data) {
            if (!empty($this->data['Websiteuser']['password']) && !empty($this->data['Websiteuser']['repassword'])) {
                if ($this->data['Websiteuser']['password'] == $this->data['Websiteuser']['repassword']) {
                    $current_passwrod = $this->Websiteuser->currentpassword($user_id);

                    if ($this->Websiteuser->oldPassword($this->data['Websiteuser']['old_password']) == $current_passwrod) {
                        $this->Websiteuser->id = $user_id;

                        if ($this->Websiteuser->save($this->request->data)) {
                            $this->Session->setFlash('Password Changed Successfully', 'default', array('class' => 'green'));
                            unset($this->request->data['Websiteuser']);
                            $this->redirect('/websiteusers/changePassword');
                        }
                    } else {
                        $this->Session->setFlash('Old password is incorrect', 'default', array('class' => 'red'));
                        $this->redirect('/websiteusers/changePassword');
                    }
                } else {
                    $this->Session->setFlash('Password and Re password does not match', 'default', array('class' => 'red'));
                    $this->redirect('/websiteusers/changePassword');
                }
            } else {
                $this->Session->setFlash('Password and Re password cannot be empty', 'default', array('class' => 'red'));
                $this->redirect('/websiteusers/changePassword');
            }
        }
    }

}

?>