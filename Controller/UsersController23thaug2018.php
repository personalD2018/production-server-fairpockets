<?php

// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    public $uses = array('MstEmployeeDepartment', 'MstEmployeeRole', 'User', 'AreaCountryMaster', 'AreaRegionMaster', 'AreaStateMaster', 'AreaCityMaster', 'AreaLocalMaster', 'PropertyNotifiction', 'BuilderApprovalNotifiction', 'AssignCityMaster', 'AssignCountryMaster', 'AssignLocalMaster', 'AssignRegionMaster', 'AssignStateMaster', 'ServiceNotifiction');
	var $helpers = array("Number");
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout', 'admin_forget_password','viewCareerLeads','viewUsers');
    }

    public function admin_index() {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        $user = $this->User->find('all', array(
            'conditions' => array(
                'User.employee_role_id !=' => '0'
            )
        ));
        $this->set('users', $user);
    }
	
	public function admin_dashboard() {
		
		$this->loadModel('Websiteuser');
		$this->loadModel('Property');
		$this->loadModel('Project');
	
	$total_user = $this->Websiteuser->find('count'); //print_r($total_user);
	$total_property = $this->Property->find('count');
	$total_projects = $this->Project->find('count');
	$this->set('total_user', $total_user);
	$this->set('total_property', $total_property);
	$this->set('total_projects', $total_projects);
	}
	// Code For Contact Page Leads frontend Start //
	
	public function admin_viewContactLeads() {
		$this->loadModel('Contact');
        $contactData = $this->Contact->find('all');
        $this->set('contactData', $contactData);
    }
	
	public function admin_deleteContact($id) {
		$this->loadModel('Contact');
        $this->Contact->delete($id);
		$this->Session->setFlash('Lead has been deleted Succesfully!!', 'default', array('class' => 'green'));
        return $this->redirect(array('action' => 'viewContactLeads'));
		die();
    }
	
	// Code For Contact Page Leads frontend End //
	
	
	// Code For Feedback Page Leads frontend Start //
	
	public function admin_viewFeedbackLeads() {
		$this->loadModel('Feedback');
        $feedbackData = $this->Feedback->find('all');
        $this->set('feedbackData', $feedbackData);
    }
	
	public function admin_deleteFeedback($id) {
		$this->loadModel('Feedback');
        $this->Feedback->delete($id);
		$this->Session->setFlash('Lead has been deleted Succesfully!!', 'default', array('class' => 'green'));
        return $this->redirect(array('action' => 'viewFeedbackLeads'));
		die();
    }
	
	// Code For Feedback Page Leads frontend End //
	
	// Code For Career Page Leads frontend Start //
	
	public function admin_viewCareerLeads() {
		Configure::read('debug', 2);
		$this->loadModel('Career');
        $careerData = $this->Career->find('all');
        $this->set('careerData', $careerData);
    }

	public function admin_deleteCareer($id) {
		$this->loadModel('Career');
        $this->Career->delete($id);
		$this->Session->setFlash('Lead has been deleted Succesfully!!', 'default', array('class' => 'green'));
        return $this->redirect(array('action' => 'viewCareerLeads'));
		die();
    }
	
	// Code For Career Page Leads frontend End //
	
	// Code For Builder Leads frontend Start //
	
	public function admin_viewBuildersLeads() {
		$this->loadModel('Lead');
        $leadData = $this->Lead->find('all');
        $this->set('leadData', $leadData);
    }
	
	public function admin_deleteLead($id) {
		$this->loadModel('Lead');
        $this->Lead->delete($id);
		$this->Session->setFlash('Lead has been deleted Succesfully!!', 'default', array('class' => 'green'));
        return $this->redirect(array('action' => 'viewBuildersLeads'));
		die();
    }
	
	// Code For Builder Leads frontend End //

    public function admin_login() {


        $this->layout = 'login';
        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                if ($this->Auth->user('is_valid') == 0) {
                    $this->Session->setFlash('User doesnot exists', 'default', array('class' => 'red'));

                    return $this->redirect($this->Auth->logout());
                }

                // did they select the remember me checkbox?
                if ($this->request->data['User']['remember_me'] == 1) {

                    $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                    // hash the user's password
                    $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                    // write the cookie
                } else {
                    $this->Cookie->delete('remember_me_cookie');
                }
                return $this->redirect('dashboard');
            } else {
                $this->Session->setFlash('Username or password is incorrect', 'default', array('class' => 'red'));
            }
        }
        $this->set('user_rember_data', $this->Cookie->read('remember_me_cookie'));
        $this->set(array(
            'title_for_layout' => 'Login'
        ));
    }

    public function admin_logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function admin_viewemployee($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $profile_data = $this->User->findById($id);
        $this->set('profile_data', $profile_data);

        if ($profile_data['EmployeeRole']['id'] == '1' && $profile_data['MstEmployeeDepartment']['id'] == '1') {
            $field_name = 'property_executive';
        }
        if ($profile_data['EmployeeRole']['id'] > '1' && $profile_data['MstEmployeeDepartment']['id'] == '1') {

            $field_name = 'property_manager';
        }

        if ($profile_data['EmployeeRole']['id'] == '1' && $profile_data['MstEmployeeDepartment']['id'] == '2') {
            $field_name = 'brokerage_executive';
        }
        if ($profile_data['EmployeeRole']['id'] > '1' && $profile_data['MstEmployeeDepartment']['id'] == '2') {

            $field_name = 'brokerage_manager';
        }

        if ($profile_data['EmployeeRole']['id'] == '1' && $profile_data['MstEmployeeDepartment']['id'] == '3') {
            $field_name = 'research_executive';
        }
        if ($profile_data['EmployeeRole']['id'] > '1' && $profile_data['MstEmployeeDepartment']['id'] == '3') {

            $field_name = 'research_manager';
        }
        if ($profile_data['EmployeeRole']['id'] == '1' && $profile_data['MstEmployeeDepartment']['id'] == '4') {
            $field_name = 'portfolio_executive';
        }
        if ($profile_data['EmployeeRole']['id'] > '1' && $profile_data['MstEmployeeDepartment']['id'] == '4') {

            $field_name = 'portfolio_manager';
        }

        $countryAreauser = $this->AssignCountryMaster->find('all', array(
            'conditions' => array(
                'AssignCountryMaster.' . $field_name => $id
            )
        ));
        $this->set('countryarea', $countryAreauser);


        $regionAreauser = $this->AssignRegionMaster->find('all', array(
            'conditions' => array(
                'AssignRegionMaster.' . $field_name => $id
            )
        ));

        if (!empty($regionAreauser)) {
            foreach ($regionAreauser as $regionMaster) {

                $CountryMaster = $this->AreaCountryMaster->find('all', array(
                    'fields' => array(
                        'countryName'
                    ),
                    'conditions' => array(
                        'countryCode' => $regionMaster['AreaRegionMaster']['countryCode']
                    )
                ));

                $regionarea[] = array_merge($CountryMaster[0], $regionMaster);
            }
            $this->set('regionarea', $regionarea);
        }





        $AreaStateMaster = $this->AssignStateMaster->find('all', array(
            'conditions' => array(
                'AssignStateMaster.' . $field_name => $id
            )
        ));


        if (!empty($AreaStateMaster)) {
            foreach ($AreaStateMaster as $StateMaster) {

                $stateMasterda = $this->AreaStateMaster->find('all', array(
                    'conditions' => array(
                        'AreaStateMaster.stateCode' => $StateMaster['AssignStateMaster']['stateCode']
                    )
                ));

                $CountryMasterdata = $this->AreaCountryMaster->find('all', array(
                    'conditions' => array(
                        'AreaCountryMaster.countryCode' => $stateMasterda['0']['AreaRegionMaster']['countryCode']
                    )
                ));

                $stateareadata[] = array_merge($CountryMasterdata[0], $stateMasterda[0]);
            }


            $this->set('stateareadata', $stateareadata);
        }


        $AreaCityMaster = $this->AssignCityMaster->find('all', array(
            'conditions' => array(
                'AssignCityMaster.' . $field_name => $id
            )
        ));

        if (!empty($AreaCityMaster)) {

            foreach ($AreaCityMaster as $CityMaster) {

                $cityMaster_data = $this->AreaCityMaster->find('all', array(
                    'conditions' => array(
                        'AreaCityMaster.citycode' => $CityMaster['AssignCityMaster']['citycode']
                    )
                ));

                $AreaStateMasterData = $this->AreaStateMaster->find('all', array(
                    'conditions' => array(
                        'AreaStateMaster.statecode' => $cityMaster_data['0']['AreaCityMaster']['statecode']
                    )
                ));
                $AreaCountryMasterData = $this->AreaCountryMaster->find('all', array(
                    'conditions' => array(
                        'AreaCountryMaster.countryCode' => $AreaStateMasterData['0']['AreaRegionMaster']['countryCode']
                    )
                ));
                $city_area[] = array_merge($cityMaster_data['0'], $AreaStateMasterData['0'], $AreaCountryMasterData['0']);
            }

            $this->set('city_area', $city_area);
        }


        $AreaLocalMaster = $this->AssignLocalMaster->find('all', array(
            'conditions' => array(
                'AssignLocalMaster.' . $field_name => $id
            )
        ));

        if (!empty($AreaLocalMaster)) {

            foreach ($AreaLocalMaster as $LocalMaster) {

                $localdata[] = $this->AreaLocalMaster->find('all', array(
                    'conditions' => array(
                        'AreaLocalMaster.localityCode' => $LocalMaster['AssignLocalMaster']['localityCode']
                    )
                ));
            }

            foreach ($localdata as $localmasdata) {

                $statenamedata = $this->AreaStateMaster->find('all', array(
                    'conditions' => array(
                        'AreaStateMaster.stateCode' => $localmasdata['0']['AreaCityMaster']['statecode']
                    )
                ));

                $country = $this->AreaCountryMaster->find('all', array(
                    'conditions' => array(
                        'AreaCountryMaster.countryCode' => $statenamedata['0']['AreaRegionMaster']['countryCode']
                    )
                ));

                $locstarea[] = array_merge($localmasdata[0], $statenamedata[0], $country[0]);
            }


            $this->set('local_area', $locstarea);
        }
    }

    public function admin_editEmployee($id = null) {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }
        $target_dir = 'upload/profile_pic/';
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $name = $this->request->data ['User']['profile_pic']['name'];

            if (!empty($name)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['User']['profile_pic']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['User']['profile_pic'] = $newfilename;
                }
            } else {
                $this->request->data ['User']['profile_pic'] = @$this->request->data ['User']['profile_pic_edit'];
            }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('The user has been Updated', 'default', array('class' => 'green'));
            }
        } else {
            $this->request->data = $this->User->findById($id);

            unset($this->request->data['User']['password']);
        }

        $employeedepartment = $this->MstEmployeeDepartment->find('list', array(
            'fields' => array(
                'id',
                'departmentname',
            ),
            'conditions' => array(
                'status' => 1
            )
        ));

        $employeerole = $this->EmployeeRole->find('list', array(
            'fields' => array(
                'id',
                'role',
            ),
            'conditions' => array(
                'id !=' => '0'
            )
        ));

        $manager = $this->User->find('list', array(
            'fields' => array(
                'id',
                'username',
            ),
            'conditions' => array(
                'employee_department_id' => $this->request->data['User']['employee_department_id'],
                'employee_role_id  >' => $this->request->data['User']['employee_role_id']
            )
        ));
        if (empty($manager)) {
            $manager = array(0 => 'No Manger');
        }
        $this->set('manager', $manager);
        $this->set('employeedepartment', $employeedepartment);
        $this->set('employeerole', $employeerole);
    }

    public function admin_deleteemployee($id = null) {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User Deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_addUsers() {
		$role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }
		
        if ($this->request->is('post')) {
            //echo '<pre>';print_r($this->request->data);die();
            $this->loadModel('Websiteuser');
            if ($this->Websiteuser->validates()) {
                $this->request->data['Websiteuser']['regsitration_token'] = md5(date('Y-m-d H:i:s') . $this->request->data['Websiteuser']['email'] . $this->request->data['Websiteuser']['username']);
                $this->request->data['Websiteuser']['useractive'] = 1;
                $this->request->data['Websiteuser']['email_verify'] = 1;
                $this->request->data['Websiteuser']['otp_verify'] = 1;
                //echo '<pre>';print_r($this->request->data);die();
                if ($this->Websiteuser->save($this->request->data)) {
                    $this->Session->setFlash('The user has been saved Successfully!!!', 'default', array('class' => 'green'));
                    unset($this->request->data);

                }else{
                    $this->Session->setFlash('Please try Again.', 'default', array('class' => 'green'));
                    unset($this->request->data);


                }
            }
        }

    }
	
	public function admin_viewUsers() {
		Configure::read('debug', 2);
		$this->loadModel('Websiteuser');
        $usersData = $this->Websiteuser->find('all');
        $this->set('usersData', $usersData);
    }

    public function admin_addEmployee() {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }
        $target_dir = 'upload/profile_pic/';
        $employeedepartment = $this->MstEmployeeDepartment->find('list', array(
            'fields' => array(
                'id',
                'departmentname',
            ),
            'conditions' => array(
                'status' => 1
            )
        ));

        $employeerole = $this->MstEmployeeRole->find('list', array(
            'fields' => array(
                'id',
                'role',
            ),
            'conditions' => array(
                'id !=' => '0'
            )
        ));

        if ($this->request->is('post')) {


            if (!empty($this->request->data ['User']['profile_pic']['name'])) {

                $name = $this->request->data ['User']['profile_pic']['name'];
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['User']['profile_pic']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['User']['profile_pic'] = $newfilename;
                }
            } else {
                $this->request->data ['User']['profile_pic'] = '';
            }
            if ($this->User->save($this->request->data)) {

				$html = 'Dear ' . $this->request->data ['User']['username'] ;
				$html .= '<p>We welcome you to Fair Pockets.</p>';
				$html .= '<p>Your Fair Pockets Employee number is :'.$this->request->data['User']['empId'].'</p>';
				$html .= '<p>Your Password is : ' . $this->request->data ['User']['password'] . '</p>';
				$html .= '<p>Your Password is : ' . $new_password.'</p>';
				$html .= '<p>Please login using the following link :</p>';
				$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot.'admin/">Login Now</a></p>';
				$html .= '<br/>';			  
				$html .= '<p>Regards,</p>';
				$html .= '<p><strong>FairPockets Team<strong></p>';
				
                $Email = new CakeEmail();
                $Email->config('smtp');
				$Email->emailFormat('html');
                /* $Email->from(array('admin@FairPockets.com' => 'FairPockets')); */
                $Email->to($this->request->data ['User']['email']);
                $Email->subject('Welcome to FairPockets');
                $Email->send($html);
                $this->Session->setFlash('The user has been saved Successfully!!!', 'default', array('class' => 'green'));

                unset($this->request->data);
            }
        }
        $this->set('employeedepartment', $employeedepartment);
        $this->set('employeerole', $employeerole);
    }

    public function admin_getManger($departmentId, $roleId) {
        if (!empty($this->request->data['department']) && $this->request->data['department'] == '4') {
            $this->request->data['department'] = 3;
        }
        $manager = $this->User->find('list', array(
            'fields' => array(
                'id',
                'username',
            ),
            'conditions' => array(
                'employee_department_id' => $departmentId,
                'employee_role_id  >' => $roleId
            )
        ));
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $this->response->body(json_encode($manager));
        return $this->response;
    }

    public function admin_myteam() {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id == 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        $user_id = $this->Session->read('Auth.User.id');

        $employee = $this->User->children($user_id);
        $myteam = array();
        foreach ($employee as $employeedata) {

            $myteam[] = $this->User->findById($employeedata['User']['id']);
        }

        $this->set('myteam', $myteam);
    }

    public function admin_profile() {
        $profile_id = $this->Session->read('Auth.User.id');

        $target_dir = 'upload/profile_pic/';
        $this->User->id = $profile_id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $name = $this->request->data ['User']['profile_pic']['name'];

            if (!empty($name)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['User']['profile_pic']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['User']['profile_pic'] = $newfilename;
                }
            } else {
                $this->request->data ['User']['profile_pic'] = $this->request->data ['User']['profile_pic_edit'];
            }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Your Profile is Updated successfully!!!', 'default', array('class' => 'green'));

                return $this->redirect(array('action' => 'profile'));
            }
        } else {
            $this->request->data = $this->User->findById($profile_id);
        }
        $profile_data = $this->User->findById($profile_id);
        $this->set('profile_data', $profile_data);
    }

    public function admin_reset() {

        $user_id = $this->Session->read('Auth.User.id');
        if ($this->data) {
            if (!empty($this->data['User']['password']) && !empty($this->data['User']['repassword'])) {
                if ($this->data['User']['password'] == $this->data['User']['repassword']) {
                    $current_passwrod = $this->User->currentpassword($user_id);

                    if ($this->User->oldPassword($this->data['User']['old_password']) == $current_passwrod) {
                        $this->User->id = $user_id;

                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash('Password Changed Successfully', 'default', array('class' => 'green'));
                            unset($this->request->data['User']);
                            $this->redirect('/admin/Users/reset');
                        }
                    } else {
                        $this->Session->setFlash('Unable to save password', 'default', array('class' => 'red'));
                        $this->redirect('/admin/Users/reset');
                    }
                } else {
                    $this->Session->setFlash('Password and Re password does not match', 'default', array('class' => 'red'));
                    $this->redirect('/admin/Users/reset');
                }
            } else {
                $this->Session->setFlash('Password and Re password cannot be empty', 'default', array('class' => 'red'));
                $this->redirect('/admin/Users/reset');
            }
        }
    }

    public function admin_unread_notification() {

        $this->PropertyNotifiction->query("Delete from `property_notifictions` WHERE `user_id` = " . $this->Session->read('Auth.User.id') . "");
        $this->BuilderApprovalNotifiction->query("Delete from `builder_approval_notifictions` WHERE `user_id` = " . $this->Session->read('Auth.User.id') . "");
        $this->ServiceNotifiction->query("Delete from `service_notifictions` WHERE `user_id` = " . $this->Session->read('Auth.User.id') . "");
        die;
    }

    public function admin_forget_password() {

        if ($this->request->is('post')) {

            $user_data = $this->request->data;
            if (!empty($user_data)) {
                $this->User->recursive = -1;
                if (empty($user_data['User']['email'])) {
                    $this->Session->setFlash('Please insert email ID', 'default', array('class' => 'red'));
                    $this->redirect('/admin/Users/login');
                }
                $check_email = $this->User->find('first', array('conditions' => array('User.email' => $user_data['User']['email'])));

                if (!empty($check_email)) {
                    $this->User->id = $check_email['User']['id'];
                    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $new_password = '';
                    for ($i = 0; $i < 6; $i++) {
                        $new_password .= $characters[rand(0, strlen($characters) - 1)];
                    }
                    $this->request->data['User']['password'] = $new_password;

                    $this->User->save($this->request->data);
                    /* Sending Email to user */
                    $email = $user_data['User']['email'];
					
					$html = 'Hi,';
					$html .= '<p>Your Password has been reset Successfully</p>';
					$html .= '<p>Your Login id : ' . $user_data['User']['email'] . '</p>';
					$html .= '<p>Your Password is : ' . $new_password . '</p>';
					$html .= '<p>Please login using the following link :</p>';
					$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot.'admin/">Login Now</a></p>';
					$html .= '<br/>';			  
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
			
                    $Email = new CakeEmail();
                    $Email->config('smtp');
					$Email->emailFormat('html');
                    /* $Email->from(array('admin@FairPockets.com' => 'FairPockets')); */
                    $Email->to($email);
                    $Email->subject('Forgot Password-FairPockets');
                    $output = $Email->send($html);

                    /* Sending Email to user */
                    if ($output) {
                        $this->Session->setFlash('Password has been changed, Check Your Mail', 'default', array('class' => 'green'));
                        $this->redirect(array('controller' => 'users', 'action' => 'login'));
                        //echo json_encode(array('status' => 'success', 'message' => "Password has been changed , please check your email")); die;
                    } else {
                        $this->Session->setFlash('Password has been changed ', 'default', array('class' => 'green'));
                        $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                } else {
                    $this->Session->setFlash('Email Not Exist', 'default', array('class' => 'red'));
                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            }
        }
    }
	
	public function admin_viewBuilders() {
		$this->loadModel('Websiteuser');		
		$usersData = $this->Websiteuser->find('all', array(           
            'conditions' => array(
                'userrole' => '3'
            )
        ));
        $this->set('usersData', $usersData);
    }
	
	
	public function admin_builderApproveAction($id) {
        if(!empty($id)) { 
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->loadModel('Websiteuser');
				$this->Websiteuser->updateAll(
					array(
						'useractive'=>$this->request->data['Websiteuser']['useractive']
						
						),
					array(
						'id'=>$id
					));
					$this->Session->setFlash('Builder approved Succesfully!!', 'default', array('class' => 'green'));
				return $this->redirect(array('action' => 'viewBuilders'));
			} 
        } 
    }

}

?>