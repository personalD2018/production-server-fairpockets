<?php

// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class AreasController extends AppController {

    public $uses = array(
        'MstAreaCountry', 
        'MstAreaRegion', 
        'MstAreaState', 
        'MstAreaCity', 
        'MstAreaLocality',
        'User', 
        'MstEmployeeDepartment', 
        'MstEmployeeRole', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState'
        
    );
    public function beforeFilter() {
        parent::beforeFilter();
        // $this->Auth->allow('getAllAreaCountries');
    }

    public function admin_getMstAreaCountry() {
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $body = json_encode($this->MstAreaCountry->find('all'));
        $this->response->body($body);
        return $this->response;
    }

    public function admin_getAllAreaRegionsByCountry($countryCode = null) {
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $result = [];
        $sql  = 'SELECT * from mst_area_regions as `MstAreaRegion` ';
        $sql .= 'JOIN `mst_area_countries` as `MstAreaCountry` ';
        $sql .= 'ON `MstAreaRegion`.`countryCode` = `MstAreaCountry`.`countryCode` ';
        if($countryCode != NULL)
            $sql .= 'WHERE `MstAreaRegion`.`countryCode` = ?';
        
        $result = $this->MstAreaRegion->query($sql, [ $countryCode ]);
        $body = json_encode($result);
        $this->response->body($body);
        return $this->response;
    }

    public function admin_getAllAreaStatesByRegion($regionCode = NULL) {
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $result = [];
        $sql  = 'SELECT * from mst_area_states as `MstAreaState` ';
        $sql .= 'JOIN `mst_area_regions` as `MstAreaRegion` ';
        $sql .= 'ON `MstAreaState`.`regionCode` = `MstAreaRegion`.`regionCode` ';
        $sql .= 'JOIN `mst_area_countries` as `MstAreaCountry` ';
        $sql .= 'ON `MstAreaRegion`.`countryCode` = `MstAreaCountry`.`countryCode` ';
        if($regionCode != NULL)
            $sql .= 'WHERE `MstAreaState`.`regionCode` = ?';
        $result = $this->MstAreaState->query($sql, [ $regionCode ]);
        $body = json_encode($result);
        $this->response->body($body);
        return $this->response;
    }

    public function admin_getAllAreaCitiesByState($stateCode = NULL) {
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $result = [];
        $sql  = 'SELECT * from mst_area_cities as `MstAreaCity` ';
        $sql .= 'JOIN `mst_area_states`          as `MstAreaState` ';        
        $sql .= 'JOIN `mst_area_regions`         as `MstAreaRegion` ';
        $sql .= 'JOIN `mst_area_countries`       as `MstAreaCountry` ';
        $sql .= 'ON ';
        $sql .= '`MstAreaCity`.`stateCode`       =  `MstAreaState`.`stateCode`     AND ';
        $sql .= '`MstAreaState`.`regionCode`     =  `MstAreaRegion`.`regionCode`   AND ';
        $sql .= '`MstAreaRegion`.`countryCode`   =  `MstAreaCountry`.`countryCode` ';
        if($stateCode != NULL)
            $sql .= 'WHERE `MstAreaCity`.`stateCode` = ?';
        $result = $this->MstAreaCity->query($sql, [ $stateCode ]);
        $body = json_encode($result);
        $this->response->body($body);
        return $this->response;
    }

    public function admin_getAllAreaLocalitiesByCity($cityCode = NULL) {
        $this->response->header([
            'Content-Type' => 'application/json'
        ]);
        $result = [];
        $sql  = 'SELECT * from  mst_area_localities as `MstAreaLocality` ';
        $sql .= 'JOIN `mst_area_cities`             as `MstAreaCity` ';        
        $sql .= 'JOIN `mst_area_states`             as `MstAreaState` ';        
        $sql .= 'JOIN `mst_area_regions`            as `MstAreaRegion` ';
        $sql .= 'JOIN `mst_area_countries`          as `MstAreaCountry` ON ';
        $sql .= '`MstAreaLocality`.`cityCode`    =  `MstAreaCity`.`cityCode` AND ';
        $sql .= '`MstAreaCity`.`stateCode`       =  `MstAreaState`.`stateCode` AND ';
        $sql .= '`MstAreaState`.`regionCode`     =  `MstAreaRegion`.`regionCode` AND ';
        $sql .= '`MstAreaRegion`.`countryCode`   =  `MstAreaCountry`.`countryCode` ';
        if($cityCode != NULL)
            $sql .= 'WHERE `MstAreaLocality`.`cityCode` = ?';
        $result = $this->MstAreaLocality->query($sql, [ $cityCode ]);
        $body = json_encode($result);
        $this->response->body($body);
        return $this->response;
    }

    public function admin_assignArea($id = null) {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $id     = $this->request->data['Area']['username'];
            $role   = $this->request->data['Area']['employee_role_id'];
            $userId = $this->request->data['Area']['username'];
            if (!empty($this->request->data['Area']['local_id'])) {
                
                // $user_id = '';
                $data = [];
                switch($this->request->data['Area']['employee_department_id']) {
                    case '1':                        
                        if($role == '1') {
                            // $user_id .= 'property_executive = ';
                            $data['property_executive'] = $this->request->data['Area']['username'];
                        } elseif ($role > '1') {
                            // $user_id = 'property_manager = ';
                            $data['property_manager'] = $this->request->data['Area']['username'];
                        }
                        break;
                    case '2':
                        if($role == '1') {
                            // $user_id .= 'brokerage_executive = ';
                            $data['brokerage_executive'] = $this->request->data['Area']['username'];
                        } elseif ($role > '1') {
                            // $user_id = 'brokerage_manager = ';
                            $data['brokerage_manager'] = $this->request->data['Area']['username'];
                        }
                        break;
                    case '3':
                        if($role == '1') {
                            // $user_id .= 'research_executive = ';
                            $data['research_executive'] = $this->request->data['Area']['username'];
                        } elseif ($role > '1') {
                            // $user_id .= 'research_manager = ';
                            $data['research_manager'] = $this->request->data['Area']['username'];
                        }
                        break;
                    case '4':
                        if($role == '1') {
                            // $user_id .= 'portfolio_executive = ';
                            $data['portfolio_executive'] = $this->request->data['Area']['username'];
                        } elseif ($role > '1') {
                            // $user_id .= 'portfolio_manager = ';
                            $data['portfolio_manager'] = $this->request->data['Area']['username'];
                        }
                        break;
                }
                $locality = $this->MstAssignLocality->find('first', [
                    'conditions' => $data
                ]);
                
                if(empty($locality)) {
                    $data = [ 'localityCode' => $this->request->data['Area']['local_id'] ];
                    $this->MstAssignLocality->save([
                        'MstAssignLocality' => $data
                    ]);    
                } else {
                    $locality['MstAssignLocality']['localityCode'] = $this->request->data['Area']['local_id'];
                    $this->MstAssignLocality->save($locality);
                }
            } else if (!empty($this->request->data['Area']['city_id'])) {
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '1') {
                    $user_id = 'property_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '1') {

                    $user_id = 'property_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '2') {
                    $user_id = 'brokerage_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '2') {

                    $user_id = 'brokerage_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '3') {
                    $user_id = 'research_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '3') {

                    $user_id = 'research_manager =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '4') {
                    $user_id = 'portfolio_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '4') {

                    $user_id = 'portfolio_manager =' . $this->request->data['Area']['username'];
                }

                $get_city = $this->MstAreaCity->query("Select * from mst_assign_cities WHERE citycode = " . $this->request->data['Area']['city_id'] . "");
                if (count($get_city) > 0) {
                    $this->MstAssignCity->query("UPDATE mst_assign_cities SET " . $user_id . " WHERE citycode = " . $this->request->data['Area']['city_id'] . "");
                } else {
                    $this->MstAssignCity->query("insert into mst_assign_cities SET " . $user_id . ", citycode = " . $this->request->data['Area']['city_id'] . "");
                }
            } else if (!empty($this->request->data['Area']['state_id'])) {
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '1') {
                    $user_id = 'property_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '1') {

                    $user_id = 'property_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '2') {
                    $user_id = 'brokerage_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '2') {

                    $user_id = 'brokerage_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '3') {
                    $user_id = 'research_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '3') {

                    $user_id = 'research_manager =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '4') {
                    $user_id = 'portfolio_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '4') {

                    $user_id = 'portfolio_manager =' . $this->request->data['Area']['username'];
                }

                $get_state = $this->MstAssignState->query("Select * from mst_assign_states WHERE stateCode = " . $this->request->data['Area']['state_id'] . "");
                if (count($get_state) > 0) {
                    $this->MstAssignState->query("UPDATE mst_assign_states SET " . $user_id . " WHERE stateCode = " . $this->request->data['Area']['state_id'] . "");
                } else {
                    $this->MstAssignState->query("insert into mst_assign_states SET " . $user_id . ", stateCode = " . $this->request->data['Area']['state_id'] . "");
                }
            } else if (!empty($this->request->data['Area']['region_id'])) {
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '1') {
                    $user_id = 'property_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '1') {

                    $user_id = 'property_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '2') {
                    $user_id = 'brokerage_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '2') {

                    $user_id = 'brokerage_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '3') {
                    $user_id = 'research_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '3') {

                    $user_id = 'research_manager =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '4') {
                    $user_id = 'portfolio_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '4') {

                    $user_id = 'portfolio_manager =' . $this->request->data['Area']['username'];
                }

                $get_region = $this->MstAssignRegion->query("Select * from mst_assign_regions WHERE RegionCode = " . $this->request->data['Area']['region_id'] . "");
                if (count($get_region) > 0) {
                    $this->MstAssignRegion->query("UPDATE mst_assign_regions SET " . $user_id . " WHERE RegionCode = " . $this->request->data['Area']['region_id'] . "");
                } else {
                    $this->MstAssignRegion->query("insert into mst_assign_regions SET " . $user_id . ", RegionCode = " . $this->request->data['Area']['region_id'] . "");
                }
            } else {

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '1') {
                    $user_id = 'property_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '1') {

                    $user_id = 'property_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '2') {
                    $user_id = 'brokerage_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '2') {

                    $user_id = 'brokerage_manager =' . $this->request->data['Area']['username'];
                }

                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '3') {
                    $user_id = 'research_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '3') {

                    $user_id = 'research_manager =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] == '1' && $this->request->data['Area']['employee_department_id'] == '4') {
                    $user_id = 'portfolio_executive =' . $this->request->data['Area']['username'];
                }
                if ($this->request->data['Area']['employee_role_id'] > '1' && $this->request->data['Area']['employee_department_id'] == '4') {

                    $user_id = 'portfolio_manager =' . $this->request->data['Area']['username'];
                }

                $get_country = $this->MstAssignCountry->query("Select * from mst_assign_countries WHERE countryCode = " . $this->request->data['Area']['country_id'] . "");
                if (count($get_country) > 0) {
                    $this->MstAssignCountry->query("UPDATE mst_assign_countries SET " . $user_id . " WHERE countryCode = " . $this->request->data['Area']['country_id'] . "");
                } else {
                    $this->MstAssignCountry->query("insert into mst_assign_countries SET " . $user_id . ", countryCode = " . $this->request->data['Area']['country_id'] . "");
                }
            }
            unset($this->request->data['Area']['region_id']);
            $this->Flash->success(__('Area has been assigned Successfully!'), array('class' => 'message'));
        }
        $user_list = $this->User->find('list', array(
            'fields' => array(
                'id',
                'username'
            ),
            'conditions' => array(
                'is_valid' => 1,
                'employee_role_id !=' => '0'
            )
        ));
        $this->set('user_list', $user_list);
        $region = $this->MstAreaRegion->find('list', array(
            'fields' => array(
                'regionCode',
                'regionName',
        )));
        $this->set('region', $region);

        $employeedepartment = $this->MstEmployeeDepartment->find('list', array(
            'fields' => array(
                'id',
                'departmentname',
            ),
            'conditions' => array(
                'status' => 1
            )
        ));

        $employeerole = $this->MstEmployeeRole->find('list', array(
            'fields' => array(
                'id',
                'role',
            ),
            'conditions' => array(
                'id !=' => '0'
            )
        ));
        $this->set('employeedepartment', $employeedepartment);
        $this->set('employeerole', $employeerole);
        $this->set('countryList', $this->MstAreaCountry->find('list', [
            'fields' => [
                'MstAreaCountry.countryCode','MstAreaCountry.countryName'
            ]
        ]));

    }

    public function admin_getuser() {
        $useroption = array();

        $useroption = $this->User->find('list', array(
            'fields' => array(
                'id',
                'username',
            ),
            'conditions' => array(
                'employee_department_id' => $this->request->data['department'],
                'employee_role_id' => $this->request->data['role']
            )
        ));


        header('Content-Type: application/json');
        echo json_encode($useroption);
        exit();
    }

}

?>