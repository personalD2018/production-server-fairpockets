<?php
App::uses('Component', 'Controller');

class PropertydataComponent extends Component {

public function getPropertyFeatures($id) {

	$model = ClassRegistry::init('PropertyFeature');

	$property_features = $model->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
        ));
        return  $property_features;	
	}

	public function getPropertyAmeities($id) {

	$model = ClassRegistry::init('PropertyAmenities');

	$property_amenities = $model->find('all', array(
                'conditions' => array(
                    'property_id' => $id
                )
        ));//print_r($property_amenities);die();
        return  $property_amenities;	
	}

}
