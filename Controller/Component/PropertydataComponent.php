<?php
App::uses('Component', 'Controller');

class PropertydataComponent extends Component {

public function getPropertyFeatures($id) {

	$model = ClassRegistry::init('PropertyFeature');

	$property_features = $model->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
        ));
        return  $property_features;	
	}

	public function getPropertyAmeities($id) {

	$model = ClassRegistry::init('PropertyAmenities');

	$property_amenities = $model->find('all', array(
                'conditions' => array(
                    'property_id' => $id
                )
        ));//print_r($property_amenities);die();
        return  $property_amenities;	
	}
	
	public function convertNumberFormatedRupees($num){
			
			$explrestunits = "" ;
			if(strlen($num)>3) {
				$lastthree = substr($num, strlen($num)-3, strlen($num));
				$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
				$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
				$expunit = str_split($restunits, 2);
				for($i=0; $i<sizeof($expunit); $i++) {
					// creates each of the 2's group and adds a comma to the end
					if($i==0) {
						$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
					} else {
						$explrestunits .= $expunit[$i].",";
					}
				}
				$thecash = $explrestunits.$lastthree;
			} else {
				$thecash = $num;
			}
			return $thecash;
			
		}

}
