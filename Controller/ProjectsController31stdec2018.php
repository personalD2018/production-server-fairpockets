<?php

// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ProjectsController extends AppController {

    public $uses = array(
        'Project', 
        'MstAreaCity', 
        'ProjectDetail', 
        'MstBank', 
        'ProjectFinancer', 
        'ProjectPricing', 
        'ProjectOfficeUse', 
        'ProjectImage', 
        'Builder'
    );
	var $helpers = array("Number");
    public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('editProjectPricingPlanTesting','ProjectPricingPaymentPlanEdit','editProjectPricingPlan1','projectPricingLists', 'editproject', 'addProjectPricingPlan', 'editProjectPricingPlan', 'addProjectPricing', 'editproject1', 'currentproject', 'pastprojectdetail', 'projectbankdetail', 'projectpricing', 'projectofficeuse', 'projectimages', 'projectsubmit', 'editpastproject', 'pastproject');
    }

    public function getProjects() {
        $getprojects = array();
            
        if (isset($this->request['data']['cityCode'])) {

            $getprojects = $this->Project->find('list', array(
                'fields' => array(
                    'project_id',
                    'project_name',
                ),
                'conditions' => array(
                    'approved' => 1,
                    'project_city' => $this->request['data']['cityCode']
                )
            ));
        }
        header('Content-Type: application/json');
        echo json_encode($getprojects);
        exit();
    }

    /* 
        @Get locality on the basis of project id 
    */
    public function getProjectLocality() {
        $projectLocality = array();
		//echo '884';
          //echo $this->request['data']['project_id'];//die();  
        if (isset($this->request['data']['project_id'])) {

            $projectLocality = $this->Project->find('first', array(
                'fields' => array(
                    'locality'
                ),
                'conditions' => array(
                    'approved' => 1,
                    'project_id' => $this->request['data']['project_id']
                )
            ));
        }
		//print_r($projectLocality);die();
		header('Content-Type: application/json');
        echo json_encode($projectLocality['Project']);
        exit();
    }

    public function pastproject() {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['Project']['project_id'])) {
                $this->Project->id = $this->request->data['Project']['project_id'];
            }
            $this->request->data ['Project']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            if ($this->Project->save($this->request->data)) {
                $lastinsertId = $this->Project->getLastInsertId();
                if (!empty($this->request->data ['Project']['userproject']) && $this->request->data ['Project']['userproject'] == '1') {
                    $this->request->data ['ProjectDetail']['project_id'] = $lastinsertId;
                    $this->request->data ['ProjectDetail']['land_area'] = $this->request->data['Project']['land_area'];
                    $this->ProjectDetail->save($this->request->data);
                    $alluserproj = $this->Project->find('list', array(
                        'fields' => array(
                            'project_id',
                            'project_name',
                        ),
                        'conditions' => array(
                            'user_id' => $this->Session->read('Auth.Websiteuser.id'),
							'project_id' => $this->request->data ['ProjectDetail']['project_id']
                        )
                    ));
                    header('Content-Type: application/json');
                    echo json_encode($alluserproj);
                    exit();
                } else {
                    echo $lastinsertId;
                    die;
                }
            }
        }

        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);
    }

    public function editpastproject($codid = Null) {

        $id = base64_decode($codid);
        // print $this->Session->read('Auth.Websiteuser.id'); exit;
        $valid = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $id,
                // 'current_project' => '0',
                'user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        // print_r($valid); exit;
        if (empty($valid) && empty($this->Session->read('Auth.User.id'))) {
            throw new NotFoundException(__('Invalid project'));
        }

        /* check builder approval */
        $this->Project->id = $id;
        if (!$this->Project->exists()) {
            throw new NotFoundException(__('Invalid project'));
        }
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }


        $this->request->data = $this->Project->findByProjectId($id);
        $proj_Detail = $this->ProjectDetail->find('all', array(
            'conditions' => array(
                'project_id' => $this->request->data['Project']['project_id']
            )
        ));

        $this->request->data['ProjectDetail'] = @$proj_Detail['0']['ProjectDetail'];

        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);
    }

    public function getProjectListByCityId($cityId) {
        header('Content-Type: application/json');
        print json_encode($this->Project->find('list', [
            'fields'     => [ 'project_id', 'project_name' ],
            'conditions' => [ 'project_city' => $cityId, 'user_id' => $this->Session->read('Auth.Websiteuser.id') ]
        ]));
        exit;
    }
	
	public function addProjectPricing() {

        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
            //if (!empty($this->request->data['Project']['id'])) {
                $this->Project->id = $this->request->data['Project']['id'];
            //}
            $this->request->data ['Project']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            //if ($this->Project->save($this->request->data)) {
               // $lastinsertId = $this->Project->getLastInsertId();
                //echo $lastinsertId;
                //die;
            //}
        }

        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));

        $banks = $this->MstBank->find('list', array(
            'fields' => array(
                'id',
                'bank_name',
            )
        ));
        $this->set('banks', $banks);
        $this->set('city_master', $city_master);
		
		$allproject = $this->Project->find('list', array(
                'fields' => array(
                    'project_id',
                    'project_name',
                ),
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
		
		$allprojectplan= array('Construction Linked Plan','Flexi Payment Plan','Down Payment Plan',
								'Subvention Plan','Special Plan');
										
		//echo "<pre>";print_r($allproject);die();
		$this->set('allproject', $allproject);
		$this->set('allprojectplan', $allprojectplan);
		
		
    }
	
	public function currentproject1() {

        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['Project']['id'])) {
                $this->Project->id = $this->request->data['Project']['id'];
            }
            $this->request->data ['Project']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            if ($this->Project->save($this->request->data)) {
                $lastinsertId = $this->Project->getLastInsertId();
                echo $lastinsertId;
                die;
            }
        }

        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));

        $banks = $this->MstBank->find('list', array(
            'fields' => array(
                'id',
                'bank_name',
            )
        ));
        $this->set('banks', $banks);
        $this->set('city_master', $city_master);
    }
	
	

    public function currentproject() {

        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['Project']['id'])) {
                $this->Project->id = $this->request->data['Project']['id'];
            }
            $this->request->data ['Project']['user_id'] = $this->Session->read('Auth.Websiteuser.id');
            if ($this->Project->save($this->request->data)) {
                $lastinsertId = $this->Project->getLastInsertId();
                echo $lastinsertId;
                die;
            }
        }

        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));

        $banks = $this->MstBank->find('list', array(
            'fields' => array(
                'id',
                'bank_name',
            )
        ));
        $this->set('banks', $banks);
        $this->set('city_master', $city_master);
    }

    public function pastprojectdetail() {
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['ProjectDetail']['id'])) {
                $this->ProjectDetail->id = $this->request->data['ProjectDetail']['id'];
            }
            $name = $this->request->data ['ProjectDetail']['proj_logo']['name'];
            $target_dir = 'upload/project_logo/';

            if (!empty($name)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['ProjectDetail']['proj_logo']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['ProjectDetail']['proj_logo'] = $newfilename;
                }
            } else {
                $this->request->data ['ProjectDetail']['proj_logo'] = '';
            }

            $this->ProjectDetail->save($this->request->data);

            exit();
        }
    }

    public function projectbankdetail() {
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['ProjectFinancer']['id'])) {
                $this->ProjectFinancer->id = $this->request->data['ProjectFinancer']['id'];
            }


            $this->request->data ['ProjectFinancer']['bank'] = json_encode($this->request->data['bank'], true);
            ;
            $this->ProjectFinancer->save($this->request->data);
            exit();
        }
    }
	
	
	
	public function projectpricing() {
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->loadModel('ProjectBspDetail');
			
			if (!empty($this->request->data['ProjectPricing']['project_id'])) {
                $projectid = $this->request->data['ProjectPricing']['project_id'];
            }
			//echo $projectid;
			//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
            $this->ProjectBspDetail->query("Delete from `project_bsp_details` WHERE `project_id` = " . $projectid . "");
			//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
			for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
                $this->request->data['ProjectBspDetail']['project_id'] = $this->request->data['ProjectPricing']['project_id'];
                  if(isset($this->request->data['bspcharges']['unit_bsp'.$i.'']) && $this->request->data['bspcharges']['unit_bsp'.$i.''] != ''){
					  $price_bsp_unit = $this->request->data['bspcharges']['unit_bsp'.$i.''];
				  }else{
					  $price_bsp_unit = 1;
				  }
				  
				$this->request->data['ProjectBspDetail']['bsp_charges'] =  $this->request->data['bspcharges']['price_bsp'.$i.''];
				$this->request->data['ProjectBspDetail']['bsp_charges_unit'] =  $price_bsp_unit;
				$this->request->data['ProjectBspDetail']['floor_from'] = $this->request->data['bspcharges']['from_floor'.$i.''];
				$this->request->data['ProjectBspDetail']['floor_to'] = $this->request->data['bspcharges']['to_floor'.$i.''];
				$this->request->data['ProjectBspDetail']['unit'] = '1';
				
                $this->ProjectBspDetail->create(false);
                $this->ProjectBspDetail->save($this->request->data['ProjectBspDetail']);
            }

            $this->ProjectBspDetail->save($this->request->data['ProjectBspDetail']);
			
			
			
			$this->loadModel('ProjectPlcDetail');
			
			$this->ProjectPlcDetail->query("Delete from `project_plc_details` WHERE `project_id` = " . $projectid . "");
			
			$arraytesting = $this->request->data['bspchargesplc'];
			for($i=1; $i <= count($arraytesting)/4; $i++){			
				$requestdata['start'] =  $arraytesting['plc_from_floor'.$i.''];
				$requestdata['end'] =	$arraytesting['plc_to_floor'.$i.''];
				$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = 0;
				for($x = $requestdata['start']; $x <= $requestdata['end']; $x++) {
					$this->request->data['ProjectPlcDetail']['project_id'] = $projectid;
					$this->request->data['ProjectPlcDetail']['plc_floor_no'] = $x;
					if($arraytesting['per_floor_mode'.$i.'']==1){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = $arraytesting['floor_plc_amount'.$i.''];
					}
					if($arraytesting['per_floor_mode'.$i.'']==2){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					}
					if($arraytesting['per_floor_mode'.$i.'']==3){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					}
					$this->request->data['ProjectPlcDetail']['plc_floor_mode'] = $arraytesting['per_floor_mode'.$i.''];
					
					//$this->request->data['ProjectPlcDetail']['plc_floor_from'] = $x;
					//$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					//echo '<pre>'; print_r($this->request->data['ProjectPlcDetail']);
					$this->ProjectPlcDetail->create(false);
					$this->ProjectPlcDetail->save($this->request->data['ProjectPlcDetail']);
				}			
			}
			
			
			
			$this->loadModel('ProjectSpaceDetail');
			
			$this->ProjectSpaceDetail->query("Delete from `project_space_details` WHERE `project_id` = " . $projectid . "");
			
			for( $i=1; $i <= count($this->request->data['bspchargesspace'])/4; $i++ ){
                $this->request->data['ProjectSpaceDetail']['project_id'] = $projectid;
                  
				$this->request->data['ProjectSpaceDetail']['space_mode'] =  $this->request->data['bspchargesspace']['space_mode'.$i.''];
				$this->request->data['ProjectSpaceDetail']['space_from_floor'] = $this->request->data['bspchargesspace']['space_from_floor'.$i.''];
				$this->request->data['ProjectSpaceDetail']['floor_space_area'] = $this->request->data['bspchargesspace']['floor_space_area'.$i.''];
				$this->request->data['ProjectSpaceDetail']['floor_space_amount'] = $this->request->data['bspchargesspace']['floor_space_amount'.$i.''];
				//echo '<pre>'; print_r($this->request->data['ProjectBspDetail']);
                $this->ProjectSpaceDetail->create(false);
                $this->ProjectSpaceDetail->save($this->request->data['ProjectSpaceDetail']);
            }
			
			
			
			
			
			
            if (!empty($this->request->data['ProjectPricing']['id'])) {
                $this->ProjectPricing->id = $this->request->data['ProjectPricing']['id'];
            }
			if($this->request->data ['ProjectPricing']['price_bsp_copy'] != '')
			{
				
			}
            else
			{
				$this->request->data ['ProjectPricing']['bsp_charges'] = json_encode($this->request->data['bspcharges'], true);
			}
			
			//echo '<pre>'; print_r($this->request->data);die();
			$this->request->data ['ProjectPricing']['bspchargesspace'] = json_encode($this->request->data['bspchargesspace'], true);
			
			$this->request->data['ProjectPricing']['bspchargesplc'] = json_encode($this->request->data['bspchargesplc'], true);
			$this->request->data['ProjectPricing']['project_charges'] = json_encode($this->request->data['charges'], true);
            $this->request->data['ProjectPricing']['addition_charges'] = json_encode($this->request->data['addition_charges'], true);
            $this->request->data['ProjectPricing']['other_charges'] = json_encode($this->request->data['othercharges'], true);
            $this->request->data['ProjectPricing']['payment_plan'] = json_encode($this->request->data['plan'], true);
            //echo '<pre>'; print_r($this->request->data['ProjectPricing']);die();
			$this->ProjectPricing->save($this->request->data['ProjectPricing']);
            exit();
        }
    }

    public function addProjectPricingPlan() {
        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data); die();
			
			if($this->request->data ['ProjectPricing']['price_bsp_copy'] != '')
			{
				
			}
            else
			{
				$this->request->data ['ProjectPricing']['bsp_charges'] = json_encode($this->request->data['bspcharges'], true);
			}
			
			//echo '<pre>'; print_r($this->request->data);die();
			$this->request->data['ProjectPricing']['bspchargesspace'] = json_encode($this->request->data['bspchargesspace'], true);
			
			$this->request->data['ProjectPricing']['bspchargesplc'] = json_encode($this->request->data['bspchargesplc'], true);
			$this->request->data['ProjectPricing']['project_charges'] = json_encode($this->request->data['charges'], true);
            $this->request->data['ProjectPricing']['addition_charges'] = json_encode($this->request->data['addition_charges'], true);
            $this->request->data['ProjectPricing']['other_charges'] = json_encode($this->request->data['othercharges'], true);
            $this->request->data['ProjectPricing']['payment_plan'] = json_encode($this->request->data['plan'], true);
			$this->request->data['ProjectPricing']['payment_plan1'] = $this->request->data['ProjectPricing']['payment_plan1'];
            //echo '<pre>'; print_r($this->request->data['ProjectPricing']);die();
			$this->ProjectPricing->save($this->request->data['ProjectPricing']);
			$lastinsertId = $this->ProjectPricing->getLastInsertId();
			
			$this->loadModel('ProjectBspDetail');
			
			for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
                $this->request->data['ProjectBspDetail']['project_id'] = $this->request->data['ProjectPricing']['project_id'];
                  if(isset($this->request->data['bspcharges']['unit_bsp'.$i.'']) && $this->request->data['bspcharges']['unit_bsp'.$i.''] != ''){
					  $price_bsp_unit = $this->request->data['bspcharges']['unit_bsp'.$i.''];
				  }else{
					  $price_bsp_unit = 1;
				  }
				  
				$this->request->data['ProjectBspDetail']['bsp_charges'] =  $this->request->data['bspcharges']['price_bsp'.$i.''];
				$this->request->data['ProjectBspDetail']['bsp_charges_unit'] =  $price_bsp_unit;
				$this->request->data['ProjectBspDetail']['floor_from'] = $this->request->data['bspcharges']['from_floor'.$i.''];
				$this->request->data['ProjectBspDetail']['floor_to'] = $this->request->data['bspcharges']['to_floor'.$i.''];
				$this->request->data['ProjectBspDetail']['project_pricing_id'] = $lastinsertId;
				$this->request->data['ProjectBspDetail']['unit'] = '1';
				
                $this->ProjectBspDetail->create(false);
                $this->ProjectBspDetail->save($this->request->data['ProjectBspDetail']);
            }

            $this->ProjectBspDetail->save($this->request->data['ProjectBspDetail']);
			
			$this->loadModel('ProjectPlcDetail');			
			
			$arraytesting = $this->request->data['bspchargesplc'];
			for($i=1; $i <= count($arraytesting)/4; $i++){			
				$requestdata['start'] =  $arraytesting['plc_from_floor'.$i.''];
				$requestdata['end'] =	$arraytesting['plc_to_floor'.$i.''];
				$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = 0;
				for($x = $requestdata['start']; $x <= $requestdata['end']; $x++) {
					$this->request->data['ProjectPlcDetail']['project_id'] = $this->request->data['ProjectPricing']['project_id'];
					$this->request->data['ProjectPlcDetail']['plc_floor_no'] = $x;
					if($arraytesting['per_floor_mode'.$i.'']==1){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = $arraytesting['floor_plc_amount'.$i.''];
					}
					if($arraytesting['per_floor_mode'.$i.'']==2){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					}
					if($arraytesting['per_floor_mode'.$i.'']==3){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					}
					$this->request->data['ProjectPlcDetail']['plc_floor_mode'] = $arraytesting['per_floor_mode'.$i.''];
					if(isset($arraytesting['unit_plc'.$i.'']) && $arraytesting['unit_plc'.$i.''] != ''){
					  $price_plc_unit = $arraytesting['unit_plc'.$i.''];
				  }else{
					  $price_plc_unit = 1;
				  }
					$this->request->data['ProjectPlcDetail']['plc_charges_unit'] = $price_plc_unit;
					$this->request->data['ProjectPlcDetail']['project_pricing_id'] = $lastinsertId;
					//$this->request->data['ProjectPlcDetail']['plc_floor_from'] = $x;
					//$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					//echo '<pre>'; print_r($this->request->data['ProjectPlcDetail']);
					$this->ProjectPlcDetail->create(false);
					$this->ProjectPlcDetail->save($this->request->data['ProjectPlcDetail']);
				}			
			}
            
            exit();
        }
    }
	
	public function ProjectPricingPaymentPlanEdit(){
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data); 	
			
			$this->loadModel('ProjectPricingPaymentPlan');			
			if (isset($this->request->data['ProjectPricingPlan']['id']) && !empty($this->request->data['ProjectPricingPlan']['id'])) {
                $projectPricingid = $this->request->data['ProjectPricingPlan']['id'];
            }
			$projectid = $this->request->data['ProjectPricingPlan']['project_id'];
			
			//$latestPaymentPlan = $this->ProjectPricingPaymentPlan->query("SELECT project_pricing_id FROM `project_pricing_payment_plans` WHERE project_id = ".$projectid."");
			 
			//echo $latestPaymentPlan[0]['project_pricing_payment_plans']['project_pricing_id'];
			//echo '<pre>'; print_r($this->request->data['ProjectPricingPlan']);die();
			//echo $projectPricingid; die();
			//if ($latestPaymentPlan[0]['project_pricing_payment_plans']['project_pricing_id'] == 0) {
			//	$this->ProjectPricingPaymentPlan->query("Delete from `project_pricing_payment_plans` WHERE `project_id` = " . $projectid . "");
			//}else{
				$this->ProjectPricingPaymentPlan->query("Delete from `project_pricing_payment_plans` WHERE `project_pricing_id` = " . $projectPricingid . "");	
			//}
			
			//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
			for( $i=1; $i <= count($this->request->data['bspchargesplan'])/4; $i++ ){
                $this->request->data['ProjectPricingPaymentPlan']['project_id'] = $projectid;
				$this->request->data['ProjectPricingPaymentPlan']['project_pricing_id'] =  $projectPricingid;
				$this->request->data['ProjectPricingPaymentPlan']['plan_base_charge'] =  $this->request->data['bspchargesplan']['plan_base_charge'.$i.''];
				$this->request->data['ProjectPricingPaymentPlan']['plan_bc_amount'] = $this->request->data['bspchargesplan']['plan_bc_amount'.$i.''];
				$this->request->data['ProjectPricingPaymentPlan']['plan_total_charges'] = $this->request->data['bspchargesplan']['plan_total_charges'.$i.''];
				$this->request->data['ProjectPricingPaymentPlan']['plan_tc_amount'] = $this->request->data['bspchargesplan']['plan_tc_amount'.$i.''];
				$this->request->data['ProjectPricingPaymentPlan']['status'] = '1';
				//echo '<pre>'; print_r($this->request->data['ProjectPricingPaymentPlan']);die();
                $this->ProjectPricingPaymentPlan->create(false);
                $this->ProjectPricingPaymentPlan->save($this->request->data['ProjectPricingPaymentPlan']);
            }
			
			$this->loadModel('ProjectPricingPayment');
			$this->ProjectPricingPaymentPlan->query("Delete from `project_pricing_payments` WHERE `project_pricing_id` = " . $projectPricingid . "");	
			$this->request->data['ProjectPricingPayment']['project_id'] = $projectid;
			$this->request->data['ProjectPricingPayment']['project_pricing_id'] = $projectPricingid;
			$this->request->data['ProjectPricingPayment']['plansjson'] = json_encode($this->request->data['bspchargesplan'], true);;
			$this->request->data['ProjectPricingPayment']['status'] = '1';
			//echo '<pre>'; print_r($this->request->data['ProjectPricingPayment']);die();
            $this->ProjectPricingPayment->save($this->request->data['ProjectPricingPayment']);
			exit();	
		}
		
	}
	
	
	
	public function addProjectPricingPlanEdit() {
        if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data); die();
			
			
			$this->loadModel('ProjectBspDetail');
			
			if (isset($this->request->data['ProjectPricing']['id']) && !empty($this->request->data['ProjectPricing']['id'])) {
                $projectPricingid = $this->request->data['ProjectPricing']['id'];
            }
			//echo $projectid;
			//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
			
			$projectid = $this->request->data['ProjectPricing']['project_id'];
			
			$latestBsp = $this->ProjectBspDetail->query("SELECT MIN(bsp_charges),project_pricing_id FROM `project_bsp_details` WHERE project_id = ".$projectid."");
			 
			//echo $latestBsp[0]['project_bsp_details']['project_pricing_id'];
			
			
			
			//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
			
			
			if ($latestBsp[0]['project_bsp_details']['project_pricing_id'] == 0) {
				$this->ProjectBspDetail->query("Delete from `project_bsp_details` WHERE `project_id` = " . $projectid . "");
			}else{
				$this->ProjectBspDetail->query("Delete from `project_bsp_details` WHERE `project_pricing_id` = " . $projectPricingid . "");	
			}
            
			
			//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
			for( $i=1; $i <= count($this->request->data['bspcharges'])/4; $i++ ){
                $this->request->data['ProjectBspDetail']['project_id'] = $projectid;
                  if(isset($this->request->data['bspcharges']['unit_bsp'.$i.'']) && $this->request->data['bspcharges']['unit_bsp'.$i.''] != ''){
					  $price_bsp_unit = $this->request->data['bspcharges']['unit_bsp'.$i.''];
				  }else{
					  $price_bsp_unit = 1;
				  }
				  
				$this->request->data['ProjectBspDetail']['bsp_charges'] =  $this->request->data['bspcharges']['price_bsp'.$i.''];
				$this->request->data['ProjectBspDetail']['bsp_charges_unit'] =  $price_bsp_unit;
				$this->request->data['ProjectBspDetail']['floor_from'] = $this->request->data['bspcharges']['from_floor'.$i.''];
				$this->request->data['ProjectBspDetail']['floor_to'] = $this->request->data['bspcharges']['to_floor'.$i.''];
				$this->request->data['ProjectBspDetail']['project_pricing_id'] = $projectPricingid;
				$this->request->data['ProjectBspDetail']['unit'] = '1';
				
                $this->ProjectBspDetail->create(false);
                $this->ProjectBspDetail->save($this->request->data['ProjectBspDetail']);
            }

            $this->ProjectBspDetail->save($this->request->data['ProjectBspDetail']);
			

			
			$this->loadModel('ProjectPlcDetail');
			$plc_price = $this->ProjectPlcDetail->query("SELECT MIN(plc_floor_charge),project_pricing_id FROM `project_plc_details` WHERE  project_id = ".$projectid."");
			
			

			if ($plc_price[0]['project_plc_details']['project_pricing_id'] == 0) {
				$this->ProjectPlcDetail->query("Delete from `project_plc_details` WHERE `project_id` = " . $projectid . "");
			}else{
				$this->ProjectPlcDetail->query("Delete from `project_plc_details` WHERE `project_pricing_id` = " . $projectPricingid . "");			
			}
			$arraytesting = $this->request->data['bspchargesplc'];
			
			//echo '<pre>'; print_r($arraytesting);die();
			
			for($i=1; $i <= count($arraytesting)/6; $i++){			
				$requestdata['start'] =  $arraytesting['plc_from_floor'.$i.''];
				$requestdata['end'] =	$arraytesting['plc_to_floor'.$i.''];
				$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = 0;
				for($x = $requestdata['start']; $x <= $requestdata['end']; $x++) {
					$this->request->data['ProjectPlcDetail']['project_id'] = $projectid;
					$this->request->data['ProjectPlcDetail']['plc_floor_no'] = $x;
					if($arraytesting['per_floor_mode'.$i.'']==1){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = $arraytesting['floor_plc_amount'.$i.''];
					}
					if($arraytesting['per_floor_mode'.$i.'']==2){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					}
					if($arraytesting['per_floor_mode'.$i.'']==3){
					$this->request->data['ProjectPlcDetail']['plc_floor_charge'] = $arraytesting['floor_plc_amount'.$i.''];
					}
					$this->request->data['ProjectPlcDetail']['plc_floor_mode'] = $arraytesting['per_floor_mode'.$i.''];
					if(isset($arraytesting['unit_plc'.$i.'']) && $arraytesting['unit_plc'.$i.''] != ''){
					  $price_plc_unit = $arraytesting['unit_plc'.$i.''];
					  }else{
						  $price_plc_unit = 1;
					  }
					 $this->request->data['ProjectPlcDetail']['area_unit_plc'] = $arraytesting['area_unit_plccharges'.$i.''];
					
					$this->request->data['ProjectPlcDetail']['plc_charges_unit'] = $price_plc_unit;
					$this->request->data['ProjectPlcDetail']['project_pricing_id'] = $projectPricingid;
					//$this->request->data['ProjectPlcDetail']['plc_floor_from'] = $x;
					//$this->request->data['ProjectPlcDetail']['plc_floor_charge'] += $arraytesting['floor_plc_amount'.$i.''];
					//echo '<pre>'; print_r($this->request->data['ProjectPlcDetail']);die();
					$this->ProjectPlcDetail->create(false);
					$this->ProjectPlcDetail->save($this->request->data['ProjectPlcDetail']);
				}			
			}
			
			
            if (!empty($this->request->data['ProjectPricing']['id'])) {
                $this->ProjectPricing->id = $this->request->data['ProjectPricing']['id'];
            }
			if($this->request->data ['ProjectPricing']['price_bsp_copy'] != '')
			{
				
			}
            else
			{
				$this->request->data ['ProjectPricing']['bsp_charges'] = json_encode($this->request->data['bspcharges'], true);
			}
			
			//echo '<pre>'; print_r($this->request->data);die();
			$this->request->data['ProjectPricing']['bspchargesspace'] = json_encode($this->request->data['bspchargesspace'], true);
			
			$this->request->data['ProjectPricing']['bspchargesplc'] = json_encode($this->request->data['bspchargesplc'], true);
			$this->request->data['ProjectPricing']['project_charges'] = json_encode($this->request->data['charges'], true);
            $this->request->data['ProjectPricing']['addition_charges'] = json_encode($this->request->data['addition_charges'], true);
            $this->request->data['ProjectPricing']['other_charges'] = json_encode($this->request->data['othercharges'], true);
            $this->request->data['ProjectPricing']['payment_plan'] = json_encode($this->request->data['plan'], true);
			$this->request->data['ProjectPricing']['payment_plan1'] = $this->request->data['ProjectPricing']['payment_plan1'];
            //echo '<pre>'; print_r($this->request->data['ProjectPricing']);die();
			$this->ProjectPricing->save($this->request->data['ProjectPricing']);
            exit();
        }
    }
	
	
	public function editProjectPricingPlan1($codid) {
        //$id = base64_decode($codid);
		$id = $codid;      

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

       
			//echo $id;
            $this->request->data = $this->ProjectPricing->findById($id);
            

            $proj_price = $this->ProjectPricing->find('all', array(
                'conditions' => array(
                    'id' => $id
                )
            ));
			//echo '<pre>'; print_r($proj_price);die();

            //$this->request->data['ProjectDetail'] = @$proj_Detail['0']['ProjectDetail'];
            //$this->request->data['ProjectFinancer'] = @$proj_fina['0']['ProjectFinancer'];
            $this->request->data['ProjectPricing'] = @$proj_price['0']['ProjectPricing'];
            //$this->request->data['ProjectOfficeUse'] = @$proj_OfficeUse['0']['ProjectOfficeUse'];
            //$this->request->data['ProjectImage'] = @$proj_image;
            if (!empty($proj_price['0']['ProjectPricing']['payment_plan'])) {
                $this->request->data['plan'] = json_decode($proj_price['0']['ProjectPricing']['payment_plan'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bsp_charges'])) {
                $this->request->data['bspcharges'] = json_decode($proj_price['0']['ProjectPricing']['bsp_charges'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesspace'])) {
                $this->request->data['bspchargesspace'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesspace'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesplc'])) {
                $this->request->data['bspchargesplc'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesplc'], 'true');
            }
			//echo '<pre>';print_r($this->request->data);
            if (!empty($proj_price['0']['ProjectPricing']['project_charges'])) {
                $this->request->data['charges'] = json_decode($proj_price['0']['ProjectPricing']['project_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['addition_charges'])) {
                $this->request->data['addition_charges'] = json_decode($proj_price['0']['ProjectPricing']['addition_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['other_charges'])) {
                $this->request->data['othercharges'] = json_decode($proj_price['0']['ProjectPricing']['other_charges'], 'true');
            }
			
			
			$this->loadModel('ProjectPricingPayment');
			$proj_price_payment_plan_array = $this->ProjectPricingPayment->find('all', array(
				
                'conditions' => array(
                    'project_pricing_id' => $id
                )
            ));
			
			//echo '<pre>'; print_r($proj_price_payment_plan_array);
			//die();
			if (!empty($proj_price_payment_plan_array['0']['ProjectPricingPayment']['plansjson'])) {
                $this->request->data['bspchargesplan'] = json_decode($proj_price_payment_plan_array['0']['ProjectPricingPayment']['plansjson'], 'true');
            }
		//echo '<pre>'; print_r($this->request->data['bspchargesplan']);
			//die();
        

        
        $this->layout = 'account';	
		$allproject = $this->Project->find('list', array(
                'fields' => array(
                    'project_id',
                    'project_name',
                ),
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
		$this->set('allproject', $allproject);		
		//echo '<pre>'; print_r($this->request->data);die();
		
    }
	
	
	public function editProjectPricingPlanTesting($codid) {
        //$id = base64_decode($codid);
		$id = $codid;      

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

       
			//echo $id;
            $this->request->data = $this->ProjectPricing->findById($id);
            

            $proj_price = $this->ProjectPricing->find('all', array(
                'conditions' => array(
                    'id' => $id
                )
            ));
			//echo '<pre>'; print_r($proj_price);die();

            //$this->request->data['ProjectDetail'] = @$proj_Detail['0']['ProjectDetail'];
            //$this->request->data['ProjectFinancer'] = @$proj_fina['0']['ProjectFinancer'];
            $this->request->data['ProjectPricing'] = @$proj_price['0']['ProjectPricing'];
            //$this->request->data['ProjectOfficeUse'] = @$proj_OfficeUse['0']['ProjectOfficeUse'];
            //$this->request->data['ProjectImage'] = @$proj_image;
            if (!empty($proj_price['0']['ProjectPricing']['payment_plan'])) {
                $this->request->data['plan'] = json_decode($proj_price['0']['ProjectPricing']['payment_plan'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bsp_charges'])) {
                $this->request->data['bspcharges'] = json_decode($proj_price['0']['ProjectPricing']['bsp_charges'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesspace'])) {
                $this->request->data['bspchargesspace'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesspace'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesplc'])) {
                $this->request->data['bspchargesplc'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesplc'], 'true');
            }
			//echo '<pre>';print_r($this->request->data);
            if (!empty($proj_price['0']['ProjectPricing']['project_charges'])) {
                $this->request->data['charges'] = json_decode($proj_price['0']['ProjectPricing']['project_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['addition_charges'])) {
                $this->request->data['addition_charges'] = json_decode($proj_price['0']['ProjectPricing']['addition_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['other_charges'])) {
                $this->request->data['othercharges'] = json_decode($proj_price['0']['ProjectPricing']['other_charges'], 'true');
            }

        

        
        $this->layout = 'account';	
		$allproject = $this->Project->find('list', array(
                'fields' => array(
                    'project_id',
                    'project_name',
                ),
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
		$this->set('allproject', $allproject);		
		//echo '<pre>'; print_r($this->request->data);die();
		
    }
	
	
	public function editProjectPricingPlan($codid) {
        //$id = base64_decode($codid);
		$id = $codid;      

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

       
			//echo $id;
            $this->request->data = $this->ProjectPricing->findById($id);
            

            $proj_price = $this->ProjectPricing->find('all', array(
                'conditions' => array(
                    'id' => $id
                )
            ));
			//echo '<pre>'; print_r($proj_price);die();

            //$this->request->data['ProjectDetail'] = @$proj_Detail['0']['ProjectDetail'];
            //$this->request->data['ProjectFinancer'] = @$proj_fina['0']['ProjectFinancer'];
            $this->request->data['ProjectPricing'] = @$proj_price['0']['ProjectPricing'];
            //$this->request->data['ProjectOfficeUse'] = @$proj_OfficeUse['0']['ProjectOfficeUse'];
            //$this->request->data['ProjectImage'] = @$proj_image;
            if (!empty($proj_price['0']['ProjectPricing']['payment_plan'])) {
                $this->request->data['plan'] = json_decode($proj_price['0']['ProjectPricing']['payment_plan'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bsp_charges'])) {
                $this->request->data['bspcharges'] = json_decode($proj_price['0']['ProjectPricing']['bsp_charges'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesspace'])) {
                $this->request->data['bspchargesspace'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesspace'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesplc'])) {
                $this->request->data['bspchargesplc'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesplc'], 'true');
            }
			//echo '<pre>';print_r($this->request->data);
            if (!empty($proj_price['0']['ProjectPricing']['project_charges'])) {
                $this->request->data['charges'] = json_decode($proj_price['0']['ProjectPricing']['project_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['addition_charges'])) {
                $this->request->data['addition_charges'] = json_decode($proj_price['0']['ProjectPricing']['addition_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['other_charges'])) {
                $this->request->data['othercharges'] = json_decode($proj_price['0']['ProjectPricing']['other_charges'], 'true');
            }

        

        
        $this->layout = 'account';	
		$allproject = $this->Project->find('list', array(
                'fields' => array(
                    'project_id',
                    'project_name',
                ),
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
		$this->set('allproject', $allproject);		
		//echo '<pre>'; print_r($this->request->data);die();
		
    }
	
	

    public function projectofficeuse() {
		ini_set('upload_max_filesize', '64M');
		ini_set('post_max_size', '64M');
		ini_set('max_input_time', 600);
		ini_set('max_execution_time', 600);

        if ($this->request->is('post') || $this->request->is('put')) {

            if (!empty($this->request->data['ProjectOfficeUse']['id'])) {
                $this->ProjectOfficeUse->id = $this->request->data['ProjectOfficeUse']['id'];
            }
            $office_brochure = $this->request->data ['ProjectOfficeUse']['office_brochure']['name'];
            $target_dir = 'upload/project_office_brochure/';

            if (!empty($office_brochure)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['ProjectOfficeUse']['office_brochure']['tmp_name'];
                $temp = explode(".", $office_brochure);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['ProjectOfficeUse']['office_brochure'] = $newfilename;
                }
            } else if (!empty($this->request->data['office_brochure1'])) {
                $this->request->data ['ProjectOfficeUse']['office_brochure'] = $this->request->data['office_brochure1'];
            } else {
                $this->request->data ['ProjectOfficeUse']['office_brochure'] = '';
            }
            $office_rtcard = $this->request->data ['ProjectOfficeUse']['office_rtcard']['name'];
            $target_dir = 'upload/project_office_rtcard/';

            if (!empty($office_rtcard)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['ProjectOfficeUse']['office_rtcard']['tmp_name'];
                $temp = explode(".", $office_rtcard);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['ProjectOfficeUse']['office_rtcard'] = $newfilename;
					$ratecard = 1;
                }
            } else if (!empty($this->request->data['office_rtcard1'])) {
                $this->request->data['ProjectOfficeUse']['office_rtcard'] = $this->request->data['office_rtcard1'];
				$ratecard = 2;
            } else {
                $this->request->data['ProjectOfficeUse']['office_rtcard'] = '';
				$ratecard = 3;
            }
			
			
			$office_bba = $this->request->data ['ProjectOfficeUse']['office_bba']['name'];
            $target_dir = 'upload/project_office_bba/';

            if (!empty($office_bba)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['ProjectOfficeUse']['office_bba']['tmp_name'];
                $temp = explode(".", $office_bba);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['ProjectOfficeUse']['office_bba'] = $newfilename;
					//$ratecard = 1;
                }
            } else if (!empty($this->request->data['office_bba1'])) {
                $this->request->data['ProjectOfficeUse']['office_bba'] = $this->request->data['office_bba1'];
				//$ratecard = 2;
            } else {
                $this->request->data['ProjectOfficeUse']['office_bba'] = '';
				//$ratecard = 3;
            }
			
			
			
			$office_uaad = $this->request->data ['ProjectOfficeUse']['office_uaad']['name'];
            $target_dir = 'upload/project_office_uaad/';

            if (!empty($office_uaad)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['ProjectOfficeUse']['office_uaad']['tmp_name'];
                $temp = explode(".", $office_uaad);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['ProjectOfficeUse']['office_uaad'] = $newfilename;
					//$ratecard = 1;
                }
            } else if (!empty($this->request->data['office_uaad1'])) {
                $this->request->data['ProjectOfficeUse']['office_uaad'] = $this->request->data['office_uaad1'];
				//$ratecard = 2;
            } else {
                $this->request->data['ProjectOfficeUse']['office_uaad'] = '';
				//$ratecard = 3;
            }
			
			
            //$this->ProjectOfficeUse->save($this->request->data);
			
			//echo $ratecard;die();
            $this->ProjectOfficeUse->save($this->request->data);
			if($ratecard = 1){				
					$this->loadModel('MessageBoardNotification');	
					$MessageBoardNotifications = $this->MessageBoardNotification->find('all', array(
						'conditions' => array(
							'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
							'status' => 'price_list'
						)
					));
					if(isset($MessageBoardNotifications) && !empty($MessageBoardNotifications)){
						//echo '1';
						$this->MessageBoardNotification->id = $this->Session->read('Auth.Websiteuser.id');
						$this->MessageBoardNotification->updateAll(array(
						'MessageBoardNotification.counts' => 'MessageBoardNotification.counts + 1'),
						array('MessageBoardNotification.builder_id' => $this->Session->read('Auth.Websiteuser.id'),
								'MessageBoardNotification.status' => 'price_list'));						
					}else{
						//echo '2';
						$this->request->data['MessageBoardNotification']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['MessageBoardNotification']['counts'] = 1;
						$this->request->data['MessageBoardNotification']['status'] = 'price_list';
						$this->MessageBoardNotification->save($this->request->data['MessageBoardNotification']);						
					}
				
			}
			if($brochure = 1){
				$this->loadModel('MessageBoardNotification');	
					$MessageBoardNotifications = $this->MessageBoardNotification->find('all', array(
						'conditions' => array(
							'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
							'status' => 'brochure_list'
						)
					));
					if(isset($MessageBoardNotifications) && !empty($MessageBoardNotifications)){
						//echo '1';
						$this->MessageBoardNotification->id = $this->Session->read('Auth.Websiteuser.id');
						$this->MessageBoardNotification->updateAll(array(
						'MessageBoardNotification.counts' => 'MessageBoardNotification.counts + 1'),
						array('MessageBoardNotification.builder_id' => $this->Session->read('Auth.Websiteuser.id'),
								'MessageBoardNotification.status' => 'brochure_list'));						
					}else{
						//echo '2';
						$this->request->data['MessageBoardNotification']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['MessageBoardNotification']['counts'] = 1;
						$this->request->data['MessageBoardNotification']['status'] = 'brochure_list';
						$this->MessageBoardNotification->save($this->request->data['MessageBoardNotification']);						
					}				
			}
			
            exit();
        }
    }

    public function projectimages() {
		
		Configure::write('debug',2);
		
		ini_set('upload_max_filesize', '64M');
		ini_set('post_max_size', '64M');
		ini_set('max_input_time', 600);
		ini_set('max_execution_time', 600);
		//ini_set('max_execution_time', 500);
		//ini_set('max_execution_time', 500);
		//ini_set('max_execution_time', 500);
		//phpinfo();
		//die();
		
		$this->loadModel('ProjectMedia');
		
		
        $target_dir = 'upload/project_images/';
        if (!empty($this->request->data)) {

            /*  Start code for o Uploading images */
			
			//echo '<pre>'; print_r($this->request->data);
			if (!empty($this->request->data['ProjectMedia'])) {				
				$projectMediaArr = $this->ProjectMedia->find('all',array(
					'conditions' => array(
						'project_id' => $this->request->data['Project']['project_id']
					)
				));
				//echo '<pre>'; print_r($projectMediaArr);die();
				
				 /*if (!empty($this->request->data['ProjectMedia']['image_app'])) {
					$name = $this->request->data['ProjectMedia']['image_app']['name'];
					
					
					
				 }*/
				 
					$image_app = $this->request->data['ProjectMedia']['image_app']['name'];
					$target_dir_image_app = 'upload/project_media/';

					if (!empty($image_app)) {
						// Check For Empty Values 
						$tmprary_name1 = $this->request->data['ProjectMedia']['image_app']['tmp_name'];
						$temp = explode(".", $image_app);
						$newfilename1= uniqid() . '.' . end($temp);
						if (move_uploaded_file($tmprary_name1, $target_dir_image_app . $newfilename1)) {
							$this->request->data['ProjectMedia']['image_app'] = $newfilename1;
							//$ratecard = 1;
						}					
					} else {
						$this->request->data['ProjectMedia']['image_app'] = '';
						//$ratecard = 3;
					}
				 
				
				
				//$youtube_link1_key = $this->request->data['ProjectMedia']['youtube_link1'];
				//$youtube_link2_key = $this->request->data['ProjectMedia']['youtube_link2'];
				if($this->request->data['ProjectMedia']['youtube_link1'] != ''){
					$video_id1 = explode("?v=", $this->request->data['ProjectMedia']['youtube_link1']);
					$video_id1 = $video_id1[1];
					$this->request->data['ProjectMedia']['youtube_link1_key'] = $video_id1;
				}else{
					$this->request->data['ProjectMedia']['youtube_link1_key'] = '';
				}
				
				if($this->request->data['ProjectMedia']['youtube_link2'] != ''){
					$video_id2 = explode("?v=", $this->request->data['ProjectMedia']['youtube_link2']);
					$video_id2 = $video_id2[1];
					$this->request->data['ProjectMedia']['youtube_link2_key'] = $video_id2;
				}else{
					$this->request->data['ProjectMedia']['youtube_link2_key'] = '';
				}
				//echo '<pre>'; print_r($this->request->data['ProjectMedia']);die();
				if (!empty($projectMediaArr)) {
					$this->ProjectMedia->id = $projectMediaArr[0]['ProjectMedia']['id'];
					//$this->ProjectMedia->create();
					$this->ProjectMedia->save($this->request->data['ProjectMedia']);
				}else{
					$this->request->data['ProjectMedia']['project_id'] = $this->request->data['Project']['project_id'];
					$this->ProjectMedia->create();
					$this->ProjectMedia->save($this->request->data['ProjectMedia']);
				}
				
				
				//$this->ProjectMedia->create();
				//echo '<pre>'; print_r($this->request->data['ProjectMedia']);
				//$this->ProjectMedia->save($this->request->data['ProjectMedia']);
			}
//die();
            if (!empty($this->request->data['fileupload'])) {

                foreach ($this->request->data['fileupload'] as $result) {

                    $name = $result['name'];

                    if (!empty($name)) {
                        // Check For Empty Values 
                        $tmprary_name = $result['tmp_name'];
                        $temp = explode(".", $name);
                        $newfilename = uniqid() . '.' . end($temp);
                        if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                            $this->ProjectImage->create();
                            $this->ProjectImage->save(
                                    array(
                                        "project_id" => $this->request->data['Project']['project_id'],
                                        "image_name" => $target_dir . $newfilename,
                                    )
                            );
                        }
                    }
                }
            }
			
			
			
			

            $this->Project->id = $this->request->data['Project']['project_id'];
            $this->Project->save(array(
                "approved" => 1
            ));
        }
		/*echo $this->request->data['Project']['project_id'];die();
		$projectMediaArr = $this->ProjectMedia->find('all',array(
			'conditions' => array(
                'project_id' => $this->request->data['Project']['project_id']
            )
		));
		echo '<pre>'; print_r($projectMediaArr);
		if(!empty($projectMediaArr)){
			$this->request->data['ProjectMedia'] = $projectMediaArr;
		}
		echo '<pre>'; print_r($this->request->data['ProjectMedia']);*/
        $this->redirect('projectsubmit/' . $this->request->data['Project']['project_type']);
		
    }

    public function projectsubmit($type) {
        $this->set('type', $type);
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        $this->layout = 'account';
    }

    public function allprojects($tab = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        if ($tab == '1') {
            $this->Paginator->settings = array(
                'conditions' => array('Project.approved' => '0', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
            $project_user_inner_menu = 1;
        } else if ($tab == '2') {
            $this->Paginator->settings = array(
                'conditions' => array('Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
            $project_user_inner_menu = 2;
        } else if (!empty($this->request->data['search'])) {
            $project_user_inner_menu = -1;
            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'project_name LIKE' => $this->request->data['search'] . '%',
                        'locality LIKE' => $this->request->data['search'] . '%',
                        'city_data LIKE' => $this->request->data['search'] . '%',
                        'state_data LIKE' => $this->request->data['search'] . '%'
                    ),
                    'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('Project.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
            $project_user_inner_menu = 0;
        }
        $allproj = $this->Paginator->paginate('Project');

        foreach ($allproj as $proj) {

            $proj_detail = $this->ProjectDetail->find('all', array(
                'conditions' => array(
                    'project_id' => $proj['Project']['project_id']
                )
            ));

            $proj_pricing = $this->ProjectPricing->find('all', array(
                'conditions' => array(
                    'project_id' => $proj['Project']['project_id']
                )
            ));

            $proj_finacer = $this->ProjectFinancer->find('all', array(
                'conditions' => array(
                    'project_id' => $proj['Project']['project_id']
                )
            ));
            $merge_data = $proj;
            if (!empty($proj_detail['0'])) {
                $merge_data = array_merge($merge_data, $proj_detail['0']);
            }
            if (!empty($proj_finacer[0])) {

                $merge_data = array_merge($merge_data, $proj_finacer['0']);
            }
            if (!empty($proj_pricing['0'])) {
                $merge_data = array_merge($merge_data, $proj_pricing['0']);
            }

            $allprojdata[] = $merge_data;
        }

        $total = $this->Project->find('count', array(
            'conditions' => array(
                'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $saved = $this->Project->find('count', array(
            'conditions' => array(
                'approved' => 0,
                'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $approved = $this->Project->find('count', array(
            'conditions' => array(
                'approved' => 1,
                'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $this->set(compact('project_user_inner_menu'));
        $this->set(compact('approved'));
        $this->set(compact('saved'));
        $this->set(compact('total'));
        $this->set(compact('allproj'));
        $this->set(compact('allprojdata'));
        $this->layout = 'account';
    }
	
	
	
	
	
	public function editproject($codid) {

        $id = base64_decode($codid);
        // print $id; exit;
        if (empty($this->Session->read('Auth.User.id'))) {
            
            $valid = $this->Project->find('all', array(
                'conditions' => array(
                    'project_id' => $id,
                    'current_project' => '1',
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
            if (empty($valid) && empty($this->Session->read('Auth.User.id'))) {
                throw new NotFoundException(__('Invalid project'));
            }
        }
        $this->Project->id = $id;
        if (!$this->Project->exists()) {
            throw new NotFoundException(__('Invalid project'));
        }
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if ($this->request->is('post')) {
            if ($this->Project->save($this->request->data)) {
                $lastinsertId = $this->Project->getLastInsertId();
                echo $lastinsertId;
                die;
            }
        } else {

            $this->request->data = $this->Project->findByProjectId($id);
            $proj_Detail = $this->ProjectDetail->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
            $proj_fina = $this->ProjectFinancer->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));

            $proj_price = $this->ProjectPricing->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
            $proj_OfficeUse = $this->ProjectOfficeUse->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
            $proj_image = $this->ProjectImage->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
			$this->loadModel('ProjectMedia');
			$proj_media = $this->ProjectMedia->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));


            $this->request->data['ProjectDetail'] = @$proj_Detail['0']['ProjectDetail'];
            $this->request->data['ProjectFinancer'] = @$proj_fina['0']['ProjectFinancer'];
            $this->request->data['ProjectPricing'] = @$proj_price['0']['ProjectPricing'];
            $this->request->data['ProjectOfficeUse'] = @$proj_OfficeUse['0']['ProjectOfficeUse'];
            $this->request->data['ProjectImage'] = @$proj_image;
			$this->request->data['ProjectMedia'] = $proj_media[0]['ProjectMedia'];
            if (!empty($proj_price['0']['ProjectPricing']['payment_plan'])) {
                $this->request->data['plan'] = json_decode($proj_price['0']['ProjectPricing']['payment_plan'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bsp_charges'])) {
                $this->request->data['bspcharges'] = json_decode($proj_price['0']['ProjectPricing']['bsp_charges'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesspace'])) {
                $this->request->data['bspchargesspace'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesspace'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesplc'])) {
                $this->request->data['bspchargesplc'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesplc'], 'true');
            }
			//echo '<pre>';print_r($this->request->data);
            if (!empty($proj_price['0']['ProjectPricing']['project_charges'])) {
                $this->request->data['charges'] = json_decode($proj_price['0']['ProjectPricing']['project_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['addition_charges'])) {
                $this->request->data['addition_charges'] = json_decode($proj_price['0']['ProjectPricing']['addition_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['other_charges'])) {
                $this->request->data['othercharges'] = json_decode($proj_price['0']['ProjectPricing']['other_charges'], 'true');
            }

        }

        
        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $banks = $this->MstBank->find('list', array(
            'fields' => array(
                'id',
                'bank_name',
            )
        ));
        $this->set('banks', $banks);
        $this->set('city_master', $city_master);
		
		
		//echo '<pre>'; print_r($this->request->data['ProjectMedia']);
    }
	
	

    public function editproject1($codid) {

        $id = base64_decode($codid);
        // print $id; exit;
        if (empty($this->Session->read('Auth.User.id'))) {
            
            $valid = $this->Project->find('all', array(
                'conditions' => array(
                    'project_id' => $id,
                    'current_project' => '1',
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
            if (empty($valid) && empty($this->Session->read('Auth.User.id'))) {
                throw new NotFoundException(__('Invalid project'));
            }
        }
        $this->Project->id = $id;
        if (!$this->Project->exists()) {
            throw new NotFoundException(__('Invalid project'));
        }
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if ($this->request->is('post')) {
            if ($this->Project->save($this->request->data)) {
                $lastinsertId = $this->Project->getLastInsertId();
                echo $lastinsertId;
                die;
            }
        } else {

            $this->request->data = $this->Project->findByProjectId($id);
            $proj_Detail = $this->ProjectDetail->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
            $proj_fina = $this->ProjectFinancer->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));

            $proj_price = $this->ProjectPricing->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
            $proj_OfficeUse = $this->ProjectOfficeUse->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
            $proj_image = $this->ProjectImage->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));
			$this->loadModel('ProjectMedia');
			$proj_media = $this->ProjectMedia->find('all', array(
                'conditions' => array(
                    'project_id' => $this->request->data['Project']['project_id']
                )
            ));


            $this->request->data['ProjectDetail'] = @$proj_Detail['0']['ProjectDetail'];
            $this->request->data['ProjectFinancer'] = @$proj_fina['0']['ProjectFinancer'];
            $this->request->data['ProjectPricing'] = @$proj_price['0']['ProjectPricing'];
            $this->request->data['ProjectOfficeUse'] = @$proj_OfficeUse['0']['ProjectOfficeUse'];
            $this->request->data['ProjectImage'] = @$proj_image;
			$this->request->data['ProjectMedia'] = $proj_media[0]['ProjectMedia'];
            if (!empty($proj_price['0']['ProjectPricing']['payment_plan'])) {
                $this->request->data['plan'] = json_decode($proj_price['0']['ProjectPricing']['payment_plan'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bsp_charges'])) {
                $this->request->data['bspcharges'] = json_decode($proj_price['0']['ProjectPricing']['bsp_charges'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesspace'])) {
                $this->request->data['bspchargesspace'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesspace'], 'true');
            }
			if (!empty($proj_price['0']['ProjectPricing']['bspchargesplc'])) {
                $this->request->data['bspchargesplc'] = json_decode($proj_price['0']['ProjectPricing']['bspchargesplc'], 'true');
            }
			//echo '<pre>';print_r($this->request->data);
            if (!empty($proj_price['0']['ProjectPricing']['project_charges'])) {
                $this->request->data['charges'] = json_decode($proj_price['0']['ProjectPricing']['project_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['addition_charges'])) {
                $this->request->data['addition_charges'] = json_decode($proj_price['0']['ProjectPricing']['addition_charges'], 'true');
            }
            if (!empty($proj_price['0']['ProjectPricing']['other_charges'])) {
                $this->request->data['othercharges'] = json_decode($proj_price['0']['ProjectPricing']['other_charges'], 'true');
            }

        }

        
        $this->layout = 'account';
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $banks = $this->MstBank->find('list', array(
            'fields' => array(
                'id',
                'bank_name',
            )
        ));
        $this->set('banks', $banks);
        $this->set('city_master', $city_master);
    }

    public function admin_currentproject() {
        $currentproject = $this->Project->find('all', array(
            'conditions' => array(
                'current_project' => 1,
				'project_city !=' =>NULL,
                'approved' => 1
            )
        ));


        $totcurrentproject = array();
        foreach ($currentproject as $current) {


            $proj_finacer = $this->ProjectOfficeUse->find('all', array(
                'conditions' => array(
                    'project_id' => $current['Project']['project_id']
                )
            ));
            if (empty($proj_finacer)) {
                $totcurrentproject[] = $current;
            } else {
                $totcurrentproject[] = array_merge($current, $proj_finacer['0']);
            }
        }

        $this->set('currentproject', $totcurrentproject);
    }

    public function admin_pastproject() {
        $pastproject = $this->Project->find('all', array(
            'conditions' => array(
                'current_project' => 0,
                'approved' => 1
            )
        ));

        $this->set('pastproject', $pastproject);
    }
	
	public function admin_allproject() {
        $allproject = $this->Project->find('all', array(
            'conditions' => array(
                //'current_project' => 0,
				'project_city !=' =>NULL,
               'approved' => 1
            )
        ));

        $this->set('allproject', $allproject);
    }
	
	
	public function projectPricingLists() {
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
        $getProjectsPricingsArr = $this->Project->find('all', array(
			'fields'=> array( 
						'Projectpricing.*',
						'Project.project_name'			
					),
					  'conditions'=>array(
						'approved' => 1
					),
			'joins' => array(array('table' => 'project_pricings',
                        'alias' => 'Projectpricing',
                        'type' => 'INNER',
                        'conditions' => array('Projectpricing.project_id = Project.project_id',
						'Project.user_id' => $this->Session->read('Auth.Websiteuser.id'))))));
						
		//echo '<pre>'; print_r($getProjectsPricingsArr); die();

        $this->set('getProjectsPricingsArr', $getProjectsPricingsArr);
		$this->layout = 'account';
        //$this->set('allproject', $allproject);
    }
	public function getProjectBycity($cityId){
		//print_r($_POST);
			$text=$_POST['search'];
			$projname=$this->Project->find('all', [
				'fields'     => [ 'project_id', 'project_name' ],
				'conditions' => [ 'Project.project_name LIKE' => $text .'%' , 'user_id'=>$this->Session->read('Auth.Websiteuser.id')]
			]);
			
			$prjnamedata=array();
			//$dataa=array();
			//echo '<pre>'; print_r($projname); 
			foreach($projname as $row){
				$prjnamedata[]= array("value"=>$row['Project']['project_name'],"label"=>$row['Project']['project_name'],"project_id"=>$row['Project']['project_id']);
			} 
			//echo '<pre>'; print_r($prjnamedata); die();
			echo json_encode($prjnamedata);
			die();
		}

}

?>