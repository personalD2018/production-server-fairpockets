<?php
	
	/**
		* Static content controller.
		*
		* This file will render views from views/pages/
		*
		* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
		* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		*
		* Licensed under The MIT License
		* For full copyright and license information, please see the LICENSE.txt
		* Redistributions of files must retain the above copyright notice.
		*
		* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.Controller
		* @since         CakePHP(tm) v 0.2.9
		* @license       http://www.opensource.org/licenses/mit-license.php MIT License
	*/
	App::uses('AppController', 'Controller');
	App::uses('CakeEmail', 'Network/Email');
	$title = "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties 11";
	/**
		* Static content controller
		*
		* Override this controller by placing a copy in controllers directory of an application
		*
		* @package       app.Controller
		* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
	*/
	class PagesController extends AppController {
		
		/**
			* This controller does not use a model
			*
			* @var array
		*/
		public $uses = array(
        'Property', 
        'MstAreaState', 
        'MstPropertyTypeOption', 
        'FeatureMasters', 
        'PropertyFeatures', 
        'AmentieMaster', 
        'PropertyPic', 
        'MstAreaRegion','MstAreaState','MstAreaCity', 'MstAreaLocality', 
        'PropertyInquirie', 
        'User', 
        'PropertyNotifiction', 
        'MstAreaCountry', 
        'Project', 
        'Builder', 
        'ProjectDetails', 
        'Bank', 
        'ProjectFinancer', 
        'ProjectPricing', 
        'ProjectOfficeUse', 
        'Builder', 
        'Websiteuser', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState', 
        'FeaturePropertyOptionView', 
        'AmenityPropertyOptionView', 
        'PropertyAmenity',
		'PropertyFeature', 
        'MstAmenity', 
		'MstFeature',
        'VAmenityPropertyOption', 
        'VFeaturePropertyOption',
        'FeatureAminityPropertyOptionView',
		'Lead');
		var $helpers = array("Number");
		public $components = array('Email','Paginator','Propertydata');
		
		/**
		* Displays a view
		* @return void
		* @throws ForbiddenException When a directory traversal attempt.
		* @throws NotFoundException When the view file could not be found
		* or MissingViewException in debug mode.
		*/
				
		public function beforeFilter() {
			parent::beforeFilter();
			// Allow users to register and logout.
			$this->Auth->allow('near','display', 'emi_calc', 'rentvbuy_calc', 'about_us', 'our_team', 'faq', 'research_reports', 'advisory', 'property_management', 'portfolio_management', 'career', 'contact', 'feedback', 'pap', 'tac', 'pricing', 'search_list', 'search_list_detail','search_list_detail_old','recentProperties','builder_property_details','builder_property_details_testing','getbspoptions','getbspoptions1','serverinfo','calculator');
		}
		
		public function serverinfo() {
			
			$Email = new CakeEmail();
		$message = 'hello world';
                        //$Email->config('smtp');
                        $Email->from(array('Support@FairPockets.com' => 'FairPockets Contact')); 
                        $Email->to('rohithp884@gmail.com');
                        $Email->emailFormat('html');
                        $Email->subject('Contact us request');
                        $Email->send($message);
						
						
						
						
						die();
			/*$selected_floor = 0;	
					$this->loadModel('PropertyPossessionCharge');
					$PropertyPossessionChargeTotal = 0;
					//$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
					$PropertyPossessionCharge = $this->PropertyPossessionCharge->query("SELECT * FROM `property_possession_charges` WHERE  property_id = '27378763552343' AND possession_from_floor = '0'");
					//echo '<pre>'; print_r($PropertyPossessionCharge); die();
					if(isset($PropertyPossessionCharge) && $PropertyPossessionCharge != ''){
						$PropertyPossessionChargeTotal = $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
					}
					echo $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
				$property_detail_array = array("property_possession_charge_total1"=>$PropertyPossessionChargeTotal,"property_possession_charge_total2"=>$PropertyPossessionChargeTotal);	
						CakeLog::write('debug', 'getCalculatorDataByFloor'.print_r(json_encode($property_detail_array), true) );
						$this->log('This message will be logged in the database','error');
CakeLog::error('Something horrible happened');
CakeLog::write(LOG_ERR, 'Something horrible happened');						
												print json_encode($property_detail_array); exit;
			$this->loadModel('Property');
			$getPropertyPowerBackup = $this->Property->find('all',
				array('fields'=> array(
						'power_backup_kva'		
					),
					  'conditions'=>array(
						'property_id' => 27378763552343
					)
				)
			);
			//echo $getPropertyPowerBackup[0]['Property']['power_backup_kva'];
			
			$this->loadModel('ProjectSpaceDetail');
			$floorno = 1;
			$prop_id = 27378763552341;
			//$PropertyPossessionChargeArray = $this->PropertyPossessionCharge->query("SELECT * FROM `property_possession_charges` WHERE ".$floorno." BETWEEN `possession_from_floor` AND `possession_to_floor` AND property_id = ".$prop_id."");			
			$propertyLawnArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = ".$prop_id." AND space_from_floor = 0 AND space_mode = 2;");			
			if(isset($propertyLawnArray) && $propertyLawnArray != ''){
				$propertyLawnValueTotal = $propertyLawnArray[0]['project_space_details']['floor_space_area']*$propertyLawnArray[0]['project_space_details']['floor_space_amount'];
			}else{
				$propertyLawnValueTotal = 0;	
			}
			$propertyTerraceArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = ".$prop_id." AND space_from_floor = 0 AND space_mode = 1;");			
			if(isset($propertyTerraceArray) && $propertyTerraceArray != ''){
				$propertyTerraceValueTotal = $propertyTerraceArray[0]['project_space_details']['floor_space_area']*$propertyTerraceArray[0]['project_space_details']['floor_space_amount'];		
			}else{
				$propertyTerraceValueTotal = 0;
			}
			echo $propertyLawnValueTotal;
			echo $propertyTerraceValueTotal;die();
			
			$this->loadModel('PropertyPossessionCharge');
			$floorno = 1;
			$prop_id = 27378763552343;
			//$PropertyPossessionChargeArray = $this->PropertyPossessionCharge->query("SELECT * FROM `property_possession_charges` WHERE ".$floorno." BETWEEN `possession_from_floor` AND `possession_to_floor` AND property_id = ".$prop_id."");			
			$PropertyPossessionChargeArray = $this->PropertyPossessionCharge->query("SELECT possession_amount FROM `property_possession_charges` WHERE ".$floorno." BETWEEN `possession_from_floor` AND `possession_to_floor` AND property_id = ".$prop_id."");			
			
			//$PropertyPossessionChargeValue = $PropertyPossessionChargeArray[0]['property_possession_charges']['possession_amount'];
			echo '<pre>'; print_r($PropertyPossessionChargeArray);die();
			echo $PropertyPossessionChargeArray;die();
			echo '<pre>'; print_r($getPropertyPowerBackup);die();
			
			//phpinfo();
			//$this->loadModel('Project');
			$this->loadModel('Project');
			$this->loadModel('SolrDataApp');*/
			$project_id = 725;
			$this->loadModel('Project');
			$this->loadModel('SolrData');
			/*$this->loadModel('SolrData');
			        $data_arr = $this->SolrData->find('all', array(
            'fields' => array('DISTINCT( CONCAT(configure," - ", builtup_area, " sqft", " / ", properties_id)) AS flat_spec'),
            'conditions' => array(
                'CONCAT(configure," - ", builtup_area, " sqft") IS NOT NULL',
                'Property.project_id = ' . $project_id
                ),
            'order' => array(
                array('configure asc'),
                array('super_builtup_area asc'),
            ),
            'joins' => array(
                array(
                    'table' => 'properties',
                    'alias' => 'Property',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array('Property.property_id = SolrData.properties_id')
                )
            )
        ));*/
		$data_arr1 = $this->SolrData->getData($project_id);
			echo '<pre>'; print_r($data_arr1);die();
			
			/*$getProjects = $this->Project->find('all',
				array('fields'=> array(
						'project_id',
						'project_name'			
					),
					  'conditions'=>array(
						'user_id' => 122
					)
				)
			);
			echo '<pre>'; print_r($getProjects);die();*/
			
			//$data_arr = $this->AppData->getData(921);
			$data_arr = $this->SolrDataApp->getData(921);
			
			echo '<pre>'; print_r($data_arr);die();
			
			$getProjects = $this->Project->find('all', array(
			'fields'=> array( 
						'DISTINCT project_id',
						'project_name'			
					),
			'joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.project_id = Project.project_id',
						'Project.user_id' => 424)))));
			echo '<pre>'; print_r($getProjects);die();
			
		}
		
		public function display() {
			
			$this->set('status', 3);
			$this->set('remember_me_user', $this->Cookie->read('remember_me_user'));
			$this->layout = "home";
			$path = func_get_args();
			
			$title = "India`s #1 Fair Price Curated Property Portal | Buy/Sell/Rent | FairPockets";
			$seo_description = "Explore 1000+ Properties for Sale in Noida on FairPockets.com. Verified Listings, 325+ Projects, 321+ Ready to Move, 512+ Resale, 60+ Dealers, Check Map View, Price & amenities...";
			$seo_keyword = "Real estate Company in Noida, Real estate Company in Delhi, real estate Company in Noida, Best property dealers in Noida, Best property dealers in Delhi, property fair prices";
			
			$count = count($path);
			if (!$count) {
				return $this->redirect('/');
			}
			if (in_array('..', $path, true) || in_array('.', $path, true)) {
				throw new ForbiddenException();
			}
			$page = $subpage = $title_for_layout = null;
			
			if (!empty($path[0])) {
				$page = $path[0];
			}
			if (!empty($path[1])) {
				$subpage = $path[1];
			}
			if (!empty($path[$count - 1])) {
				//$title = "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties";
					}
			
			$this->set(compact('page', 'subpage', 'title', 'seo_description', 'seo_keyword'));
			
			try {
				$this->render(implode('/', $path));
				} catch (MissingViewException $e) {
				if (Configure::read('debug')) {
					throw $e;
				}
				throw new NotFoundException();
			}
		}
		
		/*public function getcity() {

			$this->loadModel('MstAreaCity');
			$city = $this->MstAreaCity->find('all', array(
				'fields' => array(
					'cityName'
				)
			));
			echo '884';print_r($city);die();
			return $city;
			$this->set('city', $city);

		}*/
		
		public function search_list() {
		
			//$this->near();die;
			
			$this->layout = "search";
			include_once(ROOT . DS . "vendors".DS."autoload.php");
			$config = $this->getSolrconfig();
			$client = new Solarium\Client($config);

			// this executes the query and returns the result
			//$resultset = $client->select($query);

			// get a select query instance
			$query = $client->createSelect();
			// set a query (all prices starting from 12)
			$limit = 20;
			$start = 1;
			$page = 0;
			
			if(isset($this->params->query['page']) && $this->params->query['page'] >= 1) {
				$page = ($this->params->query['page'] - 1) * 10; 
				$start = $this->params->query['page'];
				unset($this->params->query['page']);
			}
			
			$this->loadModel('MstAreaCity');
			$city = $this->MstAreaCity->find('list', array(
				'fields' => array(
					'cityCode',
					'cityName',
				)
			));
			//$title ='';
			//Add filters for PROPERTY Name
			if(isset($this->params->query['property_name']) && $this->params->query['property_name']) {
				$search_for = $this->params->query['property_name'];
				
				if($index = $this->stristrarray($city, $search_for)) {
					$title = "Property for ".$search_for;
					$query->setQuery($search_for .', '. $city[$index]);
					//$query->createFilterQuery('property_name')->setQuery('property_name:'.$search_for.'');
					//$query->createFilterQuery('city')->setQuery('city:'.$city[$index].'');
				} else {
					//Add filter for Default location
					if(isset($this->params->query['default_location']) && $this->params->query['default_location']) {
						$default_location = $this->params->query['default_location'];
						$query->createFilterQuery('city')->setQuery('city:'.$default_location.'');
						$query->setQuery($search_for);
						$title = "Property for ".$default_location;
					}else{
						$query->setQuery($search_for);
						$title = "Property ".$search_for;
					}
				}
			}else{
				//Add filter for Default location
				if(isset($this->params->query['default_location']) && $this->params->query['default_location']) {
					$default_location = $this->params->query['default_location'];
					$query->createFilterQuery('city')->setQuery('city:'.$default_location.'');
					$title = "Property for ".$default_location;
				}
			}
			
			//Add filters for TRANSACTION TYPE
			if(isset($this->params->query['transaction_type'])) {
				$transaction_type = $this->params->query['transaction_type'];
				if($transaction_type == 'buy') {
					$transaction_type = 'sell';
				}
				$title .= " / Property for ".$transaction_type;
				if($transaction_type == 'posted_by') {
				$query->createFilterQuery('transaction_type')->setQuery('posted_by:Builder');
				}else{
				$query->createFilterQuery('transaction_type')->setQuery('transaction_type:'.$transaction_type.'');
				}
			}
			
			//Add filters for PROPERTY TYPE
			if(isset($this->params->query['property_type'])) {
				$property_type_query = 'property_type:('.implode(' OR ', $this->params->query['property_type']).')';
				$query->createFilterQuery('property_type')->setQuery($property_type_query);
				$title .= " / Property for ".$property_type_query;
			}
			
			//Filter data based on applied filters
			
			if($this->request->is('ajax')) {
				if(isset($this->params->query['filters'])) {
					$filters = $this->createFilters($this->params->query['filters']);
					foreach($filters as $field => $value){
						$query->createFilterQuery($field)->setQuery($field.':'.$value);
					}
					//$filters = json_decode($this->params->query['filters']);
				
					//pr($filters);die;
				}
				
				$this->layout = "ajax";
				$this->view = 'search_list_ajax';
			}
			if(isset($this->params->query['sortby']))
			{
			if($this->params->query['sortby'] == 0)
			{
				$this->params->query['sortby'] = 'score';
			}
			}
			if(isset($this->params->query['sortby']) && $this->params->query['sortby'] != 'score') {
				$sortby = $this->params->query['sortby'];
				// sort the results by price ascending
				$options = array();
				$options = array(
					'date'				=> array('post_date', 'DESC'),
					'discount' 			=> array('discount_percent', 'DESC'),
					'offer_price_low' 	=> array('offer_price', 'ASC'),
					'offer_price_high' 	=> array('offer_price', 'DESC'),
					'offer_price_sqft_low' 	=> array('offer_price_sqft', 'ASC'),
					'offer_price_sqft_high' => array('offer_price_sqft', 'DESC')
				);
				$query->addSort($options[$sortby][0], $options[$sortby][1]);
			}
			
			$query->addSort('score', $query::SORT_DESC);
			
			// get the facetset component
			$facetSet = $query->getFacetSet();
			
			// create a facet field instance and set options
			$facetSet->createFacetField('Society')->setField('project_name');
			$facetSet->createFacetField('Locality')->setField('sublocality1');
			//$facetSet->setLimit(-1);
			
			$query->setStart($page)->setRows($limit);
			
			// this executes the query and returns the result
			try{
				$result = $client->select($query);
			/*
				$prop = [];
				
				$this->loadModel('NewProperty');
				$this->loadModel('WebsiteUser');
					
				foreach($result as $data) {
					
					//For user add into website user table
					$user = $this->WebsiteUser->checkUserExists($data);
					// Insert properties into database on the basis on user phone no validation.
					
					
					
					$prop[] = [
							'user_id' 			=> $user['id'],
							'employee_id' 		=> '0',
							'posted_by' 		=> $user['role'],
							'project_id' 		=> $data['id'],
							'posted_by' 		=> $data['posted_by'],
							'transaction_type' 	=> $data['transaction_type'],
							'property_type' 	=> $data['property_type'],
							'city' 				=> $data['city'],
							'project_name' 		=> $data['project_name'],
							'super_builtup_area' => $data['super_builtup_area'],
							'builtup_area' 		=> $data['builtup_area'],
							'configure' 		=> $data['configure'],
							'total_floors' 		=> $data['total_floors'],
							'on_floor' 			=> $data['on_floor'],
							'covered_parking' 	=> $data['covered_parking'],
							'offer_price' 		=> $data['offer_price'],
							'offer_price_sqft' 	=> $data['offer_price_sqft'],
							'market_price' 		=> $data['market_price'],
							'market_price_sqft' => $data['market_price_sqft'],
							'discount' 			=> $data['discount'],
							'discount_sqft' 	=> $data['discount_sqft'],
							'discount_percent' 	=> $data['discount_percent'],
							'availability' 		=> $data['availability'],
							'sale_type' 		=> $data['sale_type'],
							'posession_date' 	=> $data['posession_date'],
							'amenities' 		=> json_encode($data['amenities']),
							'power_backup' 		=> $data['power_backup'],
							'water_source' 		=> $data['water_source'],
							'facing' 			=> $data['facing'],
							'flooring_type' 	=> $data['flooring_type'],
							'furnishings' 		=> $data['furnishings'],
							'posted_by_user_name' 		=> $data['posted_by_user_name'],
							'posted_by_user_contact' 	=> json_encode($data['posted_by_user_contact']),
							'score' 			=> $data['score'],
							'post_date' 		=> $data['post_date'],
							'sublocality1' 		=> $data['sublocality1'],
							'sublocality2' 		=> $data['sublocality2'],
							'sublocality3' 		=> $data['sublocality3'],
							'age_of_property' 		=> $data['age_of_property'],
						];
						
						//break;
					}
					
					//pr($prop);die;
				$this->NewProperty->saveMany($prop);
				$sss = $this->NewProperty->getInsertID();
				echo '++++'.$sss;
				die;*/
			} catch(Exception $e){  //catch exception
				echo 'Message: ' .$e->getMessage();
			}
				//echo '<pre>';print_r($result);die();
			$this->set(compact('result', 'limit', 'start', 'title'));
			
		}

		private function stristrarray($array, $str){
			//This array will hold the indexes of every
			//element that contains our substring.
			//$indexes = array();
			//$str = '';
			//$v = '';
			foreach($array as $k => $v){
				//If stristr, add the index to our
				//$indexes array.
				if($v){
				if(stristr($str, $v)){
					return $k;
				}
				}
			}
			return false;
		}
		
		private function createFilters() {
			$filters = json_decode($this->params->query['filters']);
			$sim_fil = [
				'sublocality1'	=> 'locality-filters', 
				'project_name' 	=> 'society-filters', 
				'sale_type' 	=> 'sale-type-filters', 
				'furnishings' 	=> 'furnishing-filters', 
				'amenities' 	=> 'amenities-filters',
				'configure'		=> 'bedroom-filters',
				'posted_by'		=> 'posted-by-filters',
				'availability'  => 'availability-filters',
				'age_of_property' 	=> 'age_of_property-filters',
				'posession_date' 	=> 'posession_date-filters'
			];
			
			$fil = [];
			foreach($filters as $name => $values) {
				if(count($values)) {
					if($key = array_search($name, $sim_fil)) {
						$fil[$key] = '("'.implode('" OR "', $values).'")';
					} else {
						$fil[$name] = '[' . $values . ']';
					}
				}
			}
			
			return $fil;
		}
		
		public function recentProperties($location) {
			$this->layout = "ajax";
			
			
			include_once(ROOT . DS . "vendors".DS."autoload.php");
			$config = $this->getSolrconfig();
			
			$client = new Solarium\Client($config);
			
			// get a select query instance
			$query = $client->createSelect();
			
			// set a query (all prices starting from 12)
			$limit = 10;
			$page = 0;
				
			$query->createFilterQuery('city')->setQuery('city:'.$location.'');
			$query->setStart($page)->setRows($limit);
			
			// this executes the query and returns the result
			try{
				$result = $client->select($query);
			}
			//catch exception
			catch(Exception $e){
				echo 'Message: ' .$e->getMessage();
			}	
			
			//pr($result);die;
			$this->set(compact('result'));
		}

		public function near() {
			$this->layout = false;
			//$this->autoRender = false;
			include_once(ROOT . DS . "vendors".DS."autoload.php");
			$config = $this->getSolrconfig();
			
			$client = new Solarium\Client($config);	
			// get a morelikethis query instance
			$query = $client->createMoreLikeThis();

			$query->setQuery('id:PUN_1');
			$query->setMltFields('manu,cat');
			$query->setMinimumDocumentFrequency(1);
			$query->setMinimumTermFrequency(1);
			$query->createFilterQuery('stock')->setQuery('inStock:true');
			$query->setInterestingTerms('details');
			$query->setMatchInclude(true);

			// this executes the query and returns the result
			$resultset = $client->select($query);

			pr($resultset);die;

			// display the total number of MLT documents found by solr
			echo 'Number of MLT matches found: '.$resultset->getNumFound().'<br/><br/>';
			echo '<b>Listing of matched docs:</b>';

			// show MLT documents using the resultset iterator
			foreach ($resultset as $document) {

				echo '<hr/><table>';

				// the documents are also iterable, to get all fields
				foreach ($document as $field => $value) {
					// this converts multivalue fields to a comma-separated string
					if (is_array($value)) {
						$value = implode(', ', $value);
					}

					echo '<tr><th>' . $field . '</th><td>' . $value . '</td></tr>';
				}

				echo '</table>';
			}

		}
		
		
		public function search_list_detail_old($property_id = null) {
			//$property_id = $this->params->query['fpid'];
			//$this->params->query['fpid'];
				$this->layout = "home";
			$id = base64_decode($property_id);
        $propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);
		echo $id;
		
		//$all_property_features = '';
		//$all_property_features_unit = '';

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();
		
		//$all_property_amenities = '';
		//$all_property_amenities_unit = '';

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    ' 	localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);



        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		
		//echo '<pre>'; print_r($propertyImages);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        if (!empty($projectdetail['0'])) {
            $this->set('projectdetail', $projectdetail['0']);
        }
        if (!empty($project['0'])) {
            $this->set('project', $project['0']);
            $this->set('propertyImages', $propertyImages);
        }
        $this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
			
		}
		
		
		
		
		public function search_list_detail($property_id = null) {
			$property_id = $this->params->query['fpid'];
			//$this->params->query['fpid'];
				$this->layout = "home";
			$id = base64_decode($property_id);
        $propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);
		//echo $id;
		
		//$all_property_features = '';
		//$all_property_features_unit = '';

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();
		
		//$all_property_amenities = '';
		//$all_property_amenities_unit = '';

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    ' 	localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);



        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		
		//echo '<pre>'; print_r($propertyImages);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        if (!empty($projectdetail['0'])) {
            $this->set('projectdetail', $projectdetail['0']);
        }
        if (!empty($project['0'])) {
            $this->set('project', $project['0']);
            $this->set('propertyImages', $propertyImages);
        }
        $this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
			
		}
		
		public function builder_property_details($property_id = null) {
				$this->layout = "home";
			$id = base64_decode($property_id);
			
			$propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);die();
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    'localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_financer = $this->ProjectFinancer->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_pricing = $this->ProjectPricing->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $Websiteuser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'id' => $propertydetails['Property']['user_id']
            )
        ));
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $propertydetails['Property']['user_id']
            )
        ));
        $pastproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'user_id' => $propertydetails['Property']['user_id'],
                'approved' => '1',
                'current_project' => '0'
            )
        ));
        $pastprojectdata = (implode($pastproject, ','));
		
		
		$this->loadModel('ProjectImage');
		$proj_image = $this->ProjectImage->find('all', array(
                'conditions' => array(
                    'project_id' => $propertydetails['Property']['project_id']
                )
            ));
			
			$this->set('proj_image', $proj_image);
			
		$this->loadModel('ProjectOfficeUse');
		$proj_office = $this->ProjectOfficeUse->find('all', array(
                'conditions' => array(
                    'project_id' => $propertydetails['Property']['project_id']
                )
            ));
			
			$this->set('proj_office', $proj_office);
		//echo '<pre>'; print_r($proj_office);
		
        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
            ));
		$all_property_features = array();
		$all_property_features_unit = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
			$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
		}
		
		
        $this->set('pastprojectdata', $pastprojectdata);
        $this->set('Websiteuser', $Websiteuser['0']);
        if(!empty($builder))
		{
        	$this->set('builder', $builder['0']);
		}
		else
		{
			$this->set('builder', $builder);
		}
		if(!empty($project_pricing))
		{
        	$this->set('project_pricing', $project_pricing['0']);
		}
		else
		{
			$this->set('project_pricing', $project_pricing);
		}
        if(!empty($project_financer))
		{
        	$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
		}
		else
		{
			$this->set('project_financer', $project_financer);
		}
		
        
		if(!empty($projectdetail))
		{
        	$this->set('projectdetail', $projectdetail['0']);
		}
		else
		{
			$this->set('projectdetail', $projectdetail);
		}
        
        $this->set('propertyImages', $propertyImages);
		if(!empty($propertyoption))
		{
        	$this->set('propertyoption', $propertyoption['0']);
		}
		else
		{
			$this->set('propertyoption', $propertyoption);
		}
        $this->set('propertydetails', $propertydetails);
		
        $this->set('propertyfeaturesunit', $all_property_features_unit);
        $this->set('project', $project['0']);
		
		
        //$this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
		
		$this->loadModel('ProjectBspDetail');
		$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$propertydetails['Property']['project_id']."");			
		//echo '<pre>';print_r($bsp_limit);
        $this->set('bsp_limit', $bsp_limit );

		$this->loadModel('ProjectSpaceDetail');
		$ProjectSpaceDetail = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = ".$propertydetails['Property']['property_id']." and space_from_floor = 1");			
		//echo '<pre>';print_r($ProjectSpaceDetail);
		$prop_space_area = $ProjectSpaceDetail[0]['project_space_details']['floor_space_area'];
		$prop_space_amount = $ProjectSpaceDetail[0]['project_space_details']['floor_space_amount'];
		$prop_space_mode = $ProjectSpaceDetail[0]['project_space_details']['space_mode'];
        $this->set('prop_space_area', $prop_space_area );
        $this->set('prop_space_amount', $prop_space_amount );	
		$this->set('prop_space_mode', $prop_space_mode );

			$this->loadModel('ProjectPlcDetail');
			$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$propertydetails['Property']['project_id']." AND plc_floor_no = 1");
			if(!empty($plc_price))
			{
				$this->set('project_plc_amount', $plc_price[0]['project_plc_details']['plc_floor_charge'] );
			}else{
				$this->set('project_plc_amount', 0);
				
			}	
	   
			
		}
		
		
		public function builder_property_details_testing($property_id = null) {
				$this->layout = "home";
			$id = base64_decode($property_id);
			
			$propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);die();
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    'localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_financer = $this->ProjectFinancer->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_pricing = $this->ProjectPricing->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $Websiteuser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'id' => $propertydetails['Property']['user_id']
            )
        ));
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $propertydetails['Property']['user_id']
            )
        ));
        $pastproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'user_id' => $propertydetails['Property']['user_id'],
                'approved' => '1',
                'current_project' => '0'
            )
        ));
        $pastprojectdata = (implode($pastproject, ','));
		
		
		$this->loadModel('ProjectImage');
		$proj_image = $this->ProjectImage->find('all', array(
                'conditions' => array(
                    'project_id' => $propertydetails['Property']['project_id']
                )
            ));
			
			$this->set('proj_image', $proj_image);
			
		$this->loadModel('ProjectOfficeUse');
		$proj_office = $this->ProjectOfficeUse->find('all', array(
                'conditions' => array(
                    'project_id' => $propertydetails['Property']['project_id']
                )
            ));
			
			$this->set('proj_office', $proj_office);
		//echo '<pre>'; print_r($proj_office);
		
        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
            ));
		$all_property_features = array();
		$all_property_features_unit = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
			$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
		}
		
		
        $this->set('pastprojectdata', $pastprojectdata);
        $this->set('Websiteuser', $Websiteuser['0']);
        if(!empty($builder))
		{
        	$this->set('builder', $builder['0']);
		}
		else
		{
			$this->set('builder', $builder);
		}
		if(!empty($project_pricing))
		{
        	$this->set('project_pricing', $project_pricing['0']);
		}
		else
		{
			$this->set('project_pricing', $project_pricing);
		}
        if(!empty($project_financer))
		{
        	$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
		}
		else
		{
			$this->set('project_financer', $project_financer);
		}
		
        
		if(!empty($projectdetail))
		{
        	$this->set('projectdetail', $projectdetail['0']);
		}
		else
		{
			$this->set('projectdetail', $projectdetail);
		}
        
        $this->set('propertyImages', $propertyImages);
		if(!empty($propertyoption))
		{
        	$this->set('propertyoption', $propertyoption['0']);
		}
		else
		{
			$this->set('propertyoption', $propertyoption);
		}
        $this->set('propertydetails', $propertydetails);
		
        $this->set('propertyfeaturesunit', $all_property_features_unit);
        $this->set('project', $project['0']);
		
		
        //$this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
		
		$this->loadModel('ProjectBspDetail');
		$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$propertydetails['Property']['project_id']."");			
		//echo '<pre>';print_r($bsp_limit);
        $this->set('bsp_limit', $bsp_limit );		
	   
			
		}
		
		
		
		public function calculator($property_id = null) {
				$this->layout = "home";
				$id = base64_decode($property_id);			
				$propertydetails = $this->Property->findByPropertyId($id);
				
				$propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);
				foreach($propertyFeatureData as $property_val)
				{
					$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
					$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
				}
				$this->set('all_property_features', $all_property_features);
				$this->set('all_property_features_unit', $all_property_features_unit);
	  

       

				$project = $this->Project->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));
				$projectdetail = $this->ProjectDetails->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));

				$project_financer = $this->ProjectFinancer->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));

				$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $propertydetails['Property']['project_id']
					)
				));				
						
				
				$property_features = $this->PropertyFeature->find('all', array(
						'conditions' => array(
							'properties_id' => $id
						)
					));
				$all_property_features = array();
				$all_property_features_unit = array();
				foreach($property_features as $property_val)
				{
					$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
					$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
				}
		
		
				
				if(!empty($project_pricing))
				{
					$this->set('project_pricing', $project_pricing['0']);
				}
				else
				{
					$this->set('project_pricing', $project_pricing);
				}
				if(!empty($project_financer))
				{
					$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
				}
				else
				{
					$this->set('project_financer', $project_financer);
				}
				
				
				if(!empty($projectdetail))
				{
					$this->set('projectdetail', $projectdetail['0']);
				}
				else
				{
					$this->set('projectdetail', $projectdetail);
				}
				
				$this->set('propertydetails', $propertydetails);
				
				$this->set('propertyfeaturesunit', $all_property_features_unit);
				$this->set('project', $project['0']);
				
				$this->set('propertydetails', $propertydetails);
				
				$this->loadModel('ProjectBspDetail');
				$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$propertydetails['Property']['project_id']."");			
				//echo '<pre>';print_r($bsp_limit);
				$this->set('bsp_limit', $bsp_limit );		
	   
			
		}
		
		/*public function search_list_detail($property_id = null) {
			$this->layout = "home";
			//echo $property_id;die;
			if($property_id != null) {
				
				
				$config = $this->getSolrconfig();
				
				include_once(ROOT . DS . "vendors".DS."autoload.php");	
				$client = new Solarium\Client($config);
				
				
				$query = $client->createSelect();
				
				
				// get a select query instance
				$query = $client->createSelect();

				
				// set a query (all prices starting from 12)
				$search_for = "id:".base64_decode($property_id);

					$this->loadModel('PropertyPic');
					$propertyImages = $this->PropertyPic->find('list', array(
						'fields' => array(
							'id',
							'pic',
						),
						'conditions' => array(
							'property_id' => base64_decode($property_id)
						)
					));
					$this->set('propertyImages', $propertyImages);
				
				
				// add a query and morelikethis settings (using fluent interface)
				$query->setQuery($search_for)
					  ->getMoreLikeThis()
					  ->setFields('project_name,builder_name,sublocality1,sublocality2,sublocality3')
					  ->setMinimumDocumentFrequency(1)
					  ->setMinimumTermFrequency(1);

				// this executes the query and returns the result
				
				
				try{
				
					$result = $client->select($query);
					$similar = $result->getMoreLikeThis(); 
				
					//$result = $client->select($query);
					
				}
				//catch exception
				catch(Exception $e){
					echo 'Message: ' .$e->getMessage();
				}	
				$this->set(compact('result', 'similar'));
				//$this->set('result', $resultset);
			}
		}*/
		
		public function emi_calc() {
			$this->layout = "home";
			$title = "Home Loan EMI Calculator | Calculate Monthly Bills | FairPockets";
			$seo_description = "Home Loan EMI Calculator at FairPockets enables you to get a complete break-up of your EMI & your repayment schedule at a click. Means you can easily calculate monthly instalments for your home loan while purchasing residential flats in Noida.";
			$seo_keyword = "EMI Calculator, EMI Calculation Formula, EMI Calculator Home Loan, Home Loan Eligibility Calculator, Home Loan EMI Calculator FairPockets";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function rentvbuy_calc() {
			$this->layout = "home";
			$title = "Should I rent or buy a home? | Analysis by FairPockets";
			$seo_description = "Confused about, should you rent or buy a home for you? Rental vs buying analysis is available here on FairPockets portal. Calculate renting & buying costs to get best flats in Noida.";
			$seo_keyword = "Rent vs buy calculator Noida, Rent vs buy decision Delhi, FairPockets rent vs buy calculator, FairPockets rent vs buy analysis, buying property vs renting, Renting vs buying: right decision";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function about_us() {
			$title = "About FairPockets | Real Estate Price Portal | Best Property Dealers in Noida";
			$seo_description = "Information featured on FairPockets portal page is tailored for real estate audiences. Explore 1000 + properties for Buy, Sell, Rent & List best Real Estate properties in Noida by property Owners, Dealers, Builders & Certified Real Estate Agents";
			$seo_keyword = "best real estate Company in Noida, Best real estate Company in Delhi, Best property dealers in India, Best property dealers in Noida, Top real estate service in Noida";

			$this->layout = "search";
			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function our_team() {
			$this->layout = "home";
			//$title = "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties";
			//$this->set('title', $title);
		}
		
		public function faq() {
			$this->layout = "home";
			$title = "Frequently Asked Questions | Real Estate Companies Noida | FairPockets";
			$seo_description = "FAQ page on FairPockets portal includes answers to all your queries related to real estate companies in Noida/NCR. Here at this portal, all the faq have been put together to help you in buying, selling or renting residential property in Noida at fair price.";
			$seo_keyword = "FAQ real estate questions in India, FAQ real estate questions in Noida, FAQ before buying a flat, Real estate market questions, frequently asked questions when selling a home, FAQ on real estate project";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function research_reports() {
			$this->layout = "home";
			$title = "Fairpockets | Find the property reports with assessment";
			$this->set('title', $title);
		}
		
		public function advisory() {
			$this->layout = "search";
			$title = "Searching your prospective buyer or seller?? Go FairPockets";
			$seo_description = "Search best prices for thousands of on-sale & rental listings only at FairPockets. Explore home values & connect with local professionals just at a single tap.";
			$seo_keyword = "Assist to buy real estate, Assist to sell real estate, Real estate Company in Noida, Real estate Company in Delhi, real estate Company in Noida, Best property dealers in Noida, Best property dealers in Delhi, property fair prices.";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function property_management() {
			$this->layout = "search";
			$title = "Best Property Management Services in Noida/NCR | FairPockets";
			$seo_description = "FairPockets is a unique fair price real estate portal which deals in the property management services in Noida. The firm is dedicated to provide a fair deal to both the buyers & sellers with a peace of mind through its distinct services.";
			$seo_keyword = "Property Management Services, Property Management Services in Noida/NCR, Top Property Management Company in Noida, Fair Price Property Management Company in Noida, Best Property Management Company in India, Property Management in Noida, Property Management in India.";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function portfolio_management() {
			$this->layout = "search";
			$title = "Best Portfolio Management Services in Noida/NCR | FairPockets";
			$seo_description = "FairPockets is a real estate portal in Noida/NCR which provides its customers a fully customized investment portfolio managed by professional money managers to suit the investment purpose.";
			$seo_keyword = "Portfolio Management Services in Delhi, Portfolio Management Services in Noida/NCR, Top Portfolio Management Company in Noida, Top Portfolio Management Company in India, Top Portfolio Management Company in Delhi, Portfolio Management Services Noida.";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function career() {
			//header('Content-Type: text/plain; charset=utf-8');
			$this->loadModel('Career');
			$this->layout = "search";
			$title = "Rest Estate Jobs in Noida/NCR � 2018 | FairPockets";
			$seo_description = "Real estate jobs available in Noida/NCR at FairPockets, a digital platform simplifying the process of home search across India. Founder & CEO � Ritesh Anand, Co-founder � Rumki Sengupta & Advisor � R.Sundar.";
			$seo_keyword = "Jobs in real estate company in Noida, Jobs in real estate company in Delhi, Real estate in Noida, Accountant job in real estate company in Noida, Accountant job in real estate company in Delhi, Real estate company in Noida, Best real estate company in India, Best real estate company in Noida";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
			if(!empty($this->request->data)){
				//print_r($_FILES);die();
				$target_dir = 'upload/career_images/';				
				//echo $target_dir; print_r($this->request->data);die();				
				if (!empty($this->request->data['Career']['resume_path'])) {
					$name = $this->request->data['Career']['resume_path']['name'];
					
					if (!empty($name)) {
						// Check For Empty Values 
						$tmprary_name = $this->request->data['Career']['resume_path']['tmp_name'];
						$temp = explode(".", $name);
						$newfilename = uniqid() . '.' . end($temp);
						if(move_uploaded_file($tmprary_name, $target_dir . $newfilename)){
							$this->request->data['Career']['status'] = 1;
							$this->request->data['Career']['resume_path'] = $name;
							if ($this->Career->save($this->request->data)) {				
								$this->Session->setFlash('Thanks for contacting Us, We will get back to you shortly.', 'default', array('class' => 'green'));
								unset($this->request->data);
							}
						}else{
							$this->Session->setFlash('Document file not uploded due to big size,please use small size document file.', 'default', array('class' => 'green'));
						}
					}
				}				
			}
		}
		
		public function contact() {
			
			$this->loadModel('Contact');
			$this->layout = "search";
			$title = "Fairpockets | Feel free to contact us";
			//print_r($this->request->data);die();
			if(!empty($this->request->data)){
				
				//print_r($this->request->data);die();
				if ($this->Contact->validates()) {//this ensures Model vaidation is applied along with error messages coming from model
					if ($this->Contact->save($this->request->data)) {				
						$this->Session->setFlash('Thanks for contacting Us, We will get back to you shortly.', 'default', array('class' => 'green'));

						//Sending email
						$message = 'There is a request from ' . $this->request->data['Contact']['name'];
						$message .= '<p>Email: ' . $this->request->data['Contact']['email'] . '</p>';
						$message .= '<p>Mobile: ' . $this->request->data['Contact']['mobile'] . '</p>';
						$message .= '<p>Subject: ' . $this->request->data['Contact']['subject'] . '</p>';
						$message .= '<p>Message: ' . $this->request->data['Contact']['message'] . '</p>';
                        
                        $Email = new CakeEmail();
                        $Email->config('smtp');
                        $Email->from(array('Support@FairPockets.com' => 'FairPockets Contact')); 
                        $Email->to('info@fairpockets.com');
                        $Email->emailFormat('html');
                        $Email->subject('Contact us request');
                        $Email->send($message);
						//End Sending email
						unset($this->request->data['Contact']);
					} 
				}
			}			
			$this->set('title', $title);
		}
		
		public function feedback() {
			//phpinfo();
			$this->layout = "search";
			$title = "Fairpockets | Share your valuable feedbacks with us";
			$this->set('title', $title);
			
			$this->loadModel('Feedback');
			
			if(!empty($this->request->data)){				
				//echo $target_dir; print_r($this->request->data);die();				
					//$this->request->data['Feedback']['name'] = $this->request->data['Feedbacks']['name'];
					//$this->request->data['Feedback']['email'] = $this->request->data['Feedbacks']['email'];
					//$this->request->data['Feedback']['feedback_comments'] = $this->request->data['Feedbacks']['feedback'];
					$this->request->data['Feedback']['status'] = 1;
					if ($this->Feedback->save($this->request->data)) {				
						$this->Session->setFlash('Thanks for Feedback, We will get back to you shortly.', 'default', array('class' => 'green'));
						unset($this->request->data);
				}				
			}
		}
		
		public function pap() {
			$this->layout = "home";
			$title = "Fairpockets | Privacy and Policy";
			$this->set('title', $title);
		}
		
		public function tac() {
			$this->layout = "home";
			$title = "Fairpockets | Terms & Conditions";
			$this->set('title', $title);
		}
		
		public function pricing() {
			$this->layout = "search";
			$title = "Home Pricing Strategy | Flats Price in Noida| FairPockets";
			$seo_description = "We at FairPockets portal, believes that there is a science or psychology for calculating pricing for your property. Based on that science, we ensure you of providing assistance in buying or renting of commercial or residential property in Noida.";
			$seo_keyword = "Real estate pricing psychology, FairPockets real estate price range, How to price to house to sell, How to price house to sell fast, How to price house to rent fast, Importance of real estate pricing, Strategies for setting a price for your home, How to price residential real estate, Listing price real estate";

			$this->set(compact('title', 'seo_description', 'seo_keyword'));
		}
		
		public function fp_pgform() {
			$this->layout = "account";
			$title = "Fairpockets | User Dashboard";
			$this->set('title', $title);
		}
		
		public function fp_pgsuccess() {
			$this->layout = "account";
			$title = "Fairpockets | User Dashboard";
			$this->set('title', $title);
		}
		
		public function fp_pgfailure() {
			$this->layout = "account";
			$title = "Fairpockets | User Dashboard";
			$this->set('title', $title);
		}
		
		public function getbspoptions() {
			$this->autoRender = false;
			//print_r($this->request->data);die();
			$this->loadModel('ProjectBspDetail');
			$bspoptionid = $this->request->data['id'];
			$projectid = $this->request->data['projectid'];
			$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$bspoptionid." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
			return $bsp_price[0]['project_bsp_details']['bsp_charges'];
			$this->layout = "ajax";
			exit();
		}
		
		public function getbspoptions1() {
			$this->autoRender = false;
			//print_r($this->request->data);
			$this->loadModel('ProjectBspDetail');
			$this->loadModel('ProjectPlcDetail');
			$bspoptionid = $this->request->data['id'];
			$projectid = $this->request->data['projectid'];
			$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$bspoptionid." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");
			$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
			//return $plc_price[0]['plc_floor_charge']['plc_floor_charge'];
			//print_r($plc_price);die();
			if($plc_price[0]['project_plc_details']['plc_floor_charge']!=''){
				$floor_plc_price = $plc_price[0]['project_plc_details']['plc_floor_charge'];
			}else{
				$floor_plc_price = 0;
			}
			//echo $floor_plc_price;
			return json_encode(array(
						 'bsp_charge' => $bsp_price[0]['project_bsp_details']['bsp_charges'],
						 'plc_charge' => $floor_plc_price
					 ));
			$this->layout = "ajax";
			exit();
		}
		
	}
