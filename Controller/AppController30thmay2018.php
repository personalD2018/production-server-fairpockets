<?php

App::uses('AuthComponent', 'Controller/Component');

// app/Controller/AppController.php
class AppController extends Controller {

    //...

    /* public $components = array(
      'Session','Cookie',
      'Flash',
      'Auth' => array(

      'authorize' => array('Controller') // Added this line
      )
      ); */
    public $uses = array('PropertyNotifiction', 'BuilderApprovalNotifiction', 'ServiceNotifiction');
    public $helpers = array('Html', 'Form', 'Js', 'Cache', 'Session');
    public $components = array('DebugKit.Toolbar', 'Auth' => array(
            'authorize' => array('Controller') // Added this line
        ),
        'Session', 'RequestHandler', 'Flash', 'Cookie');

    public function beforeFilter() {

        Security::setHash('blowfish');
        $this->Auth->allow('index');
        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
            $notifiction = $this->PropertyNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id'),
                    'inquiry_id !=' => 0
                )
            ));

            $propertyapproval = $this->PropertyNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id'),
                    'inquiry_id' => '0'
                )
            ));
            //get builder approval notifiction

            $buildernotifiction = $this->BuilderApprovalNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id')
                )
            ));

            $servicenotifiction = $this->ServiceNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id')
                )
            ));
            $this->set('servicenotifiction', $servicenotifiction);
            $this->set('buildernotifiction', $buildernotifiction);
            $this->set('notifiction', $notifiction);
            $this->set('propertyapproval', $propertyapproval);
            $this->layout = 'admin';
            $this->Auth->authorize = 'Controller';
            AuthComponent::$sessionKey = 'Auth.User';
            $this->Auth->authenticate = array('Form' => array('fields' => array('username' => 'email', 'password' => 'password'), 'passwordHasher' => 'Blowfish', 'userModel' => 'User'));

            $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => true);
            $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'dashboard', 'admin' => true);
            $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
        } else {
            AuthComponent::$sessionKey = 'Auth.Websiteuser';
            $this->Auth->authorize = 'Controller';
            $this->Auth->authenticate = array('Form' => array('fields' => array('username' => 'email', 'password' => 'password'), 'passwordHasher' => 'Blowfish', 'userModel' => 'Websiteuser', 'scope' => array('Websiteuser.useractive' => '1')));

            $this->Auth->loginAction = array('controller' => 'websiteusers', 'action' => 'login');
            $this->Auth->loginRedirect = array('controller' => 'websiteusers', 'action' => 'index');
            $this->Auth->logoutRedirect = array('controller' => 'websiteusers', 'action' => 'index');
        }
    }

    public function isAuthorized($user) {
        return true;
        /*

          // Admin can access every action
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '0') {

          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '1') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '2') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '3') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '4') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '5') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '6') {
          return true;
          }
          // Default deny
          return false;
         */
    }

    public function send_mail($email_data = null) {
        //echo "<pre>";print_r($email_data);die;
        //echo WWW_ROOT;die;
        $email = new CakeEmail('default');
        $email_to = $email_data['to'];
        $email_msg = $email_data['body'];
        $email_subject = $email_data['subject'];

        $email->to($email_to);
        $email->subject($email_subject);
        $mail_status = @$email->send($email_msg);

        if (!$mail_status) {
            return FALSE;
        }
        return TRUE;
    }
	
	protected function getSolrconfig() {
		return $config = array(
			"endpoint" => array(
				"localhost" => array(
					"host"=>"fairpockets.com",
					"port"=>"8983", 
					//"path"=>"/solr/farpkt_solr_data_prod"
					//"core"=>"aliveasset"
					"path"=>"/solr", 
					"core"=>"farpkt_solr_data_prod"
				)
			)
		);
	}
}

?>