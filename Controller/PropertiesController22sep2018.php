<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
//App::import('Controller', 'Countries');
//$countries = new CountriesController;
//$countries->constructClasses();

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PropertiesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array(
        'Property', 
        'MstAreaState', 
        'MstPropertyTypeOption', 
        'FeatureMasters', 
        'PropertyFeatures', 
        'AmentieMaster', 
        'PropertyPic', 
        'MstAreaRegion','MstAreaState','MstAreaCity', 'MstAreaLocality', 
        'PropertyInquirie', 
        'User', 
        'PropertyNotifiction', 
        'MstAreaCountry', 
        'Project', 
        'Builder', 
        'ProjectDetails', 
        'Bank', 
        'ProjectFinancer', 
        'ProjectPricing', 
        'ProjectOfficeUse', 
        'Builder', 
        'Websiteuser', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState', 
        'FeaturePropertyOptionView', 
        'AmenityPropertyOptionView', 
        'PropertyAmenity',
		'PropertyFeature', 
        'MstAmenity', 
		'MstFeature',
        'VAmenityPropertyOption', 
        'VFeaturePropertyOption',
        'FeatureAminityPropertyOptionView',
		'Lead');
    public $components = array('Paginator','Propertydata');

    //$countries->constructClasses();
    /**
     * Displays a view
     *
     * @return void
     * @throws ForbiddenException When a directory traversal attempt.
     * @throws NotFoundException When the view file could not be found
     *   or MissingViewException in debug mode.
     */
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('viewproperty', 'view_detail_builder','view_detail_builder1', 'editproperty', 'addproperty', 'addpropertyfeaturedetail', 'addpropertyPrice', 'addpropertyamenities', 'upload_image', 'propertysubmit', 'getpropertyoption');
    }

    /* get state for add property */

    public function getState() {
        $region_list = $this->MstAreaRegion->find('list', [ 
            'conditions' => [ 'MstAreaRegion.countryCode' => 1 ],
            'fields'     => [ 'MstAreaRegion.regionCode' ]
        ]);
        $sql = sprintf("SELECT MstAreaState.* FROM mst_area_states MstAreaState WHERE (SELECT COUNT(*) FROM mst_area_cities mc WHERE MstAreaState.stateCode = mc.stateCode) > 0 AND MstAreaState.regionCode IN (%s) ORDER BY MstAreaState.stateName ASC", implode(',', $region_list));
        $stateList = $this->MstAreaState->query($sql);
        $states = [];
        foreach ($stateList as $state) {
            $states[$state['MstAreaState']['stateCode']] = $state['MstAreaState']['stateName'];
        }
        return $states;
    }

    /* Post property Basic Details */

    public function addproperty() {
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
        $this->layout='account';

        if ($this->request->is('post')) {
            //Added this line
            /*   $this->request->data['Post']['user_id'] = $this->Auth->user('id'); */

            if (!empty($this->request->data['property_id'])) {
                $this->Property->id = $this->request->data['property_id'];
            }
            $this->request->data['user_id'] = $this->Session->read('Auth.Websiteuser.id');
			
			if (array_key_exists('property_city', $this->request->data)) {
				$city = $this->MstAreaCity->find('first', [
                'conditions' => [ 'cityCode' => $this->request->data['property_city']]
            ]);
            $this->request->data['city_data'] = $city['MstAreaCity']['cityName'];
			}
            
            $this->request->data['status_id'] = 1;
            if ($this->Property->save($this->request->data)) {

                $lastinsertId = $this->Property->getLastInsertId();
                echo $lastinsertId;
                die;
            }
        }
        $project = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'approved' => 1,
                'current_project' => 1,
                'user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $userproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            // 'conditions' => [ 'approved' => 1 ]
        ));
        /* get state */
        $states = $this->getState();
        // print_r($states); exit;
        $this->set('states', $states);
        $this->set('project', $project);
        $this->set('userproject', $userproject);
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            ),
			'order' => array(
				'cityName' => 'asc'
			)
		));
		
        $this->set('city_master', $city_master);
        if ($this->Session->read('Auth.Websiteuser.userrole') != 3) {
            $this->view = "addusrproperty";
        }
    }
	
	
	public function addproperty1() {
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
        $this->layout='account';

        if ($this->request->is('post')) {
            //Added this line
            /*   $this->request->data['Post']['user_id'] = $this->Auth->user('id'); */

            if (!empty($this->request->data['property_id'])) {
                $this->Property->id = $this->request->data['property_id'];
            }
            $this->request->data['user_id'] = $this->Session->read('Auth.Websiteuser.id');
			
			if (array_key_exists('property_city', $this->request->data)) {
				$city = $this->MstAreaCity->find('first', [
                'conditions' => [ 'cityCode' => $this->request->data['property_city']]
            ]);
            $this->request->data['city_data'] = $city['MstAreaCity']['cityName'];
			}
            
            $this->request->data['status_id'] = 1;
			
			//echo '<pre>'; print_r($this->request->data);die();
			
			$this->request->data['plans_project_pricing_ids'] = json_encode($this->request->data['planchecklist']);
			
            if ($this->Property->save($this->request->data)) {

                $lastinsertId = $this->Property->getLastInsertId();
                echo $lastinsertId;
                die;
            }
        }
        $project = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'approved' => 1,
                'current_project' => 1,
                'user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $userproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            // 'conditions' => [ 'approved' => 1 ]
        ));
        /* get state */
        $states = $this->getState();
        // print_r($states); exit;
        $this->set('states', $states);
        $this->set('project', $project);
        $this->set('userproject', $userproject);
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            ),
			'order' => array(
				'cityName' => 'asc'
			)
		));
		
        $this->set('city_master', $city_master);
        if ($this->Session->read('Auth.Websiteuser.userrole') != 3) {
            $this->view = "addusrproperty";
        }
    }
	
	
	public function getProjectsPlan() {
        //$status = 1;
        if (isset($this->request['data']['sel_project_id'])) {
			$this->loadModel('ProjectPricing');			
            $projectPricingPlansArr = $this->ProjectPricing->find('all', array(
                'fields' => array(
                    'id',
                    'payment_plan1',
                ),
                'conditions' => array(
                    'project_id' => $this->request['data']['sel_project_id'])
            ));
			//echo '<pre>'; print_r($projectPricingPlansArr);die();
            $this->set('projectPricingPlansArr', $projectPricingPlansArr);

            $this->view = "projectsplandisplayajax";
            $this->layout = "ajax";
        }
    }
	
	
	
	public function getBasePriceByProjectId(){
		$this->autoRender = false;
		//echo '<pre>'; print_r($_POST);die();
		$projectId = $_POST['projectid'];
		$projectPricingId = $_POST['project_pricing_id'];
		$this->loadModel('ProjectBspDetail');
		//$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$projectId."");			
		$latestBsp = $this->ProjectBspDetail->query("SELECT MIN(bsp_charges),bsp_charges_unit FROM `project_bsp_details` WHERE project_id = ".$projectId." and project_pricing_id = ".$projectPricingId."");			
		//return  $latestBsp[0][0]['MIN(bsp_charges)'];
		//print_r($latestBsp);die();
		
		return json_encode(array(
						 'bsp_charge' => $latestBsp[0][0]['MIN(bsp_charges)'],
						 'bsp_charge_unit' => $latestBsp[0]['project_bsp_details']['bsp_charges_unit']
					 ));
		
		//echo '<pre>';print_r($latestBsp);die();
		$this->layout = "ajax";
		exit();
	}

    /* Get feature from feature master table according to property type option */

    public function getpropertyfeature() {
        $status = 1;
        if (isset($this->request['data']['id'])) {
            $propertyoption = $this->VFeaturePropertyOption->find('list', array(
                'fields' => array(
                    'featureName',
                    'feature_id',
                ),
                'conditions' => array(
                    'active_feature' => '1', 'FIND_IN_SET(\'' . $this->request['data']['id'] . '\',propertyOptionId)')
            ));
            $this->set('propertyoption', $propertyoption);

            $this->view = "addpropertyajax";
            $this->layout = "ajax";
        }
    }

    /* Get amenities from amenities master table according to property type option */

    public function getpropertyaminities() {
        $status = 1;
        if (isset($this->request['data']['id'])) {
            $propertyoption = $this->VFeaturePropertyOption->find('list', array(
                'fields' => array(
                    'featureName',
                    'feature_id',
                ),
                'conditions' => array(
                    'active_feature' => '1', 'FIND_IN_SET(\'' . $this->request['data']['id'] . '\',propertyOptionId)')
            ));

            $aminitesoption = $this->VAmenityPropertyOption->find('list', array(
                'fields' => array(
                    'amenity_name',
                    'amenity_id',
                ),
                'conditions' => array(
                    'is_active' => '1', 'FIND_IN_SET(\'' . $this->request['data']['id'] . '\',propertyOptionId)')
            ));

            
            $this->set('propertyoption', $propertyoption);
            $this->set('aminitesoption', $aminitesoption);

            $this->view = "propertyamnitiesajax";
            $this->layout = "ajax";
        }
    }

    /* get property option from database */

    public function getpropertyoption() {
        $propertyoption = array();

        if (isset($this->request['data']['id'])) {

            $propertyoption = $this->MstPropertyTypeOption->find('list', array(
                'fields' => array(
                    'property_option_id',
                    'option_name',
                ),
                'conditions' => array(
                    'transaction_type_id' => $this->request['data']['id']
                )
            ));
        }

        header('Content-Type: application/json');
        echo json_encode($propertyoption);
        exit();
    }

    public function covert_in_sqt($value, $sqt) {
        switch ($sqt) {
            case "Sq.Yards":
                $plotinsqt = $value * 9;
                break;
            case "Sq.Meter":
                $plotinsqt = $value * 10.7639;
                break;
            case "Acres":
                $plotinsqt = $value * 43560;
                break;
            case "Marla":
                $plotinsqt = $value * 272.251;
                break;
            case "Cent":
                $plotinsqt = $value * 435.6154545;
                break;
            case "Bigha 1":
                $plotinsqt = $value * 17452;
                break;
            case "Bigha 2":
                $plotinsqt = $value * 27224.99;
                break;
            case "Kottah":
                $plotinsqt = $value * 720.04;
                break;
            case "Kanal":
                $plotinsqt = $value * 5399.56;
                break;
            case "Grounds":
                $plotinsqt = $value * 2400.38;
                break;
            case "Ares":
                $plotinsqt = $value * 1076.07;
                break;
            case "Biswa1":
                $plotinsqt = $value * 357142.85;
                break;
            case "Biswa2":
                $plotinsqt = $value * 544499.99;
                break;
            case "Guntha":
                $plotinsqt = $value * 1089.08;
                break;
            case "Aanakadam":
                $plotinsqt = $value * 71.99;
                break;
            case "Hectares":
                $plotinsqt = $value * 108695.65;
                break;
            case "Rood":
                $plotinsqt = $value * 10893.24;
                break;
            case "Chataks":
                $plotinsqt = $value * 450;
                break;
            case "Perch":
                $plotinsqt = $value * 272.3;
                break;
            default:
                $plotinsqt = $value * 1;
        }
        return $plotinsqt;
    }

    /* Add property feature detail */

    public function addpropertyfeaturedetail() {

        if ($this->request->is('post') || $this->request->is('put')) {
            /* Convert plot area in sqtft */

            if (!empty($this->request->data['Feature']['plot_area_sq']) && !empty($this->request->data['Feature']['plot_area'])) {
                $this->request->data['Feature']['plot_area_insq'] = $this->covert_in_sqt($this->request->data['Feature']['plot_area'], $this->request->data['Feature']['plot_area_sq']);
            }
            /* Convert Super Built Up area in sqtft */

            if (!empty($this->request->data['Feature']['sbua_m']) && !empty($this->request->data['Feature']['sbua'])) {

                $this->request->data['Feature']['superbuilt_area_insq'] = $this->covert_in_sqt($this->request->data['Feature']['sbua'], $this->request->data['Feature']['sbua_m']);
            }

            /* Convert  Built Up area in sqtft */

            if (!empty($this->request->data['Feature']['build_up_area']) && !empty($this->request->data['Feature']['build_up_sq'])) {
                $this->request->data['Feature']['build_up_area_insq'] = $this->covert_in_sqt($this->request->data['Feature']['build_up_area'], $this->request->data['Feature']['build_up_sq']);
            }


            /* Convert  Carpet Area  in sqtft */

            if (!empty($this->request->data['Feature']['carpet_area_sq']) && !empty($this->request->data['Feature']['carpet_area'])) {
                $this->request->data['Feature']['carpet_area_insq'] = $this->covert_in_sqt($this->request->data['Feature']['carpet_area'], $this->request->data['Feature']['carpet_area_sq']);
            }

            // $this->request->data['Property']['features'] = json_encode($this->request->data['Feature']);
			if (isset($this->request->data['property_id'])) 
			{
            	$property_id = $this->request->data['property_id'];
        	}
			$feature_in_aminity_arr = array(31,32,8,28,26,13,18,36,14,42,43,44,15,16,17,25);
			$feature_in_aminity = implode(',',$feature_in_aminity_arr);
			$this->PropertyFeature->query("Delete from `property_features` WHERE `properties_id` = " . $property_id . " AND features_id NOT IN(".$feature_in_aminity.")");
			//$this->PropertyFeature->query("Delete from `property_features` WHERE `properties_id` = " . $property_id . "");
			$unit_field_array = array(1=>'sbua_m',2=>'build_up_sq',3=>'carpet_area_sq',20=>'plot_area_sq');
			//print_r($this->request->data['Feature']);
			foreach ($this->request->data['Feature'] as $key => $value) 
			{
				$mst_feature = $this->MstFeature->find('first', [
					'fields'     => [ 'feature_id' ],
					'conditions' => [ 'MstFeature.Alias_Name' => $key ]
				]);
				if(!empty($mst_feature)) {
					$propertyFeature = $this->PropertyFeature->find('first', [
						'conditions' => [ 
							'PropertyFeature.features_id'  => $mst_feature['MstFeature']['feature_id'],
							'PropertyFeature.properties_id' => $property_id
						]
					]);
				
				if (array_key_exists($mst_feature['MstFeature']['feature_id'], $unit_field_array)) {
					$unit = $this->request->data['Feature'][$unit_field_array[$mst_feature['MstFeature']['feature_id']]];
				}
				else
				{
					$unit = '';
				}
				
					/*if(!empty($propertyFeature))
						$this->propertyFeature->id = $propertyFeature['PropertyFeature']['property_amenity_id'];*/
				   $this->PropertyFeature->save([
						'properties_id'   => $property_id,
						'features_id'    => $mst_feature['MstFeature']['feature_id'],
						'features_Value' => $value,
						'Unit' => $unit
					]);
					$this->PropertyFeature->clear();
				}
				
			}

            $this->Property->id = $this->request->data['property_id'];
            $this->Property->save($this->request->data);
            if (!empty($this->request->data['Feature']['superbuilt_area_insq'])) {
                echo $this->request->data['Feature']['superbuilt_area_insq'];
            } else {
                echo $this->request->data['Feature']['plot_area_insq'];
            }
            exit();
        }
    }

    /* Add property pricing details */

    public function addpropertyPrice() {
        if ($this->request->is('post') || $this->request->is('put')) {
			
			$propertyid = $this->request->data['property_id'];
			
			$this->loadModel('ProjectSpaceDetail');			
			$this->ProjectSpaceDetail->query("Delete from `project_space_details` WHERE `project_id` = " . $propertyid . "");			
			for( $i=1; $i <= count($this->request->data['bspchargesspace'])/4; $i++ ){
                $this->request->data['ProjectSpaceDetail']['project_id'] = $propertyid;
                  
				$this->request->data['ProjectSpaceDetail']['space_mode'] =  $this->request->data['bspchargesspace']['space_mode'.$i.''];
				$this->request->data['ProjectSpaceDetail']['space_from_floor'] = $this->request->data['bspchargesspace']['space_from_floor'.$i.''];
				$this->request->data['ProjectSpaceDetail']['floor_space_area'] = $this->request->data['bspchargesspace']['floor_space_area'.$i.''];
				$this->request->data['ProjectSpaceDetail']['floor_space_amount'] = $this->request->data['bspchargesspace']['floor_space_amount'.$i.''];
				//echo '<pre>'; print_r($this->request->data['ProjectBspDetail']);
                $this->ProjectSpaceDetail->create(false);
                $this->ProjectSpaceDetail->save($this->request->data['ProjectSpaceDetail']);
            }
			
			
			$this->loadModel('PropertyPossessionCharge');
			$this->PropertyPossessionCharge->query("Delete from `property_possession_charges` WHERE `property_id` = " . $propertyid . "");
			for( $i=1; $i <= count($this->request->data['possessioncharges'])/3; $i++ ){
                $this->request->data['PropertyPossessionCharge']['property_id'] = $propertyid;                  
				$this->request->data['PropertyPossessionCharge']['possession_amount'] =  $this->request->data['possessioncharges']['possession_amount'.$i.''];
				$this->request->data['PropertyPossessionCharge']['possession_from_floor'] = $this->request->data['possessioncharges']['possession_from_floor'.$i.''];
				$this->request->data['PropertyPossessionCharge']['possession_to_floor'] = $this->request->data['possessioncharges']['possession_to_floor'.$i.''];
			
				//echo '<pre>'; print_r($this->request->data['PropertyPossessionCharge']);die();
                $this->PropertyPossessionCharge->create(false);
                $this->PropertyPossessionCharge->save($this->request->data['PropertyPossessionCharge']);
            }
			
			
            $this->Property->id = $propertyid;
			//echo '<pre>'; print_r($this->request->data);die();
			if($this->request->data['offer_price'] == ''){
				$this->request->data['offer_price'] = $this->request->data['expected_monthly'];
			}
			
			if($this->request->data['market_price'] == ''){
				$this->request->data['market_price'] = $this->request->data['market_rent'];
			}
			$this->request->data['bspchargesspace'] = json_encode($this->request->data['bspchargesspace'], true);
			$this->request->data['possessioncharges'] = json_encode($this->request->data['possessioncharges'], true);
			//echo '<pre>'; print_r($this->request->data);die();
            $this->Property->save($this->request->data);
            exit();
        }
    }

    /* insert properties amenities */

    public function addpropertyamenities() {
        if (isset($this->request->data['property_id'])) {
            $property_id = $this->request->data['property_id'];
        }
        /*$propertyoption = $this->Property->find('list', array(
            'fields' => array(
                'features',
            ),
            'conditions' => array(
                'id' => $this->request->data['property_id']
            )
        ));

        */
		//print_r($this->request->data['Feature']);
		
		$feature_in_aminity_arr = array(31,32,8,28,26,13,18,36,14,42,43,44,15,16,17,25);
		$feature_in_aminity = implode(',',$feature_in_aminity_arr);
		$this->PropertyFeature->query("Delete from `property_features` WHERE `properties_id` = " . $property_id . " AND features_id IN(".$feature_in_aminity.")");
		$unit_field_array = array(13=>'width_of_sq');
		//print_r($this->request->data['Feature']);
		foreach ($this->request->data['Feature'] as $key => $value) 
		{
			$mst_feature = $this->MstFeature->find('first', [
				'fields'     => [ 'feature_id' ],
				'conditions' => [ 'MstFeature.Alias_Name' => $key ]
			]);
			if(!empty($mst_feature)) {
				$propertyFeature = $this->PropertyFeature->find('first', [
					'conditions' => [ 
						'PropertyFeature.features_id'  => $mst_feature['MstFeature']['feature_id'],
						'PropertyFeature.properties_id' => $property_id
					]
				]);
			
			if (array_key_exists($mst_feature['MstFeature']['feature_id'], $unit_field_array)) {
				$unit = $this->request->data['Feature'][$unit_field_array[$mst_feature['MstFeature']['feature_id']]];
			}
			else
			{
				$unit = '';
			}
			
				/*if(!empty($propertyFeature))
					$this->propertyFeature->id = $propertyFeature['PropertyFeature']['property_amenity_id'];*/
			   $this->PropertyFeature->save([
					'properties_id'   => $property_id,
					'features_id'    => $mst_feature['MstFeature']['feature_id'],
					'features_Value' => $value,
					'Unit' => $unit
				]);
				$this->PropertyFeature->clear();
			}
			
		}
		
		
		
		
		
		
		
	//print_r($this->request->data['amenities']);exit;
        foreach ($this->request->data['amenities'] as $key => $value) {
            $mst_amenity = $this->MstAmenity->find('first', [
                'fields'     => [ 'amenity_id' ],
                'conditions' => [ 'MstAmenity.alias_name' => $key ]
            ]);
            if(!empty($mst_amenity)) {
                $propertyAmenity = $this->PropertyAmenity->find('first', [
                    'conditions' => [ 
                        'PropertyAmenity.amenity_id'  => $mst_amenity['MstAmenity']['amenity_id'],
                        'PropertyAmenity.property_id' => $property_id
                    ]
                ]);
                if(!empty($propertyAmenity))
                    $this->PropertyAmenity->id = $propertyAmenity['PropertyAmenity']['property_amenity_id'];
                $this->PropertyAmenity->save([
                    'property_id'   => $property_id,
                    'amenity_id'    => $mst_amenity['MstAmenity']['amenity_id'],
                    'amenity_value' => (($value == 1) ? 'Yes' : 'No')
                ]);
                $this->PropertyAmenity->clear();
            }
            
        }
        print json_encode($this->request->data['amenities']);
        /*$feature = json_decode($propertyoption[$property_id], true);
        $all_feature = array_merge($feature, $this->request->data['Feature']);


        $this->request->data['Property']['amenities'] = json_encode($this->request->data['amenities']);
        $this->request->data['Property']['features'] = json_encode($all_feature);
        $this->Property->id = $this->request->data['property_id'];
        $this->Property->save($this->request->data);
*/
        exit();
    }

    
	
	public function dropzoneUpload() {
		$target_dir = 'upload' . DS . 'property_images' . DS;
		/*  Start code for o Uploading images*/
		//$name = $_FILES['upload_file']['name'];
		$this->autoRender = false;
		
		
		 
		
		
		
		try {
    
			// Undefined | Multiple Files | $_FILES Corruption Attack
			// If this request falls under any of them, treat it invalid.
			if (
				!isset($_FILES['upload_file']['error']) ||
				is_array($_FILES['upload_file']['error'])
			) {
				throw new RuntimeException('Invalid parameters.');
			}

			// Check $_FILES['upfile']['error'] value.
			switch ($_FILES['upload_file']['error']) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					throw new RuntimeException('No file sent.');
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					throw new RuntimeException('Exceeded filesize limit.');
				default:
					throw new RuntimeException('Unknown errors.');
			}

			// You should also check filesize here. 
			if ($_FILES['upload_file']['size'] > 1000000) {
				throw new RuntimeException('Exceeded filesize limit.');
			}

			// DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
			// Check MIME Type by yourself.
			$finfo = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
			if (!in_array($finfo, array('jpg', 'jpeg', 'png'))) {
				throw new RuntimeException('Invalid file format.');
			}

			// You should name it uniquely.
			// DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
			// On this example, obtain safe unique name from its binary data.
			if (!move_uploaded_file($_FILES['upload_file']['tmp_name'], sprintf('upload/property_images/%s.%s', sha1_file($_FILES['upload_file']['tmp_name']), $finfo))) {
				throw new RuntimeException('Failed to move uploaded file.');
			}

			echo 'File is uploaded successfully.';

		} catch (RuntimeException $e) {

			echo $e->getMessage();

		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		// if($_FILES['upload_file']['error'] == 0) {
			
			// if(!empty($name)) {
				//Check For Empty Values 
				// $tmprary_name =  $_FILES['upload_file']['tmp_name'];
				// $temp = explode(".", $name);
				// $newfilename = uniqid(). '.' . end($temp);
				//echo $tmprary_name .'------'. $target_dir.$newfilename;die;
				// if (move_uploaded_file($tmprary_name , $target_dir.$newfilename)) { 
					// $this->PropertyPic->create(); 
					// $this->PropertyPic->save(array(
						// "project_id" => 23,//$this->request->data['property_id'],
						// "is_prop_or_project" =>'1',
						// "pic" => $target_dir.$newfilename,
					// ));       
					
					// return json_encode(array(
						// 'result' => 'success',
						// 'message' => 'Upload successfully',
						// 'path' => $target_dir.$newfilename
					// ));
				// }
			// }
		// } else {
			// switch ($code) { 
					// case UPLOAD_ERR_INI_SIZE: 
						// $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
						// break; 
					// case UPLOAD_ERR_FORM_SIZE: 
						// $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
						// break; 
					// case UPLOAD_ERR_PARTIAL: 
						// $message = "The uploaded file was only partially uploaded"; 
						// break; 
					// case UPLOAD_ERR_NO_FILE: 
						// $message = "No file was uploaded"; 
						// break; 
					// case UPLOAD_ERR_NO_TMP_DIR: 
						// $message = "Missing a temporary folder"; 
						// break; 
					// case UPLOAD_ERR_CANT_WRITE: 
						// $message = "Failed to write file to disk"; 
						// break; 
					// case UPLOAD_ERR_EXTENSION: 
						// $message = "File upload stopped by extension"; 
						// break; 

					// default: 
						// $message = "Unknown upload error"; 
						// break; 
				// } 
				// return $message; 
			// } 
		// }
		
		
		
		
		
		
		
		
		
		
    
		
	}
	
	/* Upload Property Images */

    public function upload_image() {
        $target_dir = 'upload/property_images/';
        if (!empty($this->request->data)) {
		//print_r($this->request->data);	exit;
            if ($this->Session->read('Auth.Websiteuser.userrole') == 3) {
                $this->request->data['Property']['status_id'] = 3;
            } else {
                $this->request->data['Property']['status_id'] = 2;
            }
            $this->Property->id = $this->request->data['property_id'];
            $this->Property->save($this->request->data);

            /*  Start code for o Uploading images */

            foreach ($this->request->data['upload_file'] as $result) {

                $name = $result['name'];

                if (!empty($name)) {
                    // Check For Empty Values 
                    $tmprary_name = $result['tmp_name'];
                    $temp = explode(".", $name);
                    $newfilename = uniqid() . '.' . end($temp);
                    if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                        $this->PropertyPic->create();
                        $this->PropertyPic->save(
                                array(
                                    "property_id" => $this->request->data['property_id'],
                                    "pic" => $target_dir . $newfilename,
                                )
                        );
                    }
                }
            }
            

            $propertydetails = $this->Property->findByPropertyId($this->request->data['property_id']);
            $projectdetails = $this->Project->findByProjectId($propertydetails['Property']['project_id']);
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));

            $user = Set::extract('/User/.', $user);

            $locality_find = $this->MstAssignLocality->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_localities',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_localities.localityCode = MstAssignLocality.localityCode'
                        )
                    )
                ),
                'conditions' => array(
                    'mst_area_localities.localityName' => $projectdetails['Project']['locality']
                )
            ));

            $city_find = $this->MstAssignCity->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_cities',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_cities.cityCode = MstAssignCity.citycode'
                        )
                    )
                ),
                'conditions' => array(
                    'MstAssignCity.citycode' => $projectdetails['Project']['project_city']
                )
            ));

            $state_find = $this->MstAssignState->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_states',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_states.stateCode = MstAssignState.stateCode'
                        )
                    )
                ),
                'conditions' => array(
                    'stateName' => $projectdetails['Project']['state_data']
                )
            ));

            $country_find = $this->MstAssignCountry->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'mst_area_countries',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'mst_area_countries.countryCode = MstAssignCountry.countryCode'
                        )
                    )
                ),
                'conditions' => array(
                    'mst_area_countries.countryName' => $projectdetails['Project']['country_data']
                )
            ));

            if (!empty($locality_find['MstAreaLocality']['research_executive'])) {
                $key = $locality_find['MstAssignLocality']['research_executive'];
            } else if (!empty($city_find['MstAreaCity']['research_executive'])) {
                $key = $city_find['MstAssignCity']['research_executive'];
            } else if (!empty($state_find['MstAreaState']['research_executive'])) {
                $key = $state_find['MstAssignState']['research_executive'];
            } else {

                $key = isset($country_find[0]) ? $country_find[0]['MstAssignCountry']['research_executive'] : '';
            }
            // print_r($this->request->data); exit;
            $this->request->data ['PropertyNotifiction']['user_id'] = (!empty($key) ? $key : $this->Session->read('Auth.Websiteuser.id'));
            $this->request->data ['PropertyNotifiction']['property_id'] = $this->request->data['property_id'];
            $this->PropertyNotifiction->save($this->request->data);
            $this->redirect('propertysubmit');
        }
        exit();
    }

    public function propertysubmit($edit = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        $this->layout = 'account';
        $this->set('edit', $edit);
    }

    public function viewproperty($id = null) {
        $this->layout = "home";
		//$this->view_detail_builder($id);
		//$this->view = 'view_detail_builder';
        $new_array = array();
        //$this->loadComponent('Propertydata');

        $propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    ' 	localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);



        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		
		//echo '<pre>'; print_r($propertyImages);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        if (!empty($projectdetail['0'])) {
            $this->set('projectdetail', $projectdetail['0']);
        }
        if (!empty($project['0'])) {
            $this->set('project', $project['0']);
            $this->set('propertyImages', $propertyImages);
        }
        $this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
		
    }

    function get_parent($user, $id, $new_array) {

        $new_array[$id] = $id;
        $key = $this->get_key($user, $id);

        if ($user[$key]['parent_id'] == 0) {
            
        } else {

            return $this->get_parent($user, $user[$key]['parent_id'], $new_array);
        }
        return $new_array;
    }

    function get_key($arr, $id) {

        foreach ($arr as $key => $val) {

            if ($val['id'] == $id) {
                return $key;
            }
        }
        return null;
    }

    public function editproperty($codid = null) {
	
		$this->layout='account';
        $id = base64_decode($codid);
        if (empty($this->Session->read('Auth.User.id'))) {
            $valid = $this->Property->find('all', array(
                'conditions' => array(
                    'property_id' => $id,
                    'user_id' => $this->Session->read('Auth.Websiteuser.id')
                )
            ));
            if (empty($valid)) {
                throw new NotFoundException(__('Invalid project'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }

        // $propertydetails = $this->Property->findById($id);
        $propertydetails = $this->Property->findByPropertyId($id);
        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }
        
		
        $this->request->data = $propertydetails['Property'];
		$propertyoption = $this->MstPropertyTypeOption->find('first', array(
                'conditions' => array(
                    'property_option_id' => $this->request->data['property_type_id']
                )
            )); 
			//print_r($propertyoption);
        if (!empty($propertyoption)) {
            $this->request->data['property_type_op'] = $propertyoption['MstPropertyTypeOption']['transaction_type_id'];
        } else {
            $this->request->data['property_type_op'] = '';
        }
        /* Get property option for selected property type */
        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $this->request->data['property_type_op']
            )
        ));

        $this->set('propertyoptiondata', $propertyoptiondata);


        /* get edit property type option  */
        $propertyoption = $this->VFeaturePropertyOption->find('list', array(
            'fields' => array(
                'featurename',
                'feature_id',
            ),
            'conditions' => array(
                'active_feature' => '1', 'FIND_IN_SET(\'' . $this->request->data['property_type_id'] . '\',propertyOptionId)')
        ));

		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
            ));
		$all_property_features = array();
		$all_property_features_unit = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
			$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
		}
		
		$property_amenities = $this->PropertyAmenity->find('all', array(
                'conditions' => array(
                    'property_id' => $id
                )
            ));
			//print_r($property_amenities);
		$all_property_amenities = array();
		foreach($property_amenities as $propertyam_val)
		{
			$all_property_amenities[$propertyam_val['PropertyAmenity']['amenity_id']] = $propertyam_val['PropertyAmenity']['amenity_value'];
		}
		
        /* get edit amenities type option  */
        $aminitesoption = $this->VAmenityPropertyOption->find('list', array(
            'fields' => array(
                'amenity_name',
                'amenity_id',
            ),
            'conditions' => array(
                'is_active' => '1', 'FIND_IN_SET(\'' . $this->request->data['property_type_id'] . '\',propertyOptionId)')
        ));
        $this->request->data['aminities'] = $aminitesoption;
        // print json_encode($this->request->data); exit;
        $this->set('propertyoption', $propertyoption);
        $this->set('aminitesoption', $aminitesoption);
       // $this->set('aminitesoption', $aminitesoption);
        //$this->set('transaction_type', $this->request->data['transaction_type_id']);
        // print_r($this->request->data); exit;
        // $this->request->data['Feature'] = json_decode($this->request->data['features'], true);
        // $this->request->data['amenities'] = json_decode($this->request->data['amenities'], true);

        /* get edit property Images */


        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));



        $project = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'approved' => 1,
                'current_project' => 1,
                'user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));

        $userproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'approved' => 1
            )
        ));


        /* Get city option for selected state */
        $cities = array();

        if (isset($this->request->data['Property']['state'])) {

            $cities = $this->MstAreaCity->find('list', array(
                'fields' => array(
                    'cityCode',
                    'cityName',
                ),
                'conditions' => array(
                    'stateCode' => $this->request->data['Property']['state']
                )
            ));



            $this->set('city', $cities);
        }

        /* Get city option for selected Locality */
        $cities = array();

        if (isset($this->request->data['Property']['city'])) {


            $localities = $this->MstAreaLocality->find('list', array(
                'fields' => array(
                    'localityCode',
                    'localityName',
                ),
                'conditions' => array(
                    'cityCode' => $this->request->data['Property']['city']
                )
            ));

            $this->set('locality', $localities);
        }
        $areainsq = 1;
        if (isset($this->request->data['Feature']) && !empty($this->request->data['Feature']['superbuilt_area_insq'])) {
            $areainsq = $this->request->data['Feature']['superbuilt_area_insq'];
        } else if(isset($this->request->data['Feature'])) {
            $areainsq = $this->request->data['Feature']['plot_area_insq'];
        }
		$this->request->data['AllFeatures'] = $all_property_features;
		$this->request->data['AllFeaturesUnit'] = $all_property_features_unit;
		$this->request->data['AllAmenity'] = $all_property_amenities;
        $states = $this->getState();
        $this->set('project', $project);
        $this->set('userproject', $userproject);
        $this->set('areainsq', $areainsq);
        $this->set('states', $states);
        $this->set('property_id', $id);
        $this->set('propertyImages', $propertyImages);
        if ($this->Session->read('Auth.Websiteuser.userrole') == 3) {
			
			if (!empty($this->request->data['bspchargesspace'])) {
				$this->request->data['bspchargesspace'] = json_decode($this->request->data['bspchargesspace'], 'true');				
			}
			if (!empty($this->request->data['possessioncharges'])) {
				$this->request->data['possessioncharges'] = json_decode($this->request->data['possessioncharges'], 'true');			
			}
			
			//$this->request->data['power_backup_kva'] = $this->request->data['power_backup_kva'];
			//$this->request->data['possession_charge'] = $this->request->data['possession_charge'];
			
			if ($this->Session->read('Auth.Websiteuser.id') == 45) {
				//echo '884';				
				$this->loadModel('ProjectPricing');			
					$projectPricingPlansArr = $this->ProjectPricing->find('all', array(
						'fields' => array(
							'id',
							'payment_plan1',
						),
						'conditions' => array(
							'project_id' => $this->request->data['project_id'])
					));
				$this->set('projectPricingPlansArr',$projectPricingPlansArr);
				
				
				if (!empty($this->request->data['plans_project_pricing_ids'])) {
					$this->request->data['planchecklist'] = json_decode($this->request->data['plans_project_pricing_ids'], 'true');				
				}
				
				
				
				$this->view = "editbdproperty1";
			}
			else{
				$this->loadModel('ProjectPricing');			
					$projectPricingPlansArr = $this->ProjectPricing->find('all', array(
						'fields' => array(
							'id',
							'payment_plan1',
						),
						'conditions' => array(
							'project_id' => $this->request->data['project_id'])
					));
				$this->set('projectPricingPlansArr',$projectPricingPlansArr);
				
				
				if (!empty($this->request->data['plans_project_pricing_ids'])) {
					$this->request->data['planchecklist'] = json_decode($this->request->data['plans_project_pricing_ids'], 'true');				
				}
				
				$this->view = "editbdproperty1";
				
			}
            
        }
        $city_master = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            )
        ));
        $this->set('city_master', $city_master);
    }
	
	public function allresponses($id = NULL) {
		
		$builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
		
		$this->loadModel('Lead');
		//echo $id;
		if($id != ''){
			$this->Paginator->settings =
            array(
			'conditions' => array(
                    'Lead.property_id' => $id
                ),
			'joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.property_id = Lead.property_id',
						'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')))),
						'limit' => 12);
			
		}else{
			$this->Paginator->settings =
            array(
			'joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.property_id = Lead.property_id',
						'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')))),
						'limit' => 12);
			
		}
		
        $allresponse = $this->Paginator->paginate('Lead');
		//echo '<pre>'; print_r($allproj);die();

        $this->set('allresponse', $allresponse);
        $this->layout = "account";
	}

    public function allproperties($tab = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        if ($tab == '1') {
            // change required!
            $this->Paginator->settings = array(
                'conditions' => array('Property.status_id' => '1', 'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
            $property_user_inner_menu = 1;
        } else if ($tab == '2') {
            $this->Paginator->settings = array(
                'conditions' => array('Property.status_id' => '3', 'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
            $property_user_inner_menu = 2;
        } else if ($tab == '3') {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'status_id' => array(4, 5),
                    'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'limit' => 5
            );
            $property_user_inner_menu = 3;
        } else if ($tab == '4') {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'status_id' => array(2),
                    'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'limit' => 5
            );
            $property_user_inner_menu = 4;
        } else if ($tab == '5') {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'status_id' => array(6),
                    'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'limit' => 5
            );
            $property_user_inner_menu = 5;
        } else if (!empty($this->request->data['search'])) {
            $property_user_inner_menu = -1;
            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array(
                        'Project.project_name LIKE' => $this->request->data['search'] . '%',
                        'Project.locality LIKE' => $this->request->data['search'] . '%',
                        'Project.city_data LIKE' => $this->request->data['search'] . '%',
                        'Project.state_data LIKE' => $this->request->data['search'] . '%'
                    ),
                    'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
                ),
                'joins' => array(
                    array(
                        'alias' => 'Project',
                        'table' => 'projects',
                        'type' => 'Left',
                        'conditions' => '`Project`.`project_id` = `Property`.`project_id`'
                    )
                ),
                'limit' => 5
            );
        } else {
            $this->Paginator->settings = array(
                'conditions' => array('Property.user_id' => $this->Session->read('Auth.Websiteuser.id')),
                'limit' => 5
            );
            $property_user_inner_menu = 0;
        }
        $allproj = $this->Paginator->paginate('Property');
        //echo '<pre>';print_r($allproj);
        foreach ($allproj as $index => $proj) {

            $proj_detail = $this->Project->find('first', array(
                'conditions' => array(
                    'project_id' => $proj['Property']['project_id']
                )
            ));
			if(!empty($proj_detail)) 
			{
            	$allproj[$index]['Project'] = $proj_detail['Project'];
			}
			else
			{
				$allproj[$index]['Project'] = $proj_detail;
			}
            $allproj[$index]['Project'] = $proj_detail['Project'];
            $propertyoption = $this->MstPropertyTypeOption->find('first', array(
                'conditions' => array(
                    'property_option_id' => $proj['Property']['property_type_id']
                )
            ));      
			if(!empty($propertyoption))
			{
            	$allproj[$index]['property_type_id'] = $propertyoption['MstPropertyTypeOption'];
			}
			else
			{
				$allproj[$index]['property_type_id'] = array();
			}
            /*$merge_data = $proj;
            if (!empty($proj_detail['0'])) {
                $merge_data = array_merge($merge_data, $proj_detail['0']);
            }
            if (!empty($propertyoption['0'])) {
                $merge_data = array_merge($merge_data, $propertyoption['0']);
            }*/
            // $allprojdata[] = $merge_data;
        }
        // print json_encode($allproj); exit;
        $total = $this->Property->find('count', array(
            'conditions' => array(
                'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $saved = $this->Property->find('count', array(
            'conditions' => array(
                'status_id' => 1,
                'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $approved = $this->Property->find('count', array(
            'conditions' => array(
                'status_id' => 3,
                'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $underapproved = $this->Property->find('count', array(
            'conditions' => array(
                'status_id' => 2,
                'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $rejected = $this->Property->find('count', array(
            'conditions' => array(
                'status_id' => 6,
                'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        $deactived = $this->Property->find('count', array(
            'conditions' => array(
                'status_id' => array(4, 5),
                'Property.user_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        
        $this->set(compact('property_user_inner_menu'));
        $this->set(compact('underapproved'));
        $this->set(compact('approved'));
        $this->set(compact('saved'));
        $this->set(compact('total'));
        $this->set(compact('deactived'));
        $this->set(compact('rejected'));
        $this->set(compact('allproj'));
        $this->set(compact('allprojdata'));
        $this->layout = 'account';
        if ($this->Session->read('Auth.Websiteuser.userrole') != 3) {
            $this->view = "alluserproperties";
        }
    }

    public function deactivateapproved($id = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }

        if (!empty($id)) {
            $this->Property->id = $id;
            $this->request->data['status_id'] = 4;
            $this->Property->save($this->request->data);
        }
        $edit = '3';
        $this->set(compact('edit'));
        $this->layout = 'account';
        $this->view = 'propertysubmit';
    }

    public function refresh($id = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        if (!empty($id)) {
            $this->Property->id = $id;

            $this->request->data['created_date'] = date("Y-m-d H:i:s");
            $this->Property->save($this->request->data);
        }
        $edit = '4';
        $this->set(compact('edit'));
        $this->layout = 'account';
        $this->view = 'propertysubmit';
    }

    public function activateapproved($id = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        if (!empty($id)) {
            $this->Property->id = $id;
            $this->request->data['status_id'] = 3;
            $this->request->data['created_date'] = date("Y-m-d H:i:s");
            $this->Property->save($this->request->data);
        }
        $edit = '2';
        $this->set(compact('edit'));
        $this->layout = 'account';
        $this->view = 'propertysubmit';
    }

    public function deactivate($id = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        if (!empty($id)) {
            $this->Property->id = $id;
            $this->request->data['status_id'] = 5;
            $this->Property->save($this->request->data);
        }
        $edit = '3';
        $this->set(compact('edit'));
        $this->layout = 'account';
        $this->view = 'propertysubmit';
    }

    public function activate($id = Null) {
        /* check builder approval */

        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
        if (!empty($builder['0']['Builder']['status'])) {
            if ($builder['0']['Builder']['status'] != 3) {
                $this->redirect('/accounts/dashboard');
            }
            $this->set('status', $builder['0']['Builder']['status']);
        }
        if (!empty($id)) {
            $this->Property->id = $id;
            $this->request->data['status_id'] = 1;
            $this->Property->save($this->request->data);
        }
        $edit = '2';
        $this->set(compact('edit'));
        $this->layout = 'account';
        $this->view = 'propertysubmit';
    }

    public function view_detail_builder($id = Null) {
	   $this->layout = "home";
        $new_array = array();
        $propertydetails = $this->FeatureAminityPropertyOptionView->findByproperty_id($id);
		//echo '<pre>';print_r($propertydetails);

        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    ' 	localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
        $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['FeatureAminityPropertyOptionView']['property_type_id']
            )
        ));

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['FeatureAminityPropertyOptionView']['project_id']
            )
        ));

        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['FeatureAminityPropertyOptionView']['project_id']
            )
        ));

        $project_financer = $this->ProjectFinancer->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['FeatureAminityPropertyOptionView']['project_id']
            )
        ));

        $project_pricing = $this->ProjectPricing->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['FeatureAminityPropertyOptionView']['project_id']
            )
        ));

        $Websiteuser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'id' => $propertydetails['FeatureAminityPropertyOptionView']['user_id']
            )
        ));
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $propertydetails['FeatureAminityPropertyOptionView']['user_id']
            )
        ));
        $pastproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'user_id' => $propertydetails['FeatureAminityPropertyOptionView']['user_id'],
                'approved' => '1',
                'current_project' => '0'
            )
        ));
        $pastprojectdata = (implode($pastproject, ','));
        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
            ));
		$all_property_features = array();
		$all_property_features_unit = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
			$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
		}
		
		
        $this->set('pastprojectdata', $pastprojectdata);
        $this->set('Websiteuser', $Websiteuser['0']);
        if(!empty($builder))
		{
        	$this->set('builder', $builder['0']);
		}
		else
		{
			$this->set('builder', $builder);
		}
		if(!empty($project_pricing))
		{
        	$this->set('project_pricing', $project_pricing['0']);
		}
		else
		{
			$this->set('project_pricing', $project_pricing);
		}
        if(!empty($project_financer))
		{
        	$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
		}
		else
		{
			$this->set('project_financer', $project_financer);
		}
		
        
		if(!empty($projectdetail))
		{
        	$this->set('projectdetail', $projectdetail['0']);
		}
		else
		{
			$this->set('projectdetail', $projectdetail);
		}
        
        $this->set('propertyImages', $propertyImages);
		if(!empty($propertyoption))
		{
        	$this->set('propertyoption', $propertyoption['0']);
		}
		else
		{
			$this->set('propertyoption', $propertyoption);
		}
        $this->set('propertydetails', $propertydetails);
		
        $this->set('propertyfeaturesunit', $all_property_features_unit);
        $this->set('project', $project['0']);
    }
	
	public function view_detail_builder1($id = Null) {
		
		$this->layout = "home";
		//$this->view_detail_builder($id);
		//$this->view = 'view_detail_builder';
        $new_array = array();
        //$this->loadComponent('Propertydata');

        $propertydetails = $this->Property->findByPropertyId($id);

        $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);

        foreach($propertyFeatureData as $property_val)
        {
            $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
            $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
        }

        //echo '<pre>';print_r($all_property_features);die();
        //echo '<pre>';print_r($all_property_features_unit);die();

        $this->set('all_property_features', $all_property_features);
        $this->set('all_property_features_unit', $all_property_features_unit);


        $propertyAmenitiesData =  $this->Propertydata->getPropertyAmeities($id);//die();

        foreach($propertyAmenitiesData as $amenities_val)
        {
            $all_property_amenities[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['amenity_value'];
            $all_property_amenities_unit[$amenities_val['PropertyAmenities']['amenity_id']] = $amenities_val['PropertyAmenities']['unit'];
        }

        //echo '<pre>';print_r($all_property_amenities);
        //echo '<pre>';print_r($all_property_amenities_unit);die();

        $this->set('all_property_amenities', $all_property_amenities);
        $this->set('all_property_amenities_unit', $all_property_amenities_unit);
       

        //print_r($propertyoption);*/
        /*$this->set('propertyoption', $propertyoption);


        $propertyoptiondata = $this->MstPropertyTypeOption->find('list', array(
            'fields' => array(
                'property_option_id',
                'option_name',
            ),
            'conditions' => array(
                'transaction_type_id' => $propertyoption['MstPropertyTypeOption']['property_type_op']
            )
        ));*/

        
        if ($this->request->is('post')) {
            $user = $this->User->find('all', array(
                'fields' => array(
                    'id',
                    'parent_id',
                )
            ));
            $user = Set::extract('/User/.', $user);
            // $notifiction = $this->get_parent($user,32,$new_array); /* GET PARENT ACCORDING TO CHILD*/
            $locality_find = $this->AreaLocalMaster->find('all', array(
                'conditions' => array(
                    ' 	localityCode' => $propertydetails['Property']['locality']
                )
            ));
            $city_find = $this->AreaCityMaster->find('all', array(
                'conditions' => array(
                    'citycode' => $propertydetails['Property']['locality']
                )
            ));

            $state_find = $this->AreaStateMaster->find('all', array(
                'conditions' => array(
                    'stateCode' => $propertydetails['Property']['state']
                )
            ));

            $country_find = $this->AreaCountryMaster->find('all', array(
                'conditions' => array(
                    'countryCode' => $propertydetails['Property']['country']
                )
            ));

            if (@$this->request->data['PropertyInquirie']['Assisted'] == '1' || @$this->request->data['PropertyInquirie']['Immediate'] == '1') {
                if (!empty($locality_find['AreaLocalMaster']['brokerage_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['brokerage_executive'];
                } else if (!empty($city_find['AreaCityMaster']['brokerage_executive'])) {
                    $key = $city_find['AreaCityMaster']['brokerage_executive'];
                } else if (!empty($state_find['AreaStateMaster']['brokerage_executive'])) {
                    $key = $state_find['AreaStateMaster']['brokerage_executive'];
                } else if (!empty($country_find['AreaCountryMaster']['brokerage_executive'])) {
                    $key = $country_find['AreaCountryMaster']['brokerage_executive'];
                } else {
                    $key = 0;
                }
            } else {
                if (!empty($locality_find['AreaLocalMaster']['research_executive'])) {
                    $key = $locality_find['AreaLocalMaster']['research_executive'];
                } else if (!empty($city_find['AreaCityMaster']['research_executive'])) {
                    $key = $city_find['AreaCityMaster']['research_executive'];
                } else if (!empty($state_find['AreaStateMaster']['research_executive'])) {
                    $key = $state_find['AreaStateMaster']['research_executive'];
                } else {
                    $key = $country_find['AreaCountryMaster']['research_executive'];
                }
            }

            if ($this->PropertyInquirie->save($this->request->data)) {


                $this->request->data ['PropertyNotifiction']['admin_id'] = $key;
                $this->request->data ['PropertyNotifiction']['property_id'] = $id;
                $this->request->data ['PropertyNotifiction']['inquiry_id'] = $this->PropertyInquirie->id;
                $this->PropertyNotifiction->save($this->request->data);


                $this->Session->setFlash('Thanks for Contact! We will Get Back to You Shortly', 'default', array('class' => 'green'));
            }
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Property'));
        }


        if (!$propertydetails) {
            throw new NotFoundException(__('Invalid Property'));
        }

        /* Get property option for selected property type */
       $propertyoption = $this->MstPropertyTypeOption->find('all', array(
            'conditions' => array(
                'property_option_id' => $propertydetails['Property']['property_type_id']
				)
        ));
		//echo '<pre>';print_r($propertyoption);

        $project = $this->Project->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));
        $projectdetail = $this->ProjectDetails->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_financer = $this->ProjectFinancer->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $project_pricing = $this->ProjectPricing->find('all', array(
            'conditions' => array(
                'project_id' => $propertydetails['Property']['project_id']
            )
        ));

        $Websiteuser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'id' => $propertydetails['Property']['user_id']
            )
        ));
        $builder = $this->Builder->find('all', array(
            'conditions' => array(
                'websiteuser_id' => $propertydetails['Property']['user_id']
            )
        ));
        $pastproject = $this->Project->find('list', array(
            'fields' => array(
                'project_id',
                'project_name',
            ),
            'conditions' => array(
                'user_id' => $propertydetails['Property']['user_id'],
                'approved' => '1',
                'current_project' => '0'
            )
        ));
        $pastprojectdata = (implode($pastproject, ','));
        $propertyImages = $this->PropertyPic->find('list', array(
            'fields' => array(
                'id',
                'pic',
            ),
            'conditions' => array(
                'property_id' => $id
            )
        ));
		$property_features = $this->PropertyFeature->find('all', array(
                'conditions' => array(
                    'properties_id' => $id
                )
            ));
		$all_property_features = array();
		$all_property_features_unit = array();
		foreach($property_features as $property_val)
		{
			$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
			$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
		}
		
		
        $this->set('pastprojectdata', $pastprojectdata);
        $this->set('Websiteuser', $Websiteuser['0']);
        if(!empty($builder))
		{
        	$this->set('builder', $builder['0']);
		}
		else
		{
			$this->set('builder', $builder);
		}
		if(!empty($project_pricing))
		{
        	$this->set('project_pricing', $project_pricing['0']);
		}
		else
		{
			$this->set('project_pricing', $project_pricing);
		}
        if(!empty($project_financer))
		{
        	$this->set('project_financer', $project_financer['0']['ProjectFinancer']);
		}
		else
		{
			$this->set('project_financer', $project_financer);
		}
		
        
		if(!empty($projectdetail))
		{
        	$this->set('projectdetail', $projectdetail['0']);
		}
		else
		{
			$this->set('projectdetail', $projectdetail);
		}
        
        $this->set('propertyImages', $propertyImages);
		if(!empty($propertyoption))
		{
        	$this->set('propertyoption', $propertyoption['0']);
		}
		else
		{
			$this->set('propertyoption', $propertyoption);
		}
        $this->set('propertydetails', $propertydetails);
		
        $this->set('propertyfeaturesunit', $all_property_features_unit);
        $this->set('project', $project['0']);
		
		
        //$this->set('propertyoption', $propertyoption);
        $this->set('propertydetails', $propertydetails);
	   
    }

    public function admin_approvedproperties() {
        $underapproved = $this->Property->find('all', array(
            'conditions' => array(
                'status_id' => 2
            )
        ));
        $this->set('underapproved', $underapproved);
    }
	
	public function admin_builderproperties() {
        $underapproved = $this->Property->find('all', array(
            'conditions' => array(
                'status_id' => 3
            )
        ));
        $this->set('underapproved', $underapproved);
    }
	
	public function admin_ownerPropListSale($statusId) {		
		/*$ownerListSaleUser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'userrole' => 1
            )
        ));
		foreach($ownerListSaleUser as $ownerListSaleUserOk){
			$ownerPropListSale = array();
			$ownerPropListSale = $this->Property->find('all',
			array(
            'conditions' => array(
                'user_id' => $ownerListSaleUserOk['Websiteuser']['id'],
				'transaction_type_id' => 1
					)
				)
			);
			
		}*/
		
		
		$ownerPropListSale = $this->Property->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'websiteusers',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'websiteusers.id = Property.user_id'
                        )
                    )
                ),
                'conditions' => array(
                    'Property.transaction_type_id' => 1,
					'Property.status_id' => $statusId,
					'Property.is_active' => NULL,
					'websiteusers.userrole' => 1
                )
            ));
		
		//echo '<pre>'; print_r($ownerPropListSale);die();
		$this->set('ownerPropListSale', $ownerPropListSale);
		//echo '<pre>'; print_r($underapproved1);
    }
	
	public function admin_ownerPropListRent() {		
		/*$ownerListRentUser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'userrole' => 1
            )
        ));
		foreach($ownerListRentUser as $ownerListRentUserOk){
			$ownerPropListRent = array();
			$ownerPropListRent = $this->Property->find('all',
			array(
            'conditions' => array(
                'user_id' => $ownerListRentUserOk['Websiteuser']['id'],
				'transaction_type_id' => 2
					)
				)
			);
			
		}*/
		
		$ownerPropListRent = $this->Property->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'websiteusers',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'websiteusers.id = Property.user_id'
                        )
                    )
                ),
                'conditions' => array(
                    'Property.transaction_type_id' => 2,
					'Property.is_active' => NULL,
					'websiteusers.userrole' => 1
                )
            ));
		
		$this->set('ownerPropListRent', $ownerPropListRent);
		//echo '<pre>'; print_r($underapproved1);
    }
	
	
	public function admin_agentPropListSale($statusId) {		
		/*$agentListSaleUser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'userrole' => 1
            )
        ));
		foreach($agentListSaleUser as $agentListSaleUserOk){
			$agentPropListSale = array();
			$agentPropListSale = $this->Property->find('all',
			array(
            'conditions' => array(
                'user_id' => $agentListSaleUserOk['Websiteuser']['id'],
				'transaction_type_id' => 1
					)
				)
			);
			
		}*/
		
		
		$agentPropListSale = $this->Property->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'websiteusers',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'websiteusers.id = Property.user_id'
                        )
                    )
                ),
                'conditions' => array(
                    'Property.transaction_type_id' => 1,
					'Property.is_active' => NULL,
					'Property.status_id' => $statusId,
					'websiteusers.userrole' => 2
                )
            ));
		
		
		
		$this->set('agentPropListSale', $agentPropListSale);
		//echo '<pre>'; print_r($underapproved1);
    }
	
	
	
	public function admin_agentPropListRent($statusId) {		
		/*$agentListRentUser = $this->Websiteuser->find('all', array(
            'conditions' => array(
                'userrole' => 2
            )
        ));
		//echo '<pre>'; print_r($agentListRentUser);
		foreach($agentListRentUser as $agentListRentUserOk){
			//$agentPropListRent = array();
			echo $agentListRentUserOk['Websiteuser']['id'];
			$agentPropListRent = $this->Property->find('all',
			array(
            'conditions' => array(
                'user_id' => $agentListRentUserOk['Websiteuser']['id'],
				'transaction_type_id' => 2,
				'status_id' => $statusId 
					)
				)
			);
			
		}*/
		
		$agentListRentUser = $this->Property->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'websiteusers',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'websiteusers.id = Property.user_id'
                        )
                    )
                ),
                'conditions' => array(
                    'Property.transaction_type_id' => 2,
					'Property.is_active' => NULL,
					'Property.status_id' => $statusId,
					'websiteusers.userrole' => 2
                )
            ));
		//echo '<pre>'; print_r($agentListRentUser);
		$this->set('agentPropListRent', $agentListRentUser);
		// '<pre>'; print_r($agentPropListRent);
    }
	
	public function admin_propertyApproveAction($id) {

        if (!empty($id)) {           

            $propertyFeatureData =  $this->Propertydata->getPropertyFeatures($id);

            foreach($propertyFeatureData as $property_val)
            {
                $all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
                $all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
            }

        if(isset($all_property_features[1]) && $all_property_features[1] != ''){

            $calculatingrate = $all_property_features[1];

        }else{
            $calculatingrate = $all_property_features[20];
        }

        $this->set('calculatingrate', $calculatingrate);        

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Property->updateAll(
                array(
                    'offer_price'=>$this->request->data['Property']['offer_price'],
                    'market_price'=>$this->request->data['Property']['market_price'],
                    'status_id'=>$this->request->data['Property']['status_id']
                ),
                array(
                    'Property_id'=>$id
                ));
                $this->Session->setFlash('Property approved Succesfully!!', 'default', array('class' => 'green'));
            return $this->redirect(array('action' => 'approvedproperties'));
        }

        $propertyDeails = $this->Property->findByPropertyId($id);

            $this->request->data = $propertyDeails;
            $this->set('propertyDeails', $propertyDeails);

        } 
    }
	
	public function admin_salePropertyApproveAction($id) {

        if (!empty($id)) { 

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Property->updateAll(
                array(
                    'offer_price'=>$this->request->data['Property']['offer_price'],
                    'market_price'=>$this->request->data['Property']['market_price'],
                    'status_id'=>$this->request->data['Property']['status_id']
                ),
                array(
                    'Property_id'=>$id
                ));
                $this->Session->setFlash('Property approved Succesfully!!', 'default', array('class' => 'green'));
            return $this->redirect(array('action' => 'ownerPropListSale'));
        }

        $propertyDeails = $this->Property->findByPropertyId($id);

            $this->request->data = $propertyDeails;
            $this->set('propertyDeails', $propertyDeails);

        } 
    }
	
	
	public function admin_rentPropertyApproveAction($id) {

        if (!empty($id)) { 

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Property->updateAll(
                array(
                    'offer_price'=>$this->request->data['Property']['expected_monthly'],
                    'market_price'=>$this->request->data['Property']['market_rent'],
					'expected_monthly'=>$this->request->data['Property']['expected_monthly'],
                    'market_rent'=>$this->request->data['Property']['market_rent'],
                    'status_id'=>$this->request->data['Property']['status_id']
                ),
                array(
                    'Property_id'=>$id
                ));
                $this->Session->setFlash('Property approved Succesfully!!', 'default', array('class' => 'green'));
            return $this->redirect(array('action' => 'agentPropListRent'));
        }

        $propertyDeails = $this->Property->findByPropertyId($id);

            $this->request->data = $propertyDeails;
            $this->set('propertyDeails', $propertyDeails);

        } 
    }

    public function admin_propertyapproved($id) {
        if (!empty($id)) {
           $this->Property->Property_id = $id;
            
            $this->Property->updateAll(array('status_id'=>3),array('Property_id'=>$id));

           //$this->Property->saveField('status_id', 3);
           // $this->request->data['status_id'] = 3;
                        
           // $this->Property->save($this->request->data);
			

           //$data = array('Property_id' => $id, 'status_id' => 3);
            //$this->Property->save($data);

            $userdata = $this->Websiteuser->find('all', array('joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.user_id = Websiteuser.id', 'Property.property_id' => $id)))));
			
			$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
			$html .= '<p>Your property has been reviewed and approved by our experts and is now searchable on the website.</p>';
			$html .= '<p>You can view your property here</p>';
			$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot.'properties/viewproperty'.$id.'">Login Now</a></p>';
							
			$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
			$html .= '<p>We thank you again for this opportunity to serve you.</p>';
			$html .= '<p>Regards,</p>';
			$html .= '<p><strong>FairPockets Team<strong></p>';
				
            $Email = new CakeEmail();
            $Email->config('smtp');
			$Email->emailFormat('html');
            /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
            $Email->to($userdata['0']['Websiteuser']['email']);
            $Email->subject('Your property is now searchable!');
            $Email->send($html);


            $this->Session->setFlash('Property approved Succesfully!!', 'default', array('class' => 'green'));
            return $this->redirect(array('action' => 'admin_approvedproperties'));
        }
    }

    public function admin_propertyreject($id) {
        if (!empty($id)) {
            $this->Property->property_id = $id;
            $this->request->data['status_id'] = 6;
            $this->Property->save($this->request->data);

            $userdata = $this->Websiteuser->find('all', array('joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.user_id = Websiteuser.id', 'Property.property_id' => $id)))));
			
			$html = 'Dear ' . $userdata['0']['Websiteuser']['username'];
			$html .= '<p>From our research we have concluded that your property is not fit to go live our website.</p>';
			$html .= '<p>For any support needs please reply to this mail or call our helpline: +91-99109 16878</p>';
			$html .= '<p>We hope to help with your other properties.</p>';
			$html .= '<p>Regards,</p>';
			$html .= '<p><strong>FairPockets Team<strong></p>';
			
            $Email = new CakeEmail();
            $Email->config('smtp');
			$Email->emailFormat('html');
            /* $Email->from(array('Support@FairPockets.com' => 'FairPockets')); */
            $Email->to($userdata['0']['Websiteuser']['email']);
            $Email->subject('Your property was not approved');
            $Email->send($html);


            $this->Session->setFlash('Property has been Rejected', 'default', array('class' => 'green'));
            return $this->redirect(array('action' => 'admin_approvedproperties'));
        }
    }

    public function admin_searchproperties() {
        $allprojdata = '';
        if ($this->request->is('post')) {
            $websiteuserid = $this->Websiteuser->find('all', array(
                'fields' => array(
                    'id',
                    'userrole'
                ),
                'conditions' => array(
                    'usermobile' => $this->request->data['search']
                )
            ));
            if (!empty($websiteuserid)) {
                $searchproperty = $this->Property->find('all', array(
                    'conditions' => array(
                        'status' => 3,
                        'user_id' => $websiteuserid[0]['Websiteuser']['id']
                    )
                ));
                foreach ($searchproperty as $proj) {

                    $propertyoption = $this->PropertyTypeOptions->find('all', array(
                        'conditions' => array(
                            ' 	property_option_id' => $proj['Property']['property_type_id']
                        )
                    ));

                    $merge_data = $proj;

                    if (!empty($propertyoption['0'])) {
                        $merge_data = array_merge($merge_data, $propertyoption['0']);
                    }
                    $allprojdata[] = $merge_data;
                }
                $this->set('searchproperty', $allprojdata);
                $this->set('userrole', $websiteuserid[0]['Websiteuser']['userrole']);
            }
        }
    }
public function saveLead() {
			$this->layout = false;
			$response = array();
			if ($this->request->is('post')) { 
				$this->request->data['interested'] = implode(',', $this->request->data['interested']);
			/*print_r($this->request);exit;	*/
				$mobile = $this->request->data['phone'];
				$otp = $this->generateOtp();
				if (!isset($otp) || strlen($otp)<3 || strlen($otp)>11) {
					$otp = $this->generateOtp();
				}
			
				$message = $this->messageTemplate = "Your otp is $otp. Please do not share it with anybody";
				$ret = $this->sendOTP($mobile, $message, $otp);
				echo "SendOTP=".json_encode($ret);
				
				//exit;
				//$this->Property->save($this->request->data);
				if ($this->Lead->save($this->request->data)) {
					$response = array('status'=>'success', 'message' => 'Thank You for your interest, Our team will contact you soon.');
					} else {
					$response = array('status'=>'error', 'message' => 'Something went wrong!. Please try again after some time.');
				}
				}else{
				$response = array('status'=>'error', 'message' => 'No data coming from form. Try again after sometime.');
			}
			echo json_encode($response);die;
		}
		
		/**
		* Returns the 4 digit otp
		* @returns {integer} 4 digit otp
		*/
		public function generateOtp() {
		   return rand(1000, 9000);
		}
		private function sendOTP($mobile, $message, $otp){
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
			//CURLOPT_URL => "http://api.msg91.com/api/sendotp.php?authkey=178586A0aldZIg59dbed56&mobile=&message=&sender=",
			//CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?authkey=178586A0aldZIg59dbed56&mobile=$mobile&message=$message&otp=$otp",
			CURLOPT_URL => "http://control.msg91.com/api/sendotp.php?authkey=178586A0aldZIg59dbed56&message=$message&sender=SOCKET&mobile=$mobile&OTP=$otp",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
				return "cURL Error #:" . $err;
				} else {
				return $response;
			}
		}
}
