<?php

class CountriesController extends AppController {

    public $helpers = array('Html', 'Form', 'Session');
    public $components = array('RequestHandler');
    public $uses = array('MstAreaCity', 'MstAreaLocality', 'MstAreaState');

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('getpecity');
    }

    /* get locality for add property */

    public function getLocality() {
        $localities = array();

        if (isset($this->request['data']['id'])) {

            $localities = $this->MstAreaLocality->find('list', array(
                'fields' => array(
                    'localityCode',
                    'localityName',
                ),
                'conditions' => array(
                    'cityCode' => $this->request['data']['id']
                )
            ));
        }

        header('Content-Type: application/json');
        echo json_encode($localities);
        exit();
    }

    public function getLocalitiesByCity($cityId) {
        $localities = array();

        if (isset($cityId) && !empty($cityId)) {

            $localities = $this->MstAreaLocality->find('list', array(
                'fields' => array(
                    'localityCode',
                    'localityName',
                ),
                'conditions' => array(
                    'cityCode' => $cityId
                )
            ));
        }

        header('Content-Type: application/json');
        echo json_encode($localities);
        exit();
    }

    /* get cities for add property */

    public function getCities($stateCode) {
        $cities = [];
        $cities = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            ),
            'conditions' => array(
                'stateCode' => $stateCode
            )
        ));
        $this->response->type('json');
        $this->response->body(json_encode($cities));
        return $this->response;
    }

    public function getRecity() {


        $recities = array();

        if (isset($this->request->data['id'])) {

            $recities = $this->AreaCityMaster->find('list', array(
                'fields' => array(
                    'citycode',
                    'cityname',
                ),
                'conditions' => array(
                    'statecode' => $this->request->data['id']
                )
            ));
        }
        $this->set('recities', $recities);
        $this->layout = 'ajax';   // to avoid returning your full layout
        $this->view = "locationajax";
    }

    public function getpecity($stateCode) {


        $cities = [];
        $cities = $this->MstAreaCity->find('list', array(
            'fields' => array(
                'cityCode',
                'cityName',
            ),
            'conditions' => array(
                'stateCode' => $stateCode
            )
        ));
        $this->response->type('json');
        $this->response->body(json_encode($cities));
        return $this->response;
    }

    public function admin_getLocality() {
        $localities = array();

        if (isset($this->request['data']['id'])) {

            $localities = $this->AreaLocalMaster->find('list', array(
                'fields' => array(
                    'localityCode',
                    'localityName',
                ),
                'conditions' => array(
                    'cityCode' => $this->request['data']['id']
                )
            ));
        }

        $this->set('localities', $localities);
        $this->layout = 'ajax';   // to avoid returning your full layout
        $this->view = "locationajax";
    }

    /* get cities for add property */

    public function admin_getCities() {


        $cities = array();

        if (isset($this->request['data']['id'])) {

            $cities = $this->MstAreaCity->find('list', array(
                'fields' => array(
                    'cityCode',
                    'cityName',
                ),
                'conditions' => array(
                    'stateCode' => $this->request['data']['id']
                )
            ));
        }

        $this->set('cities', $cities);
        $this->layout = 'ajax';   // to avoid returning your full layout
        $this->view = "locationajax";
    }

    public function admin_getstates() {


        $states = array();

        if (isset($this->request['data']['id'])) {

            $states = $this->MstAreaState->find('list', array(
                'fields' => array(
                    'stateCode',
                    'stateName',
                ),
                'conditions' => array(
                    'regionCode' => $this->request['data']['id']
                )
            ));
        }

        $this->set('states', $states);
        $this->layout = 'ajax';   // to avoid returning your full layout
        $this->view = "locationajax";
    }

}

?>