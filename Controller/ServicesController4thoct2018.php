<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ServicesController extends AppController {

    public $uses = array('Property','User','Project','ProjectDetails','ProjectPricing','Websiteuser',        
		'Lead','OtpApp','SolrData','AppClientLead','AppReminderLead',
		'InventoryManager.InvProject',
        'InventoryManager.InvProperty',
        'InventoryManager.InvInventory',
        'InventoryManager.InvInventoryPropertyStatus',
        'InventoryManager.InvInventoryPropertyPosition',
        'InventoryManager.InvWebsiteuser',
        'InventoryManager.InvInventoryProperty');
		
	public $components = array('Paginator','Propertydata','RequestHandler');
	var $helpers = array('Html','Form','Number');

    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('getInvArea','getInvDetails1','getInvStatus','getTowerDropdownByUserIdInv','getBuilderMessage','getCalculatorDataByRegistration', 'getCalculatorDataByDiscount', 'getClientDataByUserIdClientId',
		'getCalculatorDataByFloor','getClientDataByUserId',
							'getReminderDataByUserId','postCalculatorDataSubmitTesting','postCalculatorDataSubmit','getCalculatorData',
							'getCalculatorByProjectId', 'getCalculatorByProjectIdTesting', 'getBuildersByUserId',
							'salesLoginAuth', 'validateMobile', 'verifyOtp', 'getUnitsByProjectId', 'getProjectsByUserId', 'pdf_file');
    }
	
	
	
	
	
	
	public function getCalculatorData(){
		CakeLog::write('debug', 'optionalcharge'.print_r(json_encode($this->request->data), true));
		//die();
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];

		if(isset($this->request->data['properties_id'])&& $this->request->data['properties_id']!=''){
			$properties_id = $this->request->data['properties_id'];
		}
		
		
		$ProjectChargesOptionalTotal = 0;
		for($row = 0; $row < count($optionalChargesArray); $row++) {					
				$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
				if (1 == $optionalChargesArray[$row]['unit_type']) {
					// Per Square feet 	
					$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
				} else {
					// Per Unit 	
					$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
				}           
			
			}
				
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				//$property_area = 1245;
				$gBaseRate = $this->request->data['property_detail']['base_rate'];
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				//$Registration = 5;
				
				$regValue = $this->request->data['property_detail']['registration_charges'];
				$regValueCheckUnit = explode(" ", $regValue);
				//echo '<pre>'; print_r($ok);//die();
				if($regValueCheckUnit[1]=='%'){
					//echo $ok[0] .' %';
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' %';
				}else{
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' Rs';
				}
				
				
					$selected_floor = $this->request->data['property_detail']['floor_no'];
					$projectid = $this->request->data['project_id'];
				
				// Code To get Project BSP Details per floor Start //				
					$this->loadModel('ProjectBspDetail');
					$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges,bsp_charges_unit FROM `project_bsp_details` WHERE ".$selected_floor." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
					//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
					$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];
					$gBaseRateUnit = $bsp_price[0]['project_bsp_details']['bsp_charges_unit'];				
				// Code To get Project BSP Details per floor End //
				//echo $properties_id;die();
				// Code To get Project PLC Details per floor Start //				
					$this->loadModel('ProjectPlcDetail');
					$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = '".$projectid."' AND plc_floor_no = '".$selected_floor."'");
				// Code To get Project BSP Details per floor End //
				
				// Code To get Property Possession Charges per floor Start //
					$this->loadModel('PropertyPossessionCharge');
					$PropertyPossessionChargeTotal = 0;
					//$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
					$PropertyPossessionCharge = $this->PropertyPossessionCharge->query("SELECT possession_amount FROM `property_possession_charges` WHERE  property_id = '".$properties_id."' AND possession_from_floor = '".$selected_floor."'");
					//echo '<pre>'; print_r($PropertyPossessionCharge); die();
					if(isset($PropertyPossessionCharge) && $PropertyPossessionCharge != ''){
						$PropertyPossessionChargeTotal = $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
					}
					if($PropertyPossessionChargeTotal==''){
						$PropertyPossessionChargeTotal = 0;
					}				
				// Code To get Property Possession Charges per floor End //
				
				// Code To get Property Terrace/Lawn Space Charges per floor Start //
					$this->loadModel('ProjectSpaceDetail');
					$propertyLawnArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 2");			
					if(isset($propertyLawnArray) && $propertyLawnArray != ''){
						$propertyLawnValueTotal = $propertyLawnArray[0]['project_space_details']['floor_space_area']*$propertyLawnArray[0]['project_space_details']['floor_space_amount'];
					}else{
						$propertyLawnValueTotal = 0;	
					}
					$propertyTerraceArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 1");	
					if(isset($propertyTerraceArray) && $propertyTerraceArray != ''){
						$propertyTerraceValueTotal = $propertyTerraceArray[0]['project_space_details']['floor_space_area']*$propertyTerraceArray[0]['project_space_details']['floor_space_amount'];		
					}else{
						$propertyTerraceValueTotal = 0;
					}
				
				// Code To get Property Terrace/Lawn Space Charges per floor End //
				
					if($gBaseRateUnit == 1){
						$base_price_by_base_rate_unit = $property_area*$gBaseRate;
					}
					if($gBaseRateUnit == 2){
						$base_price_by_base_rate_unit = $gBaseRate;
					}
				
					if(isset($this->request->data['gstInputCredit']) && $this->request->data['gstInputCredit'] !=''){
						$gstInputCredit = $this->request->data['gstInputCredit'];
						$gstInputCreditAmount = ($base_price_by_base_rate_unit)*$gstInputCredit/100;						
						$base_price = $base_price_by_base_rate_unit - $gstInputCreditAmount;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showInputCredit = 'true';
					}
					else{
						$base_price = $base_price_by_base_rate_unit;
						$gstInputCredit = null;
						$showInputCredit = 'false';						
					}
					
					
					
					//$this->request->data['plcCreditInput'] = 250;
					if(isset($plc_price[0]['project_plc_details']['plc_floor_charge']) && $plc_price[0]['project_plc_details']['plc_floor_charge'] !=''){
						$plcCreditInput = $plc_price[0]['project_plc_details']['plc_floor_charge'];
						//$plcAmountArea = $plcCreditInput*$property_area;
						//$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						
						if($plc_price_unit == 1){
							$plcAmountArea = $plcCreditInput*$property_area;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						if($plc_price_unit == 2){
							$plcAmountArea = $plcCreditInput;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						
						$plcAmountTotal = $plcAmountArea+$plcAmountGst;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showPlcCredit = 'true';
					}
					else{
						$plcAmountArea = 0;
						$plcCreditInput = 0;
						$showPlcCredit = 'false';						
					}
				
				//die();
				//$base_price = $property_area*$gBaseRate;
				$base_price1 = $base_price+$propertyLawnValueTotal+$propertyTerraceValueTotal;
				//echo $base_price1; die();
				$gst_base_price = ($base_price1*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price1 + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100+
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100+$plcAmountTotal;
				//$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				$final_price = $PropertyPossessionChargeTotal + $total_gst_others_base_price + ($base_price1+$ProjectChargesMandatoryTotal + $ProjectChargesOptionalTotal + $plcAmountArea + $PropertyPossessionChargeTotal)*$Registration/100;
				
				
				/*$base_price1 = $base_price+$propertyLawnValueTotal+$propertyTerraceValueTotal;
				$gst_base_price = ($base_price1*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price1 + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100 +
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100+$plcAmountTotal;
				$final_price = $total_gst_others_base_price + ($base_price1+$ProjectChargesMandatoryTotal+$ProjectChargesOptionalTotal+$plcAmountArea)*$Registration/100;*/
				
				
				
				
				
				
				
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,												
												"property_possession_charge_total"=>$PropertyPossessionChargeTotal,
												"property_terrace_value_total"=>$propertyTerraceValueTotal,
												"property_lawn_value_total"=>$propertyLawnValueTotal);
				
				$chartData = array('base_charges'=>$base_price1,
								   //'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   //'gst_total'=> ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								    'gst_total'=> $base_price1*($gServiceTaxOnBSP/100) + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationDisplay,
								   'plc_amount' => $plc_price[0]['project_plc_details']['plc_floor_charge']);
								   
				$resp = array("message"=>'Success',"code"=>1,
				              "chart_data"=>$chartData,"Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult-checkbox'.print_r($resp, true) );
				print json_encode($resp); exit;
				
				
		
	}
	
	public function getCalculatorDataByDiscount(){
		//print_r($_POST);
		//print_r($this->request->data);
		CakeLog::write('debug', 'getCalculatorDataByDiscount'.print_r(json_encode($this->request->data), true));
		//$get_response = '{"optional_charges":[{"charge_title":"Corner PLC","charge_value":"150","unit_type":"1"}]}';
		
				//$optionalChargesArray1 = json_decode($get_response);
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];

		if(isset($this->request->data['properties_id'])&& $this->request->data['properties_id']!=''){
				$properties_id = $this->request->data['properties_id'];
		}
			
		$ProjectChargesOptionalTotal = 0;
		for ($row = 0; $row < count($optionalChargesArray); $row++) {					
					$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
					if (1 == $optionalChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
					}           
				
				}
				$ProjectChargesMandatoryTotal = 0;
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				
				$selected_floor = $this->request->data['property_detail']['floor_no'];
					$projectid = $this->request->data['property_detail']['project_id'];
				
				// Code To get Project BSP Details per floor Start //				
					$this->loadModel('ProjectBspDetail');
					$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$selected_floor." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
					//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
					$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];				
				// Code To get Project BSP Details per floor End //
				
				// Code To get Project PLC Details per floor Start //				
					$this->loadModel('ProjectPlcDetail');
					$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$selected_floor."");
				// Code To get Project BSP Details per floor End //
				
				// Code To get Property Possession Charges per floor Start //
					$this->loadModel('PropertyPossessionCharge');
					$PropertyPossessionChargeTotal = 0;
					//$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
					$PropertyPossessionCharge = $this->PropertyPossessionCharge->query("SELECT possession_amount FROM `property_possession_charges` WHERE  property_id = '".$properties_id."' AND possession_from_floor = '".$selected_floor."'");
					//echo '<pre>'; print_r($PropertyPossessionCharge); die();
					if(isset($PropertyPossessionCharge) && $PropertyPossessionCharge != ''){
						$PropertyPossessionChargeTotal = $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
					}
					if($PropertyPossessionChargeTotal==''){
						$PropertyPossessionChargeTotal = 0;
					}				
				// Code To get Property Possession Charges per floor End //
				
				// Code To get Property Terrace/Lawn Space Charges per floor Start //
					$this->loadModel('ProjectSpaceDetail');
					$propertyLawnArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 2");			
					if(isset($propertyLawnArray) && $propertyLawnArray != ''){
						$propertyLawnValueTotal = $propertyLawnArray[0]['project_space_details']['floor_space_area']*$propertyLawnArray[0]['project_space_details']['floor_space_amount'];
					}else{
						$propertyLawnValueTotal = 0;	
					}
					$propertyTerraceArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 1");	
					if(isset($propertyTerraceArray) && $propertyTerraceArray != ''){
						$propertyTerraceValueTotal = $propertyTerraceArray[0]['project_space_details']['floor_space_area']*$propertyTerraceArray[0]['project_space_details']['floor_space_amount'];		
					}else{
						$propertyTerraceValueTotal = 0;
					}
				
				// Code To get Property Terrace/Lawn Space Charges per floor End //
				
				
				//$gBaseRate = $this->request->data['property_detail']['base_rate'];
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				//$Registration = 5;
				
				$disc_amount = $this->request->data['discount']['disc_amount'];
				$disc_unit  = $this->request->data['discount']['disc_unit'];
				$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
				
				
				if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 2){
						$disc_price_cal = ($property_area*$gBaseRate)*$disc_amount/100;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $property_area*$disc_amount;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				
				
				$regValue = $this->request->data['property_detail']['registration_charges'];
				$regValueCheckUnit = explode(" ", $regValue);
				//echo '<pre>'; print_r($ok);//die();
				if($regValueCheckUnit[1]=='%'){
					//echo $ok[0] .' %';
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' %';
				}else{
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' Rs';
				}
				
				
					if(isset($this->request->data['gstInputCredit']) && $this->request->data['gstInputCredit'] !=''){
						$gstInputCredit = $this->request->data['gstInputCredit'];
						$gstInputCreditAmount = ($base_price_new)*$gstInputCredit/100;						
						//$base_price = ($property_area*$gBaseRate) - $gstInputCreditAmount;
						$base_price = $base_price_new - $gstInputCreditAmount;
						$showInputCredit = 'true';
					}
					else{
						//$base_price = $property_area*$gBaseRate;
						$base_price = $base_price_new;
						$gstInputCredit = null;
						$showInputCredit = 'false';						
					}
					
				
					
					//$this->request->data['plcCreditInput'] = 250;
					if(isset($plc_price[0]['project_plc_details']['plc_floor_charge']) && $plc_price[0]['project_plc_details']['plc_floor_charge'] !=''){
						$plcCreditInput = $plc_price[0]['project_plc_details']['plc_floor_charge'];
						$plcAmountArea = $plcCreditInput*$property_area;
						$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						$plcAmountTotal = $plcAmountArea+$plcAmountGst;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showPlcCredit = 'true';
					}
					else{
						$plcAmountArea = 0;
						$plcCreditInput = 0;
						$showPlcCredit = 'false';						
					}
					
					$discountArray = array('base_price_before'=> $property_area*$gBaseRate,
											'discamount'=> $disc_amount,
											'discamount_cal'=> $disc_price_cal,
											'base_price_discounted'=> $base_price_new,
											'gstInputCreditAmount'=> $gstInputCreditAmount,
											'base_price_updated' => $base_price);
				
				//$base_price = $property_area*$gBaseRate;
				$base_price1 = $base_price+$propertyLawnValueTotal+$propertyTerraceValueTotal;
				//echo $base_price1; die();
				$gst_base_price = ($base_price1*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price1 + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100+
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100+$plcAmountTotal;
				//$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				$final_price = $PropertyPossessionChargeTotal + $total_gst_others_base_price + ($base_price1+$ProjectChargesMandatoryTotal + $ProjectChargesOptionalTotal + $plcAmountArea + $PropertyPossessionChargeTotal)*$Registration/100;
				
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate												
												//"property_possession_charge_total"=>$PropertyPossessionChargeTotal,
												//"property_terrace_value_total"=>$propertyTerraceValueTotal,
												//"property_lawn_value_total"=>$propertyLawnValueTotal
												);
								   
				$resp = array("message"=>'Success',"code"=>1,
				              "chart_data"=>$chartData,"base_rate"=>$gBaseRate,
							  "property_detail"=>$property_detail_array,
							  "discountArray" => $discountArray,
							  "Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult-discount'.print_r($resp, true) );
				print json_encode($resp); exit;	
		
	
	}
	
	
	
	public function getCalculatorDataByRegistration(){
		
		CakeLog::write('debug', 'getCalculatorDataByRegistration'.print_r($this->request->data, true) );
		//$get_response = '{"optional_charges":[{"charge_title":"Corner PLC","charge_value":"150","unit_type":"1"}]}';
		
				//$optionalChargesArray1 = json_decode($get_response);
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];

		if(isset($this->request->data['properties_id'])&& $this->request->data['properties_id']!=''){
				$properties_id = $this->request->data['properties_id'];
		}		
		$ProjectChargesOptionalTotal = 0;
		for ($row = 0; $row < count($optionalChargesArray); $row++) {					
					$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
					if (1 == $optionalChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
					}           
				
				}
				$ProjectChargesMandatoryTotal = 0;
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				//$property_area = 1245;
				/*$this->loadModel('ProjectBspDetail');
			$bspoptionid = $this->request->data['property_detail']['floor_no'];
			$projectid = $this->request->data['property_detail']['project_id'];
			$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$bspoptionid." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
			//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
				$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];
			$this->loadModel('ProjectPlcDetail');
			$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
			*/
			
			$selected_floor = $this->request->data['property_detail']['floor_no'];
					$projectid = $this->request->data['property_detail']['project_id'];
				
				// Code To get Project BSP Details per floor Start //				
					$this->loadModel('ProjectBspDetail');
					$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges,bsp_charges_unit FROM `project_bsp_details` WHERE ".$selected_floor." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
					//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
					$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];
					$gBaseRateUnit = $bsp_price[0]['project_bsp_details']['bsp_charges_unit'];
				// Code To get Project BSP Details per floor End //
				//echo '8851<pre>'; print_r($bsp_price);
				//CakeLog::write('debug', 'bsp_price'.print_r($bsp_price, true) );
				// Code To get Project PLC Details per floor Start //				
					$this->loadModel('ProjectPlcDetail');
					$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge,plc_charges_unit,plc_floor_mode FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$selected_floor."");
					$plc_price_unit = $plc_price[0]['project_plc_details']['plc_charges_unit'];
					$plc_price_mode = $plc_price[0]['project_plc_details']['plc_floor_mode'];
				// Code To get Project BSP Details per floor End //
				//echo '8841<pre>'; print_r($plc_price);die();
				//CakeLog::write('debug', 'plc_price'.print_r($plc_price, true) );
				// Code To get Property Possession Charges per floor Start //
					$this->loadModel('PropertyPossessionCharge');
					$PropertyPossessionChargeTotal = 0;
					//$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
					$PropertyPossessionCharge = $this->PropertyPossessionCharge->query("SELECT possession_amount FROM `property_possession_charges` WHERE  property_id = '".$properties_id."' AND possession_from_floor = '".$selected_floor."'");
					//echo '<pre>'; print_r($PropertyPossessionCharge); die();
					if(isset($PropertyPossessionCharge) && $PropertyPossessionCharge != ''){
						$PropertyPossessionChargeTotal = $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
					}
					if($PropertyPossessionChargeTotal==''){
						$PropertyPossessionChargeTotal = 0;
					}				
				// Code To get Property Possession Charges per floor End //
				
				// Code To get Property Terrace/Lawn Space Charges per floor Start //
					$this->loadModel('ProjectSpaceDetail');
					$propertyLawnArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 2");			
					if(isset($propertyLawnArray) && $propertyLawnArray != ''){
						$propertyLawnValueTotal = $propertyLawnArray[0]['project_space_details']['floor_space_area']*$propertyLawnArray[0]['project_space_details']['floor_space_amount'];
					}else{
						$propertyLawnValueTotal = 0;	
					}
					$propertyTerraceArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 1");	
					if(isset($propertyTerraceArray) && $propertyTerraceArray != ''){
						$propertyTerraceValueTotal = $propertyTerraceArray[0]['project_space_details']['floor_space_area']*$propertyTerraceArray[0]['project_space_details']['floor_space_amount'];		
					}else{
						$propertyTerraceValueTotal = 0;
					}
				
				// Code To get Property Terrace/Lawn Space Charges per floor End //


			
				//$gBaseRate = $this->request->data['property_detail']['base_rate'];
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				//$Registration = 5;
				
				if(isset($this->request->data['discount']['disc_amount']) && $this->request->data['discount']['disc_amount'] != ''){
					$disc_amount = $this->request->data['discount']['disc_amount'];
				$disc_unit  = $this->request->data['discount']['disc_unit'];
				}else{
					$disc_amount = 0;
				$disc_unit  = 1;
				}
				
				//$disc_amount = $this->request->data['discount']['disc_amount'];
				//$disc_unit  = $this->request->data['discount']['disc_unit'];
				$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
				
				if($gBaseRateUnit == 1){
					$base_price_by_base_rate_unit = $property_area*$gBaseRate;
				}
				if($gBaseRateUnit == 2){
					$base_price_by_base_rate_unit = $gBaseRate;
				}
				
				 //CakeLog::write('debug', 'base_price_by_base_rate_unit'.print_r($base_price_by_base_rate_unit, true) );
				 //CakeLog::write('debug', 'gBaseRateUnit'.print_r($gBaseRateUnit, true) );
				
				
				if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = $base_price_by_base_rate_unit - $disc_price_cal;					
				}
				if($disc_unit == 2){
						$disc_price_cal = $base_price_by_base_rate_unit*$disc_amount/100;					
					$base_price_new = $base_price_by_base_rate_unit - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $base_price_by_base_rate_unit;					
					$base_price_new = $base_price_by_base_rate_unit - $disc_price_cal;					
				}
				
				
				$regValue = $this->request->data['property_detail']['registration_charges'];
				
				if($regValue ==0){
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' %';
				}else{
				$regValueCheckUnit = explode(" ", $regValue);
				//echo '<pre>'; print_r($ok);//die();
				if($regValueCheckUnit[1]=='%'){
					//echo $ok[0] .' %';
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' %';
				}else{
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' Rs';
				}
				}
				
				
					if(isset($this->request->data['gstInputCredit']) && $this->request->data['gstInputCredit'] !=''){
						$gstInputCredit = $this->request->data['gstInputCredit'];
						$gstInputCreditAmount = ($base_price_new)*$gstInputCredit/100;						
						//$base_price = ($property_area*$gBaseRate) - $gstInputCreditAmount;
						$base_price = $base_price_new - $gstInputCreditAmount;
						$showInputCredit = 'true';
					}
					else{
						$base_price = $base_price_by_base_rate_unit;
						$gstInputCredit = null;
						$showInputCredit = 'false';						
					}
					
				
					
					//$this->request->data['plcCreditInput'] = 250;
					if(isset($plc_price[0]['project_plc_details']['plc_floor_charge']) && $plc_price[0]['project_plc_details']['plc_floor_charge'] !=''){
						$plcCreditInput = $plc_price[0]['project_plc_details']['plc_floor_charge'];
						
						//$plcAmountArea = $plcCreditInput*$property_area;
						//$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						
						if($plc_price_unit == 1){
							$plcAmountArea = $plcCreditInput*$property_area;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						if($plc_price_unit == 2){
							$plcAmountArea = $plcCreditInput;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						
						$plcAmountTotal = $plcAmountArea+$plcAmountGst;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showPlcCredit = 'true';
					}
					else{
						$plcAmountArea = 0;
						$plcCreditInput = 0;
						$showPlcCredit = 'false';						
					}
					
					$discountArray = array('base_price_before'=> $base_price_by_base_rate_unit,
											'discamount'=> $disc_amount,
											'discamount_cal'=> $disc_price_cal,
											'base_price_discounted'=> $base_price_new,
											'gstInputCreditAmount'=> $gstInputCreditAmount,
											'base_price_updated' => $base_price);
				
				
				$base_price1 = $base_price+$propertyLawnValueTotal+$propertyTerraceValueTotal;
				//echo $base_price1; die();
				$gst_base_price = ($base_price1*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price1 + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100+
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100+$plcAmountTotal;
				//$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				$final_price = $PropertyPossessionChargeTotal + $total_gst_others_base_price + ($base_price1+$ProjectChargesMandatoryTotal + $ProjectChargesOptionalTotal + $plcAmountArea + $PropertyPossessionChargeTotal)*$Registration/100;
				
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,												
												"property_possession_charge_total"=>$PropertyPossessionChargeTotal,
												"property_terrace_value_total"=>$propertyTerraceValueTotal,
												"property_lawn_value_total"=>$propertyLawnValueTotal);
				
				$chartData = array('base_charges'=>$base_price1,
								   //'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   //'gst_total'=> ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								    'gst_total'=> $base_price1*($gServiceTaxOnBSP/100) + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationDisplay,
								   'plc_amount' => $plc_price[0]['project_plc_details']['plc_floor_charge']);
				
								   
				$resp = array("message"=>'Success',"code"=>1,
							"property_detail"=>$property_detail_array,
				              "chart_data"=>$chartData,"base_rate"=>$gBaseRate,
							  "Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult-registration'.print_r($resp, true) );
				print json_encode($resp); exit;	
		
	
	}
	
	
	
	public function getCalculatorDataByFloor(){
		//print_r($_POST);
		//print_r($this->request->data);
		CakeLog::write('debug', 'getCalculatorDataByFloor'.print_r(json_encode($this->request->data), true) );
		//$get_response = '{"optional_charges":[{"charge_title":"Corner PLC","charge_value":"150","unit_type":"1"}]}';
		
		if(isset($this->request->data['property_detail']['properties_id'])&& $this->request->data['property_detail']['properties_id']!=''){
				$properties_id = $this->request->data['property_detail']['properties_id'];
		}
		//echo $properties_id;die();
		//$properties_id = 27378763552341;
		
				//$optionalChargesArray1 = json_decode($get_response);
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];;		
		$ProjectChargesOptionalTotal = 0;
		for ($row = 0; $row < count($optionalChargesArray); $row++) {					
					$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
					if (1 == $optionalChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
					}           
				
				}
				$ProjectChargesMandatoryTotal = 0;
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				//$property_area = 1245;
				
					$selected_floor = $this->request->data['property_detail']['floor_no'];
					$projectid = $this->request->data['property_detail']['project_id'];
				
				// Code To get Project BSP Details per floor Start //				
					$this->loadModel('ProjectBspDetail');
					$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges,bsp_charges_unit FROM `project_bsp_details` WHERE ".$selected_floor." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
					//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
					$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];
					$gBaseRateUnit = $bsp_price[0]['project_bsp_details']['bsp_charges_unit'];				
				// Code To get Project BSP Details per floor End //
				
				// Code To get Project PLC Details per floor Start //				
					$this->loadModel('ProjectPlcDetail');
					$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge,plc_charges_unit,plc_floor_mode FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$selected_floor."");
					$plc_price_unit = $plc_price[0]['project_plc_details']['plc_charges_unit'];
					$plc_price_mode = $plc_price[0]['project_plc_details']['plc_floor_mode'];
				// Code To get Project BSP Details per floor End //
				
				// Code To get Property Possession Charges per floor Start //
					$this->loadModel('PropertyPossessionCharge');
					$PropertyPossessionChargeTotal = 0;
					//$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
					$PropertyPossessionCharge = $this->PropertyPossessionCharge->query("SELECT possession_amount FROM `property_possession_charges` WHERE  property_id = '".$properties_id."' AND possession_from_floor = '".$selected_floor."'");
					//echo '<pre>'; print_r($PropertyPossessionCharge); die();
					if(isset($PropertyPossessionCharge) && $PropertyPossessionCharge != ''){
						$PropertyPossessionChargeTotal = $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
					}
					if($PropertyPossessionChargeTotal==''){
						$PropertyPossessionChargeTotal = "0";
					}				
				// Code To get Property Possession Charges per floor End //
				
				// Code To get Property Terrace/Lawn Space Charges per floor Start //
					$this->loadModel('ProjectSpaceDetail');
					$propertyLawnArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 2");			
					if(isset($propertyLawnArray) && $propertyLawnArray != ''){
						$propertyLawnValueTotal = $propertyLawnArray[0]['project_space_details']['floor_space_area']*$propertyLawnArray[0]['project_space_details']['floor_space_amount'];
					}else{
						$propertyLawnValueTotal = 0;	
					}
					$propertyTerraceArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = '".$selected_floor."' AND space_mode = 1");	
					if(isset($propertyTerraceArray) && $propertyTerraceArray != ''){
						$propertyTerraceValueTotal = $propertyTerraceArray[0]['project_space_details']['floor_space_area']*$propertyTerraceArray[0]['project_space_details']['floor_space_amount'];		
					}else{
						$propertyTerraceValueTotal = 0;
					}
				
				// Code To get Property Terrace/Lawn Space Charges per floor End //
				
				//$gBaseRate = $this->request->data['property_detail']['base_rate'];
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				//$Registration = 5;
				
				
				$regValue = $this->request->data['property_detail']['registration_charges'];
				$regValueCheckUnit = explode(" ", $regValue);
				//echo '<pre>'; print_r($ok);//die();
				if($regValueCheckUnit[1]=='%'){
					//echo $ok[0] .' %';
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' %';
				}else{
					$Registration = $regValueCheckUnit[0];
					$RegistrationDisplay = $regValueCheckUnit[0].' Rs';
				}
				
				
				if($gBaseRateUnit == 1){
					$base_price_by_base_rate_unit = $property_area*$gBaseRate;
				}
				if($gBaseRateUnit == 2){
					$base_price_by_base_rate_unit = $gBaseRate;
				}
				
				
					if(isset($this->request->data['gstInputCredit']) && $this->request->data['gstInputCredit'] !=''){
						$gstInputCredit = $this->request->data['gstInputCredit'];
						$gstInputCreditAmount = ($base_price_by_base_rate_unit)*$gstInputCredit/100;						
						$base_price = ($base_price_by_base_rate_unit) - $gstInputCreditAmount;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showInputCredit = 'true';
					}
					else{
						$base_price = $base_price_by_base_rate_unit;
						$gstInputCredit = null;
						$showInputCredit = 'false';						
					}
					
				
					
					//$this->request->data['plcCreditInput'] = 250;
					if(isset($plc_price[0]['project_plc_details']['plc_floor_charge']) && $plc_price[0]['project_plc_details']['plc_floor_charge'] !=''){
						$plcCreditInput = $plc_price[0]['project_plc_details']['plc_floor_charge'];
						//$plcAmountArea = $plcCreditInput*$property_area;
						//$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						
						if($plc_price_unit == 1){
							$plcAmountArea = $plcCreditInput*$property_area;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						if($plc_price_unit == 2){
							$plcAmountArea = $plcCreditInput;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						
						$plcAmountTotal = $plcAmountArea+$plcAmountGst;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showPlcCredit = 'true';
					}
					else{
						$plcAmountArea = 0;
						$plcCreditInput = 0;
						$showPlcCredit = 'false';						
					}
					
				
				//$base_price = $property_area*$gBaseRate;
				$base_price1 = $base_price+$propertyLawnValueTotal+$propertyTerraceValueTotal;
				//echo $base_price1; die();
				$gst_base_price = ($base_price1*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price1 + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100+$plcAmountTotal;
				//$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				$final_price = $PropertyPossessionChargeTotal + $total_gst_others_base_price + ($base_price1+$ProjectChargesMandatoryTotal + $plcAmountArea + $PropertyPossessionChargeTotal)*$Registration/100;
				
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,												
												"property_possession_charge_total"=>$PropertyPossessionChargeTotal,
												"property_terrace_value_total"=>$propertyTerraceValueTotal,
												"property_lawn_value_total"=>$propertyLawnValueTotal);
				
				$chartData = array('base_charges'=>$base_price1,
								   //'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   //'gst_total'=> ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								    'gst_total'=> $base_price1*($gServiceTaxOnBSP/100) + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationDisplay,
								   'plc_amount' => $plc_price[0]['project_plc_details']['plc_floor_charge']);
								   
				$resp = array("message"=>'Success',"code"=>1,
								"property_detail"=>$property_detail_array,
				              "chart_data"=>$chartData,"base_rate"=>$gBaseRate,"Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult'.print_r($resp, true) );
				print json_encode($resp); exit;		
				
		
	}
	
	
	
	function _newWorkorderpdf($workorderHTMLName, $workorderPDFName) {
        // Include Component 
        App::import('Component', 'Pdf');
        // Make instance 
        $Pdf = new PdfComponent();
        // Invoice name (output name) 
        $Pdf->filename = 'workorder-' . $workorderPDFName; // Without .pdf 
        // You can use download or browser here 
        //$Pdf->output = 2;//'download'; 
        $Pdf->output = 'download';
        //$Pdf->method = 'pdflib';
        $Pdf->init();
        $Pdf->process(Router::url('/', true) . 'app/webroot/files/workorders/' . $workorderHTMLName);		
        return 1;
        die;
    }
	
	function _newWorkorderpdf1($workorderHTMLName, $workorderPDFName) {
        // Include Component 
        App::import('Component', 'Pdf');
        // Make instance 
        $Pdf1 = new PdfComponent();
        // Invoice name (output name) 
        $Pdf1->filename = 'workorder-' . $workorderPDFName; // Without .pdf 
        // You can use download or browser here 
        //$Pdf->output = 2;//'download'; 
        $Pdf1->output = 'download';
        //$Pdf->method = 'pdflib';
        $Pdf1->init();
        $Pdf1->process(Router::url('/', true) . 'app/webroot/files/workorders/' . $workorderHTMLName);		
        return 1;
        die;
    }
	
	public function pdf_file(){ 
	
			$this->layout = "empty";			
			
				require_once(ROOT . DS . "vendors/pdfapi".DS."tcpdf.php");
				$this->view = 'pdf_file';
				
				
				
	}
	
	
	public function postCalculatorDataSubmit(){
	
			CakeLog::write('debug', 'postCalculatorDataSubmit'.print_r(json_encode($this->request->data), true) );
			
			$this->request->data['AppClientLead']['user_id'] = $this->request->data['user_id'];
			$this->request->data['AppClientLead']['email'] = $this->request->data['clientDetails']['email'];
			$this->request->data['AppClientLead']['mobile'] = $this->request->data['clientDetails']['mobile'];
			$this->request->data['AppClientLead']['name'] = $this->request->data['clientDetails']['name'];
			$this->request->data['AppClientLead']['remarks'] = $this->request->data['clientDetails']['remarks'];
			$this->request->data['AppClientLead']['reminderDate'] = $this->request->data['clientDetails']['reminderDate'];
			$this->request->data['AppClientLead']['reminderText'] = $this->request->data['clientDetails']['reminderText'];
			$this->request->data['AppClientLead']['status'] = 1;
		   //print_r($this->request->data['AppClientLead']);
		   
		   $query = "Select * from app_client_leads WHERE email = '".$this->request->data['AppClientLead']['email']."' OR mobile = '".$this->request->data['AppClientLead']['mobile']."' AND user_id = '".$this->request->data['user_id']."'";
			
		   $clientExistOrNot = $this->AppClientLead->query($query);
		   
		   $this->loadModel('AppClientLeadsHistory');
		   if($clientExistOrNot){				
				$this->request->data['AppClientLeadsHistory']['client_id'] = $clientExistOrNot[0]['app_client_leads']['id'];
				$this->request->data['AppClientLeadsHistory']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppClientLeadsHistory']['project_id'] = $this->request->data['project_id'];
				$this->request->data['AppClientLeadsHistory']['name'] = $this->request->data['AppClientLead']['name'];
				$this->request->data['AppClientLeadsHistory']['remarks'] = $this->request->data['AppClientLead']['remarks'];
				$this->request->data['AppClientLeadsHistory']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppClientLeadsHistory']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				
				$linkId1 = $clientExistOrNot[0]['app_client_leads']['id'].'_'.$this->request->data['project_id'];
				
				$this->request->data['AppClientLeadsHistory']['pdf_link'] = 'http://fairpockets.com/files/workorders/project-'.$linkId1.'.pdf';
				
				$savdata = $this->AppClientLeadsHistory->save($this->request->data['AppClientLeadsHistory']);
				$lastinsertId = $clientExistOrNot[0]['app_client_leads']['id'];
			}else{				
				$savdata = $this->AppClientLead->save($this->request->data['AppClientLead']);
				$lastinsertId = $this->AppClientLead->getLastInsertId();

				$this->request->data['AppClientLeadsHistory']['client_id'] = $lastinsertId;
				$this->request->data['AppClientLeadsHistory']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppClientLeadsHistory']['project_id'] = $this->request->data['project_id'];
				$this->request->data['AppClientLeadsHistory']['name'] = $this->request->data['AppClientLead']['name'];
				$this->request->data['AppClientLeadsHistory']['remarks'] = $this->request->data['AppClientLead']['remarks'];
				$this->request->data['AppClientLeadsHistory']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppClientLeadsHistory']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				
				$linkId1 = $lastinsertId.'_'.$this->request->data['project_id'];
				
				$this->request->data['AppClientLeadsHistory']['pdf_link'] = 'http://fairpockets.com/files/workorders/project-'.$linkId1.'.pdf';
				
				$savdata = $this->AppClientLeadsHistory->save($this->request->data['AppClientLeadsHistory']);
				
			}
		   
		   
			//if ($this->AppClientLead->save($this->request->data['AppClientLead'])) {
				$this->layout = 'empty';
				//$lastinsertId = $this->AppClientLead->getLastInsertId();
				
				$this->request->data['AppReminderLead']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppReminderLead']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppReminderLead']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				$this->request->data['AppReminderLead']['status'] = 1;
				//CakeLog::write('debug', 'AppReminderLead - '.print_r($this->request->data['AppReminderLead'], true) );
				$this->AppReminderLead->save($this->request->data['AppReminderLead']);
				
				//$pdfdataview = $this->request->data;
				
				$view1 = new View($this);
				$Custom1 = $view1->loadHelper('Number');
				$projectName = $Custom1->getProjectNameByProjectId($this->request->data['project_id']);
				
				
				$this->loadModel('Project');
				$builderIdByProjectId= $this->Project->find('first', array(
					'fields' => array(
						'user_id'
					),
					'conditions' => array(
						'project_id' => $this->request->data['project_id']
					)
				));
				
				$this->loadModel('Builder');
				$builderlogoById = $this->Builder->find('first', array(
					'fields' => array(
						'borg_logo'
					),
					'conditions' => array(
						'websiteuser_id' => $builderIdByProjectId['Project']['user_id']
					)
				));
				$builderlogo = $builderlogoById['Builder']['borg_logo'];
				if($builderlogoById['Builder']['borg_logo']!=''){
				$builderlogo = $builderlogoById['Builder']['borg_logo'];	
				}else{
					$builderlogo = 'logo.jpg';
				}
				
				
				$this->loadModel('Websiteuser');
				$builderNameById = $this->Websiteuser->find('first', array(
					'fields' => array(
						'userorgname'
					),
					'conditions' => array(
						'id' => $builderIdByProjectId['Project']['user_id']
					)
				));
				$buildername = $builderNameById['Websiteuser']['userorgname'];
				
				$this->set('pdfGenerateRequest', true);
				$this->set('projectId', $this->request->data['project_id']);
				$this->set('projectName', $projectName[0]['Project']['project_name']);
				$this->set('baseRate', $this->request->data['property_detail']['base_rate']);
				$this->set('propertyArea', $this->request->data['property_detail']['property_area']);
				$this->set('propertyOnFloor', $this->request->data['property_detail']['floor_no']);
				$this->set('baseCharges', $this->request->data['chart_data']['base_charges']);
				$this->set('additionalCharges', $this->request->data['chart_data']['additional_charges']);
				$this->set('gstTotal', $this->request->data['chart_data']['gst_total']);
				$this->set('stampRegTotal', $this->request->data['chart_data']['stamp_plus_registration']);
				$this->set('finalCalculatePrice', $this->request->data['final_calculated_price']);
				$this->set('visitorName', $this->request->data['AppClientLead']['name']);
				$this->set('builderlogo', $builderlogo);
				$this->set('buildername', $buildername);
				
				// For project 917 Start //
				if($this->request->data['project_id'] == 917){
					
					$this->loadModel('ProjectPlcDetail');
					$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = '".$this->request->data['project_id']."' AND plc_floor_no = '".$this->request->data['property_detail']['floor_no']."'");
				
					
					$bsprow1 = $this->request->data['property_detail']['base_rate'] * $this->request->data['property_detail']['property_area'];
					$gstInputCreditAmount = $this->request->data['gstInputCredit'];
					$gstInputCreditAmountCalculated = $bsprow1*$gstInputCreditAmount/100;
					$addGstAmount = $this->request->data['property_detail']['gst_on_base'];
					$addGstAmountCalculated = $bsprow1*$addGstAmount/100;
					$effectiveBSP = $bsprow1+$addGstAmountCalculated;
					$floor_no = $this->request->data['property_detail']['floor_no'];
					$floorCharges = $plc_price[0]['project_plc_details']['plc_floor_charge']*$this->request->data['property_detail']['property_area'];
					$newGST = $this->request->data['property_detail']['floor_no']*$this->request->data['property_detail']['property_area']*18;
					$totalPricenew = $bsprow1+$addGstAmountCalculated+$newGST;
					
					$this->set('bsprow1', $bsprow1);
					$this->set('gstInputCreditAmount', $gstInputCreditAmount);
					$this->set('gstInputCreditAmountCalculated', $gstInputCreditAmountCalculated);
					$this->set('addGstAmount', $addGstAmount);
					//$this->set('addGstAmountCalculated', $addGstAmountCalculated);
					$this->set('addGstAmountCalculated', $this->request->data['chart_data']['gst_total']);
					$this->set('effectiveBSP', $effectiveBSP);
					$this->set('floor_no', $floor_no);
					$this->set('floorCharges', $floorCharges);
					$this->set('newGST', $newGST);
					$this->set('totalPricenew', $this->request->data['final_calculated_price']);
				}
				// For project 917 End  //
				
				
				$this->loadModel('SalesUser');
				$usersDatas = $this->SalesUser->find('all', array(
					'conditions' => array(
						'id' => $this->request->data['user_id']
					)
				));
				$this->set('userName', $usersDatas[0]['SalesUser']['name']);
				$this->set('userMobile', $usersDatas[0]['SalesUser']['mobile']);
				
				
				/*$viewdata = $this->render('post_calculator_data_submit');
				
				$linkId = $lastinsertId.'_'.$this->request->data['project_id'];
				$workorderHTMLName = "workorder_html4pdf.html";
				$workorderPDFName = 'project-'.$linkId;
				$path = WWW_ROOT . "files/workorders/" . $workorderHTMLName;
				
				$file = new File($path, true);
				
				if ($file->write($viewdata)) {
					$this->_newWorkorderpdf($workorderHTMLName, $workorderPDFName);				
					$docketFileFullPath = _ROOT_PATH . 'vendors/html2ps/public_html/out/workorder-project-'.$linkId.'.pdf';
					$newFileFullPath = _ROOT_PATH . 'app/webroot/files/workorders/project-'.$linkId.'.pdf';
					$isPdf = true;
					copy($docketFileFullPath, $newFileFullPath);
						unlink($path);
						unlink($docketFileFullPath);
					
				}*/
				
				//$this->layout = "empty";
				$linkId = $lastinsertId.'_'.$this->request->data['project_id'];	
				$this->set('linkid', $linkId);
				
				//$this->layout = "empty";			
			
				require_once(ROOT . DS . "vendors/pdfapi".DS."tcpdf.php");
				if($this->request->data['project_id'] == 917){
					$this->view = 'post_calculator_data_submit1';
				}else{
					$this->view = 'post_calculator_data_submit';
				}
				
				
				//CakeLog::write('debug', 'pdf-debug'.print_r($this->request->data, true) );
				
					$pdf_link = 'http://fairpockets.com/files/workorders/project-'.$linkId.'.pdf';
					$resp = array("message"=>'Success',"code"=>1,"pdf_link"=>(string)$pdf_link,"data"=>null);
					
					CakeLog::write('debug', 'myArray-submit'.print_r($resp, true) );
					
					print json_encode($resp,JSON_UNESCAPED_SLASHES); //exit;
				//}
						
		//}
		
		
	}
	
	
	public function postCalculatorDataSubmitTesting(){
	
			CakeLog::write('debug', 'postCalculatorDataSubmit'.print_r(json_encode($this->request->data), true) );
			$jsondata = '{"chart_data":{"additional_charges":"46700","base_charges":"10390750","gst_total":"1255296","stamp_plus_registration":"5 %"},"clientDetails":{"email":"bdhdg@gmail.com","mobile":"9464664646","name":"bdbdhfnb","remarks":"hsgdg","reminderDate":"30-09-2018","reminderText":"hshhd"},"code":"1","discount":{"disc_amount":""},"final_calculated_price":"13096221","gstInputCredit":"0","mandatory_charges":[{"charge_title":"Power BackUp per KVA","charge_value":"0","unit_type":2},{"charge_title":"Interest Free Maintenance Deposit","charge_value":"20","unit_type":1}],"message":"Success","optional_charges":[{"charge_title":"Corner PLC","charge_value":"50","isChecked":true,"serialNo":103,"unit_type":1},{"charge_title":"Double Covered Car Park","charge_value":"600000","isChecked":true,"serialNo":104,"unit_type":2}],"project_id":"701","properties_id":"27378763551994","property_detail":{"base_rate":"4450","floor_no":"7","gst_on_base":"12","gst_on_others":"18","project_id":"701","properties_id":"27378763551994","property_area":"2335","property_lawn_value_total":0,"property_possession_charge_total":"0","property_terrace_value_total":0,"registration_charges":"5 %","stamp_duty":"Rs 0"},"showDiscount":true,"showInputCredit":true,"user_id":"161"}';
			$this->request->data = json_decode($jsondata, true);
			//echo '<pre>'; print_r($this->request->data);die();
			$this->request->data['AppClientLead']['user_id'] = $this->request->data['user_id'];
			$this->request->data['AppClientLead']['email'] = $this->request->data['clientDetails']['email'];
			$this->request->data['AppClientLead']['mobile'] = $this->request->data['clientDetails']['mobile'];
			$this->request->data['AppClientLead']['name'] = $this->request->data['clientDetails']['name'];
			$this->request->data['AppClientLead']['remarks'] = $this->request->data['clientDetails']['remarks'];
			$this->request->data['AppClientLead']['reminderDate'] = $this->request->data['clientDetails']['reminderDate'];
			$this->request->data['AppClientLead']['reminderText'] = $this->request->data['clientDetails']['reminderText'];
			$this->request->data['AppClientLead']['status'] = 1;
		    //print_r($this->request->data['AppClientLead']);
		   
		   $query = "Select * from app_client_leads WHERE email = '".$this->request->data['AppClientLead']['email']."' OR mobile = '".$this->request->data['AppClientLead']['mobile']."' AND user_id = '".$this->request->data['user_id']."'";
			
		   $clientExistOrNot = $this->AppClientLead->query($query);
		   
		   $this->loadModel('AppClientLeadsHistory');
		   if($clientExistOrNot){				
				$this->request->data['AppClientLeadsHistory']['client_id'] = $clientExistOrNot[0]['app_client_leads']['id'];
				$this->request->data['AppClientLeadsHistory']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppClientLeadsHistory']['project_id'] = $this->request->data['project_id'];
				$this->request->data['AppClientLeadsHistory']['name'] = $this->request->data['AppClientLead']['name'];
				$this->request->data['AppClientLeadsHistory']['remarks'] = $this->request->data['AppClientLead']['remarks'];
				$this->request->data['AppClientLeadsHistory']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppClientLeadsHistory']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				
				$linkId1 = $clientExistOrNot[0]['app_client_leads']['id'].'_'.$this->request->data['project_id'];
				
				$this->request->data['AppClientLeadsHistory']['pdf_link'] = 'http://fairpockets.com/files/workorders/project-'.$linkId1.'.pdf';
				
				$savdata = $this->AppClientLeadsHistory->save($this->request->data['AppClientLeadsHistory']);
				$lastinsertId = $clientExistOrNot[0]['app_client_leads']['id'];
			}else{				
				$savdata = $this->AppClientLead->save($this->request->data['AppClientLead']);
				$lastinsertId = $this->AppClientLead->getLastInsertId();

				$this->request->data['AppClientLeadsHistory']['client_id'] = $lastinsertId;
				$this->request->data['AppClientLeadsHistory']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppClientLeadsHistory']['project_id'] = $this->request->data['project_id'];
				$this->request->data['AppClientLeadsHistory']['name'] = $this->request->data['AppClientLead']['name'];
				$this->request->data['AppClientLeadsHistory']['remarks'] = $this->request->data['AppClientLead']['remarks'];
				$this->request->data['AppClientLeadsHistory']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppClientLeadsHistory']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				
				$linkId1 = $lastinsertId.'_'.$this->request->data['project_id'];
				
				$this->request->data['AppClientLeadsHistory']['pdf_link'] = 'http://fairpockets.com/files/workorders/project-'.$linkId1.'.pdf';
				
				$savdata = $this->AppClientLeadsHistory->save($this->request->data['AppClientLeadsHistory']);
				
			}
		   
		   
			//if ($this->AppClientLead->save($this->request->data['AppClientLead'])) {
				$this->layout = 'empty';
				//$lastinsertId = $this->AppClientLead->getLastInsertId();
				
				$this->request->data['AppReminderLead']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppReminderLead']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppReminderLead']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				$this->request->data['AppReminderLead']['status'] = 1;
				//CakeLog::write('debug', 'AppReminderLead - '.print_r($this->request->data['AppReminderLead'], true) );
				$this->AppReminderLead->save($this->request->data['AppReminderLead']);
				
				//$pdfdataview = $this->request->data;
				
				$view1 = new View($this);
				$Custom1 = $view1->loadHelper('Number');
				$projectName = $Custom1->getProjectNameByProjectId($this->request->data['project_id']);
				
				
				$this->loadModel('Project');
				$builderIdByProjectId= $this->Project->find('first', array(
					'fields' => array(
						'user_id'
					),
					'conditions' => array(
						'project_id' => $this->request->data['project_id']
					)
				));
				
				$this->loadModel('Builder');
				$builderlogoById = $this->Builder->find('first', array(
					'fields' => array(
						'borg_logo'
					),
					'conditions' => array(
						'websiteuser_id' => $builderIdByProjectId['Project']['user_id']
					)
				));
				$builderlogo = $builderlogoById['Builder']['borg_logo'];
				if($builderlogoById['Builder']['borg_logo']!=''){
				$builderlogo = $builderlogoById['Builder']['borg_logo'];	
				}else{
					$builderlogo = 'logo.jpg';
				}
				
				
				$this->loadModel('Websiteuser');
				$builderNameById = $this->Websiteuser->find('first', array(
					'fields' => array(
						'userorgname'
					),
					'conditions' => array(
						'id' => $builderIdByProjectId['Project']['user_id']
					)
				));
				$buildername = $builderNameById['Websiteuser']['userorgname'];
				
				$this->set('pdfGenerateRequest', true);
				$this->set('projectId', $this->request->data['project_id']);
				$this->set('projectName', $projectName[0]['Project']['project_name']);
				$this->set('baseRate', $this->request->data['property_detail']['base_rate']);
				$this->set('propertyArea', $this->request->data['property_detail']['property_area']);
				$this->set('propertyOnFloor', $this->request->data['property_detail']['floor_no']);
				$this->set('baseCharges', $this->request->data['chart_data']['base_charges']);
				$this->set('additionalCharges', $this->request->data['chart_data']['additional_charges']);
				$this->set('gstTotal', $this->request->data['chart_data']['gst_total']);
				$this->set('stampRegTotal', $this->request->data['chart_data']['stamp_plus_registration']);
				$this->set('finalCalculatePrice', $this->request->data['final_calculated_price']);
				$this->set('visitorName', $this->request->data['AppClientLead']['name']);
				$this->set('builderlogo', $builderlogo);
				$this->set('buildername', $buildername);
				
				// For project 917 Start //
				if($this->request->data['project_id'] == 917){
					
					$this->loadModel('ProjectPlcDetail');
					$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = '".$this->request->data['project_id']."' AND plc_floor_no = '".$this->request->data['property_detail']['floor_no']."'");
				
					
					$bsprow1 = $this->request->data['property_detail']['base_rate'] * $this->request->data['property_detail']['property_area'];
					$gstInputCreditAmount = $this->request->data['gstInputCredit'];
					$gstInputCreditAmountCalculated = $bsprow1*$gstInputCreditAmount/100;
					$addGstAmount = $this->request->data['property_detail']['gst_on_base'];
					$addGstAmountCalculated = $bsprow1*$addGstAmount/100;
					$effectiveBSP = $bsprow1+$addGstAmountCalculated;
					$floor_no = $this->request->data['property_detail']['floor_no'];
					$floorCharges = $plc_price[0]['project_plc_details']['plc_floor_charge']*$this->request->data['property_detail']['property_area'];
					$newGST = $this->request->data['property_detail']['floor_no']*$this->request->data['property_detail']['property_area']*18;
					$totalPricenew = $bsprow1+$addGstAmountCalculated+$newGST;
					
					$this->set('bsprow1', $bsprow1);
					$this->set('gstInputCreditAmount', $gstInputCreditAmount);
					$this->set('gstInputCreditAmountCalculated', $gstInputCreditAmountCalculated);
					$this->set('addGstAmount', $addGstAmount);
					//$this->set('addGstAmountCalculated', $addGstAmountCalculated);
					$this->set('addGstAmountCalculated', $this->request->data['chart_data']['gst_total']);
					$this->set('effectiveBSP', $effectiveBSP);
					$this->set('floor_no', $floor_no);
					$this->set('floorCharges', $floorCharges);
					$this->set('newGST', $newGST);
					$this->set('totalPricenew', $this->request->data['final_calculated_price']);
				}
				// For project 917 End  //
				
				
				$this->loadModel('SalesUser');
				$usersDatas = $this->SalesUser->find('all', array(
					'conditions' => array(
						'id' => $this->request->data['user_id']
					)
				));
				$this->set('userName', $usersDatas[0]['SalesUser']['name']);
				$this->set('userMobile', $usersDatas[0]['SalesUser']['mobile']);
				
				
				/*$viewdata = $this->render('post_calculator_data_submit');
				
				$linkId = $lastinsertId.'_'.$this->request->data['project_id'];
				$workorderHTMLName = "workorder_html4pdf.html";
				$workorderPDFName = 'project-'.$linkId;
				$path = WWW_ROOT . "files/workorders/" . $workorderHTMLName;
				
				$file = new File($path, true);
				
				if ($file->write($viewdata)) {
					$this->_newWorkorderpdf($workorderHTMLName, $workorderPDFName);				
					$docketFileFullPath = _ROOT_PATH . 'vendors/html2ps/public_html/out/workorder-project-'.$linkId.'.pdf';
					$newFileFullPath = _ROOT_PATH . 'app/webroot/files/workorders/project-'.$linkId.'.pdf';
					$isPdf = true;
					copy($docketFileFullPath, $newFileFullPath);
						unlink($path);
						unlink($docketFileFullPath);
					
				}*/
				
				
				$this->loadModel('ProjectPricingPaymentPlan');
				$ProjectPricingPaymentPlan= $this->ProjectPricingPaymentPlan->find('all', array(
					'fields' => array(
						'plan_base_charge','plan_bc_amount','plan_total_charges','plan_tc_amount'
					),
					'conditions' => array(
						'project_id' => 701,
						'project_pricing_id' => 50
					)
				));
				$this->set('ProjectPricingPaymentPlan', $ProjectPricingPaymentPlan);
				//echo '<pre>';print_r($ProjectPricingPaymentPlan); die();
				
				//$this->layout = "empty";
				$linkId = $lastinsertId.'_'.$this->request->data['project_id'];	
				$this->set('linkid', $linkId);
				
				//$this->layout = "empty";			
			
				require_once(ROOT . DS . "vendors/pdfapi".DS."tcpdf.php");
				if($this->request->data['project_id'] == 917){
					$this->view = 'post_calculator_data_submit1';
				}else{
					$this->view = 'post_calculator_data_submit_testing';
				}
				
				
				//CakeLog::write('debug', 'pdf-debug'.print_r($this->request->data, true) );
				
					$pdf_link = 'http://fairpockets.com/files/workorders/project-'.$linkId.'.pdf';
					$resp = array("message"=>'Success',"code"=>1,"pdf_link"=>(string)$pdf_link,"data"=>null);
					
					CakeLog::write('debug', 'myArray-submit'.print_r($resp, true) );
					
					print json_encode($resp,JSON_UNESCAPED_SLASHES); //exit;
				//}
						
		//}
		
		
	} 
	
	public function getClientDataByUserId(){
	
            $this->autoRender = false;
			$user_id = $_POST['user_id'];
			//$this->loadModel('AppClientLead');
			CakeLog::write('debug', 'getClientDataByUserId'.print_r(json_encode($this->request->data), true));
			$clientData = $this->AppClientLead->find('all', array(
					'conditions' => array(
						'user_id' => $user_id
					),
					'order' => 'created DESC'
				));
				if(!empty($clientData)){
				$things = Set::extract('/AppClientLead/.', $clientData);		
				$message = "Success";
				$code = 1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);				
				}else{
					$message = "Failed";
				$code = -1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>null);
					
				}
				print json_encode($resp);
				
	
	}
	
	
	public function getClientDataByUserIdClientId(){
	CakeLog::write('debug', 'getClientDataByUserIdClientId'.print_r($this->request->data, true) );
            $this->autoRender = false;
			$user_id = $_POST['user_id'];
			$client_id = $_POST['client_id'];
			$this->loadModel('AppClientLeadsHistory');
			
			$clientData = $this->AppClientLeadsHistory->find('all', array(
					'conditions' => array(
						'user_id' => $user_id,
						'client_id' => $client_id
					),
					'order' => 'created DESC'
				));
				
				if(!empty($clientData)){
				$things = Set::extract('/AppClientLeadsHistory/.', $clientData);		
				$message = "Success";
				$code = 1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);				
				}else{
					$message = "Failed";
				$code = -1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>null);
					
				}
				print json_encode($resp);
	
	}
	
	public function getReminderDataByUserId(){
	
            $this->autoRender = false;
			$user_id = $_POST['user_id'];
			//$this->loadModel('AppClientLead');
			CakeLog::write('debug', 'getReminderDataByUserId'.print_r($this->request->data, true));
			$clientData = $this->AppReminderLead->find('all', array(
					'conditions' => array(
						'user_id' => $user_id
					)
				));
				
				if(!empty($clientData)){
				$things = Set::extract('/AppReminderLead/.', $clientData);		
				$message = "Success";
				$code = 1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);				
				}else{
					$message = "Failed";
				$code = -1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>null);
					
				}
				print json_encode($resp);
	
	}
	
	
	
	public function getCalculatorByProjectId(){
	//print $json = json_encode($a);//die();
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = $_POST['project_id'];
			$property_area = $_POST['property_area'];
			if(isset($_POST['properties_id'])&& $_POST['properties_id']!=''){
				$properties_id = $_POST['properties_id'];
			}
			//properties_id
			//$project_id = 903;
			//$property_area = 1045;
			//$project_id = 701;
			CakeLog::write('debug', 'getCalculatorByProjectId'.print_r(json_encode($this->request->data), true) );
			$this->loadModel('ProjectPricing');
			
			$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $project_id
					)
				));
				
			$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];
						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Total Power Backup Cost";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance Deposit";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							case "9":
								$project_charges[$j]['0'] = "Sinking Fund";
								break;
							case "12":
								$project_charges[$j]['0'] = "Swimming Pool PLC";
								break;
							case "13":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "14":
								$project_charges[$j]['0'] = "Hill View PLC";
								break;
							case "15":
								$project_charges[$j]['0'] = "Additional Car Parking";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}
						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];

					}
				}
						//echo '884'; echo '<pre>'; print_r($project_charges);
						//print json_encode($project_charges);
						//echo count($project_pricing_charge) / 4;
						
						
						$this->loadModel('Property');
						$getPropertyPowerBackup = $this->Property->find('all',
							array('fields'=> array(
									'power_backup_kva'		
								),
								  'conditions'=>array(
									'property_id' => $properties_id
								)
							)
						);
						if(isset($getPropertyPowerBackup) && $getPropertyPowerBackup != ''){
							$power_backup_kva_calue = $getPropertyPowerBackup[0]['Property']['power_backup_kva'];							
						}else{
							$power_backup_kva_calue = 1;
						}				
						
						
						
						
				for ($i = 0, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if ($project_charges[$i][1] == 1) {
						$project_charges_mand[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						if($project_charges_mand[$j]['charge_title'] == 'Power BackUp per KVA'){
							$project_charges_mand[$j]['charge_value'] = strval($project_charges[$i][2]*$power_backup_kva_calue);
						}else{
							$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						}
						$project_charges_mand[$j]['unit_type'] = $project_charges[$i][3];
					}
					if ($project_charges[$i][1] == 2) {
						$project_charges_optional[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_optional[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_optional[$j]['unit_type'] = $project_charges[$i][3];						
					}
				}
				
				//$project_charges_mand_ok = json_encode(array_values($project_charges_mand));
				//$project_charges_optional_ok = json_encode(array_values($project_charges_optional));
				//print_r($project_charges_mand);
				//print_r($project_charges_optional);die();
				
				
				
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							case "9":
								$additon_charges[$j]['0'] = "Sinking Fund";
								break;
							case "10":
								$additon_charges[$j]['0'] = "Security  & 1 time connection charges";
								break;
							case "11":
								$additon_charges[$j]['0'] = "Water & sewer connection charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Internal development Charges";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				$additon_charges_mand = array();
				$additon_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if ($additon_charges[$i][1] == 1) {
						$additon_charges_mand[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_mand[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_mand[$j]['unit_type'] = $additon_charges[$i][3];
					}
					if ($additon_charges[$i][1] == 2) {
						$additon_charges_optional[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_optional[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_optional[$j]['unit_type'] = $additon_charges[$i][3];						
					}
				}
				
				
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				$other_charges_mand = array();
				$other_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if ($other_charges[$i][1] == 1) {
						$other_charges_mand[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_mand[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_mand[$j]['unit_type'] = $other_charges[$i][3];
					}
					if ($other_charges[$i][1] == 2) {
						$other_charges_optional[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_optional[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_optional[$j]['unit_type'] = $other_charges[$i][3];						
					}
				}
				//print_r($project_charges_mand);
				//print_r($project_charges_optional_ok);
				$gaProjectCharges = array_merge($additon_charges_mand, $project_charges_mand, $other_charges_mand);
				//echo '<pre>'; print_r($mandProjectCharges); echo '----884----';
				$gaProjectChargesLen = count($mandProjectCharges);
				$optionalProjectCharges = array_merge($additon_charges_optional, $project_charges_optional, $other_charges_optional);
				//echo '<pre>'; print_r($optionalProjectCharges);die();
				
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				//$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				//$gaProjectChargesLen = count($mandProjectCharges);
				//print 
				//echo '<pre>'; print_r($gaProjectCharges);//die();
				//print json_encode($gaProjectCharges); die();
				
				
				
				
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
						
						if(isset($basePriceArr['unit_bsp1']) && $basePriceArr['unit_bsp1'] != ''){
							$gBaseRateUnit = $basePriceArr['unit_bsp1'];
						}else{
							$gBaseRateUnit = 1;
						}
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}
				
				
				
				
				// Project Pricing - Stamp Duty
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				
				
				
				
				
				//echo $gBaseRate.'--- reg--'.$Registration; die();
				
				
				
				for ($row = 0; $row < count($gaProjectCharges); $row++) {					
					$MandatoryChargeAmt = $gaProjectCharges[$row]['charge_value'];
					if (1 == $gaProjectCharges[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt * $property_area );
						//echo $ProjectChargesMandatoryTotal.',';						
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt );
					}           
				
				}//die();
				//echo $ProjectChargesMandatoryTotal; die();
				//$ProjectChargesMandatoryTotal = 795100;
				//echo $final_price;  die();
				
				
						
					
				     //echo $PropertyPossessionChargeTotal;die();
				
				if(!empty($gaRegistration)){
					if (2 == $gaRegistration[0][0]) {
						// Percentage
						$Registration = $registration_amount = $gaRegistration[0][1] . " %";
						$RegistrationCalcutedAmount = (($property_area*$gBaseRate + $ProjectChargesMandatoryTotal ) * ( $gaRegistration[0][1] / 100 ));
					} else {
						// Flat Charges
						$Registration = "Rs " . $gaRegistration[0][1];
						$RegistrationCalcutedAmount = "Rs " . $gaRegistration[0][1];
					}
				}
				
				if(!empty($gaStampDuty)){
					if (2 == $gaStampDuty[0][0]) {
						// Percentage
						$gaStampDutyok = $gaStampDuty[0][1] . " %";
					} else {
						// Flat Charges
						$gaStampDutyok =  "Rs " . $gaStampDuty[0][1];
					}
				}
				
				
				
				$selected_floor = 0;	
					$this->loadModel('PropertyPossessionCharge');
					$PropertyPossessionChargeTotal = 0;
					//$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge FROM `project_plc_details` WHERE  project_id = ".$projectid." AND plc_floor_no = ".$bspoptionid."");
					$PropertyPossessionCharge = $this->PropertyPossessionCharge->query("SELECT possession_amount FROM `property_possession_charges` WHERE  property_id = '".$properties_id."' AND possession_from_floor = '".$selected_floor."'");
					//echo '<pre>'; print_r($PropertyPossessionCharge); die();
					if(isset($PropertyPossessionCharge) && $PropertyPossessionCharge != ''){
						$PropertyPossessionChargeTotal = $PropertyPossessionCharge[0]['property_possession_charges']['possession_amount'];
					}
					if($PropertyPossessionChargeTotal==''){
						$PropertyPossessionChargeTotal = 0;
					}
					
				
				$this->loadModel('ProjectSpaceDetail');
				$floorno = 0;
				//$PropertyPossessionChargeArray = $this->PropertyPossessionCharge->query("SELECT * FROM `property_possession_charges` WHERE '".$floorno."' BETWEEN `possession_from_floor` AND `possession_to_floor` AND property_id = '".$prop_id."'");			
				$propertyLawnArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = 0 AND space_mode = 2");			
				if(isset($propertyLawnArray) && $propertyLawnArray != ''){
					$propertyLawnValueTotal = $propertyLawnArray[0]['project_space_details']['floor_space_area']*$propertyLawnArray[0]['project_space_details']['floor_space_amount'];
				}else{
					$propertyLawnValueTotal = 0;	
				}
				$propertyTerraceArray = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = '".$properties_id ."' AND space_from_floor = 0 AND space_mode = 1");	
				if(isset($propertyTerraceArray) && $propertyTerraceArray != ''){
					$propertyTerraceValueTotal = $propertyTerraceArray[0]['project_space_details']['floor_space_area']*$propertyTerraceArray[0]['project_space_details']['floor_space_amount'];		
				}else{
					$propertyTerraceValueTotal = 0;
				}
				
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,
												"gst_on_base"=>$gServiceTaxOnBSP,
												"gst_on_others"=>$gServiceTaxOnOTH,
												"registration_charges"=>$Registration,
												"stamp_duty"=>$gaStampDutyok,
												"property_possession_charge_total"=>$PropertyPossessionChargeTotal,
												"property_terrace_value_total"=>$propertyTerraceValueTotal,
												"property_lawn_value_total"=>$propertyLawnValueTotal,
												"base_rate_unit"=>$gBaseRateUnit);
												
												
				$this->loadModel('ProjectsAndroidApp');
				$getProjectsDiscount = $this->ProjectsAndroidApp->find('all',
					array('fields'=> array(
							'proj_discount',
							'proj_discount_unit'			
						),
						  'conditions'=>array(
							'sales_or_broker_employee_id' => $_POST['user_id'],
							'project_id' => $project_id
						)
					)
				);
				if($getProjectsDiscount !=''){
					$disc_amount = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount'];
					$disc_unit  = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount_unit'];
					$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
					$showDiscount = 'true';
				}else{
					$showDiscount = 'false';
				}
				
				
				/*if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = ($property_area*$gBaseRate) - $disc_amount;					
				}
				if($disc_unit == 2){
						$disc_price_cal = ($property_area*$gBaseRate)*$disc_amount/100;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $property_area*$disc_amount;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}*/
				
				
				
				/*if(isset($base_price_new) && $base_price_new!=''){
					$base_price = $base_price_new;
					$showDiscount = 'true';
				}else{
					$base_price = $property_area*$gBaseRate;
					$showDiscount = 'false';
				}*/
				
				
				/*if(isset($base_price_new) && $base_price_new!=''){					
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;						
						$base_price = $base_price_new - $gstInputCreditAmount;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showInputCredit = 'true';
					}
					else{												
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $base_price_new;
					$showDiscount = 'true';					
				}else{
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;
						$base_price = $base_price_new - $gstInputCreditAmount;
						$showInputCredit = 'true';
					}
					else{						
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $property_area*$gBaseRate;
					$showDiscount = 'false';
				}*/
				
				if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;						
						//$base_price = ($property_area*$gBaseRate) - $gstInputCreditAmount;
						
						if($gBaseRateUnit == 1){
							$base_price = ($property_area*$gBaseRate) - $gstInputCreditAmount;	
						}
						if($gBaseRateUnit == 2){
							$base_price = ($gBaseRate) - $gstInputCreditAmount;	
						}
						
						$showInputCredit = 'true';
					}
					else{
						$base_price = $property_area*$gBaseRate;
						$gstInputCredit = null;
						$showInputCredit = 'false';						
					}
				
				$this->loadModel('ProjectPlcDetail');
				$bspoptionid = 0;
				$plc_price = $this->ProjectPlcDetail->query("SELECT plc_floor_charge,plc_charges_unit,plc_floor_mode FROM `project_plc_details` WHERE  project_id = ".$project_id." AND plc_floor_no = ".$bspoptionid."");
			
				
				if(isset($plc_price[0]['project_plc_details']['plc_floor_charge']) && $plc_price[0]['project_plc_details']['plc_floor_charge'] !=''){
						$plcCreditInput = $plc_price[0]['project_plc_details']['plc_floor_charge'];
						
						if($plc_price[0]['project_plc_details']['plc_charges_unit'] == 1){
							$plcAmountArea = $plcCreditInput*$property_area;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						if($plc_price[0]['project_plc_details']['plc_charges_unit'] == 2){
							$plcAmountArea = $plcCreditInput;
							$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						}
						
						//$plcAmountArea = $plcCreditInput*$property_area;
						//$plcAmountGst = ($plcAmountArea*$gServiceTaxOnOTH)/100;
						$plcAmountTotal = $plcAmountArea+$plcAmountGst;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showPlcCredit = 'true';
					}
					else{
						$plcAmountArea = 0;
						$plcCreditInput = 0;
						$showPlcCredit = 'false';						
					}
					//$plcAmountArea = 0;
					//$plcAmountTotal = 0;
					//$base_price = $base_price_new;
					//$base_price = $property_area*$gBaseRate;
					//$Registration = 0;
					/*$space_base_price = 0;
					$this->loadModel('ProjectSpaceDetail');
					$PropSpaceDetail = $this->ProjectSpaceDetail->query("SELECT * FROM `project_space_details` WHERE project_id = ".$propertydetails['Property']['property_id']." and space_from_floor = 1");			
					if(isset($PropSpaceDetail) && $PropSpaceDetail !=''){
						$PropSpaceTotal = $ProjectSpaceDetail[0]['project_space_details']['floor_space_area']*$ProjectSpaceDetail[0]['project_space_details']['floor_space_amount'];
						$space_base_price = ($PropSpaceTotal*$gServiceTaxOnBSP)/100;
					}else{
						$PropSpaceTotal = 0;
						$space_base_price = 0;
					}*/
				
					// Code for Additional Space  start//
					
					//echo $propertyLawnValueTotal.'-';
					//echo $propertyTerraceValueTotal;
					//die();
					// Code for Additional Space end//
				
				
					//echo $base_price.'-';
					//echo 'base-price: '.$propertyLawnValueTotal.',';
					//echo $base_price; die();
				$base_price1 = $base_price+$propertyLawnValueTotal+$propertyTerraceValueTotal;
				//echo $base_price1; die();
				$gst_base_price = ($base_price1*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price1 + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100+$plcAmountTotal;
				//$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				$final_price = $PropertyPossessionChargeTotal + $total_gst_others_base_price + ($base_price1+$ProjectChargesMandatoryTotal + $plcAmountArea + $PropertyPossessionChargeTotal)*$Registration/100;
				
				//echo $final_price;die();
				
				
				
				if(!isset($project_id) || empty($project_id)){
                $resp = array("message"=>"Failed","code"=>-1,"data"=>null);
					} else {
						
						//$gstTotal = $base_price1*($gServiceTaxOnBSP/100) + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100;
					if($ProjectChargesMandatoryTotal == ''){
						$ProjectChargesMandatoryTotal = 0;
					}	
				$chartData = array('base_charges'=>$base_price1,
								   //'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'gst_total'=> $base_price1*($gServiceTaxOnBSP/100) + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   //'gst_total'=> ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationCalcutedAmount);
								   
				$this->loadModel('ProjectBspDetail');
				$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$project_id ."");			
				//echo '<pre>';print_r($bsp_limit);
				//$bsp_limit[0][0]['startloop'];
				//$bsp_limit[0][0]['endloop'];
				$floor_choice = array('startloop'=>$bsp_limit[0][0]['startloop'],
								   'endloop'=> $bsp_limit[0][0]['endloop']);
							   
                $resp = array("message"=>$message,
								"code"=>$code,
								"property_detail"=>$property_detail_array,
								"mandatory_charges"=>$gaProjectCharges,
								"optional_charges"=>$optionalProjectCharges,
								"showDiscount"=>$showDiscount,
								"discount"=>$project_disc_array,								
								"showInputCredit"=>$showInputCredit,
								"gstInputCredit"=>$gstInputCredit,
								"floor_choice"=>$floor_choice,
								"chart_data"=>$chartData,
								"final_calculated_price"=>$final_price);
								//echo '<pre>'; print_r($resp);die();	
				}
				CakeLog::write('debug', 'getCalculatorByProjectId-response884'.print_r($resp, true) );
            
				print json_encode($resp); exit;
				
				
				//die();
				
				
				
				

				/*
				  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
				  pr($gaProjectCharges);
				  echo "<br>"."gaStampDuty"."<br>";
				  pr($gaStampDuty);
				  echo "<br>"."gaRegistration"."<br>";
				  pr($gaRegistration);
				  echo "<br>"."gaFloorPLC"."<br>";
				  pr($gaFloorPLC);
				 */
				 
				 	


				$gaMandatoryCharges = '';
				$gaOptionalCharges = '';
				$gaOptionalCount = 0;
				$ProjectCharges = 0;
				$gaFloorPLCCharges = 0;

				if(!empty($gaFloorPLC))
				{
				if ($gaFloorPLC[0][1] > 0) {
					$gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

					if ($gaFloorPLC[0][0] == 2) {
						// BSP w.r.t Ground floor
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					} else {
						// BSP w.r.t TOP floor;
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					}

					if ($gaFloorPLC[0][2] == 2) {
						// sub option
						$gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
					}

					$gaMandatoryCharges = $gaMandatoryCharges . "
									<div class='col-sm-12'>
										<label for='' class='checkbox-custom-label'>
								";
					$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
							'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC1[0][1] . "&nbsp;/ sq ft</span>
									";
					$gaMandatoryCharges = $gaMandatoryCharges . "
										</label>  
									</div>
							";
				}
				}
				$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
				$gOfferPrice = $gOfferPrice + $BaseCharge;
				$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

				//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
				//echo "<br><br>BaseCharge - ".$BaseCharge;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				// Project Charges
				$ProjectCharges = 0;

				echo '<pre>'; print_r($gaProjectCharges);
				
				
				

				for ($row = 0; $row < $gaProjectChargesLen; $row++) {
					//echo "<br><br>".$gaMandatoryCharges;
					if (1 == $gaProjectCharges[$row][1]) {
						// Mandatory to include the charges

						$gaMandatoryCharges = $gaMandatoryCharges . "
										<div class='col-sm-12'>
											<label for='' class='checkbox-custom-label'>
								";

						//Calculate Charges

						$MandatoryChargeAmt = $gaProjectCharges[$row][2];
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
									";
						}

						$gaMandatoryCharges = $gaMandatoryCharges . "
											</label>  
										</div>
								";

						echo "<br><br>	ProjectCharges - ".$ProjectCharges;
					} else {
						// Optional to include the charges
						$gaOptionalCount = $gaOptionalCount + 1;
						$tempid = "optional" . $gaOptionalCount;

						$gaOptionalCharges = $gaOptionalCharges . "
										<div class='col-sm-12'>
											
								";

						//Calculate Charges
						$OptionalChargeAmt = $gaProjectCharges[$row][2];
						$tempChargeAmt = 0;
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

							// Display Charge in Optional Section
							$gaOptionalCharges = $gaOptionalCharges . "
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$tempChargeAmt = $OptionalChargeAmt;

							// Display Charge in Mandatory Section
							$gaOptionalCharges = $gaOptionalCharges . "
									
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
									";
						}

						$gaOptionalCharges = $gaOptionalCharges . "
											
											</label>  
										</div>
								";
					}

					//echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
				}
				//echo '884'.'---';
				//echo $gaOptionalCharges;
				//echo '-1884'.'---';
				//echo '884'. $gaMandatoryCharges;

				$gProjectCharges_a = $ProjectCharges;
					echo '884'. $gProjectCharges_a;die();
				$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
				$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

				//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
				//echo "<br><br>gServiceTax - ".$gServiceTax;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				//Stamp Duty
				$StampDuty = 0;
				if(!empty($gaStampDuty)){
				if (2 == $gaStampDuty[0][0]) {
					// Percentage
					$StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
				} else {
					// Flat Charges
					$StampDuty = $gaStampDuty[0][1];
				}
				}
				else{
				$StampDuty = '';
				}
				$gOfferPrice = $gOfferPrice + $StampDuty;
				
				$Registration = 0;
				if(!empty($gaRegistration)){
				if (2 == $gaRegistration[0][0]) {
					// Percentage
					$Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
				} else {
					// Flat Charges
					$Registration = $gaRegistration[0][1];
				}
				}
				else{
				$Registration = '';
				}
				$gOfferPrice = $gOfferPrice + $Registration;
				
				echo $gaMandatoryCharges;
			
			
			
			
			
			
			
			
			
			
            $response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("status"=>"Project Id not provided","status_code"=>"0","result"=>array());
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }
	
	public function getCalculatorByProjectIdTesting(){

				//print $json = json_encode($a);//die();
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = 903;
			$property_area = 1045;
			//$project_id = 903;
			//$property_area = 1045;
			//$project_id = 701;
			CakeLog::write('debug', 'caldata-1'.print_r($this->request->data, true) );
			$this->loadModel('ProjectPricing');
			
			$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $project_id
					)
				));
				
			$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];
						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}
						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];

					}
				}
						//echo '884'; echo '<pre>'; print_r($project_charges);
						//print json_encode($project_charges);
						//echo count($project_pricing_charge) / 4;
				for ($i = 0, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if ($project_charges[$i][1] == 1) {
						$project_charges_mand[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_mand[$j]['unit_type'] = $project_charges[$i][3];
					}
					if ($project_charges[$i][1] == 2) {
						$project_charges_optional[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_optional[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_optional[$j]['unit_type'] = $project_charges[$i][3];						
					}
				}
				
				//$project_charges_mand_ok = json_encode(array_values($project_charges_mand));
				//$project_charges_optional_ok = json_encode(array_values($project_charges_optional));
				//print_r($project_charges_mand);
				//print_r($project_charges_optional);die();
				
				
				
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Sinking Fund";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				$additon_charges_mand = array();
				$additon_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if ($additon_charges[$i][1] == 1) {
						$additon_charges_mand[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_mand[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_mand[$j]['unit_type'] = $additon_charges[$i][3];
					}
					if ($additon_charges[$i][1] == 2) {
						$additon_charges_optional[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_optional[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_optional[$j]['unit_type'] = $additon_charges[$i][3];						
					}
				}
				
				
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				$other_charges_mand = array();
				$other_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if ($other_charges[$i][1] == 1) {
						$other_charges_mand[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_mand[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_mand[$j]['unit_type'] = $other_charges[$i][3];
					}
					if ($other_charges[$i][1] == 2) {
						$other_charges_optional[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_optional[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_optional[$j]['unit_type'] = $other_charges[$i][3];						
					}
				}
				//print_r($project_charges_mand);
				//print_r($project_charges_optional_ok);
				$gaProjectCharges = array_merge($additon_charges_mand, $project_charges_mand, $other_charges_mand);
				//echo '<pre>'; print_r($mandProjectCharges); echo '----884----';
				$gaProjectChargesLen = count($mandProjectCharges);
				$optionalProjectCharges = array_merge($additon_charges_optional, $project_charges_optional, $other_charges_optional);
				//echo '<pre>'; print_r($optionalProjectCharges);die();
				
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				//$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				//$gaProjectChargesLen = count($mandProjectCharges);
				//print 
				//echo '<pre>'; print_r($gaProjectCharges);//die();
				//print json_encode($gaProjectCharges); die();
				
				
				
				
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}
				
				
				
				
				// Project Pricing - Stamp Duty
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				
				
				
				
				
				//echo $gBaseRate.'--- reg--'.$Registration; die();
				
				
				
				for ($row = 0; $row < count($gaProjectCharges); $row++) {					
					$MandatoryChargeAmt = $gaProjectCharges[$row]['charge_value'];
					if (1 == $gaProjectCharges[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt );
					}           
				
				}
				
				
				//echo $final_price;  die();
				
				if(!empty($gaRegistration)){
					if (2 == $gaRegistration[0][0]) {
						// Percentage
						$Registration = $registration_amount = $gaRegistration[0][1] . " %";
						$RegistrationCalcutedAmount = (($property_area*$gBaseRate + $ProjectChargesMandatoryTotal ) * ( $gaRegistration[0][1] / 100 ));
					} else {
						// Flat Charges
						$Registration = "Rs " . $gaRegistration[0][1];
						$RegistrationCalcutedAmount = "Rs " . $gaRegistration[0][1];
					}
				}
				
				if(!empty($gaStampDuty)){
					if (2 == $gaStampDuty[0][0]) {
						// Percentage
						$gaStampDutyok = $gaStampDuty[0][1] . " %";
					} else {
						// Flat Charges
						$gaStampDutyok =  "Rs " . $gaStampDuty[0][1];
					}
				}
				
				
				
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,
												"gst_on_base"=>$gServiceTaxOnBSP,
												"gst_on_others"=>$gServiceTaxOnOTH,
												"registration_charges"=>$Registration,
												"stamp_duty"=>$gaStampDutyok);
												
				
				echo 'Base area -'.$gBaseRate.'<br/>';
				echo 'property_area -'.$property_area.'<br/>';
				//echo 'Base area -'.$gBaseRate.'<br/>';
				
				$this->loadModel('ProjectsAndroidApp');
				$getProjectsDiscount = $this->ProjectsAndroidApp->find('all',
					array('fields'=> array(
							'proj_discount',
							'proj_discount_unit'			
						),
						  'conditions'=>array(
							'sales_or_broker_employee_id' => 2,
							'project_id' => $project_id
						)
					)
				);
				$disc_amount = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount'];
				$disc_unit  = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount_unit'];
				$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
				
				
				if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = ($property_area*$gBaseRate) - $disc_amount;					
				}
				if($disc_unit == 2){
						$disc_price_cal = ($property_area*$gBaseRate)*$disc_amount/100;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $property_area*$disc_amount;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				echo 'base_price_new -'.$base_price_new.'<br/>';
				
				
				if(isset($base_price_new) && $base_price_new!=''){					
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;						
						$base_price = $base_price_new - $gstInputCreditAmount;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showInputCredit = 'true';
					}
					else{												
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $base_price_new;
					$showDiscount = 'true';					
				}else{
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;
						$base_price = $base_price_new - $gstInputCreditAmount;
						$showInputCredit = 'true';
					}
					else{						
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $property_area*$gBaseRate;
					$showDiscount = 'false';
				}
				
				//echo '<pre>'; print_r($project_pricing); die();
				
				
				//die();
				
				
				//$base_price = $base_price_new;
				//$base_price = $property_area*$gBaseRate;
				$gst_base_price = ($base_price*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100;
				$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				if(!isset($project_id) || empty($project_id)){
                $resp = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
				$chartData = array('base_charges'=>$base_price,
								   //'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'gst_total'=> ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationCalcutedAmount);
								   
				$this->loadModel('ProjectBspDetail');
				$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$project_id ."");			
				//echo '<pre>';print_r($bsp_limit);
				//$bsp_limit[0][0]['startloop'];
				//$bsp_limit[0][0]['endloop'];
				$floor_choice = array('startloop'=>$bsp_limit[0][0]['startloop'],
								   'endloop'=> $bsp_limit[0][0]['endloop']);
								   
                $resp = array("message"=>$message,
								"code"=>$code,
								"property_detail"=>$property_detail_array,
								"mandatory_charges"=>$gaProjectCharges,
								"optional_charges"=>$optionalProjectCharges,
								"showDiscount"=>$showDiscount,
								"discount"=>$project_disc_array,
								"showInputCredit"=>$showInputCredit,
								"gstInputCredit"=>$gstInputCredit,
								"floor_choice"=>$floor_choice,
								"chart_data"=>$chartData,
								"final_calculated_price"=>$final_price);
				}
				CakeLog::write('debug', 'getCalculatorByProjectId-response'.print_r($resp, true) );
            
				print json_encode($resp); exit;
            
        }
	
	
	public function getUnitsByProjectId(){
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			CakeLog::write('debug', 'getUnitsByProjectId'.print_r(json_encode($this->request->data), true) );
			$project_id = $_POST['project_id'];
            //$response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }else{
				$response_arr = array(
                        "message"=>"Failed",
                        "code"=>-1,
                        "data"=>null
                    );
				}
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }

    /* get state for add property */
	
	public function validateMobile(){
		header('Content-Type: application/json');
		//Configure::read('debug', 2);
		$this->loadModel('SalesUser');
		//$this->loadModel('OtpApp');
		$this->layout = false;
		$this->autoRender = false;
		//echo $_GET['mobile'];
		//print_r($_REQUEST);
		//$requestMobile = $_REQUEST['mobile'];
		
		CakeLog::write('debug', 'validateMobile'.print_r($this->request->data, true));
		
		//echo '<pre>'; print_r($this->SalesUser->find('all'));
		if(isset($_POST['mobile']) && $_POST['mobile'] !=''){
			$requestMobile = $_POST['mobile'];
			//echo $requestMobile; die();
			if($this->SalesUser->hasAny(array("mobile"=>$requestMobile))){
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'mobile' => $requestMobile
					)
				));
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				
				
				
				$randNo = rand(1000, 9999);
				$mobileNo = $requestMobile ;
				$milliseconds = round(microtime(true) * 1000) + (10 * 60 * 1000);
				$this->OtpApp->save([
					'OtpApp' => [ 'otp' => $randNo, 'expiry' => $milliseconds, 'mobile' => $mobileNo ]
				]);
				if($this->OtpApp->getAffectedRows() == 1) {
					$this->sendSMS($mobileNo, $randNo);            
				}  else {
					echo json_encode(['message' => 'Invalid request!', 'type' => 'error']);
				}
					//die;*/
					
					
				if($usersData[0]['SalesUser']['user_type'] == 'Sales'){
					$this->loadModel('BuilderSalesShareInventory');
					$getInvArray = $this->BuilderSalesShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('sales_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = $usersData[0]['SalesUser']['builder_id'];
				}
				if($usersData[0]['SalesUser']['user_type'] == 'Broker'){
										
					$this->loadModel('BuilderBrokerShareInventory');
					$getInvArray = $this->BuilderBrokerShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('broker_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = 'null';
				}
				if($usersData[0]['SalesUser']['user_type'] == 'Broker_sales'){
										
					$this->loadModel('BrokerSalesShareInventory');
					$getInvArray = $this->BrokerSalesShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('sales_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = 'null';
				}
				//echo $UserInvStatus;
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],
				"user_id"=>$usersData[0]['SalesUser']['id'],"builder_id"=>$builderId,
				"UserInvStatus"=>$UserInvStatus,"UserCalStatus"=>"true");
				
					
				$resp = array("message"=>$message,"code"=>$code,"otp"=>$randNo,"data"=>$data);
				print json_encode($resp); exit;
			}else{			
				$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;			
			}			
		}
		//echo $requestEmail;
		
		if(isset($_POST['email'])&& $_POST['email']!=''){
			$requestEmail = $_POST['email'];
			$requestPassword = $_POST['password'];
			//echo $requestEmail; die();
			if($this->SalesUser->hasAny(array("email"=>$requestEmail,"password"=>$requestPassword))){
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'email' => $requestEmail,
						'password' => $requestPassword
					)
				));
				$message = "Success";
				$code = 1;
				
				
				if($usersData[0]['SalesUser']['user_type'] == 'Sales'){
					$this->loadModel('BuilderSalesShareInventory');
					$getInvArray = $this->BuilderSalesShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('sales_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = $usersData[0]['SalesUser']['builder_id'];
				}
				if($usersData[0]['SalesUser']['user_type'] == 'Broker'){
										
					$this->loadModel('BuilderBrokerShareInventory');
					$getInvArray = $this->BuilderBrokerShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('broker_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = 'null';
				}
				if($usersData[0]['SalesUser']['user_type'] == 'Broker_sales'){
										
					$this->loadModel('BrokerSalesShareInventory');
					$getInvArray = $this->BrokerSalesShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('sales_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = 'null';
				}
				//echo $UserInvStatus;
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],
				"user_id"=>$usersData[0]['SalesUser']['id'],"builder_id"=>$builderId,
				"UserInvStatus"=>$UserInvStatus,"UserCalStatus"=>"true");
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;
			}else{			
				$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;			
			}			
		}
		
		
		
	}
	
	public function verifyOtp() {
        header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->loadModel('SalesUser');
		$this->request->data['mobile'] = $_POST['mobile'];
		$this->request->data['otp'] = $_POST['otp'];
		
		CakeLog::write('debug', 'verifyOtp'.print_r($this->request->data, true));
		
        $otp_app = $this->OtpApp->find('first', [
            'conditions' => $this->request->data
        ]);
				
       // print_r($otp); die();
        if(!empty($otp_app)) {
            $milliseconds = round(microtime(true) * 1000);
            if($otp_app['OtpApp']['expiry']) {
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'mobile' => $_POST['mobile']
					)
				));
				
				if($usersData[0]['SalesUser']['user_type'] == 'Sales'){
					$this->loadModel('BuilderSalesShareInventory');
					$getInvArray = $this->BuilderSalesShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('sales_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = $usersData[0]['SalesUser']['builder_id'];
				}
				if($usersData[0]['SalesUser']['user_type'] == 'Broker'){
										
					$this->loadModel('BuilderBrokerShareInventory');
					$getInvArray = $this->BuilderBrokerShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('broker_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = 'null';
				}
				if($usersData[0]['SalesUser']['user_type'] == 'Broker_sales'){
										
					$this->loadModel('BrokerSalesShareInventory');
					$getInvArray = $this->BrokerSalesShareInventory->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('sales_id' => $usersData[0]['SalesUser']['id'])));	
					//echo '<pre>'; print_r($getInvArray);die();
					if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
					
					$builderId = 'null';
				}
				
				
				$message = "Success";
				$code = 1;
				
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],
				"user_id"=>$usersData[0]['SalesUser']['id'],"builder_id"=>$builderId,
				"UserInvStatus"=>$UserInvStatus,"UserCalStatus"=>"true");
				
				/*$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],
				"user_id"=>$usersData[0]['SalesUser']['id']);*/
				
                print json_encode([ 'message' => 'success','code' => 1, 'data' => $data ]);
            } else {
                print json_encode([ 'type' => 'failure' ]);
            }
        } else {
            print json_encode([ 'type' => 'failure' ]);
        }
        exit;
    }
	
	public function getProjectsByUserId() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		CakeLog::write('debug', 'getProjectsByUserId'.print_r(json_encode($this->request->data), true));
		//die();
		$this->layout = false;
		$this->autoRender = false;
		
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('Project');
		//$_POST['user_id'] = 2;
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
			if(isset($_POST['user_id'])&& $_POST['user_id']==161 || $_POST['user_id'] ==163 || $_POST['user_id'] ==145){
			
			$getProjects = $this->Project->find('all', array(
			'fields'=> array( 
						'DISTINCT project_id',
						'project_name'			
					),
					  'conditions'=>array(
						'approved' => 1
					),
			'joins' => array(array('table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'INNER',
                        'conditions' => array('Property.project_id = Project.project_id',
						'Project.user_id' => $_POST['builder_id'])))));
			
			
			$things = Set::extract('/Project/.', $getProjects);			
		}else{			
			$getProjects = $this->ProjectsAndroidApp->find('all',
				array('fields'=> array(
						'project_id',
						'project_name'			
					),
					  'conditions'=>array(
						'sales_or_broker_employee_id' => $_POST['user_id']
					)
				)
			);
			$things = Set::extract('/ProjectsAndroidApp/.', $getProjects);
		}
		
		//echo '<pre>'; print_r($things);		
		//die();
		//print json_encode($things, true);
		
		$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);
				
				print json_encode($resp);
		//print_r($getProjects);exit;
		}
		
	}
	
	public function getBuildersByUserId() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		CakeLog::write('debug', 'getBuildersByUserId'.print_r(json_encode($this->request->data), true));
		$this->loadModel('Websiteuser');
		$this->loadModel('BrokerSales');
		//$_POST['user_id'] = 39;
		//echo $_POST['user_id']; die();	
		if(isset($_POST['user_id']) && $_POST['user_id']!=''){	
//echo $_POST['user_id']; die();				
			if($_POST['user_id'] == 161 || $_POST['user_id'] == 163){
				$getBuildersDropdown = $this->Websiteuser->find('all',
					array('fields'=> array(
							'id AS builder_id',
							'userorgname AS builder_name'			
						),
						  'conditions'=>array(
							'id' => array(45,122,392,141,122,402,404,412,419,424,436,459,596,447,598)
						)
					)
				);
				if(!empty($getBuildersDropdown)){
						$things = Set::extract('/Websiteuser/.', $getBuildersDropdown);			
						$message = "Success";
						$code = 1;					
						$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
						print json_encode($resp);
						
					}else{
						$message = "failed";
						$code = -1;
						$data = null;
						$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
						print json_encode($resp); exit;
						
					}
			}elseif($_POST['user_id'] == 145 || $_POST['user_id'] == 206){
				$getBuildersDropdown = $this->Websiteuser->find('all',
					array('fields'=> array(
							'id AS builder_id',
							'userorgname AS builder_name'			
						),
						  'conditions'=>array(
							'id' => array(45,122,141)
						)
					)
				);
				if(!empty($getBuildersDropdown)){
						$things = Set::extract('/Websiteuser/.', $getBuildersDropdown);			
						$message = "Success";
						$code = 1;					
						$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
						print json_encode($resp);
						
					}else{
						$message = "failed";
						$code = -1;
						$data = null;
						$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
						print json_encode($resp); exit;
						
					}
			}else{
				$userid = $_POST['user_id'];
				$user_type = $_POST['user_type'];
				//echo $userid; echo $user_type;die();
				if($user_type == 'Broker'){
					$buildersListArray= $this->Websiteuser->find('list', array(
					'fields'=> array('id'),
					'joins' => array(array('table' => 'builder_brokers',
							'alias' => 'BuilderBroker',
							'type' => 'INNER',
							'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $userid)))));					
					$comma_separated = implode(",", $buildersListArray);					
					$arr=explode(",",$comma_separated);								
					$getBuildersDropdown = $this->Websiteuser->find('all',
						array('fields'=> array(
								'id AS builder_id',
								'userorgname AS builder_name'			
							),
							  'conditions'=>array(
								'id' => $arr
							)
						)
					);
					
					if(!empty($getBuildersDropdown)){
						$things = Set::extract('/Websiteuser/.', $getBuildersDropdown);			
						$message = "Success";
						$code = 1;					
						$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
						print json_encode($resp);
						
					}else{
						$message = "failed";
						$code = -1;
						$data = null;
						$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
						print json_encode($resp); exit;
						
					}
				}				
				elseif($user_type == 'Broker_sales'){					
					$getbrokerIdBySalesId = $this->BrokerSales->find('all',array(
							'fields'=> array('broker_id'),
							'conditions'=> array(
								'sales_id'=>$userid
								)
									)
										);								
					$buildersListArray= $this->Websiteuser->find('list', array(
					'fields'=> array('id'),
					'joins' => array(array('table' => 'builder_brokers',
							'alias' => 'BuilderBroker',
							'type' => 'INNER',
							'conditions' => array('BuilderBroker.builder_id = Websiteuser.id', 'BuilderBroker.broker_id' => $getbrokerIdBySalesId[0]['BrokerSales']['broker_id'])))));
					
					$comma_separated = implode(",", $buildersListArray);
					$arr=explode(",",$comma_separated);			
					$getBuildersDropdown = $this->Websiteuser->find('all',
						array('fields'=> array(
								'id AS builder_id',
								'userorgname AS builder_name'			
							),
							  'conditions'=>array(
								'id' => $arr
							)
						)
					);
					
					if(!empty($getBuildersDropdown)){
						$things = Set::extract('/Websiteuser/.', $getBuildersDropdown);			
						$message = "Success";
						$code = 1;					
						$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
						print json_encode($resp);
						
					}else{
						$message = "failed";
						$code = -1;
						$data = null;
						$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
						print json_encode($resp); exit;
						
					}
					
				}else{
					$message = "failed";
					$code = -1;
					$data = null;
					$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
					print json_encode($resp); exit;
					
				}				
			}
			
			
		}else{
			$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;
		}
		
	}
	
	
	
	
	
	public function sendSMS($mobileNo, $randNo) {



        $authKey = "178586A0aldZIg59dbed56";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $mobileNo;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "FPOCKE";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Your FairPockets' OTP is " . $randNo);

        //Define route 
        $route = "99";


        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        // $url="http://api.msg91.com/api/sendhttp.php";
        $url = 'https://control.msg91.com/api/sendotp.php?authkey='. $authKey .'&mobile='. $mobileNo .'&message=Your OTP is '. $randNo .'&sender='. $senderId .'&otp=' . $randNo;

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_POST => true,
            // CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            // echo 'error:' . curl_error($ch);
            return false;
        }

        curl_close($ch);

        // echo $output;
        return true;

    }

    public function salesLoginAuth(){
		CakeLog::write('debug', 'salesLoginAuth'.print_r(json_encode($this->request->data), true));
		$this->loadModel('SalesUser');
		$this->layout = "home";
		print_r($_GET['name']);
		$_GET['name'];
		$_GET['password'];
		
		if($this->SalesUser->hasAny(array("mobile"=>$_GET['mobile']))){
			$usersExist = $this->SalesUser->find('all');
				//echo '<pre>'; print_r($usersExist);
			//$usersDetails = json_encode($usersExist);
			//print_r(json_encode($usersExist));
			$message = "Success";
			$code = 1;
			$data = array("id"=>1,"builder_id"=>45);
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			print json_encode($resp); exit;
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			//print json_encode($resp); exit;;
			print json_encode($resp); exit;
			
		}
		
	}
	
	public function getBuilderMessage() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		CakeLog::write('debug', 'getBuildersByUserId'.print_r(json_encode($this->request->data), true));
		$this->loadModel('Websiteuser');
		//$_POST['user_id'] = 39;
		
		
		
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
			
			$usertype = 'sales';
			$this->loadModel('MessageBoardAppusersAlls');
		
		if($usertype == 'sales'){
			echo '884';
			
			$getProjects = $this->MessageBoardAppusersAlls->find('all', array(
			'fields'=> array(
						'message'			
					),
			'joins' => array(array('table' => 'message_board_appusers_alls_histories',
                        'alias' => 'MessageBoardAppusersAllsHistories',
                        'type' => 'INNER',
                        'conditions' => array('MessageBoardAppusersAllsHistories.message_board_id = MessageBoardAppusersAlls.id',
						'MessageBoardAppusersAllsHistories.sales_or_broker_id' => $_POST['user_id'],
						'MessageBoardAppusersAllsHistories.status' => 1)))));
						
				echo $getmessage[0]['MessageBoardAppusersAlls']['message'];die();
		//print_r($getProjects);die();	
		}
		}
			
		/*if($_POST['user_id'] == 161 || $_POST['user_id'] == 163){
				$getProjects = $this->Websiteuser->find('all',
					array('fields'=> array(
							'id AS builder_id',
							'userorgname AS builder_name'			
						),
						  'conditions'=>array(
							'id' => array(45,122,392,141,122,402,404,412,419,424)
						)
					)
				);
		}elseif($_POST['user_id'] == 145){
				$getProjects = $this->Websiteuser->find('all',
					array('fields'=> array(
							'id AS builder_id',
							'userorgname AS builder_name'			
						),
						  'conditions'=>array(
							'id' => array(45,122,141)
						)
					)
				);
		}else{
				$getProjects = $this->Websiteuser->find('all',
					array('fields'=> array(
							'id AS builder_id',
							'userorgname AS builder_name'			
						),
						  'conditions'=>array(
							'id' => 122222
						)
					)
				);
			}			
			$things = Set::extract('/Websiteuser/.', $getProjects);			
			$message = "Success";
					$code = 1;					
					$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
					print json_encode($resp);
		}*/
		
	}
	
	public function getTowerDropdownByUserIdInv() {		
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		CakeLog::write('debug', 'getTowerDropdownByUserId'.print_r(json_encode($this->request->data), true));
		$this->loadModel('SalesUser');
		$this->loadModel('InvProjectsHistory');
		//$this->loadModel('InvInventory');
		$usertype = $_POST['user_type'];
		$userid = $_POST['user_id'];
		$project_id = $_POST['project_id'];
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
		if($usertype == 'sales'){
			$view1 = new View($this);
			$Custom1 = $view1->loadHelper('Number');
			$builderId = $Custom1->getBuilderIdBySalesUserId($_POST['user_id']);			
		}else{
			$builderId = $_POST['builder_id'];
		}
		//$builderId = $_POST['builder_id'];
		//echo $project_id; die();
		$towerArray = $this->InvInventory->find('all', array(
            'fields' => array("InvInventory.id,InvInventory.tower_name"),
            'conditions' => array(/*"InvWebsiteuser.id = '{$builderId}'",*/
									"InvInventory.project_id"=>$project_id),
			'group' => array(
				'InvInventory.project_id,InvInventory.tower_name'
			),
            'joins' => array(
                array(
                    'table' => 'projects',
                    'alias' => 'InvProject',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvProject.project_id = InvInventory.project_id")
                )/*,
                array(
                    'table' => 'websiteusers',
                    'alias' => 'InvWebsiteuser',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
                )*/
            )
                    
        ));
		echo '<pre>'; print_r($towerArray);die();				
		}
		
		if(!empty($towerArray)){
			$things = Set::extract('/InvInventory/.', $towerArray);			
			$message = "Success";
			$code = 1;					
			$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
			print json_encode($resp);	
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			print json_encode($resp); exit;
		}
	}
	
	public function getInvDetails1(){
		
		header('Content-Type: application/json');
        
		$this->layout = false;
		$this->autoRender = false;
		CakeLog::write('debug', 'getInvDetails1'.print_r(json_encode($this->request->data), true));
		$this->loadModel('SalesUser');
		//$this->loadModel('InvInventory');
		$usertype = $this->request->data['user_type'];
		//$userid = $this->request->data['user_id'];
		$project_id = $this->request->data['project_id'];
		$inventory_id = $this->request->data['inventory_id'];
		$inv_area = $this->request->data['inv_area'];
		$inv_status = $this->request->data['inv_status'];
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
		if($usertype == 'sales'){
			$view1 = new View($this);
			$Custom1 = $view1->loadHelper('Number');
			$builderId = $Custom1->getBuilderIdBySalesUserId($_POST['user_id']);			
		}else{
			$builderId = $_POST['builder_id'];
		}
		
		//echo '884';die();		
		//$this->loadModel('InvInventoryPropertyStatus');
		//$this->loadModel('InventoryPropertyPosition');
		//$this->loadModel('InvInventoryProperty');
		
		$status_arr = $this->InvInventoryPropertyStatus->getStatuses();
		
		//print_r($status_arr);die();
		
		//echo '884<pre>'; print_r($status_arr); die();
			$temp_arr = array();
			
			if (is_array($status_arr) && count($status_arr) > 0) {
				foreach ($status_arr as $row) {
					$temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
				}
			}
			$status_arr = $temp_arr;
			
			
			$position_arr = $this->InvInventoryPropertyPosition->getPositions();
			$temp_arr = array();
			if (is_array($position_arr) && count($position_arr) > 0) {
				foreach ($position_arr as $row) {
					$temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
				}
			}
			$position_arr = $temp_arr;
			
			$inventory_property = $this->InvInventoryProperty->find('all', array(
            'conditions' => array("InvInventoryProperty.inventory_id = '{$inventory_id}'",
			"InvInventoryProperty.flat_area"=>$inv_area,"InvInventoryProperty.flat_status"=>$inv_status
			),
            'order'=>array("InvInventoryProperty.floor_no asc", "InvInventoryProperty.flat_no asc")
			));
			
			$temp_arr = array();
			if (is_array($inventory_property) && count($inventory_property) > 0) {
				foreach ($inventory_property as $property) {
					$temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
				}
			}
			
			//echo '<pre>'; print_r($temp_arr); die();
			//return $temp_arr;
		
		
		/*$allstatusArr = $this->InvInventoryProperty->find('all', array(
			'fields' => array(
				'DISTINCT(flat_status)'
			),
				'conditions' => array(
				   'inventory_id' => $inventory_id
				)
			));
			$thingsStatus = Set::extract('/InvInventoryProperty/.', $allstatusArr);
		*/				
		}
		
		if(!empty($temp_arr)){
			//$things = Set::extract('/InvInventoryProperty/.', $allstatusArr);			
			$message = "Success";
			$code = 1;					
			$resp = array("message"=>$message,"code"=>$code,"data"=>$temp_arr);					
			print json_encode($resp);	
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			print json_encode($resp); exit;
		}
		
	}
	
	public function getInvStatus() {		
		header('Content-Type: application/json');
        //print_r($this->request->data);
		$this->layout = false;
		$this->autoRender = false;
		CakeLog::write('debug', 'getInvStatus'.print_r(json_encode($this->request->data), true));
		$this->loadModel('SalesUser');
		$this->loadModel('InvProjectsHistory');
		//$this->loadModel('InvInventory');
		$usertype = $this->request->data['user_type'];
		$userid = $this->request->data['user_id'];
		$project_id = $this->request->data['project_id'];
		$inventory_id = $this->request->data['inventory_id'];
		$inventory_area = $this->request->data['inv_area'];
		$apistatus = $this->request->data['status'];
		//echo $apistatus; die();
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
		if($usertype == 'sales'){
			$view1 = new View($this);
			$Custom1 = $view1->loadHelper('Number');
			$builderId = $Custom1->getBuilderIdBySalesUserId($_POST['user_id']);			
		}else{
			$builderId = $_POST['builder_id'];
		}
		//$builderId = $_POST['builder_id'];
		//echo $project_id; die();
		
		//$towerArray = $this->InvProperty->find('all', array());
		
		if($apistatus == 'wing'){
			$allstatusArr = $this->InvInventoryProperty->find('all', array(
					'fields' => array(
						'DISTINCT(flat_status)'
					),
						'conditions' => array(
						   'inventory_id' => $inventory_id,
						   'flat_area' => $inventory_area
						)
					));
			$thingsStatus = Set::extract('/InvInventoryProperty/.', $allstatusArr);
		}
		
		if($apistatus == 'tower'){
			//echo '884';die();
			$getTowerNameByInvId = $this->InvInventory->find('all', array(
				'fields' => array("InvInventory.tower_name"),
				'conditions' => array("InvInventory.id"=>$inventory_id)
			));
			
			//echo '<pre>'; print_r($getTowerNameByInvId); die();// 
			//$getTowerNameByInvId[0]['InvInventory']['tower_name'];
			//echo $project_id;die();
			//echo $getTowerNameByInvId[0]['InvInventory']['tower_name'];
			/*$inIdsByTowerName =  $this->InvInventory->find('all', array(
				//'fields' => array("InvInventory.id"),
				'conditions' => array("InvInventory.project_id"=>$inventory_id,
								"InvInventory.tower_name"=>'A')
			));*/
			$towername = $getTowerNameByInvId[0]['InvInventory']['tower_name'];
			$InvIdsArr = $this->InvInventory->query("SELECT id FROM `inventories` WHERE project_id = '".$project_id."' AND tower_name='".$towername."'");
					
					//echo '<pre>'; print_r($InvIdsArr);
			$InvIdsArrCommaSeperated = Set::extract('/inventories/id', $InvIdsArr);
			//echo '<pre>'; print_r($InvIdsArrCommaSeperated);die();
			$allstatusArr = $this->InvInventoryProperty->find('all', array(
					'fields' => array(
						'DISTINCT(flat_status)'
					),
						'conditions' => array(
						   'inventory_id' => $InvIdsArrCommaSeperated
						)
					));
					//echo '<pre>'; print_r($allstatusArr);die();
			$thingsStatus = Set::extract('/InvInventoryProperty/.', $allstatusArr);
		}
		//echo '<pre>'; print_r($thingsStatus);die();
		
		//echo '<pre>'; print_r($towerArray);die();				
		}
		
		if(!empty($allstatusArr)){
			$things = Set::extract('/InvInventoryProperty/.', $allstatusArr);			
			$message = "Success";
			$code = 1;					
			$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
			print json_encode($resp);	
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			print json_encode($resp); exit;
		}
	}
	
	
	public function getInvArea() {		
		header('Content-Type: application/json');
       // print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		CakeLog::write('debug', 'getInvArea'.print_r(json_encode($this->request->data), true));
		$this->loadModel('SalesUser');
		//$this->loadModel('InvInventory');
		$usertype = $this->request->data['user_type'];
		$userid = $this->request->data['user_id'];
		$project_id = $this->request->data['project_id'];
		$inventory_id = $this->request->data['inventory_id'];
		$apistatus = $this->request->data['status'];
		//echo $apistatus; die();
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
			if($usertype == 'sales'){
				$view1 = new View($this);
				$Custom1 = $view1->loadHelper('Number');
				$builderId = $Custom1->getBuilderIdBySalesUserId($_POST['user_id']);			
			}else{
				$builderId = $_POST['builder_id'];
			}
		
			if($apistatus == 'wing'){
				$allstatusArr = $this->InvInventoryProperty->find('all', array(
						'fields' => array(
							'DISTINCT(flat_area)'
						),
							'conditions' => array(
							   'inventory_id' => $inventory_id
							)
						));
				$thingsStatus = Set::extract('/InvInventoryProperty/.', $allstatusArr);
			}
		
			if($apistatus == 'tower'){
				//echo $apistatus; die();
				$getTowerNameByInvId = $this->InvInventory->find('all', array(
					'fields' => array("InvInventory.tower_name"),
					'conditions' => array("InvInventory.id"=>$inventory_id)
				));
				
				
				$towername = $getTowerNameByInvId[0]['InvInventory']['tower_name'];
				$InvIdsArr = $this->InvInventory->query("SELECT id FROM `inventories` WHERE project_id = '".$project_id."' AND tower_name='".$towername."'");
				
				$InvIdsArrCommaSeperated = Set::extract('/inventories/id', $InvIdsArr);
				$allstatusArr = $this->InvInventoryProperty->find('all', array(
						'fields' => array(
							'DISTINCT(flat_area)'
						),
							'conditions' => array(
							   'inventory_id' => $InvIdsArrCommaSeperated
							)
						));
				$thingsStatus = Set::extract('/InvInventoryProperty/.', $allstatusArr);
			}
						
		}
		
		if(!empty($allstatusArr)){
			$things = Set::extract('/InvInventoryProperty/.', $allstatusArr);			
			$message = "Success";
			$code = 1;					
			$resp = array("message"=>$message,"code"=>$code,"data"=>$things);					
			print json_encode($resp);	
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			print json_encode($resp); exit;
		}
	}
	
}
