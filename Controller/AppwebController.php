<?php

App::uses('AuthComponent', 'Controller/Component');

// app/Controller/AppController.php
class AppController extends Controller {

    //...

    public $components = array(
        'Session', 'Cookie',
        'Flash',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'users', 'action' => 'admin_dashboard'),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'admin_login'
            ),
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email', 'password' => 'password'),
                    'passwordHasher' => 'Blowfish'
                )
            ),
            'authorize' => array('Controller') // Added this line
        )
    );

    public function beforeFilter() {

        Security::setHash('blowfish');
        $this->Auth->allow('index', 'view');
        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {

            $this->layout = 'admin';
        }
    }

    public function isAuthorized($user) {


        // Admin can access every action
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '0') {

            return true;
        }
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '1') {
            return true;
        }
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '2') {
            return true;
        }
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '3') {
            return true;
        }
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '4') {
            return true;
        }
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '5') {
            return true;
        }
        if (isset($user['employee_role_id']) && $user['employee_role_id'] === '6') {
            return true;
        }
        // Default deny
        return false;
    }

    public function send_mail($email_data = null) {
        //echo "<pre>";print_r($email_data);die;
        //echo WWW_ROOT;die;
        $email = new CakeEmail('default');
        $email_to = $email_data['to'];
        $email_msg = $email_data['body'];
        $email_subject = $email_data['subject'];

        $email->to($email_to);
        $email->subject($email_subject);
        $mail_status = @$email->send($email_msg);

        if (!$mail_status) {
            return FALSE;
        }
        return TRUE;
    }

}

?>