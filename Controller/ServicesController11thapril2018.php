<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ServicesController extends AppController {

    public $uses = array('Property','User','Project','ProjectDetails','ProjectPricing','Websiteuser',        
		'Lead','OtpApp','SolrData','AppClientLead','AppReminderLead');
		
	public $components = array('Paginator','Propertydata','RequestHandler');

    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('getCalculatorDataByDiscount', 'getClientDataByUserIdClientId','getCalculatorDataByFloor','getClientDataByUserId',
							'getReminderDataByUserId','postCalculatorDataSubmit','getCalculatorData',
							'getCalculatorByProjectId', 'getCalculatorByProjectIdTesting', 'getBuildersByUserId',
							'salesLoginAuth', 'validateMobile', 'verifyOtp', 'getUnitsByProjectId', 'getProjectsByUserId');
    }
	
	/*public function pdf_file(){
			if(isset($this->request->params['pass'][0]) && $this->request->params['pass'][0] == 'pdf'){
				ob_start();
				header("Content-type: application/pdf");
				require_once(ROOT . DS . "vendors/fpdf/".DS."fpdf.php");
				$this->view = 'pdf_file';
			}else{
				$this->autoRender = false;
				echo '884'; exit;			
			}
	}*/
	
	public function getCalculatorData(){
		CakeLog::write('debug', 'caldata'.print_r($this->request->data, true));
		
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];;		
		$ProjectChargesOptionalTotal = 0;
		for ($row = 0; $row < count($optionalChargesArray); $row++) {					
					$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
					if (1 == $optionalChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				//$property_area = 1245;
				$gBaseRate = $this->request->data['property_detail']['base_rate'];
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				$Registration = 7;
				
				$base_price = $property_area*$gBaseRate;
				$gst_base_price = ($base_price*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100 +
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100;
				$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal+$ProjectChargesOptionalTotal)*$Registration/100;
				
				$chartData = array('base_charges'=>$base_price,
								   'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> 7);
								   
				$resp = array("message"=>'Success',"code"=>1,
				              "chart_data"=>$chartData,"Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult'.print_r($resp, true) );
				print json_encode($resp); exit;
				
				
		
	}
	
	public function getCalculatorDataByDiscount(){
		CakeLog::write('debug', 'caldata-discount'.print_r($this->request->data, true) );
		//echo '884';die();
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];;		
		$ProjectChargesOptionalTotal = 0;
		for ($row = 0; $row < count($optionalChargesArray); $row++) {					
					$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
					if (1 == $optionalChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				
			/*$this->loadModel('ProjectBspDetail');
			$bspoptionid = $this->request->data['property_detail']['floor_no'];
			$projectid = $this->request->data['property_detail']['project_id'];
			$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$bspoptionid." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
			//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
				$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];
				//$gBaseRate = $this->request->data['property_detail']['base_rate'];*/
				
				$disc_amount = $this->request->data['discount']['proj_discount'];
				$disc_unit  = $this->request->data['discount']['proj_discount_unit'];
				$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
				
				
				if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = ($property_area*$gBaseRate) - $disc_amount;					
				}
				if($disc_unit == 2){
						$disc_price_cal = ($property_area*$gBaseRate)*$disc_amount/100;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $property_area*$disc_amount;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				
				
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				$Registration = 7;
				
				$base_price = $base_price_new;
				$gst_base_price = ($base_price*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100 +
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100;
				$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal+$ProjectChargesOptionalTotal)*$Registration/100;
				
				$chartData = array('base_charges'=>$base_price,
								   'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> 7);
								   
				$resp = array("message"=>'Success',"code"=>1,
				              "chart_data"=>$chartData,"base_rate"=>$gBaseRate,"Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult'.print_r($resp, true) );
				print json_encode($resp); exit;
		
	
	}
	
	
	public function getCalculatorDataByFloor(){
		//print_r($_POST);
		//print_r($this->request->data);
		CakeLog::write('debug', 'caldata'.print_r($this->request->data, true) );
		//$get_response = '{"optional_charges":[{"charge_title":"Corner PLC","charge_value":"150","unit_type":"1"}]}';
		
				//$optionalChargesArray1 = json_decode($get_response);
		$optionalChargesArray = $this->request->data['optional_charges'];
		$mandatoryChargesArray = $this->request->data['mandatory_charges'];
		$property_area = $this->request->data['property_detail']['property_area'];;		
		$ProjectChargesOptionalTotal = 0;
		for ($row = 0; $row < count($optionalChargesArray); $row++) {					
					$MandatoryChargeAmt = $optionalChargesArray[$row]['charge_value'];
					if (1 == $optionalChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesOptionalTotal = $ProjectChargesOptionalTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				for ($row = 0; $row < count($mandatoryChargesArray); $row++) {					
					$MandatoryChargeAmt = $mandatoryChargesArray[$row]['charge_value'];
					if (1 == $mandatoryChargesArray[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ($MandatoryChargeAmt );
					}           
				
				}
				
				//$property_area = 1245;
				$this->loadModel('ProjectBspDetail');
			$bspoptionid = $this->request->data['property_detail']['floor_no'];
			$projectid = $this->request->data['property_detail']['project_id'];
			$bsp_price = $this->ProjectBspDetail->query("SELECT bsp_charges FROM `project_bsp_details` WHERE ".$bspoptionid." BETWEEN `floor_from` AND `floor_to` AND project_id = ".$projectid."");			
			//echo  $bsp_price[0]['project_bsp_details']['bsp_charges']; die();
				$gBaseRate = $bsp_price[0]['project_bsp_details']['bsp_charges'];
				//$gBaseRate = $this->request->data['property_detail']['base_rate'];
				$gServiceTaxOnBSP = $this->request->data['property_detail']['gst_on_base'];
				$gServiceTaxOnOTH = $this->request->data['property_detail']['gst_on_others'];
				$Registration = 7;
				
				$base_price = $property_area*$gBaseRate;
				$gst_base_price = ($base_price*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100 +
												$ProjectChargesOptionalTotal +
				                                ($ProjectChargesOptionalTotal*$gServiceTaxOnOTH)/100;
				$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal+$ProjectChargesOptionalTotal)*$Registration/100;
				
				$chartData = array('base_charges'=>$base_price,
								   'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> 7);
								   
				$resp = array("message"=>'Success',"code"=>1,
				              "chart_data"=>$chartData,"base_rate"=>$gBaseRate,"Final Calculated Price"=>$final_price);
				CakeLog::write('debug', 'finalresult'.print_r($resp, true) );
				print json_encode($resp); exit;		
				
		
	}
	
	
	
	function _newWorkorderpdf($workorderHTMLName, $workorderPDFName) {
        // Include Component 
        App::import('Component', 'Pdf');
        // Make instance 
        $Pdf = new PdfComponent();
        // Invoice name (output name) 
        $Pdf->filename = 'workorder-' . $workorderPDFName; // Without .pdf 
        // You can use download or browser here 
        //$Pdf->output = 2;//'download'; 
        $Pdf->output = 'file';
        //$Pdf->method = 'pdflib';
        $Pdf->init();
        $Pdf->process(Router::url('/', true) . 'app/webroot/files/workorders/' . $workorderHTMLName);		
        return 1;
        die;
    }
	
	public function postCalculatorDataSubmit(){
		
		if(isset($this->request->params['pass'][0]) && $this->request->params['pass'][0] == 'pdf'){
			//$this->layout = 'empty';
			
			//echo $_GET['user_id'] = 2;
			//echo $_GET['email'] = 'digpal@gmail.com';
			//echo $_GET['mobile']= '65356595';
			//$this->request->data['AppClientLead']['name'] = $this->request->data['clientDetails']['name'];
			//$this->request->data['AppClientLead']['remarks'] = $this->request->data['clientDetails']['remarks'];
			//$this->request->data['AppClientLead']['reminderDate'] = $this->request->data['clientDetails']['reminderDate'];
			//$this->request->data['AppClientLead']['reminderText'] = $this->request->data['clientDetails']['reminderText'];
			//$this->request->data['AppClientLead']['status'] = 1;
		   //print_r($this->request->data['AppClientLead']);die();
			//if(isset($_POST['mobile']) && $_POST['mobile'] !=''){
			//$requestMobile = $_POST['mobile'];
			//echo $requestMobile; die();
			$query = "Select * from app_client_leads WHERE email = '".$_GET['email']."' OR mobile = '".$_GET['mobile']."'";
			
			$clientExistOrNot = $this->AppClientLead->query($query);
            echo '<pre>'; print_r($clientExistOrNot);die();    
			if($clientExistOrNot){
				$this->loadModel('AppClientLeadsHistory');
				$this->request->data['AppClientLeadsHistory']['client_id'] = 884;
				$this->request->data['AppClientLeadsHistory']['user_id'] = 884;
				$this->request->data['AppClientLeadsHistory']['project_id'] = 884;
				$this->request->data['AppClientLeadsHistory']['name'] = 884;
				$this->request->data['AppClientLeadsHistory']['remarks'] = 884;
				$this->request->data['AppClientLeadsHistory']['reminderDate'] = 884;
				$this->request->data['AppClientLeadsHistory']['reminderText'] = 884;
				$this->request->data['AppClientLeadsHistory']['pdf_link'] = 884;
				
				$savdata = $this->AppClientLeadsHistory->save($this->request->data['AppClientLeadsHistory']);
				$lastinsertId = $clientExistOrNot[0]['app_client_leads']['id'];
			}else{
				//echo '884';die();
				$this->request->data['AppClientLead']['user_id'] = 2;
				$this->request->data['AppClientLead']['email'] = $_GET['email'];
				$this->request->data['AppClientLead']['mobile'] = $_GET['mobile'];
				//echo '<pre>'; print_r($this->request->data['AppClientLead']);die();  
				$savdata = $this->AppClientLead->save($this->request->data['AppClientLead']);
				$lastinsertId = $this->AppClientLead->getLastInsertId();				
			}
			echo $lastinsertId; die();
			
			/*CakeLog::write('debug', 'pdfdata'.print_r($this->request->data, true) );
			$this->set('pdfGenerateRequest', true);
			//$view = new View($this);
			//$viewdata = $view->render(NULL, 'empty', 'post_calculator_data_submit');
			//$this->view = 'post_calculator_data_submit';
			
			$this->request->data['project_id'] = 88945;
			
			$workorderHTMLName = "workorder_html4pdf.html";
			$workorderPDFName = 'project-'.$this->request->data['project_id'];
			$path = WWW_ROOT . "files/workorders/" . $workorderHTMLName;
			//echo $path; die();
			$file = new File($path, true);
			//$docketFileName = "";
			if ($file->write($viewdata)) {
				$this->_newWorkorderpdf($workorderHTMLName, $workorderPDFName);				
				$docketFileFullPath = _ROOT_PATH . 'vendors/html2ps/public_html/out/workorder-project-'.$this->request->data['project_id'].'.pdf';
				$newFileFullPath = _ROOT_PATH . 'app/webroot/files/workorders/project-'.$this->request->data['project_id'].'.pdf';
				$isPdf = true;
				copy($docketFileFullPath, $newFileFullPath);
					unlink($path);
					unlink($docketFileFullPath);
			}*/
		}else{
			$this->autoRender = false;
			CakeLog::write('debug', 'myArray-submit'.print_r($this->request->data, true) );
			
			$this->request->data['AppClientLead']['user_id'] = $this->request->data['user_id'];
			$this->request->data['AppClientLead']['email'] = $this->request->data['clientDetails']['email'];
			$this->request->data['AppClientLead']['mobile'] = $this->request->data['clientDetails']['mobile'];
			$this->request->data['AppClientLead']['name'] = $this->request->data['clientDetails']['name'];
			$this->request->data['AppClientLead']['remarks'] = $this->request->data['clientDetails']['remarks'];
			$this->request->data['AppClientLead']['reminderDate'] = $this->request->data['clientDetails']['reminderDate'];
			$this->request->data['AppClientLead']['reminderText'] = $this->request->data['clientDetails']['reminderText'];
			$this->request->data['AppClientLead']['status'] = 1;
		   //print_r($this->request->data['AppClientLead']);
		   
		   $query = "Select * from app_client_leads WHERE email = '".$this->request->data['AppClientLead']['email']."' OR mobile = '".$this->request->data['AppClientLead']['mobile']."'";
			
		   $clientExistOrNot = $this->AppClientLead->query($query);
		   
		   
		   if($clientExistOrNot){
				$this->loadModel('AppClientLeadsHistory');
				$this->request->data['AppClientLeadsHistory']['client_id'] = $clientExistOrNot[0]['app_client_leads']['id'];
				$this->request->data['AppClientLeadsHistory']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppClientLeadsHistory']['project_id'] = $this->request->data['project_id'];
				$this->request->data['AppClientLeadsHistory']['name'] = $this->request->data['AppClientLead']['name'];
				$this->request->data['AppClientLeadsHistory']['remarks'] = $this->request->data['AppClientLead']['remarks'];
				$this->request->data['AppClientLeadsHistory']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppClientLeadsHistory']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				
				$linkId1 = $clientExistOrNot[0]['app_client_leads']['id'].'_'.$this->request->data['project_id'];
				
				$this->request->data['AppClientLeadsHistory']['pdf_link'] = 'http://fairpockets.com/files/workorders/project-'.$linkId1.'.pdf';
				
				$savdata = $this->AppClientLeadsHistory->save($this->request->data['AppClientLeadsHistory']);
				$lastinsertId = $clientExistOrNot[0]['app_client_leads']['id'];
			}else{				
				$savdata = $this->AppClientLead->save($this->request->data['AppClientLead']);
				$lastinsertId = $this->AppClientLead->getLastInsertId();				
			}
		   
		   
			//if ($this->AppClientLead->save($this->request->data['AppClientLead'])) {
				$this->layout = 'empty';
				//$lastinsertId = $this->AppClientLead->getLastInsertId();
				
				$this->request->data['AppReminderLead']['user_id'] = $this->request->data['AppClientLead']['user_id'];
				$this->request->data['AppReminderLead']['reminderDate'] = $this->request->data['AppClientLead']['reminderDate'];
				$this->request->data['AppReminderLead']['reminderText'] = $this->request->data['AppClientLead']['reminderText'];
				$this->request->data['AppReminderLead']['status'] = 1;
				//CakeLog::write('debug', 'AppReminderLead - '.print_r($this->request->data['AppReminderLead'], true) );
				$this->AppReminderLead->save($this->request->data['AppReminderLead']);
				
				//$pdfdataview = $this->request->data;
				
				$view1 = new View($this);
				$Custom1 = $view1->loadHelper('Number');
				$projectName = $Custom1->getProjectNameByProjectId($this->request->data['project_id']);
				
				$this->set('pdfGenerateRequest', true);
				$this->set('projectName', $projectName[0]['Project']['project_name']);
				$this->set('baseRate', $this->request->data['property_detail']['base_rate']);
				$this->set('propertyArea', $this->request->data['property_detail']['property_area']);
				$this->set('propertyOnFloor', $this->request->data['property_detail']['floor_no']);
				$this->set('baseCharges', $this->request->data['chart_data']['base_charges']);
				$this->set('additionalCharges', $this->request->data['chart_data']['additional_charges']);
				$this->set('gstTotal', $this->request->data['chart_data']['gst_total']);
				$this->set('stampRegTotal', $this->request->data['chart_data']['stamp_plus_registration']);
				$this->set('finalCalculatePrice', $this->request->data['final_calculated_price']);
				$this->set('visitorName', $this->request->data['AppClientLead']['name']);
				
				$this->loadModel('SalesUser');
				$usersDatas = $this->SalesUser->find('all', array(
					'conditions' => array(
						'id' => $this->request->data['user_id']
					)
				));
				$this->set('userName', $usersDatas[0]['SalesUser']['name']);
				$this->set('userMobile', $usersDatas[0]['SalesUser']['mobile']);
				
				//$view = new View($this);
				$viewdata = $this->render('post_calculator_data_submit');
				//$this->view = 'post_calculator_data_submit';
				//$this->layout = 'empty';
				$linkId = $lastinsertId.'_'.$this->request->data['project_id'];
				$workorderHTMLName = "workorder_html4pdf.html";
				$workorderPDFName = 'project-'.$linkId;
				$path = WWW_ROOT . "files/workorders/" . $workorderHTMLName;
				//echo $path; die();
				$file = new File($path, true);
				//$docketFileName = "";
				if ($file->write($viewdata)) {
					$this->_newWorkorderpdf($workorderHTMLName, $workorderPDFName);				
					$docketFileFullPath = _ROOT_PATH . 'vendors/html2ps/public_html/out/workorder-project-'.$linkId.'.pdf';
					$newFileFullPath = _ROOT_PATH . 'app/webroot/files/workorders/project-'.$linkId.'.pdf';
					$isPdf = true;
					copy($docketFileFullPath, $newFileFullPath);
						unlink($path);
						unlink($docketFileFullPath);
					
				}				
						
					
					
					
					$pdf_link = 'http://fairpockets.com/files/workorders/project-'.$linkId.'.pdf';
					$resp = array("message"=>'Success',"code"=>1,"pdf_link"=>(string)$pdf_link,"data"=>null);
					print json_encode($resp,JSON_UNESCAPED_SLASHES); exit;
				//}
						
		}
		
		
	} 
	
	public function getClientDataByUserId(){
	
            $this->autoRender = false;
			$user_id = $_POST['user_id'];
			//$this->loadModel('AppClientLead');
			
			$clientData = $this->AppClientLead->find('all', array(
					'conditions' => array(
						'user_id' => $user_id
					)
				));
				if(!empty($clientData)){
				$things = Set::extract('/AppClientLead/.', $clientData);		
				$message = "Success";
				$code = 1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);				
				}else{
					$message = "Failed";
				$code = -1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>null);
					
				}
				print json_encode($resp);
				
	
	}
	
	
	public function getClientDataByUserIdClientId(){
	CakeLog::write('debug', 'history'.print_r($this->request->data, true) );
            $this->autoRender = false;
			$user_id = $_POST['user_id'];
			$client_id = $_POST['client_id'];
			$this->loadModel('AppClientLeadsHistory');
			
			$clientData = $this->AppClientLeadsHistory->find('all', array(
					'conditions' => array(
						'user_id' => $user_id,
						'client_id' => $client_id
					)
				));
				
				if(!empty($clientData)){
				$things = Set::extract('/AppClientLeadsHistory/.', $clientData);		
				$message = "Success";
				$code = 1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);				
				}else{
					$message = "Failed";
				$code = -1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>null);
					
				}
				print json_encode($resp);
	
	}
	
	public function getReminderDataByUserId(){
	
            $this->autoRender = false;
			$user_id = $_POST['user_id'];
			//$this->loadModel('AppClientLead');
			
			$clientData = $this->AppReminderLead->find('all', array(
					'conditions' => array(
						'user_id' => $user_id
					)
				));
				
				if(!empty($clientData)){
				$things = Set::extract('/AppReminderLead/.', $clientData);		
				$message = "Success";
				$code = 1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);				
				}else{
					$message = "Failed";
				$code = -1;				
				$resp = array("message"=>$message,"code"=>$code,"data"=>null);
					
				}
				print json_encode($resp);
	
	}
	
	
	
	public function getCalculatorByProjectId(){
	//print $json = json_encode($a);//die();
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = $_POST['project_id'];
			$property_area = $_POST['property_area'];
			//$project_id = 903;
			//$property_area = 1045;
			//$project_id = 701;
			CakeLog::write('debug', 'caldata-1'.print_r($this->request->data, true) );
			$this->loadModel('ProjectPricing');
			
			$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $project_id
					)
				));
				
			$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];
						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}
						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];

					}
				}
						//echo '884'; echo '<pre>'; print_r($project_charges);
						//print json_encode($project_charges);
						//echo count($project_pricing_charge) / 4;
				for ($i = 0, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if ($project_charges[$i][1] == 1) {
						$project_charges_mand[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_mand[$j]['unit_type'] = $project_charges[$i][3];
					}
					if ($project_charges[$i][1] == 2) {
						$project_charges_optional[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_optional[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_optional[$j]['unit_type'] = $project_charges[$i][3];						
					}
				}
				
				//$project_charges_mand_ok = json_encode(array_values($project_charges_mand));
				//$project_charges_optional_ok = json_encode(array_values($project_charges_optional));
				//print_r($project_charges_mand);
				//print_r($project_charges_optional);die();
				
				
				
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Sinking Fund";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				$additon_charges_mand = array();
				$additon_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if ($additon_charges[$i][1] == 1) {
						$additon_charges_mand[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_mand[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_mand[$j]['unit_type'] = $additon_charges[$i][3];
					}
					if ($additon_charges[$i][1] == 2) {
						$additon_charges_optional[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_optional[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_optional[$j]['unit_type'] = $additon_charges[$i][3];						
					}
				}
				
				
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				$other_charges_mand = array();
				$other_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if ($other_charges[$i][1] == 1) {
						$other_charges_mand[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_mand[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_mand[$j]['unit_type'] = $other_charges[$i][3];
					}
					if ($other_charges[$i][1] == 2) {
						$other_charges_optional[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_optional[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_optional[$j]['unit_type'] = $other_charges[$i][3];						
					}
				}
				//print_r($project_charges_mand);
				//print_r($project_charges_optional_ok);
				$gaProjectCharges = array_merge($additon_charges_mand, $project_charges_mand, $other_charges_mand);
				//echo '<pre>'; print_r($mandProjectCharges); echo '----884----';
				$gaProjectChargesLen = count($mandProjectCharges);
				$optionalProjectCharges = array_merge($additon_charges_optional, $project_charges_optional, $other_charges_optional);
				//echo '<pre>'; print_r($optionalProjectCharges);die();
				
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				//$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				//$gaProjectChargesLen = count($mandProjectCharges);
				//print 
				//echo '<pre>'; print_r($gaProjectCharges);//die();
				//print json_encode($gaProjectCharges); die();
				
				
				
				
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}
				
				
				
				
				// Project Pricing - Stamp Duty
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				
				
				
				
				
				//echo $gBaseRate.'--- reg--'.$Registration; die();
				
				
				
				for ($row = 0; $row < count($gaProjectCharges); $row++) {					
					$MandatoryChargeAmt = $gaProjectCharges[$row]['charge_value'];
					if (1 == $gaProjectCharges[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt );
					}           
				
				}
				
				
				//echo $final_price;  die();
				
				if(!empty($gaRegistration)){
					if (2 == $gaRegistration[0][0]) {
						// Percentage
						$Registration = $registration_amount = $gaRegistration[0][1] . " %";
						$RegistrationCalcutedAmount = (($property_area*$gBaseRate + $ProjectChargesMandatoryTotal ) * ( $gaRegistration[0][1] / 100 ));
					} else {
						// Flat Charges
						$Registration = "Rs " . $gaRegistration[0][1];
						$RegistrationCalcutedAmount = "Rs " . $gaRegistration[0][1];
					}
				}
				
				if(!empty($gaStampDuty)){
					if (2 == $gaStampDuty[0][0]) {
						// Percentage
						$gaStampDutyok = $gaStampDuty[0][1] . " %";
					} else {
						// Flat Charges
						$gaStampDutyok =  "Rs " . $gaStampDuty[0][1];
					}
				}
				
				
				
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,
												"gst_on_base"=>$gServiceTaxOnBSP,
												"gst_on_others"=>$gServiceTaxOnOTH,
												"registration_charges"=>$Registration,
												"stamp_duty"=>$gaStampDutyok);
												
												
				$this->loadModel('ProjectsAndroidApp');
				$getProjectsDiscount = $this->ProjectsAndroidApp->find('all',
					array('fields'=> array(
							'proj_discount',
							'proj_discount_unit'			
						),
						  'conditions'=>array(
							'sales_or_broker_employee_id' => $_POST['user_id'],
							'project_id' => $project_id
						)
					)
				);
				$disc_amount = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount'];
				$disc_unit  = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount_unit'];
				$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
				
				
				if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = ($property_area*$gBaseRate) - $disc_amount;					
				}
				if($disc_unit == 2){
						$disc_price_cal = ($property_area*$gBaseRate)*$disc_amount/100;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $property_area*$disc_amount;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				
				
				
				/*if(isset($base_price_new) && $base_price_new!=''){
					$base_price = $base_price_new;
					$showDiscount = 'true';
				}else{
					$base_price = $property_area*$gBaseRate;
					$showDiscount = 'false';
				}*/
				
				
				if(isset($base_price_new) && $base_price_new!=''){					
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;						
						$base_price = $base_price_new - $gstInputCreditAmount;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showInputCredit = 'true';
					}
					else{												
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $base_price_new;
					$showDiscount = 'true';					
				}else{
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;
						$base_price = $base_price_new - $gstInputCreditAmount;
						$showInputCredit = 'true';
					}
					else{						
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $property_area*$gBaseRate;
					$showDiscount = 'false';
				}
				
				
				
				
				//$base_price = $base_price_new;
				//$base_price = $property_area*$gBaseRate;
				$gst_base_price = ($base_price*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100;
				$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				if(!isset($project_id) || empty($project_id)){
                $resp = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
				$chartData = array('base_charges'=>$base_price,
								   'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationCalcutedAmount);
								   
				$this->loadModel('ProjectBspDetail');
				$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$project_id ."");			
				//echo '<pre>';print_r($bsp_limit);
				//$bsp_limit[0][0]['startloop'];
				//$bsp_limit[0][0]['endloop'];
				$floor_choice = array('startloop'=>$bsp_limit[0][0]['startloop'],
								   'endloop'=> $bsp_limit[0][0]['endloop']);
								   
                $resp = array("message"=>$message,
								"code"=>$code,
								"property_detail"=>$property_detail_array,
								"mandatory_charges"=>$gaProjectCharges,
								"optional_charges"=>$optionalProjectCharges,
								"showDiscount"=>$showDiscount,
								"discount"=>$project_disc_array,								
								"showInputCredit"=>$showInputCredit,
								"gstInputCredit"=>$gstInputCredit,
								"floor_choice"=>$floor_choice,
								"chart_data"=>$chartData,
								"final_calculated_price"=>$final_price);
				}
				CakeLog::write('debug', 'getCalculatorByProjectId-response884'.print_r($resp, true) );
            
				print json_encode($resp); exit;
				
				
				//die();
				
				
				
				

				/*
				  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
				  pr($gaProjectCharges);
				  echo "<br>"."gaStampDuty"."<br>";
				  pr($gaStampDuty);
				  echo "<br>"."gaRegistration"."<br>";
				  pr($gaRegistration);
				  echo "<br>"."gaFloorPLC"."<br>";
				  pr($gaFloorPLC);
				 */
				 
				 	


				$gaMandatoryCharges = '';
				$gaOptionalCharges = '';
				$gaOptionalCount = 0;
				$ProjectCharges = 0;
				$gaFloorPLCCharges = 0;

				if(!empty($gaFloorPLC))
				{
				if ($gaFloorPLC[0][1] > 0) {
					$gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

					if ($gaFloorPLC[0][0] == 2) {
						// BSP w.r.t Ground floor
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					} else {
						// BSP w.r.t TOP floor;
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					}

					if ($gaFloorPLC[0][2] == 2) {
						// sub option
						$gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
					}

					$gaMandatoryCharges = $gaMandatoryCharges . "
									<div class='col-sm-12'>
										<label for='' class='checkbox-custom-label'>
								";
					$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
							'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC1[0][1] . "&nbsp;/ sq ft</span>
									";
					$gaMandatoryCharges = $gaMandatoryCharges . "
										</label>  
									</div>
							";
				}
				}
				$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
				$gOfferPrice = $gOfferPrice + $BaseCharge;
				$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

				//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
				//echo "<br><br>BaseCharge - ".$BaseCharge;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				// Project Charges
				$ProjectCharges = 0;

				echo '<pre>'; print_r($gaProjectCharges);
				
				
				

				for ($row = 0; $row < $gaProjectChargesLen; $row++) {
					//echo "<br><br>".$gaMandatoryCharges;
					if (1 == $gaProjectCharges[$row][1]) {
						// Mandatory to include the charges

						$gaMandatoryCharges = $gaMandatoryCharges . "
										<div class='col-sm-12'>
											<label for='' class='checkbox-custom-label'>
								";

						//Calculate Charges

						$MandatoryChargeAmt = $gaProjectCharges[$row][2];
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
									";
						}

						$gaMandatoryCharges = $gaMandatoryCharges . "
											</label>  
										</div>
								";

						echo "<br><br>	ProjectCharges - ".$ProjectCharges;
					} else {
						// Optional to include the charges
						$gaOptionalCount = $gaOptionalCount + 1;
						$tempid = "optional" . $gaOptionalCount;

						$gaOptionalCharges = $gaOptionalCharges . "
										<div class='col-sm-12'>
											
								";

						//Calculate Charges
						$OptionalChargeAmt = $gaProjectCharges[$row][2];
						$tempChargeAmt = 0;
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

							// Display Charge in Optional Section
							$gaOptionalCharges = $gaOptionalCharges . "
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$tempChargeAmt = $OptionalChargeAmt;

							// Display Charge in Mandatory Section
							$gaOptionalCharges = $gaOptionalCharges . "
									
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
									";
						}

						$gaOptionalCharges = $gaOptionalCharges . "
											
											</label>  
										</div>
								";
					}

					//echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
				}
				//echo '884'.'---';
				//echo $gaOptionalCharges;
				//echo '-1884'.'---';
				//echo '884'. $gaMandatoryCharges;

				$gProjectCharges_a = $ProjectCharges;
					echo '884'. $gProjectCharges_a;die();
				$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
				$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

				//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
				//echo "<br><br>gServiceTax - ".$gServiceTax;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				//Stamp Duty
				$StampDuty = 0;
				if(!empty($gaStampDuty)){
				if (2 == $gaStampDuty[0][0]) {
					// Percentage
					$StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
				} else {
					// Flat Charges
					$StampDuty = $gaStampDuty[0][1];
				}
				}
				else{
				$StampDuty = '';
				}
				$gOfferPrice = $gOfferPrice + $StampDuty;
				
				$Registration = 0;
				if(!empty($gaRegistration)){
				if (2 == $gaRegistration[0][0]) {
					// Percentage
					$Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
				} else {
					// Flat Charges
					$Registration = $gaRegistration[0][1];
				}
				}
				else{
				$Registration = '';
				}
				$gOfferPrice = $gOfferPrice + $Registration;
				
				echo $gaMandatoryCharges;
			
			
			
			
			
			
			
			
			
			
            $response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("status"=>"Project Id not provided","status_code"=>"0","result"=>array());
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }
	
	public function getCalculatorByProjectIdTesting(){

				//print $json = json_encode($a);//die();
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = 903;
			$property_area = 1045;
			//$project_id = 903;
			//$property_area = 1045;
			//$project_id = 701;
			CakeLog::write('debug', 'caldata-1'.print_r($this->request->data, true) );
			$this->loadModel('ProjectPricing');
			
			$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $project_id
					)
				));
				
			$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];
						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}
						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];

					}
				}
						//echo '884'; echo '<pre>'; print_r($project_charges);
						//print json_encode($project_charges);
						//echo count($project_pricing_charge) / 4;
				for ($i = 0, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if ($project_charges[$i][1] == 1) {
						$project_charges_mand[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_mand[$j]['unit_type'] = $project_charges[$i][3];
					}
					if ($project_charges[$i][1] == 2) {
						$project_charges_optional[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_optional[$j]['charge_value'] = $project_charges[$i][2];
						$project_charges_optional[$j]['unit_type'] = $project_charges[$i][3];						
					}
				}
				
				//$project_charges_mand_ok = json_encode(array_values($project_charges_mand));
				//$project_charges_optional_ok = json_encode(array_values($project_charges_optional));
				//print_r($project_charges_mand);
				//print_r($project_charges_optional);die();
				
				
				
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Sinking Fund";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				$additon_charges_mand = array();
				$additon_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if ($additon_charges[$i][1] == 1) {
						$additon_charges_mand[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_mand[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_mand[$j]['unit_type'] = $additon_charges[$i][3];
					}
					if ($additon_charges[$i][1] == 2) {
						$additon_charges_optional[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_optional[$j]['charge_value'] = $additon_charges[$i][2];
						$additon_charges_optional[$j]['unit_type'] = $additon_charges[$i][3];						
					}
				}
				
				
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				$other_charges_mand = array();
				$other_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if ($other_charges[$i][1] == 1) {
						$other_charges_mand[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_mand[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_mand[$j]['unit_type'] = $other_charges[$i][3];
					}
					if ($other_charges[$i][1] == 2) {
						$other_charges_optional[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_optional[$j]['charge_value'] = $other_charges[$i][2];
						$other_charges_optional[$j]['unit_type'] = $other_charges[$i][3];						
					}
				}
				//print_r($project_charges_mand);
				//print_r($project_charges_optional_ok);
				$gaProjectCharges = array_merge($additon_charges_mand, $project_charges_mand, $other_charges_mand);
				//echo '<pre>'; print_r($mandProjectCharges); echo '----884----';
				$gaProjectChargesLen = count($mandProjectCharges);
				$optionalProjectCharges = array_merge($additon_charges_optional, $project_charges_optional, $other_charges_optional);
				//echo '<pre>'; print_r($optionalProjectCharges);die();
				
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				//$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				//$gaProjectChargesLen = count($mandProjectCharges);
				//print 
				//echo '<pre>'; print_r($gaProjectCharges);//die();
				//print json_encode($gaProjectCharges); die();
				
				
				
				
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}
				
				
				
				
				// Project Pricing - Stamp Duty
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				
				
				
				
				
				//echo $gBaseRate.'--- reg--'.$Registration; die();
				
				
				
				for ($row = 0; $row < count($gaProjectCharges); $row++) {					
					$MandatoryChargeAmt = $gaProjectCharges[$row]['charge_value'];
					if (1 == $gaProjectCharges[$row]['unit_type']) {
						// Per Square feet 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt * $property_area );					   
					} else {
						// Per Unit 	
						$ProjectChargesMandatoryTotal = $ProjectChargesMandatoryTotal + ( $MandatoryChargeAmt );
					}           
				
				}
				
				
				//echo $final_price;  die();
				
				if(!empty($gaRegistration)){
					if (2 == $gaRegistration[0][0]) {
						// Percentage
						$Registration = $registration_amount = $gaRegistration[0][1] . " %";
						$RegistrationCalcutedAmount = (($property_area*$gBaseRate + $ProjectChargesMandatoryTotal ) * ( $gaRegistration[0][1] / 100 ));
					} else {
						// Flat Charges
						$Registration = "Rs " . $gaRegistration[0][1];
						$RegistrationCalcutedAmount = "Rs " . $gaRegistration[0][1];
					}
				}
				
				if(!empty($gaStampDuty)){
					if (2 == $gaStampDuty[0][0]) {
						// Percentage
						$gaStampDutyok = $gaStampDuty[0][1] . " %";
					} else {
						// Flat Charges
						$gaStampDutyok =  "Rs " . $gaStampDuty[0][1];
					}
				}
				
				
				
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,
												"gst_on_base"=>$gServiceTaxOnBSP,
												"gst_on_others"=>$gServiceTaxOnOTH,
												"registration_charges"=>$Registration,
												"stamp_duty"=>$gaStampDutyok);
												
				
				echo 'Base area -'.$gBaseRate.'<br/>';
				echo 'property_area -'.$property_area.'<br/>';
				//echo 'Base area -'.$gBaseRate.'<br/>';
				
				$this->loadModel('ProjectsAndroidApp');
				$getProjectsDiscount = $this->ProjectsAndroidApp->find('all',
					array('fields'=> array(
							'proj_discount',
							'proj_discount_unit'			
						),
						  'conditions'=>array(
							'sales_or_broker_employee_id' => 2,
							'project_id' => $project_id
						)
					)
				);
				$disc_amount = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount'];
				$disc_unit  = $getProjectsDiscount[0]['ProjectsAndroidApp']['proj_discount_unit'];
				$project_disc_array = array('disc_amount'=>$disc_amount,'disc_unit'=>$disc_unit);
				
				
				if($disc_unit == 1){
						$disc_price_cal = $disc_amount;
					$base_price_new = ($property_area*$gBaseRate) - $disc_amount;					
				}
				if($disc_unit == 2){
						$disc_price_cal = ($property_area*$gBaseRate)*$disc_amount/100;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				if($disc_unit == 3){
						$disc_price_cal = $property_area*$disc_amount;					
					$base_price_new = ($property_area*$gBaseRate) - $disc_price_cal;					
				}
				echo 'base_price_new -'.$base_price_new.'<br/>';
				
				
				if(isset($base_price_new) && $base_price_new!=''){					
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;						
						$base_price = $base_price_new - $gstInputCreditAmount;
						//echo $base_price.'--'.$base_price_new.'--'.$gstInputCreditAmount;die();
						$showInputCredit = 'true';
					}
					else{												
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $base_price_new;
					$showDiscount = 'true';					
				}else{
					if(isset($project_pricing['ProjectPricing']['price_srtax_icr']) && $project_pricing['ProjectPricing']['price_srtax_icr'] !=''){
						$gstInputCredit = $project_pricing['ProjectPricing']['price_srtax_icr'];
						$gstInputCreditAmount = ($property_area*$gBaseRate)*$gstInputCredit/100;
						$base_price = $base_price_new - $gstInputCreditAmount;
						$showInputCredit = 'true';
					}
					else{						
						$gstInputCredit = null;
						$showInputCredit = 'fales';						
					}
					$base_price = $property_area*$gBaseRate;
					$showDiscount = 'false';
				}
				
				//echo '<pre>'; print_r($project_pricing); die();
				
				
				//die();
				
				
				//$base_price = $base_price_new;
				//$base_price = $property_area*$gBaseRate;
				$gst_base_price = ($base_price*$gServiceTaxOnBSP)/100;
				$total_gst_base_price = $base_price + $gst_base_price;
				$total_gst_others_base_price = $total_gst_base_price + $ProjectChargesMandatoryTotal +
				                                ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100;
				$final_price = $total_gst_others_base_price + ($base_price+$ProjectChargesMandatoryTotal)*$Registration/100;
				
				if(!isset($project_id) || empty($project_id)){
                $resp = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
				$chartData = array('base_charges'=>$base_price,
								   'gst_total'=> $total_gst_base_price + ($ProjectChargesMandatoryTotal*$gServiceTaxOnOTH)/100,
								   'additional_charges'=> $ProjectChargesMandatoryTotal,
								   'stamp_plus_registration'=> $RegistrationCalcutedAmount);
								   
				$this->loadModel('ProjectBspDetail');
				$bsp_limit = $this->ProjectBspDetail->query("SELECT MIN(floor_from) AS startloop, MAX(floor_to) AS endloop  FROM `project_bsp_details` WHERE project_id = ".$project_id ."");			
				//echo '<pre>';print_r($bsp_limit);
				//$bsp_limit[0][0]['startloop'];
				//$bsp_limit[0][0]['endloop'];
				$floor_choice = array('startloop'=>$bsp_limit[0][0]['startloop'],
								   'endloop'=> $bsp_limit[0][0]['endloop']);
								   
                $resp = array("message"=>$message,
								"code"=>$code,
								"property_detail"=>$property_detail_array,
								"mandatory_charges"=>$gaProjectCharges,
								"optional_charges"=>$optionalProjectCharges,
								"showDiscount"=>$showDiscount,
								"discount"=>$project_disc_array,
								"showInputCredit"=>$showInputCredit,
								"gstInputCredit"=>$gstInputCredit,
								"floor_choice"=>$floor_choice,
								"chart_data"=>$chartData,
								"final_calculated_price"=>$final_price);
				}
				CakeLog::write('debug', 'getCalculatorByProjectId-response'.print_r($resp, true) );
            
				print json_encode($resp); exit;
            
        }
	
	
	public function getUnitsByProjectId(){
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			CakeLog::write('debug', 'unitarray'.print_r($this->request, true) );
			$project_id = $_POST['project_id'];
            //$response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }

    /* get state for add property */
	
	public function validateMobile(){
		header('Content-Type: application/json');
		//Configure::read('debug', 2);
		$this->loadModel('SalesUser');
		//$this->loadModel('OtpApp');
		$this->layout = false;
		$this->autoRender = false;
		//echo $_GET['mobile'];
		//print_r($_REQUEST);
		//$requestMobile = $_REQUEST['mobile'];
		
		//echo '<pre>'; print_r($this->SalesUser->find('all'));
		if(isset($_POST['mobile']) && $_POST['mobile'] !=''){
			$requestMobile = $_POST['mobile'];
			//echo $requestMobile; die();
			if($this->SalesUser->hasAny(array("mobile"=>$requestMobile))){
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'mobile' => $requestMobile
					)
				));
				$message = "Success";
				$code = 1;
				$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				
				$randNo = rand(1000, 9999);
				$mobileNo = $requestMobile ;
				$milliseconds = round(microtime(true) * 1000) + (10 * 60 * 1000);
				$this->OtpApp->save([
					'OtpApp' => [ 'otp' => $randNo, 'expiry' => $milliseconds, 'mobile' => $mobileNo ]
				]);
				if($this->OtpApp->getAffectedRows() == 1) {
					$this->sendSMS($mobileNo, $randNo);            
				}  else {
					echo json_encode(['message' => 'Invalid request!', 'type' => 'error']);
				}
					//die;*/
					
				$resp = array("message"=>$message,"code"=>$code,"otp"=>$randNo);
				print json_encode($resp); exit;
			}else{			
				$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;			
			}			
		}
		//echo $requestEmail;
		if(isset($_POST['email'])&& $_POST['email']!=''){
			$requestEmail = $_POST['email'];
			$requestPassword = $_POST['password'];
			//echo $requestEmail; die();
			if($this->SalesUser->hasAny(array("email"=>$requestEmail,"password"=>$requestPassword))){
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'email' => $requestEmail,
						'password' => $requestPassword
					)
				));
				$message = "Success";
				$code = 1;
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],"user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;
			}else{			
				$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;			
			}			
		}
		
		
		
	}
	
	public function verifyOtp() {
        header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->loadModel('SalesUser');
		$this->request->data['mobile'] = $_POST['mobile'];
		$this->request->data['otp'] = $_POST['otp'];
		
        $otp_app = $this->OtpApp->find('first', [
            'conditions' => $this->request->data
        ]);
				
       // print_r($otp); die();
        if(!empty($otp_app)) {
            $milliseconds = round(microtime(true) * 1000);
            if($otp_app['OtpApp']['expiry'] > $milliseconds) {
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'mobile' => $_POST['mobile']
					)
				));
				$message = "Success";
				$code = 1;
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],"user_id"=>$usersData[0]['SalesUser']['id']);
				
                print json_encode([ 'message' => 'success','code' => 1, 'data' => $data ]);
            } else {
                print json_encode([ 'type' => 'failure' ]);
            }
        } else {
            print json_encode([ 'type' => 'failure' ]);
        }
        exit;
    }
	
	public function getProjectsByUserId() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		CakeLog::write('debug', 'getprojectdata'.print_r($this->request->data, true));
		//die();
		$this->layout = false;
		$this->autoRender = false;
		
		$this->loadModel('ProjectsAndroidApp');
		$this->loadModel('Project');
		//$_POST['user_id'] = 2;
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
			if(isset($_POST['user_id'])&& $_POST['user_id']==6){
				
				$getProjects = $this->Project->find('all',
				array('fields'=> array(
						'project_id',
						'project_name'			
					),
					  'conditions'=>array(
						'user_id' => $_POST['builder_id']
					)
				)
			);
			$things = Set::extract('/Project/.', $getProjects);			
		}else{			
			$getProjects = $this->ProjectsAndroidApp->find('all',
				array('fields'=> array(
						'project_id',
						'project_name'			
					),
					  'conditions'=>array(
						'sales_or_broker_employee_id' => $_POST['user_id']
					)
				)
			);
			$things = Set::extract('/ProjectsAndroidApp/.', $getProjects);
		}
		
		//echo '<pre>'; print_r($things);		
		//die();
		//print json_encode($things, true);
		
		$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);
				
				print json_encode($resp);
		//print_r($getProjects);exit;
		}
		
	}
	
	public function getBuildersByUserId() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		
		$this->loadModel('Websiteuser');
		//$_POST['user_id'] = 39;
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
		$getProjects = $this->Websiteuser->find('all',
			array('fields'=> array(
					'id AS builder_id',
					'userorgname AS builder_name'			
				),
				  'conditions'=>array(
					'id' => array(45,122,392,400,141,122,402)
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);die();
		$things = Set::extract('/Websiteuser/.', $getProjects);
		//echo '<pre>'; print_r($things);		
		//die();
		//print json_encode($things, true);
		
		$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);
				
				print json_encode($resp);
		//print_r($getProjects);exit;
		}
		
	}
	
	
	
	
	
	public function sendSMS($mobileNo, $randNo) {



        $authKey = "178586A0aldZIg59dbed56";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $mobileNo;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "FPOCKE";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Your FairPockets' OTP is " . $randNo);

        //Define route 
        $route = "99";


        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        // $url="http://api.msg91.com/api/sendhttp.php";
        $url = 'https://control.msg91.com/api/sendotp.php?authkey='. $authKey .'&mobile='. $mobileNo .'&message=Your OTP is '. $randNo .'&sender='. $senderId .'&otp=' . $randNo;

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_POST => true,
            // CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            // echo 'error:' . curl_error($ch);
            return false;
        }

        curl_close($ch);

        // echo $output;
        return true;

    }

    public function salesLoginAuth(){
		
		$this->loadModel('SalesUser');
		$this->layout = "home";
		print_r($_GET['name']);
		$_GET['name'];
		$_GET['password'];
		
		if($this->SalesUser->hasAny(array("mobile"=>$_GET['mobile']))){
			$usersExist = $this->SalesUser->find('all');
				//echo '<pre>'; print_r($usersExist);
			//$usersDetails = json_encode($usersExist);
			//print_r(json_encode($usersExist));
			$message = "Success";
			$code = 1;
			$data = array("id"=>1,"builder_id"=>45);
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			print json_encode($resp); exit;
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			//print json_encode($resp); exit;;
			print json_encode($resp); exit;
			
		}
		
	}
}
