<div class="page-content">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"> Change Password </h1>
		</div>
		<!--/.col-lg-12 -->
	</div>
	
	<?php echo $this->Session->flash(); ?>
	
	<div id="build" class="list-detail detail-block target-block">
		
		<div class="block-inner-link">
			
			<?php
				echo $this->Form->create(
				'Websiteuser', array(
				'class' => 'fpform',
				'role' => 'form',
				'id' => 'id_form_builderorg_profile',
				'novalidate' => 'novalidate',
				'url' => 'changePassword'
				)
				);
			?>
			
			
			<div class="account-block">
				
				<div class="row">
					
					<div class="col-sm-4">
						<div class="form-group">
							
							<label for="old_password" class="label_title">Old Password<span class="mand_field">*</span></label>
							<?php
								echo $this->Form->input(
								'old_password', array(
								'id' => 'old_password',
								'type' => 'password',
								'class' => 'form-control',
								'label' => false
								)
								);
							?>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							
							<label for="password" class="label_title">New Password <span class="mand_field">*</span></label>
							<?php
								echo $this->Form->input(
								'password', array(
								'id' => 'password',
								'type' => 'password',
								'class' => 'form-control',
								'label' => false
								)
								);
							?>
							
						</div>
                        </div>
						
                        <div class="col-sm-4">
                            <div class="form-group">
								
                                <label for="repassword" class="label_title">Confirm Password <span class="mand_field">*</span></label>
								<?php
                                    echo $this->Form->input(
									'repassword', array(
									'id' => 'repassword',
									'type' => 'password',
									'class' => 'form-control',
									'label' => false
									)
                                    );
								?>
							</div>
						</div>
						
						
                        <div class="col-sm-12">
                            <div class="account-block  submit_area">
								
                                <button type="submit" class="btn btn-primary" >Change</button>
								
							</div>
						</div>
						
					</div>
					
				</div>
				
                <!-- </form> -->
				<?php
                    echo $this->Form->end();
				?>
				
				
			</div>
		</div>
		
		
		
        <div class="clear"></div>
		
	</div>

<?php
	echo $this->Html->script('validate_1.9_jquery.validate.min');
	
	// Script  : To set validation rules for FORMS
	echo $this->Html->scriptBlock("
	
	
	var rulesMTarea = {
	required : true
	};
	
	var messageMTarea = {
	required : 'Password is required.'
	};
	
	var rulesPass = {
	required : true,
	equalTo: '#password'
	};
	
	var messagePass = {
	required : 'Password is required.',
	equalTo: 'Confirm Password is not same as New Password.'
	};
	
	$(function() {
	
	$('#id_form_builderorg_profile').validate({
	
	// Specify the validation rules
	rules: {
	'data[Websiteuser][old_password]': rulesMTarea,
	'data[Websiteuser][password]': rulesMTarea,
	'data[Websiteuser][repassword]': rulesPass
	},
	
	// Specify the validation error messages
	messages: {
	'data[Websiteuser][old_password]': messageMTarea,
	'data[Websiteuser][password]': messageMTarea,
	'data[Websiteuser][repassword]': messagePass
	}
	});
	});
	
	");
?>

