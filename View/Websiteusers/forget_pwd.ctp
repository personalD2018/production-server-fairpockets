<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no;" />
<title>Fair Pockets</title>
</head>

<body>
<table style="max-width:600px;border:1px solid #e6e6e6;min-width:260px; -webkit-text-size-adjust:none;" align="center" border="0" cellspacing="0" cellpadding="0">  
  <tbody>
    <tr>
      <td bgcolor="#29333d" style="border-bottom: 4px solid #ffc000;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td></td>
            <td height="20"></td>
            <td></td>
          </tr>
          <tr>
            <td width="20"></td>
            <td><img src="<?php echo _HTTP_PATH.'img/logo.gif'; ?>" width="220" height="60"></td>
            <td width="20"></td>
          </tr>
          <tr>
            <td></td>
            <td height="20"></td>
            <td></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td width="600"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td></td>
            <td height="30"></td>
            <td></td>
          </tr>
          <tr>
            <td width="20"></td>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 14px; color: #120c0c;">Hi <?php echo $user_name; ?>, </td>
                </tr>
                <tr>
                  <td height="15"></td>
                </tr>
                
                <tr>
                  <td >&nbsp;</td>
                </tr>
				<tr>
                  <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 14px; color: #120c0c;">
				  
						Welcome back to FairPockets,We bring your realty investment to life.<br>
						Your Password has been reset Successfully<br>
						Your Login id : <?php echo $user_email; ?><br>
						Your Password is : <?php echo $user_password; ?><br>
						Please Login using the following link :<br>
						<a href="https://www.fairpockets.com/">Login Now</a>
				  
				  </td>
                </tr>
				
				
				<!--<tr>
                  <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 14px; color: #120c0c;">Property For - <?php //echo $prop_link; ?></td>
                </tr>-->
                <!--<tr>
                  <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 19px; color: #2d619b;">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</td>
                </tr>-->
				
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="120" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td  height="35" align="center" style="font-family:'Open Sans', Arial; font-size:14px; color:#ffffff; font-weight: 500" bgcolor="#d62540">
							Please login using the following link :<br>
							<a href="https://www.fairpockets.com" target="_blank" style="text-decoration:none; color:#ffffff;">Login Now</a>
						</td>
                      </tr>
                    </tbody>
                  </table></td>
                </tr>
              </tbody>
            </table></td>
            <td width="20"></td>
          </tr>
          <tr>
            <td></td>
            <td height="20"></td>
            <td></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td></td>
            <td height="20"></td>
            <td></td>
          </tr>
          <tr>
            <td width="20"></td>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 14px; color: #333;">Warm Regards,</td>
                </tr>
                <tr>
                  <td height="8"></td>
                </tr>
                <tr>
                  <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 14px; color: #333;"><strong>Fair Pockets team</strong>
                    </td>
                </tr>
              </tbody>
            </table></td>
            <td width="20"></td>
          </tr>
          <tr>
            <td></td>
            <td height="20"></td>
            <td></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td width="600" bgcolor="#29333d"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td></td>
            <td height="15"></td>
            <td></td>
          </tr>
          <tr>
            <td width="20"></td>
            <td style="font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 9px; color: #fff;"><strong>*Terms & Conditions apply</strong><br>
              is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</td>
            <td width="20"></td>
          </tr>
          <tr>
            <td></td>
            <td height="17"></td>
            <td></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>


</body>
</html>
