<?php
// CSS - Form UI

echo $this->Html->css('bootstrap.min');
echo $this->Html->css('font-awesome.min');
echo $this->Html->css('animate.min');
echo $this->Html->css('prettyPhoto');
echo $this->Html->css('main14');
echo $this->Html->css('responsive');


// Jquery - Form UI

echo $this->Html->script('html5shiv');
echo $this->Html->script('respond.min');


echo $this->Html->scriptBlock("
	
		// Function : To clear/set registration from fields when registration TAB is clicked
		function InitRegisterPane()
		{
			// Show registration pane
			document.getElementById('id_register_form').style.display = 'inline' ;
			
			//Reset form
			document.getElementById('register-form').reset();
			
			//Remove validation error messages
			//document.getElementById('register-form').validate.reset();
			var validator = $( '#register-form' ).validate();
			validator.resetForm();
			
			//Remove class error from all inputs... To remove red line from form elements
			$('.form-group input').removeClass('error');
			
			// Hide Error box
			document.getElementById('id_rform-error-label').style.display = 'none';
						
			// Hide success pane
			document.getElementById('id_register_succ').style.display = 'none' ;
		}

		// Function : To clear/set login from fields when registration TAB is clicked
		function InitLoginPane()
		{
			//Mark login pane as active... bit of hardcoding
			var tabPanel = document.getElementById('id_tab_panes');
			tabPanel.childNodes[1].className = 'tab-pane active'; // Login Pane is marked active
			tabPanel.childNodes[5].className = 'tab-pane'; // Forgot password pane is marked non-acitve
			
			
			// Show login form
			document.getElementById('id_login_form').style.display = 'inline' ;
			
			//Reset form
			document.getElementById('login-form').reset();
			document.getElementById('forgotpass-form').reset();
			
			//Remove validation error messages
			//document.getElementById('register-form').validate.reset();
			var validator = $( '#login-form' ).validate();
			validator.resetForm();
			
			//Remove class error from all inputs... To remove red line from form elements
			$('.form-group input').removeClass('error');
			
			
			
			//Remove validation error messages
			//document.getElementById('login-form').validate.reset();
			
			// Hide Error box
			document.getElementById('id_lform-error-label').style.display = 'none';
			document.getElementById('id_fform-error-label').style.display = 'none';
						
			// Hide forgot password success pane
			document.getElementById('id_forgotpass_succ').style.display = 'none' ;
		}		
		
		
		// Function : To set company name option for register form
		// Company Name Mandatory - if user is agent(usr_type - 2) / builder( usr_type - 3)
		// Company Name Optional - if user is individual(usr_type - 1)		
		function SetCompanyOption( usr_type )
		{
			//alert(document.getElementById('id_reg_company').innerHTML );
			
			//Reset form ... is it needed ????
			//document.getElementById('register-form').reset();
			
			//Remove validation error messages
			//document.getElementById('register-form').validate.reset();
			var validator = $( '#register-form' ).validate();
			validator.resetForm();
			
			//Remove class error from all inputs... To remove red line from form elements
			$('.form-group input').removeClass('error');
			
			
			
			if( usr_type == '1')
			{
				document.getElementById('id_group_reg_company').style.display = 'none'; 
				document.getElementById('id_reg_company').innerHTML = ''; 				
			} 
			else 
			{
				document.getElementById('id_group_reg_company').style.display = 'block';
				document.getElementById('id_reg_company').innerHTML = 
				'<input type=' +'\"text\"' +
				      ' class='+'\"form-control\"' +
					  ' name=' +'\"data[User][userorgname]\"' +
					  ' placeholder=' +'\"Organization\"' +
				' \/>'; 
				
			}
		
		}
	");
?>


<h1> Fair Pockets </h1>








<div class="LoginApp">

    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button">Log in / Register</a>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dial">
            <div class="modal-content">
                <div class="modal-body modalpadd">
                    <div class="row">

                        <div class="col-md-6 modulsideback">

                            <div class="col-sm-12">
                                <p>&quot;Ab 42 lakh mein Gurgaon<br>
                                    mein 2BHK milega.&quot;
                                </p>
                                <h4>#Mere<span>Budget</span>Mein</h4>

                                <div class="modalbacklidiv" style="margin-top:53px;display:block;">
                                    <h3>Buy at your price</h3>
                                    <div class="col-sm-12">
                                        <ul>
                                            <li>
                                                <div class=" text-center sing-l-tet fl">
                                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Negotiate Online
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-12 ">
                                        <ul>
                                            <li>
                                                <div class=" text-center sing-l-tet fl">
                                                    <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Spam Free Experience
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-12 ">
                                        <ul>
                                            <li>
                                                <div class=" text-center sing-l-tet fl">
                                                    <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Top Rated Buyers &amp; Sellers
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 loginsidediv" style="border-right: 1px dotted #C2C2C2;padding-right: 10px;">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            </div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#Login" data-toggle="tab" onclick="InitLoginPane();" >Login</a>
                                </li>

                                <li>
                                    <a href="#Registration" data-toggle="tab" onclick="InitRegisterPane();" >Registration</a>
                                </li>
                            </ul>
                            <!-- Nav tabs Ends-->

                            <!-- Tab panes -->
                            <div class="tab-content" id="id_tab_panes">

                                <div class="tab-pane active" id="Login">

                                    <div id="id_login_form" >

                                        <h2>Sign <span>In</span></h2>

                                        <?php echo $this->Session->flash('auth'); ?>
                                        <!-- Login Form -->

                                        <?php
                                        echo $this->Form->create(
                                                'Websiteuser', array(
                                            'class' => 'form-horizontal fpform',
                                            'id' => 'login-form',
                                                )
                                        );
                                        ?>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <!-- 
                                                        <input type="text" class="form-control" id="email1" placeholder="Email" />
                                                    <div class="error has-error" style="display:block;">Box must be filled out</div> 
                                                -->
                                                <?php
                                                echo $this->Form->input(
                                                        'email', array(
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Email'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                    <!-- <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" /> -->
                                                <?php
                                                echo $this->Form->input(
                                                        'password', array(
                                                    'label' => false,
                                                    'type' => 'password',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Password'
                                                        )
                                                );
                                                ?>														
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="form-error-label" id="id_lform-error-label" ></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input id="Remember" class="checkbox-custom" name="Remember" type="checkbox">
                                                <label for="Remember" class="checkbox-custom-label">Remember Me</label>
                                            </div>

                                            <div class="col-sm-6 text-right">
                                                <a href="#Forgotpassword" class="color-red" data-toggle="tab" onclick="document.getElementById('id_forgotpass_form').style.display = 'inline';">Forgot your password?</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-primary btn-red" onclick="">
                                                    <i class="fa fa-sign-in"></i> &nbsp;Sign in
                                                </button>
                                            </div>
                                        </div>
                                        <!-- </form> -->
                                        <?php
                                        echo $this->Form->end();
                                        ?>
                                        <!-- Login Form Ends -->
                                    </div>
                                </div>


                                <div class="tab-pane" id="Registration">

                                    <div class="text-center" id="id_register_succ" style="display:none;" >
                                    </div> 

                                    <div id="id_register_form" >

                                        <h2>Sign <span>up</span></h2>

                                        <?php
                                        echo $this->Session->flash();
                                        ?>

                                        <!-- Registration Form -->

                                        <?php
                                        echo $this->Form->create(
                                                'Websiteuser', array(
                                            'class' => 'form-horizontal fpform',
                                            'role' => 'form',
                                            'id' => 'register-form',
                                            'novalidate' => 'novalidate'
                                                )
                                        );
                                        ?>

                                        <div class="form-group" style="">

                                            <div class="col-sm-12">

        <!-- <input id="usertype1" class="radio-custom" name="bachelor_all" type="radio"  checked="checked" onclick="SetCompanyOption(1);" > -->
                                                <!-- <label for="usertype1" class="radio-custom-label">Individual</label> -->
                                                <?php
                                                $options = array(
                                                    '1' => 'Individual',
                                                    '2' => 'Agent',
                                                    '3' => 'Builder'
                                                );

                                                $attributes = array(
                                                    'legend' => false,
                                                    'class' => 'radio-custom',
                                                    'label' => array('class' => 'radio-custom-label'),
                                                    'default' => '1',
                                                    'onclick' => 'SetCompanyOption(this.value)'
                                                );

                                                echo $this->Form->radio('userrole', $options, $attributes);
                                                ?>


                                                <?php
                                                /* 	
                                                  $options = array('1' => 'Individual');
                                                  $attributes = array(
                                                  'class'   => 'radio-custom',
                                                  'label'   => array ('class' => 'radio-custom-label'),
                                                  'legend'  => false,
                                                  'value'   => '1',
                                                  'onclick' => 'SetCompanyOption(this.value)'
                                                  );
                                                  echo $this->Form->radio('userrole', $options, $attributes);

                                                  $options = array('2' => 'Agent');
                                                  $attributes = array(
                                                  'class'   => 'radio-custom',
                                                  'label'   => array ('class' => 'radio-custom-label'),
                                                  'legend'  => false,
                                                  'value'   => '2',
                                                  'onclick' => 'SetCompanyOption(this.value)'
                                                  );
                                                  echo $this->Form->radio('userrole', $options, $attributes);

                                                  $options = array('3' => 'Builder');
                                                  $attributes = array(
                                                  'class'   => 'radio-custom',
                                                  'label'   => array ('class' => 'radio-custom-label'),
                                                  'legend'  => false,
                                                  'value'   => '3',
                                                  'onclick' => 'SetCompanyOption(this.value)'
                                                  );
                                                  echo $this->Form->radio('userrole', $options, $attributes);
                                                 */

                                                /*
                                                  echo $this->form->radio(
                                                  'userRole',
                                                  array(
                                                  '1' => 'Individual',
                                                  '2' => 'Agent',
                                                  '3' => 'Builder'
                                                  ),
                                                  array(
                                                  'name'    => 'userRole',
                                                  'class'   => 'radio-custom',
                                                  'label'   => array ('class' => 'radio-custom-label'),
                                                  'legend'  => false,
                                                  'onclick' => 'SetCompanyOption()',
                                                  'selected' => '1'
                                                  )
                                                  );
                                                 */
                                                ?>

                                            </div>

                                        </div>




                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                    <!-- <input type="text" class="form-control" placeholder="Name" /> -->
                                                <?php
                                                echo $this->Form->input(
                                                        'username', array(
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Name'
                                                        )
                                                );
                                                ?>														
                                            </div>
                                        </div>

                                        <div id="id_group_reg_company" class="form-group" style="display:none;" >
                                            <div id="id_reg_company" class="col-sm-12">
                                                    <!-- <input type="text" class="form-control" placeholder="Company Name" /> -->
                                                <?php
                                                /* This will be dynamically added
                                                  echo $this->Form->input(
                                                  'userOrganizatioName',
                                                  array(
                                                  'label' => false,
                                                  'type'  => 'text',
                                                  'class' => 'form-control',
                                                  'id'    => 'userOrgName',
                                                  'name'  => 'userOrgName',
                                                  'placeholder' => 'Organization Name'

                                                  )
                                                  );
                                                 */
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                    <!-- <input type="text" class="form-control" id="email" name="email" placeholder="Email"/> -->
                                                <?php
                                                echo $this->Form->input(
                                                        'email', array(
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Email'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                    <!-- <input type="text" class="form-control" id="mobile" placeholder="Mobile" /> -->
                                                <?php
                                                echo $this->Form->input(
                                                        'usermobile', array(
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Mobile'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                    <!-- <input type="password" class="form-control" id="password" placeholder="Password" /> --> 
                                                <?php
                                                echo $this->Form->input(
                                                        'password', array(
                                                    'label' => false,
                                                    'type' => 'password',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Password'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="form-error-label" id="id_rform-error-label" ></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <span >By Signing up, I agree with the <a class="color-red" href="/Pages/tac.ctp">T&C </a></span>
                                            </div>
                                        </div>

                                        <div class="form-group signupbtn">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary btn-red">
                                                    <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Sign up
                                                </button>

                                                <!-- <a href="#SuccessMsg" class="btn btn-primary " data-toggle="tab">SuccessMSG</a> -->
                                            </div>
                                        </div>

                                        <!-- </form> -->
                                        <?php
                                        echo $this->Form->end();
                                        ?>	
                                        <!-- Registration Form Ends-->

                                    </div>
                                </div>


                                <div class="tab-pane" id="Forgotpassword">

                                    <div class="text-center" id="id_forgotpass_succ" style="display:none;" >
                                    </div> 

                                    <div id="id_forgotpass_form" >

                                        <h2>Forgot <span>Password</span></h2>

                                        <!-- Forgot Password Form -->

                                        <?php
                                        echo $this->Form->create(
                                                'Websiteuser', array(
                                            'class' => 'form-horizontal fpform',
                                            'id' => 'forgotpass-form'
                                                )
                                        );
                                        ?>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <!-- < input type="text" class="form-control" id="email1" placeholder="Email" /> -->
                                                <?php
                                                echo $this->Form->input(
                                                        'email', array(
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Email'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="form-error-label" id="id_fform-error-label" ></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-primary btn-red">
                                                    <i class="fa fa-paper-plane"></i> &nbsp;Send
                                                </button>
                                            </div>

                                            <div class="col-sm-6 text-right">
                                                <a href="#Login" class="color-red"   data-toggle="tab">Login</a>
                                            </div>
                                        </div>

                                        <!-- </form> -->
                                        <?php
                                        echo $this->Form->end();
                                        ?>
                                        <!-- Forgot Password Form Ends-->

                                    </div>
                                </div>				

                            </div>
                            <!-- Tab panes Ends-->

                            <!-- <div id="OR" class="hidden-xs"> -->
                            <!-- OR -->
                            <!-- </div> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/login-app-->

