<style>
    .red{
        text-align: center;
        font-size: 20px;
        margin-top: 25px;
        margin-bottom: -33px;
        color: red;
    }
</style>
<div style='background: url(/admin_theme/images/foo_bg.png) no-repeat; background-size:cover;height:100vh;' > <!-- AG -->
    <div class="container">
        <?php echo $this->Flash->render(); ?>
        <?php //echo $this->Flash->render('auth'); ?>    
        <?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>

        <h3 class="form-signin-heading">Please sign in</h3>

        <div class="controls input-icon">
            <i class=" icon-user-md"></i>
            <?php echo $this->Form->input('email', array('class' => 'input-block-level', 'placeholder' => 'Email address', 'div' => false, 'label' => false, 'hiddenField' => false, 'id' => 'username_id', 'title' => 'Email', 'value' => $user_rember_data['email'])); ?>

        </div>
        <div class="controls input-icon">
            <i class=" icon-key"></i>     <?php echo $this->Form->input('password', array('class' => 'input-block-level', 'placeholder' => 'Password', 'div' => false, 'label' => false, 'hiddenField' => false, 'id' => 'password', 'title' => 'Password', 'value' => $user_rember_data['password'])); ?>
        </div>
        <label class="checkbox">
            <?php echo $this->Form->input('remember_me', array('label' => false, 'div' => false, 'type' => 'checkbox', 'checked' => $user_rember_data['remember_me'])); ?> Remember me </label>
        <button class="btn btn-inverse btn-block" type="submit">Sign in</button>
        <h4>Forgot your password ?</h4>
        <p>
            <a href="#" id="createacc">Click here</a> to reset your password.
        </p>

        </form>

        <?php echo $this->Form->create('User', array('url' => 'forget_password', 'id' => 'createaccPage', 'method' => 'post', 'class' => 'form-signin')); ?>	
        <h3 class="form-signin-heading">Reset Your Password</h3>
        <div class="controls input-icon">
            <i class=" icon-user-md"></i>

            <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'hiddenField' => false, 'id' => 'username_id', 'title' => 'Email', 'class' => 'input-block-level', 'placeholder' => 'Email address')); ?>
        </div>
        <input type="submit" value="Submit" class="btn btn-inverse btn-block" />
        <button type="button" class="btn btn-inverse btn-block" id="backLogin"><i class="icon-caret-left"></i> Back </button>


        </form>

    </div>
</div>



