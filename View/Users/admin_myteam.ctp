<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">My Team</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Team Details</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Name</th>
                                <th class="center">Profile Pic</th>
                                <th class="center">Emp Id</th>
                                <th class="center">Department</th>
                                <th class="center">Role</th>
                                <th class="center">Manager</th>

                                <th class="center">Email</th>

                                <th class="center">Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($myteam as $employee) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $employee['User']['username']; ?></td>
                                    <td class="center"><?php if (!empty($employee['User']['profile_pic'])) { ?><img src="<?php echo $this->webroot; ?>/upload/profile_pic/<?php echo $employee['User']['profile_pic']; ?>" alt="thumb" width="60" height="40"><?php } else { ?><img src="<?php echo $this->webroot; ?>/backend/images/no_user_image.jpg" alt="thumb" width="60" height="40"><?php } ?>

                                    </td>
                                    <td class="center"><?php echo $employee['User']['empId']; ?></td>

                                    <td class="center"><?php echo $employee['EmployeeDepartment']['departmentname']; ?></td>
                                    <td class="center"><?php echo $employee['EmployeeRole']['role']; ?></td>
                                    <td><?php echo $employee['Userextra']['username']; ?></td>

                                    <td class="center"><?php echo $employee['User']['email']; ?></td>
                                    <td class="center"><?php echo $employee['User']['mobile']; ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>

