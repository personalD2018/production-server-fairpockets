<style>
    .controls{
        margin-top: 5px;
    }
</style>		
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Employees</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>View FP Employee</h3>
                </div>
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('User', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">User Name</label>
                            <div class="controls">

                                <?php echo $this->Form->input('username', array('div' => false, 'label' => false, 'value' => $profile_data['User']['username'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Employee ID</label>
                            <div class="controls">

                                <?php echo $this->Form->input('empId', array('div' => false, 'label' => false, 'value' => $profile_data['User']['empId'], 'disabled' => 'disabled', 'class' => 'small required')); ?>


                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Profile Picture</label>
                            <div class="controls">
                                <?php if ($profile_data['User']['profile_pic'] != '') { ?>
                                    <img src="<?php echo $this->webroot; ?>upload/profile_pic/<?php echo $profile_data['User']['profile_pic']; ?>" alt="thumb" width="60" height="40">
                                    <?php
                                } else {
                                    ?>
                                    <img src="<?php echo $this->webroot; ?>backend/images/no_user_image.jpg" alt="thumb" width="60" height="40">
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Mobile</label>
                            <div class="controls">

                                <?php echo $this->Form->input('mobile', array('div' => false, 'label' => false, 'value' => $profile_data['User']['mobile'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">

                                <?php echo $this->Form->input('address', array('div' => false, 'label' => false, 'value' => $profile_data['User']['address'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">

                                <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'value' => $profile_data['User']['email'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Department</label>
                            <div class="controls">

                                <?php echo $this->Form->input('dname', array('div' => false, 'label' => false, 'value' => $profile_data['EmployeeDepartment']['departmentname'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">

                                <?php echo $this->Form->input('dname', array('div' => false, 'label' => false, 'value' => $profile_data['EmployeeRole']['role'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Manager</label>
                            <div class="controls" id="manger_Data" >
                                <?php echo!empty($profile_data['Userextra']['username']) ? $profile_data['Userextra']['username'] : 'No Manger'; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Date Of Joining</label>

                            <div class="controls">

                                <?php echo $this->Form->input('dname', array('div' => false, 'label' => false, 'value' => $profile_data['User']['doj'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Current Employee</label>
                            <div class="controls">
                                <?php
                                if ($profile_data['User']['is_valid'] == '1') {
                                    echo "Yes";
                                } else {
                                    echo "No";
                                }
                                ?>
                            </div>
                        </div>

                        </form>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="content-widgets gray">
                                    <div class="widget-head blue">
                                        <h3>Area(s) Assigned</h3>
                                    </div>
                                    <div class="widget-container">
                                        <div class="form-container grid-form form-background">						
                                            <?php if (!empty($countryarea)) { ?>						
                                                <div class="control-group">
                                                    <label class="control-label"><h4>Country</h4></label>
                                                    <div class="controls">						
                                                        <?php
                                                        foreach ($countryarea as $country) {
                                                            echo $country['AreaCountryMaster']['countryName'];
                                                            ?><?php
                                                            echo"<br/>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($regionarea)) { ?>
                                                <div class="control-group">
                                                    <label class="control-label"><h4>Region</h4></label>
                                                    <div class="controls">	
                                                        <?php
                                                        foreach ($regionarea as $region) {

                                                            echo $region['AreaCountryMaster']['countryName'];
                                                            ?> > <?php echo $region['AreaRegionMaster']['RegionName']; ?><?php
                                                            echo"<br/>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($stateareadata)) { ?>
                                                <div class="control-group">
                                                    <label class="control-label"><h4>State</h4></label>
                                                    <div class="controls">	
                                                        <?php
                                                        foreach ($stateareadata as $state) {

                                                            echo $state['AreaCountryMaster']['countryName'];
                                                            ?> > >  <?php echo $state['AreaRegionMaster']['RegionName']; ?> > >  <?php echo $state['AreaStateMaster']['stateName']; ?><?php
                                                            echo"<br/>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($city_area)) { ?>
                                                <div class="control-group">
                                                    <label class="control-label"><h4>City</h4></label>
                                                    <div class="controls">	
                                                        <?php
                                                        foreach ($city_area as $city) {

                                                            echo $city['AreaCountryMaster']['countryName'];
                                                            ?> > > > <?php echo $city['AreaRegionMaster']['RegionName']; ?> > > > <?php echo $city['AreaStateMaster']['stateName']; ?> > > > <?php echo $city['AreaCityMaster']['cityname']; ?><?php
                                                            echo"<br/>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($local_area)) { ?>
                                                <div class="control-group">
                                                    <label class="control-label"><h4>Location</h4></label>
                                                    <div class="controls">
                                                        <?php
                                                        foreach ($local_area as $local) {
                                                            echo $local['AreaCountryMaster']['countryName'];
                                                            ?> > > > <?php echo $local['AreaRegionMaster']['RegionName']; ?> > > > <?php echo $local['AreaStateMaster']['stateName']; ?> > > > <?php echo $local['AreaCityMaster']['cityname']; ?> > > > <?php echo $local['AreaLocalMaster']['localityName']; ?><?php
                                                            echo"<br/>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>								
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>				               
                </div>
            </div>
        </div>
    </div>
</div>