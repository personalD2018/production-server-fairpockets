<?php

// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    public $uses = array('EmployeeDepartment', 'EmployeeRole', 'User', 'AreaCountryMaster', 'AreaRegionMaster', 'AreaStateMaster', 'AreaCityMaster', 'AreaLocalMaster');

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout', 'admin_forget_password');
    }

    public function admin_index() {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        $user = $this->User->find('all', array(
            'conditions' => array(
                'User.employee_role_id !=' => '0'
            )
        ));
        $this->set('users', $user);
    }

    public function admin_login() {


        $this->layout = 'login';
        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                if ($this->Auth->user('is_valid') == 0) {
                    $this->Session->setFlash('User doesnot exists', 'default', array('class' => 'red'));

                    return $this->redirect($this->Auth->logout());
                }

                // did they select the remember me checkbox?
                if ($this->request->data['User']['remember_me'] == 1) {

                    $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                    // hash the user's password
                    $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                    // write the cookie
                } else {
                    $this->Cookie->delete('remember_me_cookie');
                }
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Session->setFlash('Username or password is incorrect', 'default', array('class' => 'red'));
            }
        }
        $this->set('user_rember_data', $this->Cookie->read('remember_me_cookie'));
        $this->set(array(
            'title_for_layout' => 'Login'
        ));
    }

    public function admin_logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function admin_viewemployee($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('profile_data', $this->User->findById($id));

        $countryAreauser = $this->AreaCountryMaster->find('all', array(
            'conditions' => array(
                'AreaCountryMaster.user_id' => $id
            )
        ));
        $this->set('countryarea', $countryAreauser);

        $regionAreauser = $this->AreaRegionMaster->find('all', array(
            'conditions' => array(
                'AreaRegionMaster.user_id' => $id
            )
        ));
        $this->set('regionarea', $regionAreauser);



        $AreaStateMaster = $this->AreaStateMaster->find('all', array(
            'conditions' => array(
                'AreaStateMaster.user_id' => $id
            )
        ));
        if (!empty($AreaStateMaster)) {
            foreach ($AreaStateMaster as $StateMaster) {
                $CountryMaster = $this->AreaCountryMaster->find('all', array(
                    'fields' => array(
                        'countryName'
                    ),
                    'conditions' => array(
                        'countryCode' => $StateMaster['AreaRegionMaster']['countryCode']
                    )
                ));
                $statearea[] = array_merge($CountryMaster[0], $StateMaster);
            }
            $this->set('statearea', $statearea);
        }
        $AreaCityMaster = $this->AreaCityMaster->find('all', array(
            'conditions' => array(
                'AreaCityMaster.user_id' => $id
            )
        ));

        if (!empty($AreaCityMaster)) {
            foreach ($AreaCityMaster as $CityMaster) {
                $RegionMaster = $this->AreaRegionMaster->find('all', array(
                    'conditions' => array(
                        'regionCode' => $CityMaster['AreaStateMaster']['regionCode']
                    )
                ));

                $city_area[] = array_merge($RegionMaster[0], $CityMaster);
            }
            $this->set('city_area', $city_area);
        }


        $AreaLocalMaster = $this->AreaLocalMaster->find('all', array(
            'conditions' => array(
                'AreaLocalMaster.user_id' => $id
            )
        ));

        if (!empty($AreaLocalMaster)) {

            foreach ($AreaLocalMaster as $LocalMaster) {
                $StateMaster = $this->AreaStateMaster->find('all', array(
                    'conditions' => array(
                        'statecode' => $LocalMaster['AreaCityMaster']['statecode']
                    )
                ));
                $CountryMaster = $this->AreaCountryMaster->find('all', array(
                    'fields' => array(
                        'countryName'
                    ),
                    'conditions' => array(
                        'countryCode' => $StateMaster[0]['AreaRegionMaster']['countryCode']
                    )
                ));
                $local_area[] = array_merge($StateMaster[0], $LocalMaster, $CountryMaster[0]);
            }
            $this->set('local_area', $local_area);
        }
    }

    public function admin_editEmployee($id = null) {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }
        $target_dir = 'upload/profile_pic/';
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $name = $this->request->data ['User']['profile_pic']['name'];

            if (!empty($name)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['User']['profile_pic']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['User']['profile_pic'] = $newfilename;
                }
            } else {
                $this->request->data ['User']['profile_pic'] = $this->request->data ['User']['profile_pic_edit'];
            }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('The user has been Updated', 'default', array('class' => 'green'));
            }
        } else {
            $this->request->data = $this->User->findById($id);

            unset($this->request->data['User']['password']);
        }

        $employeedepartment = $this->EmployeeDepartment->find('list', array(
            'fields' => array(
                'id',
                'departmentname',
            ),
            'conditions' => array(
                'status' => 1
            )
        ));

        $employeerole = $this->EmployeeRole->find('list', array(
            'fields' => array(
                'id',
                'role',
            ),
            'conditions' => array(
                'id !=' => '0'
            )
        ));

        $manager = $this->User->find('list', array(
            'fields' => array(
                'id',
                'username',
            ),
            'conditions' => array(
                'employee_department_id' => $this->request->data['User']['employee_department_id'],
                'employee_role_id  >' => $this->request->data['User']['employee_role_id']
            )
        ));
        if (empty($manager)) {
            $manager = array(0 => 'No Manger');
        }
        $this->set('manager', $manager);
        $this->set('employeedepartment', $employeedepartment);
        $this->set('employeerole', $employeerole);
    }

    public function admin_deleteemployee($id = null) {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User Deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_dashboard() {
        
    }

    public function admin_addEmployee() {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id != 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }
        $target_dir = 'upload/profile_pic/';
        $employeedepartment = $this->EmployeeDepartment->find('list', array(
            'fields' => array(
                'id',
                'departmentname',
            ),
            'conditions' => array(
                'status' => 1
            )
        ));

        $employeerole = $this->EmployeeRole->find('list', array(
            'fields' => array(
                'id',
                'role',
            ),
            'conditions' => array(
                'id !=' => '0'
            )
        ));

        if ($this->request->is('post')) {


            if (!empty($this->request->data ['User']['profile_pic']['name'])) {

                $name = $this->request->data ['User']['profile_pic']['name'];
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['User']['profile_pic']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['User']['profile_pic'] = $newfilename;
                }
            } else {
                $this->request->data ['User']['profile_pic'] = '';
            }
            if ($this->User->save($this->request->data)) {

				$html = 'Dear ' . $this->request->data ['User']['username'];
				$html .= '<p>We welcome you to Fair Pockets.</p>';
				$html .= '<p>Your Fair Pockets Employee number is : ' . $this->request->data ['User']['empId'] . '</p>';
				$html .= '<p>Your Login id : ' . $this->request->data ['User']['email'] . '</p>';
				$html .= '<p>Your Password is : ' . $this->request->data ['User']['password'] . '</p>';
				$html .= '<p>Please login using the following link :</p>';
				$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot.'admin/">Login Now</a></p>';
				$html .= '<p>Regards,</p>';
				$html .= '<p><strong>FairPockets Team<strong></p>';

                $Email = new CakeEmail();
                //$Email->from(array('admin@FairPockets.com' => 'FairPockets'));
                $Email->to($this->request->data ['User']['email']);
                $Email->subject('Welcome to FairPockets');
				$Email->emailFormat('html');
                $Email->send($html);
                $this->Session->setFlash('The user has been saved Successfully!!!', 'default', array('class' => 'green'));

                unset($this->request->data);
            }
        }
        $this->set('employeedepartment', $employeedepartment);
        $this->set('employeerole', $employeerole);
    }

    public function admin_getManger() {
        if (!empty($this->request->data['department']) && $this->request->data['department'] == '4') {
            $this->request->data['department'] = 3;
        }
        $manager = $this->User->find('list', array(
            'fields' => array(
                'id',
                'username',
            ),
            'conditions' => array(
                'employee_department_id' => $this->request->data['department'],
                'employee_role_id  >' => $this->request->data['role']
            )
        ));

        $this->set('manager', $manager);

        $this->view = "mangerajax";
        $this->layout = "ajax";
    }

    public function admin_myteam() {
        $role_id = $this->Session->read('Auth.User.employee_role_id');
        if ($role_id == 0) {
            return $this->redirect(array('action' => 'dashboard'));
        }

        $user_id = $this->Session->read('Auth.User.id');

        $employee = $this->User->children($user_id);
        $myteam = array();
        foreach ($employee as $employeedata) {

            $myteam[] = $this->User->findById($employeedata['User']['id']);
        }

        $this->set('myteam', $myteam);
    }

    public function admin_profile() {
        $profile_id = $this->Session->read('Auth.User.id');

        $target_dir = 'upload/profile_pic/';
        $this->User->id = $profile_id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $name = $this->request->data ['User']['profile_pic']['name'];

            if (!empty($name)) {
                // Check For Empty Values 
                $tmprary_name = $this->request->data ['User']['profile_pic']['tmp_name'];
                $temp = explode(".", $name);
                $newfilename = uniqid() . '.' . end($temp);
                if (move_uploaded_file($tmprary_name, $target_dir . $newfilename)) {
                    $this->request->data ['User']['profile_pic'] = $newfilename;
                }
            } else {
                $this->request->data ['User']['profile_pic'] = $this->request->data ['User']['profile_pic_edit'];
            }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Your Profile is Updated successfully!!!', 'default', array('class' => 'green'));

                return $this->redirect(array('action' => 'profile'));
            }
        } else {
            $this->request->data = $this->User->findById($profile_id);
        }
        $profile_data = $this->User->findById($profile_id);
        $this->set('profile_data', $profile_data);
    }

    public function admin_reset() {

        $user_id = $this->Session->read('Auth.User.id');
        if ($this->data) {
            if (!empty($this->data['User']['password']) && !empty($this->data['User']['repassword'])) {
                if ($this->data['User']['password'] == $this->data['User']['repassword']) {
                    $current_passwrod = $this->User->currentpassword($user_id);

                    if ($this->User->oldPassword($this->data['User']['old_password']) == $current_passwrod) {
                        $this->User->id = $user_id;

                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash('Password Changed Successfully', 'default', array('class' => 'green'));
                            unset($this->request->data['User']);
                            $this->redirect('/admin/Users/reset');
                        }
                    } else {
                        $this->Session->setFlash('Unable to save password', 'default', array('class' => 'red'));
                        $this->redirect('/admin/Users/reset');
                    }
                } else {
                    $this->Session->setFlash('Password and Re password does not match', 'default', array('class' => 'red'));
                    $this->redirect('/admin/Users/reset');
                }
            } else {
                $this->Session->setFlash('Password and Re password cannot be empty', 'default', array('class' => 'red'));
                $this->redirect('/admin/Users/reset');
            }
        }
    }

    public function admin_forget_password() {

        if ($this->request->is('post')) {

            $user_data = $this->request->data;
            if (!empty($user_data)) {
                $this->User->recursive = -1;
                if (empty($user_data['User']['email'])) {
                    $this->Session->setFlash('Please insert email ID', 'default', array('class' => 'red'));
                    $this->redirect('/admin/Users/login');
                }
                $check_email = $this->User->find('first', array('conditions' => array('User.email' => $user_data['User']['email'])));

                if (!empty($check_email)) {
                    $this->User->id = $check_email['User']['id'];
                    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $new_password = '';
                    for ($i = 0; $i < 6; $i++) {
                        $new_password .= $characters[rand(0, strlen($characters) - 1)];
                    }
                    $this->request->data['User']['password'] = $new_password;

                    $this->User->save($this->request->data);
                    /* Sending Email to user */
                    $email = $user_data['User']['email'];
					
					$html = 'Hi,';
					$html .= '<p>Your Password has been reset Successfully</p>';
					$html .= '<p>Your Login id : ' . $user_data['User']['email'] . '</p>';
					$html .= '<p>Your Password is : ' . $new_password . '</p>';
					$html .= '<p>Please login using the following link :</p>';
					$html .= '<p><a href="http://'. $this->request->host() . '' . $this->webroot.'admin/">Login Now</a></p>';
					$html .= '<br/>';			  
					$html .= '<p>Regards,</p>';
					$html .= '<p><strong>FairPockets Team<strong></p>';
					
                    $Email = new CakeEmail();
                    //$Email->from(array('admin@FairPockets.com' => 'FairPockets'));
                    $Email->to($email);
					$Email->emailFormat('html');
                    $Email->subject('Forgot Password-FairPockets');
                    $output = $Email->send($html);

                    /* Sending Email to user */
                    if ($output) {
                        $this->Session->setFlash('Password has been changed, Check Your Mail', 'default', array('class' => 'green'));
                        $this->redirect(array('controller' => 'users', 'action' => 'login'));
                        //echo json_encode(array('status' => 'success', 'message' => "Password has been changed , please check your email")); die;
                    } else {
                        $this->Session->setFlash('Password has been changed ', 'default', array('class' => 'green'));
                        $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                } else {
                    $this->Session->setFlash('Email Not Exist', 'default', array('class' => 'red'));
                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            }
        }
    }

}

?>