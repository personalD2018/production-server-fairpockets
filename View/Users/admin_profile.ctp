<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>		
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate({
            rules: {
                mobile: {
                    required: true
                }
            }

        })
    });

</script>		
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">My Profile</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Profile - Edit/View</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('User', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">User Name</label>
                            <div class="controls">

                                <?php echo $this->Form->input('username', array('div' => false, 'label' => false, 'value' => $profile_data['User']['username'], 'disabled' => 'disabled', 'class' => 'small required')); ?>

                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Profile Picture</label>
                            <div class="controls">
                                <?php echo $this->Form->file('profile_pic', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'fileupload')); ?>
                                <?php if ($this->request->data['User']['profile_pic'] != '') { ?>
                                    <img src="<?php echo $this->webroot; ?>upload/profile_pic/<?php echo $this->request->data['User']['profile_pic']; ?>" alt="thumb" width="60" height="40">
                                    <?php
                                    echo $this->Form->hidden('profile_pic_edit', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'hidden', 'value' => $this->request->data['User']['profile_pic']));
                                }
                                ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Mobile</label>
                            <div class="controls">
<?php echo $this->Form->input('mobile', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Mobile', 'class' => 'small required number')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
<?php echo $this->Form->input('address', array('type' => 'textarea', 'class' => 'small required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter Address')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Employee ID</label>
                            <div class="controls">
<?php echo $this->Form->input('empId', array('div' => false, 'label' => false, 'value' => $profile_data['User']['empId'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
<?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'value' => $profile_data['User']['email'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Department</label>
                            <div class="controls">
<?php echo $this->Form->input('dname', array('div' => false, 'label' => false, 'value' => $profile_data['EmployeeDepartment']['departmentname'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>


                        </div>
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">

<?php echo $this->Form->input('role', array('div' => false, 'label' => false, 'value' => $profile_data['EmployeeRole']['role'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>

                        </div>
                        <div class="control-group">
                            <label class="control-label">Manager</label>
                            <div class="controls" id="manger_Data" >
<?php echo!empty($profile_data['Userextra']['username']) ? $profile_data['Userextra']['username'] : 'No Manger'; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Date Of Joining</label>

                            <div class="controls">
<?php echo $this->Form->input('doj', array('div' => false, 'label' => false, 'value' => $profile_data['User']['doj'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>


                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update Profile</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               
