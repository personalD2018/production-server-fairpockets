<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Employees</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Employees</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Name</th>
                                <th class="center">Profile Pic</th>
                                <th class="center">Emp Id</th>
                                <th class="center">Department</th>
                                <th class="center">Role</th>
                                <th class="center">Manager</th>

                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($users as $employee) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $employee['User']['username']; ?></td>
                                    <td class="center"><?php if (!empty($employee['User']['profile_pic'])) { ?><img src="<?php echo $this->webroot; ?>/upload/profile_pic/<?php echo $employee['User']['profile_pic']; ?>" alt="thumb" width="60" height="40"><?php } else { ?><img src="<?php echo $this->webroot; ?>/backend/images/no_user_image.jpg" alt="thumb" width="60" height="40"><?php } ?>

                                    </td>
                                    <td class="center"><?php echo $employee['User']['empId']; ?></td>

                                    <td class="center"><?php echo $employee['MstEmployeeDepartment']['departmentname']; ?></td>
                                    <td class="center"><?php echo $employee['MstEmployeeRole']['role']; ?></td>
                                    <td><?php echo $employee['Userextra']['username']; ?></td>

                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/users/editEmployee/<?php echo $employee['User']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a href="<?php echo $this->webroot; ?>admin/users/viewemployee/<?php echo $employee['User']['id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail" onclick="window.location.href = '<?php echo $this->webroot; ?>admin/users/viewemployee/<?php echo $employee['User']['id']; ?>'"><i class="icon-eye-open"></i></button></a>
                                                <?php
                                                if ($employee['User']['is_valid'] == '1') {
                                                    ?>
                                                    <button class="btn btn-success" title="Current Employee"><i class=" icon-ok"></i></button>

                                                    <?php
                                                } else {
                                                    ?>
                                                    <button class="btn btn-inverse" title="Suspend Employee"><i class=" icon-remove-sign"></i></button>
                                                    <?php }
                                                    ?>


                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


