<?php

if (!empty($manager)) {
    echo $this->Form->input('User.parent_id', array('id' => 'manager', 'options' => $manager, 'empty' => 'Select', 'class' => 'small validate[required]', 'title' => 'Please Select Manager', 'label' => false, 'hiddenfield' => false));
} else {
    echo $this->Form->input('User.parent_id', array('id' => 'manager', 'options' => array(0 => 'No Manger'), 'empty' => 'Select', 'class' => 'small validate[required]', 'title' => 'Please Select Manager', 'label' => false, 'hiddenfield' => false));
}
?>

