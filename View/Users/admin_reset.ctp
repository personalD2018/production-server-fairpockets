<style>
    .red {
        background-color: red;
        text-align: left;
        padding: 2px 1px 1px 20px;
    }
    .green {
        color: white;
        padding: 2px 1px 1px 20px;
    }
</style>   
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Password</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Change Password</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php
                        echo $this->Form->create('User');
                        echo $this->Form->input('id');
                        ?>

                        <div class="control-group">
                            <label class="control-label">Old Password</label>
                            <div class="controls">
                                <?php echo $this->Form->input('old_password', array('type' => 'password', 'label' => false)); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">New Password</label>
                            <div class="controls">
                                <?php echo $this->Form->input('password', array('type' => 'password', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Repeat New Password</label>
                            <div class="controls">
                                <?php echo $this->Form->input('repassword', array('type' => 'password', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <?php echo $this->Form->hidden('id', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'hidden', 'value' => $this->Session->read('Auth.User.id'))); ?>	
                            <button type="submit" class="btn btn-primary">Change Password</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				
