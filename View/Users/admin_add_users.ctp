<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate({
            rules: {
                mobile: {
                    required: true
                }
            }

        })
    });
</script>	

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Users</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Add FP Users</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('Websiteuser', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">User Name</label>
                            <div class="controls">
                                <?php echo $this->Form->input('username', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter User Name', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Organization</label>
                            <div class="controls">
                                <?php echo $this->Form->input('userorgname', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Organization Name', 'class' => 'small required')); ?>

                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <?php echo $this->Form->input('password', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Password', 'class' => 'small required')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Mobile</label>
                            <div class="controls">
                                <?php echo $this->Form->input('usermobile', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Mobile', 'class' => 'small required number')); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Email', 'class' => 'small required email', 'title' => 'Please Enter Email')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">
                                <?php echo $this->Form->input('userrole', array('id' => 'role', 'options' => array('1' => 'Owner', '2' => 'Broker', '3' => 'Builder'), 'empty' => 'Select', 'class' => 'required', 'title' => 'Please Select Role', 'label' => false)); ?>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Add User</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>					














