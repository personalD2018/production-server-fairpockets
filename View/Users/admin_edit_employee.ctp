<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>
<script>
    $(document).ready(function () {
        $(".get_manger").on('change', function () {
            var department = $("#department").val();
            var role = $("#role").val();
            $("#manager").find('option').remove();
            if (department != '' && role != '') {


                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "users", "action" => "getManger")); ?>',
                    data: {department: department, role: role},
                    cache: false,
                    success: function (html) {

                        $('#manger_Data').html(html);
                    }
                })
            }
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate({
            rules: {
                mobile: {
                    required: true
                }
            }

        })
    });

    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
    });
</script>	

<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Employees</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Update FP Employee</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('User', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">User Name</label>
                            <div class="controls">
                                <?php echo $this->Form->input('username', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter User Name', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Employee ID</label>
                            <div class="controls">
                                <?php echo $this->Form->input('empId', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'title' => 'Please Enter Employee ID', 'class' => 'small required')); ?>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Profile Picture</label>
                            <div class="controls">
                                <?php echo $this->Form->file('profile_pic', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'fileupload')); ?>
                                <?php if ($this->request->data['User']['profile_pic'] != '') { ?>
                                    <img src="<?php echo $this->webroot; ?>upload/profile_pic/<?php echo $this->request->data['User']['profile_pic']; ?>" alt="thumb" width="60" height="40">
                                    <?php
                                    echo $this->Form->hidden('profile_pic_edit', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'hidden', 'value' => $this->request->data['User']['profile_pic']));
                                }
                                ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Mobile</label>
                            <div class="controls">
<?php echo $this->Form->input('mobile', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Mobile', 'class' => 'small required number')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
<?php echo $this->Form->input('address', array('type' => 'textarea', 'class' => 'small required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter Address')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
<?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Email', 'class' => 'small required email', 'title' => 'Please Enter Email')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Department</label>
                            <div class="controls">
<?php echo $this->Form->input('employee_department_id', array('id' => 'department', 'options' => $employeedepartment, 'empty' => 'Select', 'class' => 'chzn-select small get_manger required', 'title' => 'Please Select Department', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">
<?php echo $this->Form->input('employee_role_id', array('id' => 'role', 'options' => $employeerole, 'empty' => 'Select', 'class' => 'chzn-select small get_manger required', 'title' => 'Please Select Role', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Manager</label>
                            <div class="controls" id="manger_Data" >
<?php echo $this->Form->input('parent_id', array('id' => 'manager', 'empty' => 'Select', 'class' => 'chzn-select ', 'title' => 'Please Select Manager', 'label' => false, 'options' => $manager, 'hiddenfield' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Date Of Joining &nbsp;</label>

                            <div id="datetimepicker4" class="input-append">
<?php echo $this->Form->input('doj', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Date of Joining', 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Current Employee</label>
                            <div class="controls">
<?php echo $this->Form->input('is_valid', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox', 'hiddenfield' => false)); ?>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update Employee</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               