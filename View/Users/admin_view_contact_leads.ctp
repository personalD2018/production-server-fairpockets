<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Leads</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Contact Leads</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>
                                <th class="center">Name</th>
                                <th class="center">Email</th>
                                <th class="center">Mobile</th>
                                <th class="center">Subject</th>
                                <th class="center">Message</th>
								<th class="center">Dated</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($contactData as $data) {
                                ?>
                                <tr>

                                    

                                    <td class="center"><?php echo $data['Contact']['name']; ?></td>
                                    <td class="center"><?php echo $data['Contact']['email']; ?></td>
                                    <td class="center"><?php echo $data['Contact']['mobile']; ?></td>
									<td class="center"><?php echo $data['Contact']['subject']; ?></td>
									<td class="center"><?php echo $data['Contact']['message']; ?></td>									
									<td class="center"><?php echo $data['Contact']['created']; ?></td>
                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">                                                
                                                <a href="<?php echo $this->webroot; ?>admin/users/deleteContact/<?php echo $data['Contact']['id'];  ?>">
                                                    <button class="btn btn-inverse" title="Delete"><i class=" icon-remove-sign"></i></button>
                                                </a>

                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


