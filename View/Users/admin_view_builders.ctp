<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Builders</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Builders List</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Id</th>
								<th class="center">Username</th>
                                <th class="center">Email</th>
								<th class="center">Password</th>
                                <th class="center">Mobile</th>
                                <th class="center">Organization </th>
								<th class="center">Role</th>
								<th class="center">Active</th>
								<th class="center">Dated</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($usersData as $data) {
                                ?>
                                <tr>
									<td class="center"><?php echo $data['Websiteuser']['id']; ?></td>
                                    <td class="center"><?php echo $data['Websiteuser']['username']; ?></td>
                                    <td class="center"><?php echo $data['Websiteuser']['email']; ?></td>
									<td class="center"><?php echo $data['Websiteuser']['password_view']; ?></td>
                                    <td class="center"><?php echo $data['Websiteuser']['usermobile']; ?></td>
									<td class="center"><?php echo $data['Websiteuser']['userorgname']; ?></td>
									<td class="center"><?php echo $this->Number->getUserRoleByUserId($data['Websiteuser']['userrole']); ?></td>
									<td class="center"><?php echo $data['Websiteuser']['useractive']; ?></td>
									<td class="center"><?php echo $data['Websiteuser']['acccreatedate']; ?></td>
                                    
                                                                                   
									<td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <!--<a target="_blank" href="<?php echo $this->webroot; ?>properties/editproperty/<?php echo base64_encode($data['Websiteuser']['id']) ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a target="_blank" href="<?php echo $this->webroot; ?>properties/view_detail_builder1/<?php echo $data['Websiteuser']['id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail" ><i class="icon-eye-open"></i></button></a>-->
                                               <!-- <a href="<?php echo $this->webroot; ?>admin/properties/propertyApproveAction/<?php echo $data['Property']['property_id']; ?>">			
                                                    <button class="btn btn-success" title="Approved"><i class=" icon-ok"></i></button>
                                                </a> -->
												<?php if ($data['Websiteuser']['useractive'] != 1){ ?>
													<a href="<?php echo $this->webroot; ?>admin/users/builderApproveAction/<?php echo $data['Websiteuser']['id']; ?>">			
														<button class="btn btn-warning" title="Unapprove"><i class="icon-thumbs-down"></i></button>
													</a>
												<?php }else{ ?>
													<a href="<?php echo $this->webroot; ?>admin/users/builderApproveAction/<?php echo $data['Websiteuser']['id']; ?>">			
														<button class="btn btn-success" title="Approved"><i class="icon-thumbs-up"></i></button>
													</a>
												<?php } ?>
                                               <!-- <a href="<?php echo $this->webroot; ?>admin/properties/propertyreject/<?php echo $data['Websiteuser']['id']; ?>">
                                                    <button class="btn btn-inverse" title="Reject"><i class=" icon-remove-sign"></i></button>
                                                </a>-->
                                               <!-- <a href="#">
                                                    <button class="btn btn-inverse" title="Reject"><i class=" icon-remove-sign"></i></button>
                                                </a> -->

                                            </div>
                                        </div></td>

                                        

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


