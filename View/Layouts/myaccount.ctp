<div class="btn-group myaccountbtn" id="firstLevelNav_small">
    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-user-o" aria-hidden="true"></i> Hi <?php echo $this->Session->read('Auth.Websiteuser.username'); ?> <span class="caret"><!-- --></span>
    </button>
    <ul class="dropdown-menu pull-right dropdown-wide">
        <li><a href="<?php echo $this->webroot; ?>accounts/dashboard/">My Account</a></li>

        <?php /* if(@$status!='3' && $this->Session->read('Auth.Websiteuser.userrole')=='3'){?>
          <?php }
          else
          {?>
          <li><a href="<?php echo $this->webroot; ?>accounts/dashboard">My Account</a></li>
          <?php } */
        ?>
        <li><a href="<?php echo $this->webroot; ?>websiteusers/changePassword/">Change Password</a></li>
        <li><a href="#">Contact Us</a></li>
        <li><a href="#">Help</a></li>
        <li class="divider"></li>

        <li><a href="<?php echo $this->webroot; ?>websiteusers/logout/">Log Off</a></li>
    </ul>
</div>