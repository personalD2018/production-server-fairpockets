<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Fair Pockets');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <?php
        echo $this->Html->meta(
                'viewport', //name
                'width=device-width, initial-scale=1.0'//content
        );
        ?>

        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title'); ?>
             <?php echo "homelinks"; ?>
        </title>

        <?php
        echo $this->Html->meta('favicon.ico', 'home/res_icon.png', array('type' => 'icon'));
        echo $this->Html->css('home/bootstrap.min');
        echo $this->Html->css('home/font-awesome.min');
        echo $this->Html->css('home/animate.min');
        echo $this->Html->css('home/prettyPhoto');
        echo $this->Html->css('front/home');
        echo $this->Html->css('home/homecss');
        echo $this->Html->css('home/responsive');
        echo $this->Html->css('home/bootstrap-multiselect');
        echo $this->Html->css('home/drawer');
        ?>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel="stylesheet">
        <?php
        echo $this->Html->script('front/jquery');
        echo $this->Html->script('front/bootstrap.min');
        echo $this->Html->script('front/bootstrap-multiselect');
        ?>
        <!--[if lt IE 9]>
        <?php
        echo $this->Html->script('home/html5shiv');
        echo $this->Html->script('home/respond.min');
        ?>
     <![endif]-->


        <style>
            span.multiselect-native-select select{visibility:hidden;display:none;}
        </style>


        <?php
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <style>
            span.multiselect-native-select select{visibility:hidden;display:none;}
        </style>  
    </head>

    <body class="home">
        <header id="header">
            <nav class="navbar navbar-inverse" role="banner">
                <div class="row no-margin">
                    <div class="col-xs-12 col-sm-12">
                        <div class="navbar-header">
                            <!--  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                             <span class="sr-only">Toggle navigation</span>
                                               <img src="<?php echo $this->webroot; ?>img/front/res_icon.png"/>
                             </button> -->
                            <a class="navbar-brand" href="<?php echo $this->webroot; ?>"><img src="<?php echo $this->webroot; ?>img/front/logo.png" alt="logo" class="my-logo">
                            </a>




                            <script>
                                function showlocation() {
                                    document.getElementById("mylocations").classList.toggle("show");
                                }


                                window.onclick = function (event) {
                                    if (!event.target.matches('.dropbtn')) {

                                        var dropdowns = document.getElementsByClassName("dropdown-content");
                                        var i;
                                        for (i = 0; i < dropdowns.length; i++) {
                                            var openDropdown = dropdowns[i];
                                            if (openDropdown.classList.contains('show')) {
                                                openDropdown.classList.remove('show');
                                            }
                                        }
                                    }
                                }
                            </script>



                        </div>
                        <div class="drawer drawer--right">
                            <button type="button" class="drawer-toggle drawer-hamburger">
                                <span class="sr-only">toggle navigation</span>
                                <span class="drawer-hamburger-icon"></span>
                            </button>
                            <?php echo $this->element('navigation'); ?>

                        </div>


                        <div class="navbar-right">
                            <?php
                            if (empty($this->Session->read('Auth.Websiteuser'))) {
                                ?>
                                <ul class="nav navbar-nav mobile-hide">

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <i class="fa fa-caret-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo $this->webroot; ?>research-reports">Research Reports</a>
                                            </li>

                                            <li><a href="<?php echo $this->webroot; ?>property-management">Property Management</a>
                                            </li>
                                            <li><a href="<?php echo $this->webroot; ?>portfolio-management">Portfolio Management</a>
                                            </li>

                                            <li><a href="<?php echo $this->webroot; ?>advisory"> Assisted buying/selling</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            <?php } ?>

                            <div class="addpostlink mobile-hide">
                                <?php
                                if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) {
                                    ?>

                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button" >Sell Your Property</a>

                                <?php }
                                ?>
                            </div>

                            <div class="LoginApp">
                                <?php
                                if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) {
                                    ?>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button">Log in / Register</a>
                                <?php }
                                ?>

                                <?php echo $this->element('login'); ?>
                            </div>
                            <?php
                            if (!empty($this->Session->read('Auth.Websiteuser'))) {
                                echo $this->element('myaccount');
                            }
                            ?>

                            <?php if (empty($this->Session->read('Auth.Websiteuser')) && !empty($this->Session->read('Auth.User'))) {
                                ?>
                                <a href="<?php echo $this->webroot; ?>admin"><div class="btn-group myaccountbtn" id="firstLevelNav_small">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" >
                                            <i class="fa fa-user-o" aria-hidden="true"></i> Hi <?php echo $this->Session->read('Auth.User.username'); ?>
                                    </div></a>
                            <?php } ?>
                            <!--/login-app-->

                        </div>
                    </div><!--./col-->
                </div>
                <!--/.row-->
            </nav>
            <!--/nav-->
        </header>
        <!--/header-->

        <div id="content">

            <script>
                function check_uncheck_checkbox1(isChecked) {
                    if (isChecked) {
                        $('input[name="Residential"]').each(function () {
                            this.checked = true;
                        });
                    } else {
                        $('input[name="Residential"]').each(function () {
                            this.checked = false;
                        });
                    }
                }

                function check_uncheck_checkbox2(isChecked) {
                    if (isChecked) {
                        $('input[name="Commercial"]').each(function () {
                            this.checked = true;
                        });
                    } else {
                        $('input[name="Commercial"]').each(function () {
                            this.checked = false;
                        });
                    }
                }


                $(document).ready(function () {
                    $(".select-property-type").click(function () {
                        $(".property-checklist").slideToggle("fast");
                    });

                });

                $(document).mouseup(function (e)
                {
                    var container = $(".property-checklist");
                    var container2 = $(".select-property-type");

                    if ((!container.is(e.target) && container.has(e.target).length === 0) && (!container2.is(e.target) && container2.has(e.target).length === 0))
                    {
                        $(".property-checklist").slideUp("fast");
                    }
                });


            </script>

            <div class="top-search">
                <div class="search-form">
                    <li class="property">
                        <select>
                            <option>Rent</option>
                            <option>Sell</option>
                            <option>Project</option>
                        </select>
                    </li>
                    <li class="property-type">
                        <div class="select-property-type">Select Property Type</div>

                        <!--                <input type="text" id="result">-->            	
                        <div class="property-checklist">
                            <div id="frmCheclAll1">
                                <div id="divCheckAll1">
                                    <input type="checkbox" name="checkall" id="checkall1" onClick="check_uncheck_checkbox1(this.checked);" /><strong>All Residential</strong></div>
                                <div id="divCheckboxList1">
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Residential" id="Residential1" value="Apartment" />Apartment
                                    </div>
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Residential" id="Residential2" value="Studio Spartment" />Studio Spartment
                                    </div>
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Residential" id="Residential3" value="Residential Land" />Residential Land
                                    </div>
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Residential" id="Residential4" value="Independent Builder Floor" />Independent Builder Floor
                                    </div>
                                </div>
                            </div>  

                            <div id="frmCheclAll2">
                                <div id="divCheckAll2">
                                    <input type="checkbox" name="checkall" id="checkall2" onClick="check_uncheck_checkbox2(this.checked);" /><strong>All Commercial</strong></div>
                                <div id="divCheckboxList2">
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Commercial" id="Commercial1" value="Commercial office space" />Commercial office space
                                    </div>
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Commercial" id="Commercial2" value="Office in IT Park" />Office in IT Park
                                    </div>
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Commercial" id="Commercial3" value="Commercial shop" />Commercial shop
                                    </div>
                                    <div class="divCheckboxItem">
                                        <input type="checkbox" name="Commercial" id="Commercial4" value="showroom" />showroom
                                    </div>
                                </div>
                            </div>

                        </div>
                    </li>

                    <li class="property-details">
                        <input type="text" name="proname" class="form-control sea_loc_field" placeholder="Search for Property" />
                    </li>
                    <li class="property-button">
                        <button type="button">Search</button>
                    </li>
                </div>
            </div>

            <?php echo $this->Flash->render(); ?>

            <?php echo $this->fetch('content'); ?>
        </div>


        <?php echo $this->element('footer'); ?>
        <!--/#footer-->
        <?php
        echo $this->Html->script('front/jquery.prettyPhoto');
        echo $this->Html->script('front/jquery.isotope.min');
        echo $this->Html->script('front/main');
        echo $this->Html->script('front/wow.min');
        ?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js"></script>
        <?php
        echo $this->Html->script('front/drawer');
        ?>

        <script>
                                        $(document).ready(function () {
                                            $('.drawer').drawer();
                                        });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("li.drop-accordian a:first").bind("click", function (e) {
                    $(this).next('ul').slideToggle();
                    e.stopPropagation();
                });

                $('#firstLevelNav_small').on('hidden.bs.dropdown', function () {
                    $(this).find('ul.drop-accordian-menu').hide();
                });
            });
        </script>

        <!--more less-->
        <script>
            $(document).ready(function () {
                // Configure/customize these variables.
                var showChar = 90;  // How many characters are shown by default
                var ellipsestext = "...";
                var moretext = "Show more";
                var lesstext = "Show less";


                $('.more').each(function () {
                    var content = $(this).html();

                    if (content.length > showChar) {

                        var c = content.substr(0, showChar);
                        var h = content.substr(showChar, content.length - showChar);

                        var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span> &nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                        $(this).html(html);
                    }

                });

                $(".morelink").click(function () {
                    if ($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });
            });
        </script>	  

    </body>
</html>
