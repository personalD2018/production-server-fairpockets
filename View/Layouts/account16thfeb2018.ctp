<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Fair Pockets');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        echo $this->Html->charset();
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'));
        ?>

        <title>
             <?php echo isset($title) ? $title : "India's first price curated property portal -- Buy/Sell/Rent at fair Price";?>
        </title>

        <?php
			echo $this->Html->meta('favicon.ico', 'img/favicon.jpg', array('type' => 'icon'));
            echo $this->Html->css('home/bootstrap.min');
            echo $this->Html->css('home/font-awesome.min');
            echo $this->Html->css('home/animate.min');
            echo $this->Html->css('home/prettyPhoto');
            echo $this->Html->css('front/main');
            echo $this->Html->css('front/home');
            echo $this->Html->css('home/homecss');
            echo $this->Html->css('home/responsive');
            echo $this->Html->css('home/drawer');
            echo ($this->params['action'] != 'research_reports') ? $this->Html->css('home/myaccount') : '';
            echo $this->Html->css('home/bootstrap-datetimepicker.min');
            //echo $this->Html->css('home/bootstrap-select.min.css');
			echo $this->Html->css('home/bootstrap-multiselect');
            echo ($this->params['action'] == 'research_detail') ? $this->Html->css('home/slider.min') : '';
            echo $this->Html->script('front/jquery');
            echo $this->Html->script('front/bootstrap.min');
            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107575141-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-107575141-1');
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    </head>

    <body class="property_detail" ng-app="myNoteApp" ng-controller="myNoteCtrl">
        <header id="header">
            <nav class="navbar navbar-inverse" role="banner">
                 <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="<?php echo $this->webroot; ?>">
                             <img src="<?php echo $this->webroot; ?>img/front/logo.png" alt="Fairpockets" class="my-logo">
                         </a>
                        </div>
						<?php if($this->params['action'] == 'research_reports'): ?>
						 <?php echo $this->element('location'); ?>
						<?php endif; ?>
                        <div class="navbar-right">
                            <div class="addpostlink hidden-xs">
                                <!--<a href="add_property.html">Sell Your Property</a>-->
                            </div>
                            <?php
                                if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) {
                                    ?>
                            <div class="LoginApp">
                                
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button">Log in / Register</a>

                                    <?php echo $this->element('login'); ?>
                                
                            </div>
                            <?php } ?>

                            <?php
                                if (!empty($this->Session->read('Auth.Websiteuser'))) {
                                    echo $this->element('myaccount');
                                }
                            ?>

                            <?php if (empty($this->Session->read('Auth.Websiteuser')) && !empty($this->Session->read('Auth.User'))) {
                                ?>
                                <a href="<?php echo $this->webroot; ?>admin"><div class="btn-group myaccountbtn" id="firstLevelNav_small">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" >
                                            <i class="fa fa-user-o" aria-hidden="true"></i> Hi <?php echo $this->Session->read('Auth.User.username'); ?>
                                    </div></a>
                            <?php } ?>
                            <!--/login-app-->

                        </div>
                        <div class="drawer drawer--right drawer-close visible-xs visible-sm" style="display:none;overflow: auto;">
                            <button type="button" class="drawer-toggle drawer-hamburger">
                                <span class="sr-only">toggle navigation</span>
                                <span class="drawer-hamburger-icon"></span>
                            </button>
                            <?php echo $this->element('user-navigation'); ?>
                        </div>
                        <?php if($this->params['action'] == 'research_detail'): ?>
						<div class="mobile-search hide visible-xs">
							<?php echo $this->element('mobile-search');?>
						</div>
						<?php endif; ?>
                    </div><!--./col-->
               
            </nav>
            <!--/nav-->

        </header>

        <!--/header-->
        <div class="clearfix"></div>
        <div id="content">
			<div class="container">
				<div class="row">
					<div class="col-md-3 page-sidebar hidden-sm hidden-xs" role="navigation">
						<?php if(!empty($this->Session->read('Auth.Websiteuser'))): ?>
						<?php echo $this->element('account_sidebar'); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-9 col-xs-12">
						<?php
							echo $this->Flash->render();
							echo $this->fetch('content');
						?>
					</div>
				</div>
			<div class="space-l"></div>
			</div>
		</div>
        <div class="clearfix"></div>
		<?php 
            echo $this->element('footer');
            echo $this->Html->script('front/bootstrap-submenu.js');
			//echo $this->Html->script('front/bootstrap-select.min');
			echo $this->Html->script('front/bootstrap-multiselect');
            echo $this->Html->script('front/jquery.prettyPhoto');
            echo $this->Html->script('front/jquery.isotope.min');
            echo $this->Html->script('front/main');
            echo $this->Html->script('front/wow.min');
            echo $this->Html->script('front/stickySidebar');
			echo $this->Html->script('front/bootstrap-datetimepicker');
            echo ($this->params['action'] == 'research_detail') ? $this->Html->script('front/slider.min') : '';
        ?>
        <script>
				if ($(window).width() > 760) {
					$('#dashboard_menus').stickySidebar({sidebarTopMargin: 55,footerThreshold: 100});
                }

        </script>
        <?php if($this->params['action'] == 'research_detail'): ?>
			<script>
				$(document).ready(function () {
					$("#slider").slider({delay: 1000, interval: 5000}); 
				});
			</script>
        <?php endif; ?>
        <!--[if lt IE 9]>
        <?php
            echo $this->Html->script('home/html5shiv');
            echo $this->Html->script('home/respond.min');
        ?>
        <![endif]-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js"></script>
        
        <?php 
			echo $this->Html->script('front/drawer');
        ?>
        
        <script>
            $(document).ready(function () {
                $('.drawer').drawer();
				
                $("li.drop-accordian a:first").bind("click", function (e) {
                    $(this).next('ul').slideToggle();
                    e.stopPropagation();
                });

                $('#firstLevelNav_small').on('hidden.bs.dropdown', function () {
                    $(this).find('ul.drop-accordian-menu').hide();
                });

            });
        </script>
		<?php echo $this->Html->script(['menu']); ?>
    </body>
</html>
