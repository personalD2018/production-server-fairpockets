<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Fair Pockets');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php 
			echo $this->Html->charset(); 
			echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'));
        ?>

         <title>
            <?php echo isset($title) ? $title : "India's first price curated property portal -- Buy/Sell/Rent at fair Price";?>
            
        </title>

         <?php
            echo $this->Html->meta('favicon.ico', 'img/favicon.jpg', array('type' => 'icon'));
            echo $this->Html->css('home/bootstrap.min');
            echo $this->Html->css('home/font-awesome.min');
            echo $this->Html->css('home/animate.min');
            echo $this->Html->css('home/prettyPhoto');
            echo $this->Html->css('front/main');
            echo $this->Html->css('front/home');
            echo $this->Html->css('home/homecss');
            echo $this->Html->css('home/responsive');
            echo $this->Html->css('home/drawer');
            echo $this->Html->css('home/owl.carousel');
            echo $this->Html->css('home/owl.theme.default');
            echo ($this->params['action'] == 'research_reports') ? $this->Html->css('home/myaccount') : '';
            echo $this->Html->css('home/bootstrap-multiselect');
            echo $this->Html->script('front/jquery');
            echo $this->Html->script('front/bootstrap.min');
        ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107575141-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-107575141-1');
		</script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet">
        <?php
            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
    </head>

    <body class="home">
       <header id="header">
            <nav class="navbar navbar-inverse" role="banner">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo $this->webroot; ?>">
                            <img src="<?php echo $this->webroot; ?>img/front/logo.png" alt="Fairpockets" class="my-logo">
                        </a>
                    </div>
					<?php echo $this->element('location'); ?>
                    <div class="drawer drawer--right">
                        <button type="button" class="drawer-toggle drawer-hamburger">
                            <span class="sr-only">toggle navigation</span>
                            <span class="drawer-hamburger-icon"></span>
                        </button>
                        <?php echo $this->element('navigation'); ?>

                    </div>

                    <div class="navbar-right">
                        <?php if (empty($this->Session->read('Auth.Websiteuser'))) { ?>
                        <ul class="nav navbar-nav hidden-xs hidden-sm">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <i class="fa fa-caret-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo $this->webroot; ?>research-reports/">Research Reports</a>
                                    </li>

                                    <li><a href="<?php echo $this->webroot; ?>property-management/">Property Management</a>
                                    </li>
                                    <li><a href="<?php echo $this->webroot; ?>portfolio-management/">Portfolio Management</a>
                                    </li>

                                    <li><a href="<?php echo $this->webroot; ?>advisory/"> Assisted buying/selling</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <?php } ?>
						
						 <?php if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) { ?>
                        <div class="addpostlink hidden-xs hidden-sm">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button" >Sell Your Property</a>
                        </div>
						<?php }  ?>
							<?php if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) { ?>
                        <div class="LoginApp">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button">Log in / Register</a>
                            <?php echo $this->element('login'); ?>
                        </div>
						<?php } ?>
                        <?php
                        if (!empty($this->Session->read('Auth.Websiteuser'))) {
                            echo $this->element('myaccount');
                        }
                        ?>

                        <?php if (empty($this->Session->read('Auth.Websiteuser')) && !empty($this->Session->read('Auth.User'))) {
                            ?>
                        <a href="<?php echo $this->webroot; ?>admin"><div class="btn-group myaccountbtn" id="firstLevelNav_small">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" >
                                    <i class="fa fa-user-o" aria-hidden="true"></i> Hi <?php echo $this->Session->read('Auth.User.username'); ?>
                            </div></a>
                        <?php } ?>
						
						<div class="mobile-search visible-xs" style="display:none">
							<?php echo $this->element('mobile-search');?>
						</div>
						
                    </div>
                </div>
            </nav>
        </header>
		<div class="clearfix"></div>
		<?php echo $this->element('top-search-filter');?>
		<div class="clearfix"></div>
        <div id="content">
            <?php echo $this->fetch('content'); ?>
        </div>

        <?php echo $this->element('footer'); ?>
        <!--/#footer-->

       <?php 
            echo $this->Html->script('front/jquery.prettyPhoto');
            echo $this->Html->script('front/jquery.isotope.min');
            echo $this->Html->script('front/main');
            echo $this->Html->script('front/wow.min');
            echo $this->Html->script('front/drawer');
            echo $this->Html->script('front/owl.carousel.min');
        ?>
        <!--[if lt IE 9]>
        <?php
            echo $this->Html->script('home/html5shiv');
            echo $this->Html->script('home/respond.min');
        ?>
        <![endif]-->
		
        <script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>


        <script>
            $(document).ready(function () {
                $('.drawer').drawer();
				
                $("li.drop-accordian a:first").bind("click", function (e) {
                    $(this).next('ul').slideToggle();
                    e.stopPropagation();
                });

                $('#firstLevelNav_small').on('hidden.bs.dropdown', function () {
                    $(this).find('ul.drop-accordian-menu').hide();
                });
				
				$(".list-group a.collapsed, .list-group a.active").click(function(){
					$('.list-group div.dropdown').removeClass("in").addClass('collapse');
				});
            });
        </script>
		<?php echo $this->Html->script(['menu']); ?>
		<?= $this->fetch('scriptBottom');?>
    </body>
</html>
