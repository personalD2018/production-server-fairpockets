
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title><?php echo isset($title) ? $title : "Fairpockets | India's No.1 Real Estate Property Portal - for Buy, Sell and Rent Properties";?></title>
		<!-- core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="assets/css/animate.min.css" rel="stylesheet">
		<link href="assets/css/prettyPhoto.css" rel="stylesheet">
		<link href="assets/css/main.css" rel="stylesheet">
		<link href="assets/css/homecss.css" rel="stylesheet">
		
		<link href="assets/css/responsive.css" rel="stylesheet">
		<link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel="stylesheet">
		<script src="assets/js/jquery.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
		<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
		<link href="assets/css/drawer.css" rel="stylesheet">  
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		
		<style>
			/* jssor slider arrow navigator skin 05 css */
			/*
			.jssora05l                  (normal)
			.jssora05r                  (normal)
			.jssora05l:hover            (normal mouseover)
			.jssora05r:hover            (normal mouseover)
			.jssora05l.jssora05ldn      (mousedown)
			.jssora05r.jssora05rdn      (mousedown)
			.jssora05l.jssora05lds      (disabled)
			.jssora05r.jssora05rds      (disabled)
			*/
			.jssora05l, .jssora05r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 40px;
            cursor: pointer;
            background: url('img/a17.png') no-repeat;
            overflow: hidden;
			}
			.jssora05l { background-position: -10px -40px; }
			.jssora05r { background-position: -70px -40px; }
			.jssora05l:hover { background-position: -130px -40px; }
			.jssora05r:hover { background-position: -190px -40px; }
			.jssora05l.jssora05ldn { background-position: -250px -40px; }
			.jssora05r.jssora05rdn { background-position: -310px -40px; }
			.jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
			.jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
			/* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
		</style>
	</head>
	<!--/head-->
	
	<div id="circle"></div>
	<body class="property_detail">
		<header id="header">
			<nav class="navbar navbar-inverse" role="banner">
				<div class="row no-margin">
					<div class="col-xs-12 col-sm-12">
						<div class="navbar-header">
							<!--  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<img src="assets/images/res_icon.png"/>
							</button> -->
							<a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" alt="logo" class="my-logo"></a>
							
							
							<div class="topsearchbox loc_dropdown">
								<button onClick="myFunction()" class="dropbtn">Delhi </button>
								<div id="mylocations" class="dropdown-content dropdown-locations">
									<ul>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
										<li><a href="#">Mumbai</a></li>
										<li><a href="#">Allahabad</a></li>
										<li><a href="#">Noida</a></li>
									</ul>
								</div>
							</div>
							
							<script>
								function myFunction() {
									document.getElementById("mylocations").classList.toggle("show");
								}
								
								
								window.onclick = function(event) {
									if (!event.target.matches('.dropbtn')) {
										
										var dropdowns = document.getElementsByClassName("dropdown-content");
										var i;
										for (i = 0; i < dropdowns.length; i++) {
											var openDropdown = dropdowns[i];
											if (openDropdown.classList.contains('show')) {
												openDropdown.classList.remove('show');
											}
										}
									}
								}
							</script>
							
							
							
						</div>
						<div class="drawer drawer--right">
							<button type="button" class="drawer-toggle drawer-hamburger">
								<span class="sr-only">toggle navigation</span>
								<span class="drawer-hamburger-icon"></span>
							</button>
							<nav class="drawer-nav" role="navigation">
								<a class="menulogo desktop-hide" href="index.html"><img src="assets/images/logo.png" alt="logo" class="my-logo"></a>
								<ul class="drawer-menu">
									<li><a class="drawer-brand" href="#">Home</a></li>
									<li><a class="drawer-menu-item" href="#">Properties List</a></li>
									<li><a class="drawer-menu-item" href="#">Tools</a></li>
									
									<li class="drawer-dropdown">
										<a class="drawer-menu-item" data-target="#" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
											Services <span class="drawer-caret"></span>
										</a>
										<ul class="drawer-dropdown-menu">
											<li><a class="drawer-dropdown-menu-item" href="research-reports.html">Research Reports</a></li>
											<li><a class="drawer-dropdown-menu-item" href="#">Transactions</a></li>
											<li><a class="drawer-dropdown-menu-item" href="#">Property Management</a></li>
											<li><a class="drawer-dropdown-menu-item" href="#">Portfolio Management</a></li>
											<li><a class="drawer-dropdown-menu-item" href="#">Check your deal</a></li>
											<li><a class="drawer-dropdown-menu-item" href="#">Assisted buying/selling</a></li>
										</ul>
									</li>
									
									<li><a class="drawer-menu-item" href="#">Pricing</a></li>
									<li><a class="drawer-menu-item" href="#">Blog</a></li>
									<li class="desktop-hide"><a class="drawer-menu-item" href="add_property.html">Sell Your Property</a></li>
									<li  class="desktop-hide"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="drawer-menu-item button"><i class="fa fa-user-o" aria-hidden="true"></i> Log in</a></li>
									
									
									
								</ul>
							</nav>
							
						</div>
						<div class="navbar-right">
							<ul class="nav navbar-nav">
								
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <i class="fa fa-caret-down"></i></a>
									<ul class="dropdown-menu">
										<li><a href="javascript:void(0)">Research Reports</a>
										</li>
										<li><a href="javascript:void(0)">Transactions</a>
										</li>
										<li><a href="javascript:void(0)">Property Management</a>
										</li>
										<li><a href="javascript:void(0)">Portfolio Management</a>
										</li>
										<li><a href="javascript:void(0)">Check your deal</a>
										</li>
										<li><a href="javascript:void(0)"> Assisted buying/selling</a>
										</li>
									</ul>
								</li>
								
							</ul>
							<div class="addpostlink"><a href="add_property.html">Post Free Property</a></div>
							<div class="LoginApp">
								<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button"><i class="fa fa-user-o" aria-hidden="true"></i> Log in</a>
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg modal-dial">
										<div class="modal-content">
											<div class="modal-body modalpadd">
												<div class="row">
													<div class="col-md-6 modulsideback">
														<div class="col-sm-12">
															<p>“Ab 42 lakh mein Gurgaon<br>
															mein 2BHK milega.”</p>
															<h4>#Mere<span>Budget</span>Mein</h4>
															<div class="modalbacklidiv" style="margin-top:53px;display:block;">
																<h3>Buy at your price</h3>
																<div class="col-sm-12">
																	<ul>
																		
																		<li><div class=" text-center sing-l-tet fl"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Negotiate Online</div></li>
																	</ul>
																</div>
																<div class="col-sm-12 ">
																	<ul>
																		
																		<li><div class=" text-center sing-l-tet fl"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Spam Free Experience</div></li>
																	</ul>
																</div>
																<div class="col-sm-12 ">
																	<ul>
																		
																		<li><div class=" text-center sing-l-tet fl"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Top Rated Buyers &amp; Sellers</div></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-6 loginsidediv" style="border-right: 1px dotted #C2C2C2;padding-right: 10px;">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															×</button>
															
														</div>
														<!-- Nav tabs -->
														<ul class="nav nav-tabs">
															<li class="active"><a href="#Login" data-toggle="tab">Login</a>
															</li>
															<li><a href="#Registration" data-toggle="tab">Registration</a>
															</li>
														</ul>
														<!-- Tab panes -->
														<div class="tab-content">
															<div class="tab-pane active" id="Login">
																<h2>Sign In</h2>
																<form role="form" class="form-horizontal fpform">
																	<div class="form-group">
																		<div class="col-sm-12">
																			<input type="email" class="form-control error" id="email1" placeholder="Email" required/>
																			<div class="error has-error" style="display:block;">Box must be filled out</div>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-12">
																			<input type="password" class="form-control" id="exampleInputPassword1" required placeholder="Password" />
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-6">
																			<input id="Remember" class="checkbox-custom" name="Remember" type="checkbox" >
																			<label for="Remember" class="checkbox-custom-label">Remember Me</label>
																		</div>
																		<div class="col-sm-6 text-right">
																			<a href="#Lostpassword" class="color-red" data-toggle="tab">Forgot your password?</a>
																		</div>
																	</div>
																	
																	<div class="form-group">
																		<div class="col-sm-6">
																			<button type="submit" class="btn btn-primary btn-red">
																			<i class="fa fa-sign-in"></i> &nbsp;Sign in</button>
																		</div>
																		
																	</div>
																</form>
															</div>
															<div class="tab-pane" id="Registration">
																<h2>Sign <span>up</span></h2>
																<form role="form" class="form-horizontal fpform">
																	<div class="form-group" style="">
																		<div class="col-sm-4">
																			<input id="usertype1" class="radio-custom" name="bachelor_all" type="radio" >
																			<label for="usertype1" class="radio-custom-label">Individual</label>
																			
																		</div>
																		<div class="col-sm-4">
																			<input id="usertype2" class="radio-custom" name="bachelor_all" type="radio" >
																			<label for="usertype2" class="radio-custom-label">Agent</label>
																			
																		</div>
																		<div class="col-sm-4">
																			<input id="usertype3" class="radio-custom" name="bachelor_all" type="radio" >
																			<label for="usertype3" class="radio-custom-label">Builder</label>
																			
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-12">
																			<input type="email" class="form-control error" placeholder="Name" />
																			<div class="error has-error" style="display:block;">Box must be filled out</div>
																		</div>
																	</div>
																	<div class="form-group" id="companyinput">
																		<div class="col-sm-12">
																			<input type="text" class="form-control" placeholder="Company Name" />
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-12">
																			<input type="email" class="form-control" id="email" placeholder="Email" />
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-12">
																			<input type="email" class="form-control" id="mobile" placeholder="Mobile" />
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-12">
																			<input type="password" class="form-control" id="password" placeholder="Password" />
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-12">
																			<span >By Signing up, I agree with the <a class="color-red" href="#">T&C1</a></span>
																			</div>
																		</div>
																		<div class="form-group signupbtn">
																			<div class="col-sm-12">
																				<button type="button" class="btn btn-primary btn-red">
																					<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Sign up
																				</button>
																				<a href="#SuccessMsg" class="btn btn-primary " data-toggle="tab">SuccessMSG</a>
																			</div>
																		</div>
																	</form>
																</div>
																<div class="tab-pane" id="Lostpassword">
																	<h2>Forgot Password</h2>
																	<form role="form" class="form-horizontal fpform">
																		<div class="form-group">
																			<div class="col-sm-12">
																				<input type="email" class="form-control" id="email1" placeholder="Email" required/>
																			</div>
																			
																		</div>
																		<div class="form-group">
																			<div class="col-sm-6">
																				<button type="submit" class="btn btn-primary btn-red">
																				<i class="fa fa-sign-in"></i> &nbsp;Send</button>
																			</div>
																			<div class="col-sm-6 text-right">
																				<a href="#Login" class="color-red"   data-toggle="tab">Login</a>
																			</div>
																		</div>
																	</form>
																</div>
																<div class="tab-pane text-center" id="SuccessMsg">
																	<h3 class="regsucs">Thank you for registering with us. </h3>
																	
																	
																	<div >
																		We have send you confirmation mail to your registered email id. Please click on the link to confirm your registration. 
																	</div>
																	
																	
																	
																	
																</div> 
															</div>
															<!-- <div id="OR" class="hidden-xs"> -->
															<!-- OR -->
															<!-- </div> -->
														</div>
														<!-- <div class="col-md-4"> -->
														<!-- <div class="row text-center sign-with"> -->
														<!-- <div class="col-md-12"> -->
														<!-- <h3> -->
														<!-- Sign in with -->
														<!-- </h3> -->
														<!-- </div> -->
														<!-- <div class="col-md-12"> -->
														<!-- <div class="btn-group btn-group-justified"> -->
														<!-- <a href="#" class="btn btn-primary">Facebook</a> <a href="#" class="btn btn-default"> -->
														<!-- Google</a> -->
														<!-- </div> -->
														<!-- </div> -->
														<!-- </div> -->
														<!-- </div> -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--/login-app-->
								
							</div>
						</div><!--./col-->
					</div>
					<!--/.row-->
				</nav>
				<!--/nav-->
			</header>
			<!--/header-->
			
			
			
			
			
			<!--./advertisement-Card-->
			
			
			<section class="no-margin main-section">
				<div class="property_detail_header col-sm-12 nopaddingside">
					<div class="container">
						<div class="detail_header col-sm-12 nopaddingside">
							<div class="col-sm-12 breadCrumbs ">
								<div class="crumb"><a href="">Home</a> > <a href="">Property in Noida</a> > Sector-107 Noida </div>
							</div>
							
							<!--   <div class="col-sm-1">
								<div class="circle">20%</div>
							</div> -->
							
							
							<div class="col-sm-4 header-price">
								
								<div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i>20.5 Lac <span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i>20% less</span> <span class="price_sq">@ 5,086 per Sq.Ft</span></div>
								<div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i>25.5 Lac @5,086 per Sq.Ft</div>
							</div>
							<div class="col-sm-5 text-center header-feature">
								
								<div class="h_feature">3 Bedroom 4 Baths <span class="h_feature_sub">Residential Apartment for Sale</span></div>
								<div class="h_location">in Sunworld Vanalika, Sector-107 Noida, Noida, U P </div>	
							</div>
							<div class="col-sm-3 text-center header-other-feature">
								
								<div class="h_postdate">Post on Feb 04, 2017</div>
								<div class="h_availability">Ready to move</div>	
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="no-padding-bottom">
						
						<div class=" property_detail_area col-sm-12 nopaddingside">
							
							<div class="col-sm-12 nopaddingside property_detail_inner">
								<div class="col-sm-9  property-images-main">
									<div class="row">
										
										
										<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
											<!-- Loading Screen -->
											<div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
												<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
												<div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
											</div>
											<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
												<a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
												<div>
													<img data-u="image" src="img/01.jpg" />
													<img data-u="thumb" src="img/thumb-01.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/02.jpg" />
													<img data-u="thumb" src="img/thumb-02.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/03.jpg" />
													<img data-u="thumb" src="img/thumb-03.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/04.jpg" />
													<img data-u="thumb" src="img/thumb-04.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/05.jpg" />
													<img data-u="thumb" src="img/thumb-05.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/06.jpg" />
													<img data-u="thumb" src="img/thumb-06.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/07.jpg" />
													<img data-u="thumb" src="img/thumb-07.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/08.jpg" />
													<img data-u="thumb" src="img/thumb-08.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/09.jpg" />
													<img data-u="thumb" src="img/thumb-09.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/10.jpg" />
													<img data-u="thumb" src="img/thumb-10.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/11.jpg" />
													<img data-u="thumb" src="img/thumb-11.jpg" />
												</div>
												<div>
													<img data-u="image" src="img/12.jpg" />
													<img data-u="thumb" src="img/thumb-12.jpg" />
												</div>
											</div>
											<!-- Thumbnail Navigator -->
											<div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
												<!-- Thumbnail Item Skin Begin -->
												<div data-u="slides" style="cursor: default;">
													<div data-u="prototype" class="p">
														<div class="w">
															<div data-u="thumbnailtemplate" class="t"></div>
														</div>
														<div class="c"></div>
													</div>
												</div>
												<!-- Thumbnail Item Skin End -->
											</div>
											<!-- Arrow Navigator -->
											<span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
											<span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
										</div>
										
										
										
										
									</div>
									<!-- basic detail -->
									<div id="detail" class="detail-list detail-block target-block">
										<div class="detail-title">
											<h2 class="title-left">Detail</h2>
											
											
											
										</div>
										<div class="">
											<ul class="list-three-col">
												<li><strong>Super Built Up Area:</strong> 152 Sq.ft</li><li><strong>Built Up Area:</strong> 150 Sq.ft</li>
												<li><strong>Carpet Area:</strong> 3410 Sq.ft</li>
												<li><strong>Plot area:</strong> 3410 Sq.ft</li>
												<li><strong>Length of plot:</strong> 3410 Sq.ft</li>
												<li><strong>Width of plot:</strong> 3410 Sq.ft</li>
												<li><strong>Bedrooms:</strong> 1</li>
												<li><strong>Bathrooms:</strong> 1</li>
												<li><strong>Balconies:</strong> 1</li>
												
											</ul>
										</div>
									</div>
									<div id="detail" class="detail-list detail-block target-block">
										<div class="detail-title">
											<h4 class="title-inner">Additional details</h4>
										</div>
										<ul class="list-three-col list_odetail">
											<li><strong>Project name:</strong> <span></span></li>
											<li><strong>Launch date:</strong> <span></span></li>  
											<li><strong>Area:</strong> <span></span></li>
											<li><strong>price:</strong> <span></span></li>  
											<li><strong>floor no:</strong> <span></span></li>
											<li><strong>overlooking:</strong> <span></span></li>  
											<li><strong>configuration:</strong> <span></span></li>
											
											<li><strong>address:</strong> <span></span></li>
											<li><strong>age of property:</strong> <span></span></li>
											<li><strong>Age of Property:</strong> <span>20</span></li>
											<li><strong>Furnishing:</strong> <span>Furnishing</span></li>
											<li><strong>Total floors:</strong> <span>1</span></li>
											<li><strong>Property on floor:</strong> <span>1</span></li>
											<li><strong>Floors in property:</strong> <span>1</span></li>
											<li><strong>Floors allowed for construction:</strong> <span>1</span></li>
											<li><strong>Reserved Parking:</strong> <span>1</span></li>
											<li><strong>No. of covered parking:</strong> <span>1</span></li>
											<li><strong>No. of open parking:</strong> <span>1</span></li>
											<li><strong>Other rooms:</strong> <span></span></li>  
											
											
											
											
											
											
										</ul>
									</div>
									<!-- basic detail end -->
									<div id="features" class="detail-features detail-block target-block">
										<div class="detail-title">
											<h2 class="title-left">Description</h2>
										</div>
										
										
										<div class="property_des">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</div>
										
										
										
									</div>
									<!-- feature -->
									<div id="features" class="detail-features detail-block target-block">
										<div class="detail-title">
											<h2 class="title-left">Features</h2>
										</div>
										<ul class="list-three-col list-features">
											<li><strong>Type of Flooring:</strong> Marble</li>
											<li><strong>Power Backup:</strong> None</li>
											<li><strong>Water Source:</strong> Municipal</li>
											<li><strong>Facing:</strong> North</li>
											<li><strong>Society Type:</strong> Society</li>
											<li><strong>Width of facing road:</strong> 45 Feet</li>
											<li><strong>Overlooking:</strong> Park/Garden</li>
											<li><strong>Bachelors Allowed:</strong> Yes</li>
											<li><strong>Pet allowed:</strong> Yes</li>
											<li><strong>Non vegetarian:</strong> No</li>
											<li><strong>Boundary wall:</strong> Yes</li>
											
										</ul>
									</div>
									<!-- feature end -->
									<!-- Amenities -->
									<div id="amenities" class="detail-amenities detail-block target-block col-md-12">
										<div class="detail-title">
											<h2 class="title-left">Amenities</h2>
										</div>
										<ul class=" list-amenities">
											
											<div class="">
												<div class="row">
													<div class ="col-md-3">
														<!--<div class= "customDiv">column1</div>-->
														<li><span class="icon_amen icon_lift">Lift</span></li>
														<li><span class="icon_amen icon_park">Park</span></li>
														<li><span class="icon_amen icon_staff">Maintenance Staff</span></li>
														<li><span class="icon_amen icon_parking">Visitor Parking</span></li>
														<li><span class="icon_amen icon_water_storage">Water Storage</span></li>
														<li><span class="icon_amen icon_vaastu">Feng shui/Vaastu compliant</span></li>
														
														
													</div>
													
													<div class ="col-md-3">
														<!-- <div class= "customDiv">column2</div>-->
														<li><span class="icon_amen icon_intercomm">Intercomm Facility</span></li>
														<li><span class="icon_amen icon_security">Security/Fire alarm</span></li>
														<li><span class="icon_amen icon_waste">Waste Disposal</span></li>
														<li><span class="icon_amen icon_tarrace">Private Garden/Terrace</span></li>
														<li><span class="icon_amen icon_securityp">Security Personnel</span></li>
														<li><span class="icon_amen icon_lift">Service/Goods Lift</span></li>
														
													</div>
													<div class ="col-md-3">
														<!-- <div class= "customDiv">column3</div>-->
														<li><span class="icon_amen icon_conroom">Conference Room</span></li>	
														<li><span class="icon_amen icon_pool">Swimming Pool</span></li>
														<li><span class="icon_amen icon_atm">ATM</span></li>
														<li><span class="icon_amen icon_airc">Centrally Airconditioned</span></li>
														<li><span class="icon_amen icon_internet">High Speed Internet</span></li>
														<li><span class="icon_amen icon_foodc">Cafeteria/Food court</span></li>
													</div>
													<div class ="col-md-3">
														<!-- <div class= "customDiv">column4</div>-->
														<li><span class="icon_amen icon_gym">Gymnasium</span></li>
														<li><span class="icon_amen icon_water_plant">Water Softening Plant</span></li>
														<li><span class="icon_amen icon_com_hall">Community Hall</span></li>
														<li><span class="icon_amen icon_shopping">Shopping Center</span></li>
														<li><span class="icon_amen icon_rainwater">Rain water harvesting</span></li>
														
													</div>
												</div>
												
											</ul>
										</div>
										<!-- Amenities end -->
										
									</div>
									<!-- sidebar -->
									<div id="sidebar" class="col-sm-3 nopaddingside detail_sidebar">
										<div class="col-sm-12  sidebar-inner">
											
											<form class="fpform">
												<div class="row">
													<h3>Contact - Fairpockets</h3>
													<div class="col-sm-12 col-xs-12">
														<div class="form-group">
															
															<input id="user1" class="radio-custom" name="pet_all" type="radio" >
															<label for="user1" class="radio-custom-label">Individual</label>
															
															
															
															<input id="user2" class="radio-custom" name="pet_all" type="radio" >
															<label for="user2" class="radio-custom-label">Dealer</label>
															
															
														</div>
													</div>
													<div class="col-sm-12 col-xs-12">
														<div class="form-group">
															<input class="form-control error" placeholder="Your Name" type="text">
															<div class="error has-error" style="display:block;" >Box must be filled out</div>
														</div>
													</div>
													<div class="col-sm-12 col-xs-12">
														<div class="form-group">
															<input class="form-control" placeholder="Phone" type="text">
														</div>
													</div>
													<div class="col-sm-12 col-xs-12">
														<div class="form-group">
															<input class="form-control" placeholder="Email" type="email">
														</div>
													</div>
													<div class="form-group col-sm-12">
														<label class="label_title">Interested in (Optional)</label>
														<script type="text/javascript">
															$(document).ready(function() {
																$('#protype4').multiselect({
																	enableClickableOptGroups: true
																});
															});
														</script>
														<select id="protype4" multiple="multiple">
															
															<option value="Residential-1" >Assisted Search </option>
															<option value="Residential-2" >Immediate Purchase</option>
															<option value="Residential-3" >Portfolio Management</option>
															
															
														</select>
														
														
														
														
														
														
														
														
														
													</div>
													
													
													
													
												</div>
												<div class="text-center col-sm-12 "><button class="btn btn-primary">Submit</button></div>
												
												<div class="text-center col-sm-12 " style="padding:10px 0px;">I agree with Fairpockets <a href="#">T&C2</a> </div>
													
												</form>
												
											</div>
										</div>
										<!-- sidebar end -->
									</div>
								</div>	
								<!-- similar property  -->
								
								
								<div class="">
									
									<div class="property-listing grid-view  col-md-12">
										<div class="detail-title">
											
											<h2 class="title-left">Similar properties</h2>
										</div>
										<div class="row">
											<div class="item-wrap">
												<div class="property-item table-list">
													<div class="table-cell">
														<div class="figure-block">
															<figure class="item-thumb">
																
																
																<div class="price hide-on-list">
																	
																	<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
																	<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
																</div>
																<a href="#" class="hover-effect">
																	<img src="assets/images/property/01_434x290.jpg" alt="thumb">
																</a>
																
															</figure>
														</div>
													</div>
													<div class="item-body table-cell">
														
														<div class="body-left table-cell">
															<div class="info-row">
																<div class="label-wrap hide-on-grid">
																	<div class="label-status label label-default">For Sale</div>
																	<span class="label label-danger">Sold</span>
																</div>
																<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
																<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
																<div class="pro_tt">Residential Apartment for Sale</div>
																
																<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
															</div>
															
															
														</div>
														<div class="body-right table-cell hidden-gird-cell">
															<div class="info-row price">
																<p class="price-start">Start from</p>
																<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
																<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
															</div>
															<div class="info-row phone text-right">
																<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
																<p><a href="#">+1 (786) 225-0199</a></p>
															</div>
														</div>
														<div class="table-list full-width hide-on-list">
															<div class="cell">
																
															</div>
															
														</div>
													</div>
												</div>
												<div class="item-foot date hide-on-list">
													<div class="item-foot-left">
														<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
													</div>
													<div class="item-foot-right">
														<p><i class="fa fa-calendar"></i> 12 Days ago </p>
													</div>
												</div>
											</div>
											
											<div class="item-wrap">
												<div class="property-item table-list">
													<div class="table-cell">
														<div class="figure-block">
															<figure class="item-thumb">
																
																
																<div class="price hide-on-list">
																	
																	<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
																	<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
																</div>
																<a href="#" class="hover-effect">
																	<img src="assets/images/property/01_434x290.jpg" alt="thumb">
																</a>
																
															</figure>
														</div>
													</div>
													<div class="item-body table-cell">
														
														<div class="body-left table-cell">
															<div class="info-row">
																<div class="label-wrap hide-on-grid">
																	<div class="label-status label label-default">For Sale</div>
																	<span class="label label-danger">Sold</span>
																</div>
																<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
																<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
																<div class="pro_tt">Residential Apartment for Sale</div>
																
																<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
															</div>
															
															
														</div>
														<div class="body-right table-cell hidden-gird-cell">
															<div class="info-row price">
																<p class="price-start">Start from</p>
																<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
																<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
															</div>
															<div class="info-row phone text-right">
																<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
																<p><a href="#">+1 (786) 225-0199</a></p>
															</div>
														</div>
														<div class="table-list full-width hide-on-list">
															<div class="cell">
																
															</div>
															
														</div>
													</div>
												</div>
												<div class="item-foot date hide-on-list">
													<div class="item-foot-left">
														<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
													</div>
													<div class="item-foot-right">
														<p><i class="fa fa-calendar"></i> 12 Days ago </p>
													</div>
												</div>
											</div>
											
											<div class="item-wrap">
												<div class="property-item table-list">
													<div class="table-cell">
														<div class="figure-block">
															<figure class="item-thumb">
																
																
																<div class="price hide-on-list">
																	
																	<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
																	<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
																</div>
																<a href="#" class="hover-effect">
																	<img src="assets/images/property/01_434x290.jpg" alt="thumb">
																</a>
																
															</figure>
														</div>
													</div>
													<div class="item-body table-cell">
														
														<div class="body-left table-cell">
															<div class="info-row">
																<div class="label-wrap hide-on-grid">
																	<div class="label-status label label-default">For Sale</div>
																	<span class="label label-danger">Sold</span>
																</div>
																<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
																<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
																<div class="pro_tt">Residential Apartment for Sale</div>
																
																<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
															</div>
															
															
														</div>
														<div class="body-right table-cell hidden-gird-cell">
															<div class="info-row price">
																<p class="price-start">Start from</p>
																<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
																<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
															</div>
															<div class="info-row phone text-right">
																<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
																<p><a href="#">+1 (786) 225-0199</a></p>
															</div>
														</div>
														<div class="table-list full-width hide-on-list">
															<div class="cell">
																
															</div>
															
														</div>
													</div>
												</div>
												<div class="item-foot date hide-on-list">
													<div class="item-foot-left">
														<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
													</div>
													<div class="item-foot-right">
														<p><i class="fa fa-calendar"></i> 12 Days ago </p>
													</div>
												</div>
											</div>
											
											<div class="item-wrap">
												<div class="property-item table-list">
													<div class="table-cell">
														<div class="figure-block">
															<figure class="item-thumb">
																
																
																<div class="price hide-on-list">
																	
																	<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
																	<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
																</div>
																<a href="#" class="hover-effect">
																	<img src="assets/images/property/01_434x290.jpg" alt="thumb">
																</a>
																
															</figure>
														</div>
													</div>
													<div class="item-body table-cell">
														
														<div class="body-left table-cell">
															<div class="info-row">
																<div class="label-wrap hide-on-grid">
																	<div class="label-status label label-default">For Sale</div>
																	<span class="label label-danger">Sold</span>
																</div>
																<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
																<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
																<div class="pro_tt">Residential Apartment for Sale</div>
																
																<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
															</div>
															
															
														</div>
														<div class="body-right table-cell hidden-gird-cell">
															<div class="info-row price">
																<p class="price-start">Start from</p>
																<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
																<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
															</div>
															<div class="info-row phone text-right">
																<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
																<p><a href="#">+1 (786) 225-0199</a></p>
															</div>
														</div>
														<div class="table-list full-width hide-on-list">
															<div class="cell">
																
															</div>
															
														</div>
													</div>
												</div>
												<div class="item-foot date hide-on-list">
													<div class="item-foot-left">
														<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
													</div>
													<div class="item-foot-right">
														<p><i class="fa fa-calendar"></i> 12 Days ago </p>
													</div>
												</div>
											</div>
											
											
											
											
										</div>
									</div>
								</div>
								<!-- similar property end -->		
							</div>
							
							
							
						</div>
						<!--/.container-->
						
					</section>
					
					
					
					<!-- Footor Section -->
					<section id="bottom">
						<div class="container wow fadeInDown" data-wow-duration="500ms" data-wow-delay="200ms">
							<div class="row">
								<div class="col-md-3 col-sm-6">
									<div class="widget">
										<h3>Company</h3>
										<ul>
											<li><a href="javascript:void(0);">About us</a></li>
											<li><a href="javascript:void(0);">Our Team</a></li>
											<li><a href="javascript:void(0);">Official Blog</a></li>
											<li><a href="javascript:void(0);">Career</a></li>
										</ul>
									</div>    
								</div><!--/.col-md-3-->
								
								<div class="col-md-3 col-sm-6">
									<div class="widget">
										<h3>Product &amp; Services</h3>
										<ul>
											<li><a href="javascript:void(0);">Research Reports</a></li>
											<li><a href="javascript:void(0);">Advisory (Buy/Sell)</a></li>
											<li><a href="javascript:void(0);">Property Management</a></li>
											<li><a href="javascript:void(0);">Portfolio Management</a></li>
										</ul>
									</div>    
								</div><!--/.col-md-3-->
								
								<div class="col-md-3 col-sm-6">
									<div class="widget">
										<h3>Legal &amp; Support</h3>
										<ul>
											<li><a href="javascript:void(0);">Feedback</a></li>
											<li><a href="javascript:void(0);">My Account</a></li>
											<li><a href="javascript:void(0);">Disclaimer</a></li>
											<li><a href="javascript:void(0);">Privacy &amp; Policy</a></li>
											<li><a href="javascript:void(0);">Terms &amp; Condition</a></li>
										</ul>
									</div>    
								</div><!--/.col-md-3-->
								
								<div class="col-md-3 col-sm-6">
									<div class="widget">
										<h3>Social Links</h3>
										<div class="social">
											<ul class="social-share">
												<li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
												<li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
												<li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li> 
												<li><a href="javascript:void(0);"><i class="fa fa-skype"></i></a></li>
											</ul>
											
											<ul class="list-inline pay-icons">
												<li><a href="javascript:void(0);"><i class="fa fa-cc-visa"></i></a></li>
												<li><a href="javascript:void(0);"><i class="fa fa-cc-mastercard"></i></a></li>
												<li><a href="javascript:void(0);"><i class="fa fa-cc-amex"></i></a></li>
											</ul>
											<small><b>100% Secure Online Payments</b> accepted, powered by EBS.</small>
										</div><!--Social-Media-->
									</div>    
								</div><!--/.col-md-3-->
							</div>
						</div>
					</section><!--/#bottom-->
					
					<footer id="footer" class="midnight-blue">
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									&copy; 2017 <a target="_blank" href="javascript:void(0)">Alive Assets</a>. All Rights Reserved.
								</div>
								<div class="col-sm-6">
									<ul class="pull-right">
										<li><a href="javascript:void(0)">Home</a>
										</li>
										<li><a href="javascript:void(0)">About Us</a>
										</li>
										<li><a href="javascript:void(0)">Faq</a>
										</li>
										<li><a href="javascript:void(0)">Contact Us</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
					<!--/#footer-->
					<!-- <script src="assets/js/jquery.js"></script>
					<script src="assets/js/bootstrap.min.js"></script> -->
					<script src="assets/js/bootstrap-submenu.js.download" defer=""></script>
					<!-- <script src="assets/js/jquery.prettyPhoto.js"></script> -->
					<script src="assets/js/jquery.isotope.min.js"></script>
					<script src="assets/js/main.js"></script>
					<script src="assets/js/wow.min.js"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js"></script>
					<script src="assets/js/drawer.js" charset="utf-8"></script>
					<script>
						$(document).ready(function() {
							$('.drawer').drawer();
						});
					</script>
					<!-- <script src="assets/js/search-stylesh.js"></script> -->
					<script src="assets/js/jquery.prettyPhoto.js"></script> 
					<script type="text/javascript" src="assets/js/fresco.js"></script>
					<!-- <script src="assets/js/add_property.js"></script> -->
					<script type="text/javascript" src="assets/js/jquery.mockjax.js"></script>
					<script type="text/javascript" src="assets/js/jquery.autocomplete.js"></script>
					<script type="text/javascript" src="assets/js/cities.js"></script>
					<script type="text/javascript" src="assets/js/demo.js"></script> 
					<script>
						$(document).ready(function(){ 
							
							$('[data-toggle="tooltip"]').tooltip();
						});
					</script>
					<script type="text/javascript">
						$(document).ready(function() {
							$('#myModal').modal('hide');
							var options = [];
							
							$( '.dropdown-menu a' ).on( 'click', function( event ) {
								
								var $target = $( event.currentTarget ),
								val = $target.attr( 'data-value' ),
								$inp = $target.find( 'input' ),
								idx;
								
								if ( ( idx = options.indexOf( val ) ) > -1 ) {
									options.splice( idx, 1 );
									setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
									} else {
									options.push( val );
									setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
								}
								
								$( event.target ).blur();
								
								console.log( options );
								return false;
							});
							
							$('.add-title-tab > .add-expand').on('click',function() {
								$(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
							});   
						});
						
						
					</script>
					
					<script src="assets/js/jssor.slider-22.2.10.min.js" type="text/javascript"></script>
					<script type="text/javascript">
						jssor_1_slider_init = function() {
							
							var jssor_1_SlideshowTransitions = [
							{$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
							
							];
							
							var jssor_1_options = {
								$AutoPlay: true,
								$SlideshowOptions: {
									$Class: $JssorSlideshowRunner$,
									$Transitions: jssor_1_SlideshowTransitions,
									$TransitionsOrder: 1
								},
								$ArrowNavigatorOptions: {
									$Class: $JssorArrowNavigator$
								},
								$ThumbnailNavigatorOptions: {
									$Class: $JssorThumbnailNavigator$,
									$Cols: 10,
									$SpacingX: 8,
									$SpacingY: 8,
									$Align: 360
								}
							};
							
							var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
							
							/*responsive code begin*/
							/*you can remove responsive code if you don't want the slider scales while window resizing*/
							function ScaleSlider() {
								var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
								if (refSize) {
									refSize = Math.min(refSize, 800);
									jssor_1_slider.$ScaleWidth(refSize);
								}
								else {
									window.setTimeout(ScaleSlider, 30);
								}
							}
							ScaleSlider();
							$Jssor$.$AddEvent(window, "load", ScaleSlider);
							$Jssor$.$AddEvent(window, "resize", ScaleSlider);
							$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
							/*responsive code end*/
						};
					</script>
					<script type="text/javascript">jssor_1_slider_init();</script>
				</body>
			</html>			