<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'aliveasset');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>


<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Fairpockets | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Fair Pockets">

        <!-- styles -->
        <link href="<?php echo $this->webroot; ?>admin_theme/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>admin_theme/css/bootstrap-responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>admin_theme/css/font-awesome.css">
        <!--[if IE 7]>
                    <link rel="stylesheet" href="<?php echo $this->webroot; ?>admin_theme/css/font-awesome-ie7.min.css">
                <![endif]-->
        <link href="<?php echo $this->webroot; ?>admin_theme/css/styles.css" rel="stylesheet">
        <!--[if IE 7]>
                    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>admin_theme/css/ie/ie7.css" />
                <![endif]-->
        <!--[if IE 8]>
                    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>admin_theme/css/ie/ie8.css" />
                <![endif]-->
        <!--[if IE 9]>
                    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>admin_theme/css/ie/ie9.css" />
                <![endif]-->
        <link href="<?php echo $this->webroot; ?>admin_theme/css/aristo-ui.css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>admin_theme/css/elfinder.css" rel="stylesheet">
        <!--<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>-->
        <!--fav and touch icons -->
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>admin_theme/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->webroot; ?>admin_theme/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->webroot; ?>admin_theme/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->webroot; ?>admin_theme/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $this->webroot; ?>admin_theme/ico/apple-touch-icon-57-precomposed.png">
        <!--============j avascript===========-->
        <script src="<?php echo $this->webroot; ?>admin_theme/js/jquery.js"></script>
        <script src="<?php echo $this->webroot; ?>admin_theme/js/jquery-ui-1.10.1.custom.min.js"></script>
        <script src="<?php echo $this->webroot; ?>admin_theme/js/bootstrap.js"></script>
    </head>
    <body>
        <div class="layout">
            <!-- Navbar================================================== -->
            <div class="navbar navbar-inverse top-nav">
                <div class="navbar-inner">
                    <div class="container">
                        <img src="<?php echo $this->webroot; ?>admin_theme/images/logo_login.png" ></a>
                        <div class="btn-toolbar pull-right notification-nav">
                            <div class="btn-group">

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--/header-->





            <?php echo $this->fetch('content'); ?>
        </div>
        <script type="text/javascript" >
            $(document).ready(function () {
                $('#createaccPage').hide();
                $('#createacc').click(function (e) {

                    $('#createaccPage').show();
                    $('#UserAdminLoginForm').hide();
                });
                $('#backLogin').click(function (e) {
                    $('#createaccPage').hide();
                    $('#UserAdminLoginForm').show();
                });
            });
        </script>

    </body>
</html>