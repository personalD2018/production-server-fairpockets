<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'aliveasset');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <?php
        echo $this->Html->meta(
                'viewport', //name
                'width=device-width, initial-scale=1.0'//content
        );
        ?>

        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title'); ?>
        </title>

        <?php
        echo $this->Html->meta('favicon.ico', 'home/res_icon.png', array('type' => 'icon'));
        echo $this->Html->css('home/bootstrap.min');
        echo $this->Html->css('home/font-awesome.min');
        echo $this->Html->css('home/animate.min');
        echo $this->Html->css('home/prettyPhoto');
        echo $this->Html->css('front/home');
        echo $this->Html->css('home/homecss');
        echo $this->Html->css('home/responsive');
        ?>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel="stylesheet">
        <?php
        echo $this->Html->script('front/jquery');
        echo $this->Html->script('front/bootstrap.min');
        echo $this->Html->css('home/bootstrap-multiselect');
        echo $this->Html->script('front/bootstrap-multiselect');
        echo $this->Html->css('home/drawer');
        ?>
        <!--[if lt IE 9]>
        <?php
        echo $this->Html->script('home/html5shiv');
        echo $this->Html->script('home/respond.min');
        ?>
     <![endif]-->


        <style>
            span.multiselect-native-select select{visibility:hidden;display:none;}
        </style>


        <?php
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <style>
            span.multiselect-native-select select{visibility:hidden;display:none;}
        </style>  
    </head>

    <body class="add_property">
        <header id="header">
            <nav class="navbar navbar-inverse" role="banner">
                <div class="row no-margin">
                    <div class="col-xs-12 col-sm-12">
                        <div class="navbar-header">
                            <!--  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                             <span class="sr-only">Toggle navigation</span>
                                               <img src="<?php echo $this->webroot; ?>img/front/res_icon.png"/>
                             </button> -->
                            <a class="navbar-brand" href="index.html"><img src="<?php echo $this->webroot; ?>img/front/logo.png" alt="logo" class="my-logo">
                            </a>


                            <div class="topsearchbox loc_dropdown">

                                <button onclick="showlocation()" class="dropbtn">Delhi </button>
                                <div id="mylocations" class="dropdown-content dropdown-locations">
                                    <ul>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                        <li><a href="#">Mumbai</a></li>
                                        <li><a href="#">Allahabad</a></li>
                                        <li><a href="#">Noida</a></li>
                                    </ul>
                                </div>
                            </div>

                            <script>
                                function showlocation() {
                                    document.getElementById("mylocations").classList.toggle("show");
                                }


                                window.onclick = function (event) {
                                    if (!event.target.matches('.dropbtn')) {

                                        var dropdowns = document.getElementsByClassName("dropdown-content");
                                        var i;
                                        for (i = 0; i < dropdowns.length; i++) {
                                            var openDropdown = dropdowns[i];
                                            if (openDropdown.classList.contains('show')) {
                                                openDropdown.classList.remove('show');
                                            }
                                        }
                                    }
                                }
                            </script>



                        </div>
                        <div class="drawer drawer--right">
                            <button type="button" class="drawer-toggle drawer-hamburger">
                                <span class="sr-only">toggle navigation</span>
                                <span class="drawer-hamburger-icon"></span>
                            </button>
                            <nav class="drawer-nav" role="navigation">
                                <a class="menulogo desktop-hide" href="index.html"><img src="<?php echo $this->webroot; ?>img/front/logo.png" alt="logo" class="my-logo">
                                </a>
                                <ul class="drawer-menu">
                                    <li><a class="drawer-brand" href="#">Home</a></li>
                                    <li><a class="drawer-menu-item" href="#">Properties List</a></li>
                                    <li><a class="drawer-menu-item" href="#">Tools</a></li>

                                    <li class="drawer-dropdown">
                                        <a class="drawer-menu-item" data-target="#" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
                                            Services <span class="drawer-caret"></span>
                                        </a>
                                        <ul class="drawer-dropdown-menu">
                                            <li><a class="drawer-dropdown-menu-item" href="research-reports.html">Research Reports</a></li>
                                            <li><a class="drawer-dropdown-menu-item" href="#">Transactions</a></li>
                                            <li><a class="drawer-dropdown-menu-item" href="#">Property Management</a></li>
                                            <li><a class="drawer-dropdown-menu-item" href="#">Portfolio Management</a></li>
                                            <li><a class="drawer-dropdown-menu-item" href="#">Check your deal</a></li>
                                            <li><a class="drawer-dropdown-menu-item" href="#">Assisted buying/selling</a></li>
                                        </ul>
                                    </li>

                                    <li><a class="drawer-menu-item" href="#">Pricing</a></li>
                                    <li><a class="drawer-menu-item" href="http://aliveasset.com/blog" target="_blank">Blog</a></li>
                                    <li class="desktop-hide"><a class="drawer-menu-item" href="add_property.html">Sell Your Property</a></li>
                                    <li  class="desktop-hide"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="drawer-menu-item button"><i class="fa fa-user-o" aria-hidden="true"></i> Log in</a></li>



                                </ul>
                            </nav>

                        </div>
                        <div class="navbar-right">
                            <ul class="nav navbar-nav mobile-hide">

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="research-reports.html">Research Reports</a>
                                        </li>
                                        <li><a href="research-reports.html">Transactions</a>
                                        </li>
                                        <li><a href="research-reports.html">Property Management</a>
                                        </li>
                                        <li><a href="research-reports.html">Portfolio Management</a>
                                        </li>
                                        <li><a href="research-reports.html">Check your deal</a>
                                        </li>
                                        <li><a href="research-reports.html"> Assisted buying/selling</a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>

                            <div class="addpostlink mobile-hide"><a href="add_property.html">Sell Your Property</a></div>
                            <div class="LoginApp">

                                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button">Log in / Register</a>

                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dial">
                                        <div class="modal-content">
                                            <div class="modal-body modalpadd">
                                                <div class="row">

                                                    <div class="col-md-6 modulsideback">

                                                        <div class="col-sm-12">
                                                            <p>&quot;Ab 42 lakh mein Gurgaon<br>
                                                                mein 2BHK milega.&quot;
                                                            </p>
                                                            <h4>#Mere<span>Budget</span>Mein</h4>

                                                            <div class="modalbacklidiv" style="margin-top:53px;display:block;">
                                                                <h3>Buy at your price</h3>
                                                                <div class="col-sm-12">
                                                                    <ul>
                                                                        <li>
                                                                            <div class=" text-center sing-l-tet fl">
                                                                                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Negotiate Online
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="col-sm-12 ">
                                                                    <ul>
                                                                        <li>
                                                                            <div class=" text-center sing-l-tet fl">
                                                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Spam Free Experience
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="col-sm-12 ">
                                                                    <ul>
                                                                        <li>
                                                                            <div class=" text-center sing-l-tet fl">
                                                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Top Rated Buyers &amp; Sellers
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 loginsidediv" style="border-right: 1px dotted #C2C2C2;padding-right: 10px;">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                        </div>

                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                <a href="#Login" data-toggle="tab" onclick="InitLoginPane();" >Login</a>
                                                            </li>

                                                            <li>
                                                                <a href="#Registration" data-toggle="tab" onclick="InitRegisterPane();" >Registration</a>
                                                            </li>
                                                        </ul>
                                                        <!-- Nav tabs Ends-->

                                                        <!-- Tab panes -->
                                                        <div class="tab-content" id="id_tab_panes">

                                                            <div class="tab-pane active" id="Login">

                                                                <div id="id_login_form" >

                                                                    <h2>Sign <span>In</span></h2>

                                                                    <?php echo $this->Session->flash('auth'); ?>
                                                                    <!-- Login Form -->

                                                                    <?php
                                                                    echo $this->Form->create(
                                                                            'WebsiteUser', array(
                                                                        'class' => 'form-horizontal fpform',
                                                                        'id' => 'login-form',
                                                                            )
                                                                    );
                                                                    ?>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <!-- 
                                                                                    <input type="text" class="form-control" id="email1" placeholder="Email" />
                                                                                <div class="error has-error" style="display:block;">Box must be filled out</div> 
                                                                            -->
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'email', array(
                                                                                'label' => false,
                                                                                'type' => 'text',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Email'
                                                                                    )
                                                                            );
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                                <!-- <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" /> -->
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'password', array(
                                                                                'label' => false,
                                                                                'type' => 'password',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Password'
                                                                                    )
                                                                            );
                                                                            ?>														
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label class="form-error-label" id="id_lform-error-label" ></label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-6">
                                                                            <input id="Remember" class="checkbox-custom" name="Remember" type="checkbox">
                                                                            <label for="Remember" class="checkbox-custom-label">Remember Me</label>
                                                                        </div>

                                                                        <div class="col-sm-6 text-right">
                                                                            <a href="#Forgotpassword" class="color-red" data-toggle="tab" onclick="document.getElementById('id_forgotpass_form').style.display = 'inline';">Forgot your password?</a>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-6">
                                                                            <button type="submit" class="btn btn-primary btn-red" onclick="">
                                                                                <i class="fa fa-sign-in"></i> &nbsp;Sign in
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <!-- </form> -->
                                                                    <?php
                                                                    echo $this->Form->end();
                                                                    ?>
                                                                    <!-- Login Form Ends -->
                                                                </div>
                                                            </div>


                                                            <div class="tab-pane" id="Registration">

                                                                <div class="text-center" id="id_register_succ" style="display:none;" >
                                                                </div> 

                                                                <div id="id_register_form" >

                                                                    <h2>Sign <span>up</span></h2>

                                                                    <?php
                                                                    echo $this->Session->flash();
                                                                    ?>

                                                                    <!-- Registration Form -->

                                                                    <?php
                                                                    echo $this->Form->create(
                                                                            'WebsiteUser', array(
                                                                        'class' => 'form-horizontal fpform',
                                                                        'role' => 'form',
                                                                        'id' => 'register-form',
                                                                        'novalidate' => 'novalidate'
                                                                            )
                                                                    );
                                                                    ?>

                                                                    <div class="form-group" style="">

                                                                        <div class="col-sm-12">

        <!-- <input id="usertype1" class="radio-custom" name="bachelor_all" type="radio"  checked="checked" onclick="SetCompanyOption(1);" > -->
                                                                            <!-- <label for="usertype1" class="radio-custom-label">Individual</label> -->
                                                                            <?php
                                                                            $options = array(
                                                                                '1' => 'Individual',
                                                                                '2' => 'Agent',
                                                                                '3' => 'Builder'
                                                                            );

                                                                            $attributes = array(
                                                                                'legend' => false,
                                                                                'class' => 'radio-custom',
                                                                                'label' => array('class' => 'radio-custom-label'),
                                                                                'default' => '1',
                                                                                'onclick' => 'SetCompanyOption(this.value)'
                                                                            );

                                                                            echo $this->Form->radio('userrole', $options, $attributes);
                                                                            ?>


                                                                            <?php
                                                                            /* 	
                                                                              $options = array('1' => 'Individual');
                                                                              $attributes = array(
                                                                              'class'   => 'radio-custom',
                                                                              'label'   => array ('class' => 'radio-custom-label'),
                                                                              'legend'  => false,
                                                                              'value'   => '1',
                                                                              'onclick' => 'SetCompanyOption(this.value)'
                                                                              );
                                                                              echo $this->Form->radio('userrole', $options, $attributes);

                                                                              $options = array('2' => 'Agent');
                                                                              $attributes = array(
                                                                              'class'   => 'radio-custom',
                                                                              'label'   => array ('class' => 'radio-custom-label'),
                                                                              'legend'  => false,
                                                                              'value'   => '2',
                                                                              'onclick' => 'SetCompanyOption(this.value)'
                                                                              );
                                                                              echo $this->Form->radio('userrole', $options, $attributes);

                                                                              $options = array('3' => 'Builder');
                                                                              $attributes = array(
                                                                              'class'   => 'radio-custom',
                                                                              'label'   => array ('class' => 'radio-custom-label'),
                                                                              'legend'  => false,
                                                                              'value'   => '3',
                                                                              'onclick' => 'SetCompanyOption(this.value)'
                                                                              );
                                                                              echo $this->Form->radio('userrole', $options, $attributes);
                                                                             */

                                                                            /*
                                                                              echo $this->form->radio(
                                                                              'userRole',
                                                                              array(
                                                                              '1' => 'Individual',
                                                                              '2' => 'Agent',
                                                                              '3' => 'Builder'
                                                                              ),
                                                                              array(
                                                                              'name'    => 'userRole',
                                                                              'class'   => 'radio-custom',
                                                                              'label'   => array ('class' => 'radio-custom-label'),
                                                                              'legend'  => false,
                                                                              'onclick' => 'SetCompanyOption()',
                                                                              'selected' => '1'
                                                                              )
                                                                              );
                                                                             */
                                                                            ?>

                                                                        </div>

                                                                    </div>




                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                                <!-- <input type="text" class="form-control" placeholder="Name" /> -->
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'username', array(
                                                                                'label' => false,
                                                                                'type' => 'text',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Name'
                                                                                    )
                                                                            );
                                                                            ?>														
                                                                        </div>
                                                                    </div>

                                                                    <div id="id_group_reg_company" class="form-group" style="display:none;" >
                                                                        <div id="id_reg_company" class="col-sm-12">
                                                                                <!-- <input type="text" class="form-control" placeholder="Company Name" /> -->
                                                                            <?php
                                                                            /* This will be dynamically added
                                                                              echo $this->Form->input(
                                                                              'userOrganizatioName',
                                                                              array(
                                                                              'label' => false,
                                                                              'type'  => 'text',
                                                                              'class' => 'form-control',
                                                                              'id'    => 'userOrgName',
                                                                              'name'  => 'userOrgName',
                                                                              'placeholder' => 'Organization Name'

                                                                              )
                                                                              );
                                                                             */
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                                <!-- <input type="text" class="form-control" id="email" name="email" placeholder="Email"/> -->
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'email', array(
                                                                                'label' => false,
                                                                                'type' => 'text',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Email'
                                                                                    )
                                                                            );
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                                <!-- <input type="text" class="form-control" id="mobile" placeholder="Mobile" /> -->
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'usermobile', array(
                                                                                'label' => false,
                                                                                'type' => 'text',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Mobile'
                                                                                    )
                                                                            );
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                                <!-- <input type="password" class="form-control" id="password" placeholder="Password" /> --> 
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'password', array(
                                                                                'label' => false,
                                                                                'type' => 'password',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Password'
                                                                                    )
                                                                            );
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label class="form-error-label" id="id_rform-error-label" ></label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <span >By Signing up, I agree with the <a class="color-red" href="111">T&C1 </a></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group signupbtn">
                                                                        <div class="col-sm-12">
                                                                            <button type="submit" class="btn btn-primary btn-red">
                                                                                <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Sign up
                                                                            </button>

                                                                            <!-- <a href="#SuccessMsg" class="btn btn-primary " data-toggle="tab">SuccessMSG</a> -->
                                                                        </div>
                                                                    </div>

                                                                    <!-- </form> -->
                                                                    <?php
                                                                    echo $this->Form->end();
                                                                    ?>	
                                                                    <!-- Registration Form Ends-->

                                                                </div>
                                                            </div>


                                                            <div class="tab-pane" id="Forgotpassword">

                                                                <div class="text-center" id="id_forgotpass_succ" style="display:none;" >
                                                                </div> 

                                                                <div id="id_forgotpass_form" >

                                                                    <h2>Forgot <span>Password</span></h2>

                                                                    <!-- Forgot Password Form -->

                                                                    <?php
                                                                    echo $this->Form->create(
                                                                            'WebsiteUser', array(
                                                                        'class' => 'form-horizontal fpform',
                                                                        'id' => 'forgotpass-form'
                                                                            )
                                                                    );
                                                                    ?>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <!-- < input type="text" class="form-control" id="email1" placeholder="Email" /> -->
                                                                            <?php
                                                                            echo $this->Form->input(
                                                                                    'email', array(
                                                                                'label' => false,
                                                                                'type' => 'text',
                                                                                'class' => 'form-control',
                                                                                'placeholder' => 'Email'
                                                                                    )
                                                                            );
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label class="form-error-label" id="id_fform-error-label" ></label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-6">
                                                                            <button type="submit" class="btn btn-primary btn-red">
                                                                                <i class="fa fa-paper-plane"></i> &nbsp;Send
                                                                            </button>
                                                                        </div>

                                                                        <div class="col-sm-6 text-right">
                                                                            <a href="#Login" class="color-red"   data-toggle="tab">Login</a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- </form> -->
                                                                    <?php
                                                                    echo $this->Form->end();
                                                                    ?>
                                                                    <!-- Forgot Password Form Ends-->

                                                                </div>
                                                            </div>				

                                                        </div>
                                                        <!-- Tab panes Ends-->

                                                        <!-- <div id="OR" class="hidden-xs"> -->
                                                        <!-- OR -->
                                                        <!-- </div> -->
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
// Jquery 
// Jquery Script for Validation of form fields    
                            echo $this->Html->script('validate_1.9_jquery.validate.min');
                            echo $this->Html->script('front/additional-methods.min');


                            echo $this->Html->scriptBlock("
	
		// Function : To clear/set registration from fields when registration TAB is clicked
		function InitRegisterPane()
		{
			// Show registration pane
			document.getElementById('id_register_form').style.display = 'inline' ;
			
			//Reset form
			document.getElementById('register-form').reset();
			
			//Remove validation error messages
			//document.getElementById('register-form').validate.reset();
			var validator = $( '#register-form' ).validate();
			validator.resetForm();
			
			//Remove class error from all inputs... To remove red line from form elements
			$('.form-group input').removeClass('error');
			
			// Hide Error box
			document.getElementById('id_rform-error-label').style.display = 'none';
						
			// Hide success pane
			document.getElementById('id_register_succ').style.display = 'none' ;
		}

		// Function : To clear/set login from fields when registration TAB is clicked
		function InitLoginPane()
		{
			//Mark login pane as active... bit of hardcoding
			var tabPanel = document.getElementById('id_tab_panes');
			tabPanel.childNodes[1].className = 'tab-pane active'; // Login Pane is marked active
			tabPanel.childNodes[5].className = 'tab-pane'; // Forgot password pane is marked non-acitve
			
			
			// Show login form
			document.getElementById('id_login_form').style.display = 'inline' ;
			
			//Reset form
			document.getElementById('login-form').reset();
			document.getElementById('forgotpass-form').reset();
			
			//Remove validation error messages
			//document.getElementById('register-form').validate.reset();
			var validator = $( '#login-form' ).validate();
			validator.resetForm();
			
			//Remove class error from all inputs... To remove red line from form elements
			$('.form-group input').removeClass('error');
			
			
			
			//Remove validation error messages
			//document.getElementById('login-form').validate.reset();
			
			// Hide Error box
			document.getElementById('id_lform-error-label').style.display = 'none';
			document.getElementById('id_fform-error-label').style.display = 'none';
						
			// Hide forgot password success pane
			document.getElementById('id_forgotpass_succ').style.display = 'none' ;
		}		
		
		
		// Function : To set company name option for register form
		// Company Name Mandatory - if user is agent(usr_type - 2) / builder( usr_type - 3)
		// Company Name Optional - if user is individual(usr_type - 1)		
		function SetCompanyOption( usr_type )
		{
			//alert(document.getElementById('id_reg_company').innerHTML );
			
			//Reset form ... is it needed ????
			//document.getElementById('register-form').reset();
			
			//Remove validation error messages
			//document.getElementById('register-form').validate.reset();
			var validator = $( '#register-form' ).validate();
			validator.resetForm();
			
			//Remove class error from all inputs... To remove red line from form elements
			$('.form-group input').removeClass('error');
			
			
			
			if( usr_type == '1')
			{
				document.getElementById('id_group_reg_company').style.display = 'none'; 
				document.getElementById('id_reg_company').innerHTML = ''; 				
			} 
			else 
			{
				document.getElementById('id_group_reg_company').style.display = 'block';
				document.getElementById('id_reg_company').innerHTML = 
				'<input type=' +'\"text\"' +
				      ' class='+'\"form-control\"' +
					  ' name=' +'\"data[User][userorgname]\"' +
					  ' placeholder=' +'\"Organization\"' +
				' \/>'; 
				
			}
		
		}
	");


// Script  : To set validation rules for FORMS
                            echo $this->Html->scriptBlock("
	
		// Function : Submit ForgotPassword Form using Ajax
		function UserForgotPassFormSubmit()
		{ 
				var data_str = $('#forgotpass-form').serialize();
				var url_str = 'http://localhost/fairPockets/fp/users/forgotpass';

				// Clear previous error stmt
				$('#id_fform-error-label').html('');
				$('#id_fform-error-label').css('display', 'none');
				
				$.ajax({
					type: 'POST',
					url: url_str,
					data: data_str,
					success: function(data,textStatus,xhr)
					{
                        //alert('Forgotpass Success');
						//alert(data);
						//alert(textStatus);
						
						var response = jQuery.parseJSON(data);
						
						if( response.status == 'success' )
						{
							// Login Successful
							// alert('Forgotpass Successful');
							
							// Hide Forgot Password Pane
							$('#id_forgotpass_form').css('display', 'none');

							// Show Success Pane	
							temp_str = '<h3 class=\'regsucs\'>Password Reset Successful. </h3>';
                            temp_str = temp_str + '<div>';
							temp_str = temp_str + 'We have shared your new password on your registered email id.' 
							temp_str = temp_str + '</div>'
							
							$('#id_forgotpass_succ').html(temp_str);
							$('#id_forgotpass_succ').css('display', 'inline');
							$('#id_forgotpass_succ').css('visibility', 'visible');
						
						}
						else if( response.status == 'error' )
						{
							// Forgotpass Failed
							
							err_str = 'Given Email donot exists in our records';
							
							// Show error in error box  
							
							//alert('Forgotpass Failed');
							$('#id_fform-error-label').html(err_str);
							$('#id_fform-error-label').css('display','inline');
						}
						else
						{
							;
						}
					},
					error: function(xhr,textStatus,error)
					{
                        // Show error in error box 
						
						alert('Forgotpass Failure');
						alert(textStatus);
						alert(error);
						
						$('#id_fform-error-label').html('Form submission unsuccessful. Please try after some time');
						$('#id_fform-error-label').css('display', 'inline');
					}
				});

				//return false;		
				
		}
	
		// Function : Submit Login Form using Ajax
		function UserLoginFormSubmit()
		{ 
				var data_str = $('#login-form').serialize();
				var url_str = '/fp/dev/websiteUsers/login';

				// Clear previous error stmt
				$('#id_lform-error-label').html('');
				$('#id_lform-error-label').css('display', 'none');
				
				$.ajax({
					type: 'POST',
					url: url_str,
					data: data_str,
					success: function(data,textStatus,xhr)
					{
                        //alert('Login Success');
						//alert(data);
						//alert(textStatus);
						
						var response = jQuery.parseJSON(data);
						
						if( response.status == 'success' )
						{
							// Login Successful
							alert('Login Successful');
							
							//TODO : Redirect to correct screen
						}
						else if( response.status == 'error' )
						{
							// Login Failed
							
							err_str = 'Given Email/Password do not match';
							
							// Show error in error box  
							
							// alert(err_str);
							$('#id_lform-error-label').html(err_str);
							$('#id_lform-error-label').css('display','inline');
						}
						else
						{
							;
						}
					},
					error: function(xhr,textStatus,error)
					{
                        // Show error in error box 
						
						//alert('Login Failure');
						//alert(textStatus);
						//alert(error);
						
						$('#id_lform-error-label').html('Login unsuccessful. Please try after some time');
						$('#id_lform-error-label').css('display', 'inline');
					}
				});

				//return false;		
				
		}
		
		// Function : Submit Registration Form using Ajax
		function UserRegistrationFormSubmit()
		{ 
				var data_str = $('#register-form').serialize();
				var url_str = '/fp/dev/websiteUsers/register';

				// Clear previous error stmt
				$('#id_rform-error-label').html('');
				$('#id_rform-error-label').css('display', 'none');
				
				$.ajax({	
					type: 'POST',
					url: url_str,
					data: data_str,
					success: function(data,textStatus,xhr)
					{
                        //alert('Registration Success');
						//alert(data);
						//alert(textStatus);
						
						var response = jQuery.parseJSON(data);
						
						if( response.status == 'success' )
						{
							// Registration Successful
							
							// Hide Registration Pane
							$('#id_register_form').css('display', 'none');

							// Show Success Pane	
							temp_str = '<h3 class=\'regsucs\'>Thank you for registering with us. </h3>';
                            temp_str = temp_str + '<div>';
							temp_str = temp_str + 'We have send you confirmation mail to your registered email id. Please click on the link in mail to confirm your registration.' 
							temp_str = temp_str + '</div>'
							
							$('#id_register_succ').html(temp_str);
							$('#id_register_succ').css('display', 'inline');
							$('#id_register_succ').css('visibility', 'visible');
						}
						else if( response.status == 'error' )
						{
							// Failed in validation errors or otherwise
							
							err_str = 'Your registration was not successful';
							err_str = err_str;
							$.each(response.data, function(model, errors) 
							{
								for (fieldName in this) 
								{
									//alert( fieldName );
									//alert( this[fieldName][0] );
									//alert( this[fieldName][1] );
									
									err_str = err_str + '<br>' + '- ' + this[fieldName][0];
									
								}
							});
							
							// Show error in error box  
							
							// alert(err_str);
							$('#id_rform-error-label').html(err_str);
							$('#id_rform-error-label').css('display', 'inline');
							
						}
						else
						{
							;
						}	
					},
					error: function(xhr,textStatus,error)
					{
                        // Show error in error box 
						
						//alert('Registeration Failure');
						//alert(textStatus);
						//alert(error);
						
						$('#id_rform-error-label').html('Registeration unsuccessful. Please try after some time');
						$('#id_rform-error-label').css('display', 'inline');
					}
				});
		}
		
	$(function() {
		
		$.validator.addMethod('nameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z ]+/.test(value);
		}, 'Please use English alphabets only.');
	
		$.validator.addMethod('onameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z0-9]+/.test(value);
		}, 'Please use Alphanumeric characters only.');	
	
		$.validator.addMethod('mobileCustom', function(value, element) 
		{
			return this.optional(element) || /^[0-9]+/.test(value);  
		}, 'Please use numerals only.');	
	
		$('#register-form').validate({
		
			// Specify the validation rules
			rules: {
				'data[WebsiteUser][username]': {
					required : true,
					lettersonly : true
				},
				'data[WebsiteUser][userorgname]': {
					required : true,
					alphanumeric : true,
					minlength : 5
				},
				'data[WebsiteUser][email]': {
					required : true,
					email : true
				},
				'data[WebsiteUser][usermobile]': {
					required : true,
					number : true,
					mobileCustom : true,
					minlength : 8
				},
				'data[WebsiteUser][password]': {
					required : true
				},
				'data[WebsiteUser][userrole]': {
					required : true
				}
			},
		
			// Specify the validation error messages
			messages: {
				'data[WebsiteUser][username]': {
					required : 'Name is required.',
					lettersonly  : 'Only Letters allowed.',
				},
				'data[WebsiteUser][userorgname]': {
					required : 'Organization Name is required.',
					alphanumeric : 'Only Alphanumerics allowed.',
					minlength : 'Min 5 alphabets.'
				},
				'data[WebsiteUser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address'
				},
				'data[WebsiteUser][usermobile]': {
					required : 'Mobile Number is required.',
					number : ' Only Numeric digits allowed.',
					minlength : 'Min 8 numbers.'
				},
				'data[WebsiteUser][password]': {
					required : 'Password is required.'
				},
				'data[WebsiteUser][userrole]': {
					required : 'Member Type is required.'
				}
			},
			
			submitHandler: function(){
				
				// Validations OK. Form can be submitted
				UserRegistrationFormSubmit();
				return false;
			}
		});

		$('#login-form').validate({
		
			// Specify the validation rules
			rules: {
				'data[WebsiteUser][email]': {
					required : true,
					email : true
				},
				'data[WebsiteUser][password]': {
					required : true
				}
			},
		
			// Specify the validation error messages
			messages: {
				'data[WebsiteUser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address'
				},
				'data[WebsiteUser][password]': {
					required : 'Password is required.'
				}
			},
			
			submitHandler: function(){
				
				// Validations OK. Form can be submitted
				UserLoginFormSubmit();
				return false;
			}			
		});

		$('#forgotpass-form').validate({
		
			// Specify the validation rules
			rules: {
				'data[WebsiteUser][email]': {
					required : true,
					email : true
				}
			},
		
			// Specify the validation error messages
			messages: {
				'data[WebsiteUser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address'
				}
			},
			
			submitHandler: function(){
				
				// Validations OK. Form can be submitted
				UserForgotPassFormSubmit();
				return false;
			}
		});		
	});	
        
	");
                            ?>
                            <!--/login-app-->

                            <!--/login-app-->

                        </div>
                    </div><!--./col-->
                </div>
                <!--/.row-->
            </nav>
            <!--/nav-->
        </header>
        <!--/header-->

        <div id="content">

            <?php echo $this->Flash->render(); ?>

            <?php echo $this->fetch('content'); ?>
        </div>
        <section id="bottom" class="col-md-12">
            <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3>Company</h3>
                            <ul>
                                <li><a href="javascript:void(0);">About us</a></li>
                                <li><a href="javascript:void(0);">Our Team</a></li>
                                <li><a href="javascript:void(0);">Official Blog</a></li>
                                <li><a href="javascript:void(0);">Career</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3>Product &amp; Services</h3>
                            <ul>
                                <li><a href="javascript:void(0);">Research Reports</a></li>
                                <li><a href="javascript:void(0);">Advisory (Buy/Sell)</a></li>
                                <li><a href="javascript:void(0);">Property Management</a></li>
                                <li><a href="javascript:void(0);">Portfolio Management</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3>Legal &amp; Support</h3>
                            <ul>
                                <li><a href="javascript:void(0);">Feedback</a></li>
                                <li><a href="javascript:void(0);">My Account</a></li>
                                <li><a href="javascript:void(0);">Disclaimer</a></li>
                                <li><a href="javascript:void(0);">Privacy &amp; Policy</a></li>
                                <li><a href="javascript:void(0);">Terms &amp; Condition</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3>Social Links</h3>
                            <div class="social">
                                <ul class="social-share">
                                    <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li> 
                                    <li><a href="javascript:void(0);"><i class="fa fa-skype"></i></a></li>
                                </ul>

                                <ul class="list-inline pay-icons">
                                    <li><a href="javascript:void(0);"><i class="fa fa-cc-visa"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-cc-mastercard"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-cc-amex"></i></a></li>
                                </ul>
                                <small><b>100% Secure Online Payments</b> accepted, powered by EBS.</small>
                            </div><!--Social-Media-->
                        </div>    
                    </div><!--/.col-md-3-->
                </div>
            </div>
        </section><!--/#bottom-->

        <footer id="footer" class="midnight-blue col-md-12">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        &copy; 2017 <a target="_blank" href="javascript:void(0)">Fair Pockets</a>. All Rights Reserved.
                    </div>
                    <div class="col-sm-6">
                        <ul class="pull-right">
                            <li><a href="javascript:void(0)">Home</a>
                            </li>
                            <li><a href="javascript:void(0)">About Us</a>
                            </li>
                            <li><a href="javascript:void(0)">Faq</a>
                            </li>
                            <li><a href="javascript:void(0)">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--/#footer-->
        <?php
        echo $this->Html->script('front/jquery.prettyPhoto');
        echo $this->Html->script('front/jquery.isotope.min');
        echo $this->Html->script('front/main');
        echo $this->Html->script('front/wow.min');
        ?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js"></script>
        <?php
        echo $this->Html->script('front/drawer');
        ?>

        <script>
			$(document).ready(function () {
				$('.drawer').drawer();
			});
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("li.drop-accordian a:first").bind("click", function (e) {
                    $(this).next('ul').slideToggle();
                    e.stopPropagation();
                });

                $('#firstLevelNav_small').on('hidden.bs.dropdown', function () {
                    $(this).find('ul.drop-accordian-menu').hide();
                });
            });
        </script>
    </body>
</html>
