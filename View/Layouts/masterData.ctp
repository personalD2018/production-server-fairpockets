 <!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Fair Pockets">

        <link rel="stylesheet" type="text/css" href="<?php print $this->webroot; ?>lib/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>admin_theme/css/font-awesome.css">

        <link href="<?php echo $this->webroot; ?>admin_theme/css/styles.css" rel="stylesheet">

    </head>
    <body>

        <div class="layout1">
            <!-- Navbar================================================== -->
            <div class="navbar navbar-inverse">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="brand"><img src="<?php echo $this->webroot; ?>admin_theme/images/logo_login.png"  alt="Fair Pockets"></a>
                        <div class="nav-collapse">

                        </div>
                        <?php //echo $this->element('notifiction'); ?>
                    </div>
                </div>
            </div>
            <!-- <nav class="navbar navbar-inverse">
                <div class="container">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#">WebSiteName</a>
              </div>
              <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="#">Page 1</a></li>
                  <li><a href="#">Page 2</a></li>
                  <li><a href="#">Page 3</a></li>
              </ul>
          </div>
      </nav> -->
            <div class="leftbar leftbar-close clearfix">
                <div class="admin-info clearfix">
                    <div class="admin-thumb">
                        <?php if (!empty($this->Session->read('Auth.User.profile_pic'))) { ?><img src="<?php echo $this->webroot; ?>upload/profile_pic/<?php echo $this->Session->read('Auth.User.profile_pic'); ?>" alt="thumb" width="64" height="50"><?php } else { ?><img src="<?php echo $this->webroot; ?>/backend/images/no_user_image.jpg" alt="thumb" width="60" height="40"><?php } ?>

                    </div>
                    <div class="admin-meta">
                        <ul>
                            <li class="admin-username"><?php echo ucfirst($this->Session->read('Auth.User.username')); ?></li>
                            <li><a href="<?php echo $this->webroot; ?>admin/users/profile">Edit Profile</a></li>
                            <li><a href="<?php echo $this->webroot; ?>admin/users/reset">Reset Password</a><a href="<?php echo $this->webroot; ?>admin/users/logout"><i class="icon-lock"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="left-nav clearfix">
                    <div class="left-primary-nav">
                        <ul id="myTab">
                            <li class="<?php echo ($this->params['action'] == 'admin_dashboard') ? 'active' : 'inactive' ?>"><a href="#dashboard" class="icon-desktop" title="Dashboard"></a></li>
                            <li class="<?php echo ($this->params['action'] == 'admin_addEmployee' || $this->params['action'] == 'admin_index' || $this->params['action'] == 'admin_editEmployee' || $this->params['action'] == 'admin_viewemployee' || $this->params['action'] == 'admin_myteam') ? 'active' : 'inactive' ?>"><a href="#employee" class="icon-group " title="FP Employees"></a></li>

                            <li class="<?php echo ($this->params['action'] == 'admin_builderIndex' || $this->params['action'] == 'admin_editBuilder' || $this->params['action'] == 'admin_viewBuilder') ? 'active' : 'inactive' ?>"><a href="#builder" class="icon-building " title="FP Builder"></a></li>

                            <li class="<?php echo ($this->params['action'] == 'admin_currentproject' || $this->params['action'] == 'admin_pastproject') ? 'active' : 'inactive' ?>"><a href="#project" class="icon-list-alt " title="FP Project"></a></li>

                            <li class="<?php echo ($this->params['action'] == 'admin_searchproperties' || $this->params['action'] == 'admin_approvedproperties') ? 'active' : 'inactive' ?>"><a href="#properties" class="icon-home " title="FP Properties"></a></li>


                            <li class="<?php echo ($this->params['action'] == 'admin_addreport' || $this->params['action'] == 'admin_viewreport' || $this->params['action'] == 'admin_editResearchReport') ? 'active' : 'inactive' ?>"><a href="#reports" class="icon-briefcase " title="Research Reports"></a></li>






                            <?php if ($this->Session->read('Auth.User.employee_role_id') == 0) { ?>
                                <li class="<?php echo ($this->params['action'] == 'admin_assignArea') ? 'active' : 'inactive' ?>"><a href="#area" class="icon-th-large " title="Areawise Assignment"></a></li>


                            <?php } ?>
                            <li class="<?php echo ($this->params['action'] == 'admin_buy_rent_service' || $this->params['action'] == 'admin_viewBuyrent' || $this->params['action'] == 'admin_editBuyrent' || $this->params['action'] == 'admin_sell_rent_service' || $this->params['action'] == 'admin_viewsellrentleads' || $this->params['action'] == 'admin_editsellrentleads' || $this->params['action'] == 'admin_addsellrentleads' || $this->params['action'] == 'admin_viewSellrent' || $this->params['action'] == 'admin_view_portfolio' || $this->params['action'] == 'admin_edit_portfolio' || $this->params['action'] == 'admin_masterDataEntry') ? 'active' : 'inactive' ?>"><a href="#service" class="icon-cogs " title="Services"></a></li>


                        </ul>

                    </div>
                    <div class="responsive-leftbar">
                        <i class="icon-list"></i>
                    </div>
                    <div class="left-secondary-nav tab-content">
                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_dashboard' || $this->params['action'] == 'admin_reset' || $this->params['action'] == 'admin_profile') ? 'active' : 'inactive' ?>" id="dashboard">
                            <h4 class="side-head">Dashboard</h4>
                            <ul id="nav" class="accordion-nav">
                                <li class="<?php echo ($this->params['action'] == 'admin_dashboard') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/users/dashboard"><i class="icon-dashboard"></i>View Dashboard</a></li>


                            </ul>
                        </div>
                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_addEmployee' || $this->params['action'] == 'admin_index' || $this->params['action'] == 'admin_editEmployee' || $this->params['action'] == 'admin_viewemployee' || $this->params['action'] == 'admin_myteam') ? 'active' : 'inactive' ?>" id="employee">
                            <h4 class="side-head">FP Employees</h4>

                            <ul id="nav" class="accordion-nav">
                                <?php if ($this->Session->read('Auth.User.employee_role_id') == 0) { ?>
                                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/users"><i class="icon-user"></i>View Employees</a></li>
                                    <li class="<?php echo ($this->params['action'] == 'admin_addEmployee') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/users/addEmployee"><i class="icon-plus"></i>Add Employees</a></li>
                                    <?php
                                } else {
                                    ?>
                                    <li class="<?php echo ($this->params['action'] == 'admin_myteam') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/users/myteam"><i class="icon-list-alt"></i>My Team</a></li>
                                <?php }
                                ?>

                            </ul>
                        </div>

                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_builderIndex' || $this->params['action'] == 'admin_editBuilder' || $this->params['action'] == 'admin_viewBuilder') ? 'active' : 'inactive' ?>" id="builder">
                            <h4 class="side-head">FP Builder</h4>

                            <ul id="nav" class="accordion-nav">

                                <li class="<?php echo ($this->params['action'] == 'admin_builderIndex' || $this->params['action'] == 'admin_editBuilder' || $this->params['action'] == 'admin_viewBuilder') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/builder"><i class="icon-list-alt"></i>View Builder Organization</a></li>

                            </ul>
                        </div>


                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_currentproject' || $this->params['action'] == 'admin_pastproject') ? 'active' : 'inactive' ?>" id="project">
                            <h4 class="side-head">FP Project</h4>

                            <ul id="nav" class="accordion-nav">

                                <li class="<?php echo ($this->params['action'] == 'admin_currentproject') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/Projects/currentproject"><i class="icon-list-alt"></i>Current Projects</a></li>
                                <li class="<?php echo ($this->params['action'] == 'admin_pastproject') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/Projects/pastproject"><i class="icon-th"></i>Past Projects</a></li>


                            </ul>
                        </div>


                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_searchproperties' || $this->params['action'] == 'admin_approvedproperties') ? 'active' : 'inactive' ?>" id="properties">
                            <h4 class="side-head">FP Properties</h4>

                            <ul id="nav" class="accordion-nav">

                                <li class="<?php echo ($this->params['action'] == 'admin_searchproperties') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/properties/searchproperties"><i class="icon-search"></i>Search Properties</a></li>
                                <li class="<?php echo ($this->params['action'] == 'admin_approvedproperties') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/properties/approvedproperties"><i class="icon-check"></i>Waiting For Approval</a></li>


                            </ul>
                        </div>


                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_assignArea') ? 'active' : 'inactive' ?>" id="area">
                            <h4 class="side-head">Assignment</h4>
                            <ul id="nav" class="accordion-nav">
                                <li class="<?php echo ($this->params['action'] == 'admin_assignArea') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/areas/assignArea"><i class="icon-th"></i>Areawise Assignment</a></li>


                            </ul>
                        </div>

                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_viewreport' || $this->params['action'] == 'admin_addreport' || $this->params['action'] == 'admin_editResearchReport') ? 'active' : 'inactive' ?>" id="reports">
                            <h4 class="side-head">Research Report</h4>
                            <ul id="nav" class="accordion-nav">
                                <li class="<?php echo ($this->params['action'] == 'admin_viewreport') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/accounts/viewreport"><i class="icon-bar-chart"></i>View Reports</a></li>
                                <li class="<?php echo ($this->params['action'] == 'admin_addreport') ? 'active' : 'inactive' ?>"><a href="<?php echo $this->webroot; ?>admin/accounts/addreport"><i class="icon-plus"></i>Add Report</a></li>

                            </ul>
                        </div>
                        <div class="tab-pane <?php echo ($this->params['action'] == 'admin_buy_rent_service' || $this->params['action'] == 'admin_editBuyrent' || $this->params['action'] == 'admin_viewBuyrent' || $this->params['action'] == 'admin_sell_rent_service' || $this->params['action'] == 'admin_viewsellrentleads' || $this->params['action'] == 'admin_editsellrentleads' || $this->params['action'] == 'admin_addsellrentleads' || $this->params['action'] == 'admin_viewSellrent' || $this->params['action'] == 'admin_view_portfolio' || $this->params['action'] == 'admin_edit_portfolio' || $this->params['action'] == 'admin_property_service_request' || $this->params['action'] == 'admin_edit_property_service' || $this->params['action'] == 'admin_addservicedetail' || $this->params['action'] == 'admin_propertyDataUpload' || $this->params['action'] == 'admin_masterDataEntry') ? 'active' : '' ?>" id="service">
                            <h4 class="side-head">Services</h4>
                            <ul id="nav" class="accordion-nav">
                                <li class="<?php echo ($this->params['action'] == 'admin_buy_rent_service' || $this->params['action'] == 'admin_editBuyrent' || $this->params['action'] == 'admin_viewBuyrent') ? 'active' : 'inactive' ?>">
                                    <a href="<?php echo $this->webroot; ?>admin/accounts/buy_rent_service">
                                        <i class="icon-shopping-cart"></i>
                                        Buy/Rent
                                    </a>
                                </li>
                                <li class="<?php echo ($this->params['action'] == 'admin_sell_rent_service' || $this->params['action'] == 'admin_viewsellrentleads' || $this->params['action'] == 'admin_editsellrentleads' || $this->params['action'] == 'admin_addsellrentleads'  || $this->params['action'] == 'admin_viewSellrent') ? 'active' : '' ?>">
                                    <a href="<?php echo $this->webroot; ?>admin/accounts/sell_rent_service">
                                        <i class="icon-shopping-cart"></i>
                                        Sell/Rent
                                    </a>
                                </li>
                                <li class="<?php echo ($this->params['action'] == 'admin_view_portfolio' || $this->params['action'] == 'admin_edit_portfolio') ? 'active' : 'inactive' ?>">
                                    <a href="<?php echo $this->webroot; ?>admin/accounts/view_portfolio">
                                        <i class="icon-bar-chart"></i> 
                                        View Portfolio
                                    </a>
                                </li>
                                <li class="<?php echo ($this->params['action'] == 'admin_property_service_request' || $this->params['action'] == 'admin_edit_property_service' || $this->params['action'] == 'admin_addservicedetail') ? 'active' : 'inactive' ?>">
                                    <a href="<?php echo $this->webroot; ?>admin/accounts/property_service_request">
                                        <i class="icon-cog"></i>
                                        Property Service
                                    </a>
                                </li>
                                <li class="<?php echo ($this->params['action'] == 'admin_propertyDataUpload') ? 'active' : '' ?>">
                                    <a href="<?php echo $this->webroot; ?>admin/accounts/propertyDataUpload">
                                        <i class="icon-cog"></i>
                                        Property Data Upload
                                    </a>
                                </li>
                                <li class="<?php echo ($this->params['action'] == 'admin_masterDataEntry') ? 'active' : '' ?>">
                                    <a href="<?php echo $this->webroot; ?>admin/accounts/masterDataEntry">
                                        <i class="icon-cog"></i>
                                        Master Data Entry
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-wrapper">
                <?php echo $this->fetch('content'); ?>

            </div>
            <div class="copyright">
                <p>
                    &copy; 2017 Fair Pockets
                </p>
            </div>
            <div class="scroll-top">
                <a href="#" class="tip-top" title="Go Top"><i class="icon-double-angle-up"></i></a>
            </div>
        </div>

        <script type="text/javascript" src="<?php print $this->webroot; ?>lib/jquery/jquery-3.1.1.js"></script>
<script type="text/javascript" src="<?php print $this->webroot; ?>lib/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo $this->webroot; ?>admin_theme/js/custom.js"></script>
<script type="text/javascript" src="<?php print $this->webroot; ?>lib/angular/angular-1.6.1.js"></script>
<script type="text/javascript" src="<?php print $this->webroot; ?>lib/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php print $this->webroot; ?>lib/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php print $this->webroot; ?>lib/datatables/angular-datatables.js"></script>
<script type="text/javascript" src="<?php print $this->webroot; ?>js/master_data_entry.js"></script>
    </body>
</html>