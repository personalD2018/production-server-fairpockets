<?php

App::uses('AuthComponent', 'Controller/Component');

// app/Controller/AppController.php
class AppController extends Controller {

    //...

    /* public $components = array(
      'Session','Cookie',
      'Flash',
      'Auth' => array(

      'authorize' => array('Controller') // Added this line
      )
      ); */
    public $uses = array('PropertyNotifiction', 'BuilderApprovalNotifiction', 'ServiceNotifiction');
    public $helpers = array('Html', 'Form', 'Js', 'Cache', 'Session');
    public $components = array('DebugKit.Toolbar', 'Auth' => array(
            'authorize' => array('Controller') // Added this line
        ),
        'Session', 'RequestHandler', 'Flash', 'Cookie');

    public function beforeFilter() {

        Security::setHash('blowfish');
        $this->Auth->allow('index');
        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
            $notifiction = $this->PropertyNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id'),
                    'inquiry_id !=' => 0
                )
            ));

            $propertyapproval = $this->PropertyNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id'),
                    'inquiry_id' => '0'
                )
            ));
            //get builder approval notifiction

            $buildernotifiction = $this->BuilderApprovalNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id')
                )
            ));

            $servicenotifiction = $this->ServiceNotifiction->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Session->read('Auth.User.id')
                )
            ));
            $this->set('servicenotifiction', $servicenotifiction);
            $this->set('buildernotifiction', $buildernotifiction);
            $this->set('notifiction', $notifiction);
            $this->set('propertyapproval', $propertyapproval);
            $this->layout = 'admin';
            $this->Auth->authorize = 'Controller';
            AuthComponent::$sessionKey = 'Auth.User';
            $this->Auth->authenticate = array('Form' => array('fields' => array('username' => 'email', 'password' => 'password'), 'passwordHasher' => 'Blowfish', 'userModel' => 'User'));

            $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => true);
            $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'dashboard', 'admin' => true);
            $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
        } else {
            AuthComponent::$sessionKey = 'Auth.Websiteuser';
            $this->Auth->authorize = 'Controller';
            $this->Auth->authenticate = array('Form' => array('fields' => array('username' => 'email', 'password' => 'password'), 'passwordHasher' => 'Blowfish', 'userModel' => 'Websiteuser', 'scope' => array('Websiteuser.useractive' => '1')));

            $this->Auth->loginAction = array('controller' => 'websiteusers', 'action' => 'login');
            $this->Auth->loginRedirect = array('controller' => 'websiteusers', 'action' => 'index');
            $this->Auth->logoutRedirect = array('controller' => 'websiteusers', 'action' => 'index');
        }
    }
	
	function _setErrorLayout() {
		echo '<pre>'; print_r($this);
		if ($this->name == "CakeError") {
			$this->layout = "error";
		}
	}
	
	function beforeRender () {
			//echo $this->here;
			//echo '<pre>';print_r($_SERVER);
			/*if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http'){
				$this->redirect('https://www.fairpockets.com/');
			}*/
			
			//If the HTTPS is not found to be "on"

			
		if($this->here == '/feedback'){	$this->redirect('/feedback/'); }
		if($this->here == '/our-team'){	$this->redirect('/our-team/'); }
		if($this->here == '/property-management'){	$this->redirect('/property-management/'); }
		if($this->here == '/research-reports'){	$this->redirect('/research-reports/'); }
		if($this->here == '/portfolio-management'){	$this->redirect('/portfolio-management/'); }
		
		if($this->here == '/terms-conditions'){	$this->redirect('/terms-conditions/'); }
		if($this->here == '/about-us'){	$this->redirect('/about-us/'); }
		if($this->here == '/faq'){	$this->redirect('/faq/'); }
		if($this->here == '/privacy-policy'){	$this->redirect('/privacy-policy/'); }
		if($this->here == '/advisory'){	$this->redirect('/advisory/'); }
		if($this->here == '/pricing'){	$this->redirect('/pricing/'); }
		
		if($this->here == '/rentvbuy_calc'){	$this->redirect('/rentvbuy-calc/'); }		
		if($this->here == '/searchListDetail/Tk9JXzc='){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzE3MA=='){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzE4MQ=='){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzMxMw=='){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzIwNw=='){	$this->redirect('/'); }
		
		if($this->here == '/searchListDetail/Tk9JXzYz'){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzEyOQ=='){	$this->redirect('/'); }
		if($this->here == '/pages/recentProperties/'){	$this->redirect('/'); }
		if($this->here == '/pages/getbspoptions1'){	$this->redirect('/'); }
		
		
		if($this->here == '/3BHK---Residential-Apartmentfor-Sale?fpid=MjczNzg3NjM1NTI0MzI%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTE5ODk%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTIzNDA%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTIzMjA%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/4BHK-Apartment-for-Sale-in-Sector-78-Noida--Noida?fpid=MjczNzg3NjM1NTIwOTY%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/5BHK-Apartment-for-Sale-in-Sector-78-Noida--Noida?fpid=MjczNzg3NjM1NTIwOTM%3D'){	$this->redirect('/'); }
		
		
		if($this->here == '/builderPropertyDetails/4BHK-Apartment-for-Sale-in-Sector-78-Noida--Noida?fpid=MjczNzg3NjM1NTIwODk%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/4BHK-Apartment-for-Sale-in-Sector-78-Noida--Noida?fpid=MjczNzg3NjM1NTIyNzY%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/3BHK-Apartment-for-Sale-in-Knowledge-Park-I-Greater-Noida-West--Greater-Noida-Greater-Noida?fpid=MjczNzg3NjM1NTIzNzc%3D'){	$this->redirect('/'); }
		if($this->here == '/3BHK-Apartment-for-Sale-in-Meadows-Highrise-Sector-150-Sector-150--Noida--Noida?fpid=MjczNzg3NjM1NTIwODA%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/5BHK-Apartment-for-Sale-in-Sector-78-Noida--Noida?fpid=MjczNzg3NjM1NTIwOTI%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/2BHK-Apartment-for-Sale-in-Sector-93-B-noida-Secter-46-Noida-Noida?fpid=MjczNzg3NjM1NTIxNzE%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/3BHK-Apartment-for-Sale-in-Sector-78-Noida--Noida?fpid=MjczNzg3NjM1NTIwODg%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTIzMTg%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTIzMTc%3D'){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTE5MzQ%3D'){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzE%3D'){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/MjczNzg3NjM1NTIxNTM='){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTIzMDA%3D'){	$this->redirect('/'); }
		if($this->here == '/searchListDetail/Tk9JXzI='){	$this->redirect('/'); }
		if($this->here == '/builderPropertyDetails/MjczNzg3NjM1NTE5MzQ='){	$this->redirect('/'); }
		
		
		
		
		if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http'){
				$this->redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			}
		
		$this->_setErrorLayout();		
	}

    public function isAuthorized($user) {
        return true;
        /*

          // Admin can access every action
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '0') {

          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '1') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '2') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '3') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '4') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '5') {
          return true;
          }
          if (isset($user['employee_role_id']) && $user['employee_role_id'] === '6') {
          return true;
          }
          // Default deny
          return false;
         */
    }

    public function send_mail($email_data = null) {
        //echo "<pre>";print_r($email_data);die;
        //echo WWW_ROOT;die;
        $email = new CakeEmail('default');
        $email_to = $email_data['to'];
        $email_msg = $email_data['body'];
        $email_subject = $email_data['subject'];

        $email->to($email_to);
        $email->subject($email_subject);
        $mail_status = @$email->send($email_msg);

        if (!$mail_status) {
            return FALSE;
        }
        return TRUE;
    }
	
	protected function getSolrconfig() {
		 $config = array(
			"endpoint" => array(
				"localhost" => array(
					//"host"=>"fairpockets.com", // For webmin old
          "host" => '13.232.101.73', // For AWS solr server
					"port"=>"8983", 
					//"path"=>"/solr/farpkt_solr_data_prod"
					//"core"=>"aliveasset"
          'username' => 'fpuser',
          'password' => 'fppass',
					"path"=>"/solr",
					"core"=>"farpkt_solr_data_prod"
				)
			)
		);
    //echo print_r($config);
    return $config;
	}
}

?>