<?php
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'PDF', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
//$pdf->SetTitle('TCPDF Example 006');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(2, 2, 2);
$pdf->SetHeaderMargin(9);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 15);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();

// create some HTML content
//$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
$pdf->Image('https://www.fairpockets.com/upload/builder_logo/'.$builderlogo, 5, 6, 45, 16, 'JPG', 'https://www.fairpockets.com/', '', true, 150, '', false, false, 1, false, false, false);

//echo "<pre>";print_r($port_name).'<br>';
//echo "<pre>";print_r($user_name).'<br>';
$portName=$port_name;
$userName=$user_name['0']['Websiteuser']['username'];
$createdOn=$Created_on;
$sum=0;
$for5year=0;
$for2year=0;
foreach($properties as $portfolioProperty){	
		$prop_mrent=$portfolioProperty['Portfolio']['prop_mrent'];
		$forecast_2year=$portfolioProperty['Portfolio']['forecast_2year'];
		$forecast_5year=$portfolioProperty['Portfolio']['forecast_5year'];
$sum=$sum+$prop_mrent;
$for2year=$for2year+$forecast_2year;
$for5year=$for5year+$forecast_5year;
}

//echo $userName;
//die();
//echo "<pre>";print_r($properties);
$html ='
<table style="border-spacing: 0; border-width: 0; padding: 0; border-width: 0;">
            <tr style="padding-top:20px;">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="https://www.fairpockets.com//img/front/logo.png" style="width:500%; max-width:1000px;">
                            </td>
                            
                           
                        </tr>
                    </table>
                </td>
            </tr>
            
			 <tr style="width: 100%;
            display: block;
            text-align: center;">
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="text-align:center;margin-bottom:0px; padding-bottom: 0px;font-weight: bold;">
                                <span style="font-size:25px;">PORTFOLIO ADVISORY FOR '.$portName.'</span><br>
                                '.$userName.'<br>Created On
								'.$createdOn.'							
                            </td>
                            
						
                        </tr>
                    </table>
                </td>
            </tr>
			
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <span style="font-weight: bold;">PORTFOLIO VALUE CURRENT<br>
                                Rs. '.$sum.' Cr.</span>
                            </td>
                            
							 <td>
                                <span style="font-weight: bold;">PORTFOLIO VALUE in 2 Years<br>
                                Rs. '.$for2year.' Cr.</span>
                            </td>
							
                            <td>
                                <span style="font-weight: bold;">PORTFOLIO VALUE in 5 Years.<br>
                                Rs. '.$for5year.' Cr.</span><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			
            <tr>
                <td>
                   <span style="font-weight: bold;"> PROPERTY </span>
                </td>
                <td>
                   <span style="font-weight: bold;"> Address </span>
                </td>
				<td></td>
             
            </tr>';
			$j=1;
            foreach($properties as $portfolioProperty){	
			$prop_locality=$portfolioProperty['Portfolio']['prop_locality'];
             $html.=' 
			<tr class="spacer">
                <td>
                    '.$j.'.
                </td>
                 <td>
                    '.$prop_locality.'
                </td>
             <td></td>
            </tr>';
			$j++;
			}
			$html .='<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			
			
			';
			$i=1;
			//$sum=0;
		foreach($properties as $portfolioProperty){	
		$prop_mrent=$portfolioProperty['Portfolio']['prop_mrent'];
		$expected_growth_rate=$portfolioProperty['Portfolio']['expected_growth_rate'];
		$forecast_2year=$portfolioProperty['Portfolio']['forecast_2year'];
		$forecast_5year=$portfolioProperty['Portfolio']['forecast_5year'];
		$property_desc=$portfolioProperty['Portfolio']['property_desc'];
		$property_desc1=$portfolioProperty['Portfolio']['property_desc1'];
		$liquidity=$portfolioProperty['Portfolio']['liquidity'];
		$rental=$portfolioProperty['Portfolio']['rental'];
		$advice=$portfolioProperty['Portfolio']['advice'];
		$prop_cfg=$portfolioProperty['Portfolio']['prop_cfg'];
		//$sum=$sum+$prop_mrent;
		//echo $sum.'<br>';
$html.=            '<tr class="heading">
                <td>
                    <h2>PROPERTY '.$i.'</h2>
                </td>
                <td ><span style="padding-top: 22px !important; position:relative;
        font-weight: bold;">Real Estate Investment Profile</span></td>
              
            </tr>';
			$i++;
		
            
    $html.=       
			
			 '<tr >
                            <td>
                                <span style="font-weight: bold;">CURRENT MARKET PRICE</span>
<br>
                               <h5>Rs. '.$prop_mrent.' Cr.</h5>
                            </td>
                            
							<td>
                                <h3>House 1 --- Category A, Flat No. 701, Estate Wadala East, MuLloydmbai
Type: '.$prop_cfg.'.</h3>
                            </td>
						
            </tr>
			
			<tr>
                            <td>
                               <span style="font-weight: bold;"> EXPECTED GROWTH</span>
<br>
                                <h5>Rs. '.$expected_growth_rate.' Cr.</h5>
                            </td>
                            
							<td style="style="position: relative;
		padding-left: 0px !important;"">
                                '.$property_desc.'
                            </td>
						
            </tr>
			
			<tr>
				<td>
					<span style="font-weight: bold;">FORECAST FOR TWO YEARS</span> <br>
					<h5>Rs. '.$forecast_2year.' Cr.</h5>
				</td>
				
				<td>
					<table>
						 <tr>
							<td >
							<span style="font-weight: bold;">Liquidity:</span> $'.$liquidity.'
							</td>
							
							<td>
							  <span style="font-weight: bold;">Rental:</span> $'.$rental.'
							</td>
						</tr>
					</table>
				</td>
						
            </tr>
			
			<tr>
				<td>
					<span style="font-weight: bold;">FORECAST FOR FIVE YEARS</span><br>
					<h5>Rs. '.$forecast_5year.' Cr.</h5>
				</td>
				
				<td>
				   <table>
						 <tr>
							<td style="position: relative;
		padding-left: 0px !important;">
							<p><span style="font-weight: bold;">Advice:</span>  '.$advice.'</p>'.$property_desc1.'
							</td>
							
							<td>
							 <img src="https://www.fairpockets.com//img/front/logo.png" style="    width: 456%;
    max-width: 1000px;
}">
							</td>
						</tr>
					</table>
				</td>
						
            </tr>
			
			
			
        </table>
';
}
	

 

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->AddPage();
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

$txt = <<<EOD
TCPDF Example 002

Default page header and footer are disabled using setPrintHeader() and setPrintFooter() methods.
EOD;

//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'bakup-testing/app/webroot/files/workorders/project-'.$linkid.'.pdf', 'D');

//============================================================+
// END OF FILE
//===