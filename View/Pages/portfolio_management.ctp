<section class="portfolio-management text-center">
    <div class="space-x"></div>
        <div class="container">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="drawer-menu-item button">
            <img src="<?php echo $this->webroot; ?>css/home/images/portfolio-management-services.gif" alt="Portfolio management services">
        </a>
        </div>
</section>
<section class="light-yellow text-center">
    <div class="space-l"></div>
        <div class="container">
		<div class="row">
			<div class="col-md-12">
        <h2>Steps of portfolio Management Service <span><strong>Process</strong></span></h2>
        <div class="space-l"></div>
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/recommendation-report.png" alt="Recommendation report">
                <h3>Portfolio Recommendation Report</h3>
            </div>
            <p>We analyze the multiple data points of each of your property and suggest the best possible recommendation for optimization of your property portfolio</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-society-bills.png" alt="PM society bills">
                <h3>Executing The Recommendation</h3>
            </div>
            <p>We execute the recommendation through mutual consent to maximize gain and meet your desired objective.</p>
        </div>
        </div>
        </div>
        </div>
    <div class="space-l"></div>
    </section>    
	<section class="process text-center">
     <div class="space-l"></div>
        <div class="container">
			<div class="row">
			<div class="col-md-12">
            <h2>Our Portfolio Management <span><strong>Process</strong></span></h2>
            <ul>
                <li class="step1"><strong>Step 1</strong><br>
                    Understanding & clarifying the client objectives</li>
                <li class="step2"><strong>Step 2</strong><br>
                    Property Valuation</li>
                <li class="step3"><strong>Step 3</strong><br>
                    Future estimate of property and portfolio value</li>
                <li class="step4"><strong>Step 4</strong><br>
                    Classification of each property</li>
                <li class="step5"><strong>Step 5</strong><br>
                    Action recommendation on each property based on client objectives</li>
            </ul>
        </div>
        </div>
        </div>
     <div class="space-l"></div>
    </section>


<section class="light-grey text-center">
    <div class="space-l"></div>
    <div class="container">
	<div class="row">
			<div class="col-md-12">
    <h2><span><strong>Benefit</strong></span> of Our Property Management Services</h2>
    <div class="space-l"></div>
    <div class="row">
	<div class="col-md-12 col-md-offset-1">
		<div class="row">
    <div class="col-md-2 col-sm-6 col-xs-12">    
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/saving-time.png" alt="Saving time">
            <p>Data backed decision</p>
        </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/money-saved.png" alt="Money saved">
            <p>Timely action</p>
        </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/regular-inspection.png" alt="Regular inspection">
            <p>Repeated Success in investment</p>
         </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/one-stop-solution.png" alt="One stop solution">
            <p>Lower Risk</p>
        </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/trust-transparency.png" alt="Trust transparency">
            <p>Superior Return</p>

   </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="space-l"></div>
</section>
<section></section>
