<?php echo $this->Html->script('front/jssor.slider-22.2.10.min'); ?>
<style>
    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    .jssora05l.jssora05lds      (disabled)
    .jssora05r.jssora05rds      (disabled)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('img/a17.png') no-repeat;
        overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }
    .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
    .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
</style>
<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<section class="propert-detail">
	<div class="property_detail_header">
		<div class="container">
			<?php foreach($result as $item):
				
				//echo "<pre>"; print_r($item); echo "</pre>";
				
				if($item['transaction_type'] == 'Sell'){
					$transaction_type = 'buy';
					}else{
					$transaction_type = $item['transaction_type'];
				}
				
				$url_address  = (!empty($item['sublocality1'])) ? $item['sublocality1'].'&nbsp;' : '';
				//$url_address .= (!empty($item['city'])) ? $item['city'] : '';
				
				$address  = (!empty($item['sublocality3'])) ? $item['sublocality3'].',&nbsp;' : '';
				$address .= (!empty($item['sublocality2'])) ? $item['sublocality2'].',&nbsp;' : '';
				$address .= (!empty($item['sublocality1'])) ? $item['sublocality1'].',&nbsp;' : '';
				$address .= (!empty($item['city'])) ? $item['city'] : '';
			?>
			<div class="detail_header">
				<div class="row">
					<div class="col-sm-12">
						<ul class="breadcrumb">
							<li><a href="/">Home</a></li>
							<li><a href="/searchList?default_location=<?= $item['city'];?>&property_for=<?php echo $transaction_type;?>&property_name=Property in <?php echo $url_address ;?>">Property in <?php echo $url_address ;?></a> </li>
							<li><?php echo (!empty($item['city'])) ? $item['city'] : '';?></li>
						</ul>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 header-price">
						<div class="offer_price">
							<i class="fa fa-inr" aria-hidden="true"></i><?php echo (!empty($item['offer_price'])) ? $this->Number->format($item['offer_price']) : '0.00';?>
							<?php if(!empty($item['discount_percent'])){?>
								<span class="prooffer">
									<i class="fa fa-arrow-down" aria-hidden="true"></i>
									<?php echo round(($item['market_price'] - $item['offer_price']) / $item['market_price'] * 100, 0); ?>
									% less
								</span>
							<?php } ?>
							<span class="price_sq">@ 
								<?php if(isset($item['super_builtup_area']) && $item['super_builtup_area'] !=''){
                                echo round($item['offer_price']/$item['super_builtup_area'],3);
								}else{
								echo round($item['offer_price']/$item['plot_area'],3);	
								}
								?>
							
							per Sq.Ft</span>
						</div>
						<div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i><?= $this->Number->format($item['market_price']);?>
						@
						<?php if(isset($item['super_builtup_area']) && $item['super_builtup_area'] !=''){
                                echo round($item['market_price']/$item['super_builtup_area'],3);
								}else{
								echo round($item['market_price']/$item['plot_area'],3);	
								}
								?>

								per Sq.Ft</div>
					</div>
					<div class="col-sm-3 text-center header-feature">
						
						<div class="h_feature"><?php echo ($item['configure'] !== false) ? $item['configure'] : $item['configure'] . 'BHK';?> <span class="h_feature_sub"><?php echo $item['property_type'].' for '.$item['transaction_type'];?></span></div>
						<div class="h_location">in <?php echo $address;?></div>
					</div>
					<div class="col-sm-3 text-center header-other-feature">
						
						<div class="h_postdate">Post on <?php echo date('M d, Y', strtotime($item['post_date']));?></div>
						<?php if(!empty($item['availability'])) { ?><div class="h_availability"><?php echo $item['availability'];?></div><?php } ?>
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
	<div class=" property_detail_area">
		<div class="space-m"></div>
		<div class="container">
			<div class=" property_detail_inners">
				
				<div class="row">	
					<div class="col-md-9 col-sm-8 col-xs-12" id="page-content">
						<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                                <!-- Loading Screen -->
                                <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                    <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                </div>
                                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
                                    <a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
                                    <?php
                                    foreach ($propertyImages as $property_value) {
                                        ?>
                                        <div>
                                            <img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                            <img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Thumbnail Navigator -->
                                <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                                    <!-- Thumbnail Item Skin Begin -->
                                    <div data-u="slides" style="cursor: default;">
                                        <div data-u="prototype" class="p">
                                            <div class="w">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                            </div>
                                            <div class="c"></div>
                                        </div>
                                    </div>
                                    <!-- Thumbnail Item Skin End -->
                                </div>
                                <!-- Arrow Navigator -->
                                <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                                <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                            </div>
						<div class="space-m"></div>		
						<div id="detail" class="detail-list">
							<div class="detail-title">
								<h2 class="title-left">Detail</h2>
							</div>
							<ul class="list-three-col">
								
								<li><strong>Super Built Up Area:</strong><?php echo (!empty($item['super_builtup_area'])) ? $item['super_builtup_area'].' Sq.ft':'N/A';?></li>
								<li><strong>Built Up Area:</strong><?php echo (!empty($item['builtup_area'])) ? $item['builtup_area'].' Sq.ft':'N/A';?></li>
								<li><strong>Carpet Area:</strong><?php echo (!empty($item['carpet_area'])) ? $item['carpet_area'].' Sq.ft':'N/A';?></li>
								<li><strong>Plot area:</strong><?php echo (!empty($item['plot_area'])) ? $item['builtup_area'].' Sq.ft':'N/A';?></li>
								<li><strong>Length of plot:</strong><?php echo (!empty($item['plot_length_area'])) ? $item['plot_length_area'].' Sq.ft':'N/A';?></li>
								<li><strong>Width of plot:</strong><?php echo (!empty($item['plot_width_area'])) ? $item['plot_width_area'].' Sq.ft':'N/A';?></li>
								<li><strong>Bedrooms:</strong><?php echo (chop($item['configure'], 'BHK') != false) ? chop($item['configure'], 'BHK') : 'N/A';?></li>
								<li><strong>Bathrooms:</strong><?php echo (!empty($item['num_bathroom'])) ? $item['num_bathroom'] :'N/A';?></li>
								<li><strong>Balconies:</strong><?php echo (!empty($item['num_balcony'])) ? $item['num_balcony'] :'N/A';?></li>
								
							</ul>
						</div>
						<div class="clearfix"></div>
						<div class="space-m"></div>	
						<div id="addtional-detail">
							<div class="detail-title">
								<h2 class="title-left">Additional details</h2>
							</div>
							<ul class="list-three-col list_odetail">
								<li><strong>Project name:</strong> <span><?php echo (!empty($item['project_name'])) ? $item['project_name'] :'No Property ';?></span></li>
								<li><strong>Launch date:</strong> <span><?php echo (!empty($item['post_date'])) ? date('M d, Y', strtotime($item->post_date)) :'No date available';?></span></li>  
								<li><strong>Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i><?php echo (!empty($item['offer_price'])) ? $this->Number->format($item['offer_price']) :'0.00';?></span></li>  
								<li><strong>Flooring:</strong> <span><?php echo (!empty($item['flooring_type'])) ? $item['flooring_type'] :'N/A';?></span></li>
								<li><strong>Age of property:</strong> <span><?php echo (!empty($item['age_of_property'])) ? $item['age_of_property'] :'N/A';?></span></li>
								<li><strong>Furnishing:</strong> <span><?php echo (!empty($item['furnishings'])) ? $item['furnishings'] :'N/A';?></span></li>
								<li><strong>Total floors:</strong> <span><?php echo (!empty($item['total_floors'])) ? $item['total_floors'] :'N/A';?></span></li>
								<li><strong>Property on floor:</strong> <span><?php echo (!empty($item['on_floor'])) ? $item['on_floor'] :'N/A';?></span></li>
								<li><strong>Floors allowed for construction:</strong> <span><?php echo (!empty($item['floors_allowed_for_construction'])) ? $item['flor_construction'] :'N/A';?></span></li>
								<li><strong>Reserved Parking:</strong> <span><?php echo (!empty($item['reserved_parking'])) ? $item['reserved_parking'] :'N/A';?></span></li>
								<li><strong>Covered parking:</strong> <span><?php echo (!empty($item['covered_parking'])) ? $item['covered_parking'] :'N/A';?></span></li>
								<li><strong>Open parking:</strong> <span><?php echo (!empty($item['open_parking'])) ? $item['open_parking'] :'N/A';?></span></li>
								<li><strong>Other rooms:</strong> <span>
								
								<?php
								//echo '<pre>'; print_r($item);
                                $otherroom = '';
                                if (!empty($item['other_room']) && $item['other_room'] == '1') {
                                    $otherroom .= ' Pooja,';
                                }
                                if (!empty($item['other_room2']) && $item['other_room2'] == '1') {
                                    $otherroom .= ' Study,';
                                }
                                if (!empty($item['other_room3']) && $item['other_room3'] == '1') {
                                    $otherroom .= ' Servant,';
                                }
                                if (!empty($item['other_room4']) && $item['other_room4'] == '1') {
                                    $otherroom .= ' Other,';
                                }
                                ?>
								<?php echo (!empty($otherroom)) ? $otherroom :'N/A';?></span>
								</li> 
								<li><strong>Address:</strong> <span><?php echo $address ;?></span></li>
								
								
							</ul>
						</div>
						<div class="clearfix"></div>
						<div class="space-m"></div>	
						<!-- basic detail end
							<div id="description" class="detail-features">
							<div class="detail-title">
							<h2 class="title-left">Description</h2>
							</div>
							<div class="property_des">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</div>
							</div>
							<div class="clearfix"></div>
							<div class="space-m"></div>	
						-->
						<div id="features">
							<div class="detail-title">
								<h2 class="title-left">Features</h2>
							</div>
							<ul class="list-three-col list-features">
								<li><strong>Power Backup:</strong><?php echo (!empty($item['power_backup'])) ? $item['power_backup'] :'N/A';?></li>
								<li><strong>Water Source:</strong> <?php echo (!empty($item['water_source'])) ? $item['water_source'] :'N/A';?></li>
								<li><strong>Facing:</strong> <?php echo (!empty($item['facing'])) ? $item['facing'] :'N/A';?></li>
								<li><strong>Society Type:</strong> <?php echo (!empty($item['society_type'])) ? $item['society_type'] :'N/A';?></li>
								<li><strong>Width of facing road:</strong> <?php echo (!empty($item['width_of_facing_road'])) ? $item['width_of_facing_road'] :'N/A';?></li>
								<li><strong>Overlooking:</strong><?php echo (!empty($item['overlooking'])) ? $item['overlooking'] :'N/A';?></li>
								<li><strong>Bachelors Allowed:</strong><?php echo (!empty($item['bachelors_allowed'])) ? 'Yes' :'No';?></li>
								<li><strong>Pet allowed:</strong><?php echo (!empty($item['pet_allowed'])) ? 'Yes' :'No';?></li>
								<li><strong>Non vegetarian:</strong><?php echo (!empty($item['non_vegetarian'])) ? 'Yes' :'No';?></li>
								<li><strong>Boundary wall:</strong><?php echo (!empty($item['boundary_wall'])) ? 'Yes' :'No';?></li>
								
							</ul>
						</div>
						<div class="clearfix"></div>
						<div class="space-m"></div>	
						<!-- Amenities -->
						<div id="amenitie" class="detail-amenities">
							<div class="detail-title">
								<h2 class="title-left">Amenities</h2>
							</div>
							<div class="row">
								<?php
									//echo '<pre>';print_r($item->amenities);								
									$amenities = explode(',', $item->amenities);
									$icon_array = array(
									'Bank Attached Property'		=> 'lift',
									'Centrally Air Conditioned'		=> 'airc',
									'Club House/Community'			=> 'com_hall',
									'Feng Shui / Vaastu'	        => 'vaastu',
									'Fitness Centre/GYM'			=> 'gym',
									'intercomm_facility'			=> 'intercomm',
									'Internet/wi-fi connectivity'	=> 'internet',
									'Lift'						    => 'lift',
									'Maintenance Staff'				=> 'staff',
									'Park'							=> 'park"',
									'Piped-Gas'						=> 'waste',
									'Piped-gas'						=> 'waste',
									'Rain Water Harvesting'			=> 'rainwater',
									'Security Personnel'			=> 'securityp',
									'Security Fire Alarm'			=> 'security',
									'Shopping /Center'				=> 'shopping',
									'Swimming Pool'					=> 'pool',
									'Visitor Parking'				=> 'parking',
									'Water Purifier'				=> 'water_plant',
									'Water Storage'					=> 'water_storage'
									);
									
								?>
								<div class="col-md-12">
									<ul class=" list-amenities">
										<?php
											foreach($amenities as $key => $val) {
												if(array_search($val, $icon_array) === false) {
													echo '<li><span class="icon_amen icon_'.$icon_array[$val].'">'.$val.'</span></li>';
												}
											}
										?>
									</ul>
								</div>
								
							</div>
						</div>
					</div>	
					<!-- sidebar -->
					<div class="col-md-3 col-sm-4 col-xs-12">
					<?php //echo '<pre>'; print_r($item); ?>
						<div id="contact-biulder">					
							<div class="sidebar-inner">
							<div class="text-center" id="id_contact_succ" style="display:none;"></div>
							<div id="id_contact_form">
								<form class="fpform" id="contact-form" method="post">
									<div class="row">
										<h3>Contact - Fairpockets1</h3>
										<div class="message"></div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input id="user1" class="radio-custom" name="user_type" type="radio" value="individual">
												<label for="user1" class="radio-custom-label">Individual</label>
												<input id="user2" class="radio-custom" name="user_type" type="radio" value="dealer">
												<label for="user2" class="radio-custom-label">Dealer</label>
											</div>
										</div>
										<input class="form-control" name="propertyid" type="hidden" value="<?php echo $item['id']; ?>">
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input class="form-control" name="name" placeholder="Your Name" type="text">
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input class="form-control" placeholder="Phone" name="data[Websiteuser][usermobile]" id="user_mobile" type="text" maxlength="10">
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input class="form-control" placeholder="Email" name="email" type="email">
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<label class="label_title">Interested in (Optional)</label>
												<select id="interested" name="interested[]" class="selectpicker" multiple>
													<option value="assisted search">Assisted Search </option>
													<option value="immediate purchase">Immediate Purchase</option>
													<option value="portfolio management">Portfolio Management</option>
												</select>
												
											</div>	
										</div>	
										<input type="hidden" name="property_id" class="modal-propid">
										<div class="text-center col-sm-12 "><button class="btn btn-primary">Submit</button></div>
										<div class="text-center col-sm-12 " style="padding:10px 0px;">I agree with Fairpockets <a href="#">T&amp;C</a> </div>
									</div>
								</form>
							</div>	
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="space-m"></div>	
			</div>
		</div>
	</div>
</section>

<section id="similer-propertys" class="grid-view">
	<div class="container">
		<div class="detail-title">
			<h2 class="title-left">Similar properties</h2>
		</div>
		<div class="owl-carousel">
			<?php
				
				foreach($similar as $similar_property){
					foreach ($similar_property as $proprerty_list){
						
						
						if($proprerty_list['transaction_type'] == 'Sell'){
							$transaction_type = 'buy';
							}else{
							$transaction_type = $proprerty_list['transaction_type'];
						}
						
						$property_url_address  = (!empty($proprerty_list['sublocality1'])) ? $proprerty_list['sublocality1'].'&nbsp;' : '';
						//$url_address .= (!empty($item['city'])) ? $item['city'] : '';
						$title = (strpos($proprerty_list['configure'], 'BHK') !== false ? $proprerty_list['configure'] : $proprerty_list['configure'] . 'BHK') .' '. $proprerty_list['property_type'] . ' for '. $proprerty_list['transaction_type'];	
						$proprerty_address  = (!empty($proprerty_list['sublocality3'])) ? $proprerty_list['sublocality3'].',&nbsp;' : '';
						$proprerty_address .= (!empty($proprerty_list['sublocality2'])) ? $proprerty_list['sublocality2'].',&nbsp;' : '';
						$proprerty_address .= (!empty($proprerty_list['sublocality1'])) ? $proprerty_list['sublocality1'].',&nbsp;' : '';
						$proprerty_address .= (!empty($proprerty_list['city'])) ? $proprerty_list['city'] : '';
						
						$contact = $proprerty_list['posted_by'];
						if($contact == 'Owner') {
							$contact = 'Contact Owner';
							} else if($contact == 'Broker') {
							$contact = 'Contact Dealer';
							} else {
							$contact = 'Contact Builder';
						}
			?>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
								<h3><i class="fa fa-inr" aria-hidden="true"></i><?php echo (!empty($proprerty_list['offer_price'])) ? $this->Number->format($proprerty_list['offer_price']) : '0.00';?></h3>
								<?php if(!empty($item['discount_percent'])):?>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo ceil($proprerty_list['discount_percent']);?>%</p>
								</div>
								<?php endif; ?>
								<?= $this->Html->link($this->html->image('front/property/02_434x290.jpg', array('alt'=>$title)), array("controller" => false,"action" => "/searchListDetail", base64_encode($item->id)), array('escape' => false, 'title' => $title, 'target' => '_blank'));?>
								
							</figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For <?php echo $proprerty_list['transaction_type'];?></div>
									<span class="label label-danger"><?php echo $proprerty_list['sale_type'];?></span>
								</div>
								<h2 class="property-title">
								<?php
									echo $this->Html->link($title, array("controller"=>false,"action" => "/searchListDetail", base64_encode($proprerty_list->id)), array('escapeTitle' => false, 'title' => $title, 'target' => '_blank'));
								?>
								</h2>
								<h4 class="property-location"><?php echo $proprerty_address;?></h4>
								<div class="pro_tt"><?php echo $title;?></div>
								
								<div class="pro_area"><?php echo ($proprerty_list['super_builtup_area'] ? $proprerty_list['super_builtup_area'].' Sq.ft. Super Built Up Area' : $proprerty_list['builtup_area'].' Sq.ft. Built Up Area');?></div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right">
								<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
					</div>
					<div class="item-foot">
						<div class="item-foot-left">
							<p><i class="fa fa-user"></i> <a href="#"><?php echo $contact; ?></a></p>
						</div>
						<div class="item-foot-right">
							<p><i class="fa fa-calendar"></i> <?php echo date('d-M-Y', strtotime($proprerty_list['post_date']));?></p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				
			</div>
			<?php }
			 }
			?>
		</div>
	</div>		
</section>

<?= $this->Html->script(['validation_error_messages', 'search_list', 'owl-carousel'], ['block' => 'scriptBottom']);?>

<script type="text/javascript">


    jssor_1_slider_init = function () {

        var jssor_1_SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*you can remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>	
<script type="text/javascript">jssor_1_slider_init();</script>

