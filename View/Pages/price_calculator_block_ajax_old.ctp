<style>
.calprice1{
     background: #d62540; 
    padding: 5px 11px;
    font-size: 28px;
    color: #fff;
    margin: 15px;
    display: inline-block;
}
</style>
<?php
$gOfferPrice = $propertydetails['Property']['offer_price'];
$gServiceTax = 0;
if (!empty($all_property_features['47'])) {
    $gSuperBuiltUpArea = $all_property_features['47'];
} else if(!empty($all_property_features['50'])) {
    $gSuperBuiltUpArea = $all_property_features['50'];
}
else{
	$gSuperBuiltUpArea = '';
}
$property_id_test = $this->params->query['fpid'];
$id_test = base64_decode($property_id_test);

$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);



if(!empty($project_pricing)){
	if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
		$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
	}else{
		$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
		
		if(isset($basePriceArr['unit_bsp1']) && $basePriceArr['unit_bsp1'] != ''){
			$gBaseRateUnit = $basePriceArr['unit_bsp1'];
		}else{
			$gBaseRateUnit = 1;
		}
	}
	$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
	$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
	//$gstCreditInput = $project_pricing['ProjectPricing']['price_srtax_icr'];
}
else
{
	$gBaseRate = '';   // Base Rate
	$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
	$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
	//$gstCreditInput = 0;
}
if($project_pricing['ProjectPricing']['price_srtax_icr'] != ''){
	$gstCreditInput = $project_pricing['ProjectPricing']['price_srtax_icr'];
}else{
	$gstCreditInput = 0;
}



/* get project charges array */
if(!empty($project_pricing)){
$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
}
else{ 
$project_pricing_charge = array();
}
// ProjectCharges

$project_charges = array();
for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
    if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
        $pcharge = $project_pricing_charge['price_pcharge' . $i];

        switch ($pcharge) {
            case "1":
                $project_charges[$j]['0'] = "One Covered Car Park";
                break;
            case "2":
                $project_charges[$j]['0'] = "Double Covered Car Park";
                break;
            case "3":
                $project_charges[$j]['0'] = "Club Membership";
                break;
			case "4":
                $project_charges[$j]['0'] = "Total Power Backup Cost";
                break;
            case "13":
                $project_charges[$j]['0'] = "Power BackUp per KVA";
                break;
            case "5":
                $project_charges[$j]['0'] = "Interest Free Maintenance Deposit";
                break;
            case "6":
                $project_charges[$j]['0'] = "Road Facing PLC";
                break;
            case "7":
                $project_charges[$j]['0'] = "Park Facing PLC";
                break;
			case "9":
                $project_charges[$j]['0'] = "Sinking Fund";
                break;
			case "10":
                $project_charges[$j]['0'] = "View PLC 1";
                break;
			case "11":
                $project_charges[$j]['0'] = "View PLC 2";
                break;
			case "12":
                $project_charges[$j]['0'] = "Swimming Pool PLC";
                break;
			case "14":
                $project_charges[$j]['0'] = "Hill View PLC";
                break;
			case "15":
                $project_charges[$j]['0'] = "Additional Car Parking";
                break;			
            default:
                $project_charges[$j]['0'] = "Corner PLC";
        }

        $project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
        $project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
        $project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];
    }
}
/* get additional charges array */
if(!empty($project_pricing)){
$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
}
else{
$project_additional = array();
}
$additon_charges = array();
for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
    if (!empty($project_additional['price_core_amt' . $i])) {
        $acharge = $project_additional['price_core_plc' . $i];

        switch ($acharge) {
            case "1":
                $additon_charges[$j]['0'] = "Lease Rent";
                break;
            case "2":
                $additon_charges[$j]['0'] = "External Electrification Charges";
                break;
            case "3":
                $additon_charges[$j]['0'] = "External development Charges";
                break;
            case "4":
                $additon_charges[$j]['0'] = "Infrastructure development Charges";
                break;
            case "5":
                $additon_charges[$j]['0'] = "Electricity Connection Charges";
                break;
            case "6":
                $additon_charges[$j]['0'] = "Fire fighting charges";
                break;
            case "7":
                $additon_charges[$j]['0'] = "Electric Meter Charges";
                break;
            case "8":
                $additon_charges[$j]['0'] = "Gas Pipeline Charges";
                break;
			case "9":
                $additon_charges[$j]['0'] = "Sinking Fund";
                break;
			case "10":
                $additon_charges[$j]['0'] = "Security  & 1 time connection charges";
                break;
			case "11":
                $additon_charges[$j]['0'] = "Water & sewer connection charges";
                break;
            default:
                $additon_charges[$j]['0'] = "Internal development Charges";
        }

        $additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
        $additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
        $additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
    }
}
/* get other charges array */
if(!empty($project_pricing))
{
$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
}
else{ $project_other = array();}

$other_charges = array();
for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
    if (!empty($project_other['price_other_amt' . $i])) {
        $ocharge = $project_other['price_other_plc' . $i];

        switch ($ocharge) {
            case "1":
                $other_charges[$j]['0'] = "Other Charges1";
                break;
            case "2":
                $other_charges[$j]['0'] = "Other Charges2";
                break;
            case "3":
                $other_charges[$j]['0'] = "Other Charges3";
                break;
            case "4":
                $other_charges[$j]['0'] = "Other Charges4";
                break;
            case "5":
                $other_charges[$j]['0'] = "Other Charges5";
                break;
            case "6":
                $other_charges[$j]['0'] = "Other Charges6";
                break;
            case "7":
                $other_charges[$j]['0'] = "Other Charges7";
                break;
            case "8":
                $other_charges[$j]['0'] = "Other Charges8";
                break;
            default:
                $other_charges[$j]['0'] = "Other Charges9";
        }

        $other_charges[$j]['1'] = $project_other['price_other_type' . $i];
        $other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
        $other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
    }
}
$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
$gaProjectChargesLen = count($gaProjectCharges);


// Project Pricing - Stamp Duty
if(!empty($project_pricing))
{
$gaStampDuty = array(
    //[ option(1-2) , amount ]

    array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
);

// Project Pricing - Registration
$gaRegistration = array(
    // [ option(1-2) , amount ]
    array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
);
}
else{
$gaStampDuty = array();
$gaRegistration = array();
}
// Project Pricing - Floor PLC
$gaTotalFloors = $projectdetail['ProjectDetails']['num_of_tow']; // Total number of floor in the tower of property
if(!empty($project_pricing))
{
if ($project_pricing['ProjectPricing']['floor_per_plc_amount'] != '') {
    $gaFloorPLC = array(
        // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
        array($project_pricing['ProjectPricing']['floor_option'], $project_pricing['ProjectPricing']['floor_per_plc_amount'], $project_pricing['ProjectPricing']['floor_per_option'])
            //array( 2 , 10 , 1 )
    );
} else {
    $gaFloorPLC = array(
        // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
        array($project_pricing['ProjectPricing']['floor_option'], 0, $project_pricing['ProjectPricing']['floor_per_option'])
            //array( 2 , 10 , 1 )
    );
}
}
else
{
	 $gaFloorPLC = array();
}


$gaMandatoryCharges = '';
$gaOptionalCharges = '';
$gaOptionalCount = 0;
$ProjectCharges = 0;
$gaFloorPLCCharges = 0;

if(!empty($gaFloorPLC))
{
if ($gaFloorPLC[0][1] > 0) {
    $gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

    if ($gaFloorPLC[0][0] == 2) {
        // BSP w.r.t Ground floor
        $tempFloor = 0; //0 - Ground Floor
        $gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
    } else {
        // BSP w.r.t TOP floor;
        $tempFloor = 0; //0 - Ground Floor
        $gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
    }

    if ($gaFloorPLC[0][2] == 2) {
        // sub option
        $gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
    }

    $gaMandatoryCharges = $gaMandatoryCharges . "
					<div class='col-sm-12'>
						<label for='' class='checkbox-custom-label'>
				";
    $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
            'Per Floor Charges' . "<span class='plc_amount884'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . number_format($gaFloorPLC[0][1]) . "&nbsp;/ sq ft</span>
					";
    $gaMandatoryCharges = $gaMandatoryCharges . "
						</label>  
					</div>
			";
}
}
//$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
$gOfferPrice = $gOfferPrice + $BaseCharge;
$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
//echo "<br><br>BaseCharge - ".$BaseCharge;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
// Project Charges
$ProjectCharges = 0;
for ($row = 0; $row < $gaProjectChargesLen; $row++) {
    //echo "<br><br>".$gaMandatoryCharges;
    if (1 == $gaProjectCharges[$row][1]) {
        // Mandatory to include the charges

        $gaMandatoryCharges = $gaMandatoryCharges . "
						<div class='col-sm-12'>
							<label for='' class='checkbox-custom-label'>
				";

        //Calculate Charges
		//echo $gaProjectCharges[$row][0];
		//echo $propertydetails['Property']['power_backup_kva'];
		if($gaProjectCharges[$row][0] == 'Power BackUp per KVA'){
			if($propertydetails['Property']['power_backup_kva'] != ''){
				$MandatoryChargeAmt = $gaProjectCharges[$row][2]*$propertydetails['Property']['power_backup_kva'];
			}else{
				$MandatoryChargeAmt = $gaProjectCharges[$row][2];
			}
		}else{
		
        $MandatoryChargeAmt = $gaProjectCharges[$row][2];
		}
        if (1 == $gaProjectCharges[$row][3]) {
            // Per Square feet 	
            $ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

            // Display Charge in Mandatory Section
            $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'>&nbsp;<i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;&nbsp;" . number_format($MandatoryChargeAmt) . "&nbsp;/ sq ft</span>
					";
        } else {
            // Per Unit 	
            $ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

            // Display Charge in Mandatory Section
            $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount884'>&nbsp;&nbsp;<i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;&nbsp;" . number_format($MandatoryChargeAmt) . "</span>
					";
        }

        $gaMandatoryCharges = $gaMandatoryCharges . "
							</label>  
						</div>
				";

        //echo "<br><br>	ProjectCharges - ".$ProjectCharges;
    } else {
        // Optional to include the charges
        $gaOptionalCount = $gaOptionalCount + 1;
        $tempid = "optional11" . $gaOptionalCount;

        $gaOptionalCharges = $gaOptionalCharges . "
						<div class='col-sm-12'>
						    
				";

        //Calculate Charges
        $OptionalChargeAmt = $gaProjectCharges[$row][2];
        $tempChargeAmt = 0;
        if (1 == $gaProjectCharges[$row][3]) {
            // Per Square feet 	
            $tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

            // Display Charge in Optional Section
            $gaOptionalCharges = $gaOptionalCharges . "
						<input id='" . $tempid . "' class='checkbox-custom' name='optional11[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
						<label for='" . $tempid . "' class='checkbox-custom-label'>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . number_format($OptionalChargeAmt) . "&nbsp;/ sq ft</span>
					";
        } else {
            // Per Unit 	
            $tempChargeAmt = $OptionalChargeAmt;

            // Display Charge in Mandatory Section
            $gaOptionalCharges = $gaOptionalCharges . "
					
						<input id='" . $tempid . "' class='checkbox-custom' name='optional11[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
						<label for='" . $tempid . "' class='checkbox-custom-label'>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . number_format($OptionalChargeAmt) . "</span>
					";
        }

        $gaOptionalCharges = $gaOptionalCharges . "
							
							</label>  
						</div>
				";
    }

    //echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
}

$gProjectCharges_a = $ProjectCharges;

$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
//echo "<br><br>gServiceTax - ".$gServiceTax;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
//Stamp Duty
$StampDuty = 0;
if(!empty($gaStampDuty)){
if (2 == $gaStampDuty[0][0]) {
    // Percentage
    $StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
} else {
    // Flat Charges
    $StampDuty = $gaStampDuty[0][1];
}
}
else{
$StampDuty = '';
}
$gOfferPrice = $gOfferPrice + $StampDuty;

//echo "<br><br>StampDuty - ".$StampDuty;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
//Registration Charges
$Registration = 0;
if(!empty($gaRegistration)){
if (2 == $gaRegistration[0][0]) {
    // Percentage
    $Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
} else {
    // Flat Charges
    $Registration = $gaRegistration[0][1];
}
}
else{
$Registration = '';
}
$gOfferPrice = $gOfferPrice + $Registration;

//echo "<br><br>Registration - ".$Registration;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;


?>

<?php

//echo $this->Html->script('highcharts');
//echo $this->Html->script('highcharts-3d');

$gLng = $project['Project']['lng'];
$gLat = $project['Project']['lat'];
$gTitle = $project['Project']['project_name'] . ',' . $project['Project']['locality'] . ', ' . $project['Project']['city_data'];
echo "
        <input type='hidden' id='id_gLat' value='" . $gLat . "'>
        <input type='hidden' id='id_gLng' value='" . $gLng . "'>
        <input type='hidden' id='id_gTitle' value='" . $gTitle . "'>
    ";
?>  

<div class="">
										<div class=" col-md-12">
											<div class="detail-title">
												<h2  class="title-left">Project Price Calculator884</h2>
											</div>
										</div>
										<div id="amenities" class="detail-amenities detail-block target-block col-md-12" style="margin-top:0px;margin-bottom:0px;padding-top:0px;">

											<div class="row">
												<div class="col-sm-6">
													<label for="" class="form-group"><strong> Property area : </strong> <?php echo $gSuperBuiltUpArea ?>&nbsp;sq ft </label>
												</div>
												<div class="col-sm-6">
													<label for="" class="form-group"><strong> Base Rate : </strong> <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;  
														 
														<?php if($gBaseRateUnit == 1){ ?>
														<input name="bsp_price_value" id="bsp_price_value" value="<?php echo $gBaseRate. '&nbsp;/ sqft'; ?>" style="width:35%; border-bottom:none;">
														
														<?php } ?>
														
														<?php if($gBaseRateUnit == 2){ ?>
														<input name="bsp_price_value" id="bsp_price_value" value="<?php echo $gBaseRate. '&nbsp;'; ?>" style="width:35%; border-bottom:none;">
														
														<?php } ?>
														</label>
														
														<input type="hidden" name="bsp_price_value_unit" id="bsp_price_value_unit" value="<?php echo $gBaseRateUnit; ?>">
														<input type="hidden" name="bsp_price_value_plc" id="bsp_price_value_plc" value="<?php echo $project_plc_amount ?>">

												</div>

											</div>

											<div class="row">
												<div class="col-sm-6">
													<input type="hidden" name="gst_input_credit" id="gst_input_credit" value="<?php echo $gstCreditInput; ?>">
													<label for="" class="form-group"><strong> GST(on Base) : </strong> <?php echo $gServiceTaxOnBSP ?>&nbsp;&#37; </label>
												</div>

												<div class="col-sm-6">
													<label for="" class="form-group"><strong> GST(on Others) : </strong> <?php echo $gServiceTaxOnOTH ?>&nbsp;&#37; </label>
												</div>								
											</div>

											<div class="row">
												<div class="col-sm-6" style="padding-left:10px;">
												
													<div class="col-sm-12" style="padding-left:1px;">
													
										
														<input id="regcheckclick1" class="checkbox-custom" value="<?php echo $gaRegistration[0][1]; ?>" type="checkbox" onclick="ReCalculateOfferPrice9(this);">
														<label for= "regcheckclick1" class="checkbox-custom-label">
															Registration Charges
														<span>
														<?php
															if(!empty($gaRegistration)){
															if (2 == $gaRegistration[0][0]) {
																// Percentage
																echo $gaRegistration[0][1] . "&nbsp;&#37";
															} else {
																// Flat Charges
																echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaRegistration[0][1];
															}
															}
														?>
														</span>
											
													
													</label> 
													<input type="hidden" id="regCheckValue" value="0" >							
												</div>
												
												
												</div>
												<?php if(!empty($gaStampDuty) && $gaStampDuty[0][1]!=0){ ?>
												<div class="col-sm-6">
													<label for="" class="form-group"><strong>Stamp Duty : </strong> 
														<?php
														//if(!empty($gaStampDuty)){
														if (2 == $gaStampDuty[0][0]) {
															// Percentage
															echo $gaStampDuty[0][1] . "&nbsp;&#37";
														} else {
															// Flat Charges
															echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaStampDuty[0][1];
														}
														//}
														?>
													</label>
												</div>
												<?php } ?>
											</div>
											<!-- If no plc charges ,  donot display the below div -->
										</div>
									</div>
									<div class="col-md-6">
										<div class="detail-title">
											<h2 class="title-left"> Mandatory Charges</h2>
										</div> 
										<div class="form-group">
											<div class="row" id="id_mandatory_charges">

												<?php echo $gaMandatoryCharges; ?>

											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="detail-title">
											<h2 class="title-left">Optional Charges</h2>
										</div> 
										<div class="form-group ">
											<div class="row" id="id_optional_charges11">

												<?php echo $gaOptionalCharges; ?>

											</div>
										</div>										
										<?php //echo '<pre>';print_r($bsp_limit);
											if(!empty($gaStampDuty)){ ?>
											<div class="row" <?php //if ($gaFloorPLC[0][1] <= 0) echo "style='display:none;'" ?> >
												<div class="col-sm-6">
													<div class="form-group">
														<label for="" class="label_title"><strong>Floor Choice </strong></label>
														<div class="select-style">
															<div class="input select">
															<?php
																//echo '<pre>'; print_r($bsp_limit);
															?>
																	<select id="mySelect" onchange="myFunction1()">
																	<!--<option value="0" selected="selected" >Ground</option>-->
																	<?php
																	
																	for ($opt = $bsp_limit[0][0]['startloop']; $opt <= $bsp_limit[0][0]['endloop']; $opt++) {
																		echo '<option value=\'' . $opt . '\'>' . $opt . '</option>';
																	}
																	?>
																</select>
															</div>
														</div>	
													</div>	
												</div>
											</div>	
											<?php }?>
									</div>

									<div class="col-md-12 text-center">

									<div class="caltotprice1">
										<div class="caltitle">Your Final Calculated Price</div>
										<span class="calprice1" id="id_calprice1">
										<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo round($gOfferPrice, 2); ?>
										</span>
										<div class="caltitle">
											<span id="id_calprice1_words">
												<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;
											</span> 
										</div>
										<div id="id_chart"></div>
										<div id="containerajax1" style="width:100%; height:300px;" >

										</div>	
									</div>

								</div>
							

<?php //$projectid = $project['Project']['project_id']; ?>
<?php $projectid = $project['Project']['project_id']; ?>
<input type='hidden' id='projectid' value='<?php echo $projectid; ?>'>
<script>
function myFunction(){			
            var id = document.getElementById("mySelect").value;
			var projectid = document.getElementById("projectid").value;
			//alert(id);
			//alert(projectid);
			var dataString = 'id=' + id + '&projectid=' + projectid;
			$.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "pages", "action" => "getbspoptions1")); ?>',
                    data: dataString,
                    success: function (data) {
						//alert(data);
						//data = JSON.parse(data);
						var obj = jQuery.parseJSON(data);
						//alert(data[1].bsp_charge);
						//alert(obj.bsp_charge_unit);
						if(obj.bsp_charge_unit == 1){
							$('#bsp_price_value').val(obj.bsp_charge+' / Sqft');
						}
						if(obj.bsp_charge_unit == 2){
							$('#bsp_price_value').val(obj.bsp_charge);
						}
                    	//$('#bsp_price_value').val(obj.bsp_charge);
						$('#bsp_price_value_unit').val(obj.bsp_charge_unit);
						$('#bsp_price_value_plc').val(obj.plc_charge);
						//$('#bsp_price_value').val(data);
						//alert(obj.plc_charge);
						//alert(obj.bsp_charge);
						//alert(obj.bsp_charge_unit);
						//alert(obj.plc_charge);						
                        ReCalculateOfferPrice81(obj.bsp_charge,obj.bsp_charge_unit,obj.plc_charge);
						
                    }
                });
}
</script>

<?php
// Generate client side offer price calculator part

echo "
		<input type='hidden' id='id_gProjectChargesAmt' value='" . $gProjectCharges_a . "'>
	";
if(!empty($gaStampDuty)){
if (2 == $gaStampDuty[0][0]) {
    // Percentage
    echo "<input type='hidden' id='id_gaStampDuty' name='percent' value='" . $gaStampDuty[0][1] . "'>";
} else {
    // Flat Charges
    echo "<input type='hidden' id='id_gaStampDuty' name='amount' value='" . $gaStampDuty[0][1] . "'>";
}
}

if(!empty($gaRegistration)){
if (2 == $gaRegistration[0][0]) {
    // Percentage
    echo "<input type='hidden' id='id_gaRegistration' name='percent' value='" . $gaRegistration[0][1] . "'>";
} else {
    // Flat Charges
    echo "<input type='hidden' id='id_gaRegistration' name='amount' value='" . $gaRegistration[0][1] . "'>";
}
}

if(!empty($gaFloorPLC)){ 
$gafplc = $gaFloorPLC[0][0];
$gafplc1 = $gaFloorPLC[0][1];
$gafplc2 = $gaFloorPLC[0][2];
} else{ $gafplc ='';$gafplc1 ='';$gafplc2 ='';}

echo "
	
		<script>
		
		    var BaseCharges = 0;
			var ServiceTaxCharges = 0;
			var GovtCharges = 0;
			var ProjectCharges = 0;

			function ReCalculateOfferPrice11( elem )
			{
				//document.getElementById('id_calprice1').innerHTML = 1234;
				var BaseCharges = 0;
				var GovtCharges = 0;
				alert('hello11');
				
				//tBaseRate = 3510;
				//alert(brate);
				//tBaseRate1 = parseInt(document.getElementById('bsp_price_value').value);
				//alert(tBaseRate1);
				//area - 1215
				//tBaseRate = " . $gBaseRate . ";
				tBaseRate = parseInt(document.getElementById('bsp_price_value').value);
				tBaseRateUnit = parseInt(document.getElementById('bsp_price_value_unit').value);
				//alert(tBaseRateUnit); //- 4650
				
				tPlcRate = parseInt(document.getElementById('bsp_price_value_plc').value);
				
				tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
				
				tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
				tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
				
				eStampDuty = document.getElementById('id_gaStampDuty');
				eRegistration = document.getElementById('regCheckValue');
				
				tBSPForFloor = " . $gafplc . ";
				tBSPForFloorCharges = " . $gafplc1 . ";
				tBSPForFloorOption = " . $gafplc2 . ";
				
				gstInputCredit = ".$gstCreditInput.";
				//alert(gstInputCredit);
				gstInputCreditAmount = (tBaseRate *".$gSuperBuiltUpArea.") * gstInputCredit / 100;
				//alert(gstInputCreditAmount);
				
				if ( tBaseRateUnit == 1 ){
					tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				}
				if ( tBaseRateUnit == 2 ){
					tOfferPriceAmt = tBaseRate - gstInputCreditAmount ;
				}
				
	              //alert(tOfferPriceAmt);  
				tFloorPLC = 0;
				
				if ( tBSPForFloorCharges > 0 )
				{	
					tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
					
					if ( tBSPForFloor == 2 )
					{
						// BSP w.r.t Ground floor
						tempFloor = parseInt(document.getElementById('id_floor_choice1').value) ; //0 - Ground Floor
						tFloorPLC = tFloorPLC * tempFloor ;
						
					}
					else
					{
						// BSP w.r.t TOP floor;
						tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice1').value) + 1; //0 - Ground Floor
						tFloorPLC = tFloorPLC * tempFloor ;
					}			
					
					if ( tBSPForFloorOption == 2 )
					{
						// sub option
						tFloorPLC = tFloorPLC*(-1);
					}	

				}
				
				nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
				BaseCharges += nOfferPriceAmt;
				
				ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
				nOfferPriceAmt = nOfferPriceAmt + (nOfferPriceAmt * (tServiceTaxOnBSP / 100)) ;
				//alert(nOfferPriceAmt.toFixed(2));
				if ( elem )
				{	
					// Change in optional element
					
					if ( true == elem.checked )
					{	
						//alert('Checked  - '+elem.value);
						tProjectChargesAmt = tProjectChargesAmt + parseInt(elem.value);
						document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
						
					}	
					else
					{	
						//alert('Unchecked - '+elem.value);
						tProjectChargesAmt = tProjectChargesAmt - parseInt(elem.value);
						document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
					}
				}
				else
                {
					// Change in choice of floor
					
					tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                } 					
				
				plcAmountArea = tPlcRate*".$gSuperBuiltUpArea.";
				plcAmountGst = (tPlcRate*".$gSuperBuiltUpArea.")*tServiceTaxOnOTH/100;
				plcAmountTotal = plcAmountArea + plcAmountGst;
				//alert(plcAmountTotal);
				
				nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
				//alert(nServiceChargesAmt);
				nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt + plcAmountTotal ; 	
				//alert(nOfferPriceAmt);
				if ( 'amount' == eStampDuty.name )
				{
					GovtCharges += parseInt(eStampDuty.value);
					nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
				}
				else
				{
					GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
					nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
				}		
				
				if ( 'amount' == eRegistration.name )
				{
					GovtCharges +=  parseInt(eRegistration.value);
					nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
					
				}
				else
				{
					GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
					nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt + plcAmountArea ) * (parseInt(eRegistration.value)/100) );
					//alert(nOfferPriceAmt);
				}
				
				//alert('Final Offer Price ='+nOfferPriceAmt);
				

				
						
				document.getElementById('id_calprice1').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits: 2});
				//document.getElementById('id_calprice1').innerHTML = 1234;
				
				nOfferPriceAmt = Math.abs(nOfferPriceAmt)
            if (nOfferPriceAmt >= 10000000)
            {
                formattedNumber = (nOfferPriceAmt / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (nOfferPriceAmt >= 100000)
            {
                formattedNumber = (nOfferPriceAmt / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (nOfferPriceAmt >= 1000)
            {
                formattedNumber = (nOfferPriceAmt / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = nOfferPriceAmt;
            }
				
				document.getElementById('id_calprice1_words').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+formattedNumber;
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
				                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
				                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
															   'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
															   'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

				$('#containerajax1').highcharts({
				 
						chart: {
							type: 'pie',
							options3d: {
								enabled: true,
								alpha: 45
							}
						},
						title: {
							text: 'Property Charges'
						},
						tooltip: {
							//pointFormat: '{series.name}: <b>{point.value}%</b>'
						},
						plotOptions: {
							pie: {
								innerSize: 100,
								depth: 45
							}
						},
						series: [{
							data: [
								['Base Charge', eval(BaseCharges.toFixed(2)) ],
								['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
								['Government Charges', eval( GovtCharges.toFixed(2) )],
								['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
								['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))	 ]
							]
							
						}]
				});	
				
				//$('.place-desc-large').find(':first-child').first().text('new text');
				//$('.place-desc-large DIV:first-child').attr('html', 'abc');
				//$('.place-name' ).html( 'background-color');
				//$('.place-desc-large div.place-name').text('MY NEW TEXT');
			}
			
			ReCalculateOfferPrice11();
			
		</script>

	";
?>	 
<!-- jQuery Form Validation code and submit lead/contact information-->
 
<?php
//echo $this->Html->script('add_property_7');
// Jquery Script for Validation of form fields    
//echo $this->Html->script('validate_1.9_jquery.validate.min');

?>


<?php

echo "
    
        <script>
        
            var BaseCharges = 0;
            var ServiceTaxCharges = 0;
            var GovtCharges = 0;
            var ProjectCharges = 0;

            function ReCalculateOfferPrice81(elem,elem1,elem2)
            {
                
                var BaseCharges = 0;
                var GovtCharges = 0;
                
                tBaseRate = elem;
				tBaseRateUnit = elem1;
				tPlcRate = elem2;
                //alert(tPlcRate);
                
            
                //tBaseRate = " . $gBaseRate . ";
                tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
                
                tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
                
                eStampDuty = document.getElementById('id_gaStampDuty');
                eRegistration = document.getElementById('regCheckValue');
                
                tBSPForFloor = " . $gafplc . ";
                tBSPForFloorCharges = " . $gafplc1 . ";
                tBSPForFloorOption = " . $gafplc2 . ";
				
				gstInputCredit = ".$gstCreditInput.";
				//alert(gstInputCredit);
				gstInputCreditAmount = (tBaseRate *".$gSuperBuiltUpArea.") * gstInputCredit / 100;
				//alert(gstInputCreditAmount);
	
	            //tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				
				if ( tBaseRateUnit == 1 ){
					tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				}
				if ( tBaseRateUnit == 2 ){
					tOfferPriceAmt = tBaseRate - gstInputCreditAmount ;
				}
				
    
                //tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
                //alert(tOfferPriceAmt);
                    
                tFloorPLC = 0;
                
                if ( tBSPForFloorCharges > 0 )
                {   
                    tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
                    
                    if ( tBSPForFloor == 2 )
                    {
                        // BSP w.r.t Ground floor
                        tempFloor = parseInt(document.getElementById('id_floor_choice1').value) ; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                        
                    }
                    else
                    {
                        // BSP w.r.t TOP floor;
                        tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice1').value) + 1; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                    }           
                    
                    if ( tBSPForFloorOption == 2 )
                    {
                        // sub option
                        tFloorPLC = tFloorPLC*(-1);
                    }   

                }
                
                nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
                BaseCharges += nOfferPriceAmt;
                
                ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
                nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
                //alert(nOfferPriceAmt);
                
                    // Change in choice of floor
                    
                    tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                //} 
				//alert(tServiceTaxOnOTH);
				plcAmountArea = ".$gSuperBuiltUpArea."*tPlcRate;
				plcAmountGst = (".$gSuperBuiltUpArea."*tPlcRate)*tServiceTaxOnOTH/100;
				plcAmountTotal = plcAmountArea + plcAmountGst;
				//alert(plcAmountTotal);

                nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
                nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt + plcAmountTotal ;     
                //alert(nOfferPriceAmt);
                if ( 'amount' == eStampDuty.name )
                {
                    GovtCharges += parseInt(eStampDuty.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
                }
                else
                {
                    GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
                    nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
                }       
                
                if ( 'amount' == eRegistration.name )
                {
                    GovtCharges +=  parseInt(eRegistration.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
                }
                else
                {
                    GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
                    nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt + plcAmountArea ) * (parseInt(eRegistration.value)/100) );
                }
                
                alert('Final Offer Price ='+nOfferPriceAmt);
                

                
                        
                document.getElementById('id_calprice1').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits: 2});
                
				nOfferPriceAmt = Math.abs(nOfferPriceAmt)
            if (nOfferPriceAmt >= 10000000)
            {
                formattedNumber = (nOfferPriceAmt / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (nOfferPriceAmt >= 100000)
            {
                formattedNumber = (nOfferPriceAmt / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (nOfferPriceAmt >= 1000)
            {
                formattedNumber = (nOfferPriceAmt / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = nOfferPriceAmt;
            }
				
				document.getElementById('id_calprice1_words').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+formattedNumber;
				
                
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
                                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
                                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
                                                               'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
                                                               'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

                $('#containerajax1').highcharts({
                 
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Property Charges'
                        },
                        tooltip: {
                            //pointFormat: '{series.name}: <b>{point.value}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [{
                            data: [
                                ['Base Charge', eval(BaseCharges.toFixed(2)) ],
                                ['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
                                ['Government Charges', eval( GovtCharges.toFixed(2) )],
                                ['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
                                ['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))   ]
                            ]
                            
                        }]
                }); 
                
                //$('.place-desc-large').find(':first-child').first().text('new text');
                //$('.place-desc-large DIV:first-child').attr('html', 'abc');
                //$('.place-name' ).html( 'background-color');
                //$('.place-desc-large div.place-name').text('MY NEW TEXT');
            }
            
            //ReCalculateOfferPrice();
            
        </script>

    ";

    ?>
	
	
	
	<?php

echo "
    
        <script>
        
            var BaseCharges = 0;
            var ServiceTaxCharges = 0;
            var GovtCharges = 0;
            var ProjectCharges = 0;

            function ReCalculateOfferPrice91(elem)
            {
                
                var BaseCharges = 0;
                var GovtCharges = 0;
                
                tBaseRate = parseInt(document.getElementById('bsp_price_value').value);
				tBaseRateUnit = parseInt(document.getElementById('bsp_price_value_unit').value);
				
				tPlcRate = parseInt(document.getElementById('bsp_price_value_plc').value);
				
				alert(tBaseRate);
				alert(tPlcRate);
				
				//tRegCheck = 
				
                //alert(tPlcRate);
                
            
                //tBaseRate = " . $gBaseRate . ";
                tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
                
                tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
                
                eStampDuty = document.getElementById('id_gaStampDuty');
                //eRegistration = document.getElementById('id_gaRegistration');
				
				//eRegistration = document.getElementById('regCheckValue');
				
				
				
				if (elem)
				{	
					// Change in optional element
					
					if ( true == elem.checked )
					{	
						//alert('Checked  - '+elem.value);
						eRegistration = parseInt(elem.value);
						document.getElementById('regCheckValue').value = eRegistration;
						
					}	
					else
					{	
						eRegistration = 0;
						document.getElementById('regCheckValue').value = eRegistration;
					}
				}
				else
                {
					// Change in choice of floor
					
					eRegistration = 0;
                }
				
				//alert(eRegistration);
                
                tBSPForFloor = " . $gafplc . ";
                tBSPForFloorCharges = " . $gafplc1 . ";
                tBSPForFloorOption = " . $gafplc2 . ";
				
				gstInputCredit = ".$gstCreditInput.";
				//alert(gstInputCredit);
				gstInputCreditAmount = (tBaseRate *".$gSuperBuiltUpArea.") * gstInputCredit / 100;
				//alert(gstInputCreditAmount);
	
	            //tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
    
                //tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
                //alert(tOfferPriceAmt);
				
				if ( tBaseRateUnit == 1 ){
					tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				}
				if ( tBaseRateUnit == 2 ){
					tOfferPriceAmt = tBaseRate - gstInputCreditAmount ;
				}
                    
                tFloorPLC = 0;
                
                if ( tBSPForFloorCharges > 0 )
                {   
                    tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
                    
                    if ( tBSPForFloor == 2 )
                    {
                        // BSP w.r.t Ground floor
                        tempFloor = parseInt(document.getElementById('id_floor_choice1').value) ; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                        
                    }
                    else
                    {
                        // BSP w.r.t TOP floor;
                        tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice1').value) + 1; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                    }           
                    
                    if ( tBSPForFloorOption == 2 )
                    {
                        // sub option
                        tFloorPLC = tFloorPLC*(-1);
                    }   

                }
                
                nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
                BaseCharges += nOfferPriceAmt;
                
                ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
                nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
                //alert(nOfferPriceAmt);
                
                    // Change in choice of floor
                    
                    tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                //} 
				//alert(tServiceTaxOnOTH);
				plcAmountArea = ".$gSuperBuiltUpArea."*tPlcRate;
				plcAmountGst = (".$gSuperBuiltUpArea."*tPlcRate)*tServiceTaxOnOTH/100;
				plcAmountTotal = plcAmountArea + plcAmountGst;
				//alert(plcAmountTotal);

                nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
                nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt + plcAmountTotal ;     
                //alert(nOfferPriceAmt);
                if ( 'amount' == eStampDuty.name )
                {
                    GovtCharges += parseInt(eStampDuty.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
                }
                else
                {
                    GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
                    nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
                }       
                
                if ( 'amount' == eRegistration.name )
                {
                    GovtCharges +=  parseInt(eRegistration.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
                }
                else
                {
                    GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration)/100);
                    nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt + plcAmountArea ) * (parseInt(eRegistration)/100) );
                }
                
                //alert('Final Offer Price ='+nOfferPriceAmt);
                

                
                        
                document.getElementById('id_calprice1').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits: 2});
                
				nOfferPriceAmt = Math.abs(nOfferPriceAmt)
            if (nOfferPriceAmt >= 10000000)
            {
                formattedNumber = (nOfferPriceAmt / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (nOfferPriceAmt >= 100000)
            {
                formattedNumber = (nOfferPriceAmt / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (nOfferPriceAmt >= 1000)
            {
                formattedNumber = (nOfferPriceAmt / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = nOfferPriceAmt;
            }
				
				document.getElementById('id_calprice1_words').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+formattedNumber;
				
                
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
                                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
                                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
                                                               'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
                                                               'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

                $('#containerajax1').highcharts({
                 
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Property Charges'
                        },
                        tooltip: {
                            //pointFormat: '{series.name}: <b>{point.value}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [{
                            data: [
                                ['Base Charge', eval(BaseCharges.toFixed(2)) ],
                                ['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
                                ['Government Charges', eval( GovtCharges.toFixed(2) )],
                                ['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
                                ['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))   ]
                            ]
                            
                        }]
                }); 
                
                //$('.place-desc-large').find(':first-child').first().text('new text');
                //$('.place-desc-large DIV:first-child').attr('html', 'abc');
                //$('.place-name' ).html( 'background-color');
                //$('.place-desc-large div.place-name').text('MY NEW TEXT');
            }
            
            //ReCalculateOfferPrice();
            
        </script>

    ";

    ?>
	
	
	
	