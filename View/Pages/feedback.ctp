<?php echo $this->Html->script('validate_1.9_jquery.validate.min'); ?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#formID").validate({
            rules: {
                mobile: {
                    required: true
                }
            }

        })
    });
</script>
<style>
.career .form input{max-width:none;}
.green{
    color: green;
    text-align: center;
    font-weight: bold;
    padding: 18px;
}
</style>
<section class="feedback">
    <div class="space-m"></div>
    <div class="container">
<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="text-center">Feedback</h1>
		<hr/>
		<?php  echo $this->Session->flash(); ?>
        <p>Please share your valuable feedback to serve you better</p>
    </div>

    <?php echo $this->Form->create('Feedback', array(
			'url' => array('controller' => '/', 'action' => 'feedback'),
			"type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>
        <div class="form feedback">
            <div class="col-md-6 col-md-offset-3">  
                <div class="form-list">
                    <label>Name*</label>
                    <?php echo $this->Form->input('name', array('div' => false, 'label' => false,'title' => 'Enter Name', 'class' => 'small required', 'placeholder' => 'Enter Name')); ?>
                </div>
                <div class="form-list">
                    <label>Email*</label>
                    <?php echo $this->Form->input('email', array('div' => false, 'label' => false,'title' => 'Enter Email', 'class' => 'small required', 'placeholder' => 'Enter Email')); ?>
                </div>
                <div class="form-list">
                    <label>Feedback*</label>
                    <?php echo $this->Form->input('feedback_comments', array('div' => false, 'label' => false,'title' => 'Enter Feedback', 'class' => 'small required', 'placeholder' => 'Enter Feedback')); ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-list text-center">
                    <button type="submit" class="btn btn-primary" name="Submit" id="submit">Submit</button>
                </div>
            </div>
        </div>    
    </form>      
</div>
    </div>
<div class="space-l"></div>
</section>
