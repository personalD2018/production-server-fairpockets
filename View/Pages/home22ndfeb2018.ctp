<!--Home Banner-->
<section class="home-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="search-left">
                    <h1>Search Fair Price Properties</h1>
                    <p>Get prices upto <strong class="theme-yellow">15% lower</strong> than market</p>
					
					<?php 
						$this->Form->inputDefaults(array(
						'label' => false,
						'div' => false,
						//'class' => 'fancy'
						)
						);
					echo $this->Form->create(null, array('url' => '/searchList', 'class' => 'search-form', 'type' => 'get'));?>
                    <!--form class="search-form"-->
					<div class="row">
						<div class="form-group col-md-2">
							<?php 
								echo $this->Form->input('default_location', array(
								'label' 	=> false, 
								'div' 		=> false, 
								'hidden'	=> true,
								'id' 		=> 'defaultLocation',
								));
								$sizes = array('rent' => 'Rent', 'buy' => 'Buy', 'project' => 'New Project');

								echo $this->Form->input('property_for', array(
								'options' => $sizes, 
								'default' => 'buy', 
								'class' => 'selectpicker show-tick', 
								'label' => false, 
								'div' => false, 
								'id' =>'inputpropertyfor'
								));
							?>
						</div>
						
						<div class="form-group col-md-4 property-type">
							<?php 
								//$sizes = array('rent' => 'Rent', 'buy' => 'Buy', 'project' => 'Project');
								$options = array(
									'All Residential' => array(
										'Apartment' 				=> 'Apartment',
										'Studio Apartment' 			=> 'Studio Apartment',
										'Residential Land' 			=> 'Residential Land',
										'Independent Builder Floor' => 'Independent Builder Floor',
										'Independent House' 		=> 'Independent House',
										'Independent Villa' 		=> 'Independent Villa',
										'Farm House'				=> 'Farm House'
										
									),
									'All Commercial' => array(
										'Commercial Office Space' 	=> 'Commercial Office Space',
										'Office In IT Park' 		=> 'Office In IT Park',
										'Commercial Shop' 			=> 'Commercial Shop',
										'Commercial Showroom' 		=> 'Commercial Showroom',
										'Commercial Land' 			=> 'Commercial Land',
										'Industrial Land' 			=> 'Industrial Land',
										'Agricultural Land' 		=> 'Agricultural Land',
										'Factory' 					=> 'Factory',
										'Ware House' 				=> 'Ware House'
									)
								);
								echo $this->Form->input('property_type[]', array(
									'options' => $options, 
									//'default' => array('Apartment', 'Commercial Office Space'), 
									'class' => 'selectpicker show-tick', 
									'label' => false, 
									'div' => false, 
									'id' =>'inputpropertytype', 
									'data-select-all-headers' => 'true',
									'multiple',
									//'data-actions-box' => true,
									//'data-live-search' => trues
								));
							?>
						</div>
						<div class="form-group col-md-4">
							<input type="text" id="PropertyNotifictionPropertyName" name="property_name" class="form-control" placeholder="Search for Property">
						</div>
						<div class="form-group col-md-2">
							<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="banner">
				<img src="<?php echo $this->webroot; ?>css/home/images/home-banner-pic.png" alt="Fair pockets banner">
			</div>
		</div>
	</div>
</div>

</section>

<section class="fair-process">
    <div class="container">
        <div class="space-l"></div>
        <h1>Fair Pocket Process</h1>
        <hr>
        <img src="<?php echo $this->webroot; ?>css/home/images/steps3.png" alt="Fair Pocket Process">	
        <div class="space-l"></div>
	</div>
</section>

<!--Fair Price Decription-->
<section class="price light-yellow text-center">
    <div class="container">
        <div class="space-xl"></div>
        <div class="row">
            <div class="col-md-4">
                <h3>Advertised Price</h3>
                <p>Price at which sellers usually advertise the property. In most cases, this price is higher than the market price and is negotiable.</p>
			</div>	
            <div class="col-md-4">
                <h3>Market Price</h3>
                <p>The market price is the current price at which that particular property can be bought or sold. Our research team arrives at this price by carrying out our own desktop valuation.</p>
			</div>	
            <div class="col-md-4">
                <h3>Lowest Price</h3>
                <p>This is an exceptionally low distress price quoted by the seller when there is an urgent need to sell the property due to his own personal reasons.</p>
			</div>	
            <div class="col-md-12">
                <div class="space-l"></div>
                <h2 class="color-red">Fair Price</h2>
                <p style="font-size: 17px;">The fair price is a Fairpockets recommended <strong>offer price</strong> which helps the seller break the market and sell the property in a shorter timeframe.<br>
				This price can be below or at par with the market price of the property. This price takes into account the factors such as oversupply of properties in a certain locality, condition of the property, seller&#96;s urgency and comfort level etc and thus creates a win-win situation by attracting the buyers and enabling the sellers to sell fast.</p>
			</div>        
		</div>
        <div class="space-xl"></div>
	</div>
</section>

<!--Why Use Fairpockets-->
<section class="why-fairpocket">
    <div class="space-l"></div>
    <div class="container">
        <div class="row fairpocket fair">
            <div class="col-md-12">
                <h1>Why Use Fairpockets</h1>
                <hr>
			</div>
            <div class="col-md-6">
                <h3>Buyer</h3>
                <ul>
                    <li>
                        <div><img src="/css/home/images/icon/fair-price.png" alt="Fair Price"></div>
                        <h4>Fair Price</h4>
                        <p>Get Fairly Priced resale Properties</p>
					</li>
                    <li>
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/transparent-pricing.png" alt="Transparent Pricing"></div>
                        <h4>Transparent Pricing</h4>
                        <p>Get price breakup for builder properties</p>
					</li>
                    <li> 
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/fair-builder.png" alt="Fair Builder"></div>
                        <h4>Fair Builder</h4>
                        <p>We allow quality builders to post property with us</p>
					</li>
                    <li>
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/selling-assistance.png" alt="Buying Assistance"></div>
                        <h4>Buying Assistance</h4>
                        <p>Get assistance in buying through our Assisted Search</p>
					</li>
				</ul>
			</div>
            <div class="col-md-6">
                <h3>Seller</h3>
                <ul>
                    <li>
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/free-valuation.png" alt="Free Valuation"></div>
                        <h4>Free Valuation</h4>
                        <p>Get free valuation support of your property</p>
					</li>
                    <li>
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/sell-fast.png" alt="Sell Fast"></div>
                        <h4>Sell Fast</h4>
                        <p>We help you sell fast</p>
					</li>
                    <li>
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/transparent-pricing.png" alt="Price Recommendation"></div>
                        <h4>Price Recommendation</h4>
                        <p>Get right price recommendation for selling fast</p>
					</li>
                    <li>
                        <div><img src="<?php echo $this->webroot; ?>css/home/images/icon/selling-assistance.png" alt="Selling Assistance"></div>
                        <h4>Selling Assistance</h4>
                        <p>Get Assistance in Selling through our Assisted Search</p>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
	<div class="space-l"></div>
</section>


<section class="property-listing grid-view recent-properties"></section>

<section class="services_details">
    <div class="space-l"></div>
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom">
            <h1>Our Services</h1>
            <hr>
		</div>
        <div class="our-products">
            <div class="row ser_row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="product-title no-margin">
                        <div class="product-image">
                            <a href="<?php echo $this->webroot; ?>advisory">
                                <img src="<?php echo $this->webroot; ?>css/home/images/search.png" class="img-responsive" alt="Services"/>
							</a>
						</div>
					</div>
				</div>
				
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h2>Assisted Search</h2>
                    <div class="product-content">
                        <ul class="DescriptMain">
                            <li>We help you in searching your prospective buyer or seller in the fastest possible time.</li>
                            <li><b>Assisted Sell/Rent for sellers:</b> Outsource your selling & renting responsibilities to us for faster results</li>
                            <li><b>Assisted Buy/ Rent for buyers:</b> Get trusted properties at fair price from Fair Pockets</li>
						</ul>
                        <div class="btn-group">
                            <button onclick="location.href = '<?php echo $this->webroot; ?>advisory'" type="button" class="btn btn-success">Learn More</button>
							
                            <?php
								if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) {
								?>		
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-primary margin-left-5" >Request Service</a>
								<?php }
							?>
							
						</div>
					</div>
				</div>
			</div>
            <div class="space-xl"></div>
            <div class="space-xl"></div>
			
            <div class="row ser_row"> 
                <div class="col-md-3 col-sm-3 col-push-md-3 align-right">
                    <div class="product-title no-margin">
                        <div class="product-image">
                            <a href="<?php echo $this->webroot; ?>research-reports"><img src="<?php echo $this->webroot; ?>css/home/images/focus.png" class="img-responsive" alt="Research reports"/></a>
						</div>
					</div>
				</div>
				
                <div class="col-md-9 col-sm-9 col-push-md-9">
                    <div class="product-content">
                        <h2>Our Research Focus</h2>          
                        <p>Get to know about your property investment before committing. Our report cover important insights which are very important for both end user and investor.</p>
                        <p><strong>Facts we cover:</strong> Check about the builder past records, area livability, carpet area, density and recommendation, expected rental in the future, appreciation potential of the property, 5 year projected estimated price etc
						</p>
                        <div class="btn-group">
                            <button onclick="location.href = '<?php echo $this->webroot; ?>research-reports'" type="button" class="btn btn-success">Learn More</button>
                            <button onclick="location.href = '<?php echo $this->webroot; ?>samplereport.pdf'" type="button" class="btn btn-primary margin-left-5" target="_blank" >Sample Report</button>
							
						</div>
					</div>
				</div>
				
                
				
			</div>       
            <div class="space-xl"></div>
            <div class="space-xl"></div>
            <div class="row ser_row">
                <div class="col-md-3 col-sm-3 col-push-md-3">
                    <div class="product-title no-margin">
                        <div class="product-image">
                            <a href='<?php echo $this->webroot; ?>property-management'><img src="<?php echo $this->webroot; ?>css/home/images/property-management.png" class="img-responsive" alt="Property management"/></a>
						</div>
					</div>
				</div>
				
                <div class="col-md-9 col-sm-9 col-push-md-9">
                    <h2>Property Management</h2>
                    <div class="product-content">
                        <p>Get a local caretaker to manage your property by keeping it well managed through our services. We undertake maintenance and renting services including tasks like interiors, paying utility bills and other such services for NRI’s, outstation owners and Investors.</p>
                        <div class="btn-group">
                            <button onclick="location.href = '<?php echo $this->webroot; ?>property-management'" type="button" class="btn btn-success">Learn More</button>
							
                            <?php
								if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) {
								?>		
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-primary margin-left-5" >Request Service</a>
								<?php }
							?>
							
						</div>
					</div>
				</div>
			</div>
            <div class="space-xl"></div>
            <div class="space-xl"></div>
            <div class="row ser_row">
                <div class="col-md-3 col-sm-3 col-push-md-3 align-right">
                    <div class="product-title no-margin">
                        <div class="product-image">
                            <a href='<?php echo $this->webroot; ?>portfolio-management'><img src="<?php echo $this->webroot; ?>css/home/images/portfolio-management.png" class="img-responsive" alt="Portfolio management"/></a>
						</div>
					</div>
				</div>
                <div class="col-md-9 col-sm-9 col-push-md-9">
                    <h2>Portfolio Management</h2>
                    <div class="product-content">
                        <p>We will optimize your current property portfolio and help you decide whether to sell or hold or reinvest to grow the pie</p>
                        <ul class="DescriptMain">
                            <li><b>Portfolio Recommendation Report:</b> Get recommendation on sell or hold for each of your property based on data and detailed analysis.</li>
                            <li><b>Executing Recommendation:</b> We execute the mutual decision of selling or renting out or reinvestment or property management for each property</li>
						</ul>
                        <div class="btn-group">
                            <button onclick="location.href = '<?php echo $this->webroot; ?>portfolio-management'" type="button" class="btn btn-success">Learn More</button>
							
                            <?php
								if (empty($this->Session->read('Auth.Websiteuser')) && empty($this->Session->read('Auth.User'))) {
								?>		
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-primary margin-left-5" >Request Service</a>
								<?php }
							?>
						</div>
					</div>
				</div>
				
			</div>
			<div class="space-xl"></div>
            <div class="space-xl"></div>
		</div>
		
	</div>
</div>
</section>
<script>
	$(document).ready(function () {
        $(".select-property-type").click(function () {
            $(".property-checklist").slideToggle("fast");
		});
		
		// $('#inputpropertytype').multiselect({
			// enableClickableOptGroups: true
		// });
		
		// for some strange reason selectpicker prevents the click-event. so just use mouseup
		// when clicking on an optgroup "label", toggle it's "children"
		$(document).on("mouseup", ".bootstrap-select .dropdown-header", function (){
			var $optgroup = $(this),
				$ul = $optgroup.closest("ul"),
				optgroup = $optgroup.data("optgroup"),

				// options that belong to this optgroup
				$options = $ul.find("[data-optgroup="+optgroup+"]").map(function(i, v) {
					if(!$(this).hasClass('selected')) {
						return $(v).text();
					}
				});//.not($optgroup);

				console.log($options);
			// show/hide options
			//$options.toggle();
			$('#inputpropertytype').selectpicker('val', $options);

			//$optgroup.toggleClass("closed");
		});

		// initially close all optgroups that have the class "closed"
		/*$(document).on("loaded.bs.select", function (){
			$(this).find(".dropdown-header.closed").each(function (){
				var $optgroup = $(this),
					$ul = $optgroup.closest("ul"),
					optgroup = $optgroup.data("optgroup"),

					// options that belong to this optgroup
					$options = $ul.find("[data-optgroup="+optgroup+"]").not($optgroup);

				// show/hide options
				$options.toggle();
			});
		});*/
		
		
		// $('.selectpicker').on('changed.bs.select', function (e) {
			// console.log('hello');
			// do something...
		// });
		
		// $('.dropdown-header').click(function () {
			// console.log('hello');
			// var x = '*[data-optgroup="' + $(this).data('optgroup') + '"]';
			// console.log(x);
			// $('.property-type').find(x).addClass('selected');
		// });
		
		$('.owl-carousel').owlCarousel({
			loop: true,
			items: 1,
			margin: 10,
			lazyLoad: true,
			nav:true,
			navigation: true,
			navText: [' ', ' '],
			dots: false,
			responsiveClass: true,
			autoplay:true,
			autoplayTimeout:3000,
			autoplayHoverPause:false,
			responsive: {
				0: {
					items: 1,
					nav: true
				},
				600: {
					items: 2,
					nav: true
				},
				1000: {
					items: 4,
					nav: true,
				}
			}
			
		});
		
	});

    function check_uncheck_checkbox1(isChecked) {
        if (isChecked) {
            $('input[name="Residential"]').each(function () {
                this.checked = true;
			});
			} else {
            $('input[name="Residential"]').each(function () {
                this.checked = false;
			});
		}
	}
	
    function check_uncheck_checkbox2(isChecked) {
        if (isChecked) {
            $('input[name="Commercial"]').each(function () {
                this.checked = true;
			});
			} else {
            $('input[name="Commercial"]').each(function () {
                this.checked = false;
			});
		}
	}
	
    $(document).mouseup(function (e)
    {
        var container = $(".property-checklist");
        var container2 = $(".select-property-type");
		
        if ((!container.is(e.target) && container.has(e.target).length === 0) && (!container2.is(e.target) && container2.has(e.target).length === 0))
        {
            $(".property-checklist").slideUp("fast");
		}
	});
	
	
</script>	