<section id="main-slider" class="no-margin">
    <div class="carousel slide">
        <div class="carousel-inner">

            <div class="item active" style="background-image: url(<?php echo $this->webroot; ?>img/front/slider/bannerbg.jpg)">


                <!--search-main-->	
                <div class="container clearfix"  >
                    <div class="row searchBox" id="searchBox1">
                        <div class="col-xs-12 col-sm-12 searchBoxContent">
                            <div class="mobile-hide"><img src="<?php echo $this->webroot; ?>img/front/topsec_infograph.png" style="max-width:100%;   "></div>
                            <!-- <h2>Search Fair Price Property</h2> -->
                            <ul class="nav nav-tabs" style=" margin-top: 80px;">
                                <li class="active"><a data-toggle="tab" href="#buytab">BUY</a></li>
                                <li><a data-toggle="tab" href="#renttab">RENT</a></li>
                                <li><a data-toggle="tab" href="#projecttab">PROJECT</a></li>
                            </ul>
                            <div class="tab-content searchMain_tab clearfix">
                                <div id="buytab" class="tab-pane fade in active">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <form >
                                                <div class="input-group" id="adv-search">
                                                    <script type="text/javascript">
                                                        $(document).ready(function () {
                                                            $('#protype1').multiselect({
                                                                enableClickableOptGroups: true
                                                            });
                                                        });
                                                    </script>

                                                    <select name="protype" id="protype1" multiple="multiple">
                                                        <optgroup label="All Residential" >
                                                            <option value="Residential-1" selected>Apartment/Studio Spartment </option>
                                                            <option value="Residential-2" selected>Residential Land</option>
                                                            <option value="Residential-3" selected>Independent Builder Floor</option>
                                                        </optgroup>
                                                        <optgroup label="All Commercial">
                                                            <option value="commercial-1">Commercial office space</option>
                                                            <option value="commercial-2">Office in IT Park </option>
                                                            <option value="commercial-3">Commercial shop/showroom</option>
                                                        </optgroup>
                                                    </select>
                                                    <input type="text" name="proname" class="form-control sea_loc_field" placeholder="Search for Property" />
                                                    <div class="input-group-btn">
                                                        <div class="btn-group" role="group">
                                                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--./row-->

                                </div>

                                <div id="renttab" class="tab-pane fade">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group" id="adv-search">
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('#protype2').multiselect({
                                                            enableClickableOptGroups: true
                                                        });
                                                    });
                                                </script>
                                                <select id="protype2" multiple="multiple">
                                                    <optgroup label="All Residential">
                                                        <option value="Residential-1" selected>Apartment/Studio Spartment </option>
                                                        <option value="Residential-2" selected>Residential Land</option>
                                                        <option value="Residential-3" selected>Independent Builder Floor</option>
                                                    </optgroup>
                                                    <optgroup label="All Commercial">
                                                        <option value="commercial-1">Commercial office space</option>
                                                        <option value="commercial-2">Office in IT Park </option>
                                                        <option value="commercial-3">Commercial shop/showroom</option>
                                                    </optgroup>
                                                </select>
                                                <input type="text" class="form-control sea_loc_field" placeholder="Search for Property" />
                                                <div class="input-group-btn">
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--./row-->

                                </div>
                                <div id="projecttab" class="tab-pane fade">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group" id="adv-search">
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('#protype3').multiselect({
                                                            enableClickableOptGroups: true
                                                        });
                                                    });
                                                </script>
                                                <select id="protype3" multiple="multiple">
                                                    <optgroup label="All Residential">
                                                        <option value="Residential-1" selected>Apartment/Studio Spartment </option>
                                                        <option value="Residential-2" selected>Residential Land</option>
                                                        <option value="Residential-3" selected>Independent Builder Floor</option>
                                                    </optgroup>
                                                    <optgroup label="All Commercial">
                                                        <option value="commercial-1">Commercial office space</option>
                                                        <option value="commercial-2">Office in IT Park </option>
                                                        <option value="commercial-3">Commercial shop/showroom</option>
                                                    </optgroup>
                                                </select>
                                                <input type="text" class="form-control sea_loc_field" placeholder="Search for Property" />
                                                <div class="input-group-btn">
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--./row-->

                                </div>
                            </div><!--./tab-content-->

                        </div>
                    </div><!--./row-->
                    <div class="mobile-hide"><img src="<?php echo $this->webroot; ?>img/front/steps.png" style="max-width:100%;    margin-top: 90px;"></div>
                    <div class="desktop-hide"><img src="<?php echo $this->webroot; ?>img/front/steps-m.png" style="max-width:100%;    margin-top: 18px;"></div>

                </div>
                <!--./search-main-->


            </div>
            <!--/.item-->
        </div>
        <!--/.carousel-inner-->
    </div>
    <!--/.carousel-->

</section>
<!--/#main-slider-->


<section class="benefit_sec  col-md-12">
    <div class="container">
        <!-- <div class="wow fadeInDown no-padding-bottom">
<h2 class="sec_title"></h2>
</div> -->

        <div class="row">
            <div class="col-md-6">
                <div class="content_area">
                    <h2>Benefit to Buyer</h2>
                    <ul>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Get the lowest rates</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Deal directly with the sellers</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Get the lowest rates</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Deal directly with the sellers</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Get the lowest rates</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Deal directly with the sellers</li>
                    </ul>
                </div>


            </div>

            <div class="col-md-6">

                <div class="content_area">
                    <h2>Benefit to Seller</h2>
                    <ul>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Reach out to investors across India</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Sell fast</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Optimize own investment</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Reach out to investors across India</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Sell fast</li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Optimize own investment</li>
                    </ul>
                </div>

            </div>
        </div>

    </div>
</section>
<!--./advertisement-Card-->

<section class="property-listing grid-view  col-md-12">
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom">
            <h2 class="sec_title">Recent Properties</h2>
        </div>
        <div class="row">
            <div class="item-wrap">
                <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">


                                <div class="price hide-on-list">

                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                </div>
                                <a href="#" class="hover-effect">
                                    <img src="<?php echo $this->webroot; ?>img/front/property/01_434x290.jpg" alt="thumb">
                                </a>

                            </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">

                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
                                    <span class="label label-danger">Sold</span>
                                </div>
                                <h2 class="property-title"><a href="view_detail_builder.html">3 Bedroom 4 Baths</a></h2>
                                <h4 class="property-location">Sunworld Vanalika</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
                                <div class="pro_tt">Residential Apartment for Sale</div>

                                <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                            </div>


                        </div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                            </div>
                            <div class="info-row phone text-right">
                                <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
                            </div>
                        </div>
                        <div class="table-list full-width hide-on-list">
                            <div class="cell">

                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="item-wrap">
                <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">


                                <div class="price hide-on-list">

                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                </div>
                                <a href="#" class="hover-effect">
                                    <img src="<?php echo $this->webroot; ?>img/front/property/01_434x290.jpg" alt="thumb">
                                </a>

                            </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">

                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
                                    <span class="label label-danger">Sold</span>
                                </div>
                                <h2 class="property-title"><a href="view_detail.html">3 Bedroom 4 Baths</a></h2>
                                <h4 class="property-location">Sunworld Vanalika</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
                                <div class="pro_tt">Residential Apartment for Sale</div>

                                <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                            </div>


                        </div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                            </div>
                            <div class="info-row phone text-right">
                                <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
                            </div>
                        </div>
                        <div class="table-list full-width hide-on-list">
                            <div class="cell">

                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="item-wrap">
                <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">


                                <div class="price hide-on-list">

                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                </div>
                                <a href="#" class="hover-effect">
                                    <img src="<?php echo $this->webroot; ?>img/front/property/01_434x290.jpg" alt="thumb">
                                </a>

                            </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">

                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
                                    <span class="label label-danger">Sold</span>
                                </div>
                                <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                <h4 class="property-location">Sunworld Vanalika</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
                                <div class="pro_tt">Residential Apartment for Sale</div>

                                <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                            </div>


                        </div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                            </div>
                            <div class="info-row phone text-right">
                                <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
                            </div>
                        </div>
                        <div class="table-list full-width hide-on-list">
                            <div class="cell">

                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="item-wrap">
                <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">


                                <div class="price hide-on-list">

                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                </div>
                                <a href="#" class="hover-effect">
                                    <img src="<?php echo $this->webroot; ?>img/front/property/01_434x290.jpg" alt="thumb">
                                </a>

                            </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">

                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
                                    <span class="label label-danger">Sold</span>
                                </div>
                                <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                <h4 class="property-location">Sunworld Vanalika</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
                                <div class="pro_tt">Residential Apartment for Sale</div>

                                <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                            </div>


                        </div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                            </div>
                            <div class="info-row phone text-right">
                                <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
                            </div>
                        </div>
                        <div class="table-list full-width hide-on-list">
                            <div class="cell">

                            </div>

                        </div>
                    </div>
                </div>

            </div>




        </div></div>
</section>
<section class="services_details no-margin col-md-12">
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom">
            <h2 class="sec_title">Our Services</h2>
        </div>

        <div class="our-products">
            <div class="row ser_row">
                <div class="col-md-6 col-push-md-6">
                    <h2 class="product-title no-margin">
                        <a class="" href="javascript:void(0)">Our Research Focus</a>
                    </h2>
                    <div class="product-content">
                        <p> We categorize the information in our research reports into two brackets, 
                            for end-use purpose and for Investment-purpose. For End-Use Purpose, 
                            we track metrics like area livability, carpet area efficiency of the project, 
                            density in the project, distances.</p>

                        <div class="btn-group">
                            <button type="button" class="btn learnMoreBTN btn-green">Learn More</button>
                            <button type="button" class="btn learnMoreBTN btn-red margin-left-5">  View Report&nbsp;</button>
                        </div>

                    </div>


                </div>
                <!--./col-6-->
                <div class="col-md-6 col-push-md-6">
                    <div class="product-image">
                        <a href="javascript:void(0)"><img src="<?php echo $this->webroot; ?>img/front/1f81cab.png" class="img-responsive" /></a>
                    </div>

                </div>
                <!--./col-6-->
            </div>
            <!--/.row-->

            <hr class="lineSpacing"/>

            <div class="row ser_row">
                <div class="col-md-6 col-push-md-6">
                    <div class="product-image">
                        <a href="javascript:void(0)"><img src="<?php echo $this->webroot; ?>img/front/property-management.png" class="img-responsive"/></a>
                    </div>

                </div>
                <!--./col-6-->
                <div class="col-md-6 col-push-md-6">
                    <h2 class="product-title no-margin">
                        <a class="" href="javascript:void(0)">Assisted Search</a>
                    </h2>
                    <div class="product-content">
                        <ul class="DescriptMain">
                            <li><b>Assisted Buy/Sell:</b> Some information about the service then a button of Assisted Search (buy or sell), after clicking.</li>
                            <li><b>After clicking Assisted Search:</b> Register now button with detail of assisted buy sell. Pricing details can also be put here.</li>
                            <li>After registration it goes to login and fill up details of either requirement for buying or property details for selling purpose.</li>
                        </ul>

                        <div class="btn-group">
                            <button type="button" class="btn learnMoreBTN btn-green">Learn More</button>
                            <button type="button" class="btn learnMoreBTN btn-red margin-left-5">  View Report&nbsp;</button>
                        </div>
                    </div>
                </div>
                <!--./col-6-->


            </div>
            <!--/.row-->

            <hr class="lineSpacing"/>
            <div class="row ser_row">
                <div class="col-md-6 col-push-md-6">
                    <h2 class="product-title no-margin">
                        <a class="" href="javascript:void(0)">Portfolio Management</a>
                    </h2>
                    <div class="product-content">
                        <p> Our portfolio management service is for those investors who either have multiple properties 
                            or want to transact actively in real estate in order to increase their net worth. 
                            It is highly focused and time consuming task in which many factors have to be taken into 
                            consideration before reaching to a decision.</p>

                        <div class="btn-group">
                            <button type="button" class="btn learnMoreBTN btn-green">Learn More</button>
                            <button type="button" class="btn learnMoreBTN btn-red margin-left-5">  View Report&nbsp;</button>
                        </div>
                    </div>
                </div>
                <!--./col-6-->
                <div class="col-md-6 col-push-md-6">
                    <div class="product-image">
                        <a href="javascript:void(0)"><img src="<?php echo $this->webroot; ?>img/front/portfolio_img.png" class="img-responsive"/></a>
                    </div>

                </div>
                <!--./col-6-->
            </div>
            <!--/.row-->

            <hr class="lineSpacing"/>

            <div class="row ser_row">
                <div class="col-md-6 col-push-md-6">
                    <div class="product-image">
                        <a href="javascript:void(0)"><img src="<?php echo $this->webroot; ?>img/front/advisory_srevices.png" class="img-responsive"/></a>
                    </div>

                </div>
                <!--./col-6-->
                <div class="col-md-6 col-push-md-6">
                    <h2 class="product-title no-margin">
                        <a class="" href="javascript:void(0)">Property Management</a>
                    </h2>
                    <div class="product-content">
                        <ul class="DescriptMain">
                            <li><b>Property Management:</b> Some information about the service and then start now button</li>
                            <li><b>After clicking start now:</b> there would be registration details page and some info on the property management page along with pricing for property management</li>
                            <li>Once he registers then goes to the property management section post login.</li>
                        </ul>
                        <div class="btn-group">
                            <button type="button" class="btn learnMoreBTN btn-green">Learn More</button>
                            <button type="button" class="btn learnMoreBTN btn-red margin-left-5">  View Report&nbsp;</button>
                        </div>

                    </div>
                </div>
                <!--./col-6-->


            </div>



        </div>
        <!--/.container-->
    </div>
</section>

<section id="services" class="service-item col-md-12">
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom">
            <h2 class="sec_title">Our Approach Towards Your Investment</h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="media services-wrap wow fadeInDown">
                    <div class="text-center my_details">
                        <img class="img-ico" src="<?php echo $this->webroot; ?>img/front/pt_1.png"/>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Project Detail</h3>
                        <p>This being the foremost consideration of any investor, we aspire to 
                            generate higher than the market gains. We donot limit ourselves to a 
                            locality or city  <span id="read_1" class="collapse"> be a good source of regular recurring returns. We have a full fledged infrastructure and support system to tackle all issues related to property management and maintenance which, beyond doubt, enhances the monetary worth of assets.</span><a href="#read_1" data-toggle="collapse" class="darkbg">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="media services-wrap wow fadeInDown">
                    <div class="text-center my_details">
                        <img class="img-ico" src="<?php echo $this->webroot; ?>img/front/pt_2.png"/>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Minimum Risk</h3>
                        <p>Risk in property investment would certainly be the last nightmare any 
                            investor would like to have. Therefore we focus on ‘No or Minimal Risk’ options only <span id="read_2" class="collapse"> be a good source of regular recurring returns. We have a full fledged infrastructure and support system to tackle all issues related to property management and maintenance which, beyond doubt, enhances the monetary worth of assets.</span><a href="#read_2" data-toggle="collapse" class="darkbg">Read More</a></p>
                    </div>
                </div> 
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="media services-wrap wow fadeInDown">
                    <div class="text-center my_details">
                        <img class="img-ico" src="<?php echo $this->webroot; ?>img/front/pt_3.png"/>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Trustworthy</h3>
                        <p>We generate confidence and trust in our clients by keeping utmost transparency in our dealings with them. 
                            Our clients also understand that we are not just be a good source of <span id="read_3" class="collapse"> be a good source of regular recurring returns. We have a full fledged infrastructure and support system to tackle all issues related to property management and maintenance which, beyond doubt, enhances the monetary worth of assets.</span><a href="#read_3" data-toggle="collapse" class="darkbg">Read More</a></p>

                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="media services-wrap wow fadeInDown">
                    <div class="text-center my_details">
                        <img class="img-ico" src="<?php echo $this->webroot; ?>img/front/pt_4.png"/>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Manage and Maintain</h3>
                        <p>The post purchase maintenance and management of properties is always a big concern especially for 
                            persons with multiple and scattered investments <span id="read_4" class="collapse"> be a good source of regular recurring returns. We have a full fledged infrastructure and support system to tackle all issues related to property management and maintenance which, beyond doubt, enhances the monetary worth of assets.</span><a href="#read_4" data-toggle="collapse" class="darkbg">Read More</a></p>
                    </div>
                </div>
            </div>


        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</section>
<!--/#services-->

