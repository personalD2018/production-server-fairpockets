<?php

echo $this->Html->css('front/simple-slider');
echo $this->Html->script('highcharts');
echo $this->Html->script('slider.min');
?>
<section class="calculator">
    <div class="container">
        <div class="duser_menu  sidebar" role="navigation">
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1>EMI Calculator </h1>
                    </div>
                </div>	
                <div id="build" class="list-detail detail-block target-block">
                    <div class="add-tab-row push-padding-bottom row-color-gray">
						<div class="row">
							<div class="col-md-3">

								<div class="form-group">
									<label>Loan Amount is <strong><span class="fa fa-inr" ></span>&nbsp;<span id="la_value">100000</span></strong></label>	
									<input type="text" data-slider="true" value="100000" data-slider-range="100000,25000000" data-slider-step="10000" data-slider-snap="true" id="la">
									<div id="id_la_txt" style="font-weight:bold;"></div>
								</div>

							</div>
						</div>

						<div class="row">
							<div class="col-md-3">

								<div class="form-group">
									<label>No. of Month(s) is <strong><span class="" id="nm_value">1</span> </strong></label>
									<input type="text" data-slider="true" value="1" data-slider-range="1,360" data-slider-step="1" data-slider-snap="true" id="nm" style="display: none;">
								</div>

							</div>
						</div>

						<div class="row">	
							<div class="col-md-3">

								<div class="form-group">
									<label>Rate of Interest is <strong><span class="" id="roi_value">2.00</span></strong></label>&nbsp;%
									<input type="text" data-slider="true" value="2" data-slider-range="2,20" data-slider-step=".25" data-slider-snap="true" id="roi" style="display: none;">
								</div>
							</div>
						</div>
					</div>
					<div class="space-m"></div>
                    <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">									  
                                <p><strong>Monthly EMI</strong></p>
                                <button type="button" class="fa fa-inr btn btn-primary btn-block" id="emi" style="font-size:20px;">1,213.28</button>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">									  
                                <p><strong>Total Interest</strong></p>
                                <button type="button" class="fa fa-inr btn btn-primary btn-block" id="tbl_int" style="font-size:20px;">45,593.11</button>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">									  
                                <p><strong>Payable Amount</strong></p>
                                <button type="button" class="fa fa-inr btn btn-primary btn-block" id="tbl_full" style="font-size:20px;">145,593.11</button>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">									  
                                <p><strong>Interest Percentage</strong></p>
                                <button type="button" class="fa btn btn-primary btn-block" id="tbl_int_pge" style="height:40px;font-size:20px;">31.32 %</button>
                            </div>

                    </div>	
                    <div class="space-l"></div>
                    <div class="row">
                        <div id="container" style="width:100%; height:500px;" >

                        </div>	
                    </div>	

                    <br><br>

                    <div id="illustrate"  width="100%">
                    </div>
                    <div class="space-l"></div>
                </div>
            </div>
            <div class="clear"></div>	
        </div>
    </div>
    <div class="space-l"></div>
</section>  


<script type="text/Javascript">	
    $(document).ready(function(){
    function convert_number(number)
    {
    if ((number < 0) || (number > 9999999999)) 
    { 
    //return "NUMBER OUT OF RANGE!";
    return ""; // As it was conflicting with validation rule
    }
    var Gn = Math.floor(number / 10000000);  /* Crore */ 
    number -= Gn * 10000000; 
    var kn = Math.floor(number / 100000);     /* lakhs */ 
    number -= kn * 100000; 
    var Hn = Math.floor(number / 1000);      /* thousand */ 
    number -= Hn * 1000; 
    var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
    number = number % 100;               /* Ones */ 
    var tn= Math.floor(number / 10); 
    var one=Math.floor(number % 10); 
    var res = ""; 
    if (Gn>0) 
    { 
    res += (convert_number(Gn) + " CRORE"); 
    } 
    if (kn>0) 
    { 
    res += (((res=="") ? "" : " ") + 
    convert_number(kn) + " LAKH"); 
    } 
    if (Hn>0) 
    { 
    res += (((res=="") ? "" : " ") +
    convert_number(Hn) + " THOUSAND"); 
    } 
    if (Dn) 
    { 
    res += (((res=="") ? "" : " ") + 
    convert_number(Dn) + " HUNDRED"); 
    } 
    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX","SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN","FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN","NINETEEN"); 
    var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY","SEVENTY", "EIGHTY", "NINETY"); 
    if (tn>0 || one>0) 
    { 
    if (!(res=="")) 
    { 
    res += " AND "; 
    } 
    if (tn < 2) 
    { 
    res += ones[tn * 10 + one]; 
    } 
    else 
    { 
    res += tens[tn];
    if (one>0) 
    { 
    res += ("-" + ones[one]); 
    } 
    } 
    }
    if (res=="")
    { 
    //res += "ZERO";
    return ""; // As it was conflicting with validation rule	
    }
    return res;
    }
    $("#la").bind(
    "slider:changed", function (event, data) {				
    $("#la_value").html(data.value.toFixed(0)); 
    temp=convert_number(this.value);
    if ( temp != "" )
    {
    temp="INR : "+temp;
    $("div[id='id_la_txt']").text(temp);
    }	
    else
    {
    $("div[id='id_la_txt']").html("");
    }
    calculateEMI();
    }
    );
    $("#nm").bind(
    "slider:changed", function (event, data) {				
    $("#nm_value").html(data.value.toFixed(0)); 
    calculateEMI();
    }
    );
    $("#roi").bind(
    "slider:changed", function (event, data) {				
    $("#roi_value").html(data.value.toFixed(2)); 
    calculateEMI();
    }
    );
    function calculateEMI(){
    var loanAmount = $("#la_value").html();
    var numberOfMonths = $("#nm_value").html();
    var rateOfInterest = $("#roi_value").html();
    var monthlyInterestRatio = (rateOfInterest/100)/12;
    var top = Math.pow((1+monthlyInterestRatio),numberOfMonths);
    var bottom = top -1;
    var sp = top / bottom;
    var emi = ((loanAmount * monthlyInterestRatio) * sp);
    var full = numberOfMonths * emi;
    var interest = full - loanAmount;
    var int_pge =  (interest / full) * 100;
    $("#tbl_int_pge").html(int_pge.toFixed(2)+" %");
    //$("#tbl_loan_pge").html((100-int_pge.toFixed(2))+" %");
    var emi_str = Math.ceil(emi).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var loanAmount_str = Math.ceil(loanAmount).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var full_str = Math.ceil(full).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var int_str = Math.ceil(interest).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $("#emi").html(emi_str);
    $("#tbl_emi").html(emi_str);
    $("#tbl_la").html(loanAmount_str);
    $("#tbl_nm").html(numberOfMonths);
    $("#tbl_roi").html(rateOfInterest);
    $("#tbl_full").html(full_str);
    $("#tbl_int").html(int_str);
    var detailDesc = "";
    var bb=parseInt(loanAmount);
    var int_dd =0;var pre_dd=0;var end_dd=0;
    var int_yr=1;
    var dt = new Date();
    var int_cyr = dt.getFullYear();
    detailDesc += "<div class=\"add_property_form\">"
    for (var j=1;j<=numberOfMonths;j++){
    if ( ( j % 12 ) == 1 )
    {
    detailDesc += "<div class=\"account-block\"> <div class=\"add-title-tab\"> <h3>Year ";
    detailDesc += int_yr + " 	( " + int_cyr + " )"  ;
    detailDesc += "\<\/h3> <div class=\"add-expand\"></div></div><div class=\"add-tab-content\" >";
    detailDesc += "<div class=\"add-tab-row push-padding-bottom row-color-gray\">";								
    detailDesc += "<table class=\"table table-striped table-bordered table-responsive\" >";
    detailDesc += "<thead><tr style=\"background-color:#d4d4d4;\"><th>Payment No.</th><th>Begining Balance</th><th>EMI</th><th>Principal</th><th>Interest</th><th>Ending Balance</th></thead><tbody>";
    }
    int_dd = bb * ((rateOfInterest/100)/12);
    pre_dd = emi.toFixed(2) - int_dd.toFixed(2);
    end_dd = bb - pre_dd.toFixed(2);
    //detailDesc += "<tr><td>"+j+"</td><td>"+bb.toFixed(2)+"</td><td>"+emi.toFixed(2)+"</td><td>"+pre_dd.toFixed(2)+"</td><td>"+int_dd.toFixed(2)+"</td><td>"+end_dd.toFixed(2)+"</td></tr>";
    detailDesc += "<tr><td>"+j+"</td><td>"+	Math.ceil(bb)+"</td><td>"+Math.ceil(emi)+"</td><td>"+Math.ceil(pre_dd)+"</td><td>"+Math.ceil(int_dd)+"</td><td>"+Math.ceil(end_dd)+"</td></tr>";
    bb = bb - pre_dd.toFixed(2);
    if ( ( j % 12 ) == 0 )
    {
    detailDesc += "</tbody></table>";
    detailDesc += "</div></div></div>";
    int_yr += 1; 
    int_cyr += 1;
    }
    }
    detailDesc += "</div>";
    $("#illustrate").html(detailDesc);
    $('#container').highcharts({
    chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false
    },
    title: {
    text: 'Interest Vs Loan'
    },
    tooltip: {
    //pointFormat: '{series.name}: <b>{point.value}%</b>'
    },
    plotOptions: {
    pie: {
    allowPointSelect: true,
    cursor: 'pointer',
    dataLabels: {
    //	enabled: true,
    color: '#000000',
    connectorColor: '#000000',
    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
    }
    }
    },
    series: [{
    type: 'pie',
    name: 'Amount',
    data: [
    ['Loan', eval(loanAmount) ],
    ['Interest', eval(interest.toFixed(2))]
    ],
    colors: ['#ffc000', '#29333d']
    }]
    });			
    }
    temp='INR : ';
    temp+=convert_number(document.getElementById('la').value);
    $("div[id='id_la_txt']").text(temp);
    calculateEMI();
    });
</script>	  

<script type="text/javascript">

    $(document).ready(function () {

        /* $('.add-title-tab > .add-expand').on('click',function() { */
        $("#illustrate").on("click", ".add-expand", function () {

            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });

    });

</script>
<script>
    function showlocation() {
        document.getElementById("mylocations").classList.toggle("show");
    }


    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>