<?php
$status = $_POST["status"];
$firstname = $_POST["firstname"];
$amount = $_POST["amount"];
$txnid = $_POST["txnid"];

$posted_hash = $_POST["hash"];
$key = $_POST["key"];
$productinfo = $_POST["productinfo"];
$email = $_POST["email"];
$salt = "e5iIg1jwi8";

if (isset($_POST["additionalCharges"])) {
    $additionalCharges = $_POST["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {

    $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}

$hash = hash("sha512", $retHashSeq);
?>


<div class="user_pa_area col-sm-12">
    <div class="container user_inner">
        <div class="duser_menu  sidebar col-sm-12" role="navigation">

            <div id="page-wrapper" class="col-sm-12 user-page-area">
                <div class="row">

                    <div class="col-lg-12">
                        <h1 class="page-header">FP - PG Failure </h1>
                    </div>

                </div>	

                <div id="build" class="list-detail detail-block target-block">

                    <?php
                    if ($hash != $posted_hash) {
                        echo "Invalid Transaction. Please try again";
                        echo "<br><br>";
                    } else {

                        echo "<h3>Your order status is " . $status . ".</h3>";
                        echo "<h4>Your transaction id for this transaction is " . $txnid . ". You may try making the payment by clicking the link below.</h4>";
                        echo "<br><br>";
                    }
                    ?>
                    <p><u><a href="http://staging.fairpockets.com/fp_pgform"> Back to PG form </a></u></p>




                </div>
            </div>
            <div class="clear"></div>	
        </div>
    </div>
</div>  
<div class="clear"></div>	

