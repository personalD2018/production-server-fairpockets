<section class="property-management text-center">
    <div class="space-x"></div>
    <div class="container">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="button">
            <img src="<?php echo $this->webroot; ?>css/home/images/property-management-banner.jpg" alt="Property management">
        </a>
    </div>
</section>
 <section class="property-management light-grey ">
     <div class="space-l"></div>
     <div class="container">
     <div class="row">
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-local-support.png" alt="Local Support">
                <h3>Local Support</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br>
                    We will be providing you Local support and manage your house keys to execute activities as per your need and also maximize the ROI for your investment</li>
                <li><strong>Rented Property</strong><br>
                    Regular inspection of your house, managing tenants regularly and in transition</li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-society-bills.png" alt="Paying Utility & Society Bills">
                <h3>Paying Utility &<br> Society Bills</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> Regular Payment of utility and society bills</li>
                <li><strong>Rented Property</strong><br> We will pay the part to be paid by owner and keep track of payments to be done by tenant</li>
            </ul>
        </div>
         </div>
         </div>
     <div class="space-l"></div>
    </section>

    <section class="property-management">
        <div class="space-l"></div>
     <div class="container">
         <div class="row">
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-repair-work.png" alt="Repair Work">
                <h3>Repair Work</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> As per need to keep the house maintained.</li>
                <li><strong>Rented Property</strong><br> Primarily at the transition period and as per need.</li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-cleaning.png" alt="Cleaning">
                <h3>Cleaning</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> Quarterly Cleaning and maintenance of the house.</li>
                <li><strong>Rented Property</strong><br> Primarily at the transition period and as per need.</li>
            </ul>
        </div>
         </div>
    </div>
        <div class="space-l"></div>
    </section>

    <section class="property-management light-grey">
        <div class="space-l"></div>
     <div class="container">
         <div class="row">
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-furnishing.png" alt="Furnishing">
                <h3>Furnishing</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> Help in getting the basic lights and fixtures to complete renovation of the apartment as per the requirement</li>
                <li><strong>Rented Property</strong><br> Based on tenant requirement and as agreed by owner</li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-legal-support.png" alt="Legal Support">
                <h3>Legal Support</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> Sell/Buy/Rental/Tax Advisory</li>
                <li><strong>Rented Property</strong><br> Sell/Buy/Rental/Tax Advisory</li>
            </ul>
        </div>
              </div>
         </div>
        <div class="space-l"></div>
    </section>

    <section class="property-management">
        <div class="space-l"></div>
     <div class="container">
         <div class="row">
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-renting-out.png" alt="Renting Out">
                <h3>Renting Out</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> Renting out your property and getting the tenant as per your criteria</li>
                <li><strong>Rented Property</strong><br> Renting out your property and getting the tenant as per your criteria</li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6 col-sm12">
            <div class="heading text-center">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-seling-reinvestment.png" alt="Selling & Reinvestment">
                <h3>Selling & Reinvestment</h3>
            </div>
            <ul>
                <li><strong>Vacant Property</strong><br> We will help you exit the current investment and advice based on our research to find the suitable investment</li>
                <li><strong>Rented Property</strong><br> We will help you exit the current investment and advice based on our research to find the suitable investment</li>
            </ul>
        </div>
         </div>
          </div>
        <div class="space-l"></div>
    </section>

<section class="light-grey text-center">
    <div class="space-l"></div>
    <div class="container">
	
    <h2><span><strong>Benefit</strong></span> of Our Property Management Services</h2>
    <div class="space-m"></div>
    <div class="row">
		<div class="col-md-12 col-md-offset-1">
		<div class="row">
        <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/saving-time.png" alt="Saving Time">
            <p>Saving Time</p>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/money-saved.png" alt="Money Saved">
            <p>Money Saved</p>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/regular-inspection.png" alt="Regular Inspection">
            <p>Regular Inspection</p>
        </div>
       <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/one-stop-solution.png" alt="One Stop Solution">
            <p>One Stop Solution</p>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/trust-transparency.png" alt="Trust & Transparency">
            <p>Trust & Transparency</p>
        </div>
    </div>
    </div>
    </div>
    </div>
    <div class="space-l"></div>
</section>
