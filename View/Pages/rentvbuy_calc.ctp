<?php
echo $this->Html->css('front/simple-slider');
echo $this->Html->script('highcharts');
echo $this->Html->script('data');
echo $this->Html->script('slider.min');
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
?> 

<script>

    function calc_property_ltaken()
    {
        temp_a = parseInt(document.getElementById('id_property_tcost').value);

        if (document.getElementById('id_property_mpaid').value == '')
            temp_b = 0;
        else
        {
            temp_b = parseInt(document.getElementById('id_property_mpaid').value);

            if (temp_b <= temp_a)
                document.getElementById('la').value = temp_a - temp_b;

        }

    }

    function calc_property_tcost()
    {
        temp_a = parseInt(document.getElementById('id_property_cost').value);

        if (document.getElementById('property_charges').value == '')
            temp_b = 0;
        else
            temp_b = parseInt(document.getElementById('property_charges').value);

        document.getElementById('id_property_tcost').value = temp_a + ((temp_b / 100) * temp_a);

        calc_property_ltaken();

    }

</script>

	<div class="rentvbuy">
		<div class="container">
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="space-m"></div>
                        <h1>Rent Vs Buy Analysis </h1>
                    </div>
                </div>	
                <form class="fpform" id="id_form_calc" method="POST">
                    <div id="build" class="list-detail detail-block target-block">
                        <h2>Property Details</h2>
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row ">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="property_cost" class="label_title" >Cost of Property <span class="mand_field">*</span></label>
                                        <input type="text" class="form-control box_price allownumericwithoutdecimal" name="property_cost" id="id_property_cost" onchange="calc_property_tcost();">
                                        <div id="id_property_cost_txt" style="display:none;font-weight:bold;"></div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Govt Charges on Property is <strong><span class="" id="property_charges_value">1.00</span></strong></label>&nbsp;%
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="What is the cost of registration and stamp duty for the property in your state/ city. Expressed as a % of the property purchase value?"></i>

                                        <input type="text" data-slider="true" value="1" data-slider-range="1,25" data-slider-step="1" data-slider-snap="true" id="property_charges" style="display: none;">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Annual Increase in Property Cost is <strong><span class="" id="property_aincr_value">1.00</span></strong></label>&nbsp;%
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much (in percentage terms) do you expect the selling price of this house to increase by, every year?"></i>

                                        <input type="text" data-slider="true" value="1" data-slider-range="1,25" data-slider-step="1" data-slider-snap="true" id="property_aincr" style="display: none;">
                                    </div>
                                </div>
                                <input type="hidden" name="property_tcost" id="id_property_tcost" >
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="property_mpaid" class="label_title" >Own Money Paid <span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="What Amount of the property is financed by you ?"></i>

                                        <input type="text" class="form-control box_price allownumericwithoutdecimal" name="property_mpaid" id="id_property_mpaid" onchange="calc_property_ltaken();">
                                        <div id="id_property_mpaid_txt" style="display:none;font-weight:bold;"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="property_amc" class="label_title" >Annual Maintenance Cost <span class="mand_field">*</span> </label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="What are the maintenance costs for your property annually?"></i>

                                        <input type="text" class="form-control box_price allownumericwithoutdecimal" name="property_amc" id="id_property_amc">
                                    </div>
                                </div>									
                                <input type="hidden" value="5" id="property_amc_incr" >
                            </div>
                            <br>
                            <label><span style="color:red;">*</span> Annual Increase in Maintenance Cost is assumed to be 5%<span></label>
                        </div>
                        <div class="space-l"></div>
                        <h2>Funding Details</h2>
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property_ltaken" class="label_title" >Loan Amount </label>
                                        <input type="text" class="form-control box_price" disabled="disabled" name="property_ltaken" id="la">
                                    </div>
                                </div>
                            </div><br>	
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Home Loan interest rate is <strong><span class="" id="roi_value">1.00</span></strong></label>&nbsp;%
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="What is the interest rate charged by your bank/ financer for the loan?"></i>
                                        <input type="text" data-slider="true" value="1" data-slider-range="1,25" data-slider-step=".25" data-slider-snap="true" id="roi" style="display: none;">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>No. of Years ( for Loan Amount) <strong><span class="" id="ny_value">3</span></strong></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Over how many years will you repay this loan?"></i>

                                        <input type="text" data-slider="true" value="3" data-slider-range="3,50" data-slider-step="1" data-slider-snap="true" id="ny" style="display: none;">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Bank Interest on Deposit is <strong><span class="" id="property_ilost_value">1.00</span></strong></label>&nbsp;%
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much interest will you earn on your if the amount is to be kept in bank ( as savings instrument )?"></i>
                                        <input type="text" data-slider="true" value="1" data-slider-range="1,25" data-slider-step="1" data-slider-snap="true" id="property_ilost" style="display: none;">
                                    </div>
                                </div>
                                <input type="hidden" value="30" id="property_trate" >
                            </div>
                            <br>
                            <label><span style="color:red;">*</span> Tax Deduction is assumed to be 30%<span></label>
                        </div>
                        <div class="space-l"></div>
                        <h2>Rent Details</h2>
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="rent_pmonth" class="label_title" >Property Rent (per month)  <span class="mand_field">*</span> </label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much is the monthly rent for this property (in र/month?)"></i>

                                        <input type="text" class="form-control box_price allownumericwithoutdecimal" name="rent_pmonth" id="id_rent_pmonth">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Annual Increase in Rent is <strong><span class="" id="rent_aincr_value">1.00</span></strong></label>&nbsp;%
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How much do you expect the monthly rent to increase every year?"></i>

                                        <input type="text" data-slider="true" value="1" data-slider-range="1,25" data-slider-step="1" data-slider-snap="true" id="rent_aincr" style="display: none;">
                                    </div>
                                </div>									
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label> No. of Months ( Security Deposit ) <strong><span class="" id="rent_smonth_value">1</span></strong></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="How many times the monthly rent is the security deposit for this house?"></i>

                                        <input type="text" data-slider="true" value="1" data-slider-range="1,24" data-slider-step="1" data-slider-snap="true" id="rent_smonth" style="display: none;">
                                    </div>
                                </div>	
                            </div>
                        </div>
                        <div class="account-block text-center submit_area">
                            <button id="id_submit" class="btn btn-primary btn-red" value="1">Submit</button>
                        </div>
                        <div class="row">
                            <div id="container" style="width:100%;" >

                            </div>	
                        </div>	
                        <br>
                        <div class="account-block">
                            <div class="add-title-tab">
                                <h3>Graph Data</h3>
                                <div class="add-expand"></div>
                            </div>
                            <div class="add-tab-content" id="datatable">
                                <div class="add-tab-row push-padding-bottom row-color-gray">
                                    <table id="illustrate" class="table table-striped table-bordered" width="100%">
                                    </table>
                                </div>	
                            </div>			
                        </div>	
                    </div>
                </form>	
            </div>	
        </div>
		<div class="space-l"></div>
 
	</div>
		

<script type="text/javascript">
    $(document).ready(function () {
        $("li.drop-accordian a:first").bind("click", function (e) {
            $(this).next('ul').slideToggle();
            e.stopPropagation();
        });

        $('#firstLevelNav_small').on('hidden.bs.dropdown', function () {
            $(this).find('ul.drop-accordian-menu').hide();
        });
    });
</script>

<script type="text/Javascript">	
    $(document).ready(function(){

    $("#ny").bind(
    "slider:changed", function (event, data) {				
    $("#ny_value").html(data.value); 
    //calculateEMI();
    }
    );

    $("#roi").bind(
    "slider:changed", function (event, data) {				
    $("#roi_value").html(data.value.toFixed(2)); 
    //calculateEMI();
    }
    );

    $("#property_ilost").bind(
    "slider:changed", function (event, data) {				
    $("#property_ilost_value").html(data.value.toFixed(2)); 
    //calculateEMI();
    }
    );

    $("#property_trate").bind(
    "slider:changed", function (event, data) {				
    $("#property_trate_value").html(data.value.toFixed(2)); 
    //calculateEMI();
    }
    );

    $("#property_amc_incr").bind(
    "slider:changed", function (event, data) {				
    $("#property_amc_incr_value").html(data.value.toFixed(2)); 
    //calculateEMI();
    }
    );

    $("#property_aincr").bind(
    "slider:changed", function (event, data) {				
    $("#property_aincr_value").html(data.value.toFixed(2)); 
    //calculateEMI();
    }
    );			

    $("#rent_aincr").bind(
    "slider:changed", function (event, data) {				
    $("#rent_aincr_value").html(data.value.toFixed(2)); 
    //calculateEMI();
    }
    );

    $("#rent_smonth").bind(
    "slider:changed", function (event, data) {				
    $("#rent_smonth_value").html(data.value); 
    //calculateEMI();
    }
    );

    $("#property_charges").bind(
    "slider:changed", function (event, data) {				
    $("#property_charges_value").html(data.value.toFixed(2)); 

    calc_property_tcost();

    //calculateEMI();
    }
    );

    function calculateEMI(){

    var loanAmount = parseInt($("#la").val());

    var numberOfMonths = $("#ny_value").html(); 
    numberOfMonths = numberOfMonths * 12;

    var rateOfInterest = $("#roi_value").html();
    var monthlyInterestRatio = (rateOfInterest/100)/12;

    var top = Math.pow((1+monthlyInterestRatio),numberOfMonths);
    var bottom = top -1;
    var sp = top / bottom;
    var emi = ((loanAmount * monthlyInterestRatio) * sp);
    var full = numberOfMonths * emi;
    var interest = full - loanAmount;
    var int_pge =  (interest / full) * 100;
    $("#tbl_int_pge").html(int_pge.toFixed(2)+" %");
    //$("#tbl_loan_pge").html((100-int_pge.toFixed(2))+" %");

    var emi_str = emi.toFixed(2).toString();
    var loanAmount_str = loanAmount.toString();
    var full_str = full.toFixed(2).toString();
    var int_str = interest.toFixed(2).toString();

    $("#emi").value = emi_str;
    $("#tbl_la").value = loanAmount_str;
    $("#tbl_full").value = full_str;
    $("#tbl_int").value = int_str;
    var detailDesc = "<thead><tr style=\"background-color:#d4d4d4;\"><th>Year</th><th>Own House Expenses</th><th>Rental Expenses</th><</thead><tbody>";

    var bb=loanAmount;
    var int_dd =0;var pre_dd=0;var end_dd=0;

    var mpaid = parseInt($("#id_property_mpaid").val());
    var ilost = $("#property_ilost_value").html(); 
    var trate = parseInt($("#property_trate").val()); 			
    var temp_mpaid = mpaid;
    var intr = 0;
    var ptax = 0;

    var aincr = $("#property_aincr_value").html();
    var cost = parseInt($("#id_property_cost").val());
    var temp_cost = cost;
    var temp_tot_intr_lost = 0;
    var temp_ann_cap_appr = 0;

    var amc = parseInt($("#id_property_amc").val());
    var amc_incr = parseInt($("#property_amc_incr").val());
    var temp_amc = amc;

    var rent_pmonth = parseInt($("#id_rent_pmonth").val());
    var rent_aincr = $("#rent_aincr_value").html();
    var rent_smonth = $("#rent_smonth_value").html();

    var temp_rent = rent_pmonth * 12 ;
    var temp_sdep = rent_pmonth * rent_smonth ;
    var temp_sdep_intr;

    var temp_oexp=0;
    var temp_rexp=0;

    numberOfYears = numberOfMonths/12;
    for (var k=1;k<=numberOfYears;k++){

    //Emi part
    temp_dd=0;
    for (var j=1;j<=12;j++){
    int_dd = bb * ((rateOfInterest/100)/12);
    pre_dd = emi.toFixed(2) - int_dd.toFixed(2);
    end_dd = bb - pre_dd.toFixed(2);
    bb = bb - pre_dd.toFixed(2);

    temp_dd += int_dd // Adding up interest part for the whole year
    }

    // Interest on self money	
    intr = (ilost/100)*temp_mpaid;

    if ( k == 1 )
    ptax = intr - ( intr * (trate/100) );
    else
    ptax = intr * 0.67;

    temp_mpaid = temp_mpaid + ptax;

    // Appreciation
    temp_tot_intr_lost = temp_dd + intr;
    temp_ann_cap_appr = ( temp_cost * (aincr/100) );
    temp_cost += temp_ann_cap_appr;

    // Maintenance cost	
    if ( k > 1 )
    {
    temp_amc += ( temp_amc * (amc_incr/100) );
    }

    // Rent
    temp_sdep_intr = (ilost/100)*temp_sdep;
    temp_sdep += temp_sdep_intr;
    temp_sdep_intr_ptax = temp_sdep_intr - ( temp_sdep_intr * (trate/100) )

    if ( k > 1 )
    {
    temp_rent += ( temp_rent * (rent_aincr/100) );
    }


    temp_oexp =  temp_ann_cap_appr - temp_tot_intr_lost - temp_amc;
    temp_rexp =  (temp_rent + temp_sdep_intr_ptax) * (-1);

    //detailDesc += "<tr><td>"+k+"</td><td>"+temp_oexp.toFixed(2)+"</td><td>"+temp_rexp.toFixed(2)+"</td></tr>";
    detailDesc += "<tr><td>"+k+"</td><td>"+Math.ceil(temp_oexp)+"</td><td>"+Math.ceil(temp_rexp)+"</td></tr>";


    detailDesc += "</tbody>";
    }			

    $("#illustrate").html(detailDesc);

    $('#container').highcharts({

    data: {
    table: 'datatable',
    startColumn: 0,
    endColumn: 2
    },
    chart: {
    type: 'line'
    },
    yAxis: {
    tickInterval: 1000000,
    allowDecimals: false,
    title: {
    text: ''
    },
    labels: {
    formatter: function () {
    return Highcharts.numberFormat(this.value,0);
    }
    }
    },
    title: {
    text: 'Rent Vs Buy'
    }

    });

    }

    function convert_number(number)
    {
    if ((number < 0) || (number > 9999999999)) 
    { 
    //return "NUMBER OUT OF RANGE!";
    return ""; // As it was conflicting with validation rule
    }

    var Gn = Math.floor(number / 10000000);  /* Crore */ 
    number -= Gn * 10000000; 
    var kn = Math.floor(number / 100000);     /* lakhs */ 
    number -= kn * 100000; 
    var Hn = Math.floor(number / 1000);      /* thousand */ 
    number -= Hn * 1000; 
    var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
    number = number % 100;               /* Ones */ 
    var tn= Math.floor(number / 10); 
    var one=Math.floor(number % 10); 
    var res = ""; 

    if (Gn>0) 
    { 
    res += (convert_number(Gn) + " CRORE"); 
    } 
    if (kn>0) 
    { 
    res += (((res=="") ? "" : " ") + 
    convert_number(kn) + " LAKH"); 
    } 
    if (Hn>0) 
    { 
    res += (((res=="") ? "" : " ") +
    convert_number(Hn) + " THOUSAND"); 
    } 

    if (Dn) 
    { 
    res += (((res=="") ? "" : " ") + 
    convert_number(Dn) + " HUNDRED"); 
    } 

    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX","SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN","FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN","NINETEEN"); 
    var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY","SEVENTY", "EIGHTY", "NINETY"); 

    if (tn>0 || one>0) 
    { 
    if (!(res=="")) 
    { 
    res += " AND "; 
    } 
    if (tn < 2) 
    { 
    res += ones[tn * 10 + one]; 
    } 
    else 
    { 

    res += tens[tn];
    if (one>0) 
    { 
    res += ("-" + ones[one]); 
    } 
    } 
    }

    if (res=="")
    { 
    //res += "ZERO";
    return ""; // As it was conflicting with validation rule	
    }

    return res;
    }

    $("input[id='id_property_cost']").on('keyup', function (e) {

    if ( this.value != '')
    {
    //alert ( toWords(this.value) );
    $("div[id='id_property_cost_txt']").css('display','inline');

    temp=convert_number(this.value);
    if ( temp != "" )
    {
    temp="INR : "+temp;
    $("div[id='id_property_cost_txt']").text(temp);
    }	
    else
    {
    $("div[id='id_property_cost_txt']").html("");
    $("input[id='id_property_cost']").valid();	
    }	

    }	
    else
    {
    $("div[id='id_property_cost_txt']").css('display','none');
    }

    if(e.which === 13){

    //Disable textbox to prevent multiple submit
    //$(this).attr("disabled", "disabled");

    //Do Stuff, submit, etc..
    }
    });

    $("input[id='id_property_mpaid']").on('keyup', function (e) {

    if ( this.value != '')
    {
    //alert ( toWords(this.value) );
    $("div[id='id_property_mpaid_txt']").css('display','inline');

    temp=convert_number(this.value);
    if ( temp != "" )
    {
    temp="INR : "+temp;
    $("div[id='id_property_mpaid_txt']").text(temp);
    }	
    else
    {
    $("div[id='id_property_mpaid_txt']").html("");
    $("input[id='id_property_mpaid']").valid();	
    }	

    }	
    else
    {
    $("div[id='id_property_mpaid_txt']").css('display','none');
    }

    if(e.which === 13){

    //Disable textbox to prevent multiple submit
    //$(this).attr("disabled", "disabled");

    //Do Stuff, submit, etc..
    }
    });

    $('.allownumericwithdecimal').on('keypress keyup blur',function (event) 
    {
    //this.value = this.value.replace(/[^0-9\.]/g,'');

    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
    {
    event.preventDefault();
    }
    });

    $('.allownumericwithoutdecimal').on('keypress keyup blur',function (event) 
    {    
    $(this).val($(this).val().replace(/[^\d].+/, ''));

    if ((event.which < 48 || event.which > 57)) 
    {
    event.preventDefault();
    }
    });	

    $.validator.addMethod('lessThan',function (value, element, param) 
    {
    var otherElement = $(param);
    if ( otherElement.val() != '' )
    return parseInt(value,10) < parseInt(otherElement.val(),10);
    else
    return true;
    }, 'Money Paid should be less than Total Cost of Property.');

    var ruleMSetPercent = {
    required : true,
    range    :[0.01,100]
    };

    var messageMSetPercent = {
    required : 'Field is required.',
    range  : 'Allowed : 0.01 - 100.',
    };

    var ruleMSetYear = {
    required : true,
    range    :[1,100]
    };

    var messageMSetYear = {
    required : 'Field is required.',
    range  : 'Allowed : 1 - 100.',
    };

    var ruleMSetCost = {
    required : true,
    range    :[1,9999999999]
    };

    var messageMSetCost = {
    required : 'Field is required.',
    range  : 'Allowed : 1 - 9999999999.',
    };


    $('#id_form_calc').validate({

    // Specify the validation rules
    rules: {
    'property_mpaid': {
    required : true,
    range    :[1,9999999999],
    lessThan : '#id_property_tcost'
    },
    'property_ilost': ruleMSetPercent,
    'property_trate': ruleMSetPercent,
    'property_charges': ruleMSetPercent,
    'roi': ruleMSetPercent,
    'property_amc_incr': ruleMSetPercent,
    'property_aincr': ruleMSetPercent,
    'rent_aincr': ruleMSetPercent,
    'rent_smonth': ruleMSetYear,
    'ny': ruleMSetYear,
    'property_cost': ruleMSetCost,
    'property_amc': ruleMSetCost,
    'rent_pmonth': ruleMSetCost



    },

    // Specify the validation error messages
    messages: {
    'property_mpaid': {
    required : 'Field is required.',
    range  : 'Allowed : 1 - 9999999999.'
    },
    'property_ilost': messageMSetPercent,
    'property_trate': messageMSetPercent,
    'property_charges': messageMSetPercent,
    'roi': messageMSetPercent,
    'property_amc_incr': messageMSetPercent,
    'property_aincr': messageMSetPercent,
    'rent_aincr': messageMSetPercent,
    'rent_smonth': messageMSetYear,
    'ny': messageMSetYear,
    'property_cost': messageMSetCost,
    'property_mpaid': messageMSetCost,
    'property_amc': messageMSetCost,
    'rent_pmonth': messageMSetCost	
    },

    submitHandler: function(form){

    // Validations OK.
    calculateEMI();

    $('#id_submit').css('background','#d62540');
    $('#id_submit').css('color','#fff');


    // To remain on same page after submit
    return false;
    }


    });	

    /*
    $("#id_submit").on("click", function(e){
    //e.preventDefault();
    calculateEMI();

    return false;
    });
    */

    //document.getElementById("id_submit").addEventListener("click", calculateEMI);

    });

</script>	

<script type="text/javascript">

    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();


        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });

    });

</script>  	


