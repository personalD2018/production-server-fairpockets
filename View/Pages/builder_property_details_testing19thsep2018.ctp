<style>
.detail-amenities .list-amenities li{width:86% !important;}
</style>
<?php
//$datatext = json_decode('{"price_bsp1":"3832","from_floor1":"0","to_floor1":"0","price_bsp2":"3495","from_floor2":"1","to_floor2":"9","price_bsp3":"3485","from_floor3":"10","to_floor3":"18","price_bsp4":"3475","from_floor4":"19","to_floor4":"24","price_bsp5":"3275","from_floor5":"25","to_floor5":"25"}');
echo $this->Html->script('front/jssor.slider-22.2.10.min');
// Offer price calculator
$gOfferPrice = $propertydetails['Property']['offer_price'];
$gServiceTax = 0;
if (!empty($all_property_features['47'])) {
    $gSuperBuiltUpArea = $all_property_features['47'];
} else if(!empty($all_property_features['50'])) {
    $gSuperBuiltUpArea = $all_property_features['50'];
}
else{
	$gSuperBuiltUpArea = '';
}

$property_id_test = $this->params->query['fpid'];
$id_test = base64_decode($property_id_test);

// Super Built up area ( in sq ft )
//echo '<pre>'; print_r($project_pricing);
$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
//echo '<pre>'; print_r($basePriceArr); die();

//if($id_test == 27378763551989){
	//echo '<pre>'; print_r($basePriceArr); 
//}


if(!empty($project_pricing)){
	if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
		$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
	}else{
		$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
		
		if(isset($basePriceArr['unit_bsp1']) && $basePriceArr['unit_bsp1'] != ''){
			$gBaseRateUnit = $basePriceArr['unit_bsp1'];
		}else{
			$gBaseRateUnit = 1;
		}
	}
	$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
	$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
	//$gstCreditInput = $project_pricing['ProjectPricing']['price_srtax_icr'];
}
else
{
	$gBaseRate = '';   // Base Rate
	$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
	$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
	//$gstCreditInput = 0;
}
if($project_pricing['ProjectPricing']['price_srtax_icr'] != ''){
	$gstCreditInput = $project_pricing['ProjectPricing']['price_srtax_icr'];
}else{
	$gstCreditInput = 0;
}



/* get project charges array */
if(!empty($project_pricing)){
$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
}
else{ 
$project_pricing_charge = array();
}
// ProjectCharges

$project_charges = array();
for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
    if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
        $pcharge = $project_pricing_charge['price_pcharge' . $i];

        switch ($pcharge) {
            case "1":
                $project_charges[$j]['0'] = "One Covered Car Park";
                break;
            case "2":
                $project_charges[$j]['0'] = "Double Covered Car Park";
                break;
            case "3":
                $project_charges[$j]['0'] = "Club Membership";
                break;
			case "4":
                $project_charges[$j]['0'] = "Total Power Backup Cost";
                break;
            case "13":
                $project_charges[$j]['0'] = "Power BackUp per KVA";
                break;
            case "5":
                $project_charges[$j]['0'] = "Interest Free Maintenance Deposit";
                break;
            case "6":
                $project_charges[$j]['0'] = "Road Facing PLC";
                break;
            case "7":
                $project_charges[$j]['0'] = "Park Facing PLC";
                break;
			case "9":
                $project_charges[$j]['0'] = "Sinking Fund";
                break;
			case "10":
                $project_charges[$j]['0'] = "View PLC 1";
                break;
			case "11":
                $project_charges[$j]['0'] = "View PLC 2";
                break;
			case "12":
                $project_charges[$j]['0'] = "Swimming Pool PLC";
                break;
			case "14":
                $project_charges[$j]['0'] = "Hill View PLC";
                break;
			case "15":
                $project_charges[$j]['0'] = "Additional Car Parking";
                break;			
            default:
                $project_charges[$j]['0'] = "Corner PLC";
        }

        $project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
        $project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
        $project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];
    }
}
/* get additional charges array */
if(!empty($project_pricing)){
$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
}
else{
$project_additional = array();
}
$additon_charges = array();
for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
    if (!empty($project_additional['price_core_amt' . $i])) {
        $acharge = $project_additional['price_core_plc' . $i];

        switch ($acharge) {
            case "1":
                $additon_charges[$j]['0'] = "Lease Rent";
                break;
            case "2":
                $additon_charges[$j]['0'] = "External Electrification Charges";
                break;
            case "3":
                $additon_charges[$j]['0'] = "External development Charges";
                break;
            case "4":
                $additon_charges[$j]['0'] = "Infrastructure development Charges";
                break;
            case "5":
                $additon_charges[$j]['0'] = "Electricity Connection Charges";
                break;
            case "6":
                $additon_charges[$j]['0'] = "Fire fighting charges";
                break;
            case "7":
                $additon_charges[$j]['0'] = "Electric Meter Charges";
                break;
            case "8":
                $additon_charges[$j]['0'] = "Gas Pipeline Charges";
                break;
			case "9":
                $additon_charges[$j]['0'] = "Sinking Fund";
                break;
			case "10":
                $additon_charges[$j]['0'] = "Security  & 1 time connection charges";
                break;
			case "11":
                $additon_charges[$j]['0'] = "Water & sewer connection charges";
                break;
            default:
                $additon_charges[$j]['0'] = "Internal development Charges";
        }

        $additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
        $additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
        $additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
    }
}
/* get other charges array */
if(!empty($project_pricing))
{
$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
}
else{ $project_other = array();}

$other_charges = array();
for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
    if (!empty($project_other['price_other_amt' . $i])) {
        $ocharge = $project_other['price_other_plc' . $i];

        switch ($ocharge) {
            case "1":
                $other_charges[$j]['0'] = "Other Charges1";
                break;
            case "2":
                $other_charges[$j]['0'] = "Other Charges2";
                break;
            case "3":
                $other_charges[$j]['0'] = "Other Charges3";
                break;
            case "4":
                $other_charges[$j]['0'] = "Other Charges4";
                break;
            case "5":
                $other_charges[$j]['0'] = "Other Charges5";
                break;
            case "6":
                $other_charges[$j]['0'] = "Other Charges6";
                break;
            case "7":
                $other_charges[$j]['0'] = "Other Charges7";
                break;
            case "8":
                $other_charges[$j]['0'] = "Other Charges8";
                break;
            default:
                $other_charges[$j]['0'] = "Other Charges9";
        }

        $other_charges[$j]['1'] = $project_other['price_other_type' . $i];
        $other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
        $other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
    }
}
$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
$gaProjectChargesLen = count($gaProjectCharges);

/*
  $gaProjectCharges = array (

  // [ index(0- ( 9+8+4) ) , option(1-2) , amount , unit(1-2) ]

  array( 'Lease Rent' , 2 , 1 , 1 ),
  array( 'External Electrification Charges' , 2 , 1 , 2 ),
  array( 'External development Charges' , 1 , 1 , 2 ),
  array( 'Infrastructure development Charges' , 1 , 1 , 1	 ),
  array( 'Electricity Connection Charges' , 2 , 1 , 1 ),
  array( 'Fire fighting charges' , 2 , 1 , 2 ),
  array( 'Electric Meter Charges', 1 , 1 , 2 ),
  array( 'Gas Pipeline Charges' , 1 , 1 , 1 ),
  array( 'Sinking Fund' , 2 , 1 , 2 ),

  array( 'One Covered Car Park' , 2 , 3 , 1 ),
  array( 'Double Covered Car Park' , 2 , 1 , 2 ),
  array( 'Club Membership' , 1 , 1 , 2 ),
  array( 'Power BackUp per KVA' , 2 , 1 , 2 ),
  array( 'Interest Free Maintenance' , 2 , 1 , 1 ),
  array( 'Road Facing PLC' , 2 , 1 , 2 ),
  array( 'Park Facing PLC', 2 , 2 , 2 ),
  array( 'Corner PLC' , 2 , 3 , 2 ),

  array( 'Other PLC 1' , 2 , 1 , 1 ),
  array( 'Other PLC 2' , 2 , 1 , 2 ),
  array( 'Other PLC 3' , 1 , 1 , 2 ),
  array( 'Other PLC 4' , 2 , 1 , 2 )

  );
 */


// Project Pricing - Stamp Duty
if(!empty($project_pricing))
{
$gaStampDuty = array(
    //[ option(1-2) , amount ]

    array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
);

// Project Pricing - Registration
$gaRegistration = array(
    // [ option(1-2) , amount ]
    array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
);
}
else{
$gaStampDuty = array();
$gaRegistration = array();
}
// Project Pricing - Floor PLC
$gaTotalFloors = $projectdetail['ProjectDetails']['num_of_tow']; // Total number of floor in the tower of property
if(!empty($project_pricing))
{
if ($project_pricing['ProjectPricing']['floor_per_plc_amount'] != '') {
    $gaFloorPLC = array(
        // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
        array($project_pricing['ProjectPricing']['floor_option'], $project_pricing['ProjectPricing']['floor_per_plc_amount'], $project_pricing['ProjectPricing']['floor_per_option'])
            //array( 2 , 10 , 1 )
    );
} else {
    $gaFloorPLC = array(
        // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
        array($project_pricing['ProjectPricing']['floor_option'], 0, $project_pricing['ProjectPricing']['floor_per_option'])
            //array( 2 , 10 , 1 )
    );
}
}
else
{
	 $gaFloorPLC = array();
}
//echo '<pre>';print_r($gaFloorPLC);die();
/*
  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
  pr($gaProjectCharges);
  echo "<br>"."gaStampDuty"."<br>";
  pr($gaStampDuty);
  echo "<br>"."gaRegistration"."<br>";
  pr($gaRegistration);
  echo "<br>"."gaFloorPLC"."<br>";
  pr($gaFloorPLC);
 */


$gaMandatoryCharges = '';
$gaOptionalCharges = '';
$gaOptionalCount = 0;
$ProjectCharges = 0;
$gaFloorPLCCharges = 0;

if(!empty($gaFloorPLC))
{
if ($gaFloorPLC[0][1] > 0) {
    $gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

    if ($gaFloorPLC[0][0] == 2) {
        // BSP w.r.t Ground floor
        $tempFloor = 0; //0 - Ground Floor
        $gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
    } else {
        // BSP w.r.t TOP floor;
        $tempFloor = 0; //0 - Ground Floor
        $gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
    }

    if ($gaFloorPLC[0][2] == 2) {
        // sub option
        $gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
    }

    $gaMandatoryCharges = $gaMandatoryCharges . "
					<div class='col-sm-12'>
						<label for='' class='checkbox-custom-label'>
				";
    $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
            'Per Floor Charges' . "<span class='plc_amount884'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . number_format($gaFloorPLC[0][1]) . "&nbsp;/ sq ft</span>
					";
    $gaMandatoryCharges = $gaMandatoryCharges . "
						</label>  
					</div>
			";
}
}
//$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
$gOfferPrice = $gOfferPrice + $BaseCharge;
$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
//echo "<br><br>BaseCharge - ".$BaseCharge;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
// Project Charges
$ProjectCharges = 0;
for ($row = 0; $row < $gaProjectChargesLen; $row++) {
    //echo "<br><br>".$gaMandatoryCharges;
    if (1 == $gaProjectCharges[$row][1]) {
        // Mandatory to include the charges

        $gaMandatoryCharges = $gaMandatoryCharges . "
						<div class='col-sm-12'>
							<label for='' class='checkbox-custom-label'>
				";

        //Calculate Charges
		//echo $gaProjectCharges[$row][0];
		//echo $propertydetails['Property']['power_backup_kva'];
		if($gaProjectCharges[$row][0] == 'Power BackUp per KVA'){
			if($propertydetails['Property']['power_backup_kva'] != ''){
				$MandatoryChargeAmt = $gaProjectCharges[$row][2]*$propertydetails['Property']['power_backup_kva'];
			}else{
				$MandatoryChargeAmt = $gaProjectCharges[$row][2];
			}
		}else{
		
        $MandatoryChargeAmt = $gaProjectCharges[$row][2];
		}
        if (1 == $gaProjectCharges[$row][3]) {
            // Per Square feet 	
            $ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

            // Display Charge in Mandatory Section
            $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'>&nbsp;<i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;&nbsp;" . number_format($MandatoryChargeAmt) . "&nbsp;/ sq ft</span>
					";
        } else {
            // Per Unit 	
            $ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

            // Display Charge in Mandatory Section
            $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount884'>&nbsp;&nbsp;<i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;&nbsp;" . number_format($MandatoryChargeAmt) . "</span>
					";
        }

        $gaMandatoryCharges = $gaMandatoryCharges . "
							</label>  
						</div>
				";

        //echo "<br><br>	ProjectCharges - ".$ProjectCharges;
    } else {
        // Optional to include the charges
        $gaOptionalCount = $gaOptionalCount + 1;
        $tempid = "optional" . $gaOptionalCount;

        $gaOptionalCharges = $gaOptionalCharges . "
						<div class='col-sm-12'>
						    
				";

        //Calculate Charges
        $OptionalChargeAmt = $gaProjectCharges[$row][2];
        $tempChargeAmt = 0;
        if (1 == $gaProjectCharges[$row][3]) {
            // Per Square feet 	
            $tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

            // Display Charge in Optional Section
            $gaOptionalCharges = $gaOptionalCharges . "
						<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
						<label for='" . $tempid . "' class='checkbox-custom-label'>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . number_format($OptionalChargeAmt) . "&nbsp;/ sq ft</span>
					";
        } else {
            // Per Unit 	
            $tempChargeAmt = $OptionalChargeAmt;

            // Display Charge in Mandatory Section
            $gaOptionalCharges = $gaOptionalCharges . "
					
						<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
						<label for='" . $tempid . "' class='checkbox-custom-label'>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . number_format($OptionalChargeAmt) . "</span>
					";
        }

        $gaOptionalCharges = $gaOptionalCharges . "
							
							</label>  
						</div>
				";
    }

    //echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
}

$gProjectCharges_a = $ProjectCharges;

$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
//echo "<br><br>gServiceTax - ".$gServiceTax;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
//Stamp Duty
$StampDuty = 0;
if(!empty($gaStampDuty)){
if (2 == $gaStampDuty[0][0]) {
    // Percentage
    $StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
} else {
    // Flat Charges
    $StampDuty = $gaStampDuty[0][1];
}
}
else{
$StampDuty = '';
}
$gOfferPrice = $gOfferPrice + $StampDuty;

//echo "<br><br>StampDuty - ".$StampDuty;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
//Registration Charges
$Registration = 0;
if(!empty($gaRegistration)){
if (2 == $gaRegistration[0][0]) {
    // Percentage
    $Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
} else {
    // Flat Charges
    $Registration = $gaRegistration[0][1];
}
}
else{
$Registration = '';
}
$gOfferPrice = $gOfferPrice + $Registration;

//echo "<br><br>Registration - ".$Registration;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;


?>
<script>
    jQuery(document).ready(function () {
        function nFormatter(num)
        {
            num = Math.abs(num)
            if (num >= 10000000)
            {
                formattedNumber = (num / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (num >= 100000)
            {
                formattedNumber = (num / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (num >= 1000)
            {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = num;
            }

            return formattedNumber;
        }

<?php
if ($propertydetails['Property']['transaction_type_id'] == '1') {
    ?>
            //format = nFormatter('<?php echo $all_property_features['1'] * $gBaseRate; ?>');
			format = nFormatter('<?php echo $propertydetails['Property']['offer_price']; ?>');
            document.getElementById("offerpriceformater").innerHTML = format;
            //mformat = nFormatter('<?php echo $gBaseRate; ?>');
            //document.getElementById("marketpriceformater").innerHTML = mformat;
    <?php
} else {
    ?>
            rformat = nFormatter('<?php echo $propertydetails['Property']['market_rent']; ?>');

            document.getElementById("marketrentformater").innerHTML = rformat;

            eformat = nFormatter('<?php echo $propertydetails['Property']['expected_monthly']; ?>');

            document.getElementById("expectedmonthlyformat").innerHTML = eformat;
<?php } ?>

    });

</script>        

<script type="text/javascript">
    /*jQuery(document).ready(function () {

        jQuery("#formID").validate();
    });*/
</script>               
<style>
    .detail_header .h_feature 
    {
        font-size: 20px;
        color: #29333d;
    }
    .green{color:green; font-weight:bold;border:1px solid; padding:7px;}
    .error{color:red;}
</style>
<style>
    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    .jssora05l.jssora05lds      (disabled)
    .jssora05r.jssora05rds      (disabled)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('img/a17.png') no-repeat;
        overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }
    .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
    .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
</style>


<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

<section class="propert-detail">
    <div class="property_detail_header">
        <div class="container">
            
            <div class="detail_header">
                <div class="row">
                    <!--<div class="col-sm-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/searchList?default_location=Noida&amp;property_for=buy&amp;property_name=Property in Central Noida ">Property in Central Noida&nbsp;</a> </li>
                            <li>Noida</li>
                        </ul>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="col-sm-4 header-price">
                        <!--<div class="offer_price">
                            <i class="fa fa-inr" aria-hidden="true"></i>65 Lac                                                          <span class="prooffer">
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i>10% less
                                </span>
                                                        <span class="price_sq">@ 5200 per Sq.Ft</span>
                        </div>
                        <div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i>71.88 Lac @5750 per Sq.Ft</div>-->

                        <?php
                       //echo '<pre>'; print_r($propertydetails);
                    if ($propertydetails['Property']['transaction_type_id'] == '1') {
                        ?>  
                        <div class="offer_price">
							<i class="fa fa-inr" aria-hidden="true"></i><span id="offerpriceformater"></span>
							<span class="price_sq">@ <i class="fa fa-inr" aria-hidden="true"></i>
							<?php if($gBaseRateUnit == 1){ ?>
							<?php echo $gBaseRate; ?> / Sq.Ft</span>
							<?php } ?>
							<?php if($gBaseRateUnit == 2){ ?>
							<?php echo number_format($gBaseRate/$all_property_features['1']); ?> / Sq.Ft</span>
							<?php } ?>
						</div>
                        <div class="market_price"><span style="font-weight:bold; color:green;">Base Price </span> for Size - <?php
                            /*if (!empty($all_property_features['superbuilt_area_insq'])) {
                                $insq = $all_property_features['superbuilt_area_insq'];
                            } else {
                                $insq = $all_property_features['plot_area_insq'];
                            }*/
                            //echo round($propertydetails['Property']['market_price'] / $insq, 3);
                            echo $all_property_features['1'];
                            ?> Sq.Ft
							<a href="#calyourprice" class="btn btn-primary" style="height:32px !important; padding-top:6px !important;">Calculate Price</a></div>
							
                            <?php
                        } else {
                            ?>  
                        <div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i> <span id="expectedmonthlyformat"></span><span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo round(($propertydetails['Property']['market_rent'] - $propertydetails['Property']['expected_monthly']) / $propertydetails['Property']['market_rent'] * 100, 0); ?>% less</span></div>
                        <div class="market_price"><span>Market Rent:</span><i class="fa fa-inr" aria-hidden="true"></i><span id="marketrentformater"></span>
                        </div>
                        <?php
                    }
                    ?>
						

                    </div>
                    <div class="col-sm-5 text-center header-feature">
                        <?php //print_r($propertyoption); ?>
                        <div class="h_feature">
						<?php if(isset($all_property_features[34]) && $all_property_features[34]!==''){ echo $all_property_features[34]; } ?>
						<span class="h_feature_sub">
						<?php if($propertyoption['MstPropertyTypeOption']['transaction_type_id'] == 1){
							echo 'Residential';
							}else{
							echo 'Commercial';
							};
						?>
						<?php echo $propertyoption['MstPropertyTypeOption']['option_name']; ?>
						for Sale</span></div>
                        <div class="h_location">
							<?php echo "in " . $project['Project']['project_name']; ?>, <?php echo $project['Project']['locality']; ?>, <?php echo $project['Project']['city_data']; ?>, <?php echo $project['Project']['state_data']; ?>
						</div>
                    </div>
                    <div class="col-sm-3 text-center header-other-feature">
						
                        <div class="h_postdate">Post on <?php echo date("m/d/Y", strtotime($propertydetails['Property']['created_date'])); ?></div>
                        <div class="h_availability">
						<?php if($propertydetails['Property']['construction_status'] == 1){
							echo 'Ready To Move';
							}else if($propertydetails['Property']['construction_status'] == 2){
							echo 'Under Construction';
							}else{
							echo 'Under Construction';
							}
						?>
						</div>                    </div>
                </div>
            </div>
            <?php //endforeach;?>
        </div>
    </div>
    <div class=" property_detail_area">
        <div class="space-m"></div>
        <div class="container">
            <div class=" property_detail_inners">
                <?php //print_r($proj_image); ?>
                <div class="row">   
                    <div class="col-md-9 col-sm-8 col-xs-12" id="page-content">
                        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                                <!-- Loading Screen -->
                                <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                    <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                </div>
                                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
                                    <a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
                                    <?php
                                    foreach ($propertyImages as $property_value) {
                                        ?>
                                        <div>
                                            <img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                            <img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                        </div>
                                    <?php } ?>
									<?php
                                    foreach ($proj_image as $project_value) {
                                        ?>
                                        <div>
                                            <img data-u="image" src="<?php echo $this->base; ?>/<?php echo $project_value['ProjectImage']['image_name']; ?>" />
                                            <img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $project_value['ProjectImage']['image_name']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Thumbnail Navigator -->
                                <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                                    <!-- Thumbnail Item Skin Begin -->
                                    <div data-u="slides" style="cursor: default;">
                                        <div data-u="prototype" class="p">
                                            <div class="w">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                            </div>
                                            <div class="c"></div>
                                        </div>
                                    </div>
                                    <!-- Thumbnail Item Skin End -->
                                </div>
                                <!-- Arrow Navigator -->
                                <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                                <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                            </div>
                        <div class="space-m"></div>     
                        <div id="detail" class="detail-list">
                            <div class="detail-title">
                                <h2 class="title-left">Detail</h2> <?php //print_r($all_property_features); ?>
                            </div>
                            <ul class="list-two-col">
                                
                                <?php
                                    if (!empty($all_property_features['1'])) {
                                        ?>
                                        <li><strong>Super Built Up Area:</strong> <?php echo $all_property_features['1']; ?> <?php echo $all_property_features_unit[1]; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['2'])) {
                                        ?>
                                        <li><strong>Built Up Area:</strong> <?php echo $all_property_features['2']; ?> <?php echo $all_property_features_unit[2]; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['3'])) {
                                        ?>
                                        <li><strong>Carpet Area:</strong> <?php echo $all_property_features['3']; ?> <?php echo $all_property_features_unit[3]; ?></li>
                                    <?php } ?>

                                    <?php if (!empty($all_property_features['20'])) {
                                        ?>
                                        <li><strong>Plot Area:</strong> <?php echo $all_property_features['20']; ?> <?php echo $all_property_features_unit[20]; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['21'])) {
                                        ?>
                                        <li><strong>Length of plot:</strong> <?php echo $all_property_features['21']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['22'])) {
                                        ?>
                                        <li><strong>Width of plot:</strong> <?php echo $all_property_features['22']; ?> Sq.ft</li>
                                    <?php } ?>

                                    <?php if (!empty($all_property_features['4'])) {
                                        ?>
                                        <li><strong>Bedrooms:</strong> <?php echo $all_property_features['4']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['5'])) {
                                        ?>
                                        <li><strong>Bathrooms:</strong> <?php echo $all_property_features['5']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['33'])) {
                                        ?>
                                        <li><strong>Washrooms:</strong> <?php echo $all_property_features['33']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['6'])) {
                                        ?>
                                        <li><strong>Balconies:</strong> <?php echo $all_property_features['6']; ?></li>
                                    <?php } ?>
                                
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="space-m"></div> 
                        <div id="addtional-detail">
                            <div class="detail-title">
                                <h2 class="title-left">Additional details</h2>
                            </div>
                            
                            <ul class="list-two-col list_odetail">
                                <?php if (!empty($project['Project']['project_name'])) {
                                    ?>
                                    <li><strong>Project name:</strong> <span><?php echo $project['Project']['project_name']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($project['Project']['dtp1'])) {
                                    ?>
                                    <li><strong>Launch date:</strong> <span><?php echo $project['Project']['dtp1']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['offer_price'])) {
                                    ?>
                                    <li><strong>Offer Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['offer_price']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['market_price'])) {
                                    ?>
                                    <li><strong>Market Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_price']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['market_rent'])) {
                                    ?>
                                    <li><strong>Market Rent:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_rent']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['expected_monthly'])) {
                                    ?>
                                    <li><strong>Expected Monthly Rent:</strong> <span><?php echo $propertydetails['Property']['expected_monthly']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['deposit'])) {
                                    ?>
                                    <li><strong>Deposit Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['deposit']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['booking_amount'])) {
                                    ?>
                                    <li><strong>Booking Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['booking_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['maintance_amount'])) {
                                    ?>
                                    <li><strong>Maintance Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['maintance_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['possession_date'])) {
                                    ?>
                                    <li><strong>Possession By:</strong> <span>
									<?php //echo $project['Project']['dtp2']; ?>
									<?php
										$effectiveDate = date('Y-m-d', strtotime("+".$propertydetails['Property']['possession_date']." months", strtotime($propertydetails['Property']['created_date'])));
									//echo $effectiveDate;
										?>
									<?php echo date('Y-m-d', strtotime($propertydetails['Property']['created_date'])); ?>
									To <?php echo $effectiveDate; ?>
									</span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['ownership_type_id'])) {
                                    ?>
                                    <li><strong>Ownership Type:</strong> <span><?php
                                            if ($propertydetails['Property']['ownership_type_id'] == '1') {
                                                echo "FreeHold";
                                            }if ($propertydetails['Property']['ownership_type_id'] == '2') {
                                                echo "Lease Hold";
                                            }if ($propertydetails['Property']['ownership_type_id'] == '3') {
                                                echo "Power of Attorney";
                                            }if ($propertydetails['Property']['ownership_type_id'] == '4') {
                                                echo "Co-operative Society";
                                            }
                                            ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['sale_type'])) {
                                    ?>
                                    <li><strong>Sale Type:</strong> <span><?php
                                            if ($propertydetails['Property']['sale_type'] == '1') {
                                                echo "Resale";
                                            } else {
                                                echo "New Booking";
                                            }
                                            ?></span></li>
                                <?php } ?>

                                <?php
                                if (!empty($all_property_features['10'])) {
                                    ?>
                                    <li><strong>Property on Floor :</strong> <span><?php echo $all_property_features['10']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['30'])) {
                                    ?>
                                    <li><strong>Floors in property:</strong> <span><?php echo $all_property_features['30']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['locality'])) {
                                    ?>
                                    <li><strong>Address:</strong> <span><?php echo $propertydetails['Property']['locality']; ?>,<?php echo $propertydetails['Property']['city_data']; ?>,<?php echo $propertydetails['Property']['state_data']; ?>,<?php echo $propertydetails['Property']['country_data']; ?>,<?php echo $propertydetails['Property']['pincode']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['7'])) {
                                    ?>
                                    <li><strong>Age of property:</strong> <span><?php echo $all_property_features['7']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['8'])) {
                                    ?>
                                    <li><strong>Furnishing:</strong> <span><?php echo $all_property_features['8']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['23'])) {
                                    ?>
                                    <li><strong>Floor Allowed For Construction:</strong> <span><?php echo $all_property_features['23']; ?></span></li>
                                <?php } ?>
                                <?php
                                if (!empty($all_property_features['11'])) {
                                    ?>
                                    <li><strong>Reserved Parking:</strong> <span>None</span></li>
                                    <?php
                                } else {
                                    ?>
                                    <?php if (!empty($all_property_features['45'])) {
                                        ?>
                                        <li><strong>No of Covered Parking:</strong> <span><?php echo $all_property_features['37']; ?></span></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['46'])) {
                                        ?>
                                        <li><strong>No of Open Parking:</strong> <span><?php echo $all_property_features['38']; ?></span></li>
                                        <?php
                                    }
                                }
                                ?>
                                <?php
                                $otherroom = '';
                                if (!empty($all_property_features['12']) && $all_property_features['12'] == '1') {
                                    $otherroom .= ' Pooja,';
                                }
                                if (!empty($all_property_features['39']) && $all_property_features['39'] == '1') {
                                    $otherroom .= ' Study,';
                                }
                                if (!empty($all_property_features['40']) && $all_property_features['40'] == '1') {
                                    $otherroom .= ' Servant,';
                                }
                                if (!empty($all_property_features['41']) && $all_property_features['41'] == '1') {
                                    $otherroom .= ' Other,';
                                }
                                ?>
                                <?php
                                if (!empty($otherroom)) {
                                    ?>
                                    <li><strong>Other Rooms:</strong> <span><?php echo rtrim($otherroom, ','); ?></span></li>
                                <?php } ?>
                            </ul>


                        </div>
                        <div class="clearfix"></div>
                        <div class="space-m"></div> 
                        <!-- basic detail end -->
                            <div id="description" class="detail-features">
                            <div class="detail-title">
                            <h2 class="title-left">Description</h2>
                            </div>
                            <div class="property_des">
							<?php echo $propertydetails['Property']['description']; ?>
							</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="space-m"></div> 
                          <div id="detail" class="detail-list detail-block target-block">
                            <div class="detail-title">
                                <h2 class="title-left">Project Detail</h2>



                            </div>
                            <div class="builder-view">
                                <div class="col-md-4">
								
                                    <?php
									//echo '<pre>'; print_r($projectdetail);
                                    if (!empty($projectdetail['ProjectDetails']['proj_logo'])) {
                                        ?>
                                        <img src="<?php echo $this->webroot; ?>upload/project_logo/<?php echo $projectdetail['ProjectDetails']['proj_logo']; ?>" style="border: 1px solid #eaeaea;"> 
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo $this->webroot; ?>img/front/projectImg.png" style="    border: 1px solid #eaeaea;"> 
                                    <?php } ?>
									
                                </div>
                                <div class="col-md-8">
                                    <ul class="list-three-col">
                                        <li><strong>Launch date:</strong><?php echo $project['Project']['dtp1']; ?></li>
                                        <li><strong>Expected Completion date:</strong><?php echo $project['Project']['dtp2']; ?></li>
                                        <li><strong>Land Area: </strong><?php echo $projectdetail['ProjectDetails']['land_area']; ?> Acres</li>

                                        <li><strong>Number Of Towers:</strong><?php echo $projectdetail['ProjectDetails']['num_of_tow']; ?></li>
                                        <?php if (!empty($projectdetail['ProjectDetails']['total_units'])) { ?>
                                            <li><strong>Total Units:</strong><?php echo $projectdetail['ProjectDetails']['total_units']; ?></li>
                                        <?php } ?>
                                        <li><strong>Launch Units:</strong><?php echo $projectdetail['ProjectDetails']['launch_units']; ?></li>
                                        <?php //if (!empty($projectdetail['ProjectDetails']['web_link'])) { ?>
                                            <!--<li><strong>WebLink:</strong><?php //echo $projectdetail['ProjectDetails']['web_link']; ?></li>-->
                                        <?php// } ?>
                                        <li><strong>Construction Status:</strong><?php
                                            if ($projectdetail['ProjectDetails']['cons_status'] == '1') {
                                                echo "Under Construction";
                                            } else {
                                                echo "Ready to move";
                                            }
                                            ?></li>
											
											<li>
										<?php if (!empty($projectdetail['ProjectDetails']['rera'])) { ?>
                                            <li><strong>RERA Id:</strong><?php echo $projectdetail['ProjectDetails']['rera']; ?></li>
                                        <?php } ?>
										
                                        <?php
                                        $paymentplan = json_decode($project_pricing['ProjectPricing']['payment_plan'], true);
                                        $plan = '';
                                        if ($paymentplan['price_payplan1'] == '1') {
                                            $plan .= ' Construction Linked Plan ,';
                                        }
                                        if ($paymentplan['price_payplan2'] == '1') {
                                            $plan .= ' Flexi Payment Plan ,';
                                        }
                                        if ($paymentplan['price_payplan3'] == '1') {
                                            $plan .= ' Down Payment Plan ,';
                                        }
                                        if ($paymentplan['price_payplan4'] == '1') {
                                            $plan .= ' Subvention Plan ,';
                                        }
                                        if (!empty($plan)) {
                                            ?>
                                            <li><strong>Payment Plan:</strong><?php
                                                echo rtrim($plan, ',');
                                                ?></li>
                                            <?php
                                            if ('' != $project_pricing['ProjectPricing']['price_sp_offer'])
                                                echo '<li><strong>Offer: </strong>' . $project_pricing['ProjectPricing']['price_sp_offer'] . '</li>';
                                            ?>
                                            <?php
                                            if ('' != $project_pricing['ProjectPricing']['price_remarks'])
                                                echo '<li><strong>Remarks: </strong>' . $project_pricing['ProjectPricing']['price_remarks'] . '</li>';
                                            ?>
                                        <?php } ?>
										
                                    </ul>
                                </div>
                                <div class="clear"></div>
                                <hr />
                                <div class="builder-item"><strong>Highlights of Project:</strong><br>
                                    <?php echo $project['Project']['project_highlights']; ?>
                                </div>
                                <div class="builder-item"><strong>Description of Project:</strong><br>
                                    <?php echo $project['Project']['project_description']; ?>
                                </div>
                                <?php
								 if (!empty($project_financer)) {
                                $banks = json_decode($project_financer['bank'], true);
                                $bank = array_values($banks);
									//print_r($bank);
                                if (!empty($bank)) {
                                    ?>
                                    <div class="block-item">
                                        <strong>Financers:</strong><br>
                                        <ul class="banklogo">
                                            <?php if (in_array("1", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/icicilogo1.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("25", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/alahabad25.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("29", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/andhra29.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("28", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/bajaj-finance28.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("6", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/bob6.png"></li>
                                            <?php } ?>
											<?php if (in_array("33", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/boi33.png"></li>
                                            <?php } ?>
											<?php if (in_array("11", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/bom11.png"></li>
                                            <?php } ?>
											<?php if (in_array("15", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/canera15.png"></li>
                                            <?php } ?>
											<?php if (in_array("17", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/cbi17.png"></li>
                                            <?php } ?>
											<?php if (in_array("3", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/citybank3.png"></li>
                                            <?php } ?>
											<?php if (in_array("16", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/corporation16.png"></li>
                                            <?php } ?>
											<?php if (in_array("26", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/dena26.png"></li>
                                            <?php } ?>
											<?php if (in_array("27", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/dhfl-home-loan27.png"></li>
                                            <?php } ?>
											<?php if (in_array("32", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/federal32.png"></li>
                                            <?php } ?>
											<?php if (in_array("2", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/hdfc2.png"></li>
                                            <?php } ?>
											<?php if (in_array("35", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/hdfc-home-loan35.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("8", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/hsbc8.png"></li>
                                            <?php } ?>
											<?php if (in_array("18", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/idbi18.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("10", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/indiabulls10.png"></li>
                                            <?php } ?>
											<?php if (in_array("13", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/indianbank13.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("22", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/indianocerseas22.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("23", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/indusland23.png"></li>
                                            <?php } ?>
											<?php if (in_array("19", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/ing-vyasa19.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("20", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/kotak.png"></li>
                                            <?php } ?>
											<?php if (in_array("7", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/lic-housing-finance7.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("5", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/pnb5.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("14", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/pnb-housing-finance14.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("12", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/punjab-sind12.png"></li>
                                            <?php } ?>
											<?php if (in_array("36", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/reliance36.png"></li>
                                            <?php } ?>
											<?php if (in_array("4", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/sbi4.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("31", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/sbt31.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("21", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/syndicate21.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("24", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/uco24.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("34", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/unianbank34.jpg"></li>
                                            <?php } ?>
											<?php if (in_array("30", $bank)) { ?>
                                                <li><img src="<?php echo $this->webroot; ?>img/banks/vijaya30.jpg"></li>
                                            <?php } ?>
                                            

                                        </ul>
                                    </div>
                                <?php } } ?>
                            </div>
                        </div>
						
						
                        <div id="features">
                            <div class="detail-title">
                                <h2 class="title-left">Features</h2>
                            </div>
                            
                            <ul class="list-two-col list-features">
                                <?php if (!empty($all_property_features['31'])) {
                                    ?>
                                    <li><strong>Type of Flooring:</strong> <?php echo $all_property_features['31']; ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['32'])) {
                                    ?>
                                    <li><strong>Power Backup:</strong> <?php echo $all_property_features['32']; ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['18']) || !empty($all_property_features['36'])) {
                                    ?>
                                    <li><strong>Water Source:</strong> <?php
                                        $source = '';
                                        if (!empty($all_property_features['18'])) {
                                            $source .= "Municipal,";
                                        } if (!empty($all_property_features['36'])) {
                                            $source .= " Borewell Tank ";
                                        }
                                        ?><?php echo rtrim($source, ','); ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['facing'])) {
                                    ?>
                                    <li><strong>Facing:</strong> <?php echo $all_property_features['28']; ?></li>
                                <?php } ?>

                                <?php if (!empty($all_property_features['13'])) {
                                    ?>
                                    <li><strong>Width of facing road:</strong> <?php echo $all_property_features['13']; ?> <?php echo $all_property_features_unit[13]; ?></li>
                                <?php } ?>
                                <?php
                                if (!empty($all_property_features['14']) || !empty($all_property_features['42']) || !empty($all_property_features['43']) || !empty($all_property_features['44'])) {
                                    $overlook = '';

                                    if (!empty($all_property_features['14'])) {
                                        $overlook .= "Park/Garden, ";
                                    } if (!empty($all_property_features['42'])) {
                                        $overlook .= "Main Road, ";
                                    }if (!empty($all_property_features['43'])) {
                                        $overlook .= "Club, ";
                                    }if (!empty($all_property_features['44'])) {
                                        $overlook .= "Pool, ";
                                    }
                                    ?>
                                    <li><strong>Overlooking:</strong> <?php echo rtrim($overlook, ','); ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['15'])) {
                                    ?>
                                    <li><strong>Bachelors Allowed:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['16'])) {
                                    ?>
                                    <li><strong>Pet allowed:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['17'])) {
                                    ?>
                                    <li><strong>Non vegetarian:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['25'])) {
                                    ?>
                                    <li><strong>Boundary wall:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['26'])) {
                                    ?>
                                    <li><strong>Society Type:</strong> <span><?php echo $all_property_features['26']; ?></span></li>
                                <?php } ?>
                            </ul>


                        </div>
                        <div class="clearfix"></div>
                        <div class="space-m"></div> 
                        <!-- Amenities -->
                        <div id="amenitie" class="detail-amenities">
                            <div class="detail-title">
                                <h2 class="title-left">Amenities</h2>
                            </div>
                            <div class="row">
                                <?php 
                                    /*$amenities = explode(',', $item->amenities[0]);
                                    $icon_array = array(
                                    'Bank Attached Property'        => 'lift',
                                    'Centrally Air Conditioned'     => 'airc',
                                    'Club House/Community'          => 'com_hall',
                                    'Feng Shui/Vasstu Compliant'    => 'vaastu',
                                    'Fitness Centre/GYM'            => 'gym',
                                    'Intercom Facility'             => 'intercomm',
                                    'Internet/wi-fi connectivity'   => 'internet',
                                    'Lift(S)'                       => 'lift',
                                    'Maintenance Staff'             => 'staff',
                                    'Park'                          => 'park"',
                                    'Piped-Gas'                     => 'waste',
                                    'Piped-gas'                     => 'waste',
                                    'Rain Water Harvesting'         => 'rainwater',
                                    'Security Personnel'            => 'securityp',
                                    'Security/Fire Alarm'           => 'security',
                                    'Shopping /Center'              => 'shopping',
                                    'Swimming Pool'                 => 'pool',
                                    'Visitor Parking'               => 'parking',
                                    'Water Purifier'                => 'water_plant',
                                    'Water storage'                 => 'water_storage'
                                    );*/
                                ?>
                                <div class="col-md-12">
                                    <ul class="list-amenities">                                        
										<div class="row">
											<div class ="col-md-4">
											<?php
										   //echo '<pre>'; print_r($all_property_amenities);
											$count = 0;
											if(!empty($all_property_amenities))
											{
											//foreach ($amenities as $key => $amevalu) {
												?>

												<!--<div class= "customDiv">column1</div>-->
												<?php
												if (isset($all_property_amenities[6]) && $all_property_amenities[6] == 'Yes') {
													?>
													<li><span class="icon_amen icon_vaastu">Feng shui/Vaastu compliant</span></li>
													<?php
													echo "</div>";
													echo '<div class ="col-md-4">';
												}
												
												?>

                                            <?php
                                            if (isset($all_property_amenities[1]) && $all_property_amenities[1] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_lift">Lift</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[2]) && $all_property_amenities[2] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_park">Park</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[3]) && $all_property_amenities[3] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_staff">Maintenance Staff</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[4]) && $all_property_amenities[4] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_parking">Visitor Parking</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[5]) && $all_property_amenities[5] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_water_storage">Water Storage</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>



                                            <!-- <div class= "customDiv">column2</div>-->

                                            <?php
                                            if (isset($all_property_amenities[7]) && $all_property_amenities[7] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_intercomm">Intercomm Facility</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>

                                            <?php
                                            if (isset($all_property_amenities[16]) && $all_property_amenities[16] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_waste">Waste Disposal</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[17]) && $all_property_amenities[17] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_tarrace">Private Garden/Terrace</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[18]) && $all_property_amenities[18] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_securityp">Security Personnel</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[21]) && $all_property_amenities[21] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_lift">Service/Goods Lift</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>


                                            <!-- <div class= "customDiv">column3</div>-->
                                            <?php
                                            if (isset($all_property_amenities[22]) && $all_property_amenities[22] == 'Yes') {
                                                ?>   
                                                <li><span class="icon_amen icon_conroom">Conference Room</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[19]) && $all_property_amenities[19] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_pool">Swimming Pool</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[20]) && $all_property_amenities[20] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_atm">ATM</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[23]) && $all_property_amenities[23] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_airc">Centrally Airconditioned</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[24]) && $all_property_amenities[24] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_internet">High Speed Internet</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[25]) && $all_property_amenities[25] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_foodc">Cafeteria/Food court</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
											<?php
                                            if (isset($all_property_amenities[8]) && $all_property_amenities[8] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_security">Security/Fire alarm</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>
											
                                            <?php
                                            if (isset($all_property_amenities[15]) && $all_property_amenities[15] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_rainwater">Rain water harvesting</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-4">';
                                            }
                                            ?>

                                            
                                        <?php } //}?>

                                    </div>
                                </div>


                                    </ul>
                                </div>
								<!-- Aminities End Here  -->
								<div id="" class="detail-map detail-block target-block col-md-12">
									<div class="detail-title">
										<h2 class="title-left">Location</h2>
									</div>
									<div class="inner-block">

										<div id="map_canvas" style="height:250px;border:0" ></div>
									</div>
								</div>
								<?php  if(!empty($projectPricingPlansArr) && $projectPricingPlansArr != ''){ ?>
									<div id="" class="detail-map detail-block target-block col-md-12">
										<div class="detail-title">
											<h2 class="title-left">Plan</h2>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="" class="label_title"><strong>Select Plan </strong></label>
													<div class="select-style">
														<div class="input select">
															<select name="Pricing Plan" id="id_pricing_plan">
															   <?php															
																foreach($projectPricingPlansArr as $key=>$value) {
																	if($projectPricingPlansArr[$key] != ''){
																		echo '<option value=\'' . $key . '\'>' . $projectPricingPlansArr[$key] . '</option>';
																	}else{
																		echo '<option value=\'' . $key . '\'>Default Plan</option>';
																	}
																}
																?>
															</select>
														</div>
													</div>	
												</div>	
											</div>
										</div>
									</div>
								<?php } ?>
								
								<div id="calyourprice" class="detail-amenities detail-block target-block col-md-12">
									<?php								
										echo $this->Html->script('highcharts');
										echo $this->Html->script('highcharts-3d');								
									?>
									<div class="">
										<div class=" col-md-12">
											<div class="detail-title">
												<h2  class="title-left">Project Price Calculator</h2>
											</div>
										</div>
										
										<div id="amenities" class="detail-amenities detail-block target-block col-md-12" style="margin-top:0px;margin-bottom:0px;padding-top:0px;">

											<div class="row">
												<div class="col-sm-6">
													<label for="" class="form-group"><strong> Property area : </strong> <?php echo $gSuperBuiltUpArea ?>&nbsp;sq ft </label>
												</div>
												<div class="col-sm-6">
													<label for="" class="form-group"><strong> Base Rate : </strong> <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;  
														 
														<?php if($gBaseRateUnit == 1){ ?>
														<input name="bsp_price_value" id="bsp_price_value" value="<?php echo $gBaseRate. '&nbsp;/ sqft'; ?>" style="width:35%; border-bottom:none;">
														
														<?php } ?>
														
														<?php if($gBaseRateUnit == 2){ ?>
														<input name="bsp_price_value" id="bsp_price_value" value="<?php echo $gBaseRate. '&nbsp;'; ?>" style="width:35%; border-bottom:none;">
														
														<?php } ?>
														</label>
														
														<input type="hidden" name="bsp_price_value_unit" id="bsp_price_value_unit" value="<?php echo $gBaseRateUnit; ?>">
														<input type="hidden" name="bsp_price_value_plc" id="bsp_price_value_plc" value="<?php if(isset($project_plc_amount) && $project_plc_amount != ''){echo $project_plc_amount;}else{ echo '0';}  ?>">
														<input type="hidden" name="plc_charges_unit" id="plc_charges_unit" value="<?php echo $plc_charges_unit; ?>">

												</div>

											</div>

											<div class="row">
												<div class="col-sm-6">
													<input type="hidden" name="gst_input_credit" id="gst_input_credit" value="<?php echo $gstCreditInput; ?>">
													<label for="" class="form-group"><strong> GST(on Base) : </strong> <?php echo $gServiceTaxOnBSP ?>&nbsp;&#37; </label>
												</div>

												<div class="col-sm-6">
													<label for="" class="form-group"><strong> GST(on Others) : </strong> <?php echo $gServiceTaxOnOTH ?>&nbsp;&#37; </label>
												</div>								
											</div>

											<div class="row">
												<div class="col-sm-6" style="padding-left:10px;">
												
													<div class="col-sm-12" style="padding-left:1px;">
													
										
														<input id="regcheckclick" class="checkbox-custom" value="<?php echo $gaRegistration[0][1]; ?>" type="checkbox" onclick="ReCalculateOfferPrice9(this);">
														<label for= "regcheckclick" class="checkbox-custom-label">
															Registration Charges
														<span>
														<?php
															if(!empty($gaRegistration)){
															if (2 == $gaRegistration[0][0]) {
																// Percentage
																echo $gaRegistration[0][1] . "&nbsp;&#37";
															} else {
																// Flat Charges
																echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaRegistration[0][1];
															}
															}
														?>
														</span>
											
													
													</label> 
													<input type="hidden" id="regCheckValue" value="0" >							
												</div>
												
												
												</div>
												<?php if(!empty($gaStampDuty) && $gaStampDuty[0][1]!=0){ ?>
												<div class="col-sm-6">
													<label for="" class="form-group"><strong>Stamp Duty : </strong> 
														<?php
														//if(!empty($gaStampDuty)){
														if (2 == $gaStampDuty[0][0]) {
															// Percentage
															echo $gaStampDuty[0][1] . "&nbsp;&#37";
														} else {
															// Flat Charges
															echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaStampDuty[0][1];
														}
														//}
														?>
													</label>
												</div>
												<?php } ?>
											</div>
											<!-- If no plc charges ,  donot display the below div -->
										</div>
									</div>
									
									
									<div class="col-md-6">
										<div class="detail-title">
											<h2 class="title-left"> Mandatory Charges</h2>
										</div> 
										<div class="form-group">
											<div class="row" id="id_mandatory_charges">

												<?php echo $gaMandatoryCharges; ?>

											</div>
										</div>
									</div>

									
									
									<div class="col-md-6">
										<div class="detail-title">
											<h2 class="title-left">Optional Charges</h2>
										</div> 
										<div class="form-group ">
											<div class="row" id="id_optional_charges">

												<?php echo $gaOptionalCharges; ?>

											</div>
										</div>										
										<?php //echo '<pre>';print_r($bsp_limit);
											if(!empty($gaStampDuty)){ ?>
											<div class="row" <?php //if ($gaFloorPLC[0][1] <= 0) echo "style='display:none;'" ?> >
												<div class="col-sm-6">
													<div class="form-group">
														<label for="" class="label_title"><strong>Floor Choice </strong></label>
														<div class="select-style">
															<div class="input select">
															<?php
																//echo '<pre>'; print_r($bsp_limit);
															?>
																<select name="floor choice" id="id_floor_choice">
																	<!--<option value="0" selected="selected" >Ground</option>-->
																	<?php
																	
																	for ($opt = $bsp_limit[0][0]['startloop']; $opt <= $bsp_limit[0][0]['endloop']; $opt++) {
																		echo '<option value=\'' . $opt . '\'>' . $opt . '</option>';
																	}
																	?>
																</select>
															</div>
														</div>	
													</div>	
												</div>
											</div>	
											<?php }?>
									</div>

								
									
									
									
									
									
									
									
									<div class="col-md-12 text-center">

									<div class="caltotprice">
										<div class="caltitle">Your Final Calculated Price</div>
										<span class="calprice" id="id_calprice">
										<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo round($gOfferPrice, 2); ?>
										</span>
										<div class="caltitle">
											<span id="id_calprice_words">
												<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;
											</span> 
										</div>
										<div id="id_chart"></div>
										<div id="container" style="width:100%; height:300px;" >

										</div>	
									</div>

								</div>
							
									<div  class="detail-amenities detail-block target-block col-md-12">
										<div class="builder-view col-md-12">
											<div class="detail-title">
												<h2 class="title-left">Organization Name: <?php echo $Websiteuser['Websiteuser']['userorgname']; ?></h2>
											</div> 
											<div class="builder-item"><strong>Established year:</strong> <?php if(!empty($builder)){ echo $builder['Builder']['borg_year'];} ?></div>
											<div class="builder-item"><strong>About:</strong> <?php if(!empty($builder)) { echo $builder['Builder']['borg_about'];} ?></div>
											<?php if (!empty($pastprojectdata)) { ?>	
												<div class="builder-item"><strong>Past Projects: </strong><?php echo $pastprojectdata; ?></div>
											<?php } ?>
											</div>
									</div>
							

								</div> 
								
								<div id="newblock">
								</div>

                            </div>
                        </div>
                    </div>  
                    <!-- sidebar -->
					<?php //if ($this->Session->read('Auth.Websiteuser.userrole') == '') { ?>
						<div class="col-md-3 col-sm-4 col-xs-12">
							<div id="contact-biulder">					
								<div class="sidebar-inner">
								<div class="text-center" id="id_contact_succ" style="display:none;"></div>
								<div id="id_contact_form">
									<form class="fpform" id="contact-form" method="post">
										<div class="row">
											<h3>Contact Builder</h3>
											<div class="message"></div>
											<div class="col-sm-12 col-xs-12">
												<div class="form-group">
													<input id="user1" class="radio-custom" name="user_type" type="radio" value="individual">
													<label for="user1" class="radio-custom-label">Individual</label>
													<input id="user2" class="radio-custom" name="user_type" type="radio" value="dealer">
													<label for="user2" class="radio-custom-label">Dealer</label>
												</div>
											</div>
											<input class="form-control propertyid" name="propertyid" type="hidden" value="<?php echo $propertydetails['Property']['property_id']; ?>">
											<input class="form-control" name="userid" type="hidden" value="<?php echo $propertydetails['Property']['user_id']; ?>">
											<div class="col-sm-12 col-xs-12">
												<div class="form-group">
													<input class="form-control" name="name" placeholder="Your Name" type="text">
												</div>
											</div>
											<div class="col-sm-12 col-xs-12">
												<div class="form-group">
													<input class="form-control" placeholder="Phone" name="phone" id="user_mobile" type="text" maxlength="10">
												</div>
											</div>
											<div class="col-sm-12 col-xs-12">
												<div class="form-group">
													<input class="form-control" placeholder="Email" name="email" type="email">
												</div>
											</div>
											<div class="col-sm-12 col-xs-12">
												<div class="form-group">
													<label class="label_title">Interested in (Optional)</label>
													<select id="interested" name="interested[]" class="selectpicker" multiple>
														<option value="assisted search">Assisted Search </option>
														<option value="immediate purchase">Immediate Purchase</option>
														<option value="portfolio management">Portfolio Management</option>
													</select>
													
												</div>	
											</div>	
											<input type="hidden" name="property_id" class="modal-propid">
											<div class="text-center col-sm-12 "><button class="btn btn-primary">Submit</button></div>
											<div class="text-center col-sm-12 " style="padding:10px 0px;">I agree with Fairpockets <a href="/terms-conditions">T&amp;C</a> </div>
										</div>
									</form>
								</div>	
								</div>
							</div>
						</div>
					<?php //} ?>
                </div>
                <div class="clearfix"></div>
                <div class="space-m"></div> 
            </div>
        </div>
    </div>
</section>

<section id="similer-propertys" class="grid-view" style="display:none;">
    <div class="container">
        <div class="detail-title">
            <h2 class="title-left">Similar properties</h2>
        </div>
        <div class="owl-carousel">
            <?php
                
                foreach($similar as $similar_property){
                    foreach ($similar_property as $proprerty_list){
                        
                        
                        if($proprerty_list['transaction_type'] == 'Sell'){
                            $transaction_type = 'buy';
                            }else{
                            $transaction_type = $proprerty_list['transaction_type'];
                        }
                        
                        $property_url_address  = (!empty($proprerty_list['sublocality1'])) ? $proprerty_list['sublocality1'].'&nbsp;' : '';
                        //$url_address .= (!empty($item['city'])) ? $item['city'] : '';
                        $title = (strpos($proprerty_list['configure'], 'BHK') !== false ? $proprerty_list['configure'] : $proprerty_list['configure'] . 'BHK') .' '. $proprerty_list['property_type'] . ' for '. $proprerty_list['transaction_type'];   
                        $proprerty_address  = (!empty($proprerty_list['sublocality3'])) ? $proprerty_list['sublocality3'].',&nbsp;' : '';
                        $proprerty_address .= (!empty($proprerty_list['sublocality2'])) ? $proprerty_list['sublocality2'].',&nbsp;' : '';
                        $proprerty_address .= (!empty($proprerty_list['sublocality1'])) ? $proprerty_list['sublocality1'].',&nbsp;' : '';
                        $proprerty_address .= (!empty($proprerty_list['city'])) ? $proprerty_list['city'] : '';
                        
                        $contact = $proprerty_list['posted_by'];
                        if($contact == 'Owner') {
                            $contact = 'Contact Owner';
                            } else if($contact == 'Broker') {
                            $contact = 'Contact Dealer';
                            } else {
                            $contact = 'Contact Builder';
                        }
            ?>
            <div class="item">
                <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">
                                <div class="price hide-on-list">
                                <h3><i class="fa fa-inr" aria-hidden="true"></i><?php echo (!empty($proprerty_list['offer_price'])) ? $this->Number->format($proprerty_list['offer_price']) : '0.00';?></h3>
                                <?php if(!empty($item['discount_percent'])):?>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo ceil($proprerty_list['discount_percent']);?>%</p>
                                </div>
                                <?php endif; ?>
                                <?= $this->Html->link($this->html->image('front/property/02_434x290.jpg', array('alt'=>$title)), array("controller" => false,"action" => "/searchListDetail", base64_encode($item->id)), array('escape' => false, 'title' => $title, 'target' => '_blank'));?>
                                
                            </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For <?php echo $proprerty_list['transaction_type'];?></div>
                                    <span class="label label-danger"><?php echo $proprerty_list['sale_type'];?></span>
                                </div>
                                <h2 class="property-title">
                                <?php
                                    echo $this->Html->link($title, array("controller"=>false,"action" => "/searchListDetail", base64_encode($proprerty_list->id)), array('escapeTitle' => false, 'title' => $title, 'target' => '_blank'));
                                ?>
                                </h2>
                                <h4 class="property-location"><?php echo $proprerty_address;?></h4>
                                <div class="pro_tt"><?php echo $title;?></div>
                                
                                <div class="pro_area"><?php echo ($proprerty_list['super_builtup_area'] ? $proprerty_list['super_builtup_area'].' Sq.ft. Super Built Up Area' : $proprerty_list['builtup_area'].' Sq.ft. Built Up Area');?></div>
                            </div>
                        </div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                            </div>
                            <div class="info-row phone text-right">
                                <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <div class="item-foot-left">
                            <p><i class="fa fa-user"></i> <a href="#"><?php echo $contact; ?></a></p>
                        </div>
                        <div class="item-foot-right">
                            <p><i class="fa fa-calendar"></i> <?php echo date('d-M-Y', strtotime($proprerty_list['post_date']));?></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                
            </div>
            <?php }
             }
            ?>
        </div>
    </div>      
</section>

<script>
$(document).ready(function () {
	$("#id_pricing_plan").on('change', function () {
		$('#calyourprice').css('display','none');
		var id_pricing_plan = $("#id_pricing_plan").val();
		var propertyid = $(".propertyid").val();
		//alert(id_pricing_plan);
		//alert(propertyid);
		if (id_pricing_plan) {
			var dataString = 'id_pricing_plan=' + id_pricing_plan+'&propertyid='+propertyid;
			$.ajax({
				type: "POST",
				url: '<?php echo Router::url(array("controller" => "pages", "action" => "price_calculator_block_ajax")); ?>',
				data: dataString,
				cache: false,
				success: function (html) {
				//alert('hello');
					$('#newblock').html(html);
					
				}
			})
		}
	});
});
</script>

<?= $this->Html->script(['validation_error_messages', 'search_list', 'owl-carousel'], ['block' => 'scriptBottom']);?>   




<script type="text/javascript">
    jssor_1_slider_init = function () {

        var jssor_1_SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*you can remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>
<script type="text/javascript">jssor_1_slider_init();</script>
<?php



$gLng = $project['Project']['lng'];
$gLat = $project['Project']['lat'];
$gTitle = $project['Project']['project_name'] . ',' . $project['Project']['locality'] . ', ' . $project['Project']['city_data'];
echo "
        <input type='hidden' id='id_gLat' value='" . $gLat . "'>
        <input type='hidden' id='id_gLng' value='" . $gLng . "'>
        <input type='hidden' id='id_gTitle' value='" . $gTitle . "'>
    ";
?>  



<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<script type="text/javascript">

    var map;
    var lat = parseFloat(document.getElementById("id_gLat").value);
    var lng = parseFloat(document.getElementById("id_gLng").value);
    var tmp = document.getElementById("id_gTitle").value;

    var infowindow = new google.maps.InfoWindow({});

    function initialize()
    {
        geocoder = new google.maps.Geocoder();

        var latlng = new google.maps.LatLng(lat, lng);

        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Property Location"
        });

        marker['infowindow'] = tmp;

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
        });

    }

    window.onload = initialize;

</script>

<?php
// Generate client side offer price calculator part

echo "
		<input type='hidden' id='id_gProjectChargesAmt' value='" . $gProjectCharges_a . "'>
	";
if(!empty($gaStampDuty)){
if (2 == $gaStampDuty[0][0]) {
    // Percentage
    echo "<input type='hidden' id='id_gaStampDuty' name='percent' value='" . $gaStampDuty[0][1] . "'>";
} else {
    // Flat Charges
    echo "<input type='hidden' id='id_gaStampDuty' name='amount' value='" . $gaStampDuty[0][1] . "'>";
}
}

if(!empty($gaRegistration)){
if (2 == $gaRegistration[0][0]) {
    // Percentage
    echo "<input type='hidden' id='id_gaRegistration' name='percent' value='" . $gaRegistration[0][1] . "'>";
} else {
    // Flat Charges
    echo "<input type='hidden' id='id_gaRegistration' name='amount' value='" . $gaRegistration[0][1] . "'>";
}
}
//echo '<pre>'; print_r($gaFloorPLC);
if(!empty($gaFloorPLC)){
	
	if($gaFloorPLC[0][0] != ''){	
	$gafplc = $gaFloorPLC[0][0];
	}else{
		$gafplc = 1;
	}

	if($gaFloorPLC[0][1] != ''){	
	$gafplc1 = $gaFloorPLC[0][1];
	}else{
		$gafplc1 = 0;
	}

	if($gaFloorPLC[0][2] != ''){	
	$gafplc2 = $gaFloorPLC[0][2];
	}else{
		$gafplc2 = 1;
	}
//$gafplc1 = $gaFloorPLC[0][1];
//$gafplc2 = $gaFloorPLC[0][2];
} else{ $gafplc ='';$gafplc1 ='';$gafplc2 ='';}
/*echo '884';
echo $gBaseRate.'-base rate<br/>';
echo $gServiceTaxOnBSP.'-base rate<br/>';
echo $gServiceTaxOnOTH.'-base rate<br/>';
echo $gafplc.'-base rate<br/>';
echo $gafplc1.'-base rate<br/>';
echo $gafplc2.'-base rate<br/>';
 $gSuperBuiltUpArea.'-base rate<br/>';
echo $gaTotalFloors.'-base rate<br/>';*/
//echo  $gSuperBuiltUpArea;
//echo $gBaseRate;
echo "
	
		<script>
		
		    var BaseCharges = 0;
			var ServiceTaxCharges = 0;
			var GovtCharges = 0;
			var ProjectCharges = 0;

			function ReCalculateOfferPrice( elem )
			{
				
				var BaseCharges = 0;
				var GovtCharges = 0;
				//alert('hello');
				
				//tBaseRate = 3510;
				//alert(brate);
				//tBaseRate1 = parseInt(document.getElementById('bsp_price_value').value);
				//alert(tBaseRate1);
				//area - 1215
				//tBaseRate = " . $gBaseRate . ";
				tBaseRate = parseInt(document.getElementById('bsp_price_value').value);
				tBaseRateUnit = parseInt(document.getElementById('bsp_price_value_unit').value);
				//alert(tBaseRateUnit); //- 4650
				
				tPlcRate = parseInt(document.getElementById('bsp_price_value_plc').value);
				alert(tPlcRate);
				
				tPlcRateUnit = parseInt(document.getElementById('plc_charges_unit').value);
				//alert(tPlcRateUnit);
				tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
				
				tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
				tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
				
				eStampDuty = document.getElementById('id_gaStampDuty');
				eRegistration = document.getElementById('regCheckValue');
				
				tBSPForFloor = ".$gafplc.";
				tBSPForFloorCharges = " . $gafplc1 . ";
				tBSPForFloorOption = " . $gafplc2 . ";
				
				gstInputCredit = ".$gstCreditInput.";
				//alert(gstInputCredit);
				gstInputCreditAmount = (tBaseRate *".$gSuperBuiltUpArea.") * gstInputCredit / 100;
				//alert(gstInputCreditAmount);
				
				if ( tBaseRateUnit == 1 ){
					tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				}
				if ( tBaseRateUnit == 2 ){
					tOfferPriceAmt = tBaseRate - gstInputCreditAmount ;
				}
				
	              //alert(tOfferPriceAmt);  
				tFloorPLC = 0;
				
				if ( tBSPForFloorCharges > 0 )
				{	
					tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
					
					if ( tBSPForFloor == 2 )
					{
						// BSP w.r.t Ground floor
						tempFloor = parseInt(document.getElementById('id_floor_choice').value) ; //0 - Ground Floor
						tFloorPLC = tFloorPLC * tempFloor ;
						
					}
					else
					{
						// BSP w.r.t TOP floor;
						tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice').value) + 1; //0 - Ground Floor
						tFloorPLC = tFloorPLC * tempFloor ;
					}			
					
					if ( tBSPForFloorOption == 2 )
					{
						// sub option
						tFloorPLC = tFloorPLC*(-1);
					}	

				}
				
				nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
				BaseCharges += nOfferPriceAmt;
				
				ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
				nOfferPriceAmt = nOfferPriceAmt + (nOfferPriceAmt * (tServiceTaxOnBSP / 100)) ;
				//alert(nOfferPriceAmt.toFixed(2));
				if ( elem )
				{	
					// Change in optional element
					
					if ( true == elem.checked )
					{	
						//alert('Checked  - '+elem.value);
						tProjectChargesAmt = tProjectChargesAmt + parseInt(elem.value);
						document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
						
					}	
					else
					{	
						//alert('Unchecked - '+elem.value);
						tProjectChargesAmt = tProjectChargesAmt - parseInt(elem.value);
						document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
					}
				}
				else
                {
					// Change in choice of floor
					
					tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                } 
				
				if ( tPlcRateUnit == 1 ){
					plcAmountArea = tPlcRate*".$gSuperBuiltUpArea.";
					plcAmountGst = (tPlcRate*".$gSuperBuiltUpArea.")*tServiceTaxOnOTH/100;
				}
				
				if ( tPlcRateUnit == 2 ){
					plcAmountArea = tPlcRate;
					plcAmountGst = (tPlcRate*tServiceTaxOnOTH)/100;
				}
				
				plcAmountTotal = plcAmountArea + plcAmountGst;
				//alert(plcAmountTotal);
				
				nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
				//alert(nServiceChargesAmt);
				nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt + plcAmountTotal ; 	
				//alert(nOfferPriceAmt);
				if ( 'amount' == eStampDuty.name )
				{
					GovtCharges += parseInt(eStampDuty.value);
					nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
				}
				else
				{
					GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
					nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
				}		
				
				if ( 'amount' == eRegistration.name )
				{
					GovtCharges +=  parseInt(eRegistration.value);
					nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
					
				}
				else
				{
					GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
					nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt + plcAmountArea ) * (parseInt(eRegistration.value)/100) );
					//alert(nOfferPriceAmt);
				}
				
				//alert('Final Offer Price ='+nOfferPriceAmt);
				

				
						
				document.getElementById('id_calprice').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits: 2});
				
				nOfferPriceAmt = Math.abs(nOfferPriceAmt)
            if (nOfferPriceAmt >= 10000000)
            {
                formattedNumber = (nOfferPriceAmt / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (nOfferPriceAmt >= 100000)
            {
                formattedNumber = (nOfferPriceAmt / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (nOfferPriceAmt >= 1000)
            {
                formattedNumber = (nOfferPriceAmt / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = nOfferPriceAmt;
            }
				
				document.getElementById('id_calprice_words').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+formattedNumber;
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
				                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
				                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
															   'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
															   'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

				$('#container').highcharts({
				 
						chart: {
							type: 'pie',
							options3d: {
								enabled: true,
								alpha: 45
							}
						},
						title: {
							text: 'Property Charges'
						},
						tooltip: {
							//pointFormat: '{series.name}: <b>{point.value}%</b>'
						},
						plotOptions: {
							pie: {
								innerSize: 100,
								depth: 45
							}
						},
						series: [{
							data: [
								['Base Charge', eval(BaseCharges.toFixed(2)) ],
								['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
								['Government Charges', eval( GovtCharges.toFixed(2) )],
								['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
								['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))	 ]
							]
							
						}]
				});	
				
				//$('.place-desc-large').find(':first-child').first().text('new text');
				//$('.place-desc-large DIV:first-child').attr('html', 'abc');
				//$('.place-name' ).html( 'background-color');
				//$('.place-desc-large div.place-name').text('MY NEW TEXT');
			}
			
			ReCalculateOfferPrice();
			
		</script>

	";
?>	 
<!-- jQuery Form Validation code and submit lead/contact information-->
 
<?php
echo $this->Html->script('add_property_7');
// Jquery Script for Validation of form fields    
//echo $this->Html->script('validate_1.9_jquery.validate.min');

?>
<?php $projectid = $project['Project']['project_id']; ?>
<input type='hidden' id='projectid' value='<?php echo $projectid; ?>'>
<script>

$(document).ready(function () {
        $("#id_floor_choice").change(function ()
        {
            var id = $(this).val();
			var projectid = $('#projectid').val();
            if (id) {
                var dataString = 'id=' + id + '&projectid=' + projectid;
				//alert(dataString);
                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "pages", "action" => "getbspoptions1")); ?>',
                    data: dataString,
                    success: function (data) {
						alert(data);
						//data = JSON.parse(data);
						var obj = jQuery.parseJSON(data);
						//alert(data[1].bsp_charge);
						//alert(obj.bsp_charge_unit);
						if(obj.bsp_charge_unit == 1){
							$('#bsp_price_value').val(obj.bsp_charge+' / Sqft');
						}
						if(obj.bsp_charge_unit == 2){
							$('#bsp_price_value').val(obj.bsp_charge);
						}
                    	//$('#bsp_price_value').val(obj.bsp_charge);
						$('#bsp_price_value_unit').val(obj.bsp_charge_unit);
						$('#bsp_price_value_plc').val(obj.plc_charge);
						//$('#bsp_price_value').val(data);
						//alert(obj.plc_charge);
                        ReCalculateOfferPrice8(obj.bsp_charge,obj.bsp_charge_unit,obj.plc_charge);
                    }
                });  
				
            }
        });
    });

</script>

<?php

echo "
    
        <script>
        
            var BaseCharges = 0;
            var ServiceTaxCharges = 0;
            var GovtCharges = 0;
            var ProjectCharges = 0;

            function ReCalculateOfferPrice8(elem,elem1,elem2)
            {
                
                var BaseCharges = 0;
                var GovtCharges = 0;
                
                tBaseRate = elem;
				tBaseRateUnit = elem1;
				tPlcRate = elem2;
                //alert(tPlcRate);
                
            
                //tBaseRate = " . $gBaseRate . ";
                tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
                
                tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
                
                eStampDuty = document.getElementById('id_gaStampDuty');
                eRegistration = document.getElementById('regCheckValue');
                
                tBSPForFloor = " . $gafplc . ";
                tBSPForFloorCharges = " . $gafplc1 . ";
                tBSPForFloorOption = " . $gafplc2 . ";
				
				gstInputCredit = ".$gstCreditInput.";
				//alert(gstInputCredit);
				gstInputCreditAmount = (tBaseRate *".$gSuperBuiltUpArea.") * gstInputCredit / 100;
				//alert(gstInputCreditAmount);
	
	            //tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				
				if ( tBaseRateUnit == 1 ){
					tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				}
				if ( tBaseRateUnit == 2 ){
					tOfferPriceAmt = tBaseRate - gstInputCreditAmount ;
				}
				
    
                //tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
                //alert(tOfferPriceAmt);
                    
                tFloorPLC = 0;
                
                if ( tBSPForFloorCharges > 0 )
                {   
                    tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
                    
                    if ( tBSPForFloor == 2 )
                    {
                        // BSP w.r.t Ground floor
                        tempFloor = parseInt(document.getElementById('id_floor_choice').value) ; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                        
                    }
                    else
                    {
                        // BSP w.r.t TOP floor;
                        tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice').value) + 1; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                    }           
                    
                    if ( tBSPForFloorOption == 2 )
                    {
                        // sub option
                        tFloorPLC = tFloorPLC*(-1);
                    }   

                }
                
                nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
                BaseCharges += nOfferPriceAmt;
                
                ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
                nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
                //alert(nOfferPriceAmt);
                
                    // Change in choice of floor
                    
                    tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                //} 
				//alert(tServiceTaxOnOTH);
				plcAmountArea = ".$gSuperBuiltUpArea."*tPlcRate;
				plcAmountGst = (".$gSuperBuiltUpArea."*tPlcRate)*tServiceTaxOnOTH/100;
				plcAmountTotal = plcAmountArea + plcAmountGst;
				//alert(plcAmountTotal);

                nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
                nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt + plcAmountTotal ;     
                //alert(nOfferPriceAmt);
                if ( 'amount' == eStampDuty.name )
                {
                    GovtCharges += parseInt(eStampDuty.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
                }
                else
                {
                    GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
                    nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
                }       
                
                if ( 'amount' == eRegistration.name )
                {
                    GovtCharges +=  parseInt(eRegistration.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
                }
                else
                {
                    GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
                    nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt + plcAmountArea ) * (parseInt(eRegistration.value)/100) );
                }
                
                //alert('Final Offer Price ='+nOfferPriceAmt);
                

                
                        
                document.getElementById('id_calprice').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits: 2});
                
				nOfferPriceAmt = Math.abs(nOfferPriceAmt)
            if (nOfferPriceAmt >= 10000000)
            {
                formattedNumber = (nOfferPriceAmt / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (nOfferPriceAmt >= 100000)
            {
                formattedNumber = (nOfferPriceAmt / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (nOfferPriceAmt >= 1000)
            {
                formattedNumber = (nOfferPriceAmt / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = nOfferPriceAmt;
            }
				
				document.getElementById('id_calprice_words').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+formattedNumber;
				
                
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
                                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
                                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
                                                               'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
                                                               'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

                $('#container').highcharts({
                 
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Property Charges'
                        },
                        tooltip: {
                            //pointFormat: '{series.name}: <b>{point.value}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [{
                            data: [
                                ['Base Charge', eval(BaseCharges.toFixed(2)) ],
                                ['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
                                ['Government Charges', eval( GovtCharges.toFixed(2) )],
                                ['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
                                ['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))   ]
                            ]
                            
                        }]
                }); 
                
                //$('.place-desc-large').find(':first-child').first().text('new text');
                //$('.place-desc-large DIV:first-child').attr('html', 'abc');
                //$('.place-name' ).html( 'background-color');
                //$('.place-desc-large div.place-name').text('MY NEW TEXT');
            }
            
            //ReCalculateOfferPrice();
            
        </script>

    ";

    ?>
	
	
	
	<?php

echo "
    
        <script>
        
            var BaseCharges = 0;
            var ServiceTaxCharges = 0;
            var GovtCharges = 0;
            var ProjectCharges = 0;

            function ReCalculateOfferPrice9(elem)
            {
                
                var BaseCharges = 0;
                var GovtCharges = 0;
                
                tBaseRate = parseInt(document.getElementById('bsp_price_value').value);
				tBaseRateUnit = parseInt(document.getElementById('bsp_price_value_unit').value);
				
				tPlcRate = parseInt(document.getElementById('bsp_price_value_plc').value);
				
				tPlcRateUnit = parseInt(document.getElementById('plc_charges_unit').value);
				
				
				//alert(tBaseRate);
				//alert(tPlcRate);
				
				//tRegCheck = 
				
                //alert(tPlcRate);
                
            
                //tBaseRate = " . $gBaseRate . ";
                tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
                
                tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
                
                eStampDuty = document.getElementById('id_gaStampDuty');
                //eRegistration = document.getElementById('id_gaRegistration');
				
				//eRegistration = document.getElementById('regCheckValue');
				
				
				
				if (elem)
				{	
					// Change in optional element
					
					if ( true == elem.checked )
					{	
						//alert('Checked  - '+elem.value);
						eRegistration = parseInt(elem.value);
						document.getElementById('regCheckValue').value = eRegistration;
						
					}	
					else
					{	
						eRegistration = 0;
						document.getElementById('regCheckValue').value = eRegistration;
					}
				}
				else
                {
					// Change in choice of floor
					
					eRegistration = 0;
                }
				
				//alert(eRegistration);
                
                tBSPForFloor = " . $gafplc . ";
                tBSPForFloorCharges = " . $gafplc1 . ";
                tBSPForFloorOption = " . $gafplc2 . ";
				
				gstInputCredit = ".$gstCreditInput.";
				//alert(gstInputCredit);
				gstInputCreditAmount = (tBaseRate *".$gSuperBuiltUpArea.") * gstInputCredit / 100;
				//alert(gstInputCreditAmount);
	
	            //tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
    
                //tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
                //alert(tOfferPriceAmt);
				
				if ( tBaseRateUnit == 1 ){
					tOfferPriceAmt = (tBaseRate *".$gSuperBuiltUpArea.") - gstInputCreditAmount ;
				}
				if ( tBaseRateUnit == 2 ){
					tOfferPriceAmt = tBaseRate - gstInputCreditAmount ;
				}
                    
                tFloorPLC = 0;
                
                if ( tBSPForFloorCharges > 0 )
                {   
                    tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
                    
                    if ( tBSPForFloor == 2 )
                    {
                        // BSP w.r.t Ground floor
                        tempFloor = parseInt(document.getElementById('id_floor_choice').value) ; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                        
                    }
                    else
                    {
                        // BSP w.r.t TOP floor;
                        tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice').value) + 1; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                    }           
                    
                    if ( tBSPForFloorOption == 2 )
                    {
                        // sub option
                        tFloorPLC = tFloorPLC*(-1);
                    }   

                }
                
                nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
                BaseCharges += nOfferPriceAmt;
                
                ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
                nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
                //alert(nOfferPriceAmt);
                
                    // Change in choice of floor
                    
                    tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                //} 
				//alert(tServiceTaxOnOTH);
				
				
				
				
				if ( tPlcRateUnit == 1 ){
					plcAmountArea = ".$gSuperBuiltUpArea."*tPlcRate;
					plcAmountGst = (".$gSuperBuiltUpArea."*tPlcRate)*tServiceTaxOnOTH/100;
				}
				
				if ( tPlcRateUnit == 2 ){
					plcAmountArea = tPlcRate;
					plcAmountGst = (tPlcRate*tServiceTaxOnOTH)/100;
				}
				
				plcAmountTotal = plcAmountArea + plcAmountGst;
				//alert(plcAmountTotal);

                nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
                nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt + plcAmountTotal ;     
                //alert(nOfferPriceAmt);
                if ( 'amount' == eStampDuty.name )
                {
                    GovtCharges += parseInt(eStampDuty.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
                }
                else
                {
                    GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
                    nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
                }       
                
                if ( 'amount' == eRegistration.name )
                {
                    GovtCharges +=  parseInt(eRegistration.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
                }
                else
                {
                    GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration)/100);
                    nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt + plcAmountArea ) * (parseInt(eRegistration)/100) );
                }
                
                //alert('Final Offer Price ='+nOfferPriceAmt);
                

                
                        
                document.getElementById('id_calprice').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits: 2});
                
				nOfferPriceAmt = Math.abs(nOfferPriceAmt)
            if (nOfferPriceAmt >= 10000000)
            {
                formattedNumber = (nOfferPriceAmt / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (nOfferPriceAmt >= 100000)
            {
                formattedNumber = (nOfferPriceAmt / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (nOfferPriceAmt >= 1000)
            {
                formattedNumber = (nOfferPriceAmt / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = nOfferPriceAmt;
            }
				
				document.getElementById('id_calprice_words').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+formattedNumber;
				
                
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
                                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
                                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
                                                               'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
                                                               'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

                $('#container').highcharts({
                 
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Property Charges'
                        },
                        tooltip: {
                            //pointFormat: '{series.name}: <b>{point.value}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [{
                            data: [
                                ['Base Charge', eval(BaseCharges.toFixed(2)) ],
                                ['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
                                ['Government Charges', eval( GovtCharges.toFixed(2) )],
                                ['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
                                ['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))   ]
                            ]
                            
                        }]
                }); 
                
                //$('.place-desc-large').find(':first-child').first().text('new text');
                //$('.place-desc-large DIV:first-child').attr('html', 'abc');
                //$('.place-name' ).html( 'background-color');
                //$('.place-desc-large div.place-name').text('MY NEW TEXT');
            }
            
            //ReCalculateOfferPrice();
            
        </script>

    ";

    ?>
	
	
	
	