<section class="pap">
    <div class="space-x"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Privacy & Policy</h1>
                
				<h3>Fair Pockets Privacy Policy Terms</h3>
				<p>Fair Pockets ("Fair Pockets" or "We" or "Us") follows these privacy terms in order to demonstrate its commitment to customer privacy. Privacy on Fair Pockets Internet websites, applications (apps) or other product Services (collectively, the "Services") is of great importance to us. Because we and our third party providers may gather important information via the Website or the Apps from our visitors or customers (collectively "Customer" or "You" or "Your"), we have established these privacy terms as a means to communicate our information gathering and dissemination practices. We may modify these privacy terms at any time without notice to you by posting its revised version on our sites.</p>
				<h3>Collected information</h3>
				<p>We or our third party providers, may require customers to provide information, who register to use or purchase Fair Pockets Services (including, but not limited to, end-user licenses to use our intellectual property) from Fair Pockets website or within Fair Pockets Services, to be on our e-mail list, or generally to use our products, including, but not limited to, our Services to give us or our third party providers.</p>
				<p>The information collected about customers may be divided into various categories, such as:-</p>
				<ul class="DescriptMain">
						<li>Information provided directly, including, but not limited to, Contact information, user credentials, demographic information, public profile data, communication preferences, payment and identity information, correspondence, questions, search queries, etc.</li>
						<li>Information collected by Fair Pockets, including, but not limited to, your browser type, operating system, IP address, web pages visited, advertisements, cookies, bandwidth information, logs, etc.</li>
						<li>Information collected from other sources, including, but not limited to, contact information, third party services usage, purchase behavior, navigation data, or any data that can help us in reading spending or interest patterns, etc.</li>
				</ul>
				<p>You can opt out of providing said information by not entering it when asked or not visiting the website or using the Fair Pockets services, although not providing it may hinder your ability to use the Fair Pockets services. You can opt out of any correspondence from us by following the applicable opt out or unsubscribe procedures listed on any correspondence, but you should be aware that archival or back-up copies of said personal information will not cease to exist and your personal information may still be included in the aggregate and in the Fair Pockets history files. Further, we are under no obligation to remove said information or make any edits to your personal information and shall not be held liable for failing to do so nor for any of our services failure to do so.</p>
				<h3>Use of Information</h3>
				<p>We may use the information that we collect to enhance the services for you, including but not limited to, improving the marketing and promotional efforts, analyzing site usage, improving the Site's content and product offerings, customizing the Site's content, layout and services etc. Fair Pockets or any of its affiliates, advertisers, promoters, third party providers (collectively “Associates”) may also use the information to contact you to further discuss your interest in Fair Pockets or its Services, the website, any of our Associates’ Services, or in any way that we, in our sole discretion, deem to be reasonable. Your email address and any personal information may be distributed or shared with third parties to transact such business as you have contracted us to do, to comply with any legal processes and/or law enforcement requests, or in order to conduct business as we, in our sole discretion, deem reasonable.</p>
				<p>Except as we explicitly state at the time we request information, or as provided for herein or in the <a href="/terms-conditions">Fair Pockets Terms of Service</a>, we do not disclose to third parties the information provided. Any billing information you provide us is not used by us for marketing or promotional purposes, except as provided for herein.</p>
				<h3>Protection of Information</h3>
				<p>We are committed to protecting your information. We have adopted commercially reasonable technical, administrative, and physical procedures to help protect your information from loss, misuse, and alteration. However, we cannot guarantee to be 100% secure against any virus, Trojans, hacking attempts or events beyond our control or reasonable apprehension. </p>
				<h3>Third-party websites</h3>
				<p>The website and the services contain links to other websites. We are not responsible for the privacy practices or the content of these other websites. You will need to check the policy statement of these others websites to understand their policies. When you access a linked site you may be disclosing your personal information. It is your responsibility to keep such information private and confidential.</p>
            </div>
			
        </div>
		<div class="space-l"></div>
    </div>
</section>
