<section class="aboutus">
    <div class="space-l"></div>
    <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">


        <div class="row">
            <div class="col-md-12">
                <h1>About Us</h1>
                <hr>
                <p>As a home buyer the two most important thing is to get fair price and associate with only fair or trusted builders. Your hard earned money should go in the safest possible hand and also you should not pay a penny more than fair price for your property transaction.</p>

                <p>Fairpockets is a concept which has originated to not only provide fair priced properties but also allow only fair or trusted builders to list their properties on our website.</p>

                <p>In the current market anybody can list or advertise properties at any price which they want. This brings confusion to both buyers and sellers and they are not able to ascertain what price to ask as a seller or what price to pay as a buyer.</p>

                <p>Our idea is very simple whether you are a buyer or a seller, the first thing you should know is what would be the price of the property in discussion. We ascertain market value of the property through our desktop valuation method which gives you the first discussion point.</p>

                <p>We have also come up with a theoretical concept of “Fair Price” which is a price between market price and lowest price for a particular property, assuming both buyer and seller are aware of the market.</p>

                <p>The reason why its below market price because if a buyer is aware of the market prices then it will definitely not like to pay beyond the market price as there are enough options available for him. The scenario is though different for a seller, he can sell below the market price because of urgent need for money or he has made enough gains in his property or any other reason. For the seller the money in hand is more important.</p>

                <p>Another important aspect at Fairpockets is that we only allow fair or trusted developers to use our platform. We do a small check on developers before we accept their request to post property on our Fairpockets. We don’t charge any listing fee from a developer which helps us in filtering out the best from the rest.</p>

                <p>Whether you are a buyer or a seller we provide you the most fair platform to fulfill your property needs.</p>
            </div> 
        </div>

    </div>
    <div class="space-l"></div>
</section>

