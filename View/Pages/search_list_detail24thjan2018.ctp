<?php
echo $this->Html->script('front/jssor.slider-22.2.10.min');

//$all_property_features = json_decode("{".$propertydetails['Property']['features']."}", true);
//$amenities = json_decode($propertydetails['Property']['amenities'], true);

//echo '<pre>';print_r($property_features_unit);


?>
<script>
    jQuery(document).ready(function () {
        function nFormatter(num)
        {
            num = Math.abs(num)
            if (num >= 10000000)
            {
                formattedNumber = (num / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (num >= 100000)
            {
                formattedNumber = (num / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (num >= 1000)
            {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = num;
            }

            return formattedNumber;
        }

<?php
if ($propertydetails['Property']['transaction_type_id'] == '1') {
    ?>
            format = nFormatter('<?php echo $propertydetails['Property']['offer_price']; ?>');
            document.getElementById("offerpriceformater").innerHTML = format;
            mformat = nFormatter('<?php echo $propertydetails['Property']['market_price']; ?>');
            document.getElementById("marketpriceformater").innerHTML = mformat;
    <?php
} else {
    ?>
            rformat = nFormatter('<?php echo $propertydetails['Property']['market_rent']; ?>');

            document.getElementById("marketrentformater").innerHTML = rformat;

            eformat = nFormatter('<?php echo $propertydetails['Property']['expected_monthly']; ?>');

            document.getElementById("expectedmonthlyformat").innerHTML = eformat;
<?php } ?>

    });

</script>        

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate()
    });
</script>               
<style>
    .detail_header .h_feature 
    {
        font-size: 20px;
        color: #29333d;
    }
    .green{color:green; font-weight:bold;border:1px solid; padding:7px;}
    .error{color:red;}
</style>
<style>
    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    .jssora05l.jssora05lds      (disabled)
    .jssora05r.jssora05rds      (disabled)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('img/a17.png') no-repeat;
        overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }
    .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
    .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
</style>


<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

<section class="propert-detail">
    <div class="property_detail_header">
        <div class="container">
            
            <div class="detail_header">
                <div class="row">
                    <!--<div class="col-sm-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/searchList?default_location=Noida&amp;property_for=buy&amp;property_name=Property in Central Noida ">Property in Central Noida&nbsp;</a> </li>
                            <li>Noida</li>
                        </ul>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="col-sm-5 header-price">
                        <!--<div class="offer_price">
                            <i class="fa fa-inr" aria-hidden="true"></i>65 Lac                                                          <span class="prooffer">
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i>10% less
                                </span>
                                                        <span class="price_sq">@ 5200 per Sq.Ft</span>
                        </div>
                        <div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i>71.88 Lac @5750 per Sq.Ft</div>-->

                        <?php
                       //echo '<pre>'; print_r($propertydetails);
                    if ($propertydetails['Property']['transaction_type_id'] == '1') {
                        ?>  
                        <div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i> <span id="offerpriceformater"></span><span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo round(($propertydetails['Property']['market_price'] - $propertydetails['Property']['offer_price']) / $propertydetails['Property']['market_price'] * 100, 0); ?>% less</span><span class="price_sq">@<?php
                                /*if (!empty($all_property_features['superbuilt_area_insq'])) {
                                    $insq = $all_property_features['superbuilt_area_insq'];
                                } else {
                                    $insq = $all_property_features['plot_area_insq'];
                                }*/
                                //echo round($propertydetails['Property']['offer_price'] / $insq, 3);
								if(isset($all_property_features['1']) && $all_property_features['1'] !=''){
                                echo round($propertydetails['Property']['offer_price']/$all_property_features['1'],3);
								}else{
								echo round($propertydetails['Property']['offer_price']/$all_property_features['20'],3);	
								}
                                ?> per Sq.Ft</span></div>
                        <div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i><span id="marketpriceformater"></span> @<?php
                            /*if (!empty($all_property_features['superbuilt_area_insq'])) {
                                $insq = $all_property_features['superbuilt_area_insq'];
                            } else {
                                $insq = $all_property_features['plot_area_insq'];
                            }*/
                            //echo round($propertydetails['Property']['market_price'] / $insq, 3);
                            //echo $propertydetails['Property']['market_price_sqt'];
							if(isset($all_property_features['1']) && $all_property_features['1'] !=''){
                                echo round($propertydetails['Property']['market_price']/$all_property_features['1'],3);
								}else{
								echo round($propertydetails['Property']['market_price']/$all_property_features['20'],3);	
								}
                            ?> per Sq.Ft</div>
                            <?php
                        } else {
                            ?>  
                        <div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i> <span id="expectedmonthlyformat"></span><span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo round(($propertydetails['Property']['market_rent'] - $propertydetails['Property']['expected_monthly']) / $propertydetails['Property']['market_rent'] * 100, 0); ?>% less</span></div>
                        <div class="market_price"><span>Market Rent:</span><i class="fa fa-inr" aria-hidden="true"></i><span id="marketrentformater"></span>
                        </div>
                        <?php
                    }
                    ?>


                    </div>
                    <div class="col-sm-4 text-center header-feature">
                        
                        <div class="h_feature">
						<?php if(isset($all_property_features[34]) && $all_property_features[34]!==''){ echo $all_property_features[34]; } ?>
						<span class="h_feature_sub">
						<?php if($propertyoption[0]['MstPropertyTypeOption']['transaction_type_id'] == 1){
							echo 'Residential';
							}else{
							echo 'Commercial';
							};
						?>
						<?php echo $propertyoption[0]['MstPropertyTypeOption']['option_name']; ?>
						for Sell</span></div>
                        <div class="h_location">in <?php echo $propertydetails['Property']['locality'].
                        ','. $propertydetails['Property']['city_data'].','.$propertydetails['Property']['state_data'].
                        ','.$propertydetails['Property']['country_data']; ?></div>
                    </div>
                    <div class="col-sm-3 text-center header-other-feature">                        
                        <div class="h_postdate">Post on <?php echo date("m/d/Y", strtotime($propertydetails['Property']['created_date'])); ?></div>
                        <div class="h_availability">
						<?php if($propertydetails['Property']['construction_status'] == 1){
							echo 'Ready To Move';
							}else if($propertydetails['Property']['construction_status'] == 2){
							echo 'Under Construction';
							}else{
							echo 'Under Construction';
							}
						?>
						</div>                    </div>
                </div>
            </div>
            <?php //endforeach;?>
        </div>
    </div>
    <div class=" property_detail_area">
        <div class="space-m"></div>
        <div class="container">
            <div class=" property_detail_inners">
                
                <div class="row">   
                    <div class="col-md-9 col-sm-8 col-xs-12" id="page-content">
						<?php if(isset($propertyImages) && !empty($propertyImages)){ ?>
							<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
									<!-- Loading Screen -->
									<div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
										<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
										<div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
									</div>
									<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
										<a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
										
											<?php foreach ($propertyImages as $property_value) { ?>
												<div>
													<img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
													<img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
												</div>
											<?php } ?>
										
									</div>
									<!-- Thumbnail Navigator -->
									<div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
										<!-- Thumbnail Item Skin Begin -->
										<div data-u="slides" style="cursor: default;">
											<div data-u="prototype" class="p">
												<div class="w">
													<div data-u="thumbnailtemplate" class="t"></div>
												</div>
												<div class="c"></div>
											</div>
										</div>
										<!-- Thumbnail Item Skin End -->
									</div>
									<!-- Arrow Navigator -->
									<span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
									<span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
							</div>
						<?php }else{ ?>
						<div class="fotorama" data-nav="thumbs" data-width="100%" data-thumbheight="100" data-thumbwidth="100">
							<?= $this->Html->image('property/01.jpg')?>
							<?= $this->Html->image('property/02.jpg')?>
							<?= $this->Html->image('property/03.jpg')?>
							<?= $this->Html->image('property/04.jpg')?>
							<?= $this->Html->image('property/05.jpg')?>
							<?= $this->Html->image('property/06.jpg')?>
							<?= $this->Html->image('property/07.jpg')?>
							<?= $this->Html->image('property/08.jpg')?>
							<?= $this->Html->image('property/09.jpg')?>
							<?= $this->Html->image('property/10.jpg')?>
							<?= $this->Html->image('property/11.jpg')?>
							<?= $this->Html->image('property/12.jpg')?>
						</div>
						<?php } ?>
						
                        <div class="space-m"></div>     
                        <div id="detail" class="detail-list">
                            <div class="detail-title">
                                <h2 class="title-left">Detail</h2> <?php //print_r($all_property_features); ?>
                            </div>
                            <ul class="list-two-col">
                                
                                <?php
                                    if (!empty($all_property_features['1'])) {
                                        ?>
                                        <li><strong>Super Built Up Area:</strong> <?php echo $all_property_features['1']; ?> <?php echo $all_property_features_unit[1]; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['2'])) {
                                        ?>
                                        <li><strong>Built Up Area:</strong> <?php echo $all_property_features['2']; ?> <?php echo $all_property_features_unit[2]; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['3'])) {
                                        ?>
                                        <li><strong>Carpet Area:</strong> <?php echo $all_property_features['3']; ?> <?php echo $all_property_features_unit[3]; ?></li>
                                    <?php } ?>

                                    <?php if (!empty($all_property_features['20'])) {
                                        ?>
                                        <li><strong>Plot Area:</strong> <?php echo $all_property_features['20']; ?> <?php echo $all_property_features_unit[20]; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['21'])) {
                                        ?>
                                        <li><strong>Length of plot:</strong> <?php echo $all_property_features['21']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['22'])) {
                                        ?>
                                        <li><strong>Width of plot:</strong> <?php echo $all_property_features['22']; ?> Sq.ft</li>
                                    <?php } ?>

                                    <?php if (!empty($all_property_features['4'])) {
                                        ?>
                                        <li><strong>Bedrooms:</strong> <?php echo $all_property_features['4']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['5'])) {
                                        ?>
                                        <li><strong>Bathrooms:</strong> <?php echo $all_property_features['5']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['33'])) {
                                        ?>
                                        <li><strong>Washrooms:</strong> <?php echo $all_property_features['33']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['6'])) {
                                        ?>
                                        <li><strong>Balconies:</strong> <?php echo $all_property_features['6']; ?></li>
                                    <?php } ?>
                                
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="space-m"></div> 
                        <div id="addtional-detail">
                            <div class="detail-title">
                                <h2 class="title-left">Additional details</h2>
                            </div>
                            
                            <ul class="list-two-col list_odetail">
                                <?php if (!empty($project['Project']['project_name'])) {
                                    ?>
                                    <li><strong>Project name:</strong> <span><?php echo $project['Project']['project_name']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($project['Project']['dtp1'])) {
                                    ?>
                                    <li><strong>Launch date:</strong> <span><?php echo $project['Project']['dtp1']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['offer_price'])) {
                                    ?>
                                    <li><strong>Offer Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['offer_price']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['market_price'])) {
                                    ?>
                                    <li><strong>Market Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_price']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['market_rent'])) {
                                    ?>
                                    <li><strong>Market Rent:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_rent']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['expected_monthly'])) {
                                    ?>
                                    <li><strong>Expected Monthly Rent:</strong> <span><?php echo $propertydetails['Property']['expected_monthly']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['deposit'])) {
                                    ?>
                                    <li><strong>Deposit Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['deposit']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['booking_amount'])) {
                                    ?>
                                    <li><strong>Booking Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['booking_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['maintance_amount'])) {
                                    ?>
                                    <li><strong>Maintance Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['maintance_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($project['Project']['dtp2'])) {
                                    ?>
                                    <li><strong>Possession Date:</strong> <span><?php echo $project['Project']['dtp2']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['ownership_type_id'])) {
                                    ?>
                                    <li><strong>Ownership Type:</strong> <span><?php
                                            if ($propertydetails['Property']['ownership_type_id'] == '1') {
                                                echo "FreeHold";
                                            }if ($propertydetails['Property']['ownership_type_id'] == '2') {
                                                echo "Lease Hold";
                                            }if ($propertydetails['Property']['ownership_type_id'] == '3') {
                                                echo "Power of Attorney";
                                            }if ($propertydetails['Property']['ownership_type_id'] == '4') {
                                                echo "Co-operative Society";
                                            }
                                            ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['sale_type'])) {
                                    ?>
                                    <li><strong>Sale Type:</strong> <span><?php
                                            if ($propertydetails['Property']['sale_type'] == '1') {
                                                echo "Resale";
                                            } else {
                                                echo "New Booking";
                                            }
                                            ?></span></li>
                                <?php } ?>

                                <?php
                                if (!empty($all_property_features['10'])) {
                                    ?>
                                    <li><strong>Property on Floor :</strong> <span><?php echo $all_property_features['10']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['30'])) {
                                    ?>
                                    <li><strong>Floors in property:</strong> <span><?php echo $all_property_features['30']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['locality'])) {
                                    ?>
                                    <li><strong>Address:</strong> <span><?php echo $propertydetails['Property']['locality']; ?>,<?php echo $propertydetails['Property']['city_data']; ?>,<?php echo $propertydetails['Property']['state_data']; ?>,<?php echo $propertydetails['Property']['country_data']; ?>,<?php echo $propertydetails['Property']['pincode']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['7'])) {
                                    ?>
                                    <li><strong>Age of property:</strong> <span><?php echo $all_property_features['7']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['8'])) {
                                    ?>
                                    <li><strong>Furnishing:</strong> <span><?php echo $all_property_features['8']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['23'])) {
                                    ?>
                                    <li><strong>Floor Allowed For Construction:</strong> <span><?php echo $all_property_features['23']; ?></span></li>
                                <?php } ?>
                                <?php
                                if (!empty($all_property_features['11'])) {
                                    ?>
                                    <li><strong>Reserved Parking:</strong> <span>None</span></li>
                                    <?php
                                } else {
                                    ?>
                                    <?php if (!empty($all_property_features['45'])) {
                                        ?>
                                        <li><strong>No of Covered Parking:</strong> <span><?php echo $all_property_features['37']; ?></span></li>
                                    <?php } ?>
                                    <?php if (!empty($all_property_features['46'])) {
                                        ?>
                                        <li><strong>No of Open Parking:</strong> <span><?php echo $all_property_features['38']; ?></span></li>
                                        <?php
                                    }
                                }
                                ?>
                                <?php
                                $otherroom = '';
                                if (!empty($all_property_features['12']) && $all_property_features['12'] == '1') {
                                    $otherroom .= ' Pooja,';
                                }
                                if (!empty($all_property_features['39']) && $all_property_features['39'] == '1') {
                                    $otherroom .= ' Study,';
                                }
                                if (!empty($all_property_features['40']) && $all_property_features['40'] == '1') {
                                    $otherroom .= ' Servant,';
                                }
                                if (!empty($all_property_features['41']) && $all_property_features['41'] == '1') {
                                    $otherroom .= ' Other,';
                                }
                                ?>
                                <?php
                                if (!empty($otherroom)) {
                                    ?>
                                    <li><strong>Other Rooms:</strong> <span><?php echo rtrim($otherroom, ','); ?></span></li>
                                <?php } ?>
                            </ul>


                        </div>
                        <div class="clearfix"></div>
                        <div class="space-m"></div> 
                        <!-- basic detail end
                            <div id="description" class="detail-features">
                            <div class="detail-title">
                            <h2 class="title-left">Description</h2>
                            </div>
                            <div class="property_des">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="space-m"></div> 
                        -->
                        <div id="features">
                            <div class="detail-title">
                                <h2 class="title-left">Features</h2>
                            </div>
                            
                            <ul class="list-two-col list-features">
                                <?php if (!empty($all_property_features['31'])) {
                                    ?>
                                    <li><strong>Type of Flooring:</strong> <?php echo $all_property_features['31']; ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['32'])) {
                                    ?>
                                    <li><strong>Power Backup:</strong> <?php echo $all_property_features['32']; ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['18']) || !empty($all_property_features['36'])) {
                                    ?>
                                    <li><strong>Water Source:</strong> <?php
                                        $source = '';
                                        if (!empty($all_property_features['18'])) {
                                            $source .= "Municipal,";
                                        } if (!empty($all_property_features['36'])) {
                                            $source .= " Borewell Tank ";
                                        }
                                        ?><?php echo rtrim($source, ','); ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['facing'])) {
                                    ?>
                                    <li><strong>Facing:</strong> <?php echo $all_property_features['28']; ?></li>
                                <?php } ?>

                                <?php if (!empty($all_property_features['13'])) {
                                    ?>
                                    <li><strong>Width of facing road:</strong> <?php echo $all_property_features['13']; ?> <?php echo $all_property_features_unit[13]; ?></li>
                                <?php } ?>
                                <?php
                                if (!empty($all_property_features['14']) || !empty($all_property_features['42']) || !empty($all_property_features['43']) || !empty($all_property_features['44'])) {
                                    $overlook = '';

                                    if (!empty($all_property_features['14'])) {
                                        $overlook .= "Park/Garden, ";
                                    } if (!empty($all_property_features['42'])) {
                                        $overlook .= "Main Road, ";
                                    }if (!empty($all_property_features['43'])) {
                                        $overlook .= "Club, ";
                                    }if (!empty($all_property_features['44'])) {
                                        $overlook .= "Pool, ";
                                    }
                                    ?>
                                    <li><strong>Overlooking:</strong> <?php echo rtrim($overlook, ','); ?></li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['15'])) {
                                    ?>
                                    <li><strong>Bachelors Allowed:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['16'])) {
                                    ?>
                                    <li><strong>Pet allowed:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['17'])) {
                                    ?>
                                    <li><strong>Non vegetarian:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['25'])) {
                                    ?>
                                    <li><strong>Boundary wall:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($all_property_features['26'])) {
                                    ?>
                                    <li><strong>Society Type:</strong> <span><?php echo $all_property_features['26']; ?></span></li>
                                <?php } ?>
                            </ul>


                        </div>
                        <div class="clearfix"></div>
                        <div class="space-m"></div> 
                        <!-- Amenities -->
                        <div id="amenitie" class="detail-amenities">
                            <div class="detail-title">
                                <h2 class="title-left">Amenities</h2>
                            </div>
                            <div class="row">
                                <?php 
                                    /*$amenities = explode(',', $item->amenities[0]);
                                    $icon_array = array(
                                    'Bank Attached Property'        => 'lift',
                                    'Centrally Air Conditioned'     => 'airc',
                                    'Club House/Community'          => 'com_hall',
                                    'Feng Shui/Vasstu Compliant'    => 'vaastu',
                                    'Fitness Centre/GYM'            => 'gym',
                                    'Intercom Facility'             => 'intercomm',
                                    'Internet/wi-fi connectivity'   => 'internet',
                                    'Lift(S)'                       => 'lift',
                                    'Maintenance Staff'             => 'staff',
                                    'Park'                          => 'park"',
                                    'Piped-Gas'                     => 'waste',
                                    'Piped-gas'                     => 'waste',
                                    'Rain Water Harvesting'         => 'rainwater',
                                    'Security Personnel'            => 'securityp',
                                    'Security/Fire Alarm'           => 'security',
                                    'Shopping /Center'              => 'shopping',
                                    'Swimming Pool'                 => 'pool',
                                    'Visitor Parking'               => 'parking',
                                    'Water Purifier'                => 'water_plant',
                                    'Water storage'                 => 'water_storage'
                                    );*/
                                ?>
                                <div class="col-md-12">
                                    <ul class=" list-amenities">
                                        
                                        <div class="row">
                                    <div class ="col-md-3">
                                        <?php
                                       // print_r($all_property_amenities);
                                        $count = 0;
                                        if(isset($all_property_amenities) && !empty($all_property_amenities))
                                        {
                                        //foreach ($amenities as $key => $amevalu) {
                                            ?>

                                            <!--<div class= "customDiv">column1</div>-->
                                            <?php
                                            if (isset($all_property_amenities[6]) && $all_property_amenities[6] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_vaastu">Feng shui/Vaastu compliant</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            
                                            ?>

                                            <?php
                                            if (isset($all_property_amenities[1]) && $all_property_amenities[1] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_lift">Lift</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[2]) && $all_property_amenities[2] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_park">Park</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[3]) && $all_property_amenities[3] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_staff">Maintenance Staff</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[4]) && $all_property_amenities[4] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_parking">Visitor Parking</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[5]) && $all_property_amenities[5] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_water_storage">Water Storage</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>



                                            <!-- <div class= "customDiv">column2</div>-->

                                            <?php
                                            if (isset($all_property_amenities[7]) && $all_property_amenities[7] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_intercomm">Intercomm Facility</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>

                                            <?php
                                            if (isset($all_property_amenities[16]) && $all_property_amenities[16] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_waste">Waste Disposal</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[17]) && $all_property_amenities[17] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_tarrace">Private Garden/Terrace</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[18]) && $all_property_amenities[18] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_securityp">Security Personnel</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[21]) && $all_property_amenities[21] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_lift">Service/Goods Lift</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>


                                            <!-- <div class= "customDiv">column3</div>-->
                                            <?php
                                            if (isset($all_property_amenities[22]) && $all_property_amenities[22] == 'Yes') {
                                                ?>   
                                                <li><span class="icon_amen icon_conroom">Conference Room</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[19]) && $all_property_amenities[19] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_pool">Swimming Pool</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[20]) && $all_property_amenities[20] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_atm">ATM</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[23]) && $all_property_amenities[23] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_airc">Centrally Airconditioned</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[24]) && $all_property_amenities[24] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_internet">High Speed Internet</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[25]) && $all_property_amenities[25] == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_foodc">Cafeteria/Food court</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>


                                            <!-- <div class= "customDiv">column4</div>-->

                                            <?php
                                            /*if ($all_property_amenities == '1') {
                                                ?>
                                                <li><span class="icon_amen icon_gym">Gymnasium</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if ($all_property_amenities == '1') {
                                                ?>  
                                                <li><span class="icon_amen icon_water_plant">Water Softening Plant</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if ($all_property_amenities == '1') {
                                                ?>
                                                <li><span class="icon_amen icon_com_hall">Community Hall</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                            <?php
                                            if ($all_property_amenities == '1') {
                                                ?>
                                                <li><span class="icon_amen icon_shopping">Shopping Center</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }*/
                                            ?>
                                            <?php
                                            if (isset($all_property_amenities[15]) && $all_property_amenities == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_rainwater">Rain water harvesting</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>

                                            <?php
                                            if (isset($all_property_amenities[8]) && $all_property_amenities == 'Yes') {
                                                ?>
                                                <li><span class="icon_amen icon_security">Security/Fire alarm</span></li>
                                                <?php
                                                echo "</div>";
                                                echo '<div class ="col-md-3">';
                                            }
                                            ?>
                                        <?php }else{ ?>
											<p> Coming Soon </p>
										<?php } ?>

                                    </div>
                                </div>


                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>  
                    <!-- sidebar -->
					
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div id="contact-biulder">					
							<div class="sidebar-inner">
							<div class="text-center" id="id_contact_succ" style="display:none;"></div>
							<div id="id_contact_form">
								<form class="fpform" id="contact-form" method="post">
									<div class="row">
										<h3>Contact Owner</h3>
										<div class="message"></div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input id="user1" class="radio-custom" name="user_type" type="radio" value="individual">
												<label for="user1" class="radio-custom-label">Individual</label>
												<input id="user2" class="radio-custom" name="user_type" type="radio" value="dealer">
												<label for="user2" class="radio-custom-label">Dealer</label>
											</div>
										</div>
										<input class="form-control" name="propertyid" type="hidden" value="<?php echo $propertydetails['Property']['property_id']; ?>">
										<input class="form-control" name="userid" type="hidden" value="<?php echo $propertydetails['Property']['user_id']; ?>">
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input class="form-control" name="name" placeholder="Your Name" type="text">
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input class="form-control" placeholder="Phone" name="data[Websiteuser][usermobile]" id="user_mobile" type="text" maxlength="10">
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<input class="form-control" placeholder="Email" name="email" type="email">
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="form-group">
												<label class="label_title">Interested in (Optional)</label>
												<select id="interested" name="interested[]" class="selectpicker" multiple>
													<option value="assisted search">Assisted Search </option>
													<option value="immediate purchase">Immediate Purchase</option>
													<option value="portfolio management">Portfolio Management</option>
												</select>
												
											</div>	
										</div>	
										<input type="hidden" name="property_id" class="modal-propid">
										<div class="text-center col-sm-12 "><button class="btn btn-primary">Submit</button></div>
										<div class="text-center col-sm-12 " style="padding:10px 0px;">I agree with Fairpockets <a href="#">T&amp;C</a> </div>
									</div>
								</form>
							</div>	
							</div>
						</div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="space-m"></div> 
            </div>
        </div>
    </div>
</section>

<section id="similer-propertys" class="grid-view" style="display:none;">
    <div class="container">
        <div class="detail-title">
            <h2 class="title-left">Similar properties</h2>
        </div>
        <div class="owl-carousel">
            <?php
                
                foreach($similar as $similar_property){
                    foreach ($similar_property as $proprerty_list){
                        
                        
                        if($proprerty_list['transaction_type'] == 'Sell'){
                            $transaction_type = 'buy';
                            }else{
                            $transaction_type = $proprerty_list['transaction_type'];
                        }
                        
                        $property_url_address  = (!empty($proprerty_list['sublocality1'])) ? $proprerty_list['sublocality1'].'&nbsp;' : '';
                        //$url_address .= (!empty($item['city'])) ? $item['city'] : '';
                        $title = (strpos($proprerty_list['configure'], 'BHK') !== false ? $proprerty_list['configure'] : $proprerty_list['configure'] . 'BHK') .' '. $proprerty_list['property_type'] . ' for '. $proprerty_list['transaction_type'];   
                        $proprerty_address  = (!empty($proprerty_list['sublocality3'])) ? $proprerty_list['sublocality3'].',&nbsp;' : '';
                        $proprerty_address .= (!empty($proprerty_list['sublocality2'])) ? $proprerty_list['sublocality2'].',&nbsp;' : '';
                        $proprerty_address .= (!empty($proprerty_list['sublocality1'])) ? $proprerty_list['sublocality1'].',&nbsp;' : '';
                        $proprerty_address .= (!empty($proprerty_list['city'])) ? $proprerty_list['city'] : '';
                        
                        $contact = $proprerty_list['posted_by'];
                        if($contact == 'Owner') {
                            $contact = 'Contact Owner';
                            } else if($contact == 'Broker') {
                            $contact = 'Contact Dealer';
                            } else {
                            $contact = 'Contact Builder';
                        }
            ?>
            <div class="item">
                <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">
                                <div class="price hide-on-list">
                                <h3><i class="fa fa-inr" aria-hidden="true"></i><?php echo (!empty($proprerty_list['offer_price'])) ? $this->Number->format($proprerty_list['offer_price']) : '0.00';?></h3>
                                <?php if(!empty($item['discount_percent'])):?>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo ceil($proprerty_list['discount_percent']);?>%</p>
                                </div>
                                <?php endif; ?>
                                <?= $this->Html->link($this->html->image('front/property/02_434x290.jpg', array('alt'=>$title)), array("controller" => false,"action" => "/searchListDetail", base64_encode($item->id)), array('escape' => false, 'title' => $title, 'target' => '_blank'));?>
                                
                            </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For <?php echo $proprerty_list['transaction_type'];?></div>
                                    <span class="label label-danger"><?php echo $proprerty_list['sale_type'];?></span>
                                </div>
                                <h2 class="property-title">
                                <?php
                                    echo $this->Html->link($title, array("controller"=>false,"action" => "/searchListDetail", base64_encode($proprerty_list->id)), array('escapeTitle' => false, 'title' => $title, 'target' => '_blank'));
                                ?>
                                </h2>
                                <h4 class="property-location"><?php echo $proprerty_address;?></h4>
                                <div class="pro_tt"><?php echo $title;?></div>
                                
                                <div class="pro_area"><?php echo ($proprerty_list['super_builtup_area'] ? $proprerty_list['super_builtup_area'].' Sq.ft. Super Built Up Area' : $proprerty_list['builtup_area'].' Sq.ft. Built Up Area');?></div>
                            </div>
                        </div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                            </div>
                            <div class="info-row phone text-right">
                                <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <div class="item-foot-left">
                            <p><i class="fa fa-user"></i> <a href="#"><?php echo $contact; ?></a></p>
                        </div>
                        <div class="item-foot-right">
                            <p><i class="fa fa-calendar"></i> <?php echo date('d-M-Y', strtotime($proprerty_list['post_date']));?></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                
            </div>
            <?php }
             }
            ?>
        </div>
    </div>      
</section>

<?= $this->Html->script(['validation_error_messages', 'search_list', 'owl-carousel'], ['block' => 'scriptBottom']);?>   




<script type="text/javascript">
    jssor_1_slider_init = function () {

        var jssor_1_SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*you can remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>
<script type="text/javascript">jssor_1_slider_init();</script>
<?php
$gLng = $propertydetails['Property']['lng'];
$gLat = $propertydetails['Property']['lat'];
// Generate client side GMap part
$gTitle = $propertydetails['Property']['locality'] . ', ' . $propertydetails['Property']['city_data'];
echo "
        <input type='hidden' id='id_gLat' value='" . $gLat . "'>
        <input type='hidden' id='id_gLng' value='" . $gLng . "'>
        <input type='hidden' id='id_gTitle' value='" . $gTitle . "'>
    ";
?>  

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<script type="text/javascript">

    var map;
    var lat = parseFloat(document.getElementById("id_gLat").value);
    var lng = parseFloat(document.getElementById("id_gLng").value);
    var tmp = document.getElementById("id_gTitle").value;

    var infowindow = new google.maps.InfoWindow({});

    function initialize()
    {
        geocoder = new google.maps.Geocoder();

        var latlng = new google.maps.LatLng(lat, lng);

        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Property Location"
        });

        marker['infowindow'] = tmp;

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
        });

    }

    window.onload = initialize;

</script>
