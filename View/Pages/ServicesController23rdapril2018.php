<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
//App::import('Controller', 'Countries');
//$countries = new CountriesController;
//$countries->constructClasses();

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ServicesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array(
        'Property', 
        'MstAreaState', 
        'MstPropertyTypeOption', 
        'FeatureMasters', 
        'PropertyFeatures', 
        'AmentieMaster', 
        'PropertyPic', 
        'MstAreaRegion','MstAreaState','MstAreaCity', 'MstAreaLocality', 
        'PropertyInquirie', 
        'User', 
        'PropertyNotifiction', 
        'MstAreaCountry', 
        'Project', 
        'Builder', 
        'ProjectDetails', 
        'Bank', 
        'ProjectFinancer', 
        'ProjectPricing', 
        'ProjectOfficeUse', 
        'Builder', 
        'Websiteuser', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState', 
        'FeaturePropertyOptionView', 
        'AmenityPropertyOptionView', 
        'PropertyAmenity',
		'PropertyFeature', 
        'MstAmenity', 
		'MstFeature',
        'VAmenityPropertyOption', 
        'VFeaturePropertyOption',
        'FeatureAminityPropertyOptionView',
		'Lead','OtpApp','SolrData');
    public $components = array('Paginator','Propertydata');

    //$countries->constructClasses();
    /**
     * Displays a view
     *
     * @return void
     * @throws ForbiddenException When a directory traversal attempt.
     * @throws NotFoundException When the view file could not be found
     *   or MissingViewException in debug mode.
     */
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('getCalculatorByUserIdTesting', 'getCalculatorByUserId', 'getBuildersByUserId', 'salesLoginAuth', 'validateMobile', 'verifyOtp', 'getUnitsByProjectId', 'getProjectsByUserId', 'view_detail_builder','view_detail_builder1', 'editproperty', 'addproperty', 'addpropertyfeaturedetail', 'addpropertyPrice', 'addpropertyamenities', 'upload_image', 'propertysubmit', 'getpropertyoption');
    }
	
	
	public function getCalculatorByUserId(){
		
		
//print $json = json_encode($a);//die();
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = $_POST['project_id'];
			
			$this->loadModel('ProjectPricing');
			
			$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $project_id
					)
				));
				
			$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];
						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}
						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];

					}
				}
						//echo '884'; echo '<pre>'; print_r($project_charges);
						//print json_encode($project_charges);
						//echo count($project_pricing_charge) / 4;
				for ($i = 0, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if ($project_charges[$i][1] == 2) {
						$project_charges_mand[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						
					}
					if ($project_charges[$i][1] == 1) {
						$project_charges_optional[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_optional[$j]['charge_value'] = $project_charges[$i][2];						
					}
				}
				
				//$project_charges_mand_ok = json_encode(array_values($project_charges_mand));
				//$project_charges_optional_ok = json_encode(array_values($project_charges_optional));
				//print_r($project_charges_mand);
				//print_r($project_charges_optional);die();
				
				
				
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Sinking Fund";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				$additon_charges_mand = array();
				$additon_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if ($additon_charges[$i][1] == 2) {
						$additon_charges_mand[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_mand[$j]['charge_value'] = $additon_charges[$i][2];
						
					}
					if ($additon_charges[$i][1] == 1) {
						$additon_charges_optional[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_optional[$j]['charge_value'] = $additon_charges[$i][2];						
					}
				}
				
				
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				$other_charges_mand = array();
				$other_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if ($other_charges[$i][1] == 2) {
						$other_charges_mand[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_mand[$j]['charge_value'] = $other_charges[$i][2];
						
					}
					if ($other_charges[$i][1] == 1) {
						$other_charges_optional[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_optional[$j]['charge_value'] = $other_charges[$i][2];						
					}
				}
				//print_r($project_charges_mand);
				//print_r($project_charges_optional_ok);
				$mandProjectCharges = array_merge($additon_charges_mand, $project_charges_mand, $other_charges_mand);
				//echo '<pre>'; print_r($mandProjectCharges); echo '----884----';
				$optionalProjectCharges = array_merge($additon_charges_optional, $project_charges_optional, $other_charges_optional);
				//echo '<pre>'; print_r($optionalProjectCharges);die();
				
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				$gaProjectChargesLen = count($gaProjectCharges);
				//print 
				//echo '<pre>'; print_r($gaProjectCharges);//die();
				//print json_encode($gaProjectCharges); die();
				
				
				
				
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}
				
				
				
				
				// Project Pricing - Stamp Duty
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				
				
				if(!empty($gaRegistration)){
					if (2 == $gaRegistration[0][0]) {
						// Percentage
						$Registration = $registration_amount = $gaRegistration[0][1] . " %";
					} else {
						// Flat Charges
						$Registration = "Rs " . $gaRegistration[0][1];
					}
				}
				
				
				//echo $gBaseRate.'--- reg--'.$Registration; die();
				
				

				
				
				$property_area = $_POST['property_area'];
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,
												"gst_on_base"=>$gServiceTaxOnBSP,
												"gst_on_others"=>$gServiceTaxOnOTH,
												"registration_charges"=>$Registration,
												"stamp_duty"=>$gaStampDuty);
				$discount = 2;
				
				if(!isset($project_id) || empty($project_id)){
                $resp = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
                $resp = array("message"=>$message,
								"code"=>$code,
								"property_detail"=>$property_detail_array,
								"mandatory_charges"=>$mandProjectCharges,
								"optional_charges"=>$optionalProjectCharges,
								"discount"=>$discount);
				}
            
				print json_encode($resp); exit;
				
				
				
				
				

				/*
				  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
				  pr($gaProjectCharges);
				  echo "<br>"."gaStampDuty"."<br>";
				  pr($gaStampDuty);
				  echo "<br>"."gaRegistration"."<br>";
				  pr($gaRegistration);
				  echo "<br>"."gaFloorPLC"."<br>";
				  pr($gaFloorPLC);
				 */
				 
				 	


				$gaMandatoryCharges = '';
				$gaOptionalCharges = '';
				$gaOptionalCount = 0;
				$ProjectCharges = 0;
				$gaFloorPLCCharges = 0;

				if(!empty($gaFloorPLC))
				{
				if ($gaFloorPLC[0][1] > 0) {
					$gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

					if ($gaFloorPLC[0][0] == 2) {
						// BSP w.r.t Ground floor
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					} else {
						// BSP w.r.t TOP floor;
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					}

					if ($gaFloorPLC[0][2] == 2) {
						// sub option
						$gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
					}

					$gaMandatoryCharges = $gaMandatoryCharges . "
									<div class='col-sm-12'>
										<label for='' class='checkbox-custom-label'>
								";
					$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
							'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC1[0][1] . "&nbsp;/ sq ft</span>
									";
					$gaMandatoryCharges = $gaMandatoryCharges . "
										</label>  
									</div>
							";
				}
				}
				$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
				$gOfferPrice = $gOfferPrice + $BaseCharge;
				$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

				//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
				//echo "<br><br>BaseCharge - ".$BaseCharge;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				// Project Charges
				$ProjectCharges = 0;

				//echo '<pre>'; print_r($gaProjectCharges);
				
				
				

				for ($row = 0; $row < $gaProjectChargesLen; $row++) {
					//echo "<br><br>".$gaMandatoryCharges;
					if (1 == $gaProjectCharges[$row][1]) {
						// Mandatory to include the charges

						$gaMandatoryCharges = $gaMandatoryCharges . "
										<div class='col-sm-12'>
											<label for='' class='checkbox-custom-label'>
								";

						//Calculate Charges

						$MandatoryChargeAmt = $gaProjectCharges[$row][2];
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
									";
						}

						$gaMandatoryCharges = $gaMandatoryCharges . "
											</label>  
										</div>
								";

						echo "<br><br>	ProjectCharges - ".$ProjectCharges;
					} else {
						// Optional to include the charges
						$gaOptionalCount = $gaOptionalCount + 1;
						$tempid = "optional" . $gaOptionalCount;

						$gaOptionalCharges = $gaOptionalCharges . "
										<div class='col-sm-12'>
											
								";

						//Calculate Charges
						$OptionalChargeAmt = $gaProjectCharges[$row][2];
						$tempChargeAmt = 0;
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

							// Display Charge in Optional Section
							$gaOptionalCharges = $gaOptionalCharges . "
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$tempChargeAmt = $OptionalChargeAmt;

							// Display Charge in Mandatory Section
							$gaOptionalCharges = $gaOptionalCharges . "
									
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
									";
						}

						$gaOptionalCharges = $gaOptionalCharges . "
											
											</label>  
										</div>
								";
					}

					//echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
				}
				//echo '884'.'---';
				//echo $gaOptionalCharges;
				//echo '-1884'.'---';
				echo $gaMandatoryCharges;

				$gProjectCharges_a = $ProjectCharges;

				$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
				$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

				//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
				//echo "<br><br>gServiceTax - ".$gServiceTax;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				//Stamp Duty
				$StampDuty = 0;
				if(!empty($gaStampDuty)){
				if (2 == $gaStampDuty[0][0]) {
					// Percentage
					$StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
				} else {
					// Flat Charges
					$StampDuty = $gaStampDuty[0][1];
				}
				}
				else{
				$StampDuty = '';
				}
				$gOfferPrice = $gOfferPrice + $StampDuty;
				
				$Registration = 0;
				if(!empty($gaRegistration)){
				if (2 == $gaRegistration[0][0]) {
					// Percentage
					$Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
				} else {
					// Flat Charges
					$Registration = $gaRegistration[0][1];
				}
				}
				else{
				$Registration = '';
				}
				$gOfferPrice = $gOfferPrice + $Registration;
				
				echo $gaMandatoryCharges;
			
			
			
			
			
			
			
			
			
			
            $response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("status"=>"Project Id not provided","status_code"=>"0","result"=>array());
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }
		
		
		
		public function getCalculatorByUserIdTesting(){
		
		
//print $json = json_encode($a);//die();
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = $_POST['project_id'];
			
			$this->loadModel('ProjectPricing');
			
			$project_pricing = $this->ProjectPricing->find('all', array(
					'conditions' => array(
						'project_id' => $project_id
					)
				));
				
			$project_pricing = $project_pricing['0'];
				//echo '<pre>'; print_r($project_pricing); 
				/* get project charges array */
				if(!empty($project_pricing)){
				$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
				}
				else{ 
				$project_pricing_charge = array();
				}
				// ProjectCharges
				//echo '<pre>'; print_r($project_pricing_charge); 
				$project_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
						$pcharge = $project_pricing_charge['price_pcharge' . $i];
						switch ($pcharge) {
							case "1":
								$project_charges[$j]['0'] = "One Covered Car Park";
								break;
							case "2":
								$project_charges[$j]['0'] = "Double Covered Car Park";
								break;
							case "3":
								$project_charges[$j]['0'] = "Club Membership";
								break;
							case "4":
								$project_charges[$j]['0'] = "Power BackUp per KVA";
								break;
							case "5":
								$project_charges[$j]['0'] = "Interest Free Maintenance";
								break;
							case "6":
								$project_charges[$j]['0'] = "Road Facing PLC";
								break;
							case "7":
								$project_charges[$j]['0'] = "Park Facing PLC";
								break;
							default:
								$project_charges[$j]['0'] = "Corner PLC";
						}
						$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
						$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
						$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];

					}
				}
						//echo '884'; echo '<pre>'; print_r($project_charges);
						//print json_encode($project_charges);
						//echo count($project_pricing_charge) / 4;
				for ($i = 0, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
					if ($project_charges[$i][1] == 2) {
						$project_charges_mand[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_mand[$j]['charge_value'] = $project_charges[$i][2];
						
					}
					if ($project_charges[$i][1] == 1) {
						$project_charges_optional[$j]['charge_title'] = $project_charges[$i][0];
						$project_charges_optional[$j]['charge_value'] = $project_charges[$i][2];						
					}
				}
				
				//$project_charges_mand_ok = json_encode(array_values($project_charges_mand));
				//$project_charges_optional_ok = json_encode(array_values($project_charges_optional));
				//print_r($project_charges_mand);
				//print_r($project_charges_optional);die();
				
				
				
				/* get additional charges array */
				if(!empty($project_pricing)){
				$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
				}
				else{
				$project_additional = array();
				}
				$additon_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if (!empty($project_additional['price_core_amt' . $i])) {
						$acharge = $project_additional['price_core_plc' . $i];

						switch ($acharge) {
							case "1":
								$additon_charges[$j]['0'] = "Lease Rent";
								break;
							case "2":
								$additon_charges[$j]['0'] = "External Electrification Charges";
								break;
							case "3":
								$additon_charges[$j]['0'] = "External development Charges";
								break;
							case "4":
								$additon_charges[$j]['0'] = "Infrastructure development Charges";
								break;
							case "5":
								$additon_charges[$j]['0'] = "Electricity Connection Charges";
								break;
							case "6":
								$additon_charges[$j]['0'] = "Fire fighting charges";
								break;
							case "7":
								$additon_charges[$j]['0'] = "Electric Meter Charges";
								break;
							case "8":
								$additon_charges[$j]['0'] = "Gas Pipeline Charges";
								break;
							default:
								$additon_charges[$j]['0'] = "Sinking Fund";
						}

						$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
						$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
						$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
					}
				}
				$additon_charges_mand = array();
				$additon_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
					if ($additon_charges[$i][1] == 2) {
						$additon_charges_mand[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_mand[$j]['charge_value'] = $additon_charges[$i][2];
						
					}
					if ($additon_charges[$i][1] == 1) {
						$additon_charges_optional[$j]['charge_title'] = $additon_charges[$i][0];
						$additon_charges_optional[$j]['charge_value'] = $additon_charges[$i][2];						
					}
				}
				
				
				/* get other charges array */
				if(!empty($project_pricing))
				{
				$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
				}
				else{ $project_other = array();}

				$other_charges = array();
				for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if (!empty($project_other['price_other_amt' . $i])) {
						$ocharge = $project_other['price_other_plc' . $i];

						switch ($ocharge) {
							case "1":
								$other_charges[$j]['0'] = "Other Charges1";
								break;
							case "2":
								$other_charges[$j]['0'] = "Other Charges2";
								break;
							case "3":
								$other_charges[$j]['0'] = "Other Charges3";
								break;
							case "4":
								$other_charges[$j]['0'] = "Other Charges4";
								break;
							case "5":
								$other_charges[$j]['0'] = "Other Charges5";
								break;
							case "6":
								$other_charges[$j]['0'] = "Other Charges6";
								break;
							case "7":
								$other_charges[$j]['0'] = "Other Charges7";
								break;
							case "8":
								$other_charges[$j]['0'] = "Other Charges8";
								break;
							default:
								$other_charges[$j]['0'] = "Other Charges9";
						}

						$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
						$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
						$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
					}
				}
				$other_charges_mand = array();
				$other_charges_optional = array();
				for ($i = 0, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
					if ($other_charges[$i][1] == 2) {
						$other_charges_mand[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_mand[$j]['charge_value'] = $other_charges[$i][2];
						
					}
					if ($other_charges[$i][1] == 1) {
						$other_charges_optional[$j]['charge_title'] = $other_charges[$i][0];
						$other_charges_optional[$j]['charge_value'] = $other_charges[$i][2];						
					}
				}
				//print_r($project_charges_mand);
				//print_r($project_charges_optional_ok);
				$mandProjectCharges = array_merge($additon_charges_mand, $project_charges_mand, $other_charges_mand);
				//echo '<pre>'; print_r($mandProjectCharges); echo '----884----';
				$optionalProjectCharges = array_merge($additon_charges_optional, $project_charges_optional, $other_charges_optional);
				//echo '<pre>'; print_r($optionalProjectCharges);die();
				
				//echo '<pre>'; print_r($additon_charges); print_r($project_charges); print_r($other_charges); die();
				//echo '<pre>'; print_r($project_charges);die();
				//echo '<pre>'; print_r($other_charges);die();
				$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
				$gaProjectChargesLen = count($gaProjectCharges);
				//print 
				//echo '<pre>'; print_r($gaProjectCharges);//die();
				//print json_encode($gaProjectCharges); die();
				
				
				
				
				$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
				if(!empty($project_pricing)){
					if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
						$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
					}else{
						$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
					}
					$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
					$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
				}
				else
				{
					$gBaseRate = '';   // Base Rate
					$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
					$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
				}
				
				
				
				
				// Project Pricing - Stamp Duty
				if(!empty($project_pricing))
				{
				$gaStampDuty = array(
					//[ option(1-2) , amount ]

					array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
				);

				// Project Pricing - Registration
				$gaRegistration = array(
					// [ option(1-2) , amount ]
					array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
				);
				}
				else{
				$gaStampDuty = array();
				$gaRegistration = array();
				}
				
				
				if(!empty($gaRegistration)){
					if (2 == $gaRegistration[0][0]) {
						// Percentage
						$Registration = $registration_amount = $gaRegistration[0][1] . " %";
					} else {
						// Flat Charges
						$Registration = "Rs " . $gaRegistration[0][1];
					}
				}
				
				
				//echo $gBaseRate.'--- reg--'.$Registration; die();
				
				

				
				
				$property_area = $_POST['property_area'];
				$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				$property_detail_array = array("property_area"=>$property_area,
												"base_rate"=>$gBaseRate,
												"gst_on_base"=>$gServiceTaxOnBSP,
												"gst_on_others"=>$gServiceTaxOnOTH,
												"registration_charges"=>$Registration,
												"stamp_duty"=>$gaStampDuty);
				$discount = 2;
				
				if(!isset($project_id) || empty($project_id)){
                $resp = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
                $resp = array("message"=>$message,
								"code"=>$code,
								"property_detail"=>$property_detail_array,
								"mandatory_charges"=>$mandProjectCharges,
								"optional_charges"=>$optionalProjectCharges,
								"discount"=>$discount);
				}
            
				print json_encode($resp); exit;
				
				
				
				
				

				/*
				  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
				  pr($gaProjectCharges);
				  echo "<br>"."gaStampDuty"."<br>";
				  pr($gaStampDuty);
				  echo "<br>"."gaRegistration"."<br>";
				  pr($gaRegistration);
				  echo "<br>"."gaFloorPLC"."<br>";
				  pr($gaFloorPLC);
				 */
				 
				 	


				$gaMandatoryCharges = '';
				$gaOptionalCharges = '';
				$gaOptionalCount = 0;
				$ProjectCharges = 0;
				$gaFloorPLCCharges = 0;

				if(!empty($gaFloorPLC))
				{
				if ($gaFloorPLC[0][1] > 0) {
					$gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

					if ($gaFloorPLC[0][0] == 2) {
						// BSP w.r.t Ground floor
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					} else {
						// BSP w.r.t TOP floor;
						$tempFloor = 0; //0 - Ground Floor
						$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
					}

					if ($gaFloorPLC[0][2] == 2) {
						// sub option
						$gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
					}

					$gaMandatoryCharges = $gaMandatoryCharges . "
									<div class='col-sm-12'>
										<label for='' class='checkbox-custom-label'>
								";
					$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
							'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC1[0][1] . "&nbsp;/ sq ft</span>
									";
					$gaMandatoryCharges = $gaMandatoryCharges . "
										</label>  
									</div>
							";
				}
				}
				$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
				$gOfferPrice = $gOfferPrice + $BaseCharge;
				$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

				//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
				//echo "<br><br>BaseCharge - ".$BaseCharge;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				// Project Charges
				$ProjectCharges = 0;

				//echo '<pre>'; print_r($gaProjectCharges);
				
				
				

				for ($row = 0; $row < $gaProjectChargesLen; $row++) {
					//echo "<br><br>".$gaMandatoryCharges;
					if (1 == $gaProjectCharges[$row][1]) {
						// Mandatory to include the charges

						$gaMandatoryCharges = $gaMandatoryCharges . "
										<div class='col-sm-12'>
											<label for='' class='checkbox-custom-label'>
								";

						//Calculate Charges

						$MandatoryChargeAmt = $gaProjectCharges[$row][2];
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

							// Display Charge in Mandatory Section
							$gaMandatoryCharges = $gaMandatoryCharges . "
									<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
									<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
									";
						}

						$gaMandatoryCharges = $gaMandatoryCharges . "
											</label>  
										</div>
								";

						echo "<br><br>	ProjectCharges - ".$ProjectCharges;
					} else {
						// Optional to include the charges
						$gaOptionalCount = $gaOptionalCount + 1;
						$tempid = "optional" . $gaOptionalCount;

						$gaOptionalCharges = $gaOptionalCharges . "
										<div class='col-sm-12'>
											
								";

						//Calculate Charges
						$OptionalChargeAmt = $gaProjectCharges[$row][2];
						$tempChargeAmt = 0;
						if (1 == $gaProjectCharges[$row][3]) {
							// Per Square feet 	
							$tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

							// Display Charge in Optional Section
							$gaOptionalCharges = $gaOptionalCharges . "
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
									";
						} else {
							// Per Unit 	
							$tempChargeAmt = $OptionalChargeAmt;

							// Display Charge in Mandatory Section
							$gaOptionalCharges = $gaOptionalCharges . "
									
										<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
										<label for='" . $tempid . "' class='checkbox-custom-label'>" .
									$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
									";
						}

						$gaOptionalCharges = $gaOptionalCharges . "
											
											</label>  
										</div>
								";
					}

					//echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
				}
				//echo '884'.'---';
				//echo $gaOptionalCharges;
				//echo '-1884'.'---';
				echo $gaMandatoryCharges;

				$gProjectCharges_a = $ProjectCharges;

				$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
				$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

				//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
				//echo "<br><br>gServiceTax - ".$gServiceTax;
				//echo "<br><br>gOfferPrice - ".$gOfferPrice;
				//Stamp Duty
				$StampDuty = 0;
				if(!empty($gaStampDuty)){
				if (2 == $gaStampDuty[0][0]) {
					// Percentage
					$StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
				} else {
					// Flat Charges
					$StampDuty = $gaStampDuty[0][1];
				}
				}
				else{
				$StampDuty = '';
				}
				$gOfferPrice = $gOfferPrice + $StampDuty;
				
				$Registration = 0;
				if(!empty($gaRegistration)){
				if (2 == $gaRegistration[0][0]) {
					// Percentage
					$Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
				} else {
					// Flat Charges
					$Registration = $gaRegistration[0][1];
				}
				}
				else{
				$Registration = '';
				}
				$gOfferPrice = $gOfferPrice + $Registration;
				
				echo $gaMandatoryCharges;
			
			
			
			
			
			
			
			
			
			
            $response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("status"=>"Project Id not provided","status_code"=>"0","result"=>array());
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }
	
	
	public function getUnitsByProjectId(){
            $this->autoRender = false;
            //$data_arr = $this->SolrData->getData();
			$project_id = $_POST['project_id'];
            //$response_arr = array("status"=>"failed","status_code"=>"0","result"=>array());
            if(!isset($project_id) || empty($project_id)){
                $response_arr = array("message"=>"Failed","code"=>-1,"data"=>null);
            } else {
                $data_arr = $this->SolrData->getData($project_id);
            
                if (is_array($data_arr) && count($data_arr) > 0) {
                    $response_arr = array(
                        "message"=>"success",
                        "code"=>"1",
                        "data"=>$data_arr
                    );
                }
            }
            
            header('Content-type: application/json');
            // Convert the PHP array to JSON and echo it
            echo json_encode($response_arr);
            exit;                      
            
        }

    /* get state for add property */
	
	public function validateMobile(){
		header('Content-Type: application/json');
		//Configure::read('debug', 2);
		$this->loadModel('SalesUser');
		//$this->loadModel('OtpApp');
		$this->layout = false;
		$this->autoRender = false;
		//echo $_GET['mobile'];
		//print_r($_REQUEST);
		//$requestMobile = $_REQUEST['mobile'];
		
		//echo '<pre>'; print_r($this->SalesUser->find('all'));
		if(isset($_POST['mobile']) && $_POST['mobile'] !=''){
			$requestMobile = $_POST['mobile'];
			//echo $requestMobile; die();
			if($this->SalesUser->hasAny(array("mobile"=>$requestMobile))){
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'mobile' => $requestMobile
					)
				));
				$message = "Success";
				$code = 1;
				$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				
				$randNo = rand(1000, 9999);
				$mobileNo = $requestMobile ;
				$milliseconds = round(microtime(true) * 1000) + (10 * 60 * 1000);
				$this->OtpApp->save([
					'OtpApp' => [ 'otp' => $randNo, 'expiry' => $milliseconds, 'mobile' => $mobileNo ]
				]);
				if($this->OtpApp->getAffectedRows() == 1) {
					$this->sendSMS($mobileNo, $randNo);            
				}  else {
					echo json_encode(['message' => 'Invalid request!', 'type' => 'error']);
				}
					//die;*/
					
				$resp = array("message"=>$message,"code"=>$code,"otp"=>$randNo);
				print json_encode($resp); exit;
			}else{			
				$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;			
			}			
		}
		//echo $requestEmail;
		if(isset($_POST['email'])&& $_POST['email']!=''){
			$requestEmail = $_POST['email'];
			$requestPassword = $_POST['password'];
			//echo $requestEmail; die();
			if($this->SalesUser->hasAny(array("email"=>$requestEmail,"password"=>$requestPassword))){
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'email' => $requestEmail,
						'password' => $requestPassword
					)
				));
				$message = "Success";
				$code = 1;
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],"user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;
			}else{			
				$message = "failed";
				$code = -1;
				$data = null;
				$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
				print json_encode($resp); exit;			
			}			
		}
		
		
		
	}
	
	public function verifyOtp() {
        header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->loadModel('SalesUser');
		$this->request->data['mobile'] = $_POST['mobile'];
		$this->request->data['otp'] = $_POST['otp'];
		
        $otp_app = $this->OtpApp->find('first', [
            'conditions' => $this->request->data
        ]);
				
       // print_r($otp); die();
        if(!empty($otp_app)) {
            $milliseconds = round(microtime(true) * 1000);
            if($otp_app['OtpApp']['expiry'] > $milliseconds) {
				$usersData = $this->SalesUser->find('all', array(
					'conditions' => array(
						'mobile' => $_POST['mobile']
					)
				));
				$message = "Success";
				$code = 1;
				$data = array("user_type"=>$usersData[0]['SalesUser']['user_type'],"user_id"=>$usersData[0]['SalesUser']['id']);
				
                print json_encode([ 'message' => 'success','code' => 1, 'data' => $data ]);
            } else {
                print json_encode([ 'type' => 'failure' ]);
            }
        } else {
            print json_encode([ 'type' => 'failure' ]);
        }
        exit;
    }
	
	public function getProjectsByUserId() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		
		$this->loadModel('ProjectsAndroidApp');
		//$_POST['user_id'] = 39;
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
		$getProjects = $this->ProjectsAndroidApp->find('all',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'sales_or_broker_employee_id' => $_POST['user_id']
				)
			)
		);
		$things = Set::extract('/ProjectsAndroidApp/.', $getProjects);
		//echo '<pre>'; print_r($things);		
		//die();
		//print json_encode($things, true);
		
		$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);
				
				print json_encode($resp);
		//print_r($getProjects);exit;
		}
		
	}
	
	public function getBuildersByUserId() {
		header('Content-Type: application/json');
        //print_r($this->request->data);die();
		$this->layout = false;
		$this->autoRender = false;
		
		$this->loadModel('Websiteuser');
		//$_POST['user_id'] = 39;
		if(isset($_POST['user_id'])&& $_POST['user_id']!=''){
		$getProjects = $this->Websiteuser->find('all',
			array('fields'=> array(
					'id AS builder_id',
					'userorgname AS builder_name'			
				),
				  'conditions'=>array(
					'id' => array(45,122)
				)
			)
		);
		//echo '<pre>'; print_r($getProjects);die();
		$things = Set::extract('/Websiteuser/.', $getProjects);
		//echo '<pre>'; print_r($things);		
		//die();
		//print json_encode($things, true);
		
		$message = "Success";
				$code = 1;
				//$data = array("user_type"=>"Sales","user_id"=>$usersData[0]['SalesUser']['id']);
				
				$resp = array("message"=>$message,"code"=>$code,"data"=>$things);
				
				print json_encode($resp);
		//print_r($getProjects);exit;
		}
		
	}
	
	
	
	
	
	public function sendSMS($mobileNo, $randNo) {



        $authKey = "178586A0aldZIg59dbed56";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $mobileNo;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "FPOCKE";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Your FairPockets' OTP is " . $randNo);

        //Define route 
        $route = "99";


        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        // $url="http://api.msg91.com/api/sendhttp.php";
        $url = 'https://control.msg91.com/api/sendotp.php?authkey='. $authKey .'&mobile='. $mobileNo .'&message=Your OTP is '. $randNo .'&sender='. $senderId .'&otp=' . $randNo;

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_POST => true,
            // CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            // echo 'error:' . curl_error($ch);
            return false;
        }

        curl_close($ch);

        // echo $output;
        return true;

    }

    public function salesLoginAuth(){
		
		$this->loadModel('SalesUser');
		$this->layout = "home";
		print_r($_GET['name']);
		$_GET['name'];
		$_GET['password'];
		
		if($this->SalesUser->hasAny(array("mobile"=>$_GET['mobile']))){
			$usersExist = $this->SalesUser->find('all');
				//echo '<pre>'; print_r($usersExist);
			//$usersDetails = json_encode($usersExist);
			//print_r(json_encode($usersExist));
			$message = "Success";
			$code = 1;
			$data = array("id"=>1,"builder_id"=>45);
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			print json_encode($resp); exit;
		}else{
			$message = "failed";
			$code = -1;
			$data = null;
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			//print json_encode($resp); exit;;
			print json_encode($resp); exit;
			
		}
		
	}
}
