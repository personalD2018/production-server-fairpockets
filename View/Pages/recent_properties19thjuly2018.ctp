<style>
.hover-effect{
	height: 180px !important;
}
</style>
<?php if($result->getNumFound()) {?>
    <div class="container">
		<div class="wow fadeInDown no-padding-bottom">
			<h1>Recent Properties</h1>
		</div>
		<div class="owl-carousel ">
			<?php foreach($result as $item):?>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i> <?= $this->Number->format($item->offer_price);?></h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?= ceil(($item->discount/$item->market_price) * 100);?>%</p>
								</div>
								
								<?php $firstpic = $this->Number->getPropertyPic($item['id']);
					//echo '<pre>';print_r($firstpic);
					if(!empty($firstpic) && $firstpic != ''){
					$firstpic1 = $firstpic[0]['PropertyPic']['pic'];
					}else{
						$firstpic1 = 'upload/default.png';
					}
						//echo '<pre>'; print_r($firstpic1);
					?>
								
								<?php
										/*$title = (strpos($item->configure, 'BHK') !== false ? $item->configure : $item->configure . 'BHK') . ' - Residential ' . $item->property_type . 'for Sale';
										echo $this->Html->link($this->html->image('/'.$firstpic1, array('alt' => $title)), array("controller" => false,"action" => "/searchListDetail", base64_encode($item->id)), array('escape' => false, 'target' => '_blank', 'class' => 'hover-effect'));
										*/
								?>
								<?php if($item['posted_by'] == 'Owner' || $item['posted_by'] == 'Broker'){ ?>
									<?= $this->Html->link($this->html->image('/'.$firstpic1), array("controller" => false,"action" => "/searchListDetail", base64_encode($item->id)), array('escape' => false, 'target' => '_blank', 'class' => 'hover-effect'));?>
								<?php }else{ ?>
									<?= $this->Html->link($this->html->image('/'.$firstpic1), array("controller" => false,"action" => "/builderPropertyDetails", base64_encode($item->id)), array('escape' => false, 'target' => '_blank', 'class' => 'hover-effect'));?>
								<?php } ?>
								<!--a href="#" class="hover-effect"> 
									<img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> 
								</a--> 
							</figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
									<span class="label label-danger">Sold</span> 
								</div>
								<h2 class="property-title">
								<?php if($item['posted_by'] == 'Owner' || $item['posted_by'] == 'Broker'){ ?>
									<?php
										$url_address1_seo_url1 = (strpos($item->configure, 'BHK') !== false ? $item->configure : $item->configure . 'BHK') . ' - Residential ' . $item->property_type . 'for Sale';
										$title = str_replace(" ", "-", $url_address1_seo_url1);
										echo $this->Html->link($url_address1_seo_url1, array("controller"=>false,"action" => "/searchListDetail", $title, '?'=> array( 'fpid'=>base64_encode($item->id))), array('escapeTitle' => false, 'title' => $title));
									?>
								<?php }else{ ?>
									<?php
										$url_address1_seo_url1 = (strpos($item->configure, 'BHK') !== false ? $item->configure : $item->configure . 'BHK') . ' - Residential ' . $item->property_type . 'for Sale';
										$title = str_replace(" ", "-", $url_address1_seo_url1);
										echo $this->Html->link($url_address1_seo_url1, array("controller"=>false,"action" => "/builderPropertyDetails", $title, '?'=> array( 'fpid'=>base64_encode($item->id))), array('escapeTitle' => false, 'title' => $title));
									?>
								<?php } ?>
									<a href="view_detail_builder.html">3 Bedroom 4 Baths</a>
								</h2>
								<h4 class="property-location"><?= $item->project_name;?></h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $item->sublocality1 . ', ' . $item->city;?></h4>
							<div class="pro_tt">Residential Apartment for Sale</div>
							<div class="pro_area"><?= $item->super_builtup_area;?> Sq.ft. Super Built Up Area </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach;?>
	</div>
</div>
	<?= $this->Html->script('front/owl.carousel.min');?>
	<script>
		$(document).ready(function() {
			$('.owl-carousel').owlCarousel({
				loop: true,
				items: 1,
				margin: 10,
				lazyLoad: true,
				nav:true,
				navigation: true,
				navText: [' ', ' '],
				dots: false,
				responsiveClass: true,
				autoplay:true,
				autoplayTimeout:3000,
				autoplayHoverPause:false,
				responsive: {
					0: {
						items: 1,
						nav: true
					},
					600: {
						items: 2,
						nav: true
					},
					1000: {
						items: 4,
						nav: true,
					}
				}
				
			});
			
		});
	</script>
<?php }?>
