<?php echo $this->Html->script('validate_1.9_jquery.validate.min'); ?>

<script type="text/javascript">
    /*jQuery(document).ready(function () {
        jQuery("#formID1").validate({
            rules: {
                mobile: {
                    required: true
                }
            }

        })
    });*/
</script>
<style>
.career .form input{max-width:none;}
.green{
    color: green;
    text-align: center;
    font-weight: bold;
    padding: 18px;
}
</style>
<section class="career">
    <div class="space-m"></div>
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Career</h1>
				<?php  echo $this->Session->flash(); ?>
				<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
			</div>

			
			<?php echo $this->Form->create('Career', array(
			'url' => array('controller' => '/', 'action' => 'career'),
			"type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>
				<div class="form">
					<div class="col-md-6">
						<div class="form-list">
							<label>Name <span class="mand_field">*</span></label><br/>
								<?php echo $this->Form->input('name', array('div' => false, 'label' => false,'title' => 'Enter User Name', 'class' => 'small required', 'placeholder' => 'Enter Name')); ?>							
						</div>
						<div class="form-list">
							<label>Email*</label>
							<?php echo $this->Form->input('email',array('div' => false, 'label' => false,'title' => 'Enter Email', 'class' => 'small required', 'placeholder' => 'Enter Email')); ?>
						</div>
						<div class="form-list">
							<label>Mobile*</label>
							<?php echo $this->Form->input('mobile',array('div' => false, 'label' => false,'title' => 'Enter Mobile', 'class' => 'small required', 'placeholder' => 'Enter Mobile')); ?>
						</div>
						<div class="form-list">
							<label>City*</label>
							<?php echo $this->Form->input('city',array('div' => false, 'label' => false,'title' => 'Enter City', 'class' => 'small required', 'placeholder' => 'Enter City')); ?>
						</div>
						<div class="form-list">
							<label>Function*</label>
							<?php echo $this->Form->input('function',array('div' => false, 'label' => false,'title' => 'Enter Function', 'class' => 'small required', 'placeholder' => 'Enter Function')); ?>
						</div>
					</div>
					<div class="col-md-6">
						 <div class="form-list">
							<label>Upload Resume*</label>
							<?php echo $this->Form->file('resume_path',array('div' => false, 'label' => false,'title' => 'Select Resume', 'class' => 'small required', 'placeholder' => 'Select Resume')); ?>
						</div>
						<div class="space-s"></div>
						<div class="form-list text-center">
							<button type="submit" name="Submit" id="resume-submit">Submit</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<div class="space-l"></div>
</section>

