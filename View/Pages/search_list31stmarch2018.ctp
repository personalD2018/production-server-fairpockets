<section class="search">
	<div class="drawer-overlay text-center">
		<div class="loader">
			<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
			<h3>Loading Properties...</h3>
		</div>
	</div>
	<div class="space-m"></div>
	<div class="container">
		<div class="row">
			<div class="visible-xs visible-sm mobile-sort-link" style="display:none;">
				<div id="sort-link-filter" class="sort-link-item" data-toggle="collapse" data-parent="#accordion" data-target="#mobile-filter" >
					<i class="fa fa-filter" aria-hidden="true"></i> Filter
				</div>
				<div id="sort-link-sortby" class="sort-link-item " data-toggle="collapse" data-parent="#accordion" data-target="#mobile-sortby">
					<i class="fa fa-sort" aria-hidden="true"></i> Sort By
				</div>
			</div>
			<?php echo $this->element('search-filters');?>
			
			<div class="col-md-9 property-listing list-view">
				<?php echo $this->element('search-list-item');?>
			</div>
		</div>
	</div>
	
	<div id="contact-biulder" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Contact to <span id="leadName"></h4>
					</div>
					<div class="modal-body">
						<p>Please share your details just once to contact the Dealer</p>
						<div class="text-center" id="id_contact_succ" style="display:none;"></div>
						<div id="id_contact_form">
							<form class="fpform" id="contact-form" method="post">
								<div class="message"></div>
								<div class="form-group">
									<input id="user1" class="radio-custom" name="user_type" type="radio" value="individual">
									<label for="user1" class="radio-custom-label">Individual</label>
									<input id="user2" class="radio-custom" name="user_type" type="radio" value="dealer">
									<label for="user2" class="radio-custom-label">Dealer</label>
								</div>
								<div class="form-group">
									<input class="form-control" name="name" placeholder="Your Name" type="text">
								</div>
								
								<div class="form-group">
									<input class="form-control" placeholder="Phone" name="phone" type="text" maxlength="10">
								</div>
								
								<div class="form-group">
									<input class="form-control" placeholder="Email" name="email" type="email">
								</div>
								
								<div class="form-group">
									<label for="interested" class="label_title">Interested in (Optional)</label>
									<select id="interested" name="interested[]" class="selectpicker show-tick" multiple>
										<option value="assisted search">Assisted Search </option>
										<option value="immediate purchase">Immediate Purchase</option>
										<option value="portfolio management">Portfolio Management</option>
									</select>
								</div>
								
								<div class="form-group text-center">
									<input type="hidden" name="propertyid" class="modal-propid">
									<input type="hidden" name="userid" class="modal-userid">
									<div class="space-x"></div>
									<P><button type="submit" class="btn btn-primary">Submit</button></p>
									<p>By submitting I accept Fairpockets <a href="#">Terms & Conditions</a> </p>
								</div>
							</form>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
		<div id="verify" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Contact to <span id="leadName"></h4>
					</div>
					<div class="modal-body">
						<p>Please verify your mobile number:</p>
						<form class="fpform" id="contact-form">
							<div class="message"></div>
							<p>Enter verification code sent to mobile</p>
							<div class="row">
								<div class="col-md-7 col-sm-7 col-xs-12">
									<div class="form-group">
										<input class="form-control" name="otp" placeholder="Enter your OTP number" type="text">
									</div>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="form-group">
										<input type="hidden" name="property_id" class="modal-propid">
										<P><button type="submit" class="btn btn-primary">Submit</button></p>
									</div>
								</div>
							</div>
							<p>Didn't receive the OTP ? <a href="javascript:void(0);"  id="resend_otp" mobile="9999852468">Request Again</a></p>
							<div class="message-otp"></div>
						</form>
					</div>
				</div>
				
			</div>
		</div>
		<div class="space-s"></div>
	</section>
	<?= $this->Html->script(['front/jquery.validate', 'validation_error_messages', 'search_list'], ['block' => 'scriptBottom']);?>
