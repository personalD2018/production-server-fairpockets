<script>

    $("body").css("background-color", "#f7f7f7");

</script>

<div class="row research-reports-top inside-pages">
    <div class="col-md-12">
        <div class="container">                
            <h1>Know Your Property Before You Invests</h1>
            <form>
                <select name="city" id="city" class="selectpicker show-tick form-control">
                    <optgroup label="Select City" >
                        <option value="Noida">Noida</option>
                        <option value="Gurgoan">Gurgoan</option>
                    </optgroup>
                </select>
                <input name="proname" placeholder="Search for Property" type="text">
                <button type="submit" class="btn btn-primary">Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>                  
            </form>

            <h2 style="margin-top:40px; margin-bottom:20px;">Some important datapoints of our reports</h2>
            <ul>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon1.png">Appreciation Potential</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon2.png"> Expected Target Price</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon3.png">Value for Money</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon4.png">Rental Return</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon5.png">Market Liquidity</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon6.png">Locality Review</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon5.png">Builder Trackrecord</li>
                <li><img src="<?php echo $this->webroot; ?>css/home/images/icon/search_icon6.png">Competition Projects</li>
            </ul>            


            <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="drawer-menu-item button">Download Sample Report</a></div>


        </div>
    </div>
</div>

<div class="property-listing grid-view">
    <div class="container">
        <div class="spacer"></div>
        <div class="wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h1>Sample Reports </h1>
        </div>
        <div class="row reports">
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
                                    <span class="label label-danger">Sold</span> </div>
                                <h2 class="property-title"><a href="#">Prestige Ferns Residency</a></h2>
                                <h4 class="property-location">by Prestige Group</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Harlur Road, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 6000 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <h2 class="property-title"><a href="#">Samruddhi Wintergreen</a></h2>
                                <h4 class="property-location">by Samruddhi Realty Ltd.</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Varthur, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 4600 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <h2 class="property-title"><a href="#">Mana Karmel</a></h2>
                                <h4 class="property-location">by Mana Projects</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sarjapur Road, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 3900 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <h2 class="property-title"><a href="#">Mana Karmel</a></h2>
                                <h4 class="property-location">by Mana Projects</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sarjapur Road, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 3900 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
                                    <span class="label label-danger">Sold</span> </div>
                                <h2 class="property-title"><a href="#">Prestige Ferns Residency</a></h2>
                                <h4 class="property-location">by Prestige Group</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Harlur Road, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 6000 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <h2 class="property-title"><a href="#">Samruddhi Wintergreen</a></h2>
                                <h4 class="property-location">by Samruddhi Realty Ltd.</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Varthur, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 4600 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <h2 class="property-title"><a href="#">Mana Karmel</a></h2>
                                <h4 class="property-location">by Mana Projects</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sarjapur Road, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 3900 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="property-item table-list well">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb"> <a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt="thumb"> </a> </figure>
                        </div>
                    </div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <h2 class="property-title"><a href="#">Mana Karmel</a></h2>
                                <h4 class="property-location">by Mana Projects</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sarjapur Road, Bangalore</h4>
                                <div class="pro_tt"><b><i class="fa fa-rupee" aria-hidden="true"></i> 3900 psft</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/#services-->


<div id="services" class="service-item">
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h1 style="margin:0px;">Our Research Focus</h1>
            <h3 style="margin:0px; margin-top:5px; margin-bottom:30px;">Three pillars of our research report are Project, Builder and Location</h3>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="media services-wrap wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <div class="text-center my_details"> <img class="img-ico" src="<?php echo $this->webroot; ?>css/home/images/pt_1.png"> </div>
                    <div class="media-body">
                        <h3 class="media-heading">Project Detail	
                        </h3>
                        <p class="more">We track relevant metrics like area livability, carpet area efficiency, unit density, appreciation potential, estimated target price, market liquidity, project proximity to corporate hub, shopping areas, hospitals, rail/metro stations, comparison with other nearby projects etc.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="media services-wrap wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <div class="text-center my_details"> <img class="img-ico" src="<?php echo $this->webroot; ?>css/home/images/pt_2.png"> </div>
                    <div class="media-body">
                        <h3 class="media-heading">Builder Detail	</h3>
                        <p class="more">We cover track record of developer, performance of their past project, the architect they appoint, current project quality etc. All these facts reveal the intentions and quality policy of the builder.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="media services-wrap wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <div class="text-center my_details"> <img class="img-ico" src="<?php echo $this->webroot; ?>css/home/images/pt_3.png"> </div>
                    <div class="media-body">
                        <h3 class="media-heading">Location Insight
                        </h3>
                        <p class="more">Choosing right city and right locality can make you richer by over 100% in a short span of few years. Spending more and more time in researching about location is the key to successful investing in real estate. Our reports elaborately describe details of location for better decision making.</p>
                    </div>
                </div>
            </div>

        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>



