<style>
	.detail-amenities .list-amenities li{width:86% !important;}
	#bottom{display:none;}
	#header{display:none;}
	#footer{display:none;}
</style>
<?php
// Offer price calculator
$gOfferPrice = $propertydetails['Property']['offer_price'];
$gServiceTax = 0;
if (!empty($all_property_features['47'])) {
    $gSuperBuiltUpArea = $all_property_features['47'];
} else if(!empty($all_property_features['50'])) {
    $gSuperBuiltUpArea = $all_property_features['50'];
}
else{
	$gSuperBuiltUpArea = '';
}
// Super Built up area ( in sq ft )
//echo '<pre>'; print_r($project_pricing);
$basePriceArr = json_decode($project_pricing['ProjectPricing']['bsp_charges'], true);
if(!empty($project_pricing)){
	if($project_pricing['ProjectPricing']['price_bsp_copy'] != ''){
		$gBaseRate = $project_pricing['ProjectPricing']['price_bsp_copy'];   // Base Rate
	}else{
		$gBaseRate = $basePriceArr['price_bsp1']; // Base Rate
	}
	$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
	$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
}
else
{
	$gBaseRate = '';   // Base Rate
	$gServiceTaxOnBSP = '';  // GST on BSP ( in % )	
	$gServiceTaxOnOTH = '';  // GST on OTHERS ( in % )
}



/* get project charges array */
if(!empty($project_pricing)){
$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
}
else{ 
$project_pricing_charge = array();
}
// ProjectCharges

$project_charges = array();
for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
    if (!empty($project_pricing_charge['price_pcharge_amt' . $i])) {
        $pcharge = $project_pricing_charge['price_pcharge' . $i];

        switch ($pcharge) {
            case "1":
                $project_charges[$j]['0'] = "One Covered Car Park";
                break;
            case "2":
                $project_charges[$j]['0'] = "Double Covered Car Park";
                break;
            case "3":
                $project_charges[$j]['0'] = "Club Membership";
                break;
            case "4":
                $project_charges[$j]['0'] = "Power BackUp per KVA";
                break;
            case "5":
                $project_charges[$j]['0'] = "Interest Free Maintenance";
                break;
            case "6":
                $project_charges[$j]['0'] = "Road Facing PLC";
                break;
            case "7":
                $project_charges[$j]['0'] = "Park Facing PLC";
                break;
            default:
                $project_charges[$j]['0'] = "Corner PLC";
        }

        $project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
        $project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
        $project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];
    }
}
/* get additional charges array */
if(!empty($project_pricing)){
$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
}
else{
$project_additional = array();
}
$additon_charges = array();
for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
    if (!empty($project_additional['price_core_amt' . $i])) {
        $acharge = $project_additional['price_core_plc' . $i];

        switch ($acharge) {
            case "1":
                $additon_charges[$j]['0'] = "Lease Rent";
                break;
            case "2":
                $additon_charges[$j]['0'] = "External Electrification Charges";
                break;
            case "3":
                $additon_charges[$j]['0'] = "External development Charges";
                break;
            case "4":
                $additon_charges[$j]['0'] = "Infrastructure development Charges";
                break;
            case "5":
                $additon_charges[$j]['0'] = "Electricity Connection Charges";
                break;
            case "6":
                $additon_charges[$j]['0'] = "Fire fighting charges";
                break;
            case "7":
                $additon_charges[$j]['0'] = "Electric Meter Charges";
                break;
            case "8":
                $additon_charges[$j]['0'] = "Gas Pipeline Charges";
                break;
            default:
                $additon_charges[$j]['0'] = "Sinking Fund";
        }

        $additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
        $additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
        $additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
    }
}
/* get other charges array */
if(!empty($project_pricing))
{
$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
}
else{ $project_other = array();}

$other_charges = array();
for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
    if (!empty($project_other['price_other_amt' . $i])) {
        $ocharge = $project_other['price_other_plc' . $i];

        switch ($ocharge) {
            case "1":
                $other_charges[$j]['0'] = "Other Charges1";
                break;
            case "2":
                $other_charges[$j]['0'] = "Other Charges2";
                break;
            case "3":
                $other_charges[$j]['0'] = "Other Charges3";
                break;
            case "4":
                $other_charges[$j]['0'] = "Other Charges4";
                break;
            case "5":
                $other_charges[$j]['0'] = "Other Charges5";
                break;
            case "6":
                $other_charges[$j]['0'] = "Other Charges6";
                break;
            case "7":
                $other_charges[$j]['0'] = "Other Charges7";
                break;
            case "8":
                $other_charges[$j]['0'] = "Other Charges8";
                break;
            default:
                $other_charges[$j]['0'] = "Other Charges9";
        }

        $other_charges[$j]['1'] = $project_other['price_other_type' . $i];
        $other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
        $other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
    }
}
$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
$gaProjectChargesLen = count($gaProjectCharges);

/*
  $gaProjectCharges = array (

  // [ index(0- ( 9+8+4) ) , option(1-2) , amount , unit(1-2) ]

  array( 'Lease Rent' , 2 , 1 , 1 ),
  array( 'External Electrification Charges' , 2 , 1 , 2 ),
  array( 'External development Charges' , 1 , 1 , 2 ),
  array( 'Infrastructure development Charges' , 1 , 1 , 1	 ),
  array( 'Electricity Connection Charges' , 2 , 1 , 1 ),
  array( 'Fire fighting charges' , 2 , 1 , 2 ),
  array( 'Electric Meter Charges', 1 , 1 , 2 ),
  array( 'Gas Pipeline Charges' , 1 , 1 , 1 ),
  array( 'Sinking Fund' , 2 , 1 , 2 ),

  array( 'One Covered Car Park' , 2 , 3 , 1 ),
  array( 'Double Covered Car Park' , 2 , 1 , 2 ),
  array( 'Club Membership' , 1 , 1 , 2 ),
  array( 'Power BackUp per KVA' , 2 , 1 , 2 ),
  array( 'Interest Free Maintenance' , 2 , 1 , 1 ),
  array( 'Road Facing PLC' , 2 , 1 , 2 ),
  array( 'Park Facing PLC', 2 , 2 , 2 ),
  array( 'Corner PLC' , 2 , 3 , 2 ),

  array( 'Other PLC 1' , 2 , 1 , 1 ),
  array( 'Other PLC 2' , 2 , 1 , 2 ),
  array( 'Other PLC 3' , 1 , 1 , 2 ),
  array( 'Other PLC 4' , 2 , 1 , 2 )

  );
 */


// Project Pricing - Stamp Duty
if(!empty($project_pricing))
{
$gaStampDuty = array(
    //[ option(1-2) , amount ]

    array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
);

// Project Pricing - Registration
$gaRegistration = array(
    // [ option(1-2) , amount ]
    array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
);
}
else{
$gaStampDuty = array();
$gaRegistration = array();
}
// Project Pricing - Floor PLC
$gaTotalFloors = $projectdetail['ProjectDetails']['num_of_tow']; // Total number of floor in the tower of property
if(!empty($project_pricing))
{
if ($project_pricing['ProjectPricing']['floor_per_plc_amount'] != '') {
    $gaFloorPLC = array(
        // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
        array($project_pricing['ProjectPricing']['floor_option'], $project_pricing['ProjectPricing']['floor_per_plc_amount'], $project_pricing['ProjectPricing']['floor_per_option'])
            //array( 2 , 10 , 1 )
    );
} else {
    $gaFloorPLC = array(
        // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
        array($project_pricing['ProjectPricing']['floor_option'], 0, $project_pricing['ProjectPricing']['floor_per_option'])
            //array( 2 , 10 , 1 )
    );
}
}
else
{
	 $gaFloorPLC = array();
}

/*
  echo "<br><br><br><br>"."gaProjectCharges"."<br>";
  pr($gaProjectCharges);
  echo "<br>"."gaStampDuty"."<br>";
  pr($gaStampDuty);
  echo "<br>"."gaRegistration"."<br>";
  pr($gaRegistration);
  echo "<br>"."gaFloorPLC"."<br>";
  pr($gaFloorPLC);
 */


$gaMandatoryCharges = '';
$gaOptionalCharges = '';
$gaOptionalCount = 0;
$ProjectCharges = 0;
$gaFloorPLCCharges = 0;

if(!empty($gaFloorPLC))
{
if ($gaFloorPLC[0][1] > 0) {
    $gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;

    if ($gaFloorPLC[0][0] == 2) {
        // BSP w.r.t Ground floor
        $tempFloor = 0; //0 - Ground Floor
        $gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
    } else {
        // BSP w.r.t TOP floor;
        $tempFloor = 0; //0 - Ground Floor
        $gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
    }

    if ($gaFloorPLC[0][2] == 2) {
        // sub option
        $gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
    }

    $gaMandatoryCharges = $gaMandatoryCharges . "
					<div class='col-sm-12'>
						<label for='' class='checkbox-custom-label'>
				";
    $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
            'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC1[0][1] . "&nbsp;/ sq ft</span>
					";
    $gaMandatoryCharges = $gaMandatoryCharges . "
						</label>  
					</div>
			";
}
}
$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
$gOfferPrice = $gOfferPrice + $BaseCharge;
$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );

//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
//echo "<br><br>BaseCharge - ".$BaseCharge;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
// Project Charges
$ProjectCharges = 0;

//echo '<pre>'; print_r($gaProjectCharges);

for ($row = 0; $row < $gaProjectChargesLen; $row++) {
    //echo "<br><br>".$gaMandatoryCharges;
    if (1 == $gaProjectCharges[$row][1]) {
        // Mandatory to include the charges

        $gaMandatoryCharges = $gaMandatoryCharges . "
						<div class='col-sm-12'>
							<label for='' class='checkbox-custom-label'>
				";

        //Calculate Charges

        $MandatoryChargeAmt = $gaProjectCharges[$row][2];
        if (1 == $gaProjectCharges[$row][3]) {
            // Per Square feet 	
            $ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );

            // Display Charge in Mandatory Section
            $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
					";
        } else {
            // Per Unit 	
            $ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );

            // Display Charge in Mandatory Section
            $gaMandatoryCharges = $gaMandatoryCharges . "
					<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
					<label class='' style='cursor:default;'><i class='fa fa-check-square-o' aria-hidden='true' style='padding-right:6px !important;'></i>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
					";
        }

        $gaMandatoryCharges = $gaMandatoryCharges . "
							</label>  
						</div>
				";

        //echo "<br><br>	ProjectCharges - ".$ProjectCharges;
    } else {
        // Optional to include the charges
        $gaOptionalCount = $gaOptionalCount + 1;
        $tempid = "optional" . $gaOptionalCount;

        $gaOptionalCharges = $gaOptionalCharges . "
						<div class='col-sm-12'>
						    
				";

        //Calculate Charges
        $OptionalChargeAmt = $gaProjectCharges[$row][2];
        $tempChargeAmt = 0;
        if (1 == $gaProjectCharges[$row][3]) {
            // Per Square feet 	
            $tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;

            // Display Charge in Optional Section
            $gaOptionalCharges = $gaOptionalCharges . "
						<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
						<label for='" . $tempid . "' class='checkbox-custom-label'>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
					";
        } else {
            // Per Unit 	
            $tempChargeAmt = $OptionalChargeAmt;

            // Display Charge in Mandatory Section
            $gaOptionalCharges = $gaOptionalCharges . "
					
						<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
						<label for='" . $tempid . "' class='checkbox-custom-label'>" .
                    $gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
					";
        }

        $gaOptionalCharges = $gaOptionalCharges . "
							
							</label>  
						</div>
				";
    }

    //echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
}

$gProjectCharges_a = $ProjectCharges;

$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );

//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
//echo "<br><br>gServiceTax - ".$gServiceTax;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
//Stamp Duty
$StampDuty = 0;
if(!empty($gaStampDuty)){
if (2 == $gaStampDuty[0][0]) {
    // Percentage
    $StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
} else {
    // Flat Charges
    $StampDuty = $gaStampDuty[0][1];
}
}
else{
$StampDuty = '';
}
$gOfferPrice = $gOfferPrice + $StampDuty;

//echo "<br><br>StampDuty - ".$StampDuty;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;
//Registration Charges
$Registration = 0;
if(!empty($gaRegistration)){
if (2 == $gaRegistration[0][0]) {
    // Percentage
    $Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
} else {
    // Flat Charges
    $Registration = $gaRegistration[0][1];
}
}
else{
$Registration = '';
}
$gOfferPrice = $gOfferPrice + $Registration;

//echo "<br><br>Registration - ".$Registration;
//echo "<br><br>gOfferPrice - ".$gOfferPrice;


?>
<script>
    jQuery(document).ready(function () {
        function nFormatter(num)
        {
            num = Math.abs(num)
            if (num >= 10000000)
            {
                formattedNumber = (num / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (num >= 100000)
            {
                formattedNumber = (num / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (num >= 1000)
            {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = num;
            }

            return formattedNumber;
        }

<?php
if ($propertydetails['Property']['transaction_type_id'] == '1') {
    ?>
            //format = nFormatter('<?php echo $all_property_features['1'] * $gBaseRate; ?>');
			format = nFormatter('<?php echo $propertydetails['Property']['offer_price']; ?>');
            document.getElementById("offerpriceformater").innerHTML = format;
            //mformat = nFormatter('<?php echo $gBaseRate; ?>');
            //document.getElementById("marketpriceformater").innerHTML = mformat;
    <?php
} else {
    ?>
            rformat = nFormatter('<?php echo $propertydetails['Property']['market_rent']; ?>');

            document.getElementById("marketrentformater").innerHTML = rformat;

            eformat = nFormatter('<?php echo $propertydetails['Property']['expected_monthly']; ?>');

            document.getElementById("expectedmonthlyformat").innerHTML = eformat;
<?php } ?>

    });

</script>        
               
<style>
    .detail_header .h_feature 
    {
        font-size: 20px;
        color: #29333d;
    }
    .green{color:green; font-weight:bold;border:1px solid; padding:7px;}
    .error{color:red;}
</style>



<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

<section class="propert-detail">
    <div class=" property_detail_area">
        <div class="space-m"></div>
        <div class="container">
            <div class=" property_detail_inners">
                <?php //print_r($proj_image); ?>
                <div class="row">   
                    <div class="col-md-9 col-sm-8 col-xs-12" id="page-content">
                        <div id="amenitie" class="detail-amenities">                            
                            <div class="row">
								<div id="calyourprice" class="detail-amenities detail-block target-block col-md-12">

									<div class="">

										<div class=" col-md-12">
											<div class="detail-title">
												<h2  class="title-left">Project Price Calculator</h2>
											</div>
										</div>

										<div id="amenities" class="detail-amenities detail-block target-block col-md-12" style="margin-top:0px;margin-bottom:0px;padding-top:0px;">

											<div class="row">
												<div class="col-sm-6">
													<label for="" class="form-group"><strong>Property area : </strong> <?php echo $gSuperBuiltUpArea ?>&nbsp;sq ft </label>
												</div>
												<div class="col-sm-6">
													<label for="" class="form-group"><strong>Base Rate : </strong> <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;  
														<input name="bsp_price_value" id="bsp_price_value" value="<?php echo $gBaseRate; ?>" style="width:14%;">
														&nbsp;/ sq ft

													</label>
												</div>

											</div>

											<div class="row">
												<div class="col-sm-6">
													<label for="" class="form-group"><strong>GST ( on Base ) : </strong> <?php echo $gServiceTaxOnBSP ?>&nbsp;&#37; </label>
												</div>

												<div class="col-sm-6">
													<label for="" class="form-group"><strong>GST ( on Others ) : </strong> <?php echo $gServiceTaxOnOTH ?>&nbsp;&#37; </label>
												</div>								
											</div>

											<div class="row">
												<div class="col-sm-6">
													<label for="" class="form-group"><strong>Registration Charges : </strong> 
														<?php
														if(!empty($gaRegistration)){
														if (2 == $gaRegistration[0][0]) {
															// Percentage
															echo $gaRegistration[0][1] . "&nbsp;&#37";
														} else {
															// Flat Charges
															echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaRegistration[0][1];
														}
														}
														?>
													</label>
												</div>

												<div class="col-sm-6">
													<label for="" class="form-group"><strong>Stamp Duty : </strong> 
														<?php
														if(!empty($gaStampDuty)){
														if (2 == $gaStampDuty[0][0]) {
															// Percentage
															echo $gaStampDuty[0][1] . "&nbsp;&#37";
														} else {
															// Flat Charges
															echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaStampDuty[0][1];
														}
														}
														?>
													</label>
												</div>								
											</div>

											<!-- If no plc charges ,  donot display the below div -->
									

										</div>

									</div>




									<!--builder name -->






									<!-- builder name end-->


									<div class="col-md-6">

										<div class="detail-title">
											<h2 class="title-left"> Mandatory Charges</h2>
										</div> 

										<div class="form-group">
											<div class="row" id="id_mandatory_charges">

												<?php echo $gaMandatoryCharges; ?>

											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="detail-title">
											<h2 class="title-left">Optional Charges</h2>
										</div> 

										<div class="form-group ">
											<div class="row" id="id_optional_charges">

												<?php echo $gaOptionalCharges; ?>

											</div>                          

										</div>
										
										<?php //echo '<pre>';print_r($bsp_limit);
											if(!empty($gaStampDuty)){ ?>
											<div class="row" <?php //if ($gaFloorPLC[0][1] <= 0) echo "style='display:none;'" ?> >
												<div class="col-sm-6">
													<div class="form-group">
														<label for="" class="label_title"><strong>Floor Choice </strong></label>
														<div class="select-style">
															<div class="input select">
																<select name="floor choice" id="id_floor_choice">
																	<!--<option value="0" selected="selected" >Ground</option>-->
																	<?php
																	for ($opt = $bsp_limit[0][0]['startloop']; $opt <= $bsp_limit[0][0]['endloop']; $opt++) {
																		echo '<option value=\'' . $opt . '\'>' . $opt . '</option>';
																	}
																	?>
																</select>
															</div>
														</div>	
													</div>	
												</div>
											</div>	
											<?php }?>


									</div>

									<div class="col-md-12 text-center">

										<div class="caltotprice">
											<div class="caltitle">Your Final Calculated Price</div>
											<span class="calprice" id="id_calprice">
											<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo round($gOfferPrice, 2); ?>
											</span>
											<!-- <div class="caltitle">GST ( Others ) : <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $gServiceTax; ?> </div> -->
											<div id="id_chart"></div>
											<div id="container" style="width:100%; height:300px;" >

											</div>	
										</div>

									</div>
							
													

                        </div>
                                
                            </div>
                        </div>
                    </div>  
                 
                </div> 
            </div>
        </div>
    </div>
</div>
	</section>

<?= $this->Html->script(['validation_error_messages', 'search_list', 'owl-carousel'], ['block' => 'scriptBottom']);?>   

<?php
echo $this->Html->script('highcharts');
echo $this->Html->script('highcharts-3d');
?>  


<?php
// Generate client side offer price calculator part
echo "
		<input type='hidden' id='id_gProjectChargesAmt' value='" . $gProjectCharges_a . "'>
	";
if(!empty($gaStampDuty)){
if (2 == $gaStampDuty[0][0]) {
    // Percentage
    echo "<input type='hidden' id='id_gaStampDuty' name='percent' value='" . $gaStampDuty[0][1] . "'>";
} else {
    // Flat Charges
    echo "<input type='hidden' id='id_gaStampDuty' name='amount' value='" . $gaStampDuty[0][1] . "'>";
}
}
if(!empty($gaRegistration)){
if (2 == $gaRegistration[0][0]) {
    // Percentage
    echo "<input type='hidden' id='id_gaRegistration' name='percent' value='" . $gaRegistration[0][1] . "'>";
} else {
    // Flat Charges
    echo "<input type='hidden' id='id_gaRegistration' name='amount' value='" . $gaRegistration[0][1] . "'>";
}
}
if(!empty($gaFloorPLC)){ 
$gafplc = $gaFloorPLC[0][0];
$gafplc1 = $gaFloorPLC[0][1];
$gafplc2 = $gaFloorPLC[0][2];
} else{ $gafplc ='';$gafplc1 ='';$gafplc2 ='';}
/*echo '884';
echo $gBaseRate.'-base rate<br/>';
echo $gServiceTaxOnBSP.'-base rate<br/>';
echo $gServiceTaxOnOTH.'-base rate<br/>';
echo $gafplc.'-base rate<br/>';
echo $gafplc1.'-base rate<br/>';
echo $gafplc2.'-base rate<br/>';
echo $gSuperBuiltUpArea.'-base rate<br/>';
echo $gaTotalFloors.'-base rate<br/>';*/
echo "
	
		<script>
		
		    var BaseCharges = 0;
			var ServiceTaxCharges = 0;
			var GovtCharges = 0;
			var ProjectCharges = 0;

			function ReCalculateOfferPrice( elem )
			{
				
				var BaseCharges = 0;
				var GovtCharges = 0;
				
				//tBaseRate = 3510;
				//alert(brate);
				//tBaseRate1 = parseInt(document.getElementById('bsp_price_value').value);
				//alert(tBaseRate1);
			
				//tBaseRate = " . $gBaseRate . ";
				tBaseRate = parseInt(document.getElementById('bsp_price_value').value);
				tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
				
				tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
				alert(tProjectChargesAmt);
				tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
				
				eStampDuty = document.getElementById('id_gaStampDuty');
				eRegistration = document.getElementById('id_gaRegistration');
				
				tBSPForFloor = " . $gafplc . ";
				tBSPForFloorCharges = " . $gafplc1 . ";
				tBSPForFloorOption = " . $gafplc2 . ";
	
	            tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
	                
				tFloorPLC = 0;
				
				if ( tBSPForFloorCharges > 0 )
				{	
					tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
					
					if ( tBSPForFloor == 2 )
					{
						// BSP w.r.t Ground floor
						tempFloor = parseInt(document.getElementById('id_floor_choice').value) ; //0 - Ground Floor
						tFloorPLC = tFloorPLC * tempFloor ;
						
					}
					else
					{
						// BSP w.r.t TOP floor;
						tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice').value) + 1; //0 - Ground Floor
						tFloorPLC = tFloorPLC * tempFloor ;
					}			
					
					if ( tBSPForFloorOption == 2 )
					{
						// sub option
						tFloorPLC = tFloorPLC*(-1);
					}	

				}
				
				nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
				BaseCharges += nOfferPriceAmt;
				
				ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
				nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
				//alert(nOfferPriceAmt);
				if ( elem )
				{	
					// Change in optional element
					
					if ( true == elem.checked )
					{	
						//alert('Checked  - '+elem.value);
						tProjectChargesAmt = tProjectChargesAmt + parseInt(elem.value);
						document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
						
					}	
					else
					{	
						//alert('Unchecked - '+elem.value);
						tProjectChargesAmt = tProjectChargesAmt - parseInt(elem.value);
						document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
					}
				}
				else
                {
					// Change in choice of floor
					
					tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                } 					
				//alert(tProjectChargesAmt);
				nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
				nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt ; 	
				alert(nOfferPriceAmt);
				if ( 'amount' == eStampDuty.name )
				{
					GovtCharges += parseInt(eStampDuty.value);
					nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
				}
				else
				{
					GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
					nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
				}		
				
				if ( 'amount' == eRegistration.name )
				{
					GovtCharges +=  parseInt(eRegistration.value);
					nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
				}
				else
				{//alert(BaseCharges);
					GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
					nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100) );
				}
				alert(GovtCharges);
				//alert('Final Offer Price ='+nOfferPriceAmt);
				

				
						
				document.getElementById('id_calprice').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString('en');
				
				//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
				                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
				                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
															   'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
															   'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

				$('#container').highcharts({
				 
						chart: {
							type: 'pie',
							options3d: {
								enabled: true,
								alpha: 45
							}
						},
						title: {
							text: 'Property Charges'
						},
						tooltip: {
							//pointFormat: '{series.name}: <b>{point.value}%</b>'
						},
						plotOptions: {
							pie: {
								innerSize: 100,
								depth: 45
							}
						},
						series: [{
							data: [
								['Base Charge', eval(BaseCharges.toFixed(2)) ],
								['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
								['Government Charges', eval( GovtCharges.toFixed(2) )],
								['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
								['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))	 ]
							]
							
						}]
				});	
				
				//$('.place-desc-large').find(':first-child').first().text('new text');
				//$('.place-desc-large DIV:first-child').attr('html', 'abc');
				//$('.place-name' ).html( 'background-color');
				//$('.place-desc-large div.place-name').text('MY NEW TEXT');
			}
			
			ReCalculateOfferPrice();
			
		</script>

	";
?>	 
<!-- jQuery Form Validation code and submit lead/contact information-->
 
<?php
echo $this->Html->script('add_property_7');
// Jquery Script for Validation of form fields    
//echo $this->Html->script('validate_1.9_jquery.validate.min');

?>
<?php $projectid = $project['Project']['project_id']; ?>
<input type='hidden' id='projectid' value='<?php echo $projectid; ?>'>
<script>

$(document).ready(function () {
        $("#id_floor_choice").change(function ()
        {
            var id = $(this).val();
			var projectid = $('#projectid').val();
            if (id) {
                var dataString = 'id=' + id + '&projectid=' + projectid;
				//alert(dataString);
                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "pages", "action" => "getbspoptions")); ?>',
                    data: dataString,
                    success: function (data) {
                    	$('#bsp_price_value').val(data);
                        ReCalculateOfferPrice8(data);
                    }
                });  
				
            }
        });
    });

</script>

<?php

echo "
    
        <script>
        
            var BaseCharges = 0;
            var ServiceTaxCharges = 0;
            var GovtCharges = 0;
            var ProjectCharges = 0;

            function ReCalculateOfferPrice8( elem )
            {
                
                var BaseCharges = 0;
                var GovtCharges = 0;
                
                tBaseRate = elem;
                //alert(tBaseRate);
                
            
                //tBaseRate = " . $gBaseRate . ";
                tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
                
                tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
                
                eStampDuty = document.getElementById('id_gaStampDuty');
                eRegistration = document.getElementById('id_gaRegistration');
                
                tBSPForFloor = " . $gafplc . ";
                tBSPForFloorCharges = " . $gafplc1 . ";
                tBSPForFloorOption = " . $gafplc2 . ";
    
                tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
                //alert(tOfferPriceAmt);
                    
                tFloorPLC = 0;
                
                if ( tBSPForFloorCharges > 0 )
                {   
                    tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
                    
                    if ( tBSPForFloor == 2 )
                    {
                        // BSP w.r.t Ground floor
                        tempFloor = parseInt(document.getElementById('id_floor_choice').value) ; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                        
                    }
                    else
                    {
                        // BSP w.r.t TOP floor;
                        tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice').value) + 1; //0 - Ground Floor
                        tFloorPLC = tFloorPLC * tempFloor ;
                    }           
                    
                    if ( tBSPForFloorOption == 2 )
                    {
                        // sub option
                        tFloorPLC = tFloorPLC*(-1);
                    }   

                }
                
                nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
                BaseCharges += nOfferPriceAmt;
                
                ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
                nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
                //alert(nOfferPriceAmt);
                
                    // Change in choice of floor
                    
                    tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
                //}                   

                nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
                nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt ;     
                //alert(nOfferPriceAmt);
                if ( 'amount' == eStampDuty.name )
                {
                    GovtCharges += parseInt(eStampDuty.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
                }
                else
                {
                    GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
                    nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
                }       
                
                if ( 'amount' == eRegistration.name )
                {
                    GovtCharges +=  parseInt(eRegistration.value);
                    nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
                }
                else
                {
                    GovtCharges +=  (BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
                    nOfferPriceAmt = nOfferPriceAmt + ((BaseCharges + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100) );
                }
                
                //alert('Final Offer Price ='+nOfferPriceAmt);
                

                
                        
                document.getElementById('id_calprice').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toLocaleString('en');
                
                //document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
                                                               'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
                                                               'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
                                                               'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
                                                               'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   

                $('#container').highcharts({
                 
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Property Charges'
                        },
                        tooltip: {
                            //pointFormat: '{series.name}: <b>{point.value}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [{
                            data: [
                                ['Base Charge', eval(BaseCharges.toFixed(2)) ],
                                ['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
                                ['Government Charges', eval( GovtCharges.toFixed(2) )],
                                ['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
                                ['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))   ]
                            ]
                            
                        }]
                }); 
                
                //$('.place-desc-large').find(':first-child').first().text('new text');
                //$('.place-desc-large DIV:first-child').attr('html', 'abc');
                //$('.place-name' ).html( 'background-color');
                //$('.place-desc-large div.place-name').text('MY NEW TEXT');
            }
            
            //ReCalculateOfferPrice();
            
        </script>

    ";

    ?>