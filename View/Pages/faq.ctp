<section class="faq">
<div class="space-m"></div>
<div class="container">
<div class="row">
    <div class="col-md-12">
		<h1 style="text-align:center">FAQ</h1>
		<div class="space-m"></div>
		<div class="account-block">
            <div class="add-title-tab">
                <h3>Price related query</h3>
                <div class="add-expand active"></div>
            </div>
            <div class="add-tab-content" style="display:block;">
                <div class="add-tab-row push-padding-bottom row-color-gray">
                    <h3>What is fair price ?</h3>
                    <p>Fair price is a subjective fairpockets exclusive offered price of a property which is below or at par with the market price and above the lowest price for that particular property. It&#39;s lower or at par with the market price because the seller has the option to reduce the price below market rate if he needs money and he wants to sell fast. On the other hand the buyer will never buy at a higher price than the market rate as there are enough options for him/her to choose from.</p>

                    <h3>What is advertised price, market price and lowest price ?</h3>
                    <p>
                        &#45; Advertised Price: Price that sellers quote, this does not generally include any discount.<br> 
                        &#45; Market Price: Average transaction price of the property in recent past. <br>
                        &#45; Lowest Price: Exceptionally low price quoted by seller to sell their property fast. <br> 

                    </p>

                    <h3>Why is fair price required ?</h3>
                    <p>The price of the property is the make or break factor. It is the main attraction for buyers. The &quot;fair price&quot; will help you in attracting more prospective customers, enabling you to sell your property faster. </p>

                    <h3>How does the listing works ?</h3>
                    <p>You can register and fill up the details of your property. Once you fill up then our research team evaluates your market and offer price. If require then our team will interact with you and then the listing is approved and it goes live.</p>



                    <h3>What is market Price and Fair Offer Price while posting property ?</h3>
                    <p>Market price is the approx. price at which the transaction is happening for similar properties. Fair offer price is the price at which you would like to dispose your property. Ideally the Fair offer price cannot be higher than the market price.</p>


                </div>
            </div>

            <div class="add-title-tab">
                <h3>Service related query</h3>
                <div class="add-expand"></div>
            </div>
            <div class="add-tab-content">
                <div class="add-tab-row push-padding-bottom row-color-gray">
                    <h3>Can you help us buy or sell properties ?</h3>
                    <p> If you avail of our services, we need exclusive rights to buy/sell your property and we give full effort to make sure that the transaction takes place. The mandate for selling the property is for 6 months and the mandate for buying can be decided mutually. Though there is no guarantee that the transaction will take place. We also donot charge until the transaction takes place.</p>

                    <h3>How does assisted search work ?</h3>
                    <p> Assisted Search gives provides assistance to you and makes sure that the buying/selling of the property does not take a backseat and the property is sold/bought at the right time. <br><br>
                        In the real estate market, timing is the most crucial factor. The right decision at the right time will make you gain tremendously from a property transaction. 
                    </p>

                    <h3>What is property management ?</h3>
                    <p> Property management is a service for out of town customers or busy professionals or anyone who doesn&#39;t have the time to take care of their property or tenant on a day to day basis. </p>


                    <h3>What is portfolio management ?</h3>
                    <p> If you have multiple properties and want to maximize the returns then portfolio management is the right service for you. We evaluate each property of yours to recommend the right action plan for each property and portfolio as a whole. We also help in auctioning these recommendations.</p>


                    <h3>Are these services all over the country ?</h3>
                    <p> No. We are expanding but currently its available primarily in Delhi NCR region. Listing at fair price can be done from most of the cities in India</p>




                </div>
            </div>

            <!--
                         <div class="add-title-tab">
                               <h3>Lorem Ipsum</h3>
                                    <div class="add-expand"></div>
                         </div>
                         <div class="add-tab-content">
                            <div class="add-tab-row push-padding-bottom row-color-gray">
                                    <h3>What is Lorem Ipsum?</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                           </div>
                        </div>
            -->

        </div>
    </div>  

</div>
</div>
<div class="space-m"></div>
</section>


<script type="text/javascript">
    $(document).ready(function ($) {

        $("li.drop-accordian a:first").bind("click", function (e) {
            $(this).next('ul').slideToggle();
            e.stopPropagation();
        });

        $('#firstLevelNav_small').on('hidden.bs.dropdown', function () {
            $(this).find('ul.drop-accordian-menu').hide();
        });


        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });



    });
</script> 
