<div class="our-research-focus">
    <div class="space-l"></div>
    <div class="container">
        <div class="banner-heading text-center">
            <h2>We take complete ownership in searching your prospective buyer or seller in the fastest possible time</h2>
            <img src="<?php echo $this->webroot; ?>css/home/images/our-research-focus.gif" alt="Our Research Focus">
        </div>
       
        <div class="link">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="drawer-menu-item button">Sell Your Property</a>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="drawer-menu-item button">Buy Your Property</a>
        </div>
    </div> 
    <div class="space-l"></div>
    <!--	<div class="row light-grey">
            <div class="col-lg-6 col-md-6 col-sm12">
                <div class="heading">
                    <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-local-support.png">
                    <h3>Portfolio<br>
    Recommendation Report</h3>
                </div>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm12">
                <div class="heading">
                    <img src="<?php echo $this->webroot; ?>css/home/images/icon/pm-society-bills.png">
                    <h3>Executing<br>
    The Recommendation</h3>
                </div>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
            </div>
        </div>    
    -->    

    <section class="light-yellow">
        <div class="space-l"></div>
        <div clas="container">
            <div class="process">
                <img src="<?php echo $this->webroot; ?>css/home/images/icon/sell.png" alt="Sell">
                <h2>Assisted <span><strong>Sell/Rent</strong></span> for Sellers</h2>
                <h4>Outsource your selling & renting responsibilities to us for faster results.</h4>
                <ul>
                    <li class="step1"><strong>Step 1</strong><br>
                        Property Post on Fair Pockets</li>
                    <li class="step2"><strong>Step 2</strong><br>
                        we reach out to buyers and brokers</li>
                    <li class="step3"><strong>Step 3</strong><br>
                        Closing Transaction</li>
                    <li class="step4"><strong>Step 4</strong><br>
                        Reinvestment Recommendation</li>
                </ul>
                <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Sell Your Property</a></div>
            </div>
        </div>
        <div class="space-l"></div>
    </section>



    <section class="process-contents">
        <div class="space-l"></div>
        <div clas="container">
        <div class="process">
            <img src="<?php echo $this->webroot; ?>css/home/images/icon/buy.png" alt="Buy">
            <h2>Assisted <span><strong>Buy/Rent</strong></span> for Buyers</h2>
            <h4>Get trusted properties at fair price from Fair Pockets</h4>
            <ul>
                <li class="step1"><strong>Step 1</strong><br>
                    Need Assessment</li>
                <li class="step2"><strong>Step 2</strong><br>
                    Shortlisting Project</li>
                <li class="step3"><strong>Step 3</strong><br>
                    Closing Transaction</li>
                <li class="step4"><strong>Step 4</strong><br>
                    Post Sale Services</li>
            </ul>
            <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" >Buy Your Property</a></div>
        </div>
            </div>
        <div class="space-l"></div>
    </section>



</div>


<section class="light-grey text-center">
    <div class="space-l"></div>
    <div class="container">
    <h2><span>Benefit</span> of Assisted search services</h2>
    <div class="space-l"></div>
    <div class="row">
	<div class="col-md-9 col-md-offset-2">
		<div class="row">
    <div class="col-md-2 col-sm-6 col-xs-12">
        <img src="<?php echo $this->webroot; ?>css/home/images/icon/saving-time.png" alt="Saving time">
        <p>We take ownership</p>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
        <img src="<?php echo $this->webroot; ?>css/home/images/icon/money-saved.png" alt="Money taved">
        <p>Professional approach</p>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
        <img src="<?php echo $this->webroot; ?>css/home/images/icon/regular-inspection.png" alt="Regular inspection">
        <p>Speed of transaction</p>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
        <img src="<?php echo $this->webroot; ?>css/home/images/icon/one-stop-solution.png" alt="One stop solution">
        <p>Documentation support</p>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
        <img src="<?php echo $this->webroot; ?>css/home/images/icon/trust-transparency.png" alt="Trust transparency">
        <p>Post transaction support</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="space-l"></div>
</section>
