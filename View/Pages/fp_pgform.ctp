<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "rjQUPktU";

// Merchant Salt as provided by Payu
$SALT = "e5iIg1jwi8";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";

$form_action = '';

// echo "<br><br>"."Action 1 ... ".$form_action ;

$posted = array();
if (!empty($_POST)) {
    print_r($_POST);

    foreach ($_POST as $key => $value) {
        $posted[$key] = $value;
    }
}

// echo "<br><br>"."Action 2 ... ".$form_action ;

$formError = 0;

if (empty($posted['txnid'])) {

    // Generate random transaction id
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {

    $txnid = $posted['txnid'];
}

// echo "<br><br>"."Action 3 ... ".$form_action ;


$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

// echo "<br><br>"."posted 3 ... ".empty($posted['hash']) ;
// echo "<br><br>"."sizeof 3 ... ".sizeof($posted);

if (empty($posted['hash']) && sizeof($posted) > 0) {

//echo "<br><br>"."Action 4... ".$form_action ;

    if (
            empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])
    ) {

// echo "<br><br>"."Action 5... ".$form_action ;


        $formError = 1;
    } else {

// echo "<br><br>"."Action 6... ".$form_action ;
        //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';

        foreach ($hashVarsSeq as $hash_var) {

            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }

        $hash_string .= $SALT;

        $hash = strtolower(hash('sha512', $hash_string));
        $form_action = $PAYU_BASE_URL . '/_payment';

// echo "<br><br>"."Action 7... ".$form_action ;
// echo "<br><br>"."hash  7... ".$hash ;
    }
} elseif (!empty($posted['hash'])) {

    $hash = $posted['hash'];
    $form_action = $PAYU_BASE_URL . '/_payment';

// echo "<br><br>"."Action 8... ".$form_action ;
}

//echo "<br><br>"."Action 9... ".$form_action ;
?>

<style>
    span.multiselect-native-select select{visibility:hidden;display:none;}
</style>  

<script>
    var hash = '<?php echo $hash ?>';
//alert(hash);

    function submitPayuForm() {
        if (hash == '') {
            return;
        }
        var payuForm = document.forms.payuForm;
        payuForm.submit();
    }
</script>


<div class="user_pa_area col-sm-12">
    <div class="container user_inner">
        <div class="duser_menu  sidebar col-sm-12" role="navigation">

            <div id="page-wrapper" class="col-sm-12 user-page-area">
                <div class="row">

                    <div class="col-lg-12">
                        <h1 class="page-header">FP - Payment Form</h1>
                    </div>

                </div>


                <form class="fpform" id="id_form_pg" action="<?php echo $form_action; ?>" method="post" name="payuForm" >

                    <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                    <input type="hidden" name="surl" value="http://staging.fairpockets.com/fppg_success.php" size="64" />
                    <input type="hidden" name="furl" value="http://staging.fairpockets.com/fppg_failure.php" size="64" />

                    <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                    <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

                    <div id="build" class="list-detail detail-block target-block">

                        <h2>Mandatory Parameters</h2>

                        <div class="add-tab-row push-padding-bottom row-color-gray">

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="amount" class="label_title" >Amount <span class="mand_field">*</span></label>
                                        <input type="text" class="form-control box_price allownumericwithoutdecimal" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>">
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="firstname" class="label_title" >First Name <span class="mand_field">*</span></label>
                                        <input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>">
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="email" class="label_title" >Email <span class="mand_field">*</span></label>
                                        <input type="text" class="form-control" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" >
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="phone" class="label_title" >Phone <span class="mand_field">*</span></label>
                                        <input type="text" class="form-control" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" >
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <label for="productinfo" class="label_title" >Product Information <span class="mand_field">*</span></label>
                                        <input type="text" class="form-control" name="productinfo" id="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo']; ?>" >
                                    </div>

                                </div>



                            </div>




                        </div>

                        <h2>Optional Parameters</h2>

                        <div class="add-tab-row push-padding-bottom row-color-gray">

                            <div class="row">								

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="lastname" class="label_title" >Last Name </label>
                                        <input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>">
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="address1" class="label_title" >Address 1 </label>
                                        <input type="text" class="form-control" name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" >
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="address2" class="label_title" >Address 2 </label>
                                        <input type="text" class="form-control" name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" >
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="city" class="label_title" >City </label>
                                        <input type="text" class="form-control" name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" >
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="state" class="label_title" >State </label>
                                        <input type="text" class="form-control" name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" >
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="country" class="label_title" >Country </label>
                                        <input type="text" class="form-control" name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" >
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="zipcode" class="label_title" >Zip Code  </label>
                                        <input type="text" class="form-control" name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" >
                                    </div>

                                </div>

                            </div>


                        </div>


                        <div class="account-block text-center submit_area">
                            <button id="id_submit" class="btn btn-primary btn-red" value="1">Pay Now</button>
                        </div>

                    </div>

                </form>	


            </div>
            <div class="clear"></div>	
        </div>
    </div>
</div>  
<div class="clear"></div>	

<?php
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
?>			  
<script type="text/Javascript">	

    $(document).ready(function(){

    // Onload call the function submitPayuForm
    submitPayuForm();

    //Validation Rules
    var ruleMRequired = {
    required : true
    };

    var messageMRequired = {
    required : 'Field is required.'
    };

    $('#id_form_pg').validate({

    // Specify the validation rules
    rules: {
    'amount': {
    required : true,
    number : true,
    min : 1
    },
    'firstname': ruleMRequired,
    'email': {
    required : true,
    email : true
    },
    'phone': {
    required : true,
    number : true,
    minlength:10,
    maxlength:10
    },
    'productinfo': ruleMRequired,
    'zipcode':  {
    number : true,
    min : 1
    }

    },

    // Specify the validation error messages
    messages: {
    'amount': {
    required : 'Field is required.',
    number : 'Only numeric values allowed.',
    min : '> 0 allowed'
    },
    'firstname': messageMRequired,
    'email': {
    required : 'Field is required.',
    email : 'Please enter a valid email address'
    },
    'phone':  {
    required : 'Field is required.',
    number : 'Only numeric values allowed.',
    minlength: 'Min 10 digits allowed.',
    maxlength: 'Max 10 digits allowed.'
    },
    'productinfo': messageMRequired,
    'zipcode':  {
    number : 'Only numeric values allowed.',
    min : '> 0 allowed'
    }					
    }


    });	


    });

</script>	  







