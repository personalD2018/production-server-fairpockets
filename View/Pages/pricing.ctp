<section class="pricing">
    <div class="space-l"></div>
    <div class="container">
<div class="row">
    <div class="col-md-12 center-align">
        <h1>Buy Our Service</h1>
        <h3>Assisted Search</h3>
        <div class="space-l"></div>
    </div> 

    <div class="buy-services">
        <div class="col-sm-6 buy-services-list">
            <div class="pic"><img src="<?php echo $this->webroot; ?>css/home/images/icon/buy.png" width="100"></div>
            <div class="content"><h4><span>Assistance in buying or renting (Buyer)</span><br>
                    <strong>1% of transaction value</strong> for Selling & 
                    1 month rental for Rent out post transaction</h4></div>
            <div class="clear"></div>
            <ul>
                <li>Appoints Relationship Manager</li>
                <li>Site Visit of Fair Priced properties</li>
                <li>Negotiation and closing transaction</li>
                <li>Agreement</li>
                <li>Post Sale Support</li>
            </ul>
            <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Buy Now</a></div>

        </div>
        <div class="col-sm-6 buy-services-list">
            <div class="pic"><img src="<?php echo $this->webroot; ?>css/home/images/icon/sell.png" width="100"></div>
            <div class="content"><h4><span>Assistance in Selling or renting out (Owner)</span><br>
                    <strong>1% of transaction value</strong> for Selling & 
                    1 month rental for Rent out post transaction</h4></div>
            <div class="clear"></div>
            <ul>
                <li>Appoints Relationship Manager</li>
                <li>List Property on Fair Pockets</li>
                <li>Listing response handled by Relationship Manager</li>
                <li>Negotiation and closing transaction</li>
                <li>Agreement</li>
                <li>Suggestion and support on reinvestment in case of selling</li>
            </ul>
            <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Buy Now</a></div>

        </div>
        <div class="clear"></div>
    </div>
    <div class="space-l"></div>
    <div class="col-lg-12 sub-discription">
        GST will be charged extra  |  The above mentioned prices are for 1 year period  |  All external charges in the entire execution will be borne by the client  |  Bunglow/Villa/Kothi will attract 20% premium charges on the above  |  These are tentative rates, final prices may change post inspection
    </div>
</div>
</div>
<div class="space-l"></div>
</section>
<section class="light-yellow buy-services">
<div class="space-l"></div>
<div class="container">
<div class="row ">
    <div class="col-sm-6 buy-services-list">
        <div class="pic"><img src="<?php echo $this->webroot; ?>css/home/images/icon/about-portfolio-management.png" width="100"></div>
        <div class="content"><h2>Portfolio Management</h2>
            <h4><strong>Rs. 9999/- </strong> (Duration 1 year)</h4></div>
        <div class="clear"></div>
        <ul>
            <li>Appoints Relationship Manager</li>
            <li>Call/Meeting with owner for</li>
            <li>Submit Portfolio Report</li>
            <li>Consultation and decision on next steps</li>
            <li>Client can opt for Assisted search service to execute the decision</li>
        </ul>
        <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Buy Now</a></div>

    </div>

    <div class="col-sm-6 buy-services-list">
        <div class="pic"><img src="<?php echo $this->webroot; ?>css/home/images/icon/about-property-management.png" width="100"></div>
        <div class="content"><h2>Property Management</h2>
            <h4><strong>Starts from 18,000/-</strong><br>
                Request for service (Duration 1 year)</h4></div>
        <div class="clear"></div>
        <ul>
            <li>Inspection of property for acceptance or rejection of request for service</li>
            <li>If request for service accepted then</li>
            <li>Payment request sent to the client</li>
            <li>Detailed inspection to be done post payment and suggest action plan</li>
            <li>Regular visit and cleaning to be done every 3 months in case of vacant apartment</li>
            <li>Tenant management is done for rented property</li>
            <li>Rest as per need</li>
        </ul>
        <div class="link"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Buy Now</a></div>

    </div>

</div> 
</div>
<div class="space-l"></div>
</section>
<section class="buy-services-list">
<div class="space-l"></div>
<div class="container">
<div class="row">
    <div class="col-lg-12 center-align">
        <img src="<?php echo $this->webroot; ?>css/home/images/icon/about-research-report.png">
        <h2>Research Report</h2>
        <h4><strong>Free of Cost</strong></h4>
        <p><strong>Get detailed information on :</strong> Location  |  Builder  |  Project</p>
    </div>
</div>

</div>
<div class="space-l"></div>
</section>

