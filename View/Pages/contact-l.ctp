<section class="contact">
    <div class="space-m"></div>
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Contact Us</h1>
			</div>

			<div class="col-md-6 col-sm-6">
            <p>C66, Sector-2, Noida (UP) 201301<br>
			</p>
		   <p>
		   	<strong>Call:</strong> 0120 - 4126305; +91 9910916878<br>
			<strong>Email:</strong> <a href="mailto:info@fairpocket.com">info@fairpocket.com</a>
			</p>
			<p>&nbsp;</p>
			<h3>Send Us a Message</h3>
			<form>
				<div class="form">
					<div class="form-list">
						<label>Name</label><input type="text" id="Name" name="Name" placeholder="Enter Your Name">
					</div>
					<div class="form-list">
						<label>Email</label><input type="text" id="Email" name="Email" placeholder="Enter Your Email">
					</div>
					<div class="form-list">
						<label>Mobile</label><input type="text" id="Mobile" name="Mobile" placeholder="Enter Your Mobile">
					</div>
					<div class="form-list">
						<label>Subject</label><input type="text" id="City" name="City" placeholder="Enter Your Subject">
					</div>
					 <div class="form-list">
						<label>Message</label><textarea rows="3" placeholder="Enter Your Message"></textarea>
					 </div>
					<div class="form-list">
						<button type="submit" name="Submit" id="resume-submit">Submit</button>
					</div>
				</div>    
		  </form>      
        </div>
    
        <div class="col-md-6 col-sm-6">
			<p>&nbsp;</p>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.529941436997!2d77.31239621456749!3d28.58387469303348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce45f28220c0f%3A0x97a00ad6db545aff!2sC-66%2C+C+Block%2C+Sector+2%2C+Noida%2C+Uttar+Pradesh+201301!5e0!3m2!1sen!2sin!4v1504441842784" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
       	
		</div>
        
		</div>
	</div>
<div class="space-l"></div>
</section>
