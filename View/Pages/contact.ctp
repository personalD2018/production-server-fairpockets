<section class="contact">
    <div class="space-m"></div>
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Contact Us</h1>
			</div>

			<div class="col-md-6 col-sm-6">
            <p>G-27, Sector-3, Noida (UP), 201301<br>
			</p>
		   <p>
		   	<strong>Call:</strong> 0120 - 4126305; +91 9910916878<br>
			<strong>Email:</strong> <a href="mailto:info@fairpockets.com">info@fairpockets.com</a>
			</p>
			 <p>
		   	<strong>Registered Address:</strong> P-506, Sector-21, Noida - 201301<br>
			</p>
			<p>&nbsp;</p>
			<h3>Send Us a Message</h3>

			<?php
                echo $this->Session->flash();
            ?>
			
			<?php echo $this->Form->create('Contact', array(
					'url' => array('controller' => '/', 'action' => 'contact'),
					'inputDefaults' => array(
						'div' => array(
							'class' => 'form-list'
						)
					),
					'type' => 'post',
					'novalidate' => 'novalidate'
				));
			?>
			<form>
				<div class="form">
						<?php 
							echo $this->Form->input('name', array(
									'placeholder' => 'Enter Your Name'
								)
							);
							
							echo $this->Form->input('email', array(
									'placeholder' => 'Enter Your Email'
								)
							);
							
							echo $this->Form->input('mobile', array(
									'placeholder' => 'Enter Your Mobile Number'
								)
							);
							
							echo $this->Form->input('subject', array(
									'placeholder' => 'Subject'
								)
							);
							
							echo $this->Form->input('message', array(
									'placeholder' => 'Enter Your Message',
									'type' => 'textarea',
									'rows' => 3
								)
							);
							
							echo '<div class="form-list">';
							echo $this->Form->button('Submit', array(
								'type' => 'submit'
							));
							echo '</div>';
							
							echo $this->Form->end();
						?>
				</div>    
		  </form>      
        </div>
    
        <div class="col-md-6 col-sm-6">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.529941436997!2d77.31239621456749!3d28.58387469303348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce45f28220c0f%3A0x97a00ad6db545aff!2sC-66%2C+C+Block%2C+Sector+2%2C+Noida%2C+Uttar+Pradesh+201301!5e0!3m2!1sen!2sin!4v1504441842784" height="450" frameborder="0" style="border:0; width:100%;"></iframe>
       	</div>
        </div>
	</div>
<div class="space-l"></div>
</section>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	/*$(document).ready(function() {
			
		var $form = $("form");
		$form.submit(function(e) {
			e.preventDefault();
			$.ajax({
				type:"POST",
				url 	: $(this).attr("action"),
				data 	: $(this).serialize(),
				success: function(response) {
					resetForm();
					//console.log(response);  
				}
			});
		});
		
		function resetForm() {
			$("form").trigger('reset');
		}
	});*/
</script>