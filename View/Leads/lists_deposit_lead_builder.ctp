	<?php $paginator = $this->Paginator;?>
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">List of Leads</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="sales_employees_lists" >
						<div class="input-group custom-search-form">
							<input type="text" name="search" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		
		<div class="space-x"></div>
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Budget</th>
						<th>Source</th>
						<th>Sales Person / Broker</th>
						<th>Dated</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($allleads as $key=>$row):
                    ?>
                    <tr id="row<?php echo $row['AppClientLead']['id']?>">
                        <td><?php echo isset($row['AppClientLead']['name']) ? $row['AppClientLead']['name'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['email']) ? $row['AppClientLead']['email'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['mobile']) ? $row['AppClientLead']['mobile'] : "N/A";?></td>
						<td><?php echo isset($row['AppClientLead']['budget']) ? $row['AppClientLead']['budget'] : "N/A";?></td>
						<td><?php echo isset($row['AppClientLead']['source_lead']) ? $row['AppClientLead']['source_lead'] : "N/A";?></td>
						<td><?php echo $this->Number->getSalesNameBySalesId($row['AppClientLead']['user_id']) ? $this->Number->getSalesNameBySalesId($row['AppClientLead']['user_id']) : "N/A"; ?></td>
                        <td><?php echo isset($row['AppClientLead']['created']) ? $row['AppClientLead']['created'] : "N/A";?></td>
						
						<td style="text-align: center;">
                            <a href="<?php echo Router::url('/leads/editLeadsBuilder/'.$row['AppClientLead']['id'],true);?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a class='ajaxDelete' href="javascript:void(0)" id='<?php echo $row['AppClientLead']['id'];?>' title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>		
				
			</ul>
			
		</div>
		
	</div>
	
		<script type="text/javascript">
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/leads/deleteLeadsBuilder/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
});
</script>
