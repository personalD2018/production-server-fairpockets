<style>.select-style select{	padding:10px 5px !important;}</style>

<div class="page-content">
	<div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
		<h2>Edit Lead</h2>
		<br />
	</div>	

	<div class="add_lead_form">
		<?php
		
		//echo '<pre>'; print_r($this->request->data);
		
		echo $this->Form->create(
				'AppClientLead', array(
			'class' => 'fpform',
			'role' => 'form',
			'id' => 'id_form_deposit_lead'
				)
		);
		?>
		<div class="account-block">
			<div class="add-title-tab">
				<h3>Leads Information</h3>				
			</div>
			<div class="add-tab-content1 detail-block" id="id_proj_pricing_content">
				<div class="add-tab-row push-padding-bottom">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_proj" class="label_title">Name </label>
								</div>
								<div class="col-sm-12">
									<?php
									echo $this->Form->input(
											'name', array(
										'id' => 'name',
										'type' => 'text',
										'class' => 'form-control',
										'label' => false
											)
									);
									?>

								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_desc" class="label_title">Email <span class="mand_field">*</span></label>
								</div>
								<div class="col-sm-12">
									<?php
									echo $this->Form->input(
											'email', array(
										'id' => 'email',
										'type' => 'text',
										'class' => 'form-control required',
										'label' => false
											)
									);
									?>	
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Phone No <span class="mand_field">*</span></label>
								</div>
								<div class="col-sm-12">
									<?php
									echo $this->Form->input(
											'mobile', array(
										'id' => 'mobile',
										'type' => 'text',
										'class' => 'form-control required',
										'label' => false
											)
									);
									?>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Mobile No <span class="mand_field">*</span></label>
								</div>
								<div class="col-sm-12">
									<?php
									echo $this->Form->input(
											'mobile', array(
										'id' => 'mobile',
										'type' => 'text',
										'class' => 'form-control required',
										'label' => false
											)
									);
									?>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Project</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->select(
												'project', 
												$allprojects,
												array('id'=>'project_dropd')
										);
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">City</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->select(
												'city', 
												$allprojects,
												array('id'=>'city_dropd')
										);
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Locality</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->select(
												'locality', 
												$allprojects,
												array('id'=>'loc_dropd')
										);
										?>
									</div>
								</div>
							</div>
						</div>
						<br>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Budget</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php //echo $this->Form->input('property_type_id', array('id' => 'property_type_val', 'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder Floor', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'), 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>
									
										<?php
										echo $this->Form->input(
												'budget',
												array('id'=>'project_dropd',
												'options'=>array(
												'Below 25 lakh'=>'Below 25 lakh',
												'25-50 lakh'=>'25-50 lakh',
												'50-75 lakh'=>'25-50 lakh',
												'75-100 lakh'=>'25-50 lakh',
												'100-150 lakh'=>'25-50 lakh',
												'150-200 lakh'=>'25-50 lakh',
												'200-300 lakh'=>'25-50 lakh',
												'3cr-5cr'=>'3cr-5cr',
												'5cr-10cr'=>'5cr-10cr',
												'10cr-15cr'=>'10cr-15cr',
												'15cr-25cr'=>'15cr-25cr',
												'25cr-50cr'=>'25cr-50cr'),
												'empty' => 'Select',
												'title' => 'Please Select Budget',
												'class' => 'selectpicker bs-select-hidden',
												'label' => false)
										);
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Source Of Lead</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->input(
												'source_lead',
												array('id'=>'source_lead',
												'options'=>array(
												'source1'=>'source1',
												'source2'=>'source2'),
												'empty' => 'Select',
												'title' => 'Please Select Source Of Lead',
												'class' => 'selectpicker bs-select-hidden',
												'label' => false)
										);
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Property Type</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->input(
												'property_type',
												array('id'=>'property_type',
												'options'=>array(
												'Residential'=>'Residential',
												'Commercial'=>'Commercial'),
												'empty' => 'Select',
												'title' => 'Please Select Property Type',
												'class' => 'selectpicker bs-select-hidden',
												'label' => false)
										);
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Unit Type</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->input(
												'unit_type',
												array('id'=>'unit_type',
												'options'=>array(
												'2bhk'=>'2bhk',
												'3bhk'=>'3bhk',
												'Retail'=>'retails',
												'Office'=>'office'),
												'empty' => 'Select',
												'title' => 'Please Select Unit Type',
												'class' => 'selectpicker bs-select-hidden',
												'label' => false)
										);
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<label for="prop_addr" class="label_title">Remarks</label>
								</div>
								<div class="col-sm-12">
									<div class="select-style">
										<?php
										echo $this->Form->input(
												'remarks',
												array('id'=>'remarks',
												'type' => 'textarea',
												'title' => 'Please Select Property Type',
												'class' => 'selectpicker bs-select-hidden',
												'label' => false)
										);
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</div>			
				</div>
			</div>
		</div>
		<?php
		echo $this->Form->end();
		?>
	</div>		
</div> 
<?php
	echo $this->Html->script('add_more_app');
	echo $this->Html->script('add_property_7');
	echo $this->Html->script('front/bootstrap-datetimepicker');

	// Jquery Script for Validation of form fields    
	echo $this->Html->script('validate_1.9_jquery.validate.min');
	echo $this->Html->script('front/additional-methods.min');
	//echo $this->Html->script('fp-loc8.js');
	
	echo $this->Html->scriptBlock("
	
		var rulesName = {
					required : true
		};
		var rulesEmail = {
					required : true,
					email : true
		};
		var rulesMobile = {
					required : true,
					minlength: 10,
					maxlength: 10,
		};
		var messageMTarea = {
					required : 'Mobile should be 10 digit'
		};
		
		$('#id_form_deposit_lead').validate({
			
			// Specify the validation rules
			rules: {
				'data[SalesUser][name]': rulesName,
				'data[SalesUser][email]': rulesEmail,
				'data[SalesUser][mobile]': rulesMobile
				
			},
			
			// Specify the validation error messages
			messages: {				
				'data[SalesUser][mobile]': messageMTarea				
			}
		});	
	");

?>


