<div class="team inside-pages">
    <div class="col-lg-12"><h1 style="margin-bottom:40px;">Our Team</h1></div>

    <div class="row row-padding">
        <div class="col-lg-12">
            <img src="<?php echo $this->webroot; ?>img/team/Ritesh Anand.png">
            <h3>Ritesh Anand - <span>Founder & CEO</span></h3>
            <p>Ritesh has 16 years of cross functional experience in Sales, Strategy, Product and Heading Business - in Internet, Media, Consulting and Software industries. He worked with organizations like Radio Mirchi, Monster, SHL consulting, TBSL and Times Internet before starting Fairpockets. He is also passionate about teaching and has been a visiting faculty at IIM Raipur & IMT Ghaziabad. Ritesh is a Co-founder and Director of India Canada Alumni Network (ICAN), an organization that represents Canadian Alumni residing in India. Ritesh has been a long time real estate investor in multiple cities - having executed residential/commercial/land/JV with developers and related transactions. He completed his MBA from Ivey Business School, Canada.</p>
        </div>
    </div>    

     <div class="row row-padding">
        <div class="col-lg-12">
            <img src="<?php echo $this->webroot; ?>img/team/Rumki Sengupta.png">
            <h3>Rumki Sengupta - <span>Co-founder & Director Operations</span></h3>
            <p>Rumki has around 10 years experience in Human resource and Operations. She has worked with companies like Patni Computers, L&T Infotech & Octopus Retail. She has completed her MBA from Pune University & Sunstone Business School. She is also a professional artist and has participated in many group shows.</p>
        </div>
    </div>
	
	<div class="row row-padding">
        <div class="col-lg-12">
            <img src="<?php echo $this->webroot; ?>img/team/final.gif" style="width:104px;">
            <h3>R.Sundar - <span>Advisor</span></h3>
	    <p>
		Mr. Sundar has over 30 years of experience primarily in Times Group. He was Director Corporate,
		Response and Brand in the print division of Times Group.  He was also the founding member and CEO of TBSL and created brands like Magicbricks,  TimesJobs, Simplymarry etc.
currently, he serves as an Advisor to - Global School Foundation, Singapore, iBidmyhome.com, Prosightpartners ( An Advisory Firm ), Faircent.com. An active early stage Investor and Mentor to a few startups.
		</p>
        </div>
    </div>
	
	<div class="row row-padding">
        <div class="col-lg-12">
            <img src="<?php echo $this->webroot; ?>img/team/kunal benerji.jpg" style="width:104px height:104px;">
            <h3>KUNAL BANERJI - MCAM (UK) - <span>Advisor</span></h3>
	    <p>
		Dr. Banerji brings a vast corpus of knowledge and experience of over thirty years in marketing and sales strategies across various multinational organizations. He has worked with brands all across the world as a Senior Director at the WPP-Ogilvy group with clients such as Rolex , Seagram , Johnson & Johnson , Beecham, Mercedes-Benz , Piaget, Baume & Mercier, Siemens, Hewlett-Packard and Nestle to name a few. <br>
He has worked as CMO & President of companies such as M3M, Ansal API, Unitech .<br>
Forever a student, he has undertaken various courses at the Harvard Business School, Boston and the Judge Business School. He also has had the opportunity to lecture at numerous institutes including the Kellogg Business School at Illinois and Amity University in India. 


		</p>
        </div>
    </div>


</div> 


