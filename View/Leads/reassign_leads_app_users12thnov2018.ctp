<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Share Leads To App Users</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('ShareLead', array(
			'url' => '/leads/reassign_leads_app_users_assign/',
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		
	?>
	
	<input type="hidden" name="broker_id" value="broker_id">
	<input type="hidden" name="builder_id" value="builder_id">
	
	<input type="hidden" name="leadsjson" value='<?php echo $leadsjson; ?>'>
    <div class="row">
	
	<div class="col-md-12">
            <table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($BuilderSalesList)) {
                        for ($i = 0; $i < count($BuilderSalesList); $i++) {
                    ?>
                    <tr>
                        <td><?php echo $BuilderSalesList[$i]['sales_users']['name']; ?></td>
                        <td><?php echo $BuilderSalesList[$i]['sales_users']['email']; ?></td>
                        <td><?php echo $BuilderSalesList[$i]['sales_users']['mobile']; ?></td>
						<td style="text-align: center;">
							<input type="checkbox" name="check_list[]" id="shareinv<?php echo $i; ?>.sales_check" class="checkbox-custom" value="<?php echo $BuilderSalesList[$i]['sales_users']['id']; ?>">
							<label for="shareinv<?php echo $i; ?>.sales_check" class="checkbox-custom-label"></label>    
						</td>
                    </tr>
					<?php }}else{ ?>
					<tr>
                        <td colspan="4" style="text-align:center;">
							No User available, please add sales employees. <a href="/accounts/add_sales_employees1">Click Here</a>
						</td>
					</tr>
					<?php } ?>
                </tbody>
            </table>
        </div>
           
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>