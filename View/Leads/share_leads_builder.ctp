	<style>
	*, *:before, *:after {
  	-webkit-box-sizing:border-box;
	  -moz-box-sizing:border-box;
	  box-sizing:border-box;
}

input, select, input[type="checkbox"], input[type="radio"] {
    -webkit-border-radius:0;
    -webkit-appearance:none;
    outline:none;
}
li {
  margin-bottom:1.5em;
  padding-left:2.5em;
  	position:relative;
}

input[type="checkbox"],
input[type="radio"],
li label::before {
	  cursor:pointer;
	  height:40px;
	  left:0;
	  margin-top:-20px;
	  position:absolute;
	  width:40px;
	  top:50%;
}

input[type="checkbox"],
input[type="radio"] {
	  display:inline-block;
	  opacity:0;
	  vertical-align:middle;
}

li label::before {
	  border:2px solid lighten(#2a4a59,10%);
	  border-radius:4px;
	  color:darken(#48CFAD, 20%);
	  content:'';
	  font-size:1.5em;
	  padding:.1em 0 0 .2em;
}

li input.error + label::before {
	  border-color:#f93337;
}

li input[type="checkbox"]:checked + label::before {
  	border-color:darken(#48CFAD, 20%);
	  content:'\2714';
}

li input[type="radio"] + label::before {
	  border-radius:50%;
}

li input[type="radio"]:checked + label::before {
  	border-color:darken(#48CFAD, 20%);
  	content:'\25CF';
  	font-size:1.5em;
  	padding:0 0 0 .3em;
}

ul {
  	margin-bottom:1em;
  padding-top:1em;
  	overflow:hidden;
}

li label {
	  display:inline-block;
	  vertical-align:top;
}

.js-errors {
    background:#d62540;
    border-radius:8px;
    color:#FFF;
    font-size:.9em;
    list-style-type:none;
    margin-bottom:1em;
    padding:1em;
	font-weight:bold;
}

.js-errors {
	  display:none;
}

.js-errors li {
	  margin-left:1em;
  margin-bottom:.5em;
  padding-left:0;
}

	</style>
	
	<?php $paginator = $this->Paginator;?>
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">List of Leads</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="sales_employees_lists" >
						<div class="input-group custom-search-form">
							<input type="text" name="search" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		
		<div class="space-x"></div>
		<ul class="js-errors"></ul>
		
		<?php
			echo $this->Form->create('ShareLeads', array(
				'url' => '/leads/share_leads_app_users/',
				'id' => 'frm_inventory_first', 
				'name'=>'frm_inventory_first',
				'method'=>'POST'
			));
			
		?>
		
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Budget</th>
						<th>Source</th>
						<th>Dated</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
					<?php if (!empty($allleads)) {
                        for ($i = 0; $i < count($allleads); $i++) {
                    ?>
                    <tr id="row<?php echo $allleads[$i]['AppClientLead']['id']?>">
                        <td><?php echo isset($allleads[$i]['AppClientLead']['name']) ? $allleads[$i]['AppClientLead']['name'] : "N/A";?></td>
                        <td><?php echo isset($allleads[$i]['AppClientLead']['email']) ? $allleads[$i]['AppClientLead']['email'] : "N/A";?></td>
                        <td><?php echo isset($allleads[$i]['AppClientLead']['mobile']) ? $allleads[$i]['AppClientLead']['mobile'] : "N/A";?></td>
						<td><?php echo isset($allleads[$i]['AppClientLead']['budget']) ? $allleads[$i]['AppClientLead']['budget'] : "N/A";?></td>
						<td><?php echo isset($allleads[$i]['AppClientLead']['source_lead']) ? $allleads[$i]['AppClientLead']['source_lead'] : "N/A";?></td>
						<td><?php echo isset($allleads[$i]['AppClientLead']['created']) ? $allleads[$i]['AppClientLead']['created'] : "N/A";?></td>
						
						<td style="text-align: center;">
							<input type="checkbox" name="check_list[]" id="shareleads<?php echo $i; ?>.leads_check" class="checkbox-custom" value="<?php echo $allleads[$i]['AppClientLead']['id']; ?>">
							<label for="shareleads<?php echo $i; ?>.leads_check" class="checkbox-custom-label"></label>    
						</td>
                    </tr>
					<?php }} ?>
                </tbody>
            </table>
			
			
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<div class="col-sm-3">&nbsp;</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-primary" name="submitaction" value="sales">Share With Sales Users</button>
						</div>						
						<div class="col-sm-3">
							<button type="submit" class="btn btn-primary" name="submitaction" value="broker">Share With Brokers</button>
						</div>
						<div class="col-sm-3">&nbsp;</div>
					</div>
				</div>
			</div>
			
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>		
				
			</ul>
			
		</div>
		
	</div>
	<?php
		echo $this->Html->script('validate_1.9_jquery.validate.min');
		echo $this->Html->script('front/additional-methods.min');
	?>
		<script type="text/javascript">
		
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/leads/deleteLeadsBuilder/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
	
	
	
	
	 /*$('#frm_inventory_first').validate({ // initialize the plugin
        rules: {
            'check_list[]': {
                required: true,
                maxlength: 2
            }
        },
        messages: {
            'check_list[]': {
                required: "You must check at least 1 box",
                maxlength: "Check no more than {0} boxes"
            }
        }
    });*/
	
	
	
		$("#frm_inventory_first").validate({
        rules: {
          "check_list[]": {
            required: true,
            minlength: 1
          }
        },

        // FIX
        // Using highlight and unhighlight options we can add the error class to the parent ul which can then be selected and styled
        			highlight: function(element, errorClass, validClass) {
        				  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
        				  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
        			},
        			unhighlight: function(element, errorClass, validClass) {
          				$(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
          				$(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
			        },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",

        messages: {
          "check_list[]": "Please select at least one checkbox"
        },
      });
	
	
	
	
	
});
</script>
