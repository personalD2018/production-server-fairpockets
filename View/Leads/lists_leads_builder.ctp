<?php		
	echo $this->Html->script('../facebox/facebox');
	echo $this->Html->css('../facebox/facebox'); 
?>
<div class="main_content">
	<div class="right_content">
		<center><?php echo $this->Session->flash(); ?></center>
		<div class="columnWidth100 left">
			<div class="columnWidth50 left">
				<h2>Share Lead List</h2> 
			</div>
		</div>
		<div class="row">
			<?php echo $this->Form->create("AppClientLead",array("controller"=>"leads","action"=>"/listsLeadsBuilder","method"=>"Post"));	?>	
				<div class="col-md-3">
					<?php echo $this->Form->input('AppClientLead.keyword',array('maxlength'=>'50','size'=>'20','label'=>false,'div'=>false,'class'=>'form-control','placeholder'=>'Enter search keyword...'))?>
				</div>
				
				
				<div class="col-md-2">
					<div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                        <?php
                                        echo $this->Form->input(
                                                'AppClientLead.date_from', array(
                                            'id' => 'keyword1',
                                            'class' => 'form-control enddate',
                                            'size' => '16',
                                            'value' => '',
                                            'readonly' => 'readonly',
                                            'style' => 'cursor : pointer',
                                            'type' => 'text',
											'placeholder' => 'Date From',
                                            'label' => false
                                                )
                                        ); 
                                        ?>
                                        <span class="add-on calicon"><i class="icon-th"></i></span>
                                    </div>
				</div>
				<div class="col-md-2">
					<div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					
                                        <?php
                                        echo $this->Form->input(
                                                'AppClientLead.date_to', array(
                                            'id' => 'keyword2',
                                            'class' => 'form-control enddate',
                                            'size' => '16',
                                            'value' => '',
                                            'readonly' => 'readonly',
                                            'style' => 'cursor : pointer',
                                            'type' => 'text',
											'placeholder' => 'Date To',
                                            'label' => false
                                                )
                                        ); 
                                        ?>
                                        <span class="add-on calicon"><i class="icon-th"></i></span>
                                    </div>
				</div>
				<div class="col-md-3">
					<div class="select-style">
						<?php $optArr = array(''=>'--Search For--','name'=>'Client Name','city'=>'City','locality'=>'Locality','leads_created_by'=>'Leads From'); ?>	
						<?php echo $this->Form->select('AppClientLead.scriteria',$optArr,array('id'=>'ClientScriteria','default'=>'fgdf','empty'=>false,'div'=>false,'class'=>'searchControlSelect'),false); ?>
					</div>
				</div>
				<div class="col-md-2">
					<?php 
					//$searchImage = _HTTP_PATH.'img/images/search.png';
					echo $this->Js->submit('Search', array('url'=> array('controller'=>'leads','action'=>'listsLeadsBuilder'),'class'=>'btn btn-default btn-yellow', 'update' => '#clientLists', 
					'evalScripts' => true,
					'before' => $this->Js->get('#loaderID')->effect('show', array('buffer' => false)),
					'complete' => $this->Js->get('#loaderID')->effect('hide', array('buffer' =>   false))));				
					?>
				</div>
			<?php echo $this->Form->end(); ?>
		</div>
		<div id='clientLists' class="left columnWidth100">
			<?php echo $this->element("leads/lists_leads_builder_row"); ?>
		</div>		       
	</div>
	<!-- end of right content-->
	<div class="clear"></div>
</div> <!--end of main content-->


<?php
echo $this->Html->script('front/bootstrap-datetimepicker');

echo $this->Html->scriptBlock("
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1);
		// Handling Date input from calendar
		$('#keyword1,#keyword2').datetimepicker({
			format: 'yyyy-mm-dd',
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});

	");

// Script  : To set validation rules for FORMS

?>

<?php echo $this->Js->writeBuffer(); ?>