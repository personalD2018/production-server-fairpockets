<!--./advertisement-Card-->
    <div class="page-content">

        <?php
			$paginator = $this->Paginator;
		?>
     <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Property Management - My Properties</h1>
                </div>
                 <div class="col-md-3">
                    <div class="user_panel_search">
                            <form class="search" id="search" method="post"  action="Property_ViewProperty" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                           </form>
                       </div>
                   </div>
            </div>
			<div class="space-x"></div>
<!--
            <div class="user_panel_search">
                <form class="search" id="search" method="post" action="Property_ViewProperty">
                    <input type="text" name="search" placeholder="Search..">
                    <a onclick="document.getElementById('search').submit()" ><i class="fa fa-search fa-lg" aria-hidden="true" style="color:#000;"></i></a>
                </form>
            </div>-->



            <?php
            if (!empty($allprojdata)) {

                foreach ($allprojdata as $data) {

                    if ($data['Portmgmt']['prop_type'] == '1') {
                        $pro_type = "Residential";
                    } else {
                        $pro_type = "Commercial";
                    }
                    ?>
                    <div id="build" class="list-detail detail-block target-block">

                        <div class="detail-title">
                            <a href="#"><?php echo $pro_type ?> Property in <?php echo $data['Portmgmt']['prop_locality']; ?> ,  <?php echo $data['MstAreaCity']['cityName']; ?></a>
                        </div>



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" > <strong> Area : </strong> <?php echo $data['Portmgmt']['prop_area']; ?> sq feet </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Property Type Option : </strong><?php echo $data['MstPropertyTypeOption']['option_name']; ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Locality: </strong><?php echo $data['Portmgmt']['prop_locality']; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Address : </strong> <?php echo $data['Portmgmt']['prop_addr']; ?> </div>
                            </div>

                        </div>

                        <div class="row">

                            <?php
                            if ($data['Portmgmt']['prop_type'] == '1') {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Configure : </strong><?php echo $data['Portmgmt']['prop_cfg']; ?></div>
                                </div>
                            <?php } ?>		
                        </div>

                        <div class="clear"></div>

                        <div class="block-inner-link">
                            <ul>
                                <li><a href="<?php echo $this->webroot; ?>accounts/property_editProperty/<?php echo $data['Portmgmt']['id']; ?>">Edit</a></li>
                            </ul>
                        </div>

                    </div>

                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">

                    No Property Found

                </div>
            <?php }
            ?>


            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">
                    <?php
//   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
// 'prev' page button, 
// we can check using the paginator hasPrev() method if there's a previous page
// save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

// the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
                        'tag' => 'li',
                        'currentClass' => 'active', 'class' => 'pagi_nu'));

// for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

// the 'last' page button
// echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>	
                </ul>

            </div>

        </div>		

    </div>

