<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Share Inventory To Sales</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('BuilderSalesShareInventory', array(
			'url' => '/accounts/inventory/share_inventory_to_sales/'.$inventory_id,
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		
	?>
	
	<input type="hidden" name="broker_id" value="broker_id">
	<input type="hidden" name="builder_id" value="builder_id">
	
	<input type="hidden" name="projectjson" value="<?php echo $projectjson; ?>">
    <div class="row">
        <div class="col-md-12" style="border: 1px solid #bdbdbd;">
            
            
            
           
           
                        
            <div class="row cell-content">
                <div class="col-md-4"><b>Sales User Name</b></div>
                <div class="col-md-4">
				&nbsp;
				</div>
				<div class="col-md-4">
				<b>Actions</b>
				</div>
			</div>
			<?php //echo '<pre>';print_r($BuilderSalesList);
			if (!empty($BuilderSalesList)) { ?>
			<?php for ($i = 0; $i < count($BuilderSalesList); $i++) { ?>
            <div class="row cell-content">
                <div class="col-md-4"><?php echo $BuilderSalesList[$i]['SalesUser']['name']; ?></div>
                
				<div class="col-md-4">
				<?php echo $this->Form->input('shareinv1.sales_check'.$i, array(
                                    'id' => 'shareinv1.sales_check'.$i,
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?><?php //echo $i; ?>
				<!--<div class="col-sm-12"><input type="hidden" name="data[shareinv<?php echo $i; ?>][sales_check<?php echo $i; ?>]" id="shareinv<?php echo $i; ?>.sales_check<?php echo $i; ?>" value="0">
				<input type="checkbox" name="data[shareinv<?php echo $i; ?>][sales_check<?php echo $i; ?>]" id="shareinv1.sales_check<?php echo $i; ?>" class="checkbox-custom" value="1">
				<label for="shareinv<?php echo $i; ?>.sales_check<?php echo $i; ?>" class="checkbox-custom-label"></label></div>-->
				</div>
            </div>
			<?php } }?>
            
           
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>