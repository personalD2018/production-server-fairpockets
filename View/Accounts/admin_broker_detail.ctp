<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<?php
//echo $this->Html->css('home/report_detail');
//echo "hello view";die();
$unit = json_decode($Reasearchreport['Broker']['unit'], true);
?>
<div class="container">
	<div class="space-l"></div>
<div class="research_details inside-pages">
    <h1><?php echo (!empty($Reasearchreport['Broker']['report_name'])) ? $Reasearchreport['Broker']['report_name'] : 'Broker Details'; ?></h1>
    <div class="row project-details">
		
		   <div class="col-md-6">
				<div class="propert-details">
					<ul class="right">
					 <li class="color"><strong>Broker Name:</strong> <?php echo $Reasearchreport['Broker']['broker_name']; ?></li>
					<li><strong>Company Name:</strong><?php echo $Reasearchreport['Broker']['company_name']; ?></li>
					<li class="color"><strong>Mobile First:</strong> <?php echo $Reasearchreport['Broker']['mobile_first']; ?></li>
					<li><strong>Mobile Second:</strong> <?php echo $Reasearchreport['Broker']['mobile_second']; ?></li>
					<li class="color"><strong>City:</strong><?php echo $Reasearchreport['Broker']['city']; ?></li>
					<li class="color"><strong>Locality:</strong><?php echo $Reasearchreport['Broker']['locality']; ?></li>
					<li class="color"><strong>Project:</strong><?php echo $Reasearchreport['Broker']['project']; ?></li>
					<li><strong>Address:</strong> <?php echo $Reasearchreport['Broker']['address']; ?> sq. ft.</li>
				   </ul>
				</div>
	    	</div>
			
			<div class="col-md-6">
			 <div class="box">
                <h2>Download verifed Report</h2>
                <p>Lorem Ipsum is simply dummy text of the simply dummy text of the printing and typesetting industry.</p>
                <ul>
                    <?php
                    if ($Reasearchreport['Broker']['report_price'] == 0) {
                        ?>
                        <li><a target="_blank" href="<?php echo $this->webroot . 'upload/reportpdf/' . $Reasearchreport['Broker']['reportpdf']; ?>" class="buy-now">Download now</a></li>
                        <?php
                    } else {
                        ?>
                        <li><a href="#" class="buy-now">Buy now</a></li>
<?php } ?>	
                    <li><a href="<?php echo $this->webroot .'contact'?>" class="contact">Contact Fairpockets</a></li>
                </ul>
                <ul>
                    <a href="<?php echo $this->webroot; ?>samplereport.pdf" class="sample-report">View Sample Report</a>
                </ul>
            </div>
           
        </div>
			
			</div>
	<div class="space-m"></div>

   

  


</div>
<div class="space-l"></div>
<div class="row light-yellow text-center">
    <div class="col-md-12">
		<div class="space-x"></div>
        <h2>About Broker</h2>
        <p><?php echo $Reasearchreport['Broker']['broker_name']; ?>.</p>
    </div>    
</div>
<div class="space-l"></div>
</div>
<!-- To display gmap -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<script type="text/javascript">

    var map;
    var lat = parseFloat(document.getElementById("id_gLat").value);
    var lng = parseFloat(document.getElementById("id_gLng").value);
    var tmp = document.getElementById("id_gTitle").value;

    var infowindow = new google.maps.InfoWindow({});

    function initialize()
    {
        geocoder = new google.maps.Geocoder();

        var latlng = new google.maps.LatLng(lat, lng);

        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Property Location"
        });

        marker['infowindow'] = tmp;

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
        });

    }

    window.onload = initialize;

</script>



