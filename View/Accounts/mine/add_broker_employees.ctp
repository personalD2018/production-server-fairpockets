<style>
.select-style select{
	padding:10px 5px !important;
}
</style>
<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Add Master Broker</h2>
            <br />
        </div>	

        <div class="add_property_form">

            

            <?php
            echo $this->Form->create(
                    'ProjectPricing', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Master Broker Information</h3>
                    <div class="add-expand" id="id_proj_pricing_exp"></div>
                </div>

                <div class="add-tab-content1 detail-block" id="id_proj_pricing_content" >
					
					
					
					<div class="add-tab-row push-padding-bottom">
						<?php
							echo    $this->Form->create('SalesUser', array(
										'class' => 'fpform',
										'role' => 'form',
										'id' => 'id_form_PortMgmtAdd',
										'novalidate' => 'novalidate'
									));
							?>

						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Name <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'name', array(
                                            'id' => 'name',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Company Name <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'company', array(
                                            'id' => 'company',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
						
						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Email <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'email', array(
                                            'id' => 'email',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Mobile <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'mobile', array(
                                            'id' => 'mobile',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
						
                        <div class="row" style="display:none;">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">City <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'city', array(
                                            'id' => 'city',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>                          

                        </div>							

                        <br>

                        <h3>Add Projects</h3>
                        
						<input type="hidden" id="id_cloneProjectBspChargeCount" value="10" >
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab" >
							<div class="row cloneProjectBspCharge" id="cloneProjectBspCharge1" >
								<div class="col-sm-3">
                                    <div class="form-group"> 
                                        <label for="property-price-before" class="label_title">Projects</label>
                                        <div class="select-style">
                                            <?php
												echo $this->Form->input(
													'bspcharges.from_project1', array(
													'id' => 'from_project1',
													'options' => $getProjects,
													'class' => 'selectpicker bs-select-hidden required',
													'label' => false,
													'empty' => 'Select Project'
														)
												);
                                            ?>

                                        </div>
                                    </div>
                                </div>
								
								<div class="col-sm-4">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Maximum Discount Allowed</label>
                                        <?php
											echo $this->Form->input(
												'bspcharges.proj_disc1', array(
												'id' => 'proj_disc1',
												'type' => 'text',
												'class' => 'form-control box_price required',
												'label' => false,
												'value' => ''
													)
											);
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-4">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Discount Unit</label>
                                        <div class="select-style">
										<?php

                                            //$bspFromToOptions=array('percentage'=>'');

                                            echo $this->Form->input(
                                                    'bspcharges.to_floor1', array(
                                                'id' => 'to_floor1',
                                                'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage',
                                                        '3' => 'Per Sq feet'
                                                    ),
                                                'class' => 'selectpicker bs-select-hidden required',
                                                'label' => false,
                                                'empty' => 'Select Unit'
                                                    )
                                            );
                                            ?>
											</div>
                                    </div>
                                </div>
								
								<div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
								
							</div>
												
							
						</div>
						<div class="row" style="padding-bottom:14px;">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRow" onclick="cloneProjectBspChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
						<br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>
						<?php
							echo $this->Form->end();
						?>
						
                    </div>
					
					
										
                    							

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>

        </div>		
    </div> 
<?php
echo $this->Html->script('add_more_app');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');
?>

<script>
	
	$(function(){		
		$('#id_form_project_pricing').validate({
			rules: {
				'data[Websiteuser][username]': {
					required : true
				},
				'data[Websiteuser][userorgname]': {
					required : true,
					minlength : 5
				},
				'data[SalesUser][email]': {
					required : true,
					email : true
					//remote: "http://localhost:8012/staging/accounts/check_broker_exist/"
				},
				'data[Websiteuser][usermobile]': {
					required : true,
					number : true,
					mobileCustom : true,
					minlength : 10,
					maxlength : 10,
					remote: "http://localhost:8012/staging/accounts/check_broker_exist/"
				},
				'data[Websiteuser][password]': {
					required : true
				},
				'data[Websiteuser][userrole]': {
					required : true
				}
			},
			messages: {
				'data[Websiteuser][username]': {
					required : 'Name is required.'
				},
				'data[Websiteuser][userorgname]': {
					required : 'Organization Name is required.',
					minlength : 'Min 5 alphabets.'
				},
				'data[SalesUser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address'
					//remote: 'Email id aready exist, please try another email.'
				},
				'data[Websiteuser][usermobile]': {
					required : 'Mobile Number is required.',
					number : ' Only Numeric digits allowed.',
					minlength : 'Min 10 numbers.',
					maxlength : 'Max 10 numbers.',
					remote: 'A user with same mobile number already exist, please try another.'
				},
				'data[Websiteuser][password]': {
					required : 'Password is required.'
				},
				'data[Websiteuser][userrole]': {
					required : 'Member Type is required.'
				}
			},
			submitHandler: function() {
				//sendOTP();
				// UserRegistrationFormSubmit();
				return false;
			}
		});
		
		
		
				
	});	
</script>


