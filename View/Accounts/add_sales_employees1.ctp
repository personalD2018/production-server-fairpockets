<style>
.select-style select{
	padding:10px 5px !important;
}
</style>
<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Add Sales Employees</h2>
            <br />
        </div>	

        <div class="add_property_form">           

            <?php
            echo $this->Form->create(
                    'ProjectPricing', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing'
                    )
            );
            ?>

            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Builder Sales Information</h3>
                    
                </div>

                <div class="add-tab-content1 detail-block" id="id_proj_pricing_content">
					
					
					
					<div class="add-tab-row push-padding-bottom">
						<?php
							echo    $this->Form->create('SalesUser', array(
										'class' => 'fpform',
										'role' => 'form',
										'id' => 'id_form_PortMgmtAdd',
										'novalidate' => 'novalidate'
									));
							?>

						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Name </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'name', array(
                                            'id' => 'name',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Email <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'email', array(
                                            'id' => 'email',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">Mobile No <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'mobile', array(
                                            'id' => 'mobile',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>                          

                        </div>							

                        <br>
						
						<br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>
						<?php
							echo $this->Form->end();
						?>
						
                    </div>
					
					
										
                    							

                </div>

            </div>          	

            <?php
            echo $this->Form->end();
            ?>

        </div>		
    </div> 
<?php
echo $this->Html->script('add_more_app');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');
?>

<?php echo $this->Html->scriptBlock("
	
		var rulesName = {
					required : true
		};
		var rulesEmail = {
					required : true,
					email : true
		};
		var rulesMobile = {
					required : true,
					minlength: 10,
					maxlength: 10,
		};
		var messageMTarea = {
					required : 'Mobile should be 10 digit'
		};
		
		$('#id_form_project_pricing').validate({
			
			// Specify the validation rules
			rules: {
				'data[SalesUser][name]': rulesName,
				'data[SalesUser][email]': rulesEmail,
				'data[SalesUser][mobile]': rulesMobile
				
			},
			
			// Specify the validation error messages
			messages: {				
				'data[SalesUser][mobile]': messageMTarea				
			}
		});	
	");

?>


