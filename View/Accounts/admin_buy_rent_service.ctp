<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Buy/Rent Services</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Buy/Rent Services</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Requirement</th>
                                <th class="center">Property Type </th>
                                <th class="center">Completion status</th>
                                <th class="center">Purpose</th>
                                <th class="center">City</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($allprojdata as $data) {
                                if ($data['BuyRentRequirement']['prop_type'] == '1') {
                                    $pro_type = "Residential";
                                } else {
                                    $pro_type = "Commercial";
                                }
                                if ($data['BuyRentRequirement']['prop_reqt'] == '1') {
                                    $prop_reqt = "Rent";
                                } else {
                                    $prop_reqt = "Buy";
                                }
                                ?>
                                <tr>
                                    <td class="center"><?php echo $pro_type; ?></td>
                                    <td class="center"><?php echo $prop_reqt; ?></td>
                                    <td class="center"><?php
                                        if ($data['BuyRentRequirement']['prop_cstat'] == '1') {
                                            echo $prop_reqt = "Ready to move in";
                                        } else {
                                            echo $prop_reqt = "Under Construction";
                                        }
                                        ?></td>

                                    <td class="center"><?php
                                        if ($data['BuyRentRequirement']['prop_purpose'] == '1') {
                                            echo $prop_purpose = "End use";
                                        } else {
                                            echo $prop_purpose = "Investment";
                                        }
                                        ?></td>
                                    <td class="center"><?php
                                        echo isset($data['MstAreaCity']) ? $data['MstAreaCity']['cityName'] : '';
                                        ?></td>


                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a target="_blank" href="<?php echo $this->webroot; ?>accounts/AsstBuyRent_EditRequirement/<?php echo $data['BuyRentRequirement']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/viewBuyrent/<?php echo $data['BuyRentRequirement']['id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail"><i class="icon-eye-open"></i></button></a>


                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


