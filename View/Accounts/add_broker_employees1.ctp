<style>
.select-style select{
	padding:10px 5px !important;
}
</style>
<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Add Master Broker</h2>
            <br />
        </div>
        <div class="add_property_form">
            <?php
            echo $this->Form->create(
                    'ProjectPricing', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing',
                'novalidate' => 'novalidate'
                    )
            );
            ?>
            <div class="account-block project_pricing">
                <div class="add-title-tab">
                    <h3>Master Broker Information</h3>
                    
                </div>
                <div class="add-tab-content1 detail-block" id="id_proj_pricing_content" >
					<div class="add-tab-row push-padding-bottom">
						<?php
							echo    $this->Form->create('SalesUser', array(
										'class' => 'fpform',
										'role' => 'form',
										'id' => 'id_form_PortMgmtAdd',
										'novalidate' => 'novalidate'
									));
							?>

						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Name <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'name', array(
                                            'id' => 'name',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Company Name <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'company', array(
                                            'id' => 'company',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
						
						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Email <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'email', array(
                                            'id' => 'email',
                                            'type' => 'text',
                                            'class' => 'emailvalue form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Mobile <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'mobile', array(
                                            'id' => 'mobile',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
						
                        <div class="row" style="display:none;">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">City <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'city', array(
                                            'id' => 'city',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>                          

                        </div>							

                        <br>

                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>
						<?php
							echo $this->Form->end();
						?>						
                    </div>
                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>

        </div>		
    </div> 
<?php
echo $this->Html->script('add_more_app');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');
?>

<script>	
	$(function(){	
	//var mailsok = 123;
		//$(".emailvalue").change(function(){
		//var	mailsokq = '';
        //var mailsokq = $(".emailvalue").val();
    //});
	//var mailsok = mailsokq;
		$('#id_form_project_pricing').validate({
			rules: {
				'data[SalesUser][email]': {
					required : true,
					email : true,
					remote: "https://www.fairpockets.com/accounts/check_broker_exist/"
				},
				'data[SalesUser][mobile]': {
					required : true,
					number : true,
					//mobileCustom : true,
					minlength : 10,
					maxlength : 10
					//remote: "http://localhost:8012/staging/accounts/check_broker_exist/"
				}
			},
			messages: {
				'data[SalesUser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address',
					//remote : jQuery.validator.format("{0}")
					remote: jQuery.validator.format('This Broker already exist in our Database.<a href="https://www.fairpockets.com/accounts/clone_broker_employees/{0}">Do You want add this broker in own network.</a>')
				},
				'data[SalesUser][mobile]': {
					required : 'Mobile Number is required.',
					number : ' Only Numeric digits allowed.',
					minlength : 'Min 10 numbers.',
					maxlength : 'Max 10 numbers.'
					//remote: 'A user with same mobile number already exist, please try another.'
				}
			}
			/*submitHandler: function() {
				//sendOTP();
				// UserRegistrationFormSubmit();
				return false;
			}*/
		});
		
		
		
				
	});	
</script>


