	<?php $paginator = $this->Paginator;?>
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">List of Projects</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="share_projects_inventory_view" >
						<div class="input-group custom-search-form">
							<input type="text" name="search" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Locality</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Regd. Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (is_array($projects) && count($projects) > 0) {
                            foreach($projects as $row) {
                    ?>
                    <tr>
					 <?php 
					 //echo $builder_id;
					 /*$status = $this->Number->shareProjectInvStatusProjectId($row['InvProject']['project_id'],$builder_id);
						if($status == 'add'){
							$link = 'share-project-info';
						}
						if($status == 'update'){
							$link = 'share-project-info-update';
						}*/
					 ?>
                        <td><?php echo isset($row['Project']['project_name']) ? $row['Project']['project_name'] : 'n/a';?></td>
                        <td><?php echo isset($row['Project']['locality']) ? $row['Project']['locality'] : 'n/a';?></td>
                        <td><?php echo isset($row['Project']['city_data']) ? $row['Project']['city_data'] : 'n/a';?></td>
                        <td><?php echo isset($row['Project']['state_data']) ? $row['Project']['state_data'] : 'n/a';?></td>
                        <td><?php echo isset($row['Project']['created_date']) ? date("d/m/Y", strtotime($row['Project']['created_date'])) : 'n/a';?></td>
                        <td style="text-align: center;"><a href="<?php echo $this->Html->url('share_projects_inventory/' . $row['Project']['project_id']);?>" style="font-size:16px;" title="Edit"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>
                    <?php
                            }
                        }
                    ?>                    
                </tbody>
            </table>
		<div class="clear"></div>		
		<div class="panel-pagination col-sm-12">			
			<ul class="inner_pagi">				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>
			</ul>			
		</div>		
	</div>
	
	<script type="text/javascript">
   /* jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });*/
</script>