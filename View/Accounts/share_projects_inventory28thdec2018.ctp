<style>
.select-style select{
	padding:10px 5px !important;
}
</style>
<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2 style="font-size:20px; font-weight:normal;">
			Configure Inventory For 
			<b><?php echo $this->Number->getProjectNameByProjectId($project_id)[0]['Project']['project_name']; ?></b>
			</h2>
            <br />
        </div>	

        <div class="add_property_form">

            

            <?php //echo '<pre>'; print_r($getProjects1);die();
            echo $this->Form->create(
                    'InvProjects', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_sharing',
                'novalidate' => 'novalidate'
                    )
            );
			$project_id = 701;
			//$this->request->data['invprojects'] = $getProjects1;
			//echo '<pre>'; print_r($this->request->data['invprojects']);die();
            ?>
			<input type="hidden" value="<?php echo $project_id; ?>" name="project_id">
            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Information </h3>
                    <div class="add-expand" id="id_proj_pricing_exp"></div>
                </div>

                <div class="add-tab-content1 detail-block" id="id_proj_pricing_content">
					
					
					
					<div class="add-tab-row push-padding-bottom">

                       

                        <h3>Projects</h3>                        
						<input type="hidden" id="id_cloneProjectInventoryCount" value="50" >
						<div class="addrowarea morerow" id="id_cloneProjectInventory_tab">
						<?php
                            if (!empty($this->request->data['invprojects'])) {
                                for ($i = 1; $i <= count($this->request->data['invprojects']) / 5; $i++) {
                                    //print_r($this->request->data['invprojects']);
									?>
						
							<div class="row cloneProjectInventory" id="cloneProjectInventory<?php echo $i; ?>" >																				

								<!--<div class="col-sm-2">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">Projects<br></label>

                                        <div class="select-style">

                                            <?php                                           
                                            /*echo $this->Form->input(
                                                    'invprojects.from_project'.$i, array(
                                                'id' => 'from_project'.$i,
                                                'options' => $getProjects,
                                                'class' => 'selectpicker bs-select-hidden required',
                                                'label' => false,
                                                'empty' => 'Select Project'
                                                    )
                                            );*/
                                            ?>

                                        </div>
                                    </div>
                                </div>-->
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Tower Name</label>
                                        <?php
                                        echo $this->Form->input(
                                                'invprojects.tower'.$i, array(
                                            'id' => 'tower'.$i,
                                            'type' => 'text',
                                            'class' => 'form-control box_price required',
                                            'label' => false
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Wing(If Any)</label>
                                        <?php
                                        echo $this->Form->input(
                                                'invprojects.wing'.$i, array(
                                            'id' => 'wing'.$i,
                                            'type' => 'text',
                                            'class' => 'form-control box_price',
                                            'label' => false
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Total Floors</label>
										 <div class="select-style" style="width:83%;">
                                        
												<?php
                                            $total_floor_from=array_combine(range(0,30,1),range(0,30,1));
                                            $total_floor_from[0]='G';

                                            echo $this->Form->input(
                                                    'invprojects.total_floor_from'.$i, array(
                                                'id' => 'total_floor_from'.$i,
                                                'options' => $total_floor_from,
                                                'class' => 'selectpicker bs-select-hidden required',
                                                'label' => false,
                                                'empty' => 'From'
                                                    )
                                            );
                                            ?>
												
												</div>
												

                                    </div>
                                </div>
								
								<div class="col-sm-1">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">&nbsp;</label>
										 
                                        <br><b>
												<?php
                                            echo 'TO';
                                            ?>
												</b>
												
												

                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">&nbsp;</label>
                                        
												<div class="select-style">
												
												<?php
                                            $total_floor_to=array_combine(range(0,30,1),range(0,30,1));
                                            $total_floor_to[0]='G';

                                            echo $this->Form->input(
                                                    'invprojects.total_floor_to'.$i, array(
                                                'id' => 'total_floor_to'.$i,
                                                'options' => $total_floor_to,
                                                'class' => 'selectpicker bs-select-hidden required',
                                                'label' => false,
                                                'empty' => 'TO'
                                                    )
                                            );
                                            ?>
											</div>

                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Flats Per Floor</label>
                                        <?php
                                        echo $this->Form->input(
                                                'invprojects.flats_per_floor'.$i, array(
                                            'id' => 'flats_per_floor'.$i,
                                            'type' => 'text',
                                            'class' => 'form-control box_price required',
                                            'label' => false
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								
								
								
							
								
								<div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectInventoryDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
									
							</div>
								<?php }
							} else { ?>
							
							<div class="row cloneProjectInventory" id="cloneProjectInventory1" >																				

								<!--<div class="col-sm-2">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">Projects<br></label>

                                        <div class="select-style">

                                            <?php
                                            //$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
                                            //$bspFromToOptions[0]='Ground';

                                            /*echo $this->Form->input(
                                                    'invprojects.from_project1', array(
                                                'id' => 'from_project1',
                                                'options' => $getProjects,
                                                'class' => 'selectpicker bs-select-hidden required',
                                                'label' => false,
                                                'empty' => 'Select Project'
                                                    )
                                            );*/
                                            ?>

                                        </div>
                                    </div>
                                </div>-->
								
								<div class="col-sm-3">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Tower Name</label>
                                        <?php
                                        echo $this->Form->input(
                                                'invprojects.tower1', array(
                                            'id' => 'tower1',
                                            'type' => 'text',
                                            'class' => 'form-control box_price required',
                                            'label' => false,
                                            'value' => ''
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-3">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Wing(If Any)</label>
                                        <?php
                                        echo $this->Form->input(
                                                'invprojects.wing1', array(
                                            'id' => 'wing1',
                                            'type' => 'text',
                                            'class' => 'form-control box_price',
                                            'label' => false,
                                            'value' => ''
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Total Floors</label>
										 <div class="select-style" style="width:58%;">
                                        <select name="data[invprojects][total_floor_from1]" id="total_floor_from" class="selectpicker bs-select-hidden">
                                                    <option value="G" selected="selected">G</option>
                                                    <?php for ($i = 1; $i <= 30; $i++) :?>
                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php endfor; ?>
                                                </select>
												
												</div>
												

                                    </div>
                                </div>
								
								<div class="col-sm-1">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">&nbsp;</label>
                                        <div class="select-style">
                                        <select name="data[invprojects][total_floor_to1]" id="total_floor_to" class="selectpicker bs-select-hidden">
                                                    <option value="G" selected="selected">G</option>
                                                    <?php for ($i = 1; $i <= 30; $i++) :?>
                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php endfor; ?>
                                                </select>
												</div>

                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Flats Per Floor</label>
                                        <?php
                                        echo $this->Form->input(
                                                'invprojects.flats_per_floor1', array(
                                            'id' => 'flats_per_floor1',
                                            'type' => 'text',
                                            'class' => 'form-control box_price required',
                                            'label' => false,
                                            'value' => ''
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								
								
								
							
								
								<div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectInventoryDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
									
							</div>
							
							
							<?php } ?>
												
							
						</div>
						<div class="row" style="padding-bottom:14px;">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectInventoryAddRow" onclick="cloneProjectInventoryAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
						
						<br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>
						<?php
							echo $this->Form->end();
						?>
						
                    </div>
					
					
										
                    							

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>

        </div>		
    </div> 
<?php
echo $this->Html->script('share_inv');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');

echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		var rulespTarea = {
					required : true
					//min:1
			
		};
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_project_sharing').validate({
			
			// Specify the validation rules
			rules: {
				'data[invprojects][tower1]': rulesMTarea,
				'data[Portmgmt][prop_locality]': rulesMTarea,
				'data[Portmgmt][prop_desc]': rulesMTarea,
				'data[Portmgmt][prop_area]': rulespTarea
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[invprojects][tower1]': messageMTarea,
				'data[Portmgmt][prop_locality]': messageMTarea,
				'data[Portmgmt][prop_desc]': messageMTarea,
				'data[Portmgmt][prop_area]': messageMTarea
				
			}
		});	
	");

?>



