<script>
    $(document).ready(function () {
		property_option(id=1);
        $("#prop_state").on('change', function () {
            var id = $(this).val();
			
            $("#prop_city").find('option').remove();
			
            if (id) {
                var dataString = 'id=' + id;
				
				$.getJSON('/countries/getpecity/' + id, function(data) {
					$("#prop_city").empty();
					for(var id in data) {
						$("#prop_city").append($('<option>', { 
							value: id,
							text: data[id]
						}));	
					}
					
				});
			}
		});
		
        $(".radio-custom").on('click', function () {
			
            var id = $(this).val();
            if (id == '1')
            {
                $(".hide_config").show();
			} else
            {
                $(".hide_config").hide();
			}
            $("#prop_toption").find('option').remove();
			property_option(id);
		});
		$("#prop_state").trigger('change');
	});
	
	function property_option(id){
		var transaction = $("#transaction_type").val();
		var dataString = 'id=' + id;
		$.ajax({
			type: "POST",
			url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyoption")); ?>',
			data: dataString,
			cache: false,
			success: function (html) {
				$('<option>').val('').text('Select').appendTo($("#prop_toption"));
				$.each(html, function (key, value) {
					$('<option>').val(key).text(value).appendTo($("#prop_toption"));
				});
				if (transaction == '2')
				{
					$("#prop_toption option[value='3']").hide();
					$("#property_type_val option[value='27']").hide();
				}
				if (transaction == '1')
				{
					$("#prop_toption option[value='3']").show();
					$("#prop_toption option[value='27']").show();
				}
			}
		}).responseJSON;
	}
</script>
<!--./advertisement-Card-->
<div class="page-content">
	
	<h1 class="page-header"> Portfolio Management - Add Property </h1>
	
	<div id="build" class="list-detail detail-block target-block">
		
		<div class="block-inner-link">
			
			<?php
				echo $this->Form->create(
				'Portfolio', array(
				'class' => 'fpform',
				'role' => 'form',
				'id' => 'id_form_PortMgmtAdd',
				'novalidate' => 'novalidate'
				)
				);
			?>
			
			
			<div class="account-block">
				
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_type"  class="label_title">Property Type </label>
							</div>
							
							<div class="col-sm-12">
								<?php
									$options = array(
										'1' => 'Residential',
										'2' => 'Commercial'
									);
									
									$attributes = array(
										'legend' => false,
										'class' => 'radio-custom',
										'label' => array('class' => 'radio-custom-label'),
										'default' => '1',
										'id' => 'property_type'
									);
									
									echo $this->Form->radio('prop_type', $options, $attributes);
								?>									
							</div>		
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_toption" class="label_title">Property Type Options <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								
								<div class="select-style ">
									<?php
										echo $this->Form->input(
										'prop_toption', array(
										'id' => 'prop_toption',
										'options' => '',
										'class' => 'selectpicker required',
										'label' => false,
										'empty' => 'Select'
										)
										);
									?>
									
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_proj" class="label_title">Project </label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_proj', array(
									'id' => 'prop_proj',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_stat" class="label_title">Status </label>
							</div>
							
							<div class="col-sm-12">
								
								<div class="select-style ">
									<?php
										echo $this->Form->input(
										'prop_stat', array(
										'id' => 'prop_stat',
										'options' => array(
										'Rented' => 'Rented',
										'Self' => 'Self',
										'Occupied' => 'Occupied',
										'Vacant' => 'Vacant'
										),
										'class' => 'selectpicker',
										'label' => false
										)
										);
									?>
									
								</div>
								
							</div>
							
						</div>
					</div>
					
				</div>
				
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_addr" class="label_title">Address <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_addr', array(
									'id' => 'prop_addr',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_locality" class="label_title">Locality <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_locality', array(
									'id' => 'prop_locality',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
				</div>							
				
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_state" class="label_title">State </label>
							</div>
							
							<div class="col-sm-12">
								
								<div class="select-style ">
									<?php
										echo $this->Form->input(
										'prop_state', array(
										'id' => 'prop_state',
										'options' => $state_master,
										'class' => 'selectpicker',
										'label' => false,
										)
										);
									?>
									
								</div>
								
							</div>
							
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_city" class="label_title">City <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								
								<div class="select-style " id="manger_city">
									<?php
										echo $this->Form->input(
										'prop_city', array(
										'id' => 'prop_city',
										'options' => '',
										'class' => 'selectpicker required',
										'label' => false
										)
										);
									?>
									
								</div>
								
							</div>
							
						</div>
					</div>								
				</div>
				
				<br>
				<div class="row">
					
					<div class="col-sm-12">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_desc" class="label_title">Description <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->textarea(
									'prop_desc', array(
									'id' => 'prop_desc',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false,
									'rows' => '3',
									)
									);
								?>	
								
							</div>
						</div>
					</div>
				</div>	
				
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_area" class="label_title">Area ( in Sq feet ) <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_area', array(
									'id' => 'prop_area',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6 hide_config">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_cfg" class="label_title">Configure </label>
							</div>
							
							<div class="col-sm-12">
								
								<div class="select-style ">
									<?php
										echo $this->Form->input(
										'prop_cfg', array(
										'id' => 'prop_cfg',
										'options' => array('None' => 'None', 'Shared' => 'Shared', '1RK' => '1RK', 'Studio' => 'Studio', '1BHK' => '1BHK', '2BHK' => '2BHK', '3BHK' => '3BHK', '4BHK' => '4BHK', '5BHK' => '5BHK', '6BHK' => '6BHK', 'PentHouse' => 'PentHouse'),
										'class' => 'selectpicker',
										'label' => false
										)
										);
									?>
									
								</div>
								
							</div>
							
						</div>
					</div>								
				</div>
				
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_pprice" class="label_title">Purchase Price <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_pprice', array(
									'id' => 'prop_pprice',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_pdate" class="label_title">Purchase date <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
									<?php
										echo $this->Form->input(
										'prop_pdate', array(
										'id' => 'prop_pdate',
										'class' => 'form-control',
										'size' => '16',
										'value' => '',
										'readonly' => 'readonly',
										'style' => 'cursor : pointer',
										'type' => 'text',
										'label' => false
										)
										);
									?>
									<span class="add-on removeicon"><i class="icon-remove"></i></span>
									<span class="add-on calicon"><i class="icon-th"></i></span>
								</div>
								<?php
									echo $this->Form->input(
									'dtp_input1', array(
									'id' => 'dtp_input1',
									'value' => '',
									'type' => 'hidden',
									'label' => false
									)
									);
								?>
								<br>
							</div>			
						</div>
					</div>			
					
				</div>
				
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_mprice" class="label_title">Market Price <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_mprice', array(
									'id' => 'prop_mprice',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_mrent" class="label_title">Market Rent <span class="mand_field">*</span></label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->input(
									'prop_mrent', array(
									'id' => 'prop_mrent',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
				</div>
				
				<br>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_oth" class="label_title">Desired Action </label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->textarea(
									'prop_oth', array(
									'id' => 'prop_oth',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false,
									'rows' => '3'
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							
							<div class="col-sm-12">
								<label for="prop_rsn" class="label_title">Reason </label>
							</div>
							
							<div class="col-sm-12">
								<?php
									echo $this->Form->textarea(
									'prop_rsn', array(
									'id' => 'prop_rsn',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false,
									'rows' => '3'
									)
									);
								?>	
								
							</div>
						</div>
					</div>
					
				</div>
				
				<br>
				<div class="row">
					
					<div class="col-sm-12">
						
						<div class="form-group">
							
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary" >Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- </form> -->	
			<?= $this->Form->end();?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php
	
	// Jquery Script for Validation of form fields    
	echo $this->Html->script('validate_1.9_jquery.validate.min');
	echo $this->Html->script('front/additional-methods.min');
	echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css');
	echo $this->Html->script('front/bootstrap-datetimepicker');
	echo $this->Html->scriptBlock("
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1);
		// Handling Date input from calendar
		$('#prop_pdate').datetimepicker({
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		
	");
	// Script  : To set validation rules for FORMS
	echo $this->Html->scriptBlock("

		$('#id_form_PortMgmtAdd').validate({
			
						// Specify the validation rules
			// Specify the validation rules
			rules: {
				'data[Portfolio][prop_toption]': {
					required : true
				},
				'data[Portfolio][prop_addr]': {
					required : true
				},
				'data[Portfolio][prop_locality]': {
					required : true
				},
				'data[Portfolio][prop_city]': {
					required : true
				},
				'data[Portfolio][prop_desc]': {
					required : true
				},
				'data[Portfolio][prop_area]': {
					required : true,
                    number: true,
                    min: 1
				},
				'data[Portfolio][prop_pprice]': {
					required : true,
                    number: true,
                    min: 1
				},
				'data[Portfolio][prop_mprice]': {
					required : true,
                    number: true,
                    min: 1
				},
				'data[Portfolio][prop_mrent]': {
					required : true,
                    number: true,
                    min: 1
				},
				'data[Portfolio][prop_pdate]': {
					required : true
				},
			},
			
			// Specify the validation error messages
			messages: {
				'data[Portfolio][prop_area]': {
                    number: 'Enter Only Numeric',
					min: 'Area must be greater than 0'
                },
				'data[Portfolio][prop_pprice]': {
					number: 'Enter Only Numeric',
					min: 'Purchase Price must be greater than 0'
                },
				'data[Portfolio][prop_mprice]': {
					number: 'Enter Only Numeric',
					min: 'Market Price must be greater than 0'
                },
				'data[Portfolio][prop_mrent]': {
					number: 'Enter Only Numeric',
					min: 'Market Rent must be greater than 0'
                }
			}
		});	
	");
?>