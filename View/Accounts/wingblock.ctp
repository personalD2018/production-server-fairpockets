<div class="account-block">
	<?php echo $mess ;?><?php echo $mess2 ;?>
	<div class="row">
		<div class="col-sm-12"><label class="label_title" style="font-weight:bold;">Select Wing</label></div><br>
		 <div class="form-group col-md-4 col-sm-4 property-type">
			<table id="sales_list" class="table table-striped table-bordered" style="width:100%">                
				<tbody>
					<?php  if (!empty($allwing)) {
						for ($i = 0; $i < count($allwing); $i++) {
					?>
					<?php echo $i % 4 == 0 ? "<tr>\n" : ""; ?>
						<td>
							<input type="checkbox" name="check_list[<?php echo $i; ?>]" 
								id="check_list[<?php echo $allwing[$i]['Inventorie']['wing'].$i; ?>]" value="<?php echo $allwing[$i]['Inventorie']['id'] ?>" class="checkbox-custom">
	   
								<label for="check_list[<?php echo $allwing[$i]['Inventorie']['wing'].$i; ?>]" class="checkbox-custom-label">
								Wing - <?php echo $allwing[$i]['Inventorie']['wing'] ?> <b>[Tower - <?php echo $allwing[$i]['Inventorie']['tower_name'] ?>]</b>
								</label>
						</td>
						<?php echo $i % 4 == 3 ? "</tr>\n" : ""; ?>
						<?php }}else{ ?>
					<tr>
						<td colspan="4" style="text-align:center;">
						<input type="hidden" name="check_list_tower" value='<?php echo $toweridStrings; ?>'>
							No Wing Available.</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	
	
	<div class="row" style="display:none;">
		<div class="col-sm-12"><label class="label_title" style="font-weight:bold;">Select Area</label></div><br>
		 <div class="form-group col-md-4 col-sm-4 property-type">
			<table id="sales_list" class="table table-striped table-bordered" style="width:100%">                
				<tbody>
					<?php  if (!empty($invArea)) {
						for ($i = 0; $i < count($invArea)-1; $i++) {
					?>
					<?php echo $i % 4 == 0 ? "<tr>\n" : ""; ?>
						<td>
							<input type="checkbox" name="check_list_area[<?php echo $i; ?>]" 
								id="check_list[<?php echo $invArea[$i]['Inventory_propertie']['flat_area'].$i; ?>]" value="<?php echo $invArea[$i]['Inventory_propertie']['flat_area']; ?>" class="checkbox-custom">
	   
								<label for="check_list[<?php echo $invArea[$i]['Inventory_propertie']['flat_area'].$i; ?>]" class="checkbox-custom-label"><?php echo $invArea[$i]['Inventory_propertie']['flat_area']; ?></label>
						</td>
						<?php echo $i % 4 == 3 ? "</tr>\n" : ""; ?>
						<?php }}else{ ?>
					<tr>
						<td colspan="4" style="text-align:center;">
							No Wing Available.</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	
	
	<div class="row" style="display:none;">
		<div class="col-sm-12"><label class="label_title" style="font-weight:bold;">Select Status</label></div><br>
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">                
			<tbody>
				<tr>
					<td>
						<input type="checkbox" name="status[]" id="stat5" value="5" class="checkbox-custom">					   
						<label for="stat5" class="checkbox-custom-label">
							Total
						</label>
					</td>
					
					<td>
						<input type="checkbox" name="status[]" id="stat1" value="1" class="checkbox-custom">					   
						<label for="stat1" class="checkbox-custom-label">
							Available
						</label>
					</td>
					
					<td>
						<input type="checkbox" name="status[]" id="stat2" value="2" class="checkbox-custom">					   
						<label for="stat2" class="checkbox-custom-label">
							Not Available
						</label>
					</td>
					
					<td>
						<input type="checkbox" name="status[]" id="stat3" value="3" class="checkbox-custom">					   
						<label for="stat3" class="checkbox-custom-label">
							On-Hold
						</label>
					</td>
					
					<td>
						<input type="checkbox" name="status[]" id="stat4" value="4" class="checkbox-custom">					   
						<label for="stat4" class="checkbox-custom-label">
							Internal Hold
						</label>
					</td>
				</tr>
			</tbody>
		</table>								 
	</div>	
	<div class="add-tab-row  push-padding-bottom">
			<div class="account-block text-center submit_area">
			<button type="submit" class="btn btn-primary btn-next " id="next12">Next</button>
			</div>
	</div>
</div>