<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Share Project To Users</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('ProjectsAndroidApp', array(
			'url' => '/accounts/	/',
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		
	?>
	
	<input type="hidden" name="broker_id" value="broker_id">
	<input type="hidden" name="builder_id" value="builder_id">
	
	<input type="hidden" name="projectjson" value='<?php echo $projectjson; ?>'>
    <div class="row">
        <div class="col-md-12" style="border: 1px solid #bdbdbd;">
            
            
            <table id="sales_list" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="sales_list_info">
                <thead>
                    <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="sales_list" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 101px;">Name</th><th class="sorting" tabindex="0" aria-controls="sales_list" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 225px;">Email</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Mobile" style="width: 115px;">Mobile</th><th class="sorting" tabindex="0" aria-controls="sales_list" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 163px;">Creation Date</th><th class="sorting" tabindex="0" aria-controls="sales_list" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 86px;">Actions</th></tr>
                </thead>
                <tbody>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    <tr role="row" class="odd">
                        <td class="sorting_1">digpalupdate</td>
                        <td>digpalsingh884@gmail.com</td>
                        <td>8882348796</td>
                        <td>2018-06-08 09:42:54</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/142" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('142')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">Kamal Batra</td>
                        <td>info@buniyad.com</td>
                        <td>9810002627</td>
                        <td>2018-06-04 10:53:02</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/145" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('145')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">new user</td>
                        <td>newss@gmail.com</td>
                        <td>1231236780</td>
                        <td>2018-08-01 13:03:57</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/230" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('230')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">newuser1</td>
                        <td>nnn@gmail.com</td>
                        <td>1234568890</td>
                        <td>2018-08-01 18:39:20</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/231" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('231')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">rohan</td>
                        <td>rohan@gmail.com</td>
                        <td>1231238908</td>
                        <td>2018-08-01 05:41:40</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/227" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('227')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">rohit</td>
                        <td>rht@gmail.com</td>
                        <td>8882308987</td>
                        <td>2018-07-31 05:52:41</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/212" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('212')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">rohit1</td>
                        <td>rohit1@gmsil.com</td>
                        <td>1231231245</td>
                        <td>2018-07-31 05:52:41</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/223" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('223')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">rohit2</td>
                        <td>rohit2</td>
                        <td>1231231239</td>
                        <td>2018-07-31 06:17:42</td>
						<td style="text-align: center;">
                            <a href="http://www.fairpockets.com/accounts/edit_sales_employees/224" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="javascript:void(0);" onclick="javascript:deleteSalesEmployees('224')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr></tbody>
            </table>
           
           
                        
            <div class="row cell-content">
                <div class="col-md-4"><b>User Name</b></div>
                <div class="col-md-4">
				&nbsp;
				</div>
				<div class="col-md-4">
				<b>Actions</b>
				</div>
			</div>
			<?php //echo '<pre>';print_r($BuilderSalesList);
			if (!empty($BuilderSalesList)) { ?>
			<?php for ($i = 0; $i < count($BuilderSalesList); $i++) { ?>
            <div class="row cell-content">
                <div class="col-md-4"><?php echo $BuilderSalesList[$i]['SalesUser']['name']; ?></div>
                 <div class="col-md-4">
				 <input type="hidden" name="data[shareprojects1][sales_id<?php echo $i; ?>]" value="<?php echo $BuilderSalesList[$i]['SalesUser']['id']; ?>">
				 </div>
				
				<div class="col-md-4">
				<!--<input type="checkbox" class= "checkbox-custom" name="check_list[]" value="<?php echo $BuilderSalesList[$i]['SalesUser']['id']; ?>"><label class="checkbox-custom-label"><?php echo $BuilderSalesList[$i]['SalesUser']['name']; ?></label>-->
				<?php /*echo $this->Form->input('check_list[]', array(
                                    'id' => 'shareinv1.sales_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
									'value' => $BuilderSalesList[$i]['SalesUser']['id'], 
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );*/
				?><?php //echo $i; ?>
				
				<div class="col-sm-12">
				<input type="checkbox" name="check_list[]" id="shareinv<?php echo $i; ?>.sales_check" class="checkbox-custom" value="<?php echo $BuilderSalesList[$i]['SalesUser']['id']; ?>">
				<label for="shareinv<?php echo $i; ?>.sales_check" class="checkbox-custom-label"></label></div>
				
				</div>
            </div>
			<?php } ?>
            
           
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="col-md-12" style="font-weight:bold; text-align: center;padding-top: 12px;padding-bottom: 12px;">
			No User available, please add sales employees. <a href="/accounts/add_sales_employees1">Click Here</a>
			</div>
			<?php } ?>
            <?php
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>