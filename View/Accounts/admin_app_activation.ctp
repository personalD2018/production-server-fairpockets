<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Builders</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Builders List</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Id</th>
								<th class="center">Builder Name </th>
                                <th class="center">Contact person Name </th>
								<th class="center">Contact person Email </th>
                                <th class="center">Contact person Mobile</th>
                                <th class="center">Free Trial Activated </th>
								<th class="center">Paid activation start date</th>
								<th class="center">Paid activation end date</th>
								<th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
							

							
							/*
							$i='';
							echo $len=count($Freeactivation);
						
							for($i = 0 $i < $len; $i++){
								echo $i;
							}
							*/
							
							//echo $activationdata['date_created'];
							
                            foreach ($builderData as $data) {
                                ?>
                                <tr>
									<td class="center"><?php echo $data['Websiteuser']['id']; ?></td>
                                    <td class="center"><?php echo $data['Websiteuser']['username']; ?></td>
									<td class="center"><?php echo $data['Websiteuser']['username']; ?></td>
                                    <td class="center"><?php echo $data['Websiteuser']['email']; ?></td>
							        <td class="center"><?php echo $data['Websiteuser']['usermobile']; ?></td>
									<td class="center">
										<?php 
											if($data['Freeactivation']['servicetype'] == 'Free'){			
												echo $data['Freeactivation']['date_created'];
											}else{
												echo '--- ---'; 
											}
										?>									
									</td>
									
									<td class="center">
										<?php 
											if($data['Freeactivation']['servicetype'] == 'Paid'){			
												echo $data['Freeactivation']['date_created'];
											}else{
												echo '--- ---'; 
											}
										?>									
									</td>
									
									<td class="center">
										<?php echo $data['Freeactivation']['expiry_date']; ?>
									</td>
									
									
									
									
									
									 <td class="center">
									 <a target="_blank" href="<?php echo $this->webroot; ?>admin/accounts/paid_activation/<?php echo $data['Websiteuser']['id'] ?>"> <button style="background-color:#ce4b27	;margin:10px;" class="btn btn-info" title="Edit">Paid user</button></a>
									 
									 <a target="_blank" href="<?php echo $this->webroot; ?>admin/accounts/free_activation/<?php echo $data['Websiteuser']['id'] ?>"> <button style="background-color:#00859d;" class="btn btn-info " title="Edit">Free user</button></a>
									 </td>                               
                                                                                
								                                        

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


