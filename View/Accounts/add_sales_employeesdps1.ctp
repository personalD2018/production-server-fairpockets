<link rel="stylesheet" type="text/css" href="/css/home/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/home/bootstrap-multiselect.css"/>

<style>
/*.bootstrap-select.btn-group .dropdown-menu li a span.check-mark{
    display:inline-block !important;
	float:none;
}
form .selected .glyphicon{
	top:6px;
	right:5px;
	position:absolute !important;
}
form .glyphicon{
	top:6px;
	right:5px;
	position:absolute !important;
}

.account-block .selected .glyphicon-ok:before, .property-type .selected .glyphicon-ok:before {
    content: "";
    background:url('/img/front/check_a.png') no-repeat;
    padding: 2px 12px;
}
.account-block .glyphicon-ok:before, .property-type .glyphicon-ok:before {
    content: "";
    background:url('/img/front/check_b.png') no-repeat;
    padding: 1px 12px;
}

.property-type .bootstrap-select.btn-group .dropdown-menu{
	min-width:288px;
}
.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
    margin-right: 0 !important;
	margin-left:5px;
}
.bootstrap-select.btn-group.show-tick .dropdown-menu li.selected a span.check-mark {
    position: absolute;
    display: inline-block;
	right:5px !important;
    margin-top: 0px !important;
}

.glyphicon-ok:before {
    content: "";
}
.selected .glyphicon-ok:before {
    content: "";
}

.btn-default{
	border:none;
}
.btn-default:hover, .btn-default:focus, .btn-default.focus, .btn-default:active, .btn-default.active, .open>.dropdown-toggle.btn-default{
	border:none;
}

.bootstrap-select .dropdown-toggle:focus {
    outline: thin dotted #333!important;
    outline: 0px auto -webkit-focus-ring-color!important;
    outline-offset: -2px;
}
.block-inner-link li{
float:none;
}
.block-inner-link li a{
    background: none;
    padding: 2px 5px;
	color:#000 !important;
}*/

.row {
    margin: 0 !important;
}
</style>


<script>
    $(document).ready(function () {

        $("#prop_state").on('change', function () {
            var id = $(this).val();
            if (id != '') {
                $.getJSON('/countries/getpecity/' + id, function(data) {
                    $('#prop_city').empty();
                    for(var cityCode in data) {
                        $('#prop_city').append($('<option>', { 
                            'value': cityCode, 
                            'text': data[cityCode] 
                        }));
                    }
                });
            }
        });

        $("#prop_state").trigger('change');
    });
</script>
<script>
    $(document).ready(function () {
        $(".radio-custom").on('click', function () {

            var id = $(this).val();
            if (id == '1')
            {
                $(".hide_config").show();
            } else
            {
                $(".hide_config").hide();
            }
            var transaction = $("#transaction_type").val();

            $("#prop_toption").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyoption")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#prop_toption"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#prop_toption"));
                        });
                        if (transaction == '2')
                        {
                            $("#prop_toption option[value='3']").hide();
                            $("#property_type_val option[value='27']").hide();
                        }
                        if (transaction == '1')
                        {
                            $("#prop_toption option[value='3']").show();
                            $("#prop_toption option[value='27']").show();
                        }
                    }
                }).responseJSON;
            }



        });

    });


</script>	 
<!--./advertisement-Card-->
    <div class="page-content">

            <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header"> Android App Management - Add Sales Emplopyee </h1>
                </div>

            </div>

            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo    $this->Form->create('SalesUser', array(
                                'class' => 'fpform',
                                'role' => 'form',
                                'id' => 'id_form_PortMgmtAdd',
                                'novalidate' => 'novalidate'
                            ));
                    ?>


                    <div class="account-block">

                        
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Name </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'name', array(
                                            'id' => 'name',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Email <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'email', array(
                                            'id' => 'email',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">Mobile No <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'mobile', array(
                                            'id' => 'mobile',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_locality" class="label_title">Discount <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'discount', array(
                                            'id' => 'discount',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>							

                        <br>
                        <div class="row">                           
							
                                            <?php
                                            /*echo $this->Form->input(
                                                    'projects', array(
                                                'id' => 'projects',
                                                'options' => array('project1' => 'Project1'),
                                                'class' => 'selectpicker required',
                                                'label' => false,
                                                'empty' => 'Select'
                                                    )
                                            );*/
                                            ?>
							
							<!--<div class="col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="label_title">Select Projects</label>
									<select id="projects" name="projects[]" class="selectpicker required" multiple>
									    <?php //foreach($getProjects as $data){ ?>
											<option value="<?php //echo $data['Project']['project_id']; ?>"><?php //echo $data['Project']['project_name']; ?> </option>
										<?php //} ?>
									</select>
									
								</div>	
							</div>-->

							
                        </div>
						
						<div class="row">
						
						<?php //echo '<pre>'; print_r($getProjects); ?>
							<input type="hidden" id="id_cloneProjectBspChargeCount" value="40" >
							<h3><center>Assign Discount With Projects</center></h3>
							<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab" >
								<div class="row cloneProjectBspCharge" id="cloneProjectBspCharge1" >	
									<div class="col-sm-4">
										<div class="form-group">

											 <label for="project_discount1" class="label_title">Select Projects</label>
											<?php

                                            $bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
                                            $bspFromToOptions[0]='Ground';

                                            echo $this->Form->input(
                                                    'bspcharges.from_floor1', array(
                                                'id' => 'from_floor1',
                                                'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'empty' => 'From'
                                                    )
                                            );
                                            ?>
											

										</div>
									</div>
									
									<div class="col-sm-4">
										<div class="form-group"> 

											<label for="property-price-before" class="label_title">Discount</label>
											<div class="select-style">
												<?php
													echo $this->Form->input(
														'projects.project_discount1', array(
													'id' => 'project_discount1',
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false,
													'value' => ''
														)
													);
												?>
											</div>
										</div>
									</div>
								
									
									<div class="col-sm-1">
										<div class="form-group">
											<div class="frowedit">
												<a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
											</div>
										</div>
									</div>
									
								</div>
													
								
							</div>
							<div class="row" style="padding-bottom:14px;">
								<div class="col-sm-2 pull-right">
									<a id="id_cloneProjectBspChargeAddRow" onclick="cloneProjectBspChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
								</div>
							</div>
						
						</div>


                        <br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>


                </div>
            </div>



            <div class="clear"></div>



        </div>

<?php
// Jquery 

echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');



// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		var rulespTarea = {
					required : true,
					min:1
			
		};
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_PortMgmtAdd').validate({
			
			// Specify the validation rules
			rules: {
				'data[Portmgmt][prop_addr]': rulesMTarea,
				'data[Portmgmt][prop_locality]': rulesMTarea,
				'data[Portmgmt][prop_desc]': rulesMTarea,
				'data[Portmgmt][prop_area]': rulespTarea
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[Portmgmt][prop_addr]': messageMTarea,
				'data[Portmgmt][prop_locality]': messageMTarea,
				'data[Portmgmt][prop_desc]': messageMTarea,
				'data[Portmgmt][prop_area]': messageMTarea
				
			}
		});	
	");
?>
<script type="text/javascript" src="/js/front/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/front/bootstrap-multiselect.js"></script>


<script type="text/javascript" src="/js/add-project.js">





