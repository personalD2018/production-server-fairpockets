<script>
    $(document).ready(function () {

        $("#prop_state").on('change', function () {
            var id = $(this).val();
            if (id != '') {
                $.getJSON('/countries/getpecity/' + id, function(data) {
                    $('#prop_city').empty();
                    for(var cityCode in data) {
                        $('#prop_city').append($('<option>', { 
                            'value': cityCode, 
                            'text': data[cityCode] 
                        }));
                    }
                });
            }
        });

        $("#prop_state").trigger('change');
    });
</script>
<script>
    $(document).ready(function () {
        $(".radio-custom").on('click', function () {

            var id = $(this).val();
            if (id == '1')
            {
                $(".hide_config").show();
            } else
            {
                $(".hide_config").hide();
            }
            var transaction = $("#transaction_type").val();

            $("#prop_toption").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyoption")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#prop_toption"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#prop_toption"));
                        });
                        if (transaction == '2')
                        {
                            $("#prop_toption option[value='3']").hide();
                            $("#property_type_val option[value='27']").hide();
                        }
                        if (transaction == '1')
                        {
                            $("#prop_toption option[value='3']").show();
                            $("#prop_toption option[value='27']").show();
                        }
                    }
                }).responseJSON;
            }



        });

    });


</script>	 
<!--./advertisement-Card-->
    <div class="page-content">

            <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header"> Property Management - Add Property </h1>
                </div>

            </div>

            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo    $this->Form->create('Portmgmt', array(
                                'class' => 'fpform',
                                'role' => 'form',
                                'id' => 'id_form_PortMgmtAdd',
                                'novalidate' => 'novalidate'
                            ));
                    ?>


                    <div class="account-block">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_type"  class="label_title">Property Type </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        $options = array(
                                            '1' => 'Residential',
                                            '2' => 'Commercial'
                                        );

                                        $attributes = array(
                                            'legend' => false,
                                            'class' => 'radio-custom',
                                            'label' => array('class' => 'radio-custom-label'),
                                            'default' => '1',
                                            'id' => 'property_type'
                                        );

                                        echo $this->Form->radio('prop_type', $options, $attributes);
                                        ?>									
                                    </div>		
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_toption" class="label_title">Property Type Options <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_toption', array(
                                                'id' => 'prop_toption',
                                                'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder Floor', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'),
                                                'class' => 'selectpicker required',
                                                'label' => false,
                                                'empty' => 'Select'
                                                    )
                                            );
                                            ?>


                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Project </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_proj', array(
                                            'id' => 'prop_proj',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Description <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->textarea(
                                                'prop_desc', array(
                                            'id' => 'prop_desc',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false,
                                            'rows' => '1'
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">Address <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_addr', array(
                                            'id' => 'prop_addr',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_locality" class="label_title">Locality <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_locality', array(
                                            'id' => 'prop_locality',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>							

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_state" class="label_title">State </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_state', array(
                                                'id' => 'prop_state',
                                                'options' => $state_master,
                                                'class' => 'selectpicker',
                                                'label' => false,
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_city" class="label_title">City </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style " id="manger_city">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_city', array(
                                                'id' => 'prop_city',
                                                'options' => '',
                                                'class' => 'selectpicker required',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>								
                        </div>


                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_area" class="label_title">Area ( in Sq feet ) <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_area', array(
                                            'id' => 'prop_area',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 hide_config">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_cfg" class="label_title">Configure </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_cfg', array(
                                                'id' => 'prop_cfg',
                                                'options' => array('None' => 'None', 'Shared' => 'Shared', '1RK' => '1RK', 'Studio' => 'Studio', '1BHK' => '1BHK', '2BHK' => '2BHK', '3BHK' => '3BHK', '4BHK' => '4BHK', '5BHK' => '5BHK', '6BHK' => '6BHK', 'PentHouse' => 'PentHouse'),
                                                'class' => 'selectpicker',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>								
                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" >Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>


                </div>
            </div>



            <div class="clear"></div>



        </div>

<?php
// Jquery 

echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');

echo $this->Html->scriptBlock("
	
		// Handling Date input from calendar
		$('.form_date').datetimepicker({
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});

	");

// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		var rulespTarea = {
					required : true,
					min:1
			
		};
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_PortMgmtAdd').validate({
			
			// Specify the validation rules
			rules: {
				'data[Portmgmt][prop_addr]': rulesMTarea,
				'data[Portmgmt][prop_locality]': rulesMTarea,
				'data[Portmgmt][prop_desc]': rulesMTarea,
				'data[Portmgmt][prop_area]': rulespTarea
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[Portmgmt][prop_addr]': messageMTarea,
				'data[Portmgmt][prop_locality]': messageMTarea,
				'data[Portmgmt][prop_desc]': messageMTarea,
				'data[Portmgmt][prop_area]': messageMTarea
				
			}
		});	
	");
?>






