<!--./advertisement-Card-->
<div class="page-content">
	<?php $paginator = $this->Paginator; ?>
	<div class="row">
		<div class="col-md-9">
			<h1 class="page-header">Portfolio Management - My Properties</h1>
		</div>
		<div class="col-md-3">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="Portfolio_ViewProperty" >
					<div class="input-group custom-search-form">
						<input type="text" name="search" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
								<i class="fa fa-search"></i>
							</button>
						</div>
						
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		<?php
			//pr($allprojdata);die;
            if (!empty($allprojdata)) {
                foreach ($allprojdata as $data) {
                    if ($data['Portfolio']['prop_type'] == '1') {
                        $pro_type = "Residential";
						} else {
                        $pro_type = "Commercial";
					}
				?>
				<div id="build" class="list-detail detail-block target-block">
					
					<div class="detail-title">
						<a href="#"><?php echo $pro_type ?> Property in <?php echo $data['Portfolio']['prop_locality']; ?>, <?php echo $data['MstAreaCity']['cityName']; ?></a>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" > <strong>Purchase Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_pprice']; ?> </div>
						</div>
						<div class="col-md-6">
							<div class="form-control" > <strong>Market Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_mprice']; ?> </div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" > <strong> Area : </strong> <?php echo $data['Portfolio']['prop_area']; ?> sq feet </div>
						</div>
						<div class="col-md-6">
							<div class="form-control" > <strong>Status : </strong> <?php echo $data['Portfolio']['prop_stat']; ?> </div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" ><strong>Locality: </strong><?php echo $data['Portfolio']['prop_locality']; ?> </div>
						</div>
						<div class="col-md-6">
							<div class="form-control" > <strong>Address : </strong> <?php echo $data['Portfolio']['prop_addr']; ?> </div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" ><strong>Property Type Option : </strong><?php echo $data['MstPropertyTypeOption']['option_name']; ?> </div>
						</div>
						<?php
                            if ($data['Portfolio']['prop_type'] == '1') {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Configure : </strong><?php echo $data['Portfolio']['prop_cfg']; ?></div>
							</div>
						<?php } ?>		
					</div>
					
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="block-inner-link">
								<ul>
									<li><a href="<?php echo $this->webroot; ?>accounts/portfolio_editProperty/<?php echo $data['Portfolio']['id']; ?>">Edit</a></li>
								</ul>
							</div>
						</div>
					</div>
					
				</div>
				<div class="space-x"></div>
				<?php
				}
				} else {
			?>
			<div id="build" class="list-detail detail-block target-block">
				
				No Property Found
				
			</div>
            <?php }
		?>
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				<?php
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					
                    
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					
                    // for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>	
			</ul>
		</div>
	</div>		
</div>