<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<?php
echo $this->Html->css('home/report_detail');
$unit = json_decode($Reasearchreport['ResearchReport']['unit'], true);
?>
<div class="container">
	<div class="space-l"></div>
<div class="research_details inside-pages">
    <h1><?php echo (!empty($Reasearchreport['ResearchReport']['report_name'])) ? $Reasearchreport['ResearchReport']['report_name'] : 'Fairpockets Property'; ?></h1>
    <div class="row project-details">
			<div class="col-md-6 project-details">
			<?php //echo pr($Reasearchreport);?>
				<!--<div class="fotorama" data-width="100%" data-thumbheight="100" data-thumbwidth="100">-->
                    <?php if(!empty($Reasearchreport['ResearchReport']['image'])){ ?>
						<img class="img-responsive" src="<?php echo $this->webroot . $Reasearchreport['ResearchReport']['image']; ?>" alt="<?php echo $Reasearchreport['ResearchReport']['report_name']; ?>" >
                    <?php }else{ ?> 
						<img class="img-responsive" src="<?php echo $this->webroot;?>img/01_434x290.jpg" style="width:100%;" alt="<?php echo $Reasearchreport['ResearchReport']['report_name'];?>" >
					<?php } ?>
                <!--</div>
                -->
				<div class="space-m"></div>
			
           </div>
		   <div class="col-md-6">
				<div class="propert-details">
				<ul class="right">
                <li><strong>Type of Project:</strong> <?php
                    if ($Reasearchreport['ResearchReport']['type_of_project'] == 1) {
                        echo "Residential";
                    }
                    if ($Reasearchreport['ResearchReport']['type_of_project'] == 2) {
                        echo "Commercial";
                    }
                    ?></li>
                <li class="color"><strong>Plot Area:</strong> <?php echo $Reasearchreport['ResearchReport']['plot_area']; ?> Acres (Approx)</li>
                <li><strong>No. of Floors:</strong><?php echo $Reasearchreport['ResearchReport']['no_of_floors']; ?></li>
                <li class="color"><strong>Launch Date:</strong> <?php echo $Reasearchreport['ResearchReport']['doj1']; ?>(Approx)</li>
                <li><strong>Possession Date:</strong> <?php echo $Reasearchreport['ResearchReport']['doj2']; ?> End (Tentative)</li>
                <li class="color"><strong>Land Holding:</strong><?php echo $Reasearchreport['ResearchReport']['land_holding']; ?></li>
                <li><strong>Project Status:</strong><?php
                    if ($Reasearchreport['ResearchReport']['project_status'] == 1) {
                        echo "Under Construction";
                    }
                    if ($Reasearchreport['ResearchReport']['project_status'] == 2) {
                        echo "Ready to move in";
                    }
                    ?></li>
                <li class="color"><strong>No. of Towers:</strong><?php echo $Reasearchreport['ResearchReport']['no_of_towers']; ?></li>
                <li><strong>Unit Sizes:</strong> <?php echo $Reasearchreport['ResearchReport']['unit_size']; ?> sq. ft.</li>
                <li class="color"><strong>No. of Units:</strong> <?php echo $Reasearchreport['ResearchReport']['no_of_unit']; ?> (Approx)</li>
                <li><strong>Area Livability:</strong><?php echo $Reasearchreport['ResearchReport']['area_livability']; ?> Years</li>
                <li class="color"><strong>Density/Acre:</strong> <?php echo $Reasearchreport['ResearchReport']['dentisy']; ?> Flats (Approx)</li>
                <li><strong>Carpet Area (Approx):</strong>  <?php echo $Reasearchreport['ResearchReport']['carpet_area']; ?>% (Average)</li>
            </ul>
			</div>
				</div>
			
			</div>
	<div class="space-m"></div>

    <div class="row">
        <div class="col-md-12 text-center">
            <h2>About Project</h2>
            <p> <?php echo $Reasearchreport['ResearchReport']['about_project']; ?></p>
        </div>
    </div>

    <div class="space-x"></div>

    <div class="row">
        <div class="col-md-6">
            <h2>About Locality</h2>
            <h4><?php echo $Reasearchreport['ResearchReport']['locality']; ?> <?php echo $Reasearchreport['ResearchReport']['city_data']; ?></h4>
            <p><?php echo $Reasearchreport['ResearchReport']['about_locality']; ?>.</p>
        </div>
        <?php
        $gLng = $Reasearchreport['ResearchReport']['lng'];
        $gLat = $Reasearchreport['ResearchReport']['lat'];
		// Generate client side GMap part
				$gTitle = $Reasearchreport['ResearchReport']['locality'] . ', ' . $Reasearchreport['ResearchReport']['city_data'];
				echo "
		<input type='hidden' id='id_gLat' value='" . $gLat . "'>
		<input type='hidden' id='id_gLng' value='" . $gLng . "'>
		<input type='hidden' id='id_gTitle' value='" . $gTitle . "'>
		";
        ?>	
        <div class="col-md-6">
			 <div class="box">
                <h2>Download verifed Report</h2>
                <p>Lorem Ipsum is simply dummy text of the simply dummy text of the printing and typesetting industry.</p>
                <ul>
                    <?php
                    if ($Reasearchreport['ResearchReport']['report_price'] == 0) {
                        ?>
                        <li><a target="_blank" href="<?php echo $this->webroot . 'upload/reportpdf/' . $Reasearchreport['ResearchReport']['reportpdf']; ?>" class="buy-now">Download now</a></li>
                        <?php
                    } else {
                        ?>
                        <li><a href="#" class="buy-now">Buy now</a></li>
<?php } ?>	
                    <li><a href="<?php echo '/contact'?>" class="contact">Contact Fairpockets</a></li>
                </ul>
                <ul>
                    <a href="<?php echo $this->webroot; ?>samplereport.pdf" class="sample-report">View Sample Report</a>
                </ul>
            </div>
           
        </div>
    </div>


    <div class="spacer"></div>

    <div class="row">
        <div class="col-md-6 unit">
            <h2>Unit Types</h2>
            <table width="100%">
                <tr class="dark">
                    <td><strong>Unit Type</strong></td>
                    <td><strong>Total Units</strong></td>
                    <td><strong>Area Base Price</strong></td>
                </tr>
                <?php
                for ($i = 0; $i < count($unit); $i++) {
                    if (!empty($unit['unittype'][$i])) {
                        ?>								 
                        <tr class="light">
                            <td><?php echo @$unit['unittype'][$i]; ?></td>
                            <td><?php echo @$unit['totalunit'][$i]; ?> sqft</td>
                            <td>Rs. <?php echo @$unit['areabaseprice'][$i]; ?></td>

                        </tr>
                    <?php
                    }
                }
                ?>
            </table>
        </div>

        <div class="col-md-6">
             <div id="map_canvas" style="height:300px;border:0; width:100%;" ></div>
        </div>
    </div>


</div>
<div class="space-l"></div>
<div class="row light-yellow text-center">
    <div class="col-md-12">
		<div class="space-x"></div>
        <h2>About Builder</h2>
        <p><?php echo $Reasearchreport['ResearchReport']['about_builder']; ?>.</p>
    </div>    
</div>
<div class="space-l"></div>
</div>
<!-- To display gmap -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<script type="text/javascript">

    var map;
    var lat = parseFloat(document.getElementById("id_gLat").value);
    var lng = parseFloat(document.getElementById("id_gLng").value);
    var tmp = document.getElementById("id_gTitle").value;

    var infowindow = new google.maps.InfoWindow({});

    function initialize()
    {
        geocoder = new google.maps.Geocoder();

        var latlng = new google.maps.LatLng(lat, lng);

        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Property Location"
        });

        marker['infowindow'] = tmp;

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
        });

    }

    window.onload = initialize;

</script>



