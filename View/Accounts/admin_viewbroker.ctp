<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Research Reports</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Research Reports</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>
                                <th class="center">Broker Name</th>
                                <th class="center">Company Name</th>
                                <th class="center">Mobile First</th>
								<th class="center">Mobile Second</th>
                                <th class="center">City</th>
                                <th class="center">Locality</th>
                                <th class="center">Project</th>
								<th class="center">Address</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($researchreport as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['Broker']['broker_name']; ?></td>		  

                                    <td class="center"><?php echo $data['Broker']['company_name']; ?></td>

                                    <td class="center"><?php echo $data['Broker']['mobile_first']; ?></td>
                                   <td class="center"><?php echo $data['Broker']['mobile_second']; ?></td>
                                    <td class="center"><?php echo $data['Broker']['city']; ?></td>
                                    <td class="center"><?php echo $data['Broker']['locality']; ?></td>
                                    <td class="center"><?php echo $data['Broker']['project_id']; ?></td>
                                 <td class="center"><?php echo $data['Broker']['address']; ?></td>
                                 
                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/editResearchBroker/<?php echo $data['Broker']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
												
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/broker_detail/<?php echo $data['Broker']['id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail" onclick="window.location.href = '<?php echo $this->webroot; ?>admin/accounts/broker_detail/<?php echo $data['Broker']['id']; ?>'"><i class="icon-eye-open"></i></button></a>


                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


