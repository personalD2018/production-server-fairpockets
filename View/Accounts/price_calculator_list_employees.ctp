	<?php $paginator = $this->Paginator;?>
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">List of Employees</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="price_calculator_list_employees" >
						<div class="input-group custom-search-form">
							<input type="text" name="search" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Creation Date</th>
						<th>Services Assigned</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($allsalesUsers as $key=>$row):
                    ?>
                    <tr>
                        <td><?php echo isset($row['SalesUser']['name']) ? $row['SalesUser']['name'] : "N/A";?></td>
                        <td><?php echo isset($row['SalesUser']['email']) ? $row['SalesUser']['email'] : "N/A";?></td>
                        <td><?php echo isset($row['SalesUser']['mobile']) ? $row['SalesUser']['mobile'] : "N/A";?></td>
                        <td><?php echo isset($row['SalesUser']['created']) ? $row['SalesUser']['created'] : "N/A";?></td>
						<td style="text-align: center;">
						<?php if($this->Number->userInvStatusSales($row['SalesUser']['id']) == 'true'){  ?>
							   <a href="#" title="Inventory" style="font-size: 16px;">
									<i class="fa fa-building" aria-hidden="true" placeholder="calculator"></i>
							   </a>								  
						<?php } ?>
						<?php if($this->Number->UserCalStatusSales($row['SalesUser']['id']) == 'true'){  ?>
							  
							  <a href="#" title="Price Calculator" style="font-size: 16px;">
									<i class="fa fa-calculator" aria-hidden="true" placeholder="calculator"></i>
							  </a>
						<?php } ?>
						
						</td>
						<td style="text-align: center;">
                            <a href="<?php echo Router::url('/accounts/edit_sales_employees1/'.$row['SalesUser']['id'],true);?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a href="#" onclick="javascript:deleteSalesEmployees('<?php echo $row['SalesUser']['id']?>')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>		
				
			</ul>
			
		</div>
		
	</div>
	
		<script type="text/javascript">
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/accounts/deleteSalesEmployees/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
});
</script>
