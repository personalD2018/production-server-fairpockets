<style>
    .controls{
        margin-top: 5px;
    }
</style>	
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Builder</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>View FP Builder Organization</h3>
                </div>
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('User', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">Contact Person</label>
                            <div class="controls">

                                <?php echo $this->Form->input('contact', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_contact'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Landline</label>
                            <div class="controls">
                                <?php echo $this->Form->input('landline', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_landline'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>									
                        </div>



                        <div class="control-group">
                            <label class="control-label">Official Website</label>
                            <div class="controls">
                                <?php echo $this->Form->input('web', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_web'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Established Year</label>
                            <div class="controls">
                                <?php echo $this->Form->input('year', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_year'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">About</label>
                            <div class="controls">
                                <?php echo $this->Form->input('about', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_about'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Management </label>
                            <div class="controls">
                                <?php echo $this->Form->input('mgmt', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_mgmt'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Map Address </label>
                            <div class="controls">

                                <?php echo $this->Form->input('sinput', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['searchInput'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>									
                        </div>
                        <div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
                                <?php echo $this->Form->input('addr', array('div' => false, 'label' => false, 'value' => $builder_data['Builder']['borg_addr'], 'disabled' => 'disabled', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Status </label>
                            <div class="controls">
                                <?php
                                if ($builder_data['Builder']['status'] == 2) {
                                    echo "Pending";
                                }
                                if ($builder_data['Builder']['status'] == 3) {
                                    echo "Approved";
                                }
                                if ($builder_data['Builder']['status'] == 4) {
                                    echo "Rejected";
                                }
                                ?>
                            </div>
                        </div>
                        </form>

                    </div>				               
                </div>
            </div>
        </div>
    </div>
</div>