 <div class="container-fluid">
	  
			<div class="row-fluid ">
				<div class="span12">
					<div class="primary-head">
					  <h1 class="page-header">Portfolio <?php echo $report_detail['PortService']['port_name']; ?> Summary </h1>

					</div>

				</div>
			</div>
           
	    <div class="row-fluid">
			<div class="span12">
			<div class="content-widgets gray">
			<div class="widget-container">
			<div class="form-container grid-form form-background">
			
            <div id="build" class="list-detail detail-block target-block" style="padding-bottom:20px;">

                <ul >
                    <li class="col-md-6"><strong>Latest Valuation:</strong> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $report_detail['PortService']['Valuation']; ?></li>
                    <li class="col-md-6"><strong>Latest Overall CAGR:</strong> <?php echo $report_detail['PortService']['CAGR']; ?>%</li>
                    <li class="col-md-6"><strong>Created on:</strong><?php echo $report_detail['PortService']['Created_on']; ?></li>
                    <li class="col-md-6"><strong>View latest report:</strong> <a href="<?php echo @$this->webroot . @$report_detail['PortService']['latest_pdf']; ?>">Pdf Download</a></li>
                </ul>

            </div>
			
			 
            <?php
            if (!empty($properties)) {

                foreach ($properties as $data) {
                    if ($data['Portfolio']['prop_type'] == '1') {
                        $pro_type = "Residential";
                    } else {
                        $pro_type = "Commercial";
                    }
                    ?>
					<div class="widget-head blue">
								<h3><?php echo $pro_type ?> Property in <?php echo $data['Portfolio']['prop_locality']; ?> ,  <?php echo $data['MstAreaCity']['cityName']; ?></h3>
					</div>
					<?php echo $this->Form->create('portfolio', array('id' => "formID11", 'class' => 'form-horizontal left-align')); ?>
					    <div class="row-fluid">
							
														
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong> Area : </strong> <?php echo $data['Portfolio']['prop_area']; ?> /sq feet </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
								
                                <div class="form-control" > <strong>Status : </strong> <?php echo $data['Portfolio']['prop_state']; ?> </div>
                          
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Locality: </strong><?php echo $data['Portfolio']['prop_locality']; ?> </div>
									</div>
								</div>
							</div>
							
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Address : </strong> <?php echo $data['Portfolio']['prop_addr']; ?> </div>
									</div>
								</div>
							</div>
							
					    </div>
					
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Current Valuation : </strong><?php echo $data['Portfolio']['prop_curr_val']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Current Rental: </strong><?php echo $data['Portfolio']['prop_curr_rent']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>5 years value: : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_5yr_val']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>5 years rental : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_5yr_rent']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
					
					
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Project : </strong><?php echo $data['Portfolio']['prop_proj']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Status: </strong><?php echo $data['Portfolio']['prop_stat']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Address: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_5yr_val']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>locality: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_locality']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>State : </strong><?php echo $data['Portfolio']['prop_state']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>City: </strong><?php echo $data['Portfolio']['prop_city']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Description: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_desc']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Configure: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_cfg']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Purchase Price : </strong><?php echo $data['Portfolio']['prop_pprice']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Purchase Date: </strong><?php echo $data['Portfolio']['prop_pdate']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Market Price: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_mprice']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Market Rent: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_mrent']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Desired Action : </strong><?php echo $data['Portfolio']['prop_oth']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Reson: </strong><?php echo $data['Portfolio']['prop_rsn']; ?></div>
									</div>
								</div>
						    </div>
						
						
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						
						
					<!-- <div class="form-actions">
						<button type="submit" id="formid1" class="btn btn-primary">Edit</button>

						</div>-->
					</form>
					
                   
                <?php
                }
            }
            ?>


            <div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
	    </div>
   </div> 	