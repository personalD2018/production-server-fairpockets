<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');

foreach ($freeactivation as $data) {
}
$serviceid = $data['serviceid'];
?>
<script>
$( document ).ready(function() {
	var serviceType = '<?php echo $serviceid;?>'
	if(serviceType=='calculator'){
	$('#FPInventoryManagementBlockId').hide();
		$('#FPCalculatorBlockId').show();
	}
	else{
		$('#FPCalculatorBlockId').hide();
		$('#FPInventoryManagementBlockId').show();
	}
    $('#type_of_service').on('change',function(){
		var selectedId = $(this).val();
		if(selectedId=='calculator'){
			$('#FPInventoryManagementBlockId').hide();
			$('#FPCalculatorBlockId').show();
		}
		else{
			$('#FPCalculatorBlockId').hide();
			$('#FPInventoryManagementBlockId').show();
		}
	});
});
</script>

<div class="container-fluid">

   <!-- <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Free Activation</h3>

            </div>

        </div>
    </div>-->

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
               
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php //echo $this->Form->create('ResearchReport', array('id' => "formID", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>
                       
                        <div class="control-group">
                            <label class="control-label">Select Service</label>
                            <div class="controls">
                                <?php 
								echo $this->Form->input('type_of_service', 
								array(
									'id' => 'type_of_service', 
									'options' => array(
														'calculator' => 'FP Calculator', 
														'inventory' => 'Inventory management'
													),
									'empty' => $serviceid,
									'class' => 'chzn-select small required', 
									'title' => 'Please Select Type Project', 
									'label' => false
									)
								); ?>
                      
							</div>
						</div>
						<!-- ------------start FP Calculator -------------------------->
						<div id="FPCalculatorBlockId">
							<div class="content-widgets gray">
							<div class="widget-head blue">
								<h3>FP Calculator</h3>
							</div>
							<?php echo $this->Session->flash(); ?> 
							<div class="widget-container">
								<div class="form-container grid-form form-background">
									<?php echo $this->Form->create('freeactivation', array('id' => "id_form_project_basic1", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>
								   
                        <div class="control-group">
                            <label class="control-label">No of project</label>
                            <div class="controls">
                                <?php echo $this->Form->input('no_of_projects', array('id' => 'type_of_project', 'options' => array('1' => 'project1', '2' => 'project2','3' => 'project3', '4' => 'project4','5' => 'project5', '6' => 'project6'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select No of Project', 'label' => false)); ?>
                            </div>
                        </div>
                       
                           <div class="control-group">
                            <label class="control-label">Duration of free trial</label>
                            <div class="controls">
                                <?php echo $this->Form->input('duration_of_free_trial', array('id' => 'type_of_project', 'options' => array('1' => '1 day', '2' => '2 day','3' => '3 day', '4' => '4 day'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Duration of paid trial', 'label' => false)); ?>
                            </div>
							</div>
						
                       <!-- <div class="control-group">
                            <label class="control-label">Possession Date</label>
                            <div class="controls">
                                <div id="datetimepicker5" class="input-append">
                                    <?//php echo $this->Form->input('doj2', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>-->
						
						<?php echo $this->Form->input('serviceid', array('id' => '', 'label' => false, 'value' => 'calculator', 'type' => 'hidden')); ?>
						
						
						<?php echo $this->Form->input('servicetype', array('id' => '', 'label' => false, 'value' => 'Free', 'type' => 'hidden')); ?>
						
						<input type="hidden" name="data[freeactivation][id]" value="<?php echo $data['id']; ?>">
						
                        <div class="control-group">
                            <label class="control-label">Sales Employee User</label>
                            <div class="controls">
                                <?php echo $this->Form->input('sales_employees_user', array('div' => false, 'label' => false, 'hiddenField' => false,'options' => array('5' => '5', '10' => '10','25' => '25', '50' => '50','100'=>'100'), 'empty' => 'Select', 'title' => 'Please Enter Sales Employee User', 'class' => 'small required')); ?>
                            </div>
                        </div>
                       
					    <div class="control-group">
                            <label class="control-label">Master Broker User</label>
                            <div class="controls">
                                <?php echo $this->Form->input('master_broker_user', array('div' => false, 'label' => false, 'hiddenField' => false,'options' => array('5' => '5', '10' => '10','25' => '25', '50' => '50','100'=>'100'), 'empty' => 'Select', 'title' => 'Please Select Master Broker User', 'class' => 'small required')); ?>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label">Broker Sales User</label>
                            <div class="controls">
                                <?php echo $this->Form->input('broker_sales_user', array('div' => false, 'label' => false, 'hiddenField' => false,'options' => array('5' => '5', '10' => '10','25' => '25', '50' => '50','100'=>'100'), 'empty' => 'Select', 'title' => 'Please Select Broker Sales User', 'class' => 'small required')); ?>
                            </div>
                        </div>
					                   			
                                          
                      				

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update Calculator</button>

                        </div>
                         <?php
                                echo $this->Form->end();
                                ?>
						</div>
					</div>
				</div>
			</div>
						<!---------------End FP Calculator activation --------------------------->
						
						
				<!------------------------  Start Inventory Management ---------------------------------->
						<div id="FPInventoryManagementBlockId">
                              
							
					<div class="content-widgets gray">
						<div class="widget-head blue">
							<h3>FP Inventory Management</h3>
						</div>
						<?php echo $this->Session->flash(); ?> 
						<div class="widget-container">
						
				
				
						<div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('freeactivation', array('id' => "id_form_project_basic", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>
						<div class="row" id="">
                                <?php
                                echo $this->Form->create(
                                        'freeactivation', array(
                                    'class' => 'fpform',
                                    'role' => 'form',
                                    'id' => 'id_form_project_basic',
                                    'novalidate' => 'novalidate'
                                        )
                                );
                                ?>

                                <div class="col-md-12" style="border-right: 1px dotted #C2C2C2;padding-right: 0px;">



                                    <div class="add-tab-row push-padding-bottom " style=" margin-left: 60px;">
                                        <div class="row">

                                         <!--   <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname" class="label_title">Number of Project<span class="mand_field">*</span></label>
                                                    <?php
                                                    echo $this->Form->input(
                                                            'no_of_projects', array(
                                                        'id' => 'project_name',
                                                        'type' => 'text',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Project Name',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                  
                                                </div>
                                            </div>-->
											
											<div class="control-group">
                            <label class="control-label">No of project</label>
                            <div class="controls">
                                <?php echo $this->Form->input('no_of_projects', array('id' => 'type_of_project', 'options' => array('1' => 'project1', '2' => 'project2','3' => 'project3', '4' => 'project4','5' => 'project5', '6' => 'project6'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select No of Project', 'label' => false)); ?>
                            </div>
                        </div>
                       
                           <div class="control-group">
                            <label class="control-label">Duration of free trial</label>
                            <div class="controls">
                                <?php echo $this->Form->input('duration_of_free_trial', array('id' => 'type_of_project', 'options' => array('1' => '1 day', '2' => '2 day','3' => '3 day', '4' => '4 day'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Duration of paid trial', 'label' => false)); ?>
                            </div>
							</div>
											
											
											<?php echo $this->Form->input('serviceid', array('id' => '', 'label' => false, 'value' => 'inventory', 'type' => 'hidden')); ?>
											
											<?php echo $this->Form->input('servicetype', array('id' => '', 'label' => false, 'value' => 'Free', 'type' => 'hidden')); ?>
											
											
										<!--	<input type="hidden" name="data[freeactivation][builder_id]" value="<?php //echo $builder_id; ?>">-->
											
										<input type="hidden" name="data[freeactivation][id]" value="<?php echo $data['id']; ?>">
                                          <!--  <div class="row">
                                                <div class="col-md-6" style=" margin-left: 30px;">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Duration of Free Trial</label>
                                                        <?php
                                                        echo $this->Form->textarea(
                                                                'duration_of_free_trial', array(
                                                            'id' => 'project_description',
                                                            'class' => 'form-control',
                                                            'placeholder' => 'Enter your Project Description',
                                                            'rows' => '1',
                                                            'label' => false
                                                                )
                                                        );
                                                        ?>
                                                    
                                                    </div>
                                                </div>
                                            </div >-->
                                            <div class="col-md-6" style=" margin-left:0 px ">
												 <div class="form-group">
														<label for="projectname" class="label_title">Sales Employees User</label>
												   
														
													 <div class="controls">
													 <?php echo $this->Form->input('sales_employees_user', array(
																'id' => 'project_highlights',
																'options' => array('5' => '5', '10' => '10','25' => '25', '50' => '50','100'=>'100'), 'empty' => 'Select',
																'class' => 'chzn-select small required', 'title' => 'Please Select Sales Employees User',
																'placeholder' => 'Please Enter Sales Employees User',
																
																'label' => false
																	)); ?>
													 </div>
													   <!-- <div class="error has-error">Box must be filled out</div>-->
												</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname" class="label_title">Master Broker User</label>
                                                    <?php
                                                    echo $this->Form->input(
                                                            'master_broker_user', array(
                                                        'id' => 'land_area',
														'options' => array('5' => '5', '10' => '10','25' => '25', '50' => '50','100'=>'100'), 'empty' => 'Select',
                                                        //'type' => 'text',
                                                        'class' => 'chzn-select small required', 'title' => 'Please Select Master Broker User',											
                                                        'placeholder' => 'Please Enter Master Broker User Name',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
													
                                                  <!--  <div class="error has-error">Box must be filled out</div>-->
                                                </div>
                                            </div>

											 <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname" class="label_title">Broker Sales User</label>
                                                    <?php
                                                    echo $this->Form->input(
                                                            'broker_sales_user', array(
                                                        'id' => 'land_area',
                                                      //  'type' => 'text',
														'options' => array('5' => '5', '10' => '10','25' => '25', '50' => '50','100'=>'100'), 'empty' => 'Select',
                                                        'class' => 'chzn-select small required', 'title' => 'Please Select Broker Sales User',
                                                        'placeholder' => 'Please Enter Broker sales user name',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
													
													
                                                <!--    <div class="error has-error">Box must be filled out</div>-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="add-tab-row push-padding-bottom text-center" style=" margin-left: 60px;">
										<div class="space-m"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary btn-red">
                                                    Update Inventory</button>
                                            </div>
										<div class="space-m"></div>
                                        </div>

                                    </div>


                                </div>
                                <?php //echo $this->Form->input('userproject', array('id' => 'userproject', 'label' => false, 'value' => 1, 'type' => 'hidden')); ?>	

                                <?php
                                echo $this->Form->end();
                                ?>

                            </div>
                       
                    </div>
                </div>
            </div>
						
						
						</div>
				<!------------------  start Inventory Management ----------------------------------->
						
					</div>

				</div>	



	<script>

    $(document).ready(function () {
              $('#id_form_project_basic1').validate({
              submitHandler: function (form) {
					//var proj =$('#id_form_project_basic').serialize()
					//alert(proj);
                $.ajax
                        ({
                            type: 'POST',
                            url: '<?php echo $this->webroot; ?>admin/accounts/edit_activation',
                            data: $('#id_form_project_basic1').serialize(), // serializes the form's elements.
                            success: function (data){
								alert('Calculator data update success');
								window.open(
								 '<?php echo $this->Html->url(array('controller' => 'Accounts', 'action' => 'activation_list')); ?>',
								 '_self' // <- This is what makes it open in a new window.
								);
                             //window.location = 'http://localhost/bakup-testing/admin/accounts/activation_list';
                            }
                        });
               // To remain on same page after submit
                return false;
            }

        });
    }); 
</script>			
				

<script>

    $(document).ready(function () {
              $('#id_form_project_basic').validate({
              submitHandler: function (form) {
					//var proj =$('#id_form_project_basic').serialize()
					//alert(proj);
                $.ajax
                        ({
                            type: 'POST',
                            url: '<?php echo $this->webroot; ?>admin/accounts/edit_activation',
                            data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
                            success: function (data)
                            {
								alert('inventory data update success');
								//alert(data);
									window.open(
								 '<?php echo $this->Html->url(array('controller' => 'Accounts', 'action' => 'activation_list')); ?>',
								 '_self' // <- This is what makes it open in a new window.
								);
								//alert('why data not save please?');
                             
                            }
                        });
               // To remain on same page after submit
                return false;
            }

        });
    }); 
</script>
				