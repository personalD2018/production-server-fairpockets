    <div class="page-content">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Transaction History</h1>
                </div>
            </div>

            <div id="build" class="list-detail detail-block target-block">

                <table width="100%" class="table table-striped table-responsive">

                    <thead>
                        <tr style="background-color:#29333d;color:#fff;">

                            <th>Transaction ID</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <tbody>
                       

                    </tbody>

                </table>

            </div>	

        </div>


