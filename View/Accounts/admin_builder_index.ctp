<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Builders</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Builder Organization</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Contact Person</th>
                                <th class="center">Landline</th>
                                <th class="center">Web</th>
                                <th class="center">Year</th>
                                <th class="center">Status</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($builder as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['Builder']['borg_contact']; ?></td>
                                    <td class="center"><?php echo $data['Builder']['borg_landline']; ?></td>
                                    <td class="center"><?php echo $data['Builder']['borg_web']; ?></td>

                                    <td class="center"><?php echo $data['Builder']['borg_year']; ?></td>
                                    <td class="center"><?php
                                        if ($data['Builder']['status'] == 2) {
                                            echo "Pending";
                                        }
                                        if ($data['Builder']['status'] == 3) {
                                            echo "Approved";
                                        }
                                        if ($data['Builder']['status'] == 4) {
                                            echo "Rejected";
                                        }
                                        ?></td>


                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/editBuilder/<?php echo $data['Builder']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/viewBuilder/<?php echo $data['Builder']['id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail" onclick="window.location.href = '<?php echo $this->webroot; ?>admin/users/viewemployee/<?php echo $data['Builder']['id']; ?>'"><i class="icon-eye-open"></i></button></a>


                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


