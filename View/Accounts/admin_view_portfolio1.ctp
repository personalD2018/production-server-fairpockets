<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Portfolio Services</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Portfolio Services</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Portfolio Name</th>
                                <th class="center">Payment</th>
                                <th class="center">Serivce Status</th>

                                <th class="center">CAGR</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($PortService as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['PortService']['port_name']; ?></td>
                                    <td class="center"><?php echo $data['PortService']['payment']; ?></td>
                                    <td class="center"><?php
                                        if ($data['PortService']['status'] == '1') {
                                            $payment = "Approved";
                                        } else if ($data['PortService']['status'] == '2') {
                                            $payment = "Rejected";
                                        } else if ($data['PortService']['status'] == '3') {
                                            $payment = "closed";
                                        } else if ($data['PortService']['status'] == '3') {
                                            $payment = "Payment Pending";
                                        } else {
                                            $payment = "Under Approval";
                                        }
                                        echo $payment;
                                        ?></td>


                                    <td class="center"><?php echo $data['PortService']['CAGR']; ?></td>					

                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/edit_portfolio1/<?php echo $data['PortService']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a target="_blank" href="<?php echo $this->webroot; ?>accounts/Portfolio_adminViewReportDetail1/<?php echo $data['PortService']['id']; ?>">
                                                    <button class="btn btn-danger"  title="View Detail"><i class="icon-eye-open"></i></button></a>
												<a target="_blank" href="<?php echo $this->webroot; ?>pages/pdftest/<?php echo $data['PortService']['id']; ?>" download>
                                                    <button class="btn"><i class="fa fa-download"></i> Download</button></a>	

                                            </div>
                                        </div></td>	
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>
