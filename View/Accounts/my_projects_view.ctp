<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
</style>
<?php //echo '<pre>'; print_r($project_rec); ?>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Project Details</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('ShareProjectsHistory', array(
			'url' => '/accounts/projects/share-project-info-update/'.$project_id,
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		//echo '<pre>'; print_r($project_rec);
		//$this->request->data['ShareProjectsHistory']['project_name'];
	?>
	
	<input type="hidden" name="data[ShareProjectsHistory][builder_id]" value="<?php echo $builder_id; ?>">
	<input type="hidden" name="data[ShareProjectsHistory][project_id]" value="<?php echo $project_id; ?>">
    <div class="row">
        <div class="col-md-12" style="border: 1px solid #bdbdbd;">
            <div class="row cell-content">
                <div class="col-md-4">Project name</div>
                <div class="col-md-8">
				<?php echo $this->request->data['ShareProjectsHistory']['project_name'];?>
				<?php	echo $this->Form->input('project_name', array(
							'class' => 'project_name','type' => 'hidden','label' => false,
							'value' => $project_rec['projects']['InvProject']['project_name']));
				?>
				</div>
            </div>
            <div class="row cell-content">
                <div class="col-md-4">Project address</div>
                <div class="col-md-8">
				<?php   echo $this->request->data['ShareProjectsHistory']['project_address'];?>
				<?php	echo $this->Form->input('project_address', array(
							'class' => 'project_address','type' => 'hidden','label' => false,
							'value' => $project_address));
				?>
				</div>
            </div>
            <div class="row cell-content">
                <div class="col-md-4">Project city</div>
                <div class="col-md-8">
				<?php 	echo $this->request->data['ShareProjectsHistory']['city_data'];;?>
				<?php	echo $this->Form->input('city_data', array(
							'class' => 'city_data','type' => 'hidden','label' => false,
							'value' => $project_rec['projects']['InvProject']['city_data']));
				?>
				</div>
            </div>
            <div class="row cell-content">
                <div class="col-md-4">Sales contact name</div>
                <div class="col-md-8">
				<?php 	echo $this->request->data['ShareProjectsHistory']['office_sales_ctname']?>
				<?php	echo $this->Form->input('office_sales_ctname', array(
							'class' => 'office_sales_ctname','type' => 'hidden','label' => false,
							'value' => $project_rec['project_office']['ProjectOfficeUse']['office_sales_ctname']));
				?>
				</div>
            </div>
            <div class="row cell-content">
                <div class="col-md-4">Sales contact number</div>
                <div class="col-md-8">
				<?php 	echo $this->request->data['ShareProjectsHistory']['office_sales_ctmobile']?>
				<?php	echo $this->Form->input('office_sales_ctmobile', array(
							'class' => 'office_sales_ctmobile','type' => 'hidden','label' => false,
							'value' => $project_rec['project_office']['ProjectOfficeUse']['office_sales_ctmobile']));
				?>
				</div>
            </div>
            <div class="row cell-content">
                <div class="col-md-4">Sales contact email</div>
                <div class="col-md-8">
				<?php 	echo $this->request->data['ShareProjectsHistory']['office_sales_ctemail']?>
				<?php	echo $this->Form->input('office_sales_ctemail', array(
							'class' => 'office_sales_ctemail','type' => 'hidden','label' => false,
							'value' => $project_rec['project_office']['ProjectOfficeUse']['office_sales_ctemail']));
				?>
				</div>
            </div>
            <?php
                $discount_type = "";
                if(isset($project_rec['project_office']['ProjectOfficeUse']['price_reg_unit']) && $project_rec['project_office']['ProjectOfficeUse']['price_reg_unit'] == '1'){
                    $discount_type = "Amount";
                } else if(isset($project_rec['project_office']['ProjectOfficeUse']['price_reg_unit']) && $project_rec['project_office']['ProjectOfficeUse']['price_reg_unit'] == '2'){
                    $discount_type = "Percent";
                } else if(isset($project_rec['project_office']['ProjectOfficeUse']['price_reg_unit']) && $project_rec['project_office']['ProjectOfficeUse']['price_reg_unit'] == '3'){
                    $discount_type = "Per Sq feet";
                }
            ?>
			<?php if($this->request->data['ShareProjectsHistory']['office_maxdis_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Max. discount for customers</div>
                <div class="col-md-4">
				<?php   echo $this->request->data['ShareProjectsHistory']['office_maxdis'];?>&nbsp;<?php echo $discount_type;?>
				<?php	echo $this->Form->input('office_maxdis', array(
							'class' => 'office_maxdis','type' => 'hidden','label' => false,
							'value' => $project_rec['project_office']['ProjectOfficeUse']['office_maxdis'].$discount_type));
				?>
				</div>
				<?php
                $brokerage_type = "";
                if(isset($project_rec['project_office']['ProjectOfficeUse']['price_brokage_unit']) && $project_rec['project_office']['ProjectOfficeUse']['price_brokage_unit'] == '1'){
                    $brokerage_type = "Amount";
                } else if(isset($project_rec['project_office']['ProjectOfficeUse']['price_brokage_unit']) && $project_rec['project_office']['ProjectOfficeUse']['price_brokage_unit'] == '2'){
                    $brokerage_type = "Percent";
                } else if(isset($project_rec['project_office']['ProjectOfficeUse']['price_brokage_unit']) && $project_rec['project_office']['ProjectOfficeUse']['price_brokage_unit'] == '3'){
                    $brokerage_type = "Per Sq feet";
                }
            ?>
			
				<div class="col-md-4">
				<?php echo $this->Form->input('office_maxdis_check', array(
                                    'id' => 'office_maxdis_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
			</div>
			<?php } ?>
            
			<?php if($this->request->data['ShareProjectsHistory']['office_brkg_fp_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Brokerage</div>
                <div class="col-md-4">
				<?php echo  $project_rec['project_office']['ProjectOfficeUse']['office_brkg_fp'];?>&nbsp;<?php echo $brokerage_type;?>
				<?php	echo $this->Form->input('office_brkg_fp', array(
							'class' => 'office_brkg_fp','type' => 'hidden','label' => false,
							'value' => $project_rec['project_office']['ProjectOfficeUse']['office_brkg_fp'].$brokerage_type));
				?>
				</div>
				<div class="col-md-4">
				<?php echo $this->Form->input('office_brkg_fp_check', array(
                                    'id' => 'office_brkg_fp_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
			</div>
			<?php } ?>
			<?php if($this->request->data['ShareProjectsHistory']['payment_release_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Payment release timeline</div>
                <div class="col-md-4">
				<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_pay_time'];?>
				</div>
				<div class="col-md-4">
				<?php echo $this->Form->input('payment_release_check', array(
                                    'id' => 'payment_release_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
            </div>
			<?php } ?>
			<?php if($this->request->data['ShareProjectsHistory']['brochure_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Brochure</div>
                <div class="col-md-4">
				
				<?php 
				$img_extn = explode(".", strtolower($project_rec['project_office']['ProjectOfficeUse']['office_brochure']));
				$img_extn[1];
				?>
				<?php if($img_extn[1] == 'pdf'){ ?>
					<a href="<?php echo $this->webroot; ?>upload/project_office_brochure/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_brochure']; ?>" download><?php echo $project_rec['project_office']['ProjectOfficeUse']['office_brochure']; ?></a>
				<?php }else{ ?>
					<a href="<?php echo $this->webroot; ?>upload/project_office_brochure/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_brochure']; ?>" download>
					<img src="<?php echo $this->webroot; ?>upload/project_office_brochure/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_brochure']; ?>" alt="thumb" width="50" height="50">
						</a>
				<?php } ?>
				
				</div>
				<div class="col-md-4">
				<?php echo $this->Form->input('brochure_check', array(
                                    'id' => 'brochure_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
            </div>
			<?php } ?>
			<?php if($this->request->data['ShareProjectsHistory']['price_list_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Price list</div>
                <div class="col-md-4">
				
				<?php 
				$img_extn1 = explode(".", strtolower($project_rec['project_office']['ProjectOfficeUse']['office_rtcard']));
				$img_extn1[1];
				?>
				<?php if($img_extn1[1] == 'pdf'){ ?>
					<a href="<?php echo $this->webroot; ?>upload/project_office_rtcard/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_rtcard']; ?>" download><?php echo $project_rec['project_office']['ProjectOfficeUse']['office_rtcard']; ?></a>
				<?php }else{ ?>
				<a href="<?php echo $this->webroot; ?>upload/project_office_rtcard/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_rtcard']; ?>" download>
					<img src="<?php echo $this->webroot; ?>upload/project_office_rtcard/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_rtcard']; ?>" alt="thumb" width="50" height="50">                                        
				</a>
				<?php } ?>
				
				</div>
				<div class="col-md-4">
				<?php echo $this->Form->input('price_list_check', array(
                                    'id' => 'price_list_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
            </div>
			<?php } ?>
			<?php if($this->request->data['ShareProjectsHistory']['builder_buyer_agreement_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Builder buyer agreement</div>
                <div class="col-md-4">
				
				<?php 
				$img_extn2 = explode(".", strtolower($project_rec['project_office']['ProjectOfficeUse']['office_bbagrrement']));
				$img_extn2[1];
				?>
				<?php if($img_extn2[1] == 'pdf'){ ?>
					<a href="<?php echo $this->webroot; ?>upload/project_office_bbagrrement/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_bbagrrement']; ?>" download><?php echo $project_rec['project_office']['ProjectOfficeUse']['office_bbagrrement']; ?></a>
				<?php }else{ ?>
					<img src="<?php echo $this->base; ?>/upload/project_office_bbagrrement/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_bbagrrement']; ?>" alt="thumb" width="50" height="50">                                        
				<?php } ?>
				
				</div>
				<div class="col-md-4">
				<?php echo $this->Form->input('builder_buyer_agreement_check', array(
                                    'id' => 'builder_buyer_agreement_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
            </div>
			<?php } ?>
			<?php if($this->request->data['ShareProjectsHistory']['upload_doc_check'] == 1){  ?>
            <div class="row cell-content">
                <div class="col-md-4">Upload any additional document</div>
                <div class="col-md-4">
				
				<?php 
				$img_extn3 = explode(".", strtolower($project_rec['project_office']['ProjectOfficeUse']['office_adocument']));
				$img_extn3[1];
				?>
				<?php if($img_extn3[1] == 'pdf'){ ?>
					<a href="<?php echo $this->webroot; ?>upload/project_office_adocument/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_adocument']; ?>" download><?php echo $project_rec['project_office']['ProjectOfficeUse']['office_adocument']; ?></a>
				<?php }else{ ?>
					<img src="<?php echo $this->base; ?>/upload/project_office_adocument/<?php echo $project_rec['project_office']['ProjectOfficeUse']['office_adocument']; ?>" alt="thumb" width="50" height="50">                                        
				<?php } ?>
				
				</div>
				<div class="col-md-4">
				<?php echo $this->Form->input('upload_doc_check', array(
                                    'id' => 'upload_doc_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
            </div>
			<?php } ?>
            <div class="row cell-content">
                <div class="col-md-4">Message</div>
                <div class="col-md-4">
				<?php   echo $this->request->data['ShareProjectsHistory']['message'];?></div>
            </div>
            <div class="row cell-content" style="display:none;">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share with Brokers" class="btn btn-primary">
                        </div>
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_SE]" id="btn_share_SE" value="Share with Sales Employees" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>