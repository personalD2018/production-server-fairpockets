<style>
.select-style select{
	padding:10px 5px !important;
}
</style>
<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Add Sales Employees</h2>
            <br />
        </div>	

        <div class="add_property_form">

            

            <?php
            echo $this->Form->create(
                    'SalesUser', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Sales employees Information</h3>
                    <div class="add-expand" id="id_proj_pricing_exp"></div>
                </div>

                <div class="add-tab-content1 detail-block" id="id_proj_pricing_content">
					
					
					
					<div class="add-tab-row push-padding-bottom">
						

						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Name </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'name', array(
                                            'id' => 'name',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Email <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'email', array(
                                            'id' => 'email',
                                            'type' => 'text',
											'readonly' => 'readonly',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">Mobile No <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'mobile', array(
                                            'id' => 'mobile',
                                            'type' => 'text',
											'readonly' => 'readonly',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>                          

                        </div>							

                        <br>

                        <h3>Projects</h3>                        
						<input type="hidden" id="id_cloneProjectBspChargeCount" value="10" >
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab" >
							<?php
                            if (!empty($this->request->data['bspcharges'])) {
								//echo '<pre>'; print_r($this->request->data['bspcharges']);die();
                                for ($i = 1; $i <= count($this->request->data['bspcharges']) / 4; $i++) {
                                    ?>
									<div class="row cloneProjectBspCharge" id="cloneProjectBspCharge<?php echo $i; ?>" >
										
										<div class="col-sm-3">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">Builder</label>

												<div class="select-style">
												<br>
													<?php
													//$getProjects = array('1'=>'Mahagun','2'=>'Elite');
													
													
													echo $this->Form->input(
															'bspcharges.builderdropdown'.$i, array(
														'id' => 'builderdropdown'.$i,
														'options' => $buildersDropdown,
														'class' => 'selectpicker bs-select-hidden required',
														'label' => false,
														'empty' => 'Select builder'
															)
													);
													?>

												</div>
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group"> 
												<label for="property-price-before" class="label_title">Projects</label>
												<div class="select-style"><br>
													<?php

													$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													$bspFromToOptions[0]='Ground';
													
													$selectedProject = $this->Number->getProjectNameByProjectId($this->request->data['bspcharges']['from_project'.$i.'']);
													//echo '<pre>'; print_r($selectedProject[0]['Project']['project_name']);
													$getProjects = array($this->request->data['bspcharges']['from_project'.$i.'']=>$selectedProject[0]['Project']['project_name']);

													echo $this->Form->input(
															'bspcharges.from_project'.$i, array(
														'id' => 'from_project'.$i,
														'options' => $getProjects,
														'class' => 'selectpicker bs-select-hidden required',
														'label' => false,
														'empty' => 'Select Project'
															)
													);
													?>

												</div>
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group">

												 <label for="price_bsp1" class="label_title">Maximum Discount Allowed</label>
												<?php
												echo $this->Form->input(
														'bspcharges.proj_disc'.$i, array(
													'id' => 'proj_disc'.$i,
													'type' => 'text',
													'class' => 'form-control box_price required',
													'label' => false
														)
												);
												?>

											</div>
										</div>
										
										<div class="col-sm-2">
											<div class="form-group">

												 <label for="price_bsp1" class="label_title">Discount Unit</label>
												<div class="select-style"><br>
												<?php

													//$bspFromToOptions=array('percentage'=>'');

													echo $this->Form->input(
															'bspcharges.to_floor'.$i, array(
														'id' => 'to_floor'.$i,
														'options' => array(
																'1' => 'Amount',
																'2' => 'Percentage',
																'3' => 'Per Sq feet'
															),
														'class' => 'selectpicker bs-select-hidden required',
														'label' => false,
														'empty' => 'Select Unit'
															)
													);
													?>
													</div>
											</div>
										</div>
										
										
									
										
										<div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
										
									</div>
									<?php
                                }
                            } else {
                                ?>

								<div class="row cloneProjectBspCharge" id="cloneProjectBspCharge1" >
										
										
										<div class="col-sm-3">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">Builder</label>

												<div class="select-style">
												<br>
													<?php
													//$getProjects = array('1'=>'Mahagun','2'=>'Elite');
													echo $this->Form->input(
															'bspcharges.builderdropdown1', array(
														'id' => 'builderdropdown1',
														'options' => $buildersDropdown,
														'class' => 'selectpicker bs-select-hidden required',
														'label' => false,
														'empty' => 'Select builder'
															)
													);
													?>

												</div>
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">Projects</label>

												<div class="select-style"><br>


													<?php

													$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													$bspFromToOptions[0]='Ground';

													echo $this->Form->input(
															'bspcharges.from_project1', array(
														'id' => 'from_project1',
														'options' => $getProjects,
														'class' => 'selectpicker bs-select-hidden required',
														'label' => false,
														'empty' => 'Select Project'
															)
													);
													?>

												</div>
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group">

												 <label for="price_bsp1" class="label_title">Maximum Discount Allowed</label>
												<?php
												echo $this->Form->input(
														'bspcharges.proj_disc1', array(
													'id' => 'proj_disc1',
													'type' => 'text',
													'class' => 'form-control box_price required',
													'label' => false,
													'value' => ''
														)
												);
												?>

											</div>
										</div>
										
										<div class="col-sm-2">
											<div class="form-group"><br>

												 <label for="price_bsp1" class="label_title">Discount Unit</label>
												<div class="select-style">
												<?php

													//$bspFromToOptions=array('percentage'=>'');

													echo $this->Form->input(
															'bspcharges.to_floor1', array(
														'id' => 'to_floor1',
														'options' => array(
																'1' => 'Amount',
																'2' => 'Percentage',
																'3' => 'Per Sq feet'
															),
														'class' => 'selectpicker bs-select-hidden required',
														'label' => false,
														'empty' => 'Select Unit'
															)
													);
													?>
													</div>
											</div>
										</div>
										
										
									
										
										<div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
										
									</div>
							<?php } ?>
							
						</div>
						<div class="row" style="padding-bottom:14px;">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRow" onclick="cloneProjectBspChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
						<br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>
						<?php
							echo $this->Form->end();
						?>
						
                    </div>
					
					
										
                    							

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>

        </div>		
    </div> 
<?php
echo $this->Html->script('add_more_app_broker');
//echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');

echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		var rulespTarea = {
					required : true,
					min:1
			
		};
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_project_pricing').validate({
			
			// Specify the validation rules
			rules: {
				'data[Portmgmt][prop_addr]': rulesMTarea,
				'data[Portmgmt][prop_locality]': rulesMTarea,
				'data[Portmgmt][prop_desc]': rulesMTarea,
				'data[Portmgmt][prop_area]': rulespTarea
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[Portmgmt][prop_addr]': messageMTarea,
				'data[Portmgmt][prop_locality]': messageMTarea,
				'data[Portmgmt][prop_desc]': messageMTarea,
				'data[Portmgmt][prop_area]': messageMTarea
				
			}
		});	
	");

?>

<script>
    $(document).ready(function () {
        $("#builderdropdown1").on('change', function () {
			//alert('heelo2');
			var builder_id = $(this).val();
            $("#from_project1").find('option').remove();
            if (builder_id) {
                var dataString = 'builder_id=' + builder_id;
                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "accounts", "action" => "getbuilderprojects")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#from_project1"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#from_project1"));
                        });
                    }
                }).responseJSON;
            }
        });
		
		
		
		
		
    });


</script>


