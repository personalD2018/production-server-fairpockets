	
	<?php //echo "<pre>";print_r($slist);?>
		<div class="row">
			<div class="col-md-8">
			<h1 class="page-header">Calculator channel Partner</h1>
			</div>
		</div>
		<div class="space-x"></div>
		
		<div class="row">
		<div class="col-md-8">
			&nbsp;
		</div>
		<div class="col-md-4" style="text-align:right; ">
			<?php 
				$ExportCSVImage = $this->Html->image('ExportCSV.png',array('border'=>0));
				echo $this->Html->link($ExportCSVImage,array('controller'=>'accounts','action'=>'channel_partner_lists_csv'),array('escape' => false,'title'=>'Export csv'));
			?>
		</div>
		</div>
		
		<div class="row">
			<form class="search" id="search" method="post"  action="calculatorChannelPartner" >
			
			<div class="col-md-4">
					<?php echo $this->Form->input('name',array('maxlength'=>'50','size'=>'20','label'=>'','div'=>false,'class'=>'keyword form-control',
							'placeholder'=>'Search here'))?>
			</div>
			
				<div class="col-md-3">
				<label>FROM :</label>
					<div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                        <?php
                                        echo $this->Form->input(
                                                'keyword1', array(
                                            'id' => 'keyword1',
                                            'class' => 'form-control enddate',
                                            'size' => '16',
                                            'value' => '',
                                            'readonly' => 'readonly',
                                            'style' => 'cursor : pointer',
                                            'type' => 'text',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                        <span class="add-on removeicon"><i class="icon-remove"></i></span>
                                        <span class="add-on calicon"><i class="icon-th"></i></span>
                                    </div>
				</div>
				<div class="col-md-3">
				
					<div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<label>TO :</label>
                                        <?php
                                        echo $this->Form->input(
                                                'keyword2', array(
                                            'id' => 'keyword2',
                                            'class' => 'form-control enddate',
                                            'size' => '16',
                                            'value' => '',
                                            'readonly' => 'readonly',
                                            'style' => 'cursor : pointer',
                                            'type' => 'text',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                        <span class="add-on removeicon"><i class="icon-remove"></i></span>
                                        <span class="add-on calicon"><i class="icon-th"></i></span>
                                   </div>
				</div> 
				
				<div class="col-md-2" style=" margin-top: 22px;">
					<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
				</div>		
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="space-x"></div>
		<div id='inventory_list' class="left columnWidth100">
		<?php echo $this->element("inventory/broker_list"); 
		//echo $this->element("accounts/salesEmployee");
		?>		
		</div>
		<div class="clear"></div>
		
		
	</div>
	
		<?php
echo $this->Html->script('add-project');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');

echo $this->Html->scriptBlock("
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1);
		// Handling Date input from calendar
		$('#keyword1,#keyword2').datetimepicker({
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});

	");

// Script  : To set validation rules for FORMS

?>