<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Property Services</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Property Services</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Service Name</th>
                                <th class="center">Payment</th>
                                <th class="center">Serivce Status</th>

                                <th class="center">Task</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($PropService as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['PropService']['prop_sname']; ?></td>
                                    <td class="center"><?php echo $data['PropService']['payment']; ?></td>
                                    <td class="center"><?php
                                        if ($data['PropService']['status'] == '1') {
                                            $payment = "Approved";
                                        } else if ($data['PropService']['status'] == '2') {
                                            $payment = "Rejected";
                                        } else if ($data['PropService']['status'] == '3') {
                                            $payment = "closed";
                                        } else if ($data['PropService']['status'] == '4') {
                                            $payment = "Payment Pending";
                                        } else {
                                            $payment = "Under Approval";
                                        }
                                        echo $payment;
                                        ?></td>


                                    <td class="center">



                                        <?php echo $data['PropService']['services']; ?></td>					

                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/edit_property_service/<?php echo $data['PropService']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a target="_blank" href="<?php echo $this->webroot; ?>accounts/Property_ServiceDetails/<?php echo $data['PropService']['id']; ?>">
                                                    <button class="btn btn-danger"  title="View Detail"><i class="icon-eye-open"></i></button></a>

                                                <a href="<?php echo $this->webroot; ?>admin/accounts/addservicedetail/<?php echo $data['PropService']['id']; ?>">
                                                    <button class="btn btn-primary" title="Add Service Detail"><i class=" icon-plus-sign-alt"></i></button></a>
                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


