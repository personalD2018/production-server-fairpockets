<?php
$fillterOptions = array("keyword" => $keyword, "scriteria" => $scriteria);
$this->Paginator->options(array(
    'update' => '#inventory_list',
    'evalScripts' => true,
    'url' => $fillterOptions,
    'before' => $this->Js->get('#loaderID')->effect(
            'fadeIn', array('buffer' => false)
    ),
    'complete' => $this->Js->get('#loaderID')->effect(
            'fadeOut', array('buffer' => false)
    ),
));
$ascImage = $this->Html->image('up_arrow.gif', array('border' => 0));
$descImage = $this->Html->image('down_arrow.gif', array('border' => 0));
//print_r($client_id);
 $client_id1 = implode(',' , $client_id);
?>
					
			<div class="row">
					<div class="col-md-8">
						<h1 class="page-header">Listed Sales User Created By Broker</h1>
					</div>

			
					<div class="col-md-4">
						&nbsp;
					</div>
					<div class="col-md-4" style="text-align:right; ">
						<?php 
							$ExportCSV = $this->Html->image('export-csv.png',array('border'=>0));
							echo $this->Html->link('ExportCSV',array('controller'=>'accounts','action'=>"brokerPersonDetailsCreatedByBrokerCsv/$client_id1 "),array('escape' => false,'title'=>'Export csv'));
						?>
					</div>
			</div>
		
		<table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
						<th>Name of the client</th>
						<th>Email id</th>
                        <th>Mobile Number</th>
                        <!--<th>No of sales user created by broker</th>-->
						<th>No of proposal Share</th>
                        <!--<th>Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
				//	echo "<pre>";print_r($leadshistoriesdata);die;
                       // foreach ($leadshistoriesdata as $key=>$row):
					   foreach ($leadshistoriesdata as $key=>$row):					   
						//echo $no_sale_emp=count($row);
                    ?>
				    <tr>										
						  <td><?php echo isset($row['sales_users']['name']) ? $row['sales_users']['name'] : "N/A";?></td>
						  <td><?php echo isset($row['sales_users']['email']) ? $row['sales_users']['email'] : "N/A";?></td>
						  <td><?php echo isset($row['sales_users']['mobile']) ? $row['sales_users']['mobile'] : "N/A";?></td>
						  <td><?php echo $this->Number->getCountOfProposal($row['sales_users']['id']); ?></td>
					</tr> 
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
			
			<div class="pagination">
		
    <?php if ($this->Paginator->numbers()) {
        echo $this->Paginator->prev(); ?>
        <?php echo $this->Paginator->numbers(); ?>
        <?php echo $this->Paginator->next();
    } ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>