	<style>
	.keyword{
	    background-color: #fec32f;
    border-radius: 8px 8px 8px 8px;
    color: #ffffff;
    float: left;
    font-size: 0.9em;
    
	}
	</style>
	
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">List of Inventories</h1>
		</div>
				</div>
		<div class="space-x"></div>
		<div class="row">
			<form class="search" id="search" method="post"  action="inventory1" >
			<div class="col-md-3">
					<?php $optArr = array(''=>'--Search For--','project_name'=>'Project Name','tower_name'=>'Tower Name'); ?>	
						<div class="select-style ">
                                        
                                    
						<?php echo $this->Form->select('InvInventory.scriteria',$optArr,array('id'=>'InvInventoryScriteria','empty'=>false,'div'=>false,'class'=>'searchControlSelect'),false); ?>
					</div>
				</div>
			<div class="col-md-3">
					<?php echo $this->Form->input('InvInventory.keyword',array('maxlength'=>'50','size'=>'20','label'=>'','div'=>false,'class'=>'keyword form-control',
							'placeholder'=>'Enter search keyword...'))?>
				</div>			
				
				<!--<div class="col-md-2">
					<?php //echo $this->Form->input('InvInventory.keyword1',array('maxlength'=>'50','size'=>'20','label'=>'','div'=>false,'class'=>'keyword form-control',
							//'placeholder'=>'From Date'))?>
				</div>
				<div class="col-md-2">
					<?php //echo $this->Form->input('InvInventory.keyword2',array('maxlength'=>'50','size'=>'20','label'=>'','div'=>false,'class'=>'keyword form-control',
							//'placeholder'=>'End Date'))?>
				</div>-->
				
				<div class="col-md-2">
					<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
				</div>		
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="space-x"></div>
		<div id='inventory_list' class="left columnWidth100">
		<?php echo $this->element("inventory/inv_list"); ?>		
		</div>
		<div class="clear"></div>
		
		
	</div>
	
		