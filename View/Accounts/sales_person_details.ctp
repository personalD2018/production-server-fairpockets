	<?php $paginator = $this->Paginator;?>
	<?php //echo '<pre>'; print_r($leadshistoriesdata); die(); ?>
	
		<div class="row">
			<div class="col-md-8">
				<h1 class="page-header">List of Sales Employees</h1>
			</div>
			<div class="col-md-4">
				<div class="user_panel_search">
					<form class="search" id="search" method="post"  action="" >
						<div class="input-group custom-search-form">
							<input type="text" name="sname" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
				</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		<div class="row">
		<div class="col-md-8">
			&nbsp;
		</div>
		<div class="col-md-4" style="text-align:right; ">
			<?php 
				$ExportCSVImage = $this->Html->image('ExportCSV.png',array('border'=>0));
				echo $this->Html->link($ExportCSVImage,array('controller'=>'accounts','action'=>'sales_employees_details_lists_csv/'.$salesuserid),array('escape' => false,'title'=>'Export csv'));
			?>
		</div>
		</div>
		<div class="space-x"></div>
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name of sales persons</th>	
						<th>Email</th>
                        <th>Mobile Number</th>
						<th>Project Name</th>
						<th>Created Date</th>
                       <!-- <th>No of Proposal Shared</th>-->
						<th>Proposal Pdf</th>
                 
                    </tr>
                </thead>
				  <tbody>
                    <?php
					//echo "<pre>";print_r($this->params);
                       // foreach ($salesempdata as $key=>$row):
					   if(empty($slist)){
						  // echo "Empty data".print_r($list);
						   //-----------------------------------------------------------
						     foreach ($leadshistoriesdata as $key=>$row):	
                    ?>
                     <tr>
					
						<td><?php echo isset($row['AppClientLeadsHistory']['name']) ? $row['AppClientLeadsHistory']['name'] : "N/A";?></td>
                       
					   <td><?php echo isset($row['AppClientLead']['email']) ? $row['AppClientLead']['email'] : "N/A";?></td>
                   
                        <td><?php echo isset($row['AppClientLead']['mobile']) ? $row['AppClientLead']['mobile'] : "N/A";?></td>
					<td><?php echo isset($row['Project']['project_name']) ? $row['Project']['project_name'] : "N/A";?></td>
					<td><?php echo isset($row['AppClientLeadsHistory']['created']) ? $row['AppClientLeadsHistory']['created'] : "N/A";?></td>
                        <!--<td><?php echo $this->Number->getCountOfPdfs($row['AppClientLead']['id'],$row['AppClientLead']['email'],$row['AppClientLead']['mobile']); ?></td>-->
						<td><a href="<?php echo isset($row['AppClientLeadsHistory']['pdf_link']) ? $row['AppClientLeadsHistory']['pdf_link'] : "N/A";?>">Download pdf</a> </td>
                   
                       
                    </tr>
                    <?php
                        endforeach;
					   
						   //----------------------------------------------------------
					   }
					   else{
						 //  echo "Data Comming".print_r($slist);
						   //-------------------------------------
						     foreach ($slist as $key=>$row):
                    ?>
                    <tr>
					
						<td><?php echo isset($row['AppClientLeadsHistory']['name']) ? $row['AppClientLeadsHistory']['name'] : "N/A";?></td>
                       
					   <td><?php echo isset($row['AppClientLead']['email']) ? $row['AppClientLead']['email'] : "N/A";?></td>
                   
                        <td><?php echo isset($row['AppClientLead']['mobile']) ? $row['AppClientLead']['mobile'] : "N/A";?></td>
                   
                        <!--<td><?php echo $this->Number->getCountOfPdfs($row['AppClientLead']['id'],$row['AppClientLead']['email'],$row['AppClientLead']['mobile']); ?></td>-->
						<td><a href="<?php echo isset($row['AppClientLeadsHistory']['pdf_link']) ? $row['AppClientLeadsHistory']['pdf_link'] : "N/A";?>">Download pdf</a> </td>
                   
                       
                    </tr>
                    <?php
                        endforeach;
						   //--------------------------------------
					   }
					 
                    ?>
                </tbody>
				
                
            </table>
		<div class="clear"></div>
		
		<!--
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>		
				
			</ul>
			
		</div> -->
		
	</div>
	
		<script type="text/javascript">
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/accounts/deleteSalesEmployees/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
});
</script>
