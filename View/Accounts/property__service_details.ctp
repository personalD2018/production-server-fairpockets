<!--./advertisement-Card-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <h1>Service <?php echo $report_detail['PropService']['prop_sname']; ?> </h1>
            </div>
        </div>

        <div id="build" class="list-detail detail-block target-block">
            <div class="well block col-sm-12">
                <div class="col-sm-6"><b>Task:</b> <?php echo $report_detail['PropService']['services']; ?></div>
                <div class="col-sm-6"><b>Description:</b><?php echo $report_detail['PropService']['prop_sdesc']; ?></div>
                <div class="col-sm-6"><b>Cost:</b> <?php echo $report_detail['PropService']['payment']; ?></div>
            </div>
            <div class="row">
                <div class="col-sm-3"><b>Date</b></div>
                <div class="col-sm-3"><b>Status</b></div>
                <div class="col-sm-3"><b>Remarks</b></div>
                <div class="col-sm-3"><b>Photo</b></div>
            </div>
            <?php
            foreach ($report_service_detail as $detial) {
                ?> 
                <div class="row">
                    <div class="col-sm-3"> <?php echo $detial ['PropServicestage']['date']; ?> </div>
                    <div class="col-sm-3"> <?php
                        if ($detial['PropServicestage']['status'] == '1') {
                            echo "Active";
                        } else {
                            echo "Closed";
                        }
                        ?> </div>
                    <div class="col-sm-3"> <?php echo $detial['PropServicestage']['remarks']; ?></div>
                    <div class="col-sm-3">
                        <ul class="photo">
                            <?php
                            foreach ($detial['servicephotos'] as $photo) {
                                ?> 
                                <li> <a target="_blank" href="<?php echo $this->webroot . $photo['PropServicephoto']['photo_name']; ?>"><img class="img-responsive" src="<?php echo $this->webroot . $photo['PropServicephoto']['photo_name']; ?>" height="50" width="50" alt=""></a></li>
                                    <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
