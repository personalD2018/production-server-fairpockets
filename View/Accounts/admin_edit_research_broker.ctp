

<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Add Broker</h3>

            </div>

        </div>
    </div>
<?php //print_r($this->request->data);?>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>FP Research Valuation</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('Broker', array('id' => "formID", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>
                        <div class="control-group">
                            <label class="control-label">Name of Person</label>
                            <div class="controls">
                                <?php echo $this->Form->input('broker_name', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Report Name', 'class' => 'small required')); ?>
                            </div>
                        </div>
						
                        <div class="control-group">
                            <label class="control-label">Company Name</label>
                            <div class="controls">
                                <?php echo $this->Form->input('company_name', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Report Name', 'class' => 'small required')); ?>
                            </div>
                        </div>
						
                       <div class="control-group">
                            <label class="control-label">Mobile1</label>
                            <div class="controls">
                                <?php echo $this->Form->input('mobile_first', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Report Name', 'class' => 'small required')); ?>
                            </div>
                        </div>
						
                       <div class="control-group">
                            <label class="control-label">Mobile2</label>
                            <div class="controls">
                                <?php echo $this->Form->input('mobile_second', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Report Name', 'class' => 'small required')); ?>
                            </div>
                        </div>
                                            
                        
                          <div class="control-group">
                            <label class="control-label">City</label>
                            <div class="controls">
                                <?php
                                echo $this->Form->input(
                                        'city', array(
                                    'id' => 'city', 'options' => $city,
                                    'label' => false,
                                    'empty' => 'Select'
                                        )
                                );
                                ?>		
                            </div>
                        </div>
						
                        <div class="control-group">
                            <label class="control-label">Locality</label>
                            <div class="controls">
                                <?php echo $this->Form->input('locality', array('id' => 'no_of_towers', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
                            </div>
                        </div>
                      
					  <div class="control-group">
                            <label class="control-label">Project</label>
                            <div class="controls">
                                <?php
                                echo $this->Form->input(
                                        'project', array(
                                    'id' => 'project', 'options' => $city,
                                    'label' => false,
                                    'empty' => 'Select'
                                        )
                                );
                                ?>		
                            </div>
                        </div>

						
						
						<div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
                                <?php echo $this->Form->input('address', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => ' Enter Address', 'class' => 'small required')); ?>
                            </div>
                        </div>
						
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               