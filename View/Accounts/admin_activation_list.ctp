<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Activation user</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Activation user</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>
								<th class="center">Id</th>
								<th class="center">Builder Name</th>
                                <th class="center">Service Name</th>
								<th class="center">Service Type</th>
                                <th class="center">Duration of Paid service</th>
								<th class="center">Sales Employees User</th>
								<th class="center">Master Broker User</th>
								<th class="center">Broker Sales User</th>
								<th class="center">Date Created</th>
								<th class="center">Status</th>
								<th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
							//echo "<pre>";print_r($PropService);die();
                            foreach ($PropService as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['freeactivation']['id']; ?></td>
									<!--<td class="center"><?php //echo $this->Number->getUserRoleByUserId($data['freeactivation']['builder_id']); ?></td>-->
                                    <td class="center"><?php echo $data['freeactivation']['builder_id']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['serviceid']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['servicetype']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['duration_of_free_trial']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['sales_employees_user']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['master_broker_user']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['broker_sales_user']; ?></td>
									<td class="center"><?php echo $data['freeactivation']['date_created']; ?></td>
                                  <!-- <td class="center"><?php /*
                                        if ($data['freeactivation']['status'] == '1') {
                                            $payment = "Approved";
                                        } else if ($data['freeactivation']['status'] == '2') {
                                            $payment = "Rejected";
                                        } else if ($data['PropService']['status'] == '3') {
                                            $payment = "closed";
                                        } else if ($data['PropService']['status'] == '4') {
                                            $payment = "Payment Pending";
                                        } else {
                                            $payment = "Under Approval";
                                        }
                                        echo $payment; */
                                        ?></td>--> 

										<td>
											<?php if ($data['freeactivation']['status'] != 1){ ?>
													<a href="<?php echo $this->webroot; ?>admin/accounts/builderApproveAction/<?php echo $data['freeactivation']['id']; ?>">			
														<button class="btn btn-warning" title="Unapprove"><i class="icon-thumbs-down"></i></button>
													</a>
												<?php }else{ ?>
													<a href="<?php echo $this->webroot; ?>admin/accounts/builderApproveAction/<?php echo $data['freeactivation']['id']; ?>">			
														<button class="btn btn-success" title="Approved"><i class="icon-thumbs-up"></i></button>
													</a>
												<?php } ?>
										</td>	
                                   					

                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/edit_activation/<?php echo $data['freeactivation']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                              <!--  <a target="_blank" href="<?php echo $this->webroot; ?>accounts/Property_ServiceDetails/<?php echo $data['freeactivation']['id']; ?>">
                                                    <button class="btn btn-danger"  title="View Detail"><i class="icon-eye-open"></i></button></a>

                                                <a href="<?php echo $this->webroot; ?>admin/accounts/addservicedetail/<?php echo $data['freeactivation']['id']; ?>">
                                                    <button class="btn btn-primary" title="Add Service Detail"><i class=" icon-plus-sign-alt"></i></button></a>-->
                                            </div>
										</div>
									</td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


