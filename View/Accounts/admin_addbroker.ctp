<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) > Number($(params).val()));
                }, 'Must be greater than Launch date.');

        jQuery("#formID").validate(
                {

                    rules: {

                        'data[ResearchReport][report_price]': {
                            required: true,
                            number: true,
                            min: 0
                        },
                        'data[ResearchReport][doj2]': {
                            greaterThan: '#ResearchReportDoj1'
                        },
                    },
                    messages: {
                        'data[ResearchReport][report_price]': {
                            required: 'Report Price is required.',
                            number: 'Only Numeric Digits allowed.',
                            min: 'Min value allowed is 0s',

                        }
                    }


                }
        );

        $('#add_exercise').on('click', function () {
            $('#exercises').append('<div class="exercise">      <input type="text" name="unittype[]" placeholder="Please Enter Unit Type"><input type="text" name="totalunit[]" placeholder="Please Enter TotalUnit"><input type="text" name="areabaseprice[]" placeholder="Please Enter area Base Price"><button class="remove">x</button></div>');
            return false; //prevent form submission
        });

        $('#exercises').on('click', '.remove', function () {
            $(this).parent().remove();
            return false; //prevent form submission
        });

    });
    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
        $('#datetimepicker5').datetimepicker({
            pickTime: false
        });
    });


</script>	

<script type="text/javascript">
    $(document).ready(function () {
				
				//alert("heloooo");
				
        
		// GET PROJECT NAME ON THE BASIS OF SELECTED CITY.
		
		$("#id_select_city").on('change', function () {
			//$("#project_id").find('option').remove();

            var cityId = $(this).val();
			//alert(cityId);
              $.getJSON('/admin/accounts/getProjectListByCityId/' + cityId, function(data) {
                    $('#project_id').empty();

                    if(data.length == 0) {
                        $('#project_id').append($('<option>', { value: '', text: '--No Projects Found--' }));
					//	$('#project_id').append($('<option>', { value: '0', text: '--Please add Project/Select--' }));

                    } else {
							$('#project_id').append($('<option>', { value: '0', text: '--Please Select project--' }));
                        for(var id in data) {
                            $('#project_id').append($('<option>', { value: id, text: data[id] }));
                        }    
                    }
                });
        });

    });
   
</script>	



<script type="text/javascript">

    $(document).ready(function () {
			$("#id_select_city").on('change', function () {
			  var cityId = $(this).val();
			 // alert(cityId);
			    $.getJSON('/admin/accounts/getLocalityListByCityId/' + cityId, function(data) {
					
                    $('#location_id').empty();
                    if(data.length == 0) {
                        $('#location_id').append($('<option>', { value: '', text: '--No Locality Found--' }));
					} else {
							$('#location_id').append($('<option>', { value: '0', text: '--Please Select locality--' }));
                        for(var id in data) {
                            $('#location_id').append($('<option>', { value: data[id], text: data[id] }));
                        }    
                    }
                });
        });

    });
	
</script>

<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Add Broker</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>FP Research Valuation</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
					
                        <?php echo $this->Form->create('Broker', array('id' => "formID", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>
						<div class="row-fluid">
						<div class="span3">
							<div class="control-group">
								<label class="control-label">Name of Person</label>
								<div class="controls">
									<?php echo $this->Form->input('broker_name', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Broker Name is required', 'class' => 'small required')); ?>
								</div>
							</div>
						</div>
						
						<div class="span3">
							<div class="control-group">
								<label class="control-label">Company Name</label>
								<div class="controls">
									<?php echo $this->Form->input('company_name', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Company name is required', 'class' => 'small required')); ?>
								</div>
							</div>
						</div>
						
						<div class="span3">
						   <div class="control-group">
								<label class="control-label">Mobile1</label>
								<div class="controls">
									<?php echo $this->Form->input('mobile_first', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Mobile no1 is required', 'class' => 'small required')); ?>
								</div>
							</div>
						</div>
						
						<div class="span3">
						   <div class="control-group">
								<label class="control-label">Mobile2</label>
								<div class="controls">
									<?php echo $this->Form->input('mobile_second', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Mobile no2 is required', 'class' => 'small required')); ?>
								</div>
							</div>
                        </div>   
						
                        </div>
						
                         <!-- <div class="control-group">
                            <label class="control-label">City</label>
                            <div class="controls">
                                <?php
                                echo $this->Form->input(
                                        'city', array(
                                    'id' => 'id_select_city', 'options' => $city,
                                    'label' => false,
                                    'empty' => 'Select',
									'title'=>'Please select city',
									'class'=>'small required'
                                        )
                                );
                                ?>		
                            </div>
                        </div>
						
                        <div class="control-group">
                            <label class="control-label">Locality</label>
                            <div class="controls">
                                <?php /*echo $this->Form->input('locality', array('id' => 'location_id', 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'class'=>'small ','title'=>'Enter locality')); */?>
								<?php echo $this->Form->input(
															'locality', array(
															'id' => 'location_id', 
															'type' => 'select', 
															'empty' => 'Select location', 
															'class' => 'selectpicker bs-select-hidden required', 
															'placeholder' => 'Select Project', 
															'title' => 'Please Select Project', 
															'label' => false)
															);
												?>	
                            </div>
                        </div>
                      
					  <div class="control-group">
                            <label class="control-label">Project</label>
                            <div class="controls">
                              <?php echo $this->Form->input(
															'project_id', array(
															'id' => 'project_id', 
															'options' => $userproject, 
															'empty' => 'Select Project', 
															'class' => 'selectpicker bs-select-hidden required', 
															'placeholder' => 'Select Project', 
															'title' => 'Please Select Project', 
															'label' => false));
												?>		
                            </div>
                        </div>

						
						
						<div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
                                <?php echo $this->Form->input('address', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => ' Enter Address', 'class' => 'small required')); ?>
                            </div>
                        </div>
						
						-->
						
						<!-- .......... -->
						
						<div class="row-fluid">
                        <div class="span3">
                      <div class="control-group">
                            <label class="control-label">City</label>
                            <div class="controls">
                                <?php
                                echo $this->Form->input(
                                        'city', array(
                                    'id' => 'id_select_city', 'options' => $city,
                                    'label' => false,
                                    'empty' => 'Select',
									'title'=>'Please select city',
									'class'=>'small required'
                                        )
                                );
                                ?>		
                            </div>
                        </div>
                          </div>
							
						<div class="span3">
						<div class="control-group">
                            <label class="control-label">Locality</label>
                            <div class="controls">
                                <?php /*echo $this->Form->input('locality', array('id' => 'location_id', 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'class'=>'small ','title'=>'Enter locality')); */?>
								<?php echo $this->Form->input(
															'locality', array(
															'id' => 'location_id', 
															'type' => 'select', 
															'empty' => 'Select location', 
															'class' => 'selectpicker bs-select-hidden required', 
															'placeholder' => 'Select Project', 
															'title' => 'Please Select Project', 
															'label' => false)
															);
												?>	
                            </div>
                        </div>
                        </div>
						
						
						<div class="span3">
                    <div class="control-group">
                            <label class="control-label">Project</label>
                            <div class="controls">
                              <?php echo $this->Form->input(
															'project_id', array(
															'id' => 'project_id', 
															'options' => $userproject, 
															'empty' => 'Select Project', 
															'class' => 'selectpicker bs-select-hidden required', 
															'placeholder' => 'Select Project', 
															'title' => 'Please Select Project', 
															'label' => false));
												?>		
                            </div>
                        </div>
                        </div>
						
						<div class="span3">
                     <div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
                                <?php echo $this->Form->input('address', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => ' Enter Address', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        </div>
						
						</div>
						
						
						<!-- ............. -->
						
						
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               