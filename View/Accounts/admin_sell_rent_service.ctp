<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Services</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Sell/Rent services</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Requirement</th>
                                <th class="center">Property Type </th>

                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($sell_rent as $data) {

                                $project_feature = json_decode($data['Property']['features'], 'true');
                                if (!empty($project_feature['Configure'])) {
                                    $option = $project_feature['Configure'];
                                } else {
                                    if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
                                        $option = "Residential Property";
                                    } else {
                                        $option = "Commercial Property";
                                    }
                                }

                                $option .= " for ";
                                if ($data['Property']['transaction_type'] == '1') {
                                    $option .= "Sale";
                                } else {
                                    $option .= "Rent";
                                }
                                $option .= " in ";

                                $option .= $data['Property']['city_data'];
                                $option .= " , ";
                                $option .= $data['Property']['state_data'];
                                ?>
                                <tr>
                                    <td class="center"><?php echo $option; ?></td>

                                    <td class="center"><?php
                                        if ($data['AsstSellRent']['prop_type'] == '1') {
                                            echo $prop_reqt = "Rent Out";
                                        } else {
                                            echo $prop_reqt = "Sell";
                                        }
                                        ?></td>



                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">

                                                <a href="<?php echo $this->webroot; ?>admin/accounts/viewsellrentleads/<?php echo $data['AsstSellRent']['prop_id']; ?>">
                                                    <button class="btn btn-green" title="View Leads"><i class="icon-list"></i></button></a>
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/addsellrentleads/<?php echo $data['AsstSellRent']['prop_id']; ?>">
                                                    <button class="btn btn-primary" title="Add Leads"><i class=" icon-plus-sign-alt"></i></button></a>
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/viewSellrent/<?php echo $data['AsstSellRent']['id']; ?>">
                                                    <button class="btn btn-danger" title="ViewDetail"><i class="icon-eye-open"></i></button></a>
                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


