<?php
$option = array();
if (!empty($Portfolio)) {

    foreach ($Portfolio as $data) {
        if ($data['Portfolio']['prop_type'] == '1') {
            $pro_type = "Residential ";
        } else {
            $pro_type = "Commercial ";
        }
        $option[$data['Portfolio']['id']] = $pro_type . "Property in " . $data['Portfolio']['prop_locality'] . " " . $data['MstAreaCity']['cityName'];
    }
}
?>

<!--./advertisement-Card-->
    <div class="page-content">
           <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header"> Portfolio Management - Request Service </h1>
                </div>

            </div>

            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">
                    <?php
                    if (!empty($option)) {
                        ?>
                        <?php
                        echo $this->Form->create(
                            'PortService', array(
                            'class' => 'fpform',
                            'role' => 'form',
                            'id' => 'id_form_PortService',
                            'novalidate' => 'novalidate'
                           )
                        );
                        ?>


                        <div class="account-block">

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="port_name" class="label_title">Portfolio Name <span class="mand_field">*</span></label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            echo $this->Form->input(
                                                    'port_name', array(
                                                'id' => 'port_name',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>	

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="port_prop" class="label_title">Select Property </label>
                                        </div>

                                        <div class="col-sm-12">

                                            <!--
                                    
                                            <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'port_prop', array(
                                                'id' => 'id_port_prop',
                                                'multiple' => 'true',
                                                'options' => $option,
                                                'label' => false
                                                    )
                                            );
                                            ?>
                    
                                            </div>
                            
                                            -->

                                            <select name="data[PortService][port_prop][]" id="id_port_prop"  multiple="multiple">
                                                <?php
                                                if (!empty($option)) {
                                                    foreach ($option as $x => $x_value) {
                                                        ?>

                                                        <option value = "<?php echo $x; ?>"><?php echo $x_value; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>


                                        </div>

                                    </div>
                                </div>
                            </div>	

                            <br>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <!--
                                            <label><span style="color:RED;">*</span> In a Portfolio, you can add upto max 5 properties </label> 
                                            <br><br>
                                            -->
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                        </div>

                                    </div>

                                </div>

                            </div>



                        </div>

                        <!-- </form> -->	
                        <?php
                        echo $this->Form->end();
                    } else {
                        echo "No Property Added, Please add a property first to Avail service";
                    }
                    ?>


                </div>
            </div>



            <div class="clear"></div>



        </div>

<?php
// Jquery 
echo $this->Html->script('front/bootstrap-multiselect');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('additional-methods.min');

// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("

		var rulesMTarea = {
					required : true
		};
		
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_PortService').validate({
			
						// Specify the validation rules
			rules: {
				'data[PortService][port_name]': rulesMTarea
			},
			
			// Specify the validation error messages
			messages: {
				'data[PortService][port_name]': messageMTarea
			}
		});	
	");
?>

<script>
    $(document).ready(function () {

        $("#id_port_prop").multiselect({
            includeSelectAllOption: true,
            selectAllText: 'Select all!'
        });


    });
</script>	 

