<script>
    $(document).ready(function () {

        $("#prop_state").on('change', function () {
            var id = $(this).val();

            $("#prop_city").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "countries", "action" => "getpecity")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#manger_city').html(html);
                    }
                }).html;
            }
        });


    });
</script>
<script>
    $(document).ready(function () {

<?php
if ($this->request->data['Portfolio']['prop_type'] == '2') {
    ?>
            $(".hide_config").hide();
<?php }
?>
        $(".radio-custom").on('click', function () {

            var id = $(this).val();
            if (id == '1')
            {
                $(".hide_config").show();
            } else
            {
                $(".hide_config").hide();
            }
            var transaction = $("#transaction_type").val();

            $("#prop_toption").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyoption")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#prop_toption"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#prop_toption"));
                        });
                        if (transaction == '2')
                        {

                            $("#prop_toption option[value='3']").hide();
                            $("#property_type_val option[value='27']").hide();
                        }
                        if (transaction == '1')
                        {

                            $("#prop_toption option[value='3']").show();
                            $("#prop_toption option[value='27']").show();
                        }
                    }
                }).responseJSON;
            }



        });

    });


</script>
    <div class="page-content">

            <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header"> Portfolio Management - Update Property </h1>
                </div>

            </div>

            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo $this->Form->create(
                            'Portfolio', array(
                        'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'id_form_PortMgmtAdd',
                        'novalidate' => 'novalidate'
                            )
                    );
                    ?>


                    <div class="account-block">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_type"  class="label_title">Property Type </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        $options = array(
                                            '1' => 'Residential',
                                            '2' => 'Commercial'
                                        );

                                        $attributes = array(
                                            'legend' => false,
                                            'class' => 'radio-custom',
                                            'label' => array('class' => 'radio-custom-label')
                                        );

                                        echo $this->Form->radio('prop_type', $options, $attributes);
                                        ?>									
                                    </div>		
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_toption" class="label_title">Property Type Options <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_toption', array(
                                                'id' => 'prop_toption',
                                                'options' => $propertyoption,
                                                'class' => 'selectpicker required',
                                                'label' => false,
                                                'empty' => 'Select'
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Project </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_proj', array(
                                            'id' => 'prop_proj',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_stat" class="label_title">Status </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_stat', array(
                                                'id' => 'prop_stat',
                                                'options' => array(
                                                    'Rented' => 'Rented',
                                                    'Self' => 'Self',
                                                    'Occupied' => 'Occupied',
                                                    'Vacant' => 'Vacant'
                                                ),
                                                'class' => 'selectpicker',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">Address <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_addr', array(
                                            'id' => 'prop_addr',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_locality" class="label_title">Locality <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_locality', array(
                                            'id' => 'prop_locality',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>							

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_state" class="label_title">State </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_state', array(
                                                'id' => 'prop_state',
                                                'options' => $state_master,
                                                'class' => 'selectpicker',
                                                'label' => false,
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_city" class="label_title">City </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style " id="manger_city">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_city', array(
                                                'id' => 'prop_city',
                                                'options' => $city_master,
                                                'class' => 'selectpicker',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>								
                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Description <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_desc', array(
                                            'id' => 'prop_desc',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                        </div>	

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_area" class="label_title">Area ( in Sq feet ) <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_area', array(
                                            'id' => 'prop_area',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 hide_config">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_cfg" class="label_title">Configure </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_cfg', array(
                                                'id' => 'prop_cfg',
                                                'options' => array('None' => 'None', 'Shared' => 'Shared', '1RK' => '1RK', 'Studio' => 'Studio', '1BHK' => '1BHK', '2BHK' => '2BHK', '3BHK' => '3BHK', '4BHK' => '4BHK', '5BHK' => '5BHK', '6BHK' => '6BHK', 'PentHouse' => 'PentHouse'),
                                                'class' => 'selectpicker',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>								
                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_pprice" class="label_title">Purchase Price <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_pprice', array(
                                            'id' => 'prop_pprice',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_pdate" class="label_title">Purchase date <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_pdate', array(
                                                'id' => 'prop_pdate',
                                                'class' => 'form-control',
                                                'size' => '16',
                                                'readonly' => 'readonly',
                                                'style' => 'cursor : pointer',
                                                'type' => 'text',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                            <span class="add-on removeicon"><i class="icon-remove"></i></span>
                                            <span class="add-on calicon"><i class="icon-th"></i></span>
                                        </div>
                                        <?php
                                        echo $this->Form->input(
                                                'dtp_input1', array(
                                            'id' => 'dtp_input1',
                                            'value' => '',
                                            'type' => 'hidden',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                        <br>
                                    </div>			
                                </div>
                            </div>			

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_mprice" class="label_title">Market Price <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_mprice', array(
                                            'id' => 'prop_mprice',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_mrent" class="label_title">Market Rent <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_mrent', array(
                                            'id' => 'prop_mrent',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_oth" class="label_title">Desired Action </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_oth', array(
                                            'id' => 'prop_oth',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_rsn" class="label_title">Reason </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_rsn', array(
                                            'id' => 'prop_rsn',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_curr_val" class="label_title">Current Valuation <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_curr_val', array(
                                            'id' => 'prop_curr_val',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_curr_rent" class="label_title">Current Rental <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_curr_rent', array(
                                            'id' => 'prop_curr_rent',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_5yr_val" class="label_title">5 Years Value	 <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_5yr_val', array(
                                            'id' => 'prop_5yr_val',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_5yr_rent" class="label_title">5 Years Rental <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_5yr_rent', array(
                                            'id' => 'prop_5yr_rent',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_advice" class="label_title">Advice <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_advice', array(
                                            'id' => 'prop_advice',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_cagr" class="label_title">CAGR <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_cagr', array(
                                            'id' => 'prop_cagr',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                        </div>


                        <br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" >Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>


                </div>
            </div>



            <div class="clear"></div>



        </div>

<?php
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('bootstrap-datetimepicker');

echo $this->Html->scriptBlock("
	
		// Handling Date input from calendar
		$('.form_date').datetimepicker({
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});

	");

// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		var rulesMTSetPricing = {
					required : true,
					range    :[1.00,999999999]
		};
		
		var messageMTSetPricing = {
					required : 'Field is required.',
					range  : 'Allowed : 1 - 999999999.',
		};
		
		var rulesMTSetPercent = {
					required : true,
					range    :[1.0,100]
		};
		
		var messageMTSetPercent = {
					required : 'Field is required.',
					range  : 'Allowed : 1 - 100.',
		};
		
		$('#id_form_PortMgmtAdd').validate({
			
			// Specify the validation rules
			rules: {
				'data[Portfolio][prop_addr]': rulesMTarea,
				'data[Portfolio][prop_locality]': rulesMTarea,
				'data[Portfolio][prop_desc]': rulesMTarea,
				'data[Portfolio][prop_area]': rulesMTarea,
				'data[Portfolio][prop_pprice]': rulesMTSetPricing,
				'data[Portfolio][prop_mprice]': rulesMTSetPricing,
				'data[Portfolio][prop_mrent]': rulesMTSetPricing,
				'data[Portfolio][prop_pdate]': rulesMTarea,
				'data[Portfolio][prop_curr_val]': rulesMTSetPricing,
				'data[Portfolio][prop_curr_rent]': rulesMTSetPricing,
				'data[Portfolio][prop_5yr_val]': rulesMTSetPricing,
				'data[Portfolio][prop_5yr_rent]': rulesMTSetPricing,
				'data[Portfolio][prop_advice]': rulesMTarea,
				'data[Portfolio][prop_cagr]': rulesMTSetPercent
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[Portfolio][prop_addr]': messageMTarea,
				'data[Portfolio][prop_locality]': messageMTarea,
				'data[Portfolio][prop_desc]': messageMTarea,
				'data[Portfolio][prop_area]': messageMTarea,
				'data[Portfolio][prop_pprice]': messageMTSetPricing,
				'data[Portfolio][prop_mprice]': messageMTSetPricing,
				'data[Portfolio][prop_mrent]': messageMTSetPricing,
				'data[Portfolio][prop_pdate]': messageMTarea,
				'data[Portfolio][prop_curr_val]': messageMTSetPricing,
				'data[Portfolio][prop_curr_rent]': messageMTSetPricing,				
				'data[Portfolio][prop_5yr_val]': messageMTSetPricing,
				'data[Portfolio][prop_5yr_rent]': messageMTSetPricing,
				'data[Portfolio][prop_advice]': messageMTarea,
				'data[Portfolio][prop_cagr]': messageMTSetPercent
				
			}
		});	
	");
?>





