<style>
.select-style select{
	padding:10px 5px !important;
}
</style>
<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Add Sales Employees</h2>
            <br />
        </div>	

        <div class="add_property_form">

            <?php
            echo $this->Form->create(
                    'SalesUser', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Sales employees Information</h3>
                    
                </div>

                <div class="add-tab-content1 detail-block" id="id_proj_pricing_content">
					
					
					
					<div class="add-tab-row push-padding-bottom">
						

						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_proj" class="label_title">Name </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'name', array(
                                            'id' => 'name',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_desc" class="label_title">Email </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'email', array(
                                            'id' => 'email',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>


                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_addr" class="label_title">Mobile No <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'mobile', array(
                                            'id' => 'mobile',
                                            'type' => 'text',
                                            'class' => 'form-control required',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>                          

                        </div>

						<div class="row" style="display:none;">

                            <div class="col-sm-12">
                                <div class="form-group">

                                    

                                    <div class="col-sm-12">
									<?php echo $this->Form->input('share_check', array(
                                    'id' => 'share_check',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
								   <!--<label class="label_title">By Default, you will also be sharing the information like brochure, price list, images and occasional Messages from the developer</label>-->
                                </div>

                                
                                </div>
                            </div>                          

                        </div>						

                        <br>
						
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>
						<?php
							echo $this->Form->end();
						?>
						
                    </div>
					
					
										
                    							

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>

        </div>		
    </div> 
<?php
echo $this->Html->script('add_more_app_broker');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');

echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		var rulespTarea = {
					required : true,
					min:1
			
		};
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_project_pricing').validate({
			
			// Specify the validation rules
			rules: {
				'data[Portmgmt][prop_addr]': rulesMTarea,
				'data[Portmgmt][prop_locality]': rulesMTarea,
				'data[Portmgmt][prop_desc]': rulesMTarea,
				'data[Portmgmt][prop_area]': rulespTarea
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[Portmgmt][prop_addr]': messageMTarea,
				'data[Portmgmt][prop_locality]': messageMTarea,
				'data[Portmgmt][prop_desc]': messageMTarea,
				'data[Portmgmt][prop_area]': messageMTarea
				
			}
		});	
	");

?>

<script>
    $(document).ready(function () {
        $("#builderdropdown1").on('change', function () {
			//alert('heelo2');
			var builder_id = $(this).val();
            $("#from_project1").find('option').remove();
            if (builder_id) {
                var dataString = 'builder_id=' + builder_id;
                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "accounts", "action" => "getbuilderprojects")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#from_project1"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#from_project1"));
                        });
                    }
                }).responseJSON;
            }
        });
		
		$("#from_project1").on('change', function () {
			//alert('heelo2');
			var project_id = $(this).val();
			var sel_builder_id = $('#builderdropdown1').val();
            $("#to_floor1").find('option').remove();
            if (project_id) {
                //var dataString = 'project_id=' + project_id + 'sel_builder_id=' +sel_builder_id;
                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "accounts", "action" => "getbuilderprojectsdiscunit")); ?>',
                    data: {project_id:project_id, sel_builder_id:sel_builder_id},
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#to_floor1"));
                        $.each(html, function (key, value) {
                            $('<option>').val(key).text(value).appendTo($("#to_floor1"));
                        });
                    }
                }).responseJSON;
            }
        });
		
		
		
    });


</script>


