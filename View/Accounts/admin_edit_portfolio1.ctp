
       <div class="container-fluid">
	  
			<div class="row-fluid ">
				<div class="span12">
					<div class="primary-head">
					  <h1 class="page-header">Portfolio <?php echo $report_detail['PortService']['port_name']; ?> Summary </h1>

					</div>

				</div>
			</div>
           
	    <div class="row-fluid">
			<div class="span12">
			<div class="content-widgets gray">
			<div class="widget-container">
			<div class="form-container grid-form form-background">
			
            <div id="build" class="list-detail detail-block target-block" style="padding-bottom:20px;">

                <ul >
                    <li class="col-md-6"><strong>Latest Valuation:</strong> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $report_detail['PortService']['Valuation']; ?></li>
                    <li class="col-md-6"><strong>Latest Overall CAGR:</strong> <?php echo $report_detail['PortService']['CAGR']; ?>%</li>
                    <li class="col-md-6"><strong>Created on:</strong><?php echo $report_detail['PortService']['Created_on']; ?></li>
                    <li class="col-md-6"><strong>View latest report:</strong> <a href="<?php echo @$this->webroot . @$report_detail['PortService']['latest_pdf']; ?>">Pdf Download</a></li>
                </ul>

            </div>
			<div id="action_alert" title="Action">
            <?php
			//$i= 0;
            if (!empty($properties)) {
				$data = $properties;
				//o '<pre>'; print_r($data);die();
				//echo $len=count($data);
                for($i=0;$i<=count($data)-1;$i++) {
					//foreach ($properties as $data) {
                    if ($data[$i]['Portfolio']['prop_type'] == '1') {
                        $pro_type = "Residential";
                    } else {
                        $pro_type = "Commercial";
                    }
                    ?>
					<form class="form" method="post" id="user_form<?php echo $i; ?>">
					<div class="widget-head blue">
								<h3><?php echo $pro_type ?> Property in <?php echo $data[$i]['Portfolio']['prop_locality']; ?> ,  <?php echo $data['MstAreaCity']['cityName']; ?></h3>
							</div>
					    <div class="row-fluid">
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong> Area : </strong> <?php echo $data[$i]['Portfolio']['prop_area']; ?> /sq feet </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
								
                                <div class="form-control" > <strong>Status : </strong> <?php echo $data[$i]['Portfolio']['prop_state']; ?> </div>
                          
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Locality: </strong><?php echo $data[$i]['Portfolio']['prop_locality']; ?> </div>
									</div>
								</div>
							</div>
							
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Address : </strong> <?php echo $data[$i]['Portfolio']['prop_addr']; ?> </div>
									</div>
								</div>
							</div>
							
					    </div>
					
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Current Valuation : </strong><?php echo $data[$i]['Portfolio']['prop_curr_val']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Current Rental: </strong><?php echo $data[$i]['Portfolio']['prop_curr_rent']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>5 years value: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_5yr_val']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>5 years rental : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_5yr_rent']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Project : </strong><?php echo $data[$i]['Portfolio']['prop_proj']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Status: </strong><?php echo $data[$i]['Portfolio']['prop_stat']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Address: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_5yr_val']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>locality: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_locality']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>State : </strong><?php echo $data[$i]['Portfolio']['prop_state']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>City: </strong><?php echo $data[$i]['Portfolio']['prop_city']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Description: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_desc']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Configure: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_cfg']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						<div class="row-fluid">
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Purchase Price : </strong><?php echo $data[$i]['Portfolio']['prop_pprice']; ?> </div>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" ><strong>Purchase Date: </strong><?php echo $data[$i]['Portfolio']['prop_pdate']; ?></div>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Market Price: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_mprice']; ?> </div>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<div class="form-control" > <strong>Market Rent: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data[$i]['Portfolio']['prop_mrent']; ?></div>
									</div>
								</div>
							</div>
						<!--	<input type="hidden" name="property_id" value=""><>-->
					    </div>
						
						
					<div class="row-fluid" style="border:1px solid;border-color: #adb5bb;line-height: 40px;"></div>
						<div class="row-fluid">
							<div class="span6">
								<div class="control-group">
									<label class="control-label">Property Description</label>
									<div class="controls">
										<?php echo $this->Form->input('property_desc', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'textarea', 'style' => ('width: 466px; height: 50px;'), 'id' => 'property_desc','value'=> $data[$i]['Portfolio']['property_desc'], 'class' => 'required')); ?>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label">Liquidity</label>
									<div class="controls">
										<?php echo $this->Form->input('liquidity', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'liquidity','value'=> $data[$i]['Portfolio']['liquidity'], 'class' => 'required')); ?>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label">Rental</label>
									<div class="controls">
										<?php echo $this->Form->input('rental', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'rental','value'=> $data[$i]['Portfolio']['rental'], 'class' => 'required')); ?>
									</div>
								</div>
							</div>
							
							<input type="hidden" name="id" id="property_id" value="<?php echo $data[$i]['Portfolio']['id']; ?>">
					    </div>
						
						<div class="row-fluid">
						
						<div class="span3">
								<div class="control-group">
									<label class="control-label">EXPECTED GROWTH RATE</label>
									<div class="controls">
										<?php echo $this->Form->input('expected_growth_rate', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'expected_growth_rate','value'=> $data[$i]['Portfolio']['expected_growth_rate'],'class' => 'required')); ?>
									</div>
								</div>
							</div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label">FORECAST FOR TWO YEARS</label>
									<div class="controls">
										<?php echo $this->Form->input('forecast_2year', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'forecast_2year','value'=> $data[$i]['Portfolio']['forecast_2year'], 'class' => 'required')); ?>
									</div>
								</div>
						    </div>
						
							<div class="span3">
								<div class="control-group">
									<label class="control-label">FORECAST FOR FIVE YEARS</label>
									<div class="controls">
										<?php echo $this->Form->input('forecast_5year', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'forecast_5year','value'=> $data[$i]['Portfolio']['forecast_5year'], 'class' => 'required')); ?>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="control-group">
									<label class="control-label">Advice</label>
									<div class="controls">
										<?php //echo $this->Form->input('advice', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'advice','value'=> $data[$i]['Portfolio']['advice'], 'class' => 'required')); ?>
										<?php echo $this->Form->input('advice', array( 'options' => array('Hold' => 'Hold', 'Sale' => 'Sale'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
									</div>
								</div>
							</div>
																					
							<input type="hidden" name="id" id="property_id" value="<?php echo $data[$i]['Portfolio']['id']; ?>">
					    </div>
						<div class="row-fluid">
							<div class="span6">
								<div class="control-group">
									<label class="control-label">Property Description</label>
									<div class="controls">
										<?php echo $this->Form->input('property_desc1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'textarea', 'style' => ('width: 466px; height: 50px;'), 'id' => 'property_desc','value'=> $data[$i]['Portfolio']['property_desc1'], 'class' => 'required')); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
						<button type="submit" id="formid<?php echo $i; ?>" class="btn btn-primary">Update Property</button>

						</div>
					</form>
					
                   
                <?php
                }
            }
            ?>


            <div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
	    </div>
   </div> 	

<script type="text/javascript">

$(document).ready(function () {
    $( "#formid0").click(function() {
       // var data = $( "#formID11" ).submit();
	   
	//var property_id = 123;
		//alert(prop_curr_val);
	//alert(prop_curr_rent);
	//alert(prop_5yr_val);
	//alert(prop_5yr_rent);
	//alert(property_id);
	var form_data = $('#user_form0').serialize();
	//alert(form_data);
		var propId = property_id;
		  $.ajax({
       url: '<?php echo $this->webroot; ?>admin/accounts/editportfolio1/'+ propId,
        type: 'post',
       // dataType: 'json',
        data: form_data,
        success: function(data) {
                   alert("property update Successfully!!!");
				  // alert(data);
				 //  $('#action_alert').html('<p>Data Inserted Successfully</p>');
                 }
    });
		
		
    });
});

$(document).ready(function () {
    $( "#formid1" ).click(function() {
       // var data = $( "#formID11" ).submit();
	   /*var prop_curr_val = $('#prop_curr_val').val();
	var prop_curr_rent = $('#prop_curr_rent').val();
	var prop_5yr_val = $('#prop_5yr_val').val();
	var prop_5yr_rent = $('#prop_5yr_rent').val();
	var prop_addr = $('#prop_addr').val();*/
	//var property_id = 123;
		//alert(prop_curr_val);
	//alert(prop_curr_rent);
	//alert(prop_5yr_val);
	//alert(prop_5yr_rent);
	//alert(property_id);
	var form_data = $('#user_form1').serialize();
	//alert(form_data);
		var propId = property_id;
		  $.ajax({
       url: '<?php echo $this->webroot; ?>admin/accounts/editportfolio1/'+ propId,
        type: 'post',
       // dataType: 'json',
        data: form_data,
        success: function(data) {
                   alert("property update Successfully!!!");
				  // alert(data);
				  // $('#action_alert').html('<p>Data Inserted Successfully</p>');
                 }
    });
		
		
    });
});


$(document).ready(function () {
    $( "#formid2" ).click(function() {
       // var data = $( "#formID11" ).submit();
	   /*var prop_curr_val = $('#prop_curr_val').val();
	var prop_curr_rent = $('#prop_curr_rent').val();
	var prop_5yr_val = $('#prop_5yr_val').val();
	var prop_5yr_rent = $('#prop_5yr_rent').val();
	var prop_addr = $('#prop_addr').val();*/
	//var property_id = 123;
		//alert(prop_curr_val);
	//alert(prop_curr_rent);
	//alert(prop_5yr_val);
	//alert(prop_5yr_rent);
	//alert(property_id);
	var form_data = $('#user_form2').serialize();
	//alert(form_data);
		var propId = property_id;
		  $.ajax({
       url: '<?php echo $this->webroot; ?>admin/accounts/editportfolio1/'+ propId,
        type: 'post',
       // dataType: 'json',
        data: form_data,
        success: function(data) {
                   alert("property update Successfully!!!");
				  // alert(data);
				  // $('#action_alert').html('<p>Data Inserted Successfully</p>');
                 }
    });
		
		
    });
});

$(document).ready(function () {
    $( "#formid3" ).click(function() {
       // var data = $( "#formID11" ).submit();
	   /*var prop_curr_val = $('#prop_curr_val').val();
	var prop_curr_rent = $('#prop_curr_rent').val();
	var prop_5yr_val = $('#prop_5yr_val').val();
	var prop_5yr_rent = $('#prop_5yr_rent').val();
	var prop_addr = $('#prop_addr').val();*/
	//var property_id = 123;
		//alert(prop_curr_val);
	//alert(prop_curr_rent);
	//alert(prop_5yr_val);
	//alert(prop_5yr_rent);
	//alert(property_id);
	var form_data = $('#user_form3').serialize();
	//alert(form_data);
		var propId = property_id;
		  $.ajax({
       url: '<?php echo $this->webroot; ?>admin/accounts/editportfolio1/'+ propId,
        type: 'post',
       // dataType: 'json',
        data: form_data,
        success: function(data) {
                   alert("property update Successfully!!!");
				 //  alert(data);
				  // $('#action_alert').html('<p>Data Inserted Successfully</p>');
                 }
    });
		
		
    });
});

$(document).ready(function () {
    $( "#formid4" ).click(function() {
       // var data = $( "#formID11" ).submit();
	   /*var prop_curr_val = $('#prop_curr_val').val();
	var prop_curr_rent = $('#prop_curr_rent').val();
	var prop_5yr_val = $('#prop_5yr_val').val();
	var prop_5yr_rent = $('#prop_5yr_rent').val();
	var prop_addr = $('#prop_addr').val();*/
	//var property_id = 123;
		//alert(prop_curr_val);
	//alert(prop_curr_rent);
	//alert(prop_5yr_val);
	//alert(prop_5yr_rent);
	//alert(property_id);
	var form_data = $('#user_form4').serialize();
	//alert(form_data);
		var propId = property_id;
		  $.ajax({
       url: '<?php echo $this->webroot; ?>admin/accounts/editportfolio1/'+ propId,
        type: 'post',
       // dataType: 'json',
        data: form_data,
        success: function(data) {
                   alert("property update Successfully!!!");
				 //  alert(data);
				  // $('#action_alert').html('<p>Data Inserted Successfully</p>');
                 }
    });
		
		
    });
});
</script>

<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate(
                {
                    rules: {
                        "data[PortService][payment]": {
                            required: true,
                            number: true,
                            min: 1
                        },

                        "data[PortService][CAGR]": {
                            required: true,
                            number: true,
                            min: 1,
                            max: 100
                        },

                        messages: {

                            "data[PortService][payment]": {

                                number: "Enter Only Numeric",
                                min: "Value must be greater than 0",

                            },
                            "data[PortService][CAGR]": {

                                number: "Enter Only Numeric",
                                min: "Value must be greater than 0",
                                max: "Value must be less or equal to  100",
                            }
                        }


                    }
                }
        )
    });

    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
    });
</script>	

<script>
    $(document).ready(function () {
<?php
if($PortService['PortService']['status'] == '')
{
?>
 $("#payment_hide").hide();
<?php
}
if ($PortService['PortService']['status'] == '2') {
    ?>
            $("#rejection_hide").hide();
<?php }
?>

<?php
if ($PortService['PortService']['status'] == '4' ) {
    ?>
            $("#payment_hide").hide();
<?php }
?>
        $("#status").on('change', function () {
            var id = $(this).val();

            if (id == '2') {
                $("#rejection_hide").hide();
            } else
            {
                $("#rejection_hide").show();
            }
            if (id == '1') {
                $("#payment_hide").show();
            } else
            {
                $("#payment_hide").hide();
            }
			
			if (id == '4' || id == '') {
                $("#payment_hide").hide();
            } else
            {
                $("#payment_hide").show();
            }

        });


    });
</script>


<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Portfolio</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Update FP Portfolio</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('PortService', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>
                        <div class="control-group">
                            <label class="control-label">Portfolio</label>
                            <div class="controls">
                                <?php echo $PortService['PortService']['port_name']; ?>
                            </div>
                        </div>
                        <div id="rejection_hide">
                            <div class="control-group">
                                <label class="control-label">Payment</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('payment', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'class' => 'span6 required')); ?>
                                </div>
                            </div>

                            <div id="payment_hide">
                                <div class="control-group">
                                    <label class="control-label">Latest Overall CAGR</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('CAGR', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'class' => 'span6 required')); ?> %
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Latest pdf</label>
                                    <div class="controls">
                                        <?php echo $this->Form->file('latest_pdf', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'fileupload required')); ?>

                                        <a target="_blank" href="<?php echo @$this->webroot . @$this->request->data['PortService']['latest_pdf']; ?>"><?php echo @$this->request->data['PortService']['latest_pdf']; ?></a>
                                        <?php echo $this->Form->input('latest_pdf1', array('type' => 'hidden', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'report price', 'class' => 'small', 'value' => @$this->request->data['PortService']['latest_pdf'])); ?>	

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Valuation</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('Valuation', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Valuation', 'class' => 'span6 required number')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Created On</label>
                                    <div class="controls">
                                        <div id="datetimepicker4" class="input-append">
                                            <?php echo $this->Form->input('Created_on', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please enter Date of Creation of Portfolio', 'class' => 'datepicker small required span12', 'readonly' => 'true')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Service status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'status', 'options' => array('1' => 'Approved', '2' => 'Rejected', '3' => 'closed', '4' => 'Payment Pending'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
						
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>					