<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Sell/Rent Services</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Sell/Rent Services</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Contact Person</th>
                                <th class="center">Price Discussion</th>
                                <th class="center">Call Type</th>
                                <th class="center">Last Call</th>
                                <th class="center">Next Call</th>
                                <th class="center">Activity</th>
                                <th class="center">Status</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($AsstSellLead as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['AsstSellLead']['name']; ?></td>
                                    <td class="center"><?php echo $data['AsstSellLead']['price_discussion']; ?></td>
                                    <td class="center"><?php
                                        if ($data['AsstSellLead']['call_type'] == 3) {
                                            echo "Telephone";
                                        }
                                        if ($data['AsstSellLead']['call_type'] == 4) {
                                            echo "Site Visit";
                                        }
                                        ?></td>

                                    <td class="center"><?php echo $data['AsstSellLead']['doj1']; ?></td>

                                    <td class="center"><?php echo $data['AsstSellLead']['doj2']; ?></td>
                                    <td class="center"><?php echo $data['AsstSellLead']['activity']; ?></td> 
                                    <td class="center"><?php
                                        if ($data['AsstSellLead']['status'] == 1) {
                                            echo "Hot";
                                        }
                                        if ($data['AsstSellLead']['status'] == 2) {
                                            echo "Warm";
                                        }
                                        if ($data['AsstSellLead']['status'] == 3) {
                                            echo "Cold";
                                        }
                                        ?></td>


                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/editsellrentleads/<?php echo $data['AsstSellLead']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>

                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


