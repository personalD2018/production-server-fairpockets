<?php
$proprtieoption = array();

foreach ($properties as $data) {

    //$project_feature = json_decode($data['Property']['features'], 'true');
	
    if (!empty($all_property_features[34])) {
        $option = $all_property_features[34];
    } else {
		if(!empty($propertyoption))
		{
			if ($propertyoption['MstPropertyTypeOption']['transaction_type_id'] == '1') 
			{
				$option = "Residential Property";
			} 
			else 
			{
				$option = "Commercial Property";
			}
		}
		else
		{
			$option = '';
		}
    }

    $option .= " for ";
    if ($data['Property']['transaction_type_id'] == '1') {
        $option .= "Sale";
    } else {
        $option .= "Rent";
    }
    $option .= " in ";

    $option .= $data['Property']['city_data'];
    $option .= " , ";
    $option .= $data['Property']['state_data'];
    $proprtieoption[$data['Property']['property_id']] = $option;
}
?>
<!--./advertisement-Card-->
    <div class="page-content">

                <div class="col-md-12">
                    <h1 class="page-header"> Assisted Sell / Rent - Request Service </h1>
                </div>

            <?php echo $this->Session->flash(); ?> 
            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo $this->Form->create('AsstSellRent', array(
						'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'id_form_AsstSellRent',
                        'novalidate' => 'novalidate'
					));
                    ?>

                    <div class="account-block">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_id" class="label_title">Select Property <span class="mand_field">*</span> </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                           // print_r($proprtieoption);
                                            echo $this->Form->input(
                                                    'prop_id', array(
                                                'id' => 'prop_id',
                                                'options' => $proprtieoption,
                                                'class' => 'selectpicker',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_type" class="label_title">Type </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <div class="select-style ">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_type', array(
                                                'id' => 'prop_type',
                                                'options' => array(
                                                    '1' => 'Rentout',
                                                    '2' => 'Sell'
                                                ),
                                                'class' => 'selectpicker',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                    </div>

                                </div>
                            </div>								
                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group ">


                                    <?php
                                    echo $this->Form->input('prop_iagree', array(
                                        'id' => 'prop_iagree',
                                        'type' => 'checkbox',
                                        'div' => array('class' => 'col-sm-12 '),
                                        'class' => 'checkbox-custom',
                                        'label' => array('class' => 'checkbox-custom-label', 'text' => 'I agree with 1 month Rent as Brokerage <span class="mand_field">*</span>'),
                                            )
                                    );
                                    ?>
                                    <div class="col-sm-12">
                                        <label class="chkerr"></label>
                                    </div>	

                                </div>		


                            </div>	
                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" >Submit</button>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>


                </div>
            </div>



            <div class="clear"></div>



        </div>

<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');

// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("
	
		$('#prop_type').change(function(){
		
			var number = $(this).find('option:selected').val();
			
			if ( number == 1 )
			{
				$('.checkbox-custom-label').text('I agree with 1 month Rent as Brokerage');
				
			}
			
			if ( number == 2 )
			{
				$('.checkbox-custom-label').text('I agree with 1% Brokerage charge');
			}
			
			
		});
		
		var rulesMTarea = {
			required : true
		};
        var messageMTarea = {
            required : 'Field is required.'
        };
        var messageProperty = {
            required : 'Property is required.'
        };
		
		$('#id_form_AsstSellRent').validate({
			
			// Specify the validation rules
			rules: {
				'data[AsstSellRent][prop_iagree]': rulesMTarea,
                'data[AsstSellRent][prop_id]': rulesMTarea
			},
						
			messages: {
                    'data[AsstSellRent][prop_iagree]': messageMTarea,
                    'data[AsstSellRent][prop_id]': messageProperty,
                },
			errorPlacement: function(error, element){
                //if (element.attr('name') == 'data[AsstSellRent][prop_iagree]'){
                element.parent().append(error);
                    //error.append(element.parent());
                    //error.insertAfter('.chkerr');
                //}               
			}
		});	
	");
?>









