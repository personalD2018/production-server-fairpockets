<div class="container-fluid" ng-app="masterDataEntryApp" ng-controller="masterDataEntryController" ng-cloak>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Master Data</h2>
		</div>
	</div>
	<div class="alert alert-success text-center" ng-if="alertMessage != '' && alertMessage != undefined" style="position: absolute;height: 8%;width: 78%;z-index: 10000;">
		<strong>{{alertMessage}}</strong>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-primary btn-sm" style="position: absolute;left: 91%;" ng-click="addNewEntryForm()">Add New</button>
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#country" ng-click="moduleChangeEvent('MstAreaCountry')">Country</a>
							</li>
							<li>
								<a data-toggle="tab" href="#region" ng-click="moduleChangeEvent('MstAreaRegion')">Region</a>
							</li>
							<li>
								<a data-toggle="tab" href="#state" ng-click="moduleChangeEvent('MstAreaState')">State</a>
							</li>
							<li>
								<a data-toggle="tab" href="#city" ng-click="moduleChangeEvent('MstAreaCity')">City</a>
							</li>
							<li>
								<a data-toggle="tab" href="#locality" ng-click="moduleChangeEvent('MstAreaLocality')">Locality</a>
							</li>
						</ul>
						<div class="tab-content content-widgets">
						<!-- <div class="widget-container"> -->
							
							<div id="country" class="tab-pane fade in active">
								<p>
									<table datatable="ng" class="table table-striped">
										<thead>
											<tr>
												<th width="12%">Sl. No.</th>
												<th>Country Name</th>
												<th>Action</th>
											</tr>	
										</thead>
										<tbody>
											<tr ng-repeat="country in countryList">
												<td>{{$index + 1}}</td>
												<td>{{country['MstAreaCountry'].countryName}}</td>
												<td>
													<button class="btn btn-primary btn-xs" ng-click="editEntry($index)">
														<i class="icon-pencil"></i> edit
													</button>
													<!-- <button class="btn btn-primary">remove</button> -->
												</td>
											</tr>
										</tbody>
									</table>
								</p>
							</div>
							
							<div id="region" class="tab-pane fade">
								<p>
									<table class="table table-striped" datatable="ng">
										<thead>
											<tr>
												<th>Sl. No.</th>
												<th>Region Name</th>
												<th>Country</th>
												<th>Action</th>
											</tr>	
										</thead>
										<tbody>
											<tr ng-repeat="region in regionList">
												<td>{{$index + 1}}</td>
												<td>{{region['MstAreaRegion'].regionName}}</td>
												<td>{{region['MstAreaCountry'].countryName}}</td>
												<td>
													<button class="btn btn-primary btn-xs" ng-click="editEntry($index)">
														<i class="icon-pencil"></i> 
														edit
													</button>
													<!-- <button class="btn btn-primary">remove</button> -->
												</td>
											</tr>
										</tbody>
									</table>
								</p>						
							</div>
							
							<div id="state" class="tab-pane fade">
								<p>
									<table class="table table-striped" datatable="ng">
										<thead>
											<tr>
												<th>Sl. No.</th>
												<th>State Name</th>
												<th>Country</th>
												<th>Region</th>
												<th>Action</th>
											</tr>	
										</thead>
										<tbody>
											<tr ng-repeat="state in stateList">
												<td>{{$index + 1}}</td>
												<td>{{state['MstAreaState'].stateName}}</td>
												<td>{{state['MstAreaCountry'].countryName}}</td>
												<td>{{state['MstAreaRegion'].regionName}}</td>
												<td>
													<button class="btn btn-primary btn-xs" ng-click="editEntry($index)"><i class="icon-pencil"></i> edit</button>
													<!-- <button class="btn btn-primary">remove</button> -->
												</td>
											</tr>
										</tbody>
									</table>
								</p>
								
							</div>

							<div id="city" class="tab-pane fade">
								<p>
									<table class="table table-striped" datatable="ng">
										<thead>
											<tr>
												<th>Sl. No.</th>
												<th>City</th>
												<th>State</th>
												<th>Region</th>
												<th>Country</th>								
												<th>Action</th>
											</tr>	
										</thead>
										<tbody>
											<tr ng-repeat="city in cityList">
												<td>{{$index + 1}}</td>
												<td>{{city['MstAreaCity'].cityName}}</td>
												<td>{{city['MstAreaState'].stateName}}</td>
												<td>{{city['MstAreaRegion'].regionName}}</td>
												<td>{{city['MstAreaCountry'].countryName}}</td>
												<td>
													<button class="btn btn-primary btn-xs" ng-click="editEntry($index)"><i class="icon-pencil"></i> edit</button>
													<!-- <button class="btn btn-primary">remove</button> -->
												</td>
											</tr>
										</tbody>
									</table>
								</p>
								
							</div>

							<div id="locality" class="tab-pane fade">
								
								<p>
									<table class="table table-striped" datatable="ng">
										<thead>
											<tr>
												<th>Sl. No.</th>
												<th>Locality</th>
												<th>City</th>
												<th>State</th>
												<th>Region</th>
												<th>Country</th>									
												<th>Action</th>
											</tr>	
										</thead>
										<tbody>
											<tr ng-repeat="locality in localityList">
												<td>{{$index + 1}}</td>
												<td>{{locality['MstAreaLocality'].localityName}}</td>
												<td>{{locality['MstAreaCity'].cityName}}</td>
												<td>{{locality['MstAreaState'].stateName}}</td>
												<td>{{locality['MstAreaRegion'].regionName}}</td>
												<td>{{locality['MstAreaCountry'].countryName}}</td>
												<td>
													<button class="btn btn-primary btn-xs" ng-click="editEntry($index)"><i class="icon-pencil"></i> edit</button>
													<!-- <button class="btn btn-primary">remove</button> -->
												</td>
											</tr>
										</tbody>
									</table>
								</p>
								
							</div>
						<!-- </div> -->
						</div>
					</div>
				</div>
			</div>
			
			
			
			
		</div>
	</div>
	<div id="modalMstAreaCountry" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Country</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Country Name</label>
						<input type="text" ng-model="masterData.data.countryName" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" ng-click="saveMasterData()">
						Save
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div id="modalMstAreaRegion" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Region</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="form-group">
							<label>Select Country</label>				
							<select 
							ng-model="masterData.data.countryCode" 
							class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="country in countryList" ng-value="country['MstAreaCountry'].countryCode">{{country['MstAreaCountry'].countryName}}</option>
							</select>
						</div>
						<div class="form-group">
							<label>Region Name</label>
							<input type="text" ng-model="masterData.data.regionName" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" ng-click="saveMasterData()">
						<i class="icon-floppy"></i> Save
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modalMstAreaState" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add State</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Select Country</label>				
						<select 
							ng-model="countryCode" 
							ng-change="getAllRegionsByCountry(masterData.data.countryCode)" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="country in countryList" ng-value="country['MstAreaCountry'].countryCode">{{country['MstAreaCountry'].countryName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select Region</label>				
						<select 
							ng-model="masterData.data.regionCode" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="region in regionList" ng-value="region['MstAreaRegion'].regionCode">{{region['MstAreaRegion'].regionName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>State Name</label>	
						<input type="text" ng-model="masterData.data.stateName" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" ng-click="saveMasterData()">
						Save
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<div id="modalMstAreaCity" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add State</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Select Country</label>				
						<select 
							ng-model="countryCode" 
							ng-change="getAllRegionsByCountry(countryCode)" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="country in countryList" ng-value="country['MstAreaCountry'].countryCode">
							{{country['MstAreaCountry'].countryName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select Region</label>				
						<select 
							ng-model="regionCode" 
							ng-change="getAllStatesByRegion(regionCode)" class="form-control">

							<option value="">--select--</option>
							<option ng-repeat="region in regionList" ng-value="region['MstAreaRegion'].regionCode">{{region['MstAreaRegion'].regionName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select State</label>				
						<select 
							ng-model="masterData.data.stateCode" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="state in stateList" ng-value="state['MstAreaState'].stateCode" class="form-control">{{state['MstAreaState'].stateName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>City Name</label>	
						<input type="text" ng-model="masterData.data.cityName" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" ng-click="saveMasterData()">
						Save
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modalMstAreaLocality" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add State</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Select Country</label>				
						<select 
							ng-model="countryCode" 
							ng-change="getAllRegionsByCountry(countryCode)" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="country in countryList" ng-value="country['MstAreaCountry'].countryCode">{{country['MstAreaCountry'].countryName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select Region</label>				
						<select 
							ng-model="regionCode" 
							ng-change="getAllStatesByRegion(regionCode)" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="region in regionList" ng-value="region['MstAreaRegion'].regionCode">{{region['MstAreaRegion'].regionName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select State</label>				
						<select 
							ng-model="stateCode"
							ng-change="getAllCitiesByState(stateCode)" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="state in stateList" ng-value="state['MstAreaState'].stateCode">{{state['MstAreaState'].stateName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select City</label>				
						<select 
							ng-model="masterData.data.cityCode" class="form-control">
							<option value="">--select--</option>
							<option ng-repeat="city in cityList" ng-value="city['MstAreaCity'].cityCode">{{city['MstAreaCity'].cityName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Locality Name</label>	
						<input type="text" ng-model="masterData.data.localityName" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" ng-click="saveMasterData()">
						Save
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
