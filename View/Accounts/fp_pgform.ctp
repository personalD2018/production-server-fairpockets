	<script>
		var hash = '<?php echo $hash ?>';
		function submitPayuForm() {
			if(hash == '') {
			return;
			}
			var payuForm = document.forms.payuForm;
			payuForm.submit();
		}
	</script>
  
 <div class="page-content">
	
	<h1 class="page-header"> Portfolio Management - Pay Now </h1>
	
	<div id="build" class="list-detail detail-block target-block">
		
		<div class="block-inner-link">
			<form class="fpform" id="id_form_pg" action="<?php echo $action; ?>" method="post" name="payuForm" >
					
						<input type="hidden" name="service_provider" value="" size="64" />
						<input type="hidden" name="surl" value="<?php echo $this->webroot; ?>accounts/payment_success/" size="64" />
						<input type="hidden" name="furl" value="<?php echo $this->webroot; ?>accounts/payment_failure/" size="64" />
						
						<input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
						<input type="hidden" name="hash" value="<?php echo $hash ?>"/>
						<input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
        
						<div id="build" class="list-detail detail-block target-block">
						
							<h2>Mandatory Parameters</h2>
							
							<div class="add-tab-row push-padding-bottom row-color-gray">
								
								<div class="row">
								
									<div class="col-sm-6">
										
										<div class="form-group">
											<label for="amount" class="label_title" >Amount <span class="mand_field">*</span></label>
											<input type="text" readonly class="form-control box_price allownumericwithoutdecimal" name="amount" value="<?php echo (empty($PortService['payment'])) ? '' : $PortService['payment']; ?>">
										</div>
									
									</div>
									
									<div class="col-sm-6">
										
										<div class="form-group">
											<label for="firstname" class="label_title" >Name <span class="mand_field">*</span></label>
											<input type="text" readonly class="form-control" name="firstname" id="firstname" value="<?php echo (empty($userdata['username'])) ? '' : $userdata['username']; ?>">
										</div>
									
									</div>
									
								</div>
							
								<div class="row">
								
									<div class="col-sm-6">
										
										<div class="form-group">
											<label for="email" class="label_title" >Email <span class="mand_field">*</span></label>
											<input type="text" readonly class="form-control" name="email" id="email" value="<?php echo (empty($userdata['email'])) ? '' : $userdata['email']; ?>" >
										</div>
									
									</div>
									
									<div class="col-sm-6">
										
										<div class="form-group">
											<label for="phone" class="label_title" >Phone <span class="mand_field">*</span></label>
											<input type="text" readonly class="form-control" name="phone" value="<?php echo (empty($userdata['usermobile'])) ? '' : $userdata['usermobile']; ?>" >
										</div>
									
									</div>
									
								</div>
								
								<div class="row">
								
									<div class="col-sm-6">
										
										<div class="form-group">
											<label for="productinfo" class="label_title" >Portfolio Name <span class="mand_field">*</span></label>
											<input type="text" readonly class="form-control" name="productinfo" id="productinfo" value="<?php echo (empty($PortService['port_name'])) ? '' : $PortService['port_name']; ?>" >
										</div>
									
									</div>
									
									
									
								</div>
							
							
								
							
							</div>
						
							<div class="account-block text-center submit_area">
								<button id="id_submit" class="btn btn-primary btn-red" value="1">Pay Now</button>
							</div>
							
						</div>
					
					</form>	
		</div>
	</div>
</div>
					
	
		  
	<script type="text/Javascript">	
	
		$(document).ready(function(){
		
		
			var ruleMRequired = {
					required : true
			};
		
			var messageMRequired = {
					required : 'Field is required.'
			};
			
			$('#id_form_pg').validate({
			
				// Specify the validation rules
				rules: {
					'amount': {
						required : true,
						number : true,
						min : 1
					},
					'firstname': ruleMRequired,
					'email': {
						required : true,
						email : true
					},
					'phone': {
						required : true,
						number : true,
						minlength:10,
						maxlength:10
					},
					'productinfo': ruleMRequired,
					'zipcode':  {
						number : true,
						min : 1
					}

				},
				
				// Specify the validation error messages
				messages: {
					'amount': {
						required : 'Field is required.',
						number : 'Only numeric values allowed.',
						min : '> 0 allowed'
					},
					'firstname': messageMRequired,
					'email': {
						required : 'Field is required.',
						email : 'Please enter a valid email address'
					},
					'phone':  {
						required : 'Field is required.',
						number : 'Only numeric values allowed.',
						minlength: 'Min 10 digits allowed.',
						maxlength: 'Max 10 digits allowed.'
					},
					'productinfo': messageMRequired,
					'zipcode':  {
						number : 'Only numeric values allowed.',
						min : '> 0 allowed'
					}					
				}			
				
			});	
			
					
		});
		
	</script>	  
	  
	<script type="text/javascript">
         
		$(document).ready(function() {
		
			$('[data-toggle="tooltip"]').tooltip();
						
        });		 
	  
	</script>