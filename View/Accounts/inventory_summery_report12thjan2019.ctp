<!--./disable tab button-->
<script>
    $(document).ready(function ()
    {
        $("#transaction_type").find('option').removeAttr("selected");
        $("#property_type_val").find('option').removeAttr("selected");
        $("#property-type").find('option').removeAttr("selected");
        $("#submit_disable").hide(); //submit button disable by default
        $("#tower").css("display", "none");
        $("#wing").css("display", "none");
		$("#inv_area").css("display", "none");
		$("#inv_status").css("display", "none");
        $("#property_amenities_button").css("display", "none");
        $("#property_photo_button").css("display", "none");
    });
</script>

<!--Get property aminities and feature by selecting of property option --> 

<!-- jQuery Form Validation code and submit basic code-->
<script>

// When the browser is ready...
    $(function () {		      
		$("#idForm").validate({			
            submitHandler: function (form) {
			var projectid = $('#property_type_val').val();
			$.ajax
				({
					type:'POST',
					url: "towerblock/"+projectid,                         
					data: 'id='+ projectid, // serializes the form's elements.
					
					cache:false,
					success: function (html)

					{   $("#towerblocks").html(html);
					   // $(".property_id").val(data);
						$("#tower").css("display", "block");
						$("#idForm :input").prop("disabled", true);
						$(".btn-green").prop("disabled", true);
						$("#next1").html('Saved');

					}
				});
			return false;
            }
        })
		
		$("#idForm1").validate({
            submitHandler: function (form) {
			var projectid = $('#property_type_val').val();				
			$.ajax
				({   
					type:'POST',
					url: "wingblock/"+projectid,                         
				 //   data: 'projectid='+ projectid+'&towerids='+, // serializes the form's elements.
					//data: 'projectid='+ projectid,
					data: $('#idForm1').serialize(),
					cache:false,
					success: function (html)
					{
						$("#wingblock").html(html);
					   // $(".property_id").val(data);
						$("#wing").css("display", "block");
						$("#idForm1 :input").prop("disabled", true);
						$(".btn-green").prop("disabled", true);
					  //  $("#next12").html('Saved');

					}
				});
			return false;
            }
        })
		
		$("#idForm2").validate({
            submitHandler: function (form) {
			var projectid = $('#property_type_val').val();				
			$.ajax
				({   
					type:'POST',
					url: "areablock/"+projectid,                         
				 //   data: 'projectid='+ projectid+'&towerids='+, // serializes the form's elements.
					//data: 'projectid='+ projectid,
					data: $('#idForm2').serialize(),
					cache:false,
					success: function (html)
					{
						$("#areablock").html(html);
					   // $(".property_id").val(data);
						$("#inv_area").css("display", "block");
						$("#idForm1 :input").prop("disabled", true);
						$(".btn-green").prop("disabled", true);
					  //  $("#next12").html('Saved');

					}
				});
			return false;
            }
        })
		
		$("#idForm3").validate({
            submitHandler: function (form) {
			var projectid = $('#property_type_val').val();				
			$.ajax
				({   
					type:'POST',
					url: "statusblock/"+projectid,                         
				 //   data: 'projectid='+ projectid+'&towerids='+, // serializes the form's elements.
					//data: 'projectid='+ projectid,
					data: $('#idForm3').serialize(),
					cache:false,
					success: function (html)
					{
						$("#statusblock").html(html);
					   // $(".property_id").val(data);
						$("#inv_status").css("display", "block");
						$("#idForm3 :input").prop("disabled", true);
						$(".btn-green").prop("disabled", true);
					  //  $("#next12").html('Saved');

					}
				});
			return false;
            }
        })

    });

</script>
<style>.btn-primary.disabled, .btn-primary[disabled] {
        background: #ffc000;
        color: #333;
        outline: none;
        box-shadow: none;
    }
    .add-tab-content .add-tab-row {
        border-bottom: none;
    }
	
	#image_preview .property_image{
		padding: 10px;
	}
	
	#image_preview .property_image .icon-delete {
		font-size: 18px;
		color: red;
		position: absolute;
		top: 85%;
		right: 0;
		cursor: pointer;
	}
	.checkincolumn{
		width:150px !important;
		float:left !important;
	}
</style> 
<div class="page-content">
       <div class="text-center wow fadeInDown no-padding-bottom">
			<h1 class="page-header">Inventory </h1><br />
        </div>   
        <div class="add_property_form">
            <form class="fpform" id="idForm">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Select Projects</h3>
                        <div class="add-expand active"></div>
                    </div>
                    <div class="add-tab-content detail-block" style="display:block;">
                        <div class="add-tab-row push-padding-bottom">						
                            <div class="row">                                              
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Project Name<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php //echo $this->Form->input('property_type_id', array('id' => 'property_type_val', 'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder Floor', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'), 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>
											<?php echo $this->Form->select('Project', $allproject,array('id'=>'property_type_val','class'=>'required')); ?>
                                        </div>
                                        <div class="error has-error" id="property_type_val_error" style="display:block;"></div>
                                    </div>					
									
                                </div>
                            </div>
                        </div>
                        <div class="add-tab-row  push-padding-bottom">
                            <div class="account-block text-center submit_area">
                                <button type="submit" class="btn btn-primary btn-next " id="next1">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
                <div class="account-block property_feature" >
                    <div class="add-title-tab">
                        <h3>Select Tower</h3>
                        <div class="add-expand" id="tower"></div>
                    </div>
                    <div class="add-tab-content detail-block" >                        
						<form class="fpform1" id="idForm1">
							<div id="towerblocks"></div>
						</form>
                    </div>
                </div>
				
				<div class="account-block">
                    <div class="add-title-tab">
                        <h3>Select Wing</h3>
                        <div class="add-expand" id="wing"></div>
                    </div>
                    <div class="add-tab-content detail-block" >							
						<form class="fpform1" id="idForm2" method="post">
							<div id="wingblock"></div>
						</form> 
                    </div>
                </div>            
           
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Select Area</h3>
                        <div class="add-expand" id="inv_area"></div>
                    </div>
                    <div class="add-tab-content detail-block" >							
						<form class="fpform1" id="idForm3" method="post">
							<div id="areablock"></div>
						</form> 
                    </div>
                </div>
				
				<div class="account-block">
                    <div class="add-title-tab">
                        <h3>Select Status</h3>
                        <div class="add-expand" id="inv_status"></div>
                    </div>
                    <div class="add-tab-content detail-block" >							
						<form class="fpform1" id="idForm4" action="inventory/view_inventory1/8844" method="post">
							<div id="statusblock"></div>
						</form> 
                    </div>
                </div>
				
				
            
        </div>
        <!--/our-products-->
    </div>
 
<script>
function gMapOptionClick(elem)
	{
		document.getElementById('searchInput').value = elem.innerHTML;
		document.getElementById("result").style.display = 'none';

		//Generate click event
		document.getElementById('searchInput').click();
	}
    $(document).ready(function () {
        $.validator.addMethod('nameCustom', function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z ]+/.test(value);
        }, 'Please use English alphabets only.');

        $.validator.addMethod('pnameCustom', function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9]+/.test(value);
        }, 'Please use Alphanumeric characters only.');


        $('#id_form_project_basic').validate({

            // Specify the validation rules
            rules: {
                'data[Project][project_name]': {
                    required: true,
                    pnameCustom: true
                },
                /*'data[Project][project_highlights]': {
                    required: true
                },
                'data[Project][project_description]': {
                    required: true
                },*/
                'data[Project][land_area]': {
                    //required: true,
                    number: true,
                    min: 1,
                    max: 999999
                }
            },

            // Specify the validation error messages
            messages: {
                'data[Project][project_name]': {
                    required: 'Project Name is required.'
                },
                'data[Project][project_highlights]': {
                    required: 'Project Highlights is required.'
                },
                'data[Project][project_description]': {
                    required: 'Project Description is required.'
                },

                'data[ProjectDetail][land_area]': {
                    required: 'Land Area is required.',
                    number: 'Only Numeric digits allowed.',
                    min: 'Min value allowed is 1',
                    max: 'Max value allowed is 999999'
                }
            },

            submitHandler: function (form) {

                $.ajax
                        ({
                            type: 'POST',
                            url: '<?php echo $this->webroot; ?>Projects/pastproject',
                            data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
                            success: function (data)
                            {
                                $("#project_id").find('option').remove();
                                $("#Successform").css("display", "block");
								$(".sucsok").css("display", "block");
                                $("#projectform").css("display", "none");
                                $('<option>').val('').text('Select').appendTo($("#project_id"));
                                $.each(data, function (key, value) {

                                    $('<option>').val(key).text(value).appendTo($("#project_id"));
                                });
                            }
                        });


                // Validations OK. Form can be submitted

                // TODO : Ajax call

                // Changes of Page when Ajax call is successful



                // To remain on same page after submit
                return false;
            }

        });
    });
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('add_property_7');
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');
?>

<script>
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                    val = $target.attr('data-value'),
                    $inp = $target.find('input'),
                    idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });

        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });
    });



</script>
<script>
    $(document).ready(function () {

        // Parking Checkbox - Special functionality
        $("#property_feature_ajax").on("click", ".resp", function () {

            var $inputs = $('input.resp:checkbox')

            if (($(this).attr('id') == 'id_chk_none') && ($(this).is(':checked')))
            {
                //$inputs.not(this).prop('disabled', true); // <-- disable all but checked one
                $inputs.not(this).prop('checked', false); // <-- uncheck all but checked one

                $('#id_select_cvrd').css('display', 'none');
                $('#id_select_open').css('display', 'none');

            } else if (($(this).attr('id') == 'id_chk_cvrd') && ($(this).is(':checked')))
            {
                //alert("Covered Parking is Checked"); 
                // Do action as  covered parking checked

                $('#id_select_cvrd').css('display', 'inline');
            } else if (($(this).attr('id') == 'id_chk_cvrd') && (!($(this).is(':checked'))))
            {
                //alert("Covered Parking is UnChecked"); 
                // Do action as covered parking is unchecked

                $('#id_select_cvrd').css('display', 'none');
            } else if (($(this).attr('id') == 'id_chk_open') && ($(this).is(':checked')))
            {
                //alert("Open Parking is clicked");  	
                // Do action as open parking is checked

                $('#id_select_open').css('display', 'inline');

            } else if (($(this).attr('id') == 'id_chk_open') && (!($(this).is(':checked'))))
            {
                //alert("Open Parking is clicked");  	
                // Do action for open parking is unchecked

                $('#id_select_open').css('display', 'none');
            } else
            {
                $inputs.prop('disabled', false);

            }
        });


    });
</script>
