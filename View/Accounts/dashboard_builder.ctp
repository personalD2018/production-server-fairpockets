<div class="page-content text-center">
		   <div class="row user-responce">
              <div class="col-md-12">
                <h1 style="text-align:center;">Response & Updates</h1>
              </div>
            <div class="col-md-3 col-sm-3 list">
            	<h3>Current Projects</h3>
                <div class="count">NA</div>
            </div>
            <div class="col-md-3 col-sm-3 list">
            	<h3>Past projects</h3>
                <div class="count">NA</div>
            </div>
            <div class="col-md-3 col-sm-3 list">
            	<h3>listed properties</h3>
                <div class="count">NA</div>
            </div>
			<div class="col-md-3 col-sm-3 list">
            	<h3>Response</h3>
                <div class="count">NA</div>
            </div>
          </div>
          <div class="space-x"></div>
           <div class="row user-responce">
              <div class="col-md-12">
                <h1 style="text-align:center;">Post Projects and Properties</h1>
              </div>
            <div class="col-md-4 col-sm-4 list">
            	<h3>Add current Project</h3>
            	<p><a href="<?php echo $this->webroot; ?>projects/currentproject" class="button btn-add pd10">Click Here</a></p>
            </div>
            <div class="col-md-4 col-sm-4 list">
            	<h3>Add Past project</h3>
            	<p><a href="<?php echo $this->webroot; ?>projects/pastproject" class="button btn-add pd10">Click Here</a></p>
            </div>
            <div class="col-md-4 col-sm-4 list">
            	<h3>List your properties</h3>
            	<p><a href="<?php echo $this->webroot; ?>projects/allprojects" class="button btn-add pd10">Click Here</a></p>
            </div>
          </div>
         </div>
	<div class="space-m"></div>
