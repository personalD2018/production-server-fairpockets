<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Listed Projects</h1>
        </div>
        <div class="col-md-4">
            <div class="input-group custom-search-form">
                <input type="text" id="searchbox" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-yellow" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:20px;">
        
        <div class="col-md-12">
            <table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Project</th>
                        <th>Address</th>
                        <th>Sales Person Name</th>
                        <th>Sales Person Mobile</th>
                        <th>Sales Person Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($sharedProjectsArray as $key=>$row):
                    ?>
                    <tr>
                        <td><?php echo isset($row['ShareProjectsHistory']['project_name']) ? $row['ShareProjectsHistory']['project_name'] : "N/A";?></td>
                        <td><?php echo isset($row['ShareProjectsHistory']['project_address']) ? $row['ShareProjectsHistory']['project_address'] : "N/A";?></td>
                        <td><?php echo isset($row['ShareProjectsHistory']['office_sales_ctname']) ? $row['ShareProjectsHistory']['office_sales_ctname'] : "N/A";?></td>
                        <td><?php echo isset($row['ShareProjectsHistory']['office_sales_ctmobile']) ? $row['ShareProjectsHistory']['office_sales_ctmobile'] : "N/A";?></td>
                        <td><?php echo isset($row['ShareProjectsHistory']['office_sales_ctemail']) ? $row['ShareProjectsHistory']['office_sales_ctemail'] : "N/A";?></td>
                        <td style="text-align: center;">
                            
                            <a href="<?php echo Router::url('/accounts/myProjectsView/'.$row['ShareProjectsHistory']['project_id'],true);?>" title="View" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
  Launch demo modal
</button>-->
							
							<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
							
							</td>
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        deleteInventory = function(){
            var inventory_id = arguments[0];
            jQuery.confirm({
                title: 'Confirm!',
                content: 'Want to delete this inventory?',
                buttons: {
                    confirm: function () {
                        window.location.href = '<?php echo Router::url('/accounts/inventory/delete/',true);?>' + inventory_id
                    }
                }
            });
        }
        var dataTable = jQuery('#inventory_list').DataTable({
            "columnDefs": [{
                "targets": [5], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>