<script>
    $(document).ready(function () {

        /*$("#prop_state").on('change', function () {
            var id = $(this).val();

            $("#prop_city").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "countries", "action" => "getRecity")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#mangers_city').html(html);
                    }
                }).html;
            }
        });*/

        $("#prop_state").on('change', function () {
            var id = $(this).val();
            if (id != '') {
                $.getJSON('/countries/getpecity/' + id, function(data) {
                    $('#prop_city').empty();
                    for(var cityCode in data) {
                        $('#prop_city').append($('<option>', { 
                            'value': cityCode, 
                            'text': data[cityCode] 
                        }));
                    }
                });
            }
        });

        $("#prop_state").trigger('change');

    });
</script>

    <div class="page-content">

                <div class="row">

                    <div class="col-md-12">
                        <h1 class="page-header"> Assisted Buy / Rent - Add Requirement </h1>
                    </div>

                </div>
                <?php echo $this->Session->flash(); ?> 
                <div id="build" class="list-detail detail-block target-block">

                    <div class="block-inner-link">

                        <?php
                        echo $this->Form->create(
                                'BuyRentRequirement', array(
                            'class' => 'fpform',
                            'role' => 'form',
                            'id' => 'id_form_AsstBuyRent',
                            'novalidate' => 'novalidate'
                                )
                        );
                        ?>


                        <div class="account-block">

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_reqt"  class="label_title">Requirement </label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            $options = array(
                                                '1' => 'Rent',
                                                '2' => 'Buy'
                                            );

                                            $attributes = array(
                                                'legend' => false,
                                                'class' => 'radio-custom',
                                                'label' => array('class' => 'radio-custom-label'),
                                                'default' => '1',
                                                'onclick' => 'SetIAgreeOption()'
                                            );

                                            echo $this->Form->radio('prop_reqt', $options, $attributes);
                                            ?>									
                                        </div>


                                    </div>
                                </div>									

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_type"  class="label_title">Property Type </label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            $options = array(
                                                '1' => 'Residential',
                                                '2' => 'Commercial'
                                            );

                                            $attributes = array(
                                                'legend' => false,
                                                'class' => 'radio-custom',
                                                'label' => array('class' => 'radio-custom-label'),
                                                'default' => '1',
                                            );

                                            echo $this->Form->radio('prop_type', $options, $attributes);
                                            ?>									
                                        </div>		
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_cstat" class="label_title">Completion status </label>
                                        </div>

                                        <div class="col-sm-12">

                                            <div class="select-style ">
                                                <?php
                                                echo $this->Form->input(
                                                        'prop_cstat', array(
                                                    'id' => 'prop_cstat',
                                                    'options' => array(
                                                        '1' => 'Ready to move in',
                                                        '2' => 'Under Construction'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_purpose" class="label_title">Purpose </label>
                                        </div>

                                        <div class="col-sm-12">

                                            <div class="select-style ">
                                                <?php
                                                echo $this->Form->input(
                                                        'prop_purpose', array(
                                                    'id' => 'prop_purpose',
                                                    'options' => array(
                                                        '1' => 'End Use',
                                                        '2' => 'Investment'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>

                                        </div>

                                    </div>
                                </div>								
                            </div>

                            <br>
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_state" class="label_title">State </label>
                                        </div>

                                        <div class="col-sm-12">

                                            <div class="select-style ">
                                                <?php
                                                echo $this->Form->input(
                                                        'prop_state', array(
                                                    'id' => 'prop_state',
                                                    'options' => $state_master,
                                                    'class' => 'selectpicker',
                                                    'label' => false,
                                                        )
                                                );
                                                ?>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_city" class="label_title">City <span class="mand_field">*</span></label>
                                        </div>

                                        <div class="col-sm-12">

                                            <div class="select-style " id="mangers_city">
                                                <?php
                                                echo $this->Form->input(
                                                        'prop_city', array(
                                                    'id' => 'prop_city',
                                                    'options' => '',
                                                    'class' => 'selectpicker required',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>

                                        </div>

                                    </div>
                                </div>								
                            </div>

                            <br>
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_locality" class="label_title">Locality <span class="mand_field">*</span></label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_locality', array(
                                                'id' => 'prop_locality',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>	

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_budget" class="label_title">Budget <span class="mand_field">*</span></label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            echo $this->Form->input(
                                                    'prop_budget', array(
                                                'id' => 'prop_budget',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>	

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <br>
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_oth" class="label_title">Have you seen any property ? </label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            echo $this->Form->textarea(
                                                    'prop_oth', array(
                                                'id' => 'prop_oth',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false,
                                                'rows' => '3'
                                                    )
                                            );
                                            ?>	

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <label for="prop_rmks" class="label_title">Remarks </label>
                                        </div>

                                        <div class="col-sm-12">
                                            <?php
                                            echo $this->Form->textarea(
                                                    'prop_rmks', array(
                                                'id' => 'prop_rmks',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false,
                                                'rows' => '3'
                                                    )
                                            );
                                            ?>	

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <br>
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="form-group">


                                        <?php
                                        echo $this->Form->input('prop_iagree', array(
                                            'id' => 'prop_iagree',
                                            'type' => 'checkbox',
                                            'div' => array('class' => 'col-sm-12'),
                                            'class' => 'checkbox-custom',
                                            'label' => array('class' => 'checkbox-custom-label', 'text' => 'I agree with 1 month Rent as Brokerage <span class="mand_field">*</span>'),
                                                )
                                        );
                                        ?>
                                        <div class="col-sm-12">
                                            <label class="chkerr"></label>
                                        </div>	
                                    </div>		


                                </div>	
                            </div>

                            <br>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <button type="submit" name="save" value="2" class="btn btn-primary" >Submit</button>
                                            <button type="submit" name="save" value="1" class="btn btn-primary" >Save</button>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- </form> -->	
                        <?php
                        echo $this->Form->end();
                        ?>


                    </div>
                </div>



                <div class="clear"></div>



            </div>
    <?php
    echo $this->Html->script('validate_1.9_jquery.validate.min');
    echo $this->Html->script('front/additional-methods.min');

// Script  : To set validation rules for FORMS
    echo $this->Html->scriptBlock("
	
		function SetIAgreeOption()
		{
		
			if (document.getElementById('BuyRentRequirementPropReqt1').checked == true )
			{
				
				$('.checkbox-custom-label').text('I agree with 1 month Rent as Brokerage');
				
			}
			
			if (document.getElementById('BuyRentRequirementPropReqt2').checked == true )
			{
				$('.checkbox-custom-label').text('I agree with 1% Brokerage charge');
			}
			
			
		}
	
		var rulesMTarea = {
					required : true
		};
		
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_AsstBuyRent').validate({
			
			// Specify the validation rules
			rules: {
				'data[BuyRentRequirement][prop_locality]': rulesMTarea,
				'data[BuyRentRequirement][prop_budget]': rulesMTarea,
				'data[BuyRentRequirement][prop_iagree]': rulesMTarea
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[BuyRentRequirement][prop_locality]': messageMTarea,
				'data[BuyRentRequirement][prop_budget]': messageMTarea,
				'data[BuyRentRequirement][prop_iagree]': messageMTarea
			},
			
			errorPlacement: function(error, element){
				if (element.attr('name') == 'data[BuyRentRequirement][prop_iagree]'){
					error.insertAfter('.chkerr');
				}
				else
				{
					error.insertAfter(element); 
				}		
			}
		});	
	");
    ?>
	
