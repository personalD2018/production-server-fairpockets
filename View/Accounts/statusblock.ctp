<div class="account-block">	
	
	<div class="row">
		<div class="col-sm-12"><label class="label_title" style="font-weight:bold;">Select Status</label></div><br>
		
		<input type="hidden" name="selectedInvArrJson" value='<?php echo $selectedInvArrJson; ?>'>
		<input type="hidden" name="selectedAreaArrJson" value='<?php echo $selectedAreaArrJson; ?>'>
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
			
			<tbody> <?php //echo '<pre>'; print_r($allstatus); ?>
					<?php  if (!empty($allstatus)) {
						for ($i = 0; $i < count($allstatus); $i++) {
					?>
					<?php echo $i % 4 == 0 ? "<tr>\n" : ""; ?>
						<td>
							<input type="checkbox" name="check_list_status[<?php echo $i; ?>]" 
								id="check_list_status[<?php echo $allstatus[$i]['Inventory_propertie']['flat_status'].$i; ?>]"
								value="<?php echo $allstatus[$i]['Inventory_propertie']['flat_status']; ?>"
							class="checkbox-custom">	   
							<label for="check_list_status[<?php echo $allstatus[$i]['Inventory_propertie']['flat_status'].$i; ?>]" class="checkbox-custom-label">
							<?php echo $statuses[$allstatus[$i]['Inventory_propertie']['flat_status']]; ?></label>
						</td>
					<?php echo $i % 4 == 3 ? "</tr>\n" : ""; ?>
					<?php } ?>
					<tr>
					<td>
							<input type="checkbox" name="check_list_status8" 
								id="check_list_status8"
								value="8"
							class="checkbox-custom">	   
							<label for="check_list_status8" class="checkbox-custom-label">
							All</label>
						</td>
					</tr>
					<?php }else{ ?>
					<tr>
						<td colspan="4" style="text-align:center;">
							No Status Available.</a>
						</td>
					</tr>
					<?php } ?>
			</tbody>
		</table>								 
	</div>	
	<div class="add-tab-row  push-padding-bottom">
			<div class="account-block text-center submit_area">
			<button type="submit" class="btn btn-primary btn-next " id="next14">Submit</button>
			</div>
	</div>
</div>