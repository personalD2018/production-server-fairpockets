<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate(
                {
                    rules: {
                        "data[PortService][payment]": {
                            required: true,
                            number: true,
                            min: 1
                        },

                        "data[PortService][CAGR]": {
                            required: true,
                            number: true,
                            min: 1,
                            max: 100
                        },

                        messages: {

                            "data[PortService][payment]": {

                                number: "Enter Only Numeric",
                                min: "Value must be greater than 0",

                            },
                            "data[PortService][CAGR]": {

                                number: "Enter Only Numeric",
                                min: "Value must be greater than 0",
                                max: "Value must be less or equal to  100",
                            }
                        }


                    }
                }
        )
    });

    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
    });
</script>	

<script>
    $(document).ready(function () {
<?php
if($PortService['PortService']['status'] == '')
{
?>
 $("#payment_hide").hide();
<?php
}
if ($PortService['PortService']['status'] == '2') {
    ?>
            $("#rejection_hide").hide();
<?php }
?>

<?php
if ($PortService['PortService']['status'] == '4' ) {
    ?>
            $("#payment_hide").hide();
<?php }
?>
        $("#status").on('change', function () {
            var id = $(this).val();

            if (id == '2') {
                $("#rejection_hide").hide();
            } else
            {
                $("#rejection_hide").show();
            }
            if (id == '1') {
                $("#payment_hide").show();
            } else
            {
                $("#payment_hide").hide();
            }
			
			if (id == '4' || id == '') {
                $("#payment_hide").hide();
            } else
            {
                $("#payment_hide").show();
            }

        });


    });
</script>


<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Portfolio</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Update FP Portfolio</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('PortService', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>
                        <div class="control-group">
                            <label class="control-label">Portfolio</label>
                            <div class="controls">
                                <?php echo $PortService['PortService']['port_name']; ?>
                            </div>
                        </div>
                        <div id="rejection_hide">
                            <div class="control-group">
                                <label class="control-label">Payment</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('payment', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'class' => 'span6 required')); ?>
                                </div>
                            </div>

                            <div id="payment_hide">
                                <div class="control-group">
                                    <label class="control-label">Latest Overall CAGR</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('CAGR', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'class' => 'span6 required')); ?> %
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Latest pdf</label>
                                    <div class="controls">
                                        <?php echo $this->Form->file('latest_pdf', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'fileupload required')); ?>

                                        <a target="_blank" href="<?php echo @$this->webroot . @$this->request->data['PortService']['latest_pdf']; ?>"><?php echo @$this->request->data['PortService']['latest_pdf']; ?></a>
                                        <?php echo $this->Form->input('latest_pdf1', array('type' => 'hidden', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'report price', 'class' => 'small', 'value' => @$this->request->data['PortService']['latest_pdf'])); ?>	

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Valuation</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('Valuation', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Valuation', 'class' => 'span6 required number')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Created On</label>
                                    <div class="controls">
                                        <div id="datetimepicker4" class="input-append">
                                            <?php echo $this->Form->input('Created_on', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please enter Date of Creation of Portfolio', 'class' => 'datepicker small required span12', 'readonly' => 'true')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Service status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'status', 'options' => array('1' => 'Approved', '2' => 'Rejected', '3' => 'closed', '4' => 'Payment Pending'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>					