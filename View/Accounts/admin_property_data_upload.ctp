<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="content-widgets light-gray">
				<div class="widget-head orange">
					<h3>Upload Property Data</h3>
				</div>
				<div class="widget-container">
					<span class="form-group">
						<input type="file" id="file-upload" class="form-control">	
						<button class="btn btn-primary btn-sm" id="upload-file-btn">upload</button>
					</span>					
					
				</div>
			</div>
		</div>
	</div>
</div>


<div class="alert-custom" style="display: none;"></div>
<div class="alert-custom-msg" style="display: none;">Please Wait...</div>
<style type="text/css">

	.alert-custom {
		position: fixed;
		z-index: 10000000;
		top:0;
		left: 0;
		width: 100%;
		height: 100%;
		background: #ccc;
		opacity: 0.4;
	}
	.alert-custom-msg {
		position: fixed;
		z-index: 10000020;
		top:20%;
		left: 0;
		text-align: center;
		vertical-align: center;
		width: 100%;
		height: 100%;
	}
</style>
<script type="text/javascript">

	$('#upload-file-btn').click(function() {
		if($('#file-upload')[0].files.length > 0) {
			var formData = new FormData();
			formData.append('data[file]', $('#file-upload')[0].files[0]);
			var promise = $.ajax({
				url: '/admin/accounts/upload_property_details',
				method: 'POST',
				data: formData,
				beforeSend: function() {
					$('.alert-custom').show();
					$('.alert-custom-msg').show();
				},
				processData: false,
	    		contentType: false,
	    		success: function(res) {
					console.log(res);
					alert('success');
					$('.alert-custom').hide();
					$('.alert-custom-msg').hide();
					$('#file-upload')[0].files = [];
				},
				error: function(err) {
					console.log(err);
					alert('failed');
					$('.alert-custom').hide();
					$('.alert-custom-msg').hide();
					$('#file-upload')[0].files = [];
				}
			});
		} else {
			alert('Please select a file to continue');
		}
		
	})
	
</script>