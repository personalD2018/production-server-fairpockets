<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Post Your Project Pricing1</h2>
            <br />
        </div>	
		<?php //print_r($allproject); ?>

        <div class="add_property_form">

            <?php
            echo $this->Form->create(
                    'ProjectPricing', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_basic',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block">

                <div class="add-title-tab">
                    <h3>Project Basic </h3>
                    <div class="add-expand active" id="id_proj_basic_exp"></div>
                </div>

                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display:block;">
                   
                        <div class="row">

                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title" style="padding-bottom:17px;">Project Name</label>
                                        <div class="select-style">
                                            
											 <?php
                                            echo $this->Form->input(
                                                    'project_id', array(
                                                'id' => 'project_id',
                                                'options' => $allproject,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>
                                        <div class="error has-error" id="property_type_val_error" style="display:block;"></div>
                                    </div>					
									
                                </div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label for="countryState" class="label_title">Payment Plan Name </label>									
									<?php
									echo $this->Form->input(
											'payment_plan1', array(
										'id' => 'payment_plan1',
										'type' => 'text',
										'class' => 'form-control',
										'placeholder' => 'Payment Plan',
										'label' => false
											)
									);
									?>
								</div>
							</div>
							
							<!--<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title" style="padding-bottom:22px;">Payment Plan Name<span class="mand_field">*</span></label>
                                        <br>
                                            <?php //echo $this->Form->input('payment_plan1', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Project Plan', 'class' => '')); ?>
                                        
                                        
                                        <div class="error has-error" id="property_type_val_error" style="display:block;"></div>
                                    </div>					
									
                                </div>-->
							
							
                            
                        </div>


                        <!--<div class="row">
							<div class="col-md-12">
                            <input id="id_search_adm_area_1" class="form-controls" type="hidden" >
                            <input id="id_search_adm_area_2" class="form-controls" type="hidden" >



                            <div class="form_area">
                                <?php //echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
                                
                                <?php //echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>
                                <?php //echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
                                <?php //echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
                                <?php //echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
                                <?php// echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                <?php //echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
                                <?php //echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
                                <?php //echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

                            </div>
                            </div>
                        </div>-->
                        <?php
                        echo $this->Form->input('current_project', array(
                            'type' => 'hidden',
                            'label' => false,
                            'value' => '1'
                                )
                        );
                        ?>
                        <div class="account-block text-center submit_area">
                            <div id='loadingmessage1' style='display:none'>
                                <img src='../img/ajax-loader.gif'/>
                            </div>
                            <button type="submit" id="id_next1" class="btn btn-primary btn-next ">NEXT</button>
                        </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>

           
            <?php
            echo $this->Form->create(
                    'ProjectPricing', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing',
                'novalidate' => 'novalidate'
                    )
            );
            ?>
			
            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Project Pricing</h3>
                    <div class="add-expand" id="id_proj_pricing_exp"></div>
                </div>

                <div class="add-tab-content" id="id_proj_pricing_content">
					<div class="add-tab-row push-padding-bottom row-color-gray">	

                        <h3>Basic Selling Price</h3>
                        <input type="hidden" id="id_cloneProjectBspChargeCount" value="40" >

                        <!--<h3><center>OR</center></h3>-->
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab" >
                            <?php //echo '<pre>'; print_r($this->request->data['bspcharges']);
							if (array_key_exists("unit_bsp1",$this->request->data['bspcharges'])){
								$limit = count($this->request->data['bspcharges']) / 4;															
							}else{
								$limit = count($this->request->data['bspcharges']) / 3;
							}
							//echo $limit;
							
                            if (!empty($this->request->data['bspcharges'])) {
                                for ($i = 1; $i <= $limit; $i++) {
                                    ?>
                                    <div class="row cloneProjectBspCharge" id="cloneProjectBspCharge<?php echo $i; ?>" >
										<div class="col-sm-3">
											<div class="form-group">		
												 <label for="price_bsp1" class="label_title">Basic Selling Price(BSP)</label>
													<?php
													echo $this->Form->input(
															'bspcharges.price_bsp'.$i, array(
														'id' => 'price_bsp' . $i,
														'type' => 'text',
														'class' => 'form-control box_price',
														'label' => false
															)
													);
													?>
		
											</div>
										</div>
										
										<div class="col-sm-2">
											<div class="form-group">			
												<label for="price_bsp1" class="label_title">Unit</label>
												<div class="select-style">
													<?php
														$bspUnits=array('1'=>'Sq.Ft','2'=>'Amount');																								
														echo $this->Form->input(
															'bspcharges.unit_bsp'.$i, array(
														'id' => 'unit_bsp' . $i,
														'options' => $bspUnits,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
														);
													?>
												</div>
											</div>
										</div>
                                      
                                     
                                    <div class="col-sm-2">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">From Floor</label>

                                        <div class="select-style">


                                            <?php

                                            $bspFromToOptions=array_combine(range(-1,75,1),range(-1,75,1));
                                            $bspFromToOptions[-1]='Lower Ground';
											$bspFromToOptions[0]='Ground';
                                            

                                            echo $this->Form->input(
                                                    'bspcharges.from_floor'.$i, array(
                                                'id' => 'from_floor' . $i,
                                                'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">To Floor</label>

                                        <div class="select-style">


                                            <?php
                                            echo $this->Form->input(
													'bspcharges.to_floor' . $i, array(
                                                            'id' => 'to_floor' . $i,
                                                'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>
								
								<div class="col-sm-2">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">Area Status</label>

                                        <div class="select-style">


                                            <?php
											$areaStatusUnit=array('1'=>'SB Area','2'=>'B Area','3'=>'C Area');
                                            echo $this->Form->input(
													'bspcharges.area_cal_status' . $i, array(
                                                            'id' => 'area_cal_status' . $i,
                                                'options' => $areaStatusUnit,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>

                                        <div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                                    </div>		


                                    <?php
                                }
                            } else {
                                ?>
                                <div class="row cloneProjectBspCharge" id="cloneProjectBspCharge1" >	
								<div class="col-sm-4">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Basic Selling Price ( BSP )/ Sq.ft</label>
                                        <?php
                                        echo $this->Form->input(
                                                'bspcharges.price_bsp1', array(
                                            'id' => 'price_bsp1',
                                            'type' => 'text',
                                            'class' => 'form-control box_price',
                                            'label' => false
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-3">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">From Floor</label>

                                        <div class="select-style">


                                            <?php
											 //$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
                                            //$bspFromToOptions[0]='Ground';
											
											$bspFromToOptions=array_combine(range(-1,75,1),range(-1,75,1));
                                            $bspFromToOptions[-1]='Lower Ground';
											$bspFromToOptions[0]='Ground';
											
                                            echo $this->Form->input(
                                                    'bspcharges.from_floor1', array(
                                                'id' => 'from_floor1',
                                                 'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'empty' => 'From'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-1">
									<div class="form-group"> 
                                        <label for="property-price-before" class="label_title">To Floor</label>
									</div>
								</div>
								<div class="col-sm-3">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title"></label>

                                        <div class="select-style">


                                            <?php
                                            echo $this->Form->input(
                                                    'bspcharges.to_floor1', array(
                                                'id' => 'to_floor1',
                                                 'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'empty' => 'To'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
								
							</div>		

                            <?php } ?>
						</div>

                        <div class="row">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRow" onclick="cloneProjectBspChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
							

                    </div>
					
					
					
					<div class="add-tab-row push-padding-bottom row-color-gray" style="display:none;">
                        <h3>Additional Space</h3>                        
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab_space" >
						<?php 
                            if (!empty($this->request->data['bspchargesspace'])) {
                                for ($i = 1; $i <= count($this->request->data['bspchargesspace']) / 4; $i++) {
                                    ?>
                                    <div class="row cloneProjectBspChargeSpace" id="cloneProjectBspChargeSpace<?php echo $i; ?>" >
										<div class="col-sm-3">
											<div class="form-group">		
												 <label for="floor_plc_amount1" class="label_title">Select space</label>
												
												<div class="select-style">
																								
												<?php
														echo $this->Form->input(
																'bspchargesspace.space_mode'.$i, array(
															'id' => 'space_mode'.$i,
															'options' => array(
																'1' => 'Terrace',
																'2' => 'Lawn'
															),
															'class' => 'selectpicker bs-select-hidden',
															'label' => false
																)
														);
													?>
												
												</div>
		
											</div>
										</div>
										
										<div class="col-sm-2">
											<div class="form-group">
												<label for="property-price-before" class="label_title">From Floor</label>
												<div class="select-style">
													
													<?php
													$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													$bspFromToOptions[0]='Ground';
													echo $this->Form->input(
															'bspchargesspace.space_from_floor'.$i, array(
														'id' => 'space_from_floor'.$i,
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>
												</div>
											</div>
										</div>
										
																				
										<div class="col-sm-2">
											<div class="form-group">
												<label for="property-price-before" class="label_title">Area  / Sq.Ft</label>
												
												<?php
												echo $this->Form->input(
														'bspchargesspace.floor_space_area'.$i, array(
													'id' => 'floor_space_area'.$i,
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>
											</div>
										</div>
								
										<div class="col-sm-3">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">Amount</label>

												
												<?php
												echo $this->Form->input(
														'bspchargesspace.floor_space_amount' . $i, array(
													'id' => 'floor_space_amount' . $i,
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>
											</div>
										</div>
                                      
                                     
                                    
								
								

                                        <div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRowSpace(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>

                                    </div>		


                                    <?php
                                }
                            } else {
                                ?>                                
								<div class="row cloneProjectBspChargeSpace" id="cloneProjectBspChargeSpace1" >
										<div class="col-sm-3">
											<div class="form-group">		
												 <label for="price_bsp1" class="label_title">Select Space</label>
												<div class="select-style">
																								
												<?php
														echo $this->Form->input(
																'bspchargesspace.space_mode1', array(
															'id' => 'space_mode1',
															'options' => array(
																'1' => 'Terrace',
																'2' => 'Lawn'
															),
															'class' => 'selectpicker bs-select-hidden',
															'label' => false
																)
														);
													?>
												
												</div>
											</div>
										</div>
										
										
										
																				
										<div class="col-sm-2">
											<div class="form-group">
												<label for="property-price-before" class="label_title">From Floor</label>
												<div class="select-style">
													<?php
													$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													$bspFromToOptions[0]='Ground';
													echo $this->Form->input(
															'bspchargesspace.space_from_floor1', array(
														'id' => 'space_from_floor1',
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>
												</div>
											</div>
										</div>
										
										<div class="col-sm-2">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">Area</label>

												

													
													<?php
												echo $this->Form->input(
														'bspchargesspace.floor_space_area1', array(
													'id' => 'floor_space_area1',
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>

												
											</div>
										</div>
								
										<div class="col-sm-3">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">Amount / Sq.Ft</label>

												


													<?php
													/*echo $this->Form->input(
															'bspchargesplc.plc_to_floor1', array(
																	'id' => 'plc_to_floor1',
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);*/
													?>
													
													<?php
												echo $this->Form->input(
														'bspchargesspace.floor_space_amount1', array(
													'id' => 'floor_space_amount1',
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>

												
											</div>
										</div>
                                      
                                     
                                    
								
								

                                        <div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRowSpace(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>

                                    </div>

                            <?php } ?>
							</div>

                        <div class="row">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRowSpace" onclick="cloneProjectBspChargeAddRowSpace();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
							

                    </div>
					
					
					
					<div class="add-tab-row push-padding-bottom row-color-gray">
                        <h3>Basic PLC Price</h3>                        
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab_plc" >
						<?php //echo '<pre>'; print_r($this->request->data['bspchargesplc']);
						if (array_key_exists("unit_plc1",$this->request->data['bspchargesplc'])){
								$limit1 = count($this->request->data['bspchargesplc']) / 5;															
							}else{
								$limit1 = count($this->request->data['bspchargesplc']) / 4;
							}
						
                            if (!empty($this->request->data['bspchargesplc'])) {
                                for ($i = 1; $i <= $limit1; $i++) {
                                    ?>
                                    <div class="row cloneProjectBspChargePlc" id="cloneProjectBspChargePlc<?php echo $i; ?>" >
										<div class="col-sm-3" style="width:21%;">
											<div class="form-group">		
												 <label for="price_bsp1" class="label_title">Amount Per Floor</label>
												<?php
												echo $this->Form->input(
														'bspchargesplc.floor_plc_amount'.$i, array(
													'id' => 'floor_plc_amount' . $i,
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>
		
											</div>
										</div>
										
										<div class="col-sm-2" style="width:15%;">
											<div class="form-group">			
												<label for="price_bsp1" class="label_title">Unit</label>
												<div class="select-style">
													<?php
														$plcUnits=array('1'=>'Sq.Ft','2'=>'Amount');																								
														echo $this->Form->input(
															'bspchargesplc.unit_plc'.$i, array(
														'id' => 'unit_plc1'.$i,
														'options' => $plcUnits,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
														);
													?>
												</div>
											</div>
										</div>
										
										<div class="col-sm-3" style="width:22%;">
											<div class="form-group">
												<label for="property-price-before" class="label_title">Per floor Mode</label>
												<div class="select-style">
													<?php
														echo $this->Form->input(
																'bspchargesplc.per_floor_mode'.$i, array(
															'id' => 'per_floor_mode' . $i,
															'options' => array(
																'1' => 'Equal per floor',
																'2' => 'Add per floor',																
																'3' => 'Subtract per floor'
															),
															'class' => 'selectpicker bs-select-hidden',
															'label' => false
																)
														);
													?>
												</div>
											</div>
										</div>
										
																				
										<div class="col-sm-2">
											<div class="form-group">
												<label for="property-price-before" class="label_title">From Floor</label>
												<div class="select-style">
													<?php
													//$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													//$bspFromToOptions[0]='Ground';
													
													$bspFromToOptions=array_combine(range(-1,75,1),range(-1,75,1));
													$bspFromToOptions[-1]='Lower Ground';
													$bspFromToOptions[0]='Ground';
													
													echo $this->Form->input(
															'bspchargesplc.plc_from_floor'.$i, array(
														'id' => 'plc_from_floor' . $i,
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>
												</div>
											</div>
										</div>
								
										<div class="col-sm-2">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">To Floor</label>

												<div class="select-style">


													<?php
													echo $this->Form->input(
															'bspchargesplc.plc_to_floor' . $i, array(
																	'id' => 'plc_to_floor' . $i,
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>

												</div>
											</div>
										</div>
                                      
                                     
                                    
								
								

                                        <div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRowPlc(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>

                                    </div>		


                                    <?php
                                }
                            } else {
                                ?>
                                
								<div class="row cloneProjectBspChargePlc" id="cloneProjectBspChargePlc1" >
										<div class="col-sm-3">
											<div class="form-group">		
												 <label for="price_bsp1" class="label_title">Amount Per Floor</label>
												<?php
												echo $this->Form->input(
														'bspchargesplc.floor_plc_amount1', array(
													'id' => 'floor_plc_amount1',
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>
		
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group">
												<label for="property-price-before" class="label_title">Per floor Mode</label>
												<div class="select-style">
													<?php
														echo $this->Form->input(
																'bspchargesplc.per_floor_mode1', array(
															'id' => 'per_floor_mode1',
															'options' => array(
																'1' => 'Equal per floor',
																'2' => 'Add per floor',																
																'3' => 'Subtract per floor'
															),
															'class' => 'selectpicker bs-select-hidden',
															'label' => false
																)
														);
													?>
												</div>
											</div>
										</div>
										
																				
										<div class="col-sm-2">
											<div class="form-group">
												<label for="property-price-before" class="label_title">From Floor</label>
												<div class="select-style">
													<?php
													$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													$bspFromToOptions[0]='Ground';
													echo $this->Form->input(
															'bspchargesplc.plc_from_floor1', array(
														'id' => 'plc_from_floor1',
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>
												</div>
											</div>
										</div>
								
										<div class="col-sm-2">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">To Floor</label>

												<div class="select-style">


													<?php
													echo $this->Form->input(
															'bspchargesplc.plc_to_floor1', array(
																	'id' => 'plc_to_floor1',
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>

												</div>
											</div>
										</div>
                                      
                                     
                                    
								
								

                                        <div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRowPlc(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>

                                    </div>

                            <?php } ?>
							</div>

                        <div class="row">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRowPlc" onclick="cloneProjectBspChargeAddRowPlc();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
							

                    </div>
					
					
					
					
                    <div class="add-tab-row push-padding-bottom" >

                        <div class="row">

                             <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">GST on BSP ( in % ) <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="GST component on Base Price."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_srtax_bsp', array(
                                        'id' => 'price_srtax_bsp',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">GST on Others ( in % ) <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="GST component on Additional Charges."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_srtax_oth', array(
                                        'id' => 'price_srtax_oth',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
							
							<div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">GST Input Credit ( in % ) <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="GST component on Additional Charges."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_srtax_icr', array(
                                        'id' => 'price_srtax_icr',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>


                        </div>

                        <div class="row">

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <div class="row">

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Registration Charges<span class="mand_field">*</span></label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Registration charges paid to government."></i>
                                        </div>

                                        <div class="col-sm-8">
                                            <?php
                                            echo $this->Form->input(
                                                    'price_registration', array(
                                                'id' => 'price_registration',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-4">

                                            <div class="select-style">
                                                <?php
                                                echo $this->Form->input(
                                                        'price_reg_unit', array(
                                                    'id' => 'price_reg_unit',
                                                    'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                        </div>	

                                    </div>	

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <div class="row">

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Stamp Duty Charges</label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Stamp Duty charges paid to government."></i>
                                        </div>

                                        <div class="col-sm-8">
                                            <?php
                                            echo $this->Form->input(
                                                    'price_stamp', array(
                                                'id' => 'price_stamp',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                        <div class="col-sm-4" >

                                            <div class="select-style" >
                                                <?php
                                                echo $this->Form->input(
                                                        'price_stm_unit', array(
                                                    'id' => 'price_stm_unit',
                                                    'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                        </div>


                                    </div>


                                </div>

                            </div>


                        </div>

                    </div>	

                    <div class="add-tab-row push-padding-bottom row-color-gray">

                        <h3>Project Charges</h3>

                        <input type="hidden" id="id_cloneProjectChargeCount" value="25" >
                        <div class="addrowarea morerow" id="id_cloneProjectCharge_tab" >
                            <?php
                            if (!empty($this->request->data['charges'])) {
                                for ($i = 1; $i <= count($this->request->data['charges']) / 4; $i++) {
                                    ?>
                                    <div class="row cloneProjectCharge" id="cloneProjectCharge<?php echo $i; ?>" >

                                        <div class="col-sm-4">
                                            <div class="form-group">

                                                <label for="property-type" class="label_title">Name of Project Charges </label>

                                                <div class="select-style">

                                                    <?php
                                                    echo $this->Form->input(
                                                            'charges.price_pcharge' . $i, array(
                                                        'id' => 'price_pcharge' . $i,
                                                        'options' => array(
                                                            '0' => 'Select',
															'1' => 'One Covered Car Park',
															'2' => 'Double Covered Car Park',
															'3' => 'Club Membership',
															'4' => 'Total Power Backup Cost',
															'13' => 'Power BackUp per KVA',
															'5' => 'Interest Free Maintenance Deposit',
															'6' => 'Road Facing PLC ',
															'7' => 'Park Facing PLC',
															'8' => 'Corner PLC',
															'9' => 'Sinking Fund',
															'10' => 'View PLC 1',
															'11' => 'View PLC 2',
															'12' => 'Swimming Pool PLC',
															'14' => 'Hill View PLC',
															'15' => 'Additional Car Parking'
															
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden uniqueProjectCharges',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>

                                                </div>		

                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group"> 

                                                <label for="property-price-before" class="label_title">Type of Charge</label>

                                                <div class="select-style">


                                                    <?php
                                                    echo $this->Form->input(
                                                            'charges.price_pcharge_type' . $i, array(
                                                        'id' => 'price_pcharge_type' . $i,
                                                        'options' => array(
                                                            '1' => 'Mandatory',
                                                            '2' => 'Optional'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden',
                                                        'label' => false,
                                                            )
                                                    );
                                                    ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group"> 

                                                <div class="col-sm-12"> 
                                                    <label class="label_title">Amount </label>
                                                </div>

                                                <div class="col-sm-5">
                                                    <?php
                                                    echo $this->Form->input(
                                                            'charges.price_pcharge_amt' . $i, array(
                                                        'id' => 'price_pcharge_amt' . $i,
                                                        'type' => 'text',
                                                        'class' => 'form-control box_price uniqueProjectChargesAmt',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                </div>

                                                <div class="col-sm-7">
                                                    <div class="select-style">

                                                        <?php
                                                        echo $this->Form->input(
                                                                'charges.price_pcharge_amunit' . $i, array(
                                                            'id' => 'price_pcharge_amunit' . $i,
                                                            'options' => array(
                                                                '1' => 'Per Sq feet',
                                                                '2' => 'Per Unit'
                                                            ),
                                                            'class' => 'selectpicker bs-select-hidden',
                                                            'label' => false,
                                                                )
                                                        );
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <div class="frowedit">
                                                    <a onclick="cloneProjectChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>		


                                    <?php
                                }
                            } else {
                                ?>
                                <div class="row cloneProjectCharge" id="cloneProjectCharge1" >

                                    <div class="col-sm-4">
                                        <div class="form-group">

                                            <label for="property-type" class="label_title">Name of Project Charges </label>

                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'charges.price_pcharge1', array(
                                                    'id' => 'price_pcharge1',
                                                    'options' => array(
                                                        '0' => 'Select',
														'1' => 'One Covered Car Park',
														'2' => 'Double Covered Car Park',
														'3' => 'Club Membership',
														'4' => 'Power BackUp per KVA',
														'5' => 'Interest Free Maintenance Deposit',
														'6' => 'Road Facing PLC ',
														'7' => 'Park Facing PLC',
														'8' => 'Corner PLC',
														'9' => 'Sinking Fund',
														'10' => 'View PLC 1',
														'11' => 'View PLC 2',
														'12' => 'Swimming Pool PLC',
														'14' => 'Hill View PLC',
														'15' => 'Additional Car Parking'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden uniqueProjectCharges',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>		

                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group"> 

                                            <label for="property-price-before" class="label_title">Type of Charge</label>

                                            <div class="select-style">


                                                <?php
                                                echo $this->Form->input(
                                                        'charges.price_pcharge_type1', array(
                                                    'id' => 'price_pcharge_type1',
                                                    'options' => array(
                                                        '1' => 'Mandatory',
                                                        '2' => 'Optional'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                        )
                                                );
                                                ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group"> 

                                            <div class="col-sm-12"> 
                                                <label class="label_title">Amount </label>
                                            </div>

                                            <div class="col-sm-5">
                                                <?php
                                                echo $this->Form->input(
                                                        'charges.price_pcharge_amt1', array(
                                                    'id' => 'price_pcharge_amt1',
                                                    'type' => 'text',
                                                    'class' => 'form-control box_price uniqueProjectChargesAmt',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="select-style">

                                                    <?php
                                                    echo $this->Form->input(
                                                            'charges.price_pcharge_amunit1', array(
                                                        'id' => 'price_pcharge_amunit1',
                                                        'options' => array(
                                                            '1' => 'Per Sq feet',
                                                            '2' => 'Per Unit'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden',
                                                        'label' => false,
                                                            )
                                                    );
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <div class="frowedit">
                                                <a onclick="cloneProjectChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>		

                            <?php } ?>


                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneProjectChargeAddRow" onclick="cloneProjectChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>

                    

                    <div class="add-tab-row push-padding-bottom row-color-gray">

                        <h3>Additional Charges</h3>

                        <input type="hidden" id="id_cloneCorePlcCount" value="15" >
                        <div class="addrowarea morerow" id="id_cloneCorePlc_tab" >
                            <?php
                            if (!empty($this->request->data['addition_charges'])) {
                                for ($i = 1; $i <= count($this->request->data['addition_charges']) / 4; $i++) {
                                    ?>
                                    <div class="row cloneCorePlc" id="cloneCorePlc<?php echo $i; ?>" >

                                        <div class="col-sm-4">
                                            <div class="form-group">

                                                <label for="property-type" class="label_title">Name of Additional Charges </label>

                                                <div class="select-style">

                                                    <?php
                                                    echo $this->Form->input(
                                                            'addition_charges.price_core_plc' . $i, array(
                                                        'id' => 'price_core_plc' . $i,
                                                        'options' => array(
                                                            '0' => 'Select',
                                                            '1' => 'Lease Rent',
                                                            '2' => 'External Electrification Charges',
                                                            '3' => 'External development Charges',
                                                            '4' => 'Infrastructure development Charges',
                                                            '5' => 'Electricity Connection Charges',
                                                            '6' => 'Fire fighting charges',
                                                            '7' => 'Electric Meter Charges',
                                                            '8' => 'Gas Pipeline Charges',
                                                            '9' => 'Sinking Fund',
															'10' => 'Security  & 1 time connection charges',
															'11' => 'Water & sewer connection charges',
															'12' => 'Internal development Charges'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden uniqueCorePlc',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>

                                                </div>		

                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group"> 

                                                <label for="property-price-before" class="label_title">Type of Charge</label>

                                                <div class="select-style">


                                                    <?php
                                                    echo $this->Form->input(
                                                            'addition_charges.price_core_type' . $i, array(
                                                        'id' => 'price_core_type' . $i,
                                                        'options' => array(
                                                            '1' => 'Mandatory',
                                                            '2' => 'Optional'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden',
                                                        'label' => false,
                                                            )
                                                    );
                                                    ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group"> 

                                                <div class="col-sm-12"> 
                                                    <label class="label_title">Amount </label>
                                                </div>

                                                <div class="col-sm-5">
                                                    <?php
                                                    echo $this->Form->input(
                                                            'addition_charges.price_core_amt' . $i, array(
                                                        'id' => 'price_core_amt' . $i,
                                                        'type' => 'text',
                                                        'class' => 'form-control box_price uniqueCorePlcAmt',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                </div>

                                                <div class="col-sm-7">
                                                    <div class="select-style">

                                                        <?php
                                                        echo $this->Form->input(
                                                                'addition_charges.price_core_amunit' . $i, array(
                                                            'id' => 'price_core_amunit' . $i,
                                                            'options' => array(
                                                                '1' => 'Per Sq feet',
                                                                '2' => 'Per Unit'
                                                            ),
                                                            'class' => 'selectpicker bs-select-hidden',
                                                            'label' => false,
                                                                )
                                                        );
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <div class="frowedit">
                                                    <a onclick="cloneCorePlcDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>		


                                    <?php
                                }
                            } else {
                                ?>

                                <div class="row cloneCorePlc" id="cloneCorePlc1" >

                                    <div class="col-sm-4">
                                        <div class="form-group">

                                            <label for="property-type" class="label_title">Name of Additional Charges </label>

                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'addition_charges.price_core_plc1', array(
                                                    'id' => 'price_core_plc1',
                                                    'options' => array(
                                                        '0' => 'Select',
                                                        '1' => 'Lease Rent',
                                                        '2' => 'External Electrification Charges',
                                                        '3' => 'External development Charges',
                                                        '4' => 'Infrastructure development Charges',
                                                        '5' => 'Electricity Connection Charges',
                                                        '6' => 'Fire fighting charges',
                                                        '7' => 'Electric Meter Charges',
                                                        '8' => 'Gas Pipeline Charges',
                                                        '9' => 'Sinking Fund'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden uniqueCorePlc',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>		

                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group"> 

                                            <label for="property-price-before" class="label_title">Type of Charge</label>

                                            <div class="select-style">


                                                <?php
                                                echo $this->Form->input(
                                                        'addition_charges.price_core_type1', array(
                                                    'id' => 'price_core_type1',
                                                    'options' => array(
                                                        '1' => 'Mandatory',
                                                        '2' => 'Optional'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                        )
                                                );
                                                ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group"> 

                                            <div class="col-sm-12"> 
                                                <label class="label_title">Amount </label>
                                            </div>

                                            <div class="col-sm-5">
                                                <?php
                                                echo $this->Form->input(
                                                        'addition_charges.price_core_amt1', array(
                                                    'id' => 'price_core_amt1',
                                                    'type' => 'text',
                                                    'class' => 'form-control box_price uniqueCorePlcAmt',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="select-style">

                                                    <?php
                                                    echo $this->Form->input(
                                                            'addition_charges.price_core_amunit1', array(
                                                        'id' => 'price_core_amunit1',
                                                        'options' => array(
                                                            '1' => 'Per Sq feet',
                                                            '2' => 'Per Unit'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden',
                                                        'label' => false,
                                                            )
                                                    );
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <div class="frowedit">
                                                <a onclick="cloneCorePlcDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>	

                            <?php }
                            ?>




                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneCorePlcAddRow" onclick="cloneCorePlcAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>

                    <div class="add-tab-row push-padding-bottom row-color-gray">

                        <h3>Other Charges</h3>

                        <input type="hidden" id="id_cloneOtherPlcCount" value="15" >
                        <div class="addrowarea morerow" id="id_cloneOtherPlc_tab" >
                            <?php
                            if (!empty($this->request->data['othercharges'])) {
                                for ($i = 1; $i <= count($this->request->data['othercharges']) / 4; $i++) { ?>
                                    <div class="row cloneOtherPlc" id="cloneOtherPlc<?php echo $i; ?>" >

                                        <div class="col-sm-4">
                                            <div class="form-group">

                                                <label for="property-type" class="label_title">Name of Other Charges </label>

                                                <div class="select-style">

                                                    <?php
                                                    echo $this->Form->input(
                                                            'othercharges.price_other_plc' . $i, array(
                                                                'id' => 'price_other_plc' . $i,
                                                                'options' => array(
                                                                    '0' => 'Select',
                                                                    '1' => 'Other Charges1',
                                                                    '2' => 'Other Charges2',
                                                                    '3' => 'Other Charges3',
                                                                    '4' => 'Other Charges4',
                                                                    '5' => 'Other Charges5',
                                                                    '6' => 'Other Charges6',
                                                                    '7' => 'Other Charges7',
                                                                    '8' => 'Other Charges8'
                                                                ),
                                                                'class' => 'selectpicker bs-select-hidden uniqueOtherPlc',
                                                                'label' => false
                                                            )
                                                    );
                                                    ?>

                                                </div>		

                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group"> 

                                                <label for="property-price-before" class="label_title">Type of Charge </label>

                                                <div class="select-style">


                                                    <?php
                                                    echo $this->Form->input(
                                                            'othercharges.price_other_type' . $i, array(
                                                        'id' => 'price_other_type' . $i,
                                                        'options' => array(
                                                            '1' => 'Mandatory',
                                                            '2' => 'Optional'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden',
                                                        'label' => false,
                                                            )
                                                    );
                                                    ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group"> 

                                                <div class="col-sm-12"> 
                                                    <label class="label_title">Amount </label>
                                                </div>

                                                <div class="col-sm-5">
                                                    <?php
                                                    echo $this->Form->input(
                                                            'othercharges.price_other_amt' . $i, array(
                                                        'id' => 'price_other_amt' . $i,
                                                        'type' => 'text',
                                                        'class' => 'form-control box_price uniqueOtherPlcAmt',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                </div>

                                                <div class="col-sm-7">
                                                    <div class="select-style">

                                                        <?php
                                                        echo $this->Form->input(
                                                                'othercharges.price_other_amunit' . $i, array(
                                                            'id' => 'price_other_amunit' . $i,
                                                            'options' => array(
                                                                '1' => 'Per Sq feet',
                                                                '2' => 'Per Unit'
                                                            ),
                                                            'class' => 'selectpicker bs-select-hidden',
                                                            'label' => false,
                                                                )
                                                        );
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <div class="frowedit">
                                                    <a onclick="cloneOtherPlcDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>	




                            <?php } } else { ?>

                                <div class="row cloneOtherPlc" id="cloneOtherPlc1" >

                                    <div class="col-sm-4">
                                        <div class="form-group">

                                            <label for="property-type" class="label_title">Name of Other Charges </label>

                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'othercharges.price_other_plc1', array(
                                                    'id' => 'price_other_plc1',
                                                    'options' => array(
                                                        '0' => 'Select',
                                                        '1' => 'Other Charges1',
                                                        '2' => 'Other Charges2',
                                                        '3' => 'Other Charges3',
                                                        '4' => 'Other Charges4',
                                                        '5' => 'Other Charges5',
                                                        '6' => 'Other Charges6',
                                                        '7' => 'Other Charges7',
                                                        '8' => 'Other Charges8'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden uniqueOtherPlc',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>		

                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group"> 

                                            <label for="property-price-before" class="label_title">Type of Charge </label>

                                            <div class="select-style">


                                                <?php
                                                echo $this->Form->input(
                                                        'othercharges.price_other_type1', array(
                                                    'id' => 'price_other_type1',
                                                    'options' => array(
                                                        '1' => 'Mandatory',
                                                        '2' => 'Optional'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                        )
                                                );
                                                ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group"> 

                                            <div class="col-sm-12"> 
                                                <label class="label_title">Amount </label>
                                            </div>

                                            <div class="col-sm-5">
                                                <?php
                                                echo $this->Form->input(
                                                        'othercharges.price_other_amt1', array(
                                                    'id' => 'price_other_amt1',
                                                    'type' => 'text',
                                                    'class' => 'form-control box_price uniqueOtherPlcAmt',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="select-style">

                                                    <?php
                                                    echo $this->Form->input(
                                                            'othercharges.price_other_amunit1', array(
                                                        'id' => 'price_other_amunit1',
                                                        'options' => array(
                                                            '1' => 'Per Sq feet',
                                                            '2' => 'Per Unit'
                                                        ),
                                                        'class' => 'selectpicker bs-select-hidden',
                                                        'label' => false,
                                                            )
                                                    );
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <div class="frowedit">
                                                <a onclick="cloneOtherPlcDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneOtherPlcAddRow" onclick="cloneOtherPlcAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>

                    <div class="add-tab-row push-padding-bottom ">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group" style="overflow:hidden;">
                                    <label for="price_sp_offer" class="label_title">Special Offers</label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Any special offer for certain time period."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_sp_offer', array(
                                            'id' => 'price_sp_offer',
                                            'type' => 'textarea',
                                            'class' => 'form-control',
                                            'label' => false
                                        )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="price_remarks" class="label_title">Remarks</label>
                                    <?php
                                    echo $this->Form->input(
                                            'price_remarks', array(
                                        'id' => 'price_remarks',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>

                            </div>

                        </div>

                        	

                    </div>



                    <div class="account-block text-center submit_area">
                        <div id='loadingmessage4' style='display:none'>
                            <img src='../../img/ajax-loader.gif'/>
                        </div>
                        <button type="submit" id="id_next4" class="btn btn-primary btn-next ">SAVE</button>
                    </div>							

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array(
                'class' => 'project_id',
                'type' => 'hidden',
                'label' => false,
                'value' => $this->request->data['ProjectPricing']['project_id']
                    )
            );
			
			echo $this->Form->input('payment_plan1', array(
                'class' => 'payment_plan1',
                'type' => 'hidden',
                'label' => false,
                'value' => $this->request->data['ProjectPricing']['payment_plan1']
                    )
            );
			
            ?>
            <?php
            echo $this->Form->input('id', array('class' => 'pricing_id',
                'type' => 'hidden',
                'label' => false,
                'value' => $this->request->data['ProjectPricing']['id']
                    )
            );
            ?>	
				
			<?php
				//echo $this->Form->input('payment_plan1', array('class' => 'payment_plan1',
                //'type' => 'hidden','label' => false));
            ?>	

            <?php
            echo $this->Form->end();
            ?>
            
            
        </div>		
    </div> 
<?php
echo $this->Html->script('add-project-pricing');
echo $this->Html->script('add_property_7');
//echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');

$project_base_url = '/Projects/addProjectPricing';
//$project_detail_url = '/Projects/pastprojectdetail';
//$project_bank_url = '/Projects/projectbankdetail';
$project_pricing_url = '/Projects/addProjectPricingPlanEdit';
//$project_office_url = '/Projects/projectofficeuse';





// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("		

		$.validator.addMethod('box', function(value, element) 
		{
			return ( 
						( $('#price_payplan1').is(':checked')) ||
						( $('#price_payplan2').is(':checked')) ||
						( $('#price_payplan3').is(':checked')) ||
						( $('#price_payplan4').is(':checked')) 						
			)
		}, 'Please choose a payment plan.');
	
		$.validator.setDefaults({ ignore: '' });
	
		$('#price_payplan1').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});

		$('#price_payplan2').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});

		$('#price_payplan3').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});

		$('#price_payplan4').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});	
	
		$.validator.addMethod('nameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z.\s]+$/i.test(value);
		}, 'Please use English alphabets only.');
	
		$.validator.addMethod('onameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z0-9.\s]+$/i.test(value);
		}, 'Please use Alphanumeric characters only.');	
	
		$.validator.addMethod('dateCustom', function(value, element) 
		{
			return Date.parse(value);
			
		}, 'Date format not correct.');
		
		$.validator.addMethod('enddate', function(value, element) 
		{
			var startdatevalue = $('.startdate').val();
			
			if ( startdatevalue != '' )
				return Date.parse(startdatevalue) < Date.parse(value);
			else
				return true;
			
		}, 'Completion Date should be greater than Launch Date.');

		$.validator.addMethod('startdate', function(value, element) 
		{
			var enddatevalue = $('.enddate').val();
			
			if ( enddatevalue != '' )
				return Date.parse(enddatevalue) > Date.parse(value);
			else
				return true;
			
		}, 'Launch Date should be Less than Completion Date.');
		
		$.validator.addMethod('lessThanEqTo',function (value, element, param) 
		{
				var otherElement = $(param);
				if ( otherElement.val() != '' )
					return parseInt(value, 10) <= parseInt(otherElement.val(), 10);
				else
					return true;
		}, 'Launch Units should be less than or equal to Total Units.');
		
		$.validator.addMethod('greaterThanEqTo',function (value, element, param) 
		{
		
				var otherElement = $(param);
				if ( value != '' && otherElement.val() != '')
					return parseInt(value, 10) >= parseInt(otherElement.val(), 10);
				else
					return true;
		}, 'Total Units should be greater than or equal to Launch Units.');

                $.validator.addMethod('curl', function(value, element) 
		{
			return this.optional(element) || /^(?:(http|https)?:\/\/)?(?:[\w-]+\.)+([a-z]|[A-Z]|[0-9]){2,6}$/gi.test(value);
		}, 'URL format not correct.');
		
		
		$('input[id=\'dtp1\']').change( function() {
			$(this).valid();

                        //if ( $('.enddate').val() != '' )  
			//  $('.enddate').valid();
                        if ( document.getElementById(dtp2) )
			    $('.enddate').valid();  
		});
		
		$('input[id=\'dtp2\']').change( function() {
			$(this).valid();
			$('.startdate').valid();
		});

/*
		
		$('input[id=\'total_units\']').change( function() {
			$(this).valid();
			$('#launch_units').valid();
		});
		
		$('input[id=\'launch_units\']').change( function() {
			$(this).valid();
		});
		
		
		$('.allownumericwithdecimal').on('keypress keyup blur',function (event) 
		{
            //this.value = this.value.replace(/[^0-9\.]/g,'');
			
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
			{
                event.preventDefault();
            }
        });

		$('.allownumericwithoutdecimal').on('keypress keyup blur',function (event) 
		{    
			$(this).val($(this).val().replace(/[^\d].+/, ''));
            
			if ((event.which < 48 || event.which > 57)) 
			{
                event.preventDefault();
            }
		});	

*/		
		var ruleMSetPricing = {
					required : true,
					range    :[0.00,999999999]
					
		};
		
		var messageMSetPricing = {
					required : 'Field is required.',
					range  : 'Allowed : 0 - 999999999.',
		};
		
		var ruleOSetPricing = {
					range    :[0.00,999999999]
		};
		
		var messageOSetPricing = {
					range  : 'Allowed : 0 - 999999999.'
		};
		
		var rulesMEmail = {
					required : true,
					email   : true
		};
		
		var messageMEmail = {
					required : 'Email is required.',
					email   : 'Email format is not correct.'
		};
		
		var rulesMName = {
					required : true,
					onameCustom   : true
		};
		
		var messageMName = {
					required : 'Name is required.',
					onameCustom   : 'Only Alphanumeric characters allowed.'
		};
		
		var rulesOName = {
					onameCustom   : true
		};
		
		var messageOName = {
					onameCustom   : 'Only Alphanumeric characters allowed.'
		};
		
		var rulesMMobile = {
					required : true,
					number   : true,
					range    : [1000000000,9999999999]  
		};
		
		var messageMMobile = {
					required : 'Mobile is required.',
					number   : 'Only numbers allowed.',
					range    : 'Only 10 digits allowed'
		};
		
		var ruleMDiscount = {
					required : true,
					number   : true,
					range: [0,100]
		};
		
		var messageMDiscount = {
					required : 'Percentage(%) is required.',
					number   : 'Only numbers allowed.',
					range	 : 'Not a valid Percentage(%)'
		};
		
		var ruleMFile = {
					required : true,
				
		};
		
		var messageMFile = {	
					required : 'File name is required.',
					extension: 'Accepted formats (png, jpeg, gif).' 
		}
	

		
		$('#id_form_project_pricing').validate({
			// Specify the validation rules
			rules: {
				'data[ProjectPricing][price_srtax_bsp]': ruleMDiscount,
				'data[ProjectPricing][price_srtax_oth]': ruleMDiscount,
				'data[ProjectPricing][price_srtax_icr]': ruleMDiscount,
				'data[ProjectPricing][price_registration]': ruleMSetPricing,
				'data[Project][price_rf_plc]': ruleMSetPricing,
				'data[Project][price_pf_plc]': ruleMSetPricing,
				'data[Project][price_cr_plc]': ruleMSetPricing,
				'data[addition_charges][price_core_amt1]': ruleOSetPricing,
				'data[charges][price_pcharge_amt1]': ruleOSetPricing,
				'data[othercharges][price_other_amt1]': ruleOSetPricing,
				'data[ProjectPricing][floor_plc_amount]': ruleOSetPricing,
				'data[ProjectPricing][floor_per_plc_amount]': ruleOSetPricing,
				/*'data[ProjectPricing][price_sp_offer]': {
					required : true
				},*/
				/*'data[ProjectPricing][price_remarks]': {
					required : true
				},*/
				'data[ProjectPricing][pp_error]': {
					box : true
				}
			},
			
			// Specify the validation error messages
			messages: {
				'data[ProjectPricing][price_srtax_bsp]': messageMDiscount,
				'data[ProjectPricing][price_srtax_oth]': messageMDiscount,
				'data[ProjectPricing][price_srtax_icr]': messageMDiscount,
				'data[ProjectPricing][price_registration]': messageMSetPricing,
				'data[Project][price_rf_plc]': messageMSetPricing,
				'data[Project][price_pf_plc]': messageMSetPricing,
				'data[Project][price_cr_plc]': messageMSetPricing,
				'data[addition_charges][price_core_amt1]': messageOSetPricing,
				'data[charges][price_pcharge_amt1]': messageOSetPricing,
				'data[othercharges][price_other_amt1]': messageOSetPricing,
				'data[ProjectPricing][floor_plc_amount]': messageOSetPricing,
				'data[ProjectPricing][floor_per_plc_amount]': messageOSetPricing,
				/*'data[ProjectPricing][price_sp_offer]': {
					required : 'Field is required'
				},*/
				/*'data[ProjectPricing][price_remarks]': {
					required : 'Field is required'
				}*/
			},
			
			submitHandler: function(form){
				
				// Check Form Error
				if ( true == checkPriceErrors() )
				{	
					//Found Errors. Donot Proceed further.
					return false;	
				}
				
				// Validations OK. Form can be submitted
				
				// TODO : Ajax call
				$('#loadingmessage4').show();
				
				// Changes of Page when Ajax call is successful
				
				
				$.ajax
			({
				type: 'POST',
				url: '$project_pricing_url',
				data: $('#id_form_project_pricing').serialize(), // serializes the form's elements.
				complete: function(status)
				{
                                    $('#loadingmessage4').hide();
                                },
				success: function(data)
				{
				 	// TODO : Ajax call
				
				// 1. Change NEXT -> SAVED
				$('#id_next4').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next4').css('background','#ffc000');
				//$('#id_next4').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_pricing :input').prop('disabled', true);
				//$('#id_cloneCorePlcAddRow').attr('disabled','disabled');
				//$('#id_cloneProjectChargeAddRow').attr('disabled','disabled');
				//$('#id_cloneOtherPlcAddRow').attr('disabled','disabled');
				
				
				// 4. Expand the next block
				$('#id_proj_office_exp').css('display','block');
				$('#id_proj_office_exp').addClass('active');
				$('#id_proj_office_content').css('display','block');
				
                                // 5. Close current block
				//$('#id_proj_pricing_exp').removeClass('active');
				//$('#id_proj_pricing_content').css('display','none');
				//$('#id_top').attr('tabindex',-10).focus();			
			
				 //window.location.href = 'https://www.fairpockets.com/projects/projectPricingLists/'; 
				}
		});
		
				
				
				
				// To remain on same page after submit
				return false;
				
			}
		});
		
		
		$('#id_form_project_basic').validate({
		
			// Specify the validation rules
			rules: {
				'data[Project][project_name]': {
					required : true,
					onameCustom : true
				},
				'data[Project][project_type]': {
					required : true
				},
				'data[Project][project_highlights]': {
					required : true
				},
				'data[Project][project_description]': {
					required : true
				},
				/*'data[Project][project_address]': {
					required : true,
					map_check : true
				},*/
				'data[Project][dtp1]': {
					required : true,
					dateCustom :  true
				},
				'data[Project][dtp2]': {
					required : true,
					date :  true
				}
				
			},
		
			// Specify the validation error messages
			messages: {
				'data[Project][project_name]': {
					required : 'Project Name is required.'
				},
				'data[Project][project_type]': {
					required : 'Project Type is mandatory.'
				},
				'data[Project][project_highlights]': {
					required : 'Project Highlights is required.'
				},
				'data[Project][project_description]': {
					required : 'Project Description is required.'
				},
				/*'data[Project][project_address]': {
					required : 'Project address is required.'
				},*/
				'data[Project][dtp1]': {
					required : 'Project Launch Date is required.',
					date : 'Date format not correct.'
				},
				'data[Project][dtp2]': {
					required : 'Project Completion Date is required.',
					date : 'Date format not correct.'
				}
			},
			
			submitHandler: function(form){
				
				// Validations OK. Form can be submitted
				$('#loadingmessage1').show();
				
					$.ajax
			({
				type: 'POST',
				url: '$project_base_url',
				data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
				complete: function(status)
				{
                                    $('#loadingmessage1').hide();
                                },
				success: function(data)
				{
					var selprojectval = $('#project_id').val(); 
					 $('.project_id').val(selprojectval);
					 
					 var selprojectpplanval = $('#payment_plan1').val(); 
					 $('.payment_plan1').val(selprojectpplanval);
				 	// TODO : Ajax call
				
				// Changes of Page when Ajax call is successful
				
				// 1. Change NEXT -> SAVED
				$('#id_next1').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next1').css('background','#ffc000');
				//$('#id_next1').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_basic :input').prop('disabled', true);
				
				// 4. Expand the next block
				$('#id_proj_pricing_exp').css('display','block');
				$('#id_proj_pricing_exp').addClass('active');
				$('#id_proj_pricing_content').css('display','block');
				//$('#id_next2').css('display','inline');
			
                                // 5. Close current block
				/*$('#id_proj_basic_exp').removeClass('active');
				$('#id_proj_basic_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();*/
				  
				}
		});
		
				
			
				// To remain on same page after submit
				return false;
			}

		});

		
		
	");
?>



