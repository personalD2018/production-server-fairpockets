<div class="page-content">
        <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Post Your Project Pricing</h2>
            <br />
        </div>	

        <div class="add_property_form">

            <?php
            echo $this->Form->create(
                    'Project', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_basic',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block">

                <div class="add-title-tab">
                    <h3>Project Basic </h3>
                    <div class="add-expand active" id="id_proj_basic_exp"></div>
                </div>

                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display:block;">
                   
                        <div class="row">

                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Project Name<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php //echo $this->Form->input('property_type_id', array('id' => 'property_type_val', 'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder Floor', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'), 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>
											<?php echo $this->Form->select('Project', $allproject,array('id'=>'project_id')); ?>
                                        </div>
                                        <div class="error has-error" id="property_type_val_error" style="display:block;"></div>
                                    </div>					
									
                                </div>

							<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title" style="padding-bottom:22px;">Payment Plan Name<span class="mand_field">*</span></label>
                                        <br>
                                            <?php echo $this->Form->input('projectplan', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Project Plan', 'class' => '')); ?>
                                        
                                        
                                        <div class="error has-error" id="property_type_val_error" style="display:block;"></div>
                                    </div>					
									
                                </div>
							
							
                            
                        </div>


                        <div class="row">
							<div class="col-md-12">
                            <input id="id_search_adm_area_1" class="form-controls" type="hidden" >
                            <input id="id_search_adm_area_2" class="form-controls" type="hidden" >

<!-- <input id="searchInput" class="input-controls" type="hidden" > -->
                            <!-- <div class="map" id="map" style="width: 100%; height: 300px;"></div> -->

                            <div class="form_area">

                                <?php echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
                                <!-- Latitude -->
                                <?php echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>
                                <?php echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

                            </div>
                            </div>
                        </div>
                        <?php
                        echo $this->Form->input('current_project', array(
                            'type' => 'hidden',
                            'label' => false,
                            'value' => '1'
                                )
                        );
                        ?>
                        <div class="account-block text-center submit_area">
                            <div id='loadingmessage1' style='display:none'>
                                <img src='../img/ajax-loader.gif'/>
                            </div>
                            <button type="submit" id="id_next1" class="btn btn-primary btn-next ">NEXT</button>
                        </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>

           
            <?php
            echo $this->Form->create(
                    'ProjectPricing', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_pricing',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block project_pricing">

                <div class="add-title-tab">
                    <h3>Project Pricing</h3>
                    <div class="add-expand" id="id_proj_pricing_exp"></div>
                </div>

                <div class="add-tab-content detail-block" id="id_proj_pricing_content" >
					
					
					<div class="add-tab-row push-padding-bottom">	

                        <h3>Basic Selling Price</h3>
                        <div class="addrowarea morerow" id="id_ProjectChargeFloorPLC_tab"  style="display:none;">

                            <div class="row" >

                                <div class="col-sm-4">
                                    <div class="form-group"> 

                                        <label for="floor_plc_amount" class="label_title">Basic Selling Price ( BSP )/ Sq.ft</label>
                                        <?php
                                        echo $this->Form->input(
                                                'price_bsp_copy', array(
                                            'id' => 'price_bsp_copy',
                                            'type' => 'text',
                                            'class' => 'form-control box_price',
                                            'label' => false,
                                            'value' => ''
                                                )
                                        );
                                        ?>


                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">

                                        <label for="floor_option" class="label_title">BSP for floor </label>

                                        <div class="select-style">

                                            <?php
                                            echo $this->Form->input(
                                                    'floor_option', array(
                                                'id' => 'floor_option',
                                                'options' => array(
                                                    '1' => 'Top Floor',
                                                    '2' => 'Ground Floor'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>		

                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group"> 

                                        <div class="col-sm-12">
                                            <label for="floor_per_plc_amount" class="label_title">Per Floor Charge/ Sq.ft</label>
                                        </div>

                                        <div class="col-sm-6">
                                            <?php
                                            echo $this->Form->input(
                                                    'floor_per_plc_amount', array(
                                                'id' => 'floor_per_plc_amount',
                                                'type' => 'text',
                                                'class' => 'form-control box_price',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>


                                        <div class="col-sm-6">

                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'floor_per_option', array(
                                                    'id' => 'floor_per_option',
                                                    'options' => array(
                                                        '1' => 'Add per floor',
                                                        '2' => 'Subtract per floor'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false
                                                        )
                                                );
                                                ?>

                                            </div>		

                                        </div>

										




                                    </div>		

                                </div>

                            </div>
							
                        </div>
						<!--<label for="" generated="true" class="error bsperr" style="display:none;">Please choose any one option for BSP.</label>

						
                        <h3><center>OR</center></h3>-->
						<input type="hidden" id="id_cloneProjectBspChargeCount" value="40" >
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab" >
							<div class="row cloneProjectBspCharge" id="cloneProjectBspCharge1" >	
								<div class="col-sm-4">
                                    <div class="form-group">

                                         <label for="price_bsp1" class="label_title">Basic Selling Price ( BSP )/ Sq.ft</label>
                                        <?php
                                        echo $this->Form->input(
                                                'bspcharges.price_bsp1', array(
                                            'id' => 'price_bsp1',
                                            'type' => 'text',
                                            'class' => 'form-control box_price',
                                            'label' => false,
                                            'value' => ''
                                                )
                                        );
                                        ?>

                                    </div>
                                </div>
								
								<div class="col-sm-3">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">From Floor</label>

                                        <div class="select-style">


                                            <?php

                                            $bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
                                            $bspFromToOptions[0]='Ground';

                                            echo $this->Form->input(
                                                    'bspcharges.from_floor1', array(
                                                'id' => 'from_floor1',
                                                'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'empty' => 'From'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>
							
								<div class="col-sm-3">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">To Floor</label>

                                        <div class="select-style">


                                            <?php
                                            echo $this->Form->input(
                                                    'bspcharges.to_floor1', array(
                                                'id' => 'to_floor1',
                                                'options' => $bspFromToOptions,
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'empty' => 'To'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectBspChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
								
							</div>
												
							
						</div>
						<div class="row" style="padding-bottom:14px;">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRow" onclick="cloneProjectBspChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
                    </div>
					
					<div class="add-tab-row push-padding-bottom row-color-gray">
                        <h3>Basic PLC Price</h3>                        
						<div class="addrowarea morerow" id="id_cloneProjectBspCharge_tab_plc" >
						
                                
								<div class="row cloneProjectBspChargePlc" id="cloneProjectBspChargePlc1" >
										<div class="col-sm-3">
											<div class="form-group">		
												 <label for="price_bsp1" class="label_title">Amount Per Floor</label>
												<?php
												echo $this->Form->input(
														'bspchargesplc.floor_plc_amount1', array(
													'id' => 'floor_plc_amount1',
													'type' => 'text',
													'class' => 'form-control box_price',
													'label' => false
														)
												);
												?>
		
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group">
												<label for="property-price-before" class="label_title">Per floor Mode</label>
												<div class="select-style">
													<?php
														echo $this->Form->input(
																'bspchargesplc.per_floor_mode1', array(
															'id' => 'per_floor_mode1',
															'options' => array(
																'1' => 'Equal per floor',
																'2' => 'Add per floor',																
																'3' => 'Subtract per floor'
															),
															'class' => 'selectpicker bs-select-hidden',
															'label' => false
																)
														);
													?>
												</div>
											</div>
										</div>
										
																				
										<div class="col-sm-2">
											<div class="form-group">
												<label for="property-price-before" class="label_title">From Floor</label>
												<div class="select-style">
													<?php
													$bspFromToOptions=array_combine(range(0,75,1),range(0,75,1));
													$bspFromToOptions[0]='Ground';
													echo $this->Form->input(
															'bspchargesplc.plc_from_floor1', array(
														'id' => 'plc_from_floor1',
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>
												</div>
											</div>
										</div>
								
										<div class="col-sm-2">
											<div class="form-group"> 

												<label for="property-price-before" class="label_title">To Floor</label>

												<div class="select-style">


													<?php
													echo $this->Form->input(
															'bspchargesplc.plc_to_floor1', array(
																	'id' => 'plc_to_floor1',
														'options' => $bspFromToOptions,
														'class' => 'selectpicker bs-select-hidden',
														'label' => false
															)
													);
													?>

												</div>
											</div>
										</div>
                                      
                                     
                                    
								
								

                                        <div class="col-sm-1">
											<div class="form-group">
												<div class="frowedit">
													<a onclick="cloneProjectBspChargeDelRowPlc(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>

                                    </div>
							</div>

                        <div class="row">
                            <div class="col-sm-2 pull-right">
                                <a id="id_cloneProjectBspChargeAddRowPlc" onclick="cloneProjectBspChargeAddRowPlc();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
						
							

                    </div>
					
					
										
                    <div class="add-tab-row push-padding-bottom" >

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">GST on BSP ( in % ) <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="GST component on Basic Price."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_srtax_bsp', array(
                                        'id' => 'price_srtax_bsp',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">GST on Others ( in % ) <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="GST component on Additional Charges."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_srtax_oth', array(
                                        'id' => 'price_srtax_oth',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
							
							<div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">GST Input Credit ( in % ) <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="GST component on Additional Charges."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_srtax_icr', array(
                                        'id' => 'price_srtax_icr',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>


                        </div>

                        <div class="row">

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <div class="row">

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Registration Charges<span class="mand_field">*</span></label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Registration charges paid to government."></i>
                                        </div>

                                        <div class="col-sm-8">
                                            <?php
                                            echo $this->Form->input(
                                                    'price_registration', array(
                                                'id' => 'price_registration',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-4">

                                            <div class="select-style">
                                                <?php
                                                echo $this->Form->input(
                                                        'price_reg_unit', array(
                                                    'id' => 'price_reg_unit',
                                                    'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                        </div>	

                                    </div>	

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <div class="row">

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Stamp Duty Charges</label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Stamp Duty charges paid to government."></i>
                                        </div>

                                        <div class="col-sm-8">
                                            <?php
                                            echo $this->Form->input(
                                                    'price_stamp', array(
                                                'id' => 'price_stamp',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>

                                        <div class="col-sm-4" >

                                            <div class="select-style" >
                                                <?php
                                                echo $this->Form->input(
                                                        'price_stm_unit', array(
                                                    'id' => 'price_stm_unit',
                                                    'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                        </div>


                                    </div>


                                </div>

                            </div>


                        </div>

                    </div>	

                    <div class="add-tab-row push-padding-bottom row-color-gray">

                        <h3>Project Charges</h3>

                        <input type="hidden" id="id_cloneProjectChargeCount" value="25" >
                        <div class="addrowarea morerow" id="id_cloneProjectCharge_tab" >

                            <div class="row cloneProjectCharge" id="cloneProjectCharge1" >

                                <div class="col-sm-4">
                                    <div class="form-group">

                                        <label for="property-type" class="label_title">Name of Project Charges </label>

                                        <div class="select-style">

                                            <?php
                                            echo $this->Form->input(
                                                    'charges.price_pcharge1', array(
                                                'id' => 'price_pcharge1',
                                                'options' => array(
                                                    '0' => 'Select',
													'1' => 'One Covered Car Park',
													'2' => 'Double Covered Car Park',
													'3' => 'Club Membership',
													'4' => 'Total Power Backup Cost',
													'13' => 'Power BackUp per KVA',
													'5' => 'Interest Free Maintenance Deposit',
													'6' => 'Road Facing PLC ',
													'7' => 'Park Facing PLC',
													'8' => 'Corner PLC',
													'9' => 'Sinking Fund',
													'10' => 'View PLC 1',
													'11' => 'View PLC 2',
													'12' => 'Swimming Pool PLC',
													'14' => 'Hill View PLC',
													'15' => 'Additional Car Parking'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden uniqueProjectCharges',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>		

                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">Type of Charge</label>

                                        <div class="select-style">


                                            <?php
                                            echo $this->Form->input(
                                                    'charges.price_pcharge_type1', array(
                                                'id' => 'price_pcharge_type1',
                                                'options' => array(
                                                    '1' => 'Mandatory',
                                                    '2' => 'Optional'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'selected' => '1'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4" style="padding-left:2px !important; padding-right:2px !important;">
                                    <div class="form-group"> 

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Amount </label>
                                        </div>

                                        <div class="col-sm-5" style="padding-left:2px !important; padding-right:2px !important;">
                                            <?php
                                            echo $this->Form->input(
                                                    'charges.price_pcharge_amt1', array(
                                                'id' => 'price_pcharge_amt1',
                                                'type' => 'text',
                                                'class' => 'form-control box_price uniqueProjectChargesAmt',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-7">
                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'charges.price_pcharge_amunit1', array(
                                                    'id' => 'price_pcharge_amunit1',
                                                    'options' => array(
                                                        '1' => 'Per Sq feet',
                                                        '2' => 'Per Unit'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                    'selected' => '1'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneProjectChargeDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>		

                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneProjectChargeAddRow" onclick="cloneProjectChargeAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>

                    

                    <div class="add-tab-row push-padding-bottom row-color-gray">

                        <h3>Additional Charges</h3>

                        <input type="hidden" id="id_cloneCorePlcCount" value="15" >
                        <div class="addrowarea morerow" id="id_cloneCorePlc_tab" >

                            <div class="row cloneCorePlc" id="cloneCorePlc1" >

                                <div class="col-sm-4" style="width:41% !important;">
                                    <div class="form-group">

                                        <label for="property-type" class="label_title">Name of Additional Charges </label>

                                        <div class="select-style">

                                            <?php
                                            echo $this->Form->input(
                                                    'addition_charges.price_core_plc1', array(
                                                'id' => 'price_core_plc1',
                                                'options' => array(
                                                    '0' => 'Select',
                                                    '1' => 'Lease Rent',
                                                    '2' => 'External Electrification Charges',
                                                    '3' => 'External development Charges',
                                                    '4' => 'Infrastructure development Charges',
                                                    '5' => 'Electricity Connection Charges',
                                                    '6' => 'Fire fighting charges',
                                                    '7' => 'Electric Meter Charges',
                                                    '8' => 'Gas Pipeline Charges',
                                                    '9' => 'Sinking Fund',
													'10' => 'Security  & 1 time connection charges',
													'11' => 'Water & sewer connection charges',
													'12' => 'Internal development Charges'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden uniqueCorePlc',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>		

                                    </div>
                                </div>

                                <div class="col-sm-3" style="width:20% !important;">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">Type of Charge</label>

                                        <div class="select-style">


                                            <?php
                                            echo $this->Form->input(
                                                    'addition_charges.price_core_type1', array(
                                                'id' => 'price_core_type1',
                                                'options' => array(
                                                    '1' => 'Mandatory',
                                                    '2' => 'Optional'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'selected' => '1'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4" style="width:30% !important; padding-left:2px !important; padding-right:2px !important;">
                                    <div class="form-group"> 

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Amount </label>
                                        </div>

                                        <div class="col-sm-5" style="padding-left:2px !important; padding-right:2px !important;">
                                            <?php
                                            echo $this->Form->input(
                                                    'addition_charges.price_core_amt1', array(
                                                'id' => 'price_core_amt1',
                                                'type' => 'text',
                                                'class' => 'form-control box_price uniqueCorePlcAmt',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-7">
                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'addition_charges.price_core_amunit1', array(
                                                    'id' => 'price_core_amunit1',
                                                    'options' => array(
                                                        '1' => 'Per Sq feet',
                                                        '2' => 'Per Unit'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                    'selected' => '1'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneCorePlcDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>		

                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneCorePlcAddRow" onclick="cloneCorePlcAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>

                    <div class="add-tab-row push-padding-bottom row-color-gray">

                        <h3>Other Charges</h3>

                        <input type="hidden" id="id_cloneOtherPlcCount" value="15" >
                        <div class="addrowarea morerow" id="id_cloneOtherPlc_tab" >

                            <div class="row cloneOtherPlc" id="cloneOtherPlc1" >

                                <div class="col-sm-4">
                                    <div class="form-group">

                                        <label for="property-type" class="label_title">Name of Other Charges </label>

                                        <div class="select-style">

                                            <?php
                                            echo $this->Form->input(
                                                    'othercharges.price_other_plc1', array(
                                                'id' => 'price_other_plc1',
                                                'options' => array(
                                                    '0' => 'Select',
                                                    '1' => 'Other Charges1',
                                                    '2' => 'Other Charges2',
                                                    '3' => 'Other Charges3',
                                                    '4' => 'Other Charges4',
                                                    '5' => 'Other Charges5',
                                                    '6' => 'Other Charges6',
                                                    '7' => 'Other Charges7',
                                                    '8' => 'Other Charges8'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden uniqueOtherPlc',
                                                'label' => false
                                                    )
                                            );
                                            ?>

                                        </div>		

                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group"> 

                                        <label for="property-price-before" class="label_title">Type of Charge </label>

                                        <div class="select-style">


                                            <?php
                                            echo $this->Form->input(
                                                    'othercharges.price_other_type1', array(
                                                'id' => 'price_other_type1',
                                                'options' => array(
                                                    '1' => 'Mandatory',
                                                    '2' => 'Optional'
                                                ),
                                                'class' => 'selectpicker bs-select-hidden',
                                                'label' => false,
                                                'selected' => '1'
                                                    )
                                            );
                                            ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4" style="padding-left:2px !important; padding-right:2px !important;">
                                    <div class="form-group"> 

                                        <div class="col-sm-12"> 
                                            <label class="label_title">Amount </label>
                                        </div>

                                        <div class="col-sm-5" style="padding-left:2px !important; padding-right:2px !important;">
                                            <?php
                                            echo $this->Form->input(
                                                    'othercharges.price_other_amt1', array(
                                                'id' => 'price_other_amt1',
                                                'type' => 'text',
                                                'class' => 'form-control box_price uniqueOtherPlcAmt',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-7">
                                            <div class="select-style">

                                                <?php
                                                echo $this->Form->input(
                                                        'othercharges.price_other_amunit1', array(
                                                    'id' => 'price_other_amunit1',
                                                    'options' => array(
                                                        '1' => 'Per Sq feet',
                                                        '2' => 'Per Unit'
                                                    ),
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                    'selected' => '1'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <div class="frowedit">
                                            <a onclick="cloneOtherPlcDelRow(this);" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>		

                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneOtherPlcAddRow" onclick="cloneOtherPlcAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>

                    <div class="add-tab-row push-padding-bottom ">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group" style="overflow:hidden;">
                                    <label for="price_sp_offer" class="label_title">Special Offers <!--<span class="mand_field">*</span>--></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Any special offer for certain time period."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'price_sp_offer', array(
                                        'id' => 'price_sp_offer',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="price_remarks" class="label_title">Remarks</label>
                                    <?php
                                    echo $this->Form->input(
                                            'price_remarks', array(
                                        'id' => 'price_remarks',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-sm-12">

                                <div class="col-sm-12">
                                    <label class="label_title">Payment Plan</label>
                                </div>

                                <?php
                                echo $this->Form->input('plan.price_payplan1', array(
                                    'id' => 'price_payplan1',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-4'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => 'Construction Linked Plan'),
                                        )
                                );
                                echo $this->Form->input('plan.price_payplan2', array(
                                    'id' => 'price_payplan2',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-4'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => 'Flexi Payment Plan'),
                                        )
                                );

                                echo $this->Form->input('plan.price_payplan3', array(
                                    'id' => 'price_payplan3',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-4'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => 'Down Payment Plan'),
                                        )
                                );

                                echo $this->Form->input('plan.price_payplan4', array(
                                    'id' => 'price_payplan4',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-4'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => 'Subvention Plan'),
                                        )
                                );
								echo $this->Form->input('plan.price_payplan5', array(
                                    'id' => 'price_payplan5',
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-4'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => 'Special Plan'),
                                        )
                                );
                                ?>
                            </div>	
                            <div class="form-group col-sm-12">

                                <div class="col-sm-12">
                                    <!-- This is to show payment plan error -->
                                    <input name="data[ProjectPricing][pp_error]" id="id_pp_error" type="hidden" >
                                </div>
                            </div>		

                        </div>	

                    </div>



                    <div class="account-block text-center submit_area">
                        <div id='loadingmessage4' style='display:none'>
                            <img src='../img/ajax-loader.gif'/>
                        </div>
                        <button type="submit" id="id_next4" class="btn btn-primary btn-next ">NEXT</button>
                    </div>							

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>
            
            
        </div>		
    </div> 
<?php
echo $this->Html->script('add-project');
echo $this->Html->script('add_property_7');
//echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('fp-loc8.js');

$project_base_url = '/Projects/addProjectPricing';
//$project_detail_url = '/Projects/pastprojectdetail';
//$project_bank_url = '/Projects/projectbankdetail';
$project_pricing_url = '/Projects/projectpricing1';
//$project_office_url = '/Projects/projectofficeuse';





// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("		

		$.validator.addMethod('box', function(value, element) 
		{
			return ( 
						( $('#price_payplan1').is(':checked')) ||
						( $('#price_payplan2').is(':checked')) ||
						( $('#price_payplan3').is(':checked')) ||
						( $('#price_payplan4').is(':checked')) 						
			)
		}, 'Please choose a payment plan.');
	
		$.validator.setDefaults({ ignore: '' });
	
		$('#price_payplan1').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});

		$('#price_payplan2').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});

		$('#price_payplan3').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});

		$('#price_payplan4').click(function() {
			var validator = $( '#id_form_project_pricing' ).validate();
			validator.element( '#id_pp_error' );
		
    		});	
	
		$.validator.addMethod('nameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z.\s]+$/i.test(value);
		}, 'Please use English alphabets only.');
	
		$.validator.addMethod('onameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z0-9.\s]+$/i.test(value);
		}, 'Please use Alphanumeric characters only.');	
	
		$.validator.addMethod('dateCustom', function(value, element) 
		{
			return Date.parse(value);
			
		}, 'Date format not correct.');
		
		$.validator.addMethod('enddate', function(value, element) 
		{
			var startdatevalue = $('.startdate').val();
			
			if ( startdatevalue != '' )
				return Date.parse(startdatevalue) < Date.parse(value);
			else
				return true;
			
		}, 'Completion Date should be greater than Launch Date.');

		$.validator.addMethod('startdate', function(value, element) 
		{
			var enddatevalue = $('.enddate').val();
			
			if ( enddatevalue != '' )
				return Date.parse(enddatevalue) > Date.parse(value);
			else
				return true;
			
		}, 'Launch Date should be Less than Completion Date.');
		
		$.validator.addMethod('lessThanEqTo',function (value, element, param) 
		{
				var otherElement = $(param);
				if ( otherElement.val() != '' )
					return parseInt(value, 10) <= parseInt(otherElement.val(), 10);
				else
					return true;
		}, 'Launch Units should be less than or equal to Total Units.');
		
		$.validator.addMethod('greaterThanEqTo',function (value, element, param) 
		{
		
				var otherElement = $(param);
				if ( value != '' && otherElement.val() != '')
					return parseInt(value, 10) >= parseInt(otherElement.val(), 10);
				else
					return true;
		}, 'Total Units should be greater than or equal to Launch Units.');

                $.validator.addMethod('curl', function(value, element) 
		{
			return this.optional(element) || /^(?:(http|https)?:\/\/)?(?:[\w-]+\.)+([a-z]|[A-Z]|[0-9]){2,6}$/gi.test(value);
		}, 'URL format not correct.');
		
		
		$('input[id=\'dtp1\']').change( function() {
			$(this).valid();

                        //if ( $('.enddate').val() != '' )  
			//  $('.enddate').valid();
                        if ( document.getElementById(dtp2) )
			    $('.enddate').valid();  
		});
		
		$('input[id=\'dtp2\']').change( function() {
			$(this).valid();
			$('.startdate').valid();
		});

/*
		
		$('input[id=\'total_units\']').change( function() {
			$(this).valid();
			$('#launch_units').valid();
		});
		
		$('input[id=\'launch_units\']').change( function() {
			$(this).valid();
		});
		
		
		$('.allownumericwithdecimal').on('keypress keyup blur',function (event) 
		{
            //this.value = this.value.replace(/[^0-9\.]/g,'');
			
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
			{
                event.preventDefault();
            }
        });

		$('.allownumericwithoutdecimal').on('keypress keyup blur',function (event) 
		{    
			$(this).val($(this).val().replace(/[^\d].+/, ''));
            
			if ((event.which < 48 || event.which > 57)) 
			{
                event.preventDefault();
            }
		});	

*/		
		var ruleMSetPricing = {
					required : true,
					range    :[0.00,999999999]
					
		};
		
		var messageMSetPricing = {
					required : 'Field is required.',
					range  : 'Allowed : 0 - 999999999.',
		};
		
		var ruleOSetPricing = {
					range    :[0.00,999999999]
		};
		
		var messageOSetPricing = {
					range  : 'Allowed : 0 - 999999999.'
		};
		
		var rulesMEmail = {
					required : true,
					email   : true
		};
		
		var messageMEmail = {
					required : 'Email is required.',
					email   : 'Email format is not correct.'
		};
		
		var rulesMName = {
					required : true,
					onameCustom   : true
		};
		
		var messageMName = {
					required : 'Name is required.',
					onameCustom   : 'Only Alphanumeric characters allowed.'
		};
		
		var rulesOName = {
					onameCustom   : true
		};
		
		var messageOName = {
					onameCustom   : 'Only Alphanumeric characters allowed.'
		};
		
		var rulesMMobile = {
					required : true,
					number   : true,
					range    : [1000000000,9999999999]  
		};
		
		var messageMMobile = {
					required : 'Mobile is required.',
					number   : 'Only numbers allowed.',
					range    : 'Only 10 digits allowed'
		};
		
		var ruleMDiscount = {
					required : true,
					number   : true,
					range: [0,100]
		};
		
		var messageMDiscount = {
					required : 'Percentage(%) is required.',
					number   : 'Only numbers allowed.',
					range	 : 'Not a valid Percentage(%)'
		};
		
		var ruleMFile = {
					required : true,
				
		};
		
		var messageMFile = {	
					required : 'File name is required.',
					extension: 'Accepted formats (png, jpeg, gif).' 
		}
	

		
		$('#id_form_project_pricing').validate({
			// Specify the validation rules
			rules: {
				'data[ProjectPricing][price_srtax_bsp]': ruleMDiscount,
				'data[ProjectPricing][price_srtax_oth]': ruleMDiscount,
				'data[ProjectPricing][price_srtax_icr]': ruleMDiscount,
				'data[ProjectPricing][price_registration]': ruleMSetPricing,
				'data[Project][price_rf_plc]': ruleMSetPricing,
				'data[Project][price_pf_plc]': ruleMSetPricing,
				'data[Project][price_cr_plc]': ruleMSetPricing,
				'data[addition_charges][price_core_amt1]': ruleOSetPricing,
				'data[charges][price_pcharge_amt1]': ruleOSetPricing,
				'data[othercharges][price_other_amt1]': ruleOSetPricing,
				'data[ProjectPricing][floor_plc_amount]': ruleOSetPricing,
				'data[ProjectPricing][floor_per_plc_amount]': ruleOSetPricing,
				/*'data[ProjectPricing][price_sp_offer]': {
					required : true
				},*/
				/*'data[ProjectPricing][price_remarks]': {
					required : true
				},*/
				'data[ProjectPricing][pp_error]': {
					box : true
				}
			},
			
			// Specify the validation error messages
			messages: {
				'data[ProjectPricing][price_srtax_bsp]': messageMDiscount,
				'data[ProjectPricing][price_srtax_oth]': messageMDiscount,
				'data[ProjectPricing][price_srtax_icr]': messageMDiscount,
				'data[ProjectPricing][price_registration]': messageMSetPricing,
				'data[Project][price_rf_plc]': messageMSetPricing,
				'data[Project][price_pf_plc]': messageMSetPricing,
				'data[Project][price_cr_plc]': messageMSetPricing,
				'data[addition_charges][price_core_amt1]': messageOSetPricing,
				'data[charges][price_pcharge_amt1]': messageOSetPricing,
				'data[othercharges][price_other_amt1]': messageOSetPricing,
				'data[ProjectPricing][floor_plc_amount]': messageOSetPricing,
				'data[ProjectPricing][floor_per_plc_amount]': messageOSetPricing,
				/*'data[ProjectPricing][price_sp_offer]': {
					required : 'Field is required'
				},*/
				/*'data[ProjectPricing][price_remarks]': {
					required : 'Field is required'
				}*/
			},
			
			submitHandler: function(form){
				
				// Check Form Error
				if ( true == checkPriceErrors() )
				{	
					//Found Errors. Donot Proceed further.
					return false;	
				}
				
				// Validations OK. Form can be submitted
				
				// TODO : Ajax call
				$('#loadingmessage4').show();
				
				// Changes of Page when Ajax call is successful
				
				
				$.ajax
			({
				type: 'POST',
				url: '$project_pricing_url',
				data: $('#id_form_project_pricing').serialize(), // serializes the form's elements.
				complete: function(status)
				{
                                    $('#loadingmessage4').hide();
                                },
				success: function(data)
				{
				 	// TODO : Ajax call
				
				// 1. Change NEXT -> SAVED
				$('#id_next4').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next4').css('background','#ffc000');
				//$('#id_next4').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_pricing :input').prop('disabled', true);
				//$('#id_cloneCorePlcAddRow').attr('disabled','disabled');
				//$('#id_cloneProjectChargeAddRow').attr('disabled','disabled');
				//$('#id_cloneOtherPlcAddRow').attr('disabled','disabled');
				
				
				// 4. Expand the next block
				$('#id_proj_office_exp').css('display','block');
				$('#id_proj_office_exp').addClass('active');
				$('#id_proj_office_content').css('display','block');
				
                                // 5. Close current block
				//$('#id_proj_pricing_exp').removeClass('active');
				//$('#id_proj_pricing_content').css('display','none');
				//$('#id_top').attr('tabindex',-10).focus();			
			
				  
				}
		});
		
				
				
				
				// To remain on same page after submit
				return false;
				
			}
		});
		
		
		$('#id_form_project_basic').validate({
		
			// Specify the validation rules
			rules: {
				'data[Project][project_name]': {
					required : true,
					onameCustom : true
				},
				'data[Project][project_type]': {
					required : true
				},
				'data[Project][project_highlights]': {
					required : true
				},
				'data[Project][project_description]': {
					required : true
				},
				/*'data[Project][project_address]': {
					required : true,
					map_check : true
				},*/
				'data[Project][dtp1]': {
					required : true,
					dateCustom :  true
				},
				'data[Project][dtp2]': {
					required : true,
					date :  true
				}
				
			},
		
			// Specify the validation error messages
			messages: {
				'data[Project][project_name]': {
					required : 'Project Name is required.'
				},
				'data[Project][project_type]': {
					required : 'Project Type is mandatory.'
				},
				'data[Project][project_highlights]': {
					required : 'Project Highlights is required.'
				},
				'data[Project][project_description]': {
					required : 'Project Description is required.'
				},
				/*'data[Project][project_address]': {
					required : 'Project address is required.'
				},*/
				'data[Project][dtp1]': {
					required : 'Project Launch Date is required.',
					date : 'Date format not correct.'
				},
				'data[Project][dtp2]': {
					required : 'Project Completion Date is required.',
					date : 'Date format not correct.'
				}
			},
			
			submitHandler: function(form){
				
				// Validations OK. Form can be submitted
				$('#loadingmessage1').show();
				
					$.ajax
			({
				type: 'POST',
				url: '$project_base_url',
				data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
				complete: function(status)
				{
                                    $('#loadingmessage1').hide();
                                },
				success: function(data)
				{
					var selprojectval = $('#project_id').val(); 
					 $('.project_id').val(selprojectval);
					 
					 var selprojectpplanval = $('#project_plan').val(); 
					 $('.project_id').val(selprojectpplanval);
				 	// TODO : Ajax call
				
				// Changes of Page when Ajax call is successful
				
				// 1. Change NEXT -> SAVED
				$('#id_next1').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next1').css('background','#ffc000');
				//$('#id_next1').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_basic :input').prop('disabled', true);
				
				// 4. Expand the next block
				$('#id_proj_pricing_exp').css('display','block');
				$('#id_proj_pricing_exp').addClass('active');
				$('#id_proj_pricing_content').css('display','block');
				//$('#id_next2').css('display','inline');
			
                                // 5. Close current block
				/*$('#id_proj_basic_exp').removeClass('active');
				$('#id_proj_basic_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();*/
				  
				}
		});
		
				
			
				// To remain on same page after submit
				return false;
			}

		});

		
		
	");
?>



