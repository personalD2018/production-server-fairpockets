<div class="page-content">
    <div class="row">
        <div id="id_top" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Post Your Project</h2>
            <br />
        </div>	

        <div class="add_property_form">

            <?php
            echo $this->Form->create(
                    'Project', array(
                        'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'id_form_project_basic',
                        'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block">

                <div class="add-title-tab">
                    <h3>Project Basic</h3>
                    <div class="add-expand active" id="id_proj_basic_exp" ></div>
                </div>

                <div class="add-tab-content" id="id_proj_basic_content" style="display:block;">
                    
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="project_name" class="label_title">Name of Project <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input('project_name', array(
                                            'id' => 'project_name',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Project Name',
                                            'label' => false
                                        )
                                    );
                                    ?>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="property-type" class="label_title">Project Type <span class="mand_field">*</span></label>
                                    <div class="select-style ">
                                        <?php
                                        echo $this->Form->input(
                                                'project_type', array(
                                            'id' => 'project_type',
                                            'options' => array(
                                                '1' => 'Residential',
                                                '2' => 'Commercial'
                                            ),
											'empty' => 'Select',
                                            'class' => 'selectpicker',
                                            'label' => false
                                                )
                                        );
                                        ?>

                                    </div>		
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-status" class="label_title">Highlights of the Project <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Please describe unique selling proposition of the Project"></i><br />
                                    <?php
                                    echo $this->Form->textarea(
                                            'project_highlights', array(
                                        'id' => 'project_highlights',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter your Project Highlights',
                                        'rows' => '1',
										'maxlength' => '600',
                                        'label' => false
                                            )
                                    );
                                    ?>
									<label for="property-status" style="color:green; font-size:12px;">(600 Characters Only)</label>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="property-type" class="label_title">Description of Project <span class="mand_field">*</span></label><br />
									<?php
                                    echo $this->Form->textarea(
                                            'project_description', array(
                                        'id' => 'project_description',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter your Project Description',
                                        'rows' => '1',
										'maxlength' => '8000',
                                        'label' => false
                                            )
                                    );
                                    ?>
									<label for="property-status" style="color:green; font-size:12px;">(800 Characters Only)</label>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="property-type" class="label_title">Launch date <span class="mand_field">*</span></label>
                                    <div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                                        <?php
                                        echo $this->Form->input(
                                                'dtp1', array(
                                            'id' => 'dtp1',
                                            'class' => 'form-control startdate',
                                            'size' => '16',
                                            'readonly' => 'readonly',
                                            'style' => 'cursor : pointer',
                                            'type' => 'text',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                        <span class="add-on removeicon"><i class="icon-remove"></i></span>
                                        <span class="add-on calicon"><i class="icon-th"></i></span>
                                    </div>
                                    <?php
                                    echo $this->Form->input(
                                            'dtp_input1', array(
                                        'id' => 'dtp_input1',
                                        'value' => '',
                                        'type' => 'hidden',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                  
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="property-type" class="label_title">Expected Completion date <span class="mand_field">*</span></label>
                                    <div class="controls datefield date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                        <?php
                                        echo $this->Form->input(
                                                'dtp2', array(
                                            'id' => 'dtp2',
                                            'class' => 'form-control enddate',
                                            'size' => '16',
                                            'readonly' => 'readonly',
                                            'style' => 'cursor : pointer',
                                            'type' => 'text',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                        <span class="add-on removeicon"><i class="icon-remove"></i></span>
                                        <span class="add-on calicon"><i class="icon-th"></i></span>
                                    </div>
                                    <?php
                                    echo $this->Form->input(
                                            'dtp_input2', array(
                                        'id' => 'dtp_input2',
                                        'value' => '',
                                        'type' => 'hidden',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                    
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="form-group">

                                            <label for="city" class="label_title">City <span class="mand_field">*</span></label>

                                            <div class="select-style">
                                                <?php
                                                echo $this->Form->input(
                                                        'project_city', array(
                                                    'id' => 'id_select_city', 'options' => $city_master,
                                                    'class' => 'selectpicker bs-select-hidden',
                                                    'label' => false,
                                                    'empty' => 'Select'
                                                        )
                                                );
                                                ?>		
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="searchInput" class="label_title">Select Locality </label>
                                            <!-- <input class="form-control" id="searchInput" placeholder="Locality"> -->
                                            <?php
                                            echo $this->Form->input(
                                                        'locality', array(
                                                        'id' => 'searchInput',
                                                        'type' => 'text',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Locality',
                                                        'label' => false
                                                    )
                                            );
                                            ?>

                                            <div id="result" class="input text gMapResults" style="display:none";>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="countryState" class="label_title">Address </label>
                                            <!-- <input class="form-control" id="countryState" placeholder="House No, Street No, etc."> -->
                                            <?php
                                            echo $this->Form->input(
                                                    'project_address', array(
                                                'id' => 'address',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'placeholder' => 'House No, Street No, etc.',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>
                                    </div>

                                </div>	
                            </div>

                            <div class="col-md-8 col-sm-6">
                                <div class="map" id="map" style="border-style: none;border:none;width:100%;"></div>
                            </div>

                        </div>

                        <hr>
                        <div class="row">

                            <input id="id_search_adm_area_1" class="form-controls" type="hidden" >
                            <input id="id_search_adm_area_2" class="form-controls" type="hidden" >


                            <div class="form_area">

                                <?php echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
                                <!-- Latitude -->
                                <?php echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>
                                <?php echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
                                <?php echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

                            </div>
                        </div>
                        <?php echo $this->Form->input('current_project', array(
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => '1'
                                )
                        );
                        ?>
                        <?php echo $this->Form->input('id', array('class' => 'project_id',
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => $this->request->data['Project']['project_id']
                                )
                        );
                        ?>			
                        <div class="account-block text-center submit_area">
                            <div id='loadingmessage1' style='display:none'>
                                <img src='../../img/ajax-loader.gif'/>
                            </div>
                            <button type="submit" id="id_next1" class="btn btn-primary btn-next ">SAVE</button>
                        </div>

                </div>

            </div>

            <?php
            echo $this->Form->end();
            ?>


            <?php
            echo $this->Form->create(
                        'ProjectDetail', array(
                        'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'id_form_project_feature',
                        'novalidate' => 'novalidate',
                        'type' => 'file'
                    )
            );
            ?>

            <div class="account-block property_feature">

                <div class="add-title-tab">
                    <h3>Project Details</h3>
                    <div class="add-expand" id="id_proj_detail_exp" ></div>
                </div>

                <div class="add-tab-content" id="id_proj_detail_content">
                    
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="web_link" class="label_title">Web Link</label>
									<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Format should be like www.abc.com"></i>
                                    <?php
                                    echo $this->Form->input(
                                            'web_link', array(
                                        'id' => 'web_link',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'placeholder' => 'Web Link',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label_title"> Number Of Towers <span class="mand_field">*</span> </label>
                                    <div class="select-style">
                                        <?php
                                        echo $this->Form->input(
                                                'num_of_tow', array(
                                            'id' => 'num_of_tow',
                                            'options' => array(
                                                '1' => '1',
                                                '2' => '2',
                                                '3' => '3',
                                                '4' => '4',
                                                '5' => '5',
                                                '6' => '6',
                                                '7' => '7',
                                                '8' => '8',
                                                '9' => '9',
                                                '10' => '10',
                                                '11' => '11',
                                                '12' => '12',
                                                '13' => '13',
                                                '14' => '14',
												'15' => '15',
												'16' => '16',
												'17' => '17',
												'18' => '18',
												'19' => '19',
												'20' => '20',
												'21' => '21',
												'22' => '22',
												'23' => '23',
												'24' => '24',
												'25' => '25',
												'26' => '26',
												'27' => '27',
												'28' => '28',
												'29' => '29',
												'30' => '30',
												'31' => '31',
												'32' => '32',
												'33' => '33',
												'34' => '34',
												'35' => '35',
												'36' => '36',
												'37' => '37',
												'38' => '38',
												'39' => '39',
                                                '40' => '40'), 'empty' => 'Select Number of Towers',
                                            'class' => 'selectpicker',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">									

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <label for="project_name" class="label_title">Land area <span class="mand_field">*</span></label>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            echo $this->Form->input(
                                                    'land_area', array(
                                                'id' => 'land_area',
                                                'type' => 'text',
                                                'class' => 'form-control parea_box',
                                                'placeholder' => 'Land Area',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                            <div class="parea_val"> Acres </div>
                                        </div>
                                    </div>
                                </div>
                            </div>	

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label_title">Total Units</label>
                                    <?php
                                    echo $this->Form->input(
                                            'total_units', array(
                                        'id' => 'total_units',
                                        'type' => 'text',
                                        'class' => 'form-control allownumericwithoutdecimal',
                                        'placeholder' => 'Unit',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label_title">Launch Units <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'launch_units', array(
                                        'id' => 'launch_units',
                                        'type' => 'text',
                                        'class' => 'form-control allownumericwithoutdecimal',
                                        'placeholder' => 'Unit',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label_title"> Construction Status <span class="mand_field">*</span> </label>
                                    <div class="select-style">
                                        <?php
                                        echo $this->Form->input(
                                                'cons_status', array(
                                            'id' => 'cons_status',
                                            'options' => array(
                                                '1' => 'Under Construction',
                                                '2' => 'Ready to move'
                                            ),
                                            'empty' => 'Select',
                                            'class' => 'selectpicker',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label_title">Logo</label>
                                    <?php
                                    echo $this->Form->file('proj_logo', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'fileupload'));
                                    if ($this->request->data['ProjectDetail']['proj_logo']) {
                                        ?>

                                        <img src="<?php echo $this->base; ?>/upload/project_logo/<?php echo $this->request->data['ProjectDetail']['proj_logo']; ?>" alt="thumb" width="180" height="170">
                                    <?php } ?>
                                </div>		

                            </div>
							
							 <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label_title">RERA</label>
                                    <?php
                                    echo $this->Form->input(
                                            'rera', array(
                                        'id' => 'rera',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'placeholder' => 'RERA',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
							
                        </div>
                        <?php echo $this->Form->input('id', array('class' => 'project_id',
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => $this->request->data['ProjectDetail']['id']
                                )
                        );
                        ?>	
                        <?php
                        echo $this->Form->input('project_id', array(
                                    'class' => 'project_id',
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => $this->request->data['Project']['project_id']
                                )
                        );
                        ?>									
                        <div class="account-block text-center submit_area">
                            <div id='loadingmessage2' style='display:none'>
                                <img src='../../img/ajax-loader.gif'/>
                            </div>
                            <button type="submit" id="id_next2" class="btn btn-primary btn-next ">SAVE</button>
                        </div>

                </div>			

            </div>

            <?php
            echo $this->Form->end();
            ?>

            <?php
            echo $this->Form->create(
                    'ProjectFinancer', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_bank',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block project_bank">

                <div class="add-title-tab">
                    <h3>Financer Table</h3>
                    <div class="add-expand" id="id_proj_bank_exp"></div>
                </div>

                <div class="add-tab-content" id="id_proj_bank_content" >

                    <div class="add-tab-row push-padding-bottom row-color-gray " >

                        <h3>Banks</h3>
                        <?php
                            echo $this->Form->input('id', array('class' => 'project_id',
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => $this->request->data['ProjectFinancer']['id']
                                )
                            );
                            echo $this->Form->input('project_id', array('class' => 'project_id',
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => $this->request->data['Project']['project_id']
                                )
                            );
                        ?>  
                        <input type="hidden" id="id_cloneBankCount" value="5" >
                        <div class="addrowarea morerow" id="id_cloneBank_tab">
                            <?php
                            if (!empty($this->request->data['ProjectFinancer']['bank'])) {

                                $payment_bank = json_decode($this->request->data['ProjectFinancer']['bank'], 'true');

                                for ($i = 1; $i <= count($payment_bank); $i++) {
                                    ?>
                                    <div class="row cloneBank" id="cloneBank<?php echo $i; ?>"  >

                                        <div class="col-sm-6">
                                            <div class="select-style">
                                                <label for="project_name" class="label_title">Bank Name </label>
                                                <?php
                                                echo $this->Form->input('bank.bank_name' . $i, array(
                                                    'id' => 'bank_name' . $i,
                                                    'options' => $banks,
                                                    'value' => $payment_bank["bank_name" . $i], 'class' => 'selectpicker bs-select-hidden uniqueBank',
                                                    'label' => false,
											'empty' => 'Select'
                                                        )
                                                );
                                                ?>
                                            </div>
                                        </div>


                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="frowedit">
                                                    <a onclick="cloneBankDelRow(this);"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <?php
                                }
                            } else {
                                ?>


                                <div class="row cloneBank" id="cloneBank1"  >

                                    <div class="col-sm-6">
                                        <div class="select-style">
                                            <label for="project_name" class="label_title">Bank Name </label>
                                            <?php
                                            echo $this->Form->input('bank_name1', array(
                                                'id' => 'bank_name1',
                                                'options' => $banks,
                                                'class' => 'selectpicker bs-select-hidden uniqueBank',
                                                'label' => false,
												'empty' => 'Select'
                                                    )
                                            );
                                            ?>
                                        </div>
                                    </div>


                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <div class="frowedit">
                                                <a onclick="cloneBankDelRow(this);"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php } ?>



                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <a id="id_cloneBankAddRow" onclick="cloneBankAddRow();" class="btn btn-green"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>

                    </div>


                    <div class="account-block text-center submit_area">
                        <div id='loadingmessage3' style='display:none'>
                            <img src='../../img/ajax-loader.gif'/>
                        </div>
                        <button type="submit" id="id_next3" class="btn btn-primary btn-next ">SAVE</button>
                    </div>

                </div>
            </div>	
            									
            <?php
            echo $this->Form->end();
            ?>

           <?php
            echo $this->Form->create(
                    'ProjectOfficeUse', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_office',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block project_office">

                <div class="add-title-tab">
                    <h3>For Office Use Only</h3>
                    <div class="add-expand" id="id_proj_office_exp"></div>
                </div>

                <div class="add-tab-content" id="id_proj_office_content" >

                    <div class="add-tab-row push-padding-bottom" >

                        <div class="row">

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <div class="row">


                                        <div class="col-sm-12"> 
                                            <label for="property-price" class="label_title">Maximum Discount for Customers</label>
                                        </div>

                                        <div class="col-sm-8">
                                            <?php
                                            echo $this->Form->input(
                                                    'office_maxdis', array(
                                                'id' => 'office_maxdis',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-4">

                                            <div class="select-style">
                                                <?php
                                                echo $this->Form->input(
                                                        'price_reg_unit', array(
                                                    'id' => 'price_reg_unit',
                                                    'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage',
                                                        '3' => 'Per Sq feet'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                        </div>

                                    </div>											

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <div class="row">

                                        <div class="col-sm-12"> 
                                            <label for="property-price" class="label_title">Brokerage</label>
                                        </div>

                                        <div class="col-sm-8">
                                            <?php
                                            echo $this->Form->input(
                                                    'office_brkg_fp', array(
                                                'id' => 'office_brkg_fp',
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div class="col-sm-4">

                                            <div class="select-style">
                                                <?php
                                                echo $this->Form->input(
                                                        'price_brokage_unit', array(
                                                    'id' => 'price_brokage_unit',
                                                    'options' => array(
                                                        '1' => 'Amount',
                                                        '2' => 'Percentage',
                                                        '3' => 'Per Sq feet'
                                                    ),
                                                    'class' => 'selectpicker',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>

                                        </div>

                                    </div>

                                </div>		

                            </div>

                        </div>

                        <div class="row">	

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Payment Release Timelines </label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Please specify the payment release timelines and criteria."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'office_pay_time', array(
                                        'id' => 'office_pay_time',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'rows' => '1',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Remarks </label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_remarks', array(
                                        'id' => 'office_remarks',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'rows' => '1',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Sales Head Name </label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Decision maker related to sales, discounting, others."></i>
                                    <?php
                                    echo $this->Form->input(
                                            'office_sales_hdname', array(
                                        'id' => 'office_sales_hdname',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Sales Head Mobile</label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_sales_hdmobile', array(
                                        'id' => 'office_sales_hdmobile',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Sales Head Email</label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_sales_hdemail', array(
                                        'id' => 'office_sales_hdemail',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>


                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Sales Contact Name <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="This contact can be shared with prospective buyer."></i><?php
                                    echo $this->Form->input(
                                            'office_sales_ctname', array(
                                        'id' => 'office_sales_ctname',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Sales Contact Mobile <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_sales_ctmobile', array(
                                        'id' => 'office_sales_ctmobile',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Sales Contact Email <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_sales_ctemail', array(
                                        'id' => 'office_sales_ctemail',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>


                        </div>	

                        <div class="row">	

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Brochure</label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_brochure', array(
                                        'id' => 'office_brochure',
                                        'type' => 'file',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    if ($this->request->data['ProjectOfficeUse']['office_brochure']) { 
									$img_extn = explode(".", strtolower($this->request->data['ProjectOfficeUse']['office_brochure']));
									//print_r($img_extn);
									$img_extn[1];
									?>
										<?php if($img_extn[1] == 'pdf'){ ?>
											<a href="<?php echo $this->webroot; ?>upload/project_office_brochure/<?php echo $this->request->data['ProjectOfficeUse']['office_brochure']; ?>" download><?php echo $this->request->data['ProjectOfficeUse']['office_brochure']; ?></a>
										<?php }else{ ?>
											<img src="<?php echo $this->base; ?>/upload/project_office_brochure/<?php echo $this->request->data['ProjectOfficeUse']['office_brochure']; ?>" alt="thumb" width="50" height="50">                                        
										<?php } ?>
                                        <input type="hidden" name="office_brochure1" value="<?php echo $this->request->data['ProjectOfficeUse']['office_brochure']; ?>"/>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Rate Card <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'office_rtcard', array(
                                        'id' => 'office_rtcard',
                                        'type' => 'file',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    if ($this->request->data['ProjectOfficeUse']['office_rtcard']) {
										$img_extn1 = explode(".", strtolower($this->request->data['ProjectOfficeUse']['office_rtcard']));
                                        ?>
										
										<?php if($img_extn1[1] == 'pdf'){ ?>
											<a href="<?php echo $this->webroot; ?>upload/project_office_rtcard/<?php echo $this->request->data['ProjectOfficeUse']['office_rtcard']; ?>" download>Rate Card</a>
										<?php }else{ ?>
											<img src="<?php echo $this->base; ?>/upload/project_office_rtcard/<?php echo $this->request->data['ProjectOfficeUse']['office_rtcard']; ?>" alt="thumb" width="50" height="50">                                        
										<?php } ?>

                                        <input type="hidden" name="office_rtcard1" value="<?php echo $this->request->data['ProjectOfficeUse']['office_rtcard']; ?>"/>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>	

                    </div>

                    <div class="account-block text-center submit_area">
                        <div id='loadingmessage5' style='display:none'>
                            <img src='../../img/ajax-loader.gif'/>
                        </div>
                        <button type="submit" id="id_next5" class="btn btn-primary btn-next ">SAVE</button>
                    </div>

                </div>

            </div>
            <?php
            echo $this->Form->input('project_id', array(
                'class' => 'project_id',
                'type' => 'hidden',
                'label' => false,
                'value' => $this->request->data['Project']['project_id']
                    )
            );
            ?>				
            <?php
            echo $this->Form->input('id', array('class' => 'project_id',
                'type' => 'hidden',
                'label' => false,
                'value' => $this->request->data['ProjectOfficeUse']['id']
                    )
            );
            ?>		

            <?php
            echo $this->Form->end();
            ?>
            <form class="fpform" method="post" enctype="multipart/form-data" action="<?php echo $this->webroot; ?>projects/projectimages" >

                <div class="account-block project_photos">

                    <div class="add-title-tab">
                        <h3>Add Photos And Videos</h3>
                        <div class="add-expand" id="id_proj_photos_exp"></div>
                    </div>
					
					<h3 style="margin-left:21px;">You Tube Links</h3>
					<div class="row" style="width:96%; background: #fff;padding: 10px 5px;margin-bottom:10px;margin-left:18px;">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="property-price" class="label_title">You Tube Link 1 </label>
								<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Decision maker related to sales, discounting, others."></i>
								<?php
								echo $this->Form->input(
										'ProjectMedia.youtube_link1', array(
									'id' => 'youtube_link1',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
										)
								);
								?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="property-price" class="label_title">You Tube Link 2</label>
								<?php
								echo $this->Form->input(
										'ProjectMedia.youtube_link2', array(
									'id' => 'youtube_link2',
									'type' => 'text',
									'class' => 'form-control',
									'label' => false
										)
								);
								?>
							</div>
						</div>
					</div>
					
					<br>
					
					<h3 style="margin-left:21px;">Image For Mobile App</h3>
					<div class="row" style="width:96%; background: #fff;padding: 10px 5px;margin-bottom:10px;margin-left:18px;">
						<div class="col-sm-4">
                                <div class="form-group">
                                    <label for="property-price" class="label_title">Upload Image</label>
                                    <?php
                                    echo $this->Form->input(
                                            'ProjectMedia.image_app', array(
                                        'id' => 'image_app',
                                        'type' => 'file',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    if ($this->request->data['ProjectMedia']['image_app']) {
										?>
										
										
											<img src="<?php echo $this->base; ?>/upload/project_media/<?php echo $this->request->data['ProjectMedia']['image_app']; ?>" alt="thumb" width="50" height="50">                                        
										

                                        <input type="hidden" name="image_app" value="<?php echo $this->request->data['ProjectMedia']['image_app']; ?>"/>

                                    <?php } ?>
                                </div>
                            </div>
					</div>
					
					<br><br>

                    <div class="add-tab-content" id="id_proj_photos_content" >
						<h3 style="margin-left:21px;">Image</h3>
                        <div class="add-tab-row">
                            <div class="property-media">

                                <div class="account-block text-center">
                                    <div class="row">
                                        <div class="col-sm-12">	
                                            We recommend minimum image size to be 10KB and maximum size to be 5MB. Accepted file format are .png .jpg .jpeg
                                        </div>
                                    </div>
                                </div>

                                <div class="media-gallery">
                                    <div class="row">
                                        <div id="image_preview">
                                            <?php
                                            foreach ($this->request->data['ProjectImage'] as $property_value) {
                                                ?>

                                                <div class="col-sm-2 thum_ar property_image"><figure class="gallery-thumb"> <img src="<?php echo $this->base; ?>/<?php echo $property_value['ProjectImage']['image_name']; ?>" alt="thumb" width="180" height="170"><span class="icon icon-delete"><i class="fa fa-trash"></i></span><span class="icon icon-loader"><i class="fa fa-spinner fa-spin"></i></span></figure></div>

                                            <?php }
                                            ?>
                                            <div class="col-sm-2 thum_ar ">
                                                <figure class="gallery-thumb">
                                                    <img src="<?php echo $this->webroot; ?>img/addimg.jpg" alt="thumb" class="pthumb">
                                                    <label class="btn btn-default btn-file">
                                                        Add Property Picture 
                                                        <?php
                                                        echo $this->Form->input('fileupload.', ['type' => 'file', 'label' => false, 'multiple' => 'multiple', 'id' => 'upload_file', 'style' => 'display:none', 'onchange' => 'preview_image(this)']);
                                                        ?>

                                                    </label>

                                                    <span class="icon icon-loader"><i class="fa fa-spinner fa-spin"></i></span>
                                                </figure>
                                            </div>
                                        </div> 

                                    </div>
                                </div>

                                <div class="upload_btn_area">
                                </div>

                            </div>
                        </div>
                    </div>

                </div>		


                <div class="account-block text-center submit_area">
                    <?php
                    if ($this->request->data['Project']['approved'] == '1') {
                        $project_app = "Update";
                        $type = '3';
                        ?>

                        <button type="submit"  class="btn btn-primary btn-next ">SAVE</button>

                        <?php
                    } else {
                        $project_app = "Submit";
                        $type = '2';
                        ?> 
                        <div class="account-block text-center submit_area">
                            <button type="submit" id="id_submit_project" class="btn btn-primary btn-red"><?php echo $project_app; ?> Project</button>
                        </div>
                    <?php }
                    ?>

                </div>
                <?php
                echo $this->Form->input('Project.project_id', array(
                    'class' => 'project_id',
                    'type' => 'hidden',
                    'label' => false,
                    'value' => $this->request->data['Project']['project_id']
                        )
                );
                ?>
                <?php
                echo $this->Form->input('Project.id', array('class' => 'project_id',
                    'type' => 'hidden',
                    'label' => false,
                    'value' => $this->request->data['Project']['project_id']
                        )
                );
                ?>		
                <?php
                echo $this->Form->input('Project.project_type', array(
                    'type' => 'hidden',
                    'label' => false,
                    'value' => $type
                        )
                );
                ?>							
                <?php
                echo $this->Form->end();
                ?>
        </div>		

    </div> 
</div> 
  
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('add-project');
echo $this->Html->script('add_property_7');
echo $this->Html->script('front/bootstrap-datetimepicker');

// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');

$project_base_url = '/Projects/currentproject';
$project_detail_url = '/Projects/pastprojectdetail';
$project_bank_url = '/Projects/projectbankdetail';
//$project_pricing_url = '/Projects/projectpricing';
$project_office_url = '/Projects/projectofficeuse';
?>
<script>
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1);
	/*$('.enddate').datetimepicker({
			startDate: today
		});*/
	$("#id_form_project_feature #id_next2").trigger( "click" );
	$("#id_next5").trigger( "click" );
	// Disable Project Detail Section
	$('#id_proj_detail_exp').css('display','none');
	
	// Disable Bank Section
	$('#id_proj_bank_exp').css('display','none');
	
	// Disable Pricing Section
	$('#id_proj_pricing_exp').css('display','none');
	
	// Disable Office` Section
	$('#id_proj_office_exp').css('display','none');
	
	// Disable Photos Section
	$('#id_proj_photos_exp').css('display','none');
	
	// Disable submit button
	$('#id_submit_project').css('display','none');
	
	//TODO Remove this
	
	$('#id_proj_detail_exp').css('display','block');
	$('#id_proj_detail_exp').addClass('active');
	$('#id_proj_detail_content').css('display','block');
	$('#id_next2').css('display','inline');

	$('#id_proj_bank_exp').css('display','block');
	$('#id_proj_bank_exp').addClass('active');
	$('#id_proj_bank_content').css('display','block');
	$('#id_next3').css('display','inline');
	
	$('#id_proj_pricing_exp').css('display','block');
	$('#id_proj_pricing_exp').addClass('active');
	$('#id_proj_pricing_content').css('display','block');
	$('#id_next4').css('display','inline');

	$('#id_proj_office_exp').css('display','block');
	$('#id_proj_office_exp').addClass('active');
	$('#id_proj_office_content').css('display','block');
	$('#id_next5').css('display','inline');
	
	$('#id_proj_photos_exp').css('display','block');
	$('#id_proj_photos_exp').addClass('active');
	$('#id_proj_photos_content').css('display','block');
	
	$('#id_submit_project').css('display','inline');
	
});
</script>	
<script>

		$.validator.addMethod('map_check', function(value, element) 
		{
			var ret_val = checkGmapErrors();
			return ret_val ;

		}, 'Please tick a locality on the map.');

		$.validator.addMethod('box', function(value, element) 
		{
			return ( 
						( $('#price_payplan1').is(':checked')) ||
						( $('#price_payplan2').is(':checked')) ||
						( $('#price_payplan3').is(':checked')) ||
						( $('#price_payplan4').is(':checked')) ||
						( $('#price_payplan5').is(':checked'))
			)
		}, 'Please choose a payment plan.');
	
		$.validator.setDefaults({ ignore: '' });
	
		
		$.validator.addMethod('nameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z.\s]+$/i.test(value);
		}, 'Please use English alphabets only.');
	
		$.validator.addMethod('onameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z0-9.\s]+$/i.test(value);
		}, 'Please use Alphanumeric characters only.');	
	
		$.validator.addMethod('dateCustom', function(value, element) 
		{
			return Date.parse(value);
			
		}, 'Date format not correct.');
		
		$.validator.addMethod('enddate', function(value, element) 
		{
			var startdatevalue = $('.startdate').val();
			
			if ( startdatevalue != '' )
				return Date.parse(startdatevalue) < Date.parse(value);
			else
				return true;
			
		}, 'Completion Date should be greater than Launch Date.');

		$.validator.addMethod('startdate', function(value, element) 
		{
			var enddatevalue = $('.enddate').val();
			
			if ( enddatevalue != '' )
				return Date.parse(enddatevalue) > Date.parse(value);
			else
				return true;
			
		}, 'Launch Date should be Less than Completion Date.');
		
		$.validator.addMethod('lessThanEqTo',function (value, element, param) 
		{
				var otherElement = $(param);
				if ( otherElement.val() != '' )
					return parseInt(value, 10) <= parseInt(otherElement.val(), 10);
				else
					return true;
		}, 'Launch Units should be less than or equal to Total Units.');
		
		$.validator.addMethod('greaterThanEqTo',function (value, element, param) 
		{
				var otherElement = $(param);
				if ( value != '' && otherElement.val() != '')
					return parseInt(value, 10) >= parseInt(otherElement.val(), 10);
				else
					return true;
		}, 'Total Units should be greater than or equal to Launch Units.');
		
		  $.validator.addMethod('curl', function(value, element) 
		{
			return this.optional(element) || /^(?:(http|https)?:\/\/)?(?:[\w-]+\.)+([a-z]|[A-Z]|[0-9]){2,6}$/gi.test(value);
		}, 'URL format not correct.');
		
		$('input[id=\'dtp1\']').change( function() {
			$(this).valid();

                        if ( $('.enddate').val() != '' )
			    $('.enddate').valid();
		});
		
		$('input[id=\'dtp2\']').change( function() {
			$(this).valid();
			$('.startdate').valid();
		});

		
		var ruleMSetPricing = {
					required : true,
					range    :[0.00,999999999]
					
		};
		
		var messageMSetPricing = {
					required : 'Field is required.',
					range  : 'Allowed : 0 - 999999999.',
		};
		
		var ruleOSetPricing = {
					range    :[0.00,999999999]
		};
		
		var messageOSetPricing = {
					range  : 'Allowed : 0 - 999999999.'
		};
		
		var rulesMEmail = {
					required : true,
					email   : true
		};
		
		var messageMEmail = {
					required : 'Email is required.',
					email   : 'Email format is not correct.'
		};
		
		var rulesMName = {
					required : true,
					onameCustom   : true
		};
		
		var messageMName = {
					required : 'Name is required.',
					onameCustom   : 'Only Alphanumeric characters allowed.'
		};
		
		var rulesOName = {
					onameCustom   : true
		};
		
		var messageOName = {
					onameCustom   : 'Only Alphanumeric characters allowed.'
		};
		
		var rulesMMobile = {
					required : true,
					number   : true,
                                        range    : [1000000000,9999999999]   

		};
		
		var messageMMobile = {
					required : 'Mobile is required.',
					number   : 'Only numbers allowed.',
					range    : 'Only 10 digits allowed'
		};
		
		var ruleMDiscount = {
					required : true,
					number   : true,
					range: [0,100]
		};
		
		var messageMDiscount = {
					required : 'Percentage(%) is required.',
					number   : 'Only numbers allowed.',
					range	 : 'Not a valid Percentage(%)'
		};
		
		var ruleMFile = {
					required : true,
				
		};
		
		var messageMFile = {	
					required : 'File name is required.',
					extension: 'Accepted formats (png, jpeg, gif).' 
		};
		
	$('#id_form_project_office').validate({
			
			// Specify the validation rules
			rules: {
				//'data[ProjectOfficeUse][office_sales_hdemail]': rulesMEmail,
				'data[ProjectOfficeUse][office_sales_ctemail]': rulesMEmail,
				//'data[ProjectOfficeUse][office_sales_hdmobile]': rulesMMobile,
				'data[ProjectOfficeUse][office_sales_ctmobile]': rulesMMobile,
				//'data[ProjectOfficeUse][office_sales_hdname]': rulesMName,
				'data[ProjectOfficeUse][office_sales_ctname]': rulesMName,
				//'data[ProjectOfficeUse][office_maxdis]': ruleMSetPricing,
				//'data[ProjectOfficeUse][office_brkg_fp]': ruleMSetPricing,
				//'data[ProjectOfficeUse][office_brochure]': ruleMFile,
				//'data[ProjectOfficeUse][office_remarks]': {required : true},
				//'data[ProjectOfficeUse][office_pay_time]': {required : true}
			},
			
			// Specify the validation error messages
			messages: {
				//'data[ProjectOfficeUse][office_sales_hdemail]': messageMEmail,
				'data[ProjectOfficeUse][office_sales_ctemail]': messageMEmail,
				//'data[ProjectOfficeUse][office_sales_hdmobile]': messageMMobile,
				'data[ProjectOfficeUse][office_sales_ctmobile]': messageMMobile,
				//'data[ProjectOfficeUse][office_sales_hdname]': messageMName,
				'data[ProjectOfficeUse][office_sales_ctname]': messageMName,
				//'data[ProjectOfficeUse][office_maxdis]': messageMSetPricing,
				//'data[ProjectOfficeUse][office_brkg_fp]': messageMSetPricing,
				//'data[ProjectOfficeUse][office_brochure]': messageMFile,
				'data[ProjectOfficeUse][office_rtcard]': messageMFile,
				//'data[ProjectOfficeUse][office_remarks]': {required : 'Field is required'},
				//'data[ProjectOfficeUse][office_pay_time]': {required : 'Field is required'}
				
			},
			
			submitHandler: function(form){
			$( '#id_form_project_office' ).submit( function( e ) {
                            $('#loadingmessage5').show();

    $.ajax( {
      url: '<?php print $project_office_url ?>',
      type: 'POST',
      data: new FormData( this ),
      processData: false,
      contentType: false,
          complete: function(status)
				{
                                    $('#loadingmessage5').hide();
                                },
	  success: function(data)
				{
						// 1. Change NEXT -> SAVED
				$('#id_next5').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next5').css('background','#ffc000');
				//$('#id_next5').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_office :input').prop('disabled', true);
				
				// 4. Expand the next block
				
				$('#id_proj_photos_exp').css('display','block');
				$('#id_proj_photos_exp').addClass('active');
				$('#id_proj_photos_content').css('display','block');
				
                                // 5. Close current block
				$('#id_proj_office_exp').removeClass('active');
				$('#id_proj_office_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();

				// 6. Show Submit Button
				$('#id_submit_project').css('display','inline');
				
				  
				}
			} );
			
		} );// Validations OK. Form can be submitted
				
				
			return false;
		}
		});	
	
		
		
		$('#id_form_project_bank').validate({
			
			// Specify the validation rules
			rules: {
				
			},
			
			// Specify the validation error messages
			messages: {
				
			},
			
			submitHandler: function(form){
				
				// Check Form Error
				if ( true == checkBankErrors() )
				{	
					//Found Errors. Donot Proceed further.
					return false;	
				}
				
				// Validations OK. Form can be submitted
				
				// TODO : Ajax call
                                $('#loadingmessage3').show(); 

								$.ajax
			({
				type: 'POST',
				url: '<?php print $project_bank_url ?>',
				data: $('#id_form_project_bank').serialize(), // serializes the form's elements.
				complete: function(status)
				{
                                    $('#loadingmessage3').hide();
                                },
                                success: function(data)
				{
					
				// Changes of Page when Ajax call is successful
				
				// 1. Change NEXT -> SAVED
				$('#id_next3').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next3').css('background','#ffc000');
				//$('#id_next3').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_bank :input').prop('disabled', true);
				//$('#id_cloneBankAddRow').attr('disabled','disabled');
				
				// 4. Expand the next block
				$('#id_proj_office_exp').css('display','block');
				$('#id_proj_office_exp').addClass('active');
				$('#id_proj_office_content').css('display','block');

                                // 5. Close current block
				$('#id_proj_bank_exp').removeClass('active');
				$('#id_proj_bank_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();
				  
				}
		});
		
			
				// To remain on same page after submit
				return false;
				
			}
		});
		
		
			$('#id_form_project_feature').validate({
		
			// Specify the validation rules
			rules: {
				'data[ProjectDetail][web_link]': {
					curl : true
				},
				'data[ProjectDetail][num_of_tow]': {
					required : true
				},
				'data[ProjectDetail][project_highlights]': {
					required : true
				},
				'data[ProjectDetail][land_area]': {
					required : true,
					number   : true,
					min      : 1,
					max      : 999999
				},
				'data[ProjectDetail][total_units]': {
					min      : 1,
					max      : 99999,
					greaterThanEqTo : '#launch_units'
				},
				'data[ProjectDetail][launch_units]': {
					required : true,
					min      : 1,
					max      : 999999,
					lessThanEqTo : '#total_units'
				},
				
				'data[ProjectDetail][cons_status]': {
					required : true
				},
				
			},
		
			// Specify the validation error messages
	
			messages: {
				'data[ProjectDetail][web_link]': {
					url : 'Web Link format not correct.'
				},
				'data[ProjectDetail][num_of_tow]': {
					required : 'Select Number of Towers in the Project'
				},
				'data[ProjectDetail][project_highlights]': {
					required : 'Project Highlights are required.'
				},
				'data[ProjectDetail][land_area]': {
					required : 'Land Area is required.',
					number   : 'Only Numeric Digits allowed.',
					min      : 'Min value allowed is 1',
			        max      : 'Max value allowed is 999999'
				},
				'data[ProjectDetail][total_units]': {
					number   : 'Only Numeric Digits allowed.',
					min      : 'Min value allowed is 1',
			        max      : 'Max value allowed is 99999'
				},
				'data[ProjectDetail][launch_units]': {
					required : 'Launch Units are required.',
					number   : 'Only Numeric Digits allowed.',
					min      : 'Min value allowed is 1',
			        max      : 'Max value allowed is 999999'
				},
				'data[ProjectDetail][cons_status]': {
					required : 'Select Construction Status of the Project'
				},
				'data[ProjectDetail][proj_logo]': {
					extension: 'Accepted formats (png, jpeg, gif).' 
				}
			},
			
			submitHandler: function(form){

                       

			$( '#id_form_project_feature' ).submit( function( e ) {



                            $('#loadingmessage2').show();

    $.ajax( {
      url: '<?php print $project_detail_url ?>',
      type: 'POST',
      data: new FormData( $('#id_form_project_feature')[0] ),
      processData: false,
      contentType: false,
          complete: function(status)
				{
                                    $('#loadingmessage2').hide();
                                },
	  success: function(data)
				{
							// TODO : Ajax call
				
				// Changes of Page when Ajax call is successful
				
				// 1. Change NEXT -> SAVED
				$('#id_next2').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next2').css('background','#ffc000');
				//$('#id_next2').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_feature :input').prop('disabled', true);
				
				// 4. Expand the next block
				$('#id_proj_bank_exp').css('display','block');
				$('#id_proj_bank_exp').addClass('active');
				$('#id_proj_bank_content').css('display','block');

                                // 5. Close current block
				$('#id_proj_detail_exp').removeClass('active');
				$('#id_proj_detail_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();
				  
				}
			} );
			return false;
		} );// Validations OK. Form can be submitted
				
			}
			
		});				
				
				

		


		$('#id_form_project_basic').validate({
		
			// Specify the validation rules
			rules: {
				'data[Project][project_name]': {
					required : true,
					onameCustom : true
				},
				'data[Project][project_type]': {
					required : true
				},
				'data[Project][project_highlights]': {
					required : true
				},
				'data[Project][project_description]': {
					required : true
				},
				/*'data[Project][project_address]': {
					required : true,
					map_check : true 	 	
				},*/
				'data[Project][dtp1]': {
					required : true,
					dateCustom :  true
				},
				'data[Project][dtp2]': {
					required : true,
					date :  true
				}
				
			},
		
			// Specify the validation error messages
			messages: {
				'data[Project][project_name]': {
					required : 'Project Name is required.'
				},
				'data[Project][project_type]': {
					required : 'Project Type is mandatory.'
				},
				'data[Project][project_highlights]': {
					required : 'Project Highlights is required.'
				},
				'data[Project][project_description]': {
					required : 'Project Description is required.'
				},
				/*'data[Project][project_address]': {
					required : 'Project address is required.'
				},*/
				'data[Project][dtp1]': {
					required : 'Project Launch Date is required.',
					date : 'Date format not correct.'
				},
				'data[Project][dtp2]': {
					required : 'Project Completion Date is required.',
					date : 'Date format not correct.'
				}
			},
			
			submitHandler: function(form){
				
				// Validations OK. Form can be submitted
				$('#loadingmessage1').show();

					$.ajax
			({
				type: 'POST',
				url: '<?php print $project_base_url ?>',
				data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
				complete: function(status)
				{
                                    $('#loadingmessage1').hide();
                                },
                                success: function(data)
				{
					
				 	// TODO : Ajax call
				
				// Changes of Page when Ajax call is successful
				
				// 1. Change NEXT -> SAVED
				$('#id_next1').html('Saved');
				
				// 1. Change NEXT button style
				//$('#id_next1').css('background','#ffc000');
				//$('#id_next1').css('color','#333');
				
				// 3. Freeze the form content
				//$('#id_form_project_basic :input').prop('disabled', true);
				
				// 4. Expand the next block
				$('#id_proj_detail_exp').css('display','block');
				$('#id_proj_detail_exp').addClass('active');
				$('#id_proj_detail_content').css('display','block');
				$('#id_next2').css('display','inline');

                                // 5. Close current block
				$('#id_proj_basic_exp').removeClass('active');
				$('#id_proj_basic_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();
			
				  
				}
		});
		
				
			
				// To remain on same page after submit
				return false;
			}

		});
		
	
</script>


<script type="text/javascript">

    function gMapOptionClick(elem)
    {
        document.getElementById('searchInput').value = elem.innerHTML;
        document.getElementById("result").style.display = 'none';

        //Generate click event
        document.getElementById('searchInput').click();
    }

    $(document).ready(function () {

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                    val = $target.attr('data-value'),
                    $inp = $target.find('input'),
                    idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });

        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });
    });



</script>


