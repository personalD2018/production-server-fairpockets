<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Projects</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Past Projects</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Name</th>
                                <th class="center">project_type</th>
                                <th class="center"> 	City</th>
                                <th class="center">State</th>


                                <th class="center"> 	locality</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($allproject as $past) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $past['Project']['project_name']; ?></td>
                                    <td class="center"><?php
                                        if ($past['Project']['project_type'] == '1') {
                                            echo "Residential";
                                        } else {
                                            echo "Commercial";
                                        }
                                        ?>

                                    </td>
                                    <td class="center"><?php echo $past['Project']['city_data']; ?></td>

                                    <td class="center"><?php echo $past['Project']['state_data']; ?></td>
                                    <td class="center"><?php echo $past['Project']['locality']; ?></td>

                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                              <!--  <a href="<?php echo $this->webroot; ?>projects/editpastproject/<?php echo base64_encode($past['Project']['project_id']); ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
												-->
                                                <?php
                                                if ($past['Project']['approved'] == '1') {
                                                    ?>
                                                    <button class="btn btn-success" title="Approved"><i class=" icon-ok"></i></button>

                                                    <?php
                                                } else {
                                                    ?>
                                                    <button class="btn btn-inverse" title="UnderApproved"><i class=" icon-remove-sign"></i></button>
                                                <?php }
                                                ?>
                                                <?php
                                                if (!empty($past['ProjectOfficeUse']['office_brochure'])) {
                                                    ?>
                                                    <a href="<?php echo $this->base; ?>/upload/project_office_brochure/<?php echo $past['ProjectOfficeUse']['office_brochure']; ?>"> <button class="btn btn-danger" title="Brochure"><i class="icon-arrow-down "></i></button></a>

                                                    <a href="<?php echo $this->base; ?>/upload/project_office_rtcard/<?php echo $past['ProjectOfficeUse']['office_rtcard']; ?>"> <button class="btn btn-danger" title="
                                                                                                                                                                                         Rate Card"><i class="icon-arrow-down"></i></button></a>
                                                        <?php } ?>
                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


