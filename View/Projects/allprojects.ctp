	<?php $paginator = $this->Paginator;?>
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">Listed Projects</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="allprojects" >
						<div class="input-group custom-search-form">
							<input type="text" name="search" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!--
		<div class="user_panel_search">
			<form class="search" id="search" method="post"  action="allprojects" >
				<input type="text" name="search" placeholder="Search..">
				<a onclick="document.getElementById('search').submit()" ><i class="fa fa-search fa-lg" aria-hidden="true" style="color:#000;"></i></a>
			</form>
		</div>
		-->
		<div class="space-x"></div>
		<div class="detail-features detail-block menu-block target-block col-md-12">
			
				<div class="user_inner_menu">
					
					<ul class="panel_menu_top">
						<li><a href="<?php echo $this->webroot; ?>projects/allprojects" <?php
							if ($project_user_inner_menu == 0)
							echo "class=\"menu-item-active\" ";
						?> >All (<?php echo $total; ?>)</a></li>
						<li><a href="<?php echo $this->webroot; ?>projects/allprojects/1" <?php
							if ($project_user_inner_menu == 1)
							echo "class=\"menu-item-active\" ";
						?> >Saved (<?php echo $saved; ?>)</a></li>
						<li><a href="<?php echo $this->webroot; ?>projects/allprojects/2" <?php
							if ($project_user_inner_menu == 2)
							echo "class=\"menu-item-active\" ";
						?> >Approved (<?php echo $approved; ?>)</a></li> 
					</ul> 
					
				</div>
			
		</div>
		<?php
			if (!empty($allprojdata)) {
			foreach ($allprojdata as $data) {
				
				if (!empty($data['ProjectPricing']['payment_plan'])) {
					
					$payment_plan = json_decode($data['ProjectPricing']['payment_plan'], 'true');
					
					$payment_plan_data = '';
					if ($payment_plan['price_payplan1']) {
						$payment_plan_data .= ' Construction Linked Plan';
						$payment_plan_data .= ',';
					}
					if ($payment_plan['price_payplan2']) {
						$payment_plan_data .= ' Flexi Payment Plan';
						$payment_plan_data .= ',';
					}
					if ($payment_plan['price_payplan3']) {
						$payment_plan_data .= ' Down Payment Plan';
						$payment_plan_data .= ',';
					}
					
					if ($payment_plan['price_payplan4']) {
						$payment_plan_data .= ' Subvention Plan';
						$payment_plan_data .= ',';
					}
					} else {
					$payment_plan_data = '';
					}
					if (!empty($data['ProjectFinancer']['bank'])) {
						
						$payment_bank = json_decode($data['ProjectFinancer']['bank'], 'true');
						
						$payment_bank_data = '';
						for ($i = 1; $i <= count($payment_bank); $i++) {
							
							if ($payment_bank['bank_name' . $i] == '1') {
								$payment_bank_data .= ' ICICI Bank';
								$payment_bank_data .= ',';
							}
							
							if ($payment_bank['bank_name' . $i] == '2') {
								
								$payment_bank_data .= ' HDFC Bank';
								$payment_bank_data .= ',';
							}
							if ($payment_bank['bank_name' . $i] == '3') {
								$payment_bank_data .= ' Citibank';
								$payment_bank_data .= ',';
							}
							if ($payment_bank['bank_name' . $i] == '4') {
								$payment_bank_data .= ' State Bank of India';
								$payment_bank_data .= ',';
							}
							if ($payment_bank['bank_name' . $i] == '5') {
								$payment_bank_data .= ' Punjab National Bank';
								$payment_bank_data .= ',';
							}
						}
						} else {
						$payment_bank_data = '';
					}
				?>	
				<div id="build" class="list-detail target-block">
					
					<div class="detail-title">
						<a>
							<?php echo $data['Project']['project_name']; ?>, 
							<?php echo $data['Project']['locality']; ?>, 
							<?php echo $data['Project']['city_data']; ?>
							<?php
								if ($data['Project']['current_project'] == '0')
								echo ' ( Past Project )';
							?>	
						</a>
					</div>
					<div class="bg-white">
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" > <strong>Property Type : </strong><?php
								if ($data['Project']['project_type'] == '1') {
									echo "Residential";
									} else {
									echo "Commercial";
								}
							?> </div>
						</div>
						<?php
							if (!empty($data['ProjectDetail']['land_area'])) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Land Area : </strong><?php echo $data['ProjectDetail']['land_area']; ?> acres </div>
							</div>		
						<?php } ?>	
					</div>
					
					
					<div class="row">
						<?php
							if (!empty($data['ProjectDetail']['cons_status'])) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Status : </strong><?php
									if ($data['ProjectDetail']['cons_status'] == '1') {
										echo "Under Construction";
										} else {
										echo "Ready to move";
									}
								?></div>
							</div>
						<?php } ?>	
							<?php
							if (!empty($payment_plan_data)) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Payment Plans : </strong><?php
									if (!empty($payment_plan_data)) {
										echo rtrim($payment_plan_data, ',');
									};
								?></div>
							</div>
							<?php }
						?>
								
											
					</div>
					
					<div class="row">
						<div class="col-md-6">							
							<div class="form-control" >								
								<strong>Basic Selling Price : </strong><?php
							if (!empty($this->Number->getBspByProjectId($data['Project']['project_id']))) {
							?><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($this->Number->getBspByProjectId($data['Project']['project_id'])); ?> <?php } ?>
							</div>									
						</div>
						
						
							
						<?php
							if (!empty($payment_bank_data)) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Financers : </strong><?php
									if (!empty($payment_bank_data)) {
										echo rtrim($payment_bank_data, ',');
									};
								?></div>
							</div>
						<?php } ?>	
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" ><strong>Posted on : </strong><?php echo $newDate = date("d M Y", strtotime($data['Project']['created_date'])); ?></div>
						</div>
						<?php
							if ($data['ProjectDetail']['rera'] != '') {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Rera : </strong><?php
									echo $data['ProjectDetail']['rera'];
								?></div>
							</div>
						<?php } ?>
						<div class="col-md-12">
							<div class="block-inner-link">
						<ul>
							<li>
								<?php
									if ($data['Project']['current_project'] == '1') {
									?>
									<a href="<?php echo $this->webroot ?>projects/editproject1/<?php echo base64_encode($data['Project']['project_id']) ?>">Edit</a>
									<?php
										} else {
									?>
									<a href="<?php echo $this->webroot ?>projects/editpastproject/<?php echo base64_encode($data['Project']['project_id']) ?>">Edit</a>
									<?php }
								?>
							</li>
						</ul>
					</div>
					
						</div>		
					</div>
					</div>
				</div>
				
				<?php
				}
				} else {
			?>
			<div id="build" class="list-detail detail-block target-block">
				
				No Projects Listing
				
			</div>
			<?php }
		?>
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php
					//   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					
					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					
					// for the 'next' button
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					
					// the 'last' page button
					// echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				?>		
				
			</ul>
			
		</div>
		
	</div>
	

