<?php
	echo $this->Html->script('front/jssor.slider-22.2.10.min');
	$features = json_decode($propertydetails['Property']['features'], true);
	$amenities = json_decode($propertydetails['Property']['amenities'], true);
	
	// Gmap
	$gLng = $project['Project']['lng'];
	$gLat = $project['Project']['lat'];
	$gTitle = $project['Project']['project_name'] . ',' . $project['Project']['locality'] . ', ' . $project['Project']['city_data'];
	
	// Offer price calculator
	$gOfferPrice = $propertydetails['Property']['offer_price'];
	$gServiceTax = 0;
	if (!empty($features['superbuilt_area_insq'])) {
		$gSuperBuiltUpArea = $features['superbuilt_area_insq'];
		} else {
		$gSuperBuiltUpArea = $features['plot_area_insq'];
	}
	// Super Built up area ( in sq ft )
	$gBaseRate = $project_pricing['ProjectPricing']['price_bsp'];   // Base Rate
	$gServiceTaxOnBSP = $project_pricing['ProjectPricing']['price_srtax_bsp'];  // GST on BSP ( in % )
	
	$gServiceTaxOnOTH = $project_pricing['ProjectPricing']['price_srtax_oth'];  // GST on OTHERS ( in % )
	/* get project charges array */
	
	$project_pricing_charge = json_decode($project_pricing['ProjectPricing']['project_charges'], true);
	// ProjectCharges
	
	$project_charges = array();
	for ($i = 1, $j = 0; $i <= count($project_pricing_charge) / 4; $i++, $j++) {
		$pcharge = $project_pricing_charge['price_pcharge' . $i];
		
		switch ($pcharge) {
			case "1":
            $project_charges[$j]['0'] = "One Covered Car Park";
            break;
			case "2":
            $project_charges[$j]['0'] = "Double Covered Car Park";
            break;
			case "3":
            $project_charges[$j]['0'] = "Club Membership";
            break;
			case "4":
            $project_charges[$j]['0'] = "Power BackUp per KVA";
            break;
			case "5":
            $project_charges[$j]['0'] = "Interest Free Maintenance";
            break;
			case "6":
            $project_charges[$j]['0'] = "Road Facing PLC";
            break;
			case "7":
            $project_charges[$j]['0'] = "Park Facing PLC";
            break;
			default:
            $project_charges[$j]['0'] = "Corner PLC";
		}
		
		$project_charges[$j]['1'] = $project_pricing_charge['price_pcharge_type' . $i];
		$project_charges[$j]['2'] = $project_pricing_charge['price_pcharge_amt' . $i];
		$project_charges[$j]['3'] = $project_pricing_charge['price_pcharge_amunit' . $i];
	}
	
	/* get additional charges array */
	
	$project_additional = json_decode($project_pricing['ProjectPricing']['addition_charges'], true);
	
	$additon_charges = array();
	for ($i = 1, $j = 0; $i <= count($project_additional) / 4; $i++, $j++) {
		
		$acharge = $project_additional['price_core_plc' . $i];
		
		switch ($acharge) {
			case "1":
            $additon_charges[$j]['0'] = "Lease Rent";
            break;
			case "2":
            $additon_charges[$j]['0'] = "External Electrification Charges";
            break;
			case "3":
            $additon_charges[$j]['0'] = "External development Charges";
            break;
			case "4":
            $additon_charges[$j]['0'] = "Infrastructure development Charges";
            break;
			case "5":
            $additon_charges[$j]['0'] = "Electricity Connection Charges";
            break;
			case "6":
            $additon_charges[$j]['0'] = "Fire fighting charges";
            break;
			case "7":
            $additon_charges[$j]['0'] = "Electric Meter Charges";
            break;
			case "8":
            $additon_charges[$j]['0'] = "Gas Pipeline Charges";
            break;
			default:
            $additon_charges[$j]['0'] = "Sinking Fund";
		}
		
		$additon_charges[$j]['1'] = $project_additional['price_core_type' . $i];
		$additon_charges[$j]['2'] = $project_additional['price_core_amt' . $i];
		$additon_charges[$j]['3'] = $project_additional['price_core_amunit' . $i];
	}
	
	/* get other charges array */
	
	$project_other = json_decode($project_pricing['ProjectPricing']['other_charges'], true);
	
	$other_charges = array();
	for ($i = 1, $j = 0; $i <= count($project_other) / 4; $i++, $j++) {
		
		$ocharge = $project_other['price_other_plc' . $i];
		
		switch ($ocharge) {
			case "1":
            $other_charges[$j]['0'] = "Other Charges1";
            break;
			case "2":
            $other_charges[$j]['0'] = "Other Charges2";
            break;
			case "3":
            $other_charges[$j]['0'] = "Other Charges3";
            break;
			case "4":
            $other_charges[$j]['0'] = "Other Charges4";
            break;
			case "5":
            $other_charges[$j]['0'] = "Other Charges5";
            break;
			case "6":
            $other_charges[$j]['0'] = "Other Charges6";
            break;
			case "7":
            $other_charges[$j]['0'] = "Other Charges7";
            break;
			case "8":
            $other_charges[$j]['0'] = "Other Charges8";
            break;
			default:
            $other_charges[$j]['0'] = "Other Charges9";
		}
		
		$other_charges[$j]['1'] = $project_other['price_other_type' . $i];
		$other_charges[$j]['2'] = $project_other['price_other_amt' . $i];
		$other_charges[$j]['3'] = $project_other['price_other_amunit' . $i];
	}
	$gaProjectCharges = array_merge($additon_charges, $project_charges, $other_charges);
	$gaProjectChargesLen = count($gaProjectCharges);
	
	/*
		$gaProjectCharges = array (
		
		// [ index(0- ( 9+8+4) ) , option(1-2) , amount , unit(1-2) ]
		
		array( 'Lease Rent' , 2 , 1 , 1 ),
		array( 'External Electrification Charges' , 2 , 1 , 2 ),
		array( 'External development Charges' , 1 , 1 , 2 ),
		array( 'Infrastructure development Charges' , 1 , 1 , 1	 ),
		array( 'Electricity Connection Charges' , 2 , 1 , 1 ),
		array( 'Fire fighting charges' , 2 , 1 , 2 ),
		array( 'Electric Meter Charges', 1 , 1 , 2 ),
		array( 'Gas Pipeline Charges' , 1 , 1 , 1 ),
		array( 'Sinking Fund' , 2 , 1 , 2 ),
		
		array( 'One Covered Car Park' , 2 , 3 , 1 ),
		array( 'Double Covered Car Park' , 2 , 1 , 2 ),
		array( 'Club Membership' , 1 , 1 , 2 ),
		array( 'Power BackUp per KVA' , 2 , 1 , 2 ),
		array( 'Interest Free Maintenance' , 2 , 1 , 1 ),
		array( 'Road Facing PLC' , 2 , 1 , 2 ),
		array( 'Park Facing PLC', 2 , 2 , 2 ),
		array( 'Corner PLC' , 2 , 3 , 2 ),
		
		array( 'Other PLC 1' , 2 , 1 , 1 ),
		array( 'Other PLC 2' , 2 , 1 , 2 ),
		array( 'Other PLC 3' , 1 , 1 , 2 ),
		array( 'Other PLC 4' , 2 , 1 , 2 )
		
		);
	*/
	
	
	// Project Pricing - Stamp Duty
	$gaStampDuty = array(
    //[ option(1-2) , amount ]
	
    array($project_pricing['ProjectPricing']['price_stm_unit'], $project_pricing['ProjectPricing']['price_stamp'])
	);
	
	// Project Pricing - Registration
	$gaRegistration = array(
    // [ option(1-2) , amount ]
    array($project_pricing['ProjectPricing']['price_reg_unit'], $project_pricing['ProjectPricing']['price_registration'])
	);
	
	// Project Pricing - Floor PLC
	$gaTotalFloors = $projectdetail['ProjectDetail']['num_of_tow']; // Total number of floor in the tower of property
	
	$gaFloorPLC = array(
    // [ BSP for Floor (1-2) , per floor charges , per floor charge option [1-2] ]
    array($project_pricing['ProjectPricing']['floor_option'], $project_pricing['ProjectPricing']['floor_per_plc_amount'], $project_pricing['ProjectPricing']['floor_per_option'])
	//array( 2 , 10 , 1 )
	);
	
	$gaMandatoryCharges = '';
	$gaOptionalCharges = '';
	$gaOptionalCount = 0;
	$ProjectCharges = 0;
	$gaFloorPLCCharges = 0;
	
	
	if ($gaFloorPLC[0][1] > 0) {
		$gaFloorPLCCharges = $gaFloorPLC[0][1] * $gSuperBuiltUpArea;
		
		if ($gaFloorPLC[0][0] == 2) {
			// BSP w.r.t Ground floor
			$tempFloor = 0; //0 - Ground Floor
			$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
			} else {
			// BSP w.r.t TOP floor;
			$tempFloor = 0; //0 - Ground Floor
			$gaFloorPLCCharges = $gaFloorPLCCharges * $tempFloor;
		}
		
		if ($gaFloorPLC[0][2] == 2) {
			// sub option
			$gaFloorPLCCharges = $gaFloorPLCCharges * (-1);
		}
		
		$gaMandatoryCharges = $gaMandatoryCharges . "
		<div class='col-sm-12'>
		<label for='' class='checkbox-custom-label'>
		";
		$gaMandatoryCharges = $gaMandatoryCharges . "
		<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
		<label class='checkbox-custom-label' style='cursor:default;'>" .
		'Per Floor Charges' . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $gaFloorPLC[0][1] . "&nbsp;/ sq ft</span>
		";
		$gaMandatoryCharges = $gaMandatoryCharges . "
		</label>  
		</div>
		";
	}
	
	$BaseCharge = ($gSuperBuiltUpArea * $gBaseRate) + $gaFloorPLCCharges;
	$gOfferPrice = $gOfferPrice + $BaseCharge;
	$gOfferPrice = $gOfferPrice + ( $gOfferPrice * ( $gServiceTaxOnBSP / 100 ) );
	
	//echo "<br><br>** gaFloorPLCCharges ** - ".$gaFloorPLCCharges;		
	//echo "<br><br>BaseCharge - ".$BaseCharge;
	//echo "<br><br>gOfferPrice - ".$gOfferPrice;
	// Project Charges
	$ProjectCharges = 0;
	for ($row = 0; $row < $gaProjectChargesLen; $row++) {
		//echo "<br><br>".$gaMandatoryCharges;
		if (2 == $gaProjectCharges[$row][1]) {
			// Mandatory to include the charges
			
			$gaMandatoryCharges = $gaMandatoryCharges . "
			<div class='col-sm-12'>
			<label for='' class='checkbox-custom-label'>
			";
			
			//Calculate Charges
			
			$MandatoryChargeAmt = $gaProjectCharges[$row][2];
			if (1 == $gaProjectCharges[$row][3]) {
				// Per Square feet 	
				$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt * $gSuperBuiltUpArea );
				
				// Display Charge in Mandatory Section
				$gaMandatoryCharges = $gaMandatoryCharges . "
				<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
				<label class='checkbox-custom-label' style='cursor:default;'>" .
				$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "&nbsp;/ sq ft</span>
				";
				} else {
				// Per Unit 	
				$ProjectCharges = $ProjectCharges + ( $MandatoryChargeAmt );
				
				// Display Charge in Mandatory Section
				$gaMandatoryCharges = $gaMandatoryCharges . "
				<input class='checkbox-custom' name='optional[]' type='checkbox' disabled='disabled' checked='checked' style='cursor:default;'>
				<label class='checkbox-custom-label' style='cursor:default;'>" .
				$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true' style='cursor:default;' ></i>&nbsp;" . $MandatoryChargeAmt . "</span>
				";
			}
			
			$gaMandatoryCharges = $gaMandatoryCharges . "
			</label>  
			</div>
			";
			
			//echo "<br><br>	ProjectCharges - ".$ProjectCharges;
			} else {
			// Optional to include the charges
			$gaOptionalCount = $gaOptionalCount + 1;
			$tempid = "optional" . $gaOptionalCount;
			
			$gaOptionalCharges = $gaOptionalCharges . "
			<div class='col-sm-12'>
			
			";
			
			//Calculate Charges
			$OptionalChargeAmt = $gaProjectCharges[$row][2];
			$tempChargeAmt = 0;
			if (1 == $gaProjectCharges[$row][3]) {
				// Per Square feet 	
				$tempChargeAmt = $OptionalChargeAmt * $gSuperBuiltUpArea;
				
				// Display Charge in Optional Section
				$gaOptionalCharges = $gaOptionalCharges . "
				<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
				<label for='" . $tempid . "' class='checkbox-custom-label'>" .
				$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "&nbsp;/ sq ft</span>
				";
				} else {
				// Per Unit 	
				$tempChargeAmt = $OptionalChargeAmt;
				
				// Display Charge in Mandatory Section
				$gaOptionalCharges = $gaOptionalCharges . "
				
				<input id='" . $tempid . "' class='checkbox-custom' name='optional[]' value='" . $tempChargeAmt . "' type='checkbox' onclick='ReCalculateOfferPrice(this);' >
				<label for='" . $tempid . "' class='checkbox-custom-label'>" .
				$gaProjectCharges[$row][0] . "<span class='plc_amount'><i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $OptionalChargeAmt . "</span>
				";
			}
			
			$gaOptionalCharges = $gaOptionalCharges . "
			
			</label>  
			</div>
			";
		}
		
		//echo "<br><br>".$row." ProjectCharges - ".$ProjectCharges;
	}
	
	$gProjectCharges_a = $ProjectCharges;
	
	$gServiceTax = $gServiceTax + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
	$gOfferPrice = $gOfferPrice + $ProjectCharges + ( $ProjectCharges * ( $gServiceTaxOnOTH / 100 ) );
	
	//echo "<br><br>** ProjectCharges ** - ".$ProjectCharges;
	//echo "<br><br>gServiceTax - ".$gServiceTax;
	//echo "<br><br>gOfferPrice - ".$gOfferPrice;
	//Stamp Duty
	$StampDuty = 0;
	if (1 == $gaStampDuty[0][0]) {
		// Percentage
		$StampDuty = ( $ProjectCharges * ( $gaStampDuty[0][1] / 100 ) );
		} else {
		// Flat Charges
		$StampDuty = $gaStampDuty[0][1];
	}
	$gOfferPrice = $gOfferPrice + $StampDuty;
	
	//echo "<br><br>StampDuty - ".$StampDuty;
	//echo "<br><br>gOfferPrice - ".$gOfferPrice;
	//Registration Charges
	$Registration = 0;
	if (1 == $gaRegistration[0][0]) {
		// Percentage
		$Registration = ( ($BaseCharge + $ProjectCharges ) * ( $gaRegistration[0][1] / 100 ) );
		} else {
		// Flat Charges
		$Registration = $gaRegistration[0][1];
	}
	$gOfferPrice = $gOfferPrice + $Registration;
	
	//echo "<br><br>Registration - ".$Registration;
	//echo "<br><br>gOfferPrice - ".$gOfferPrice;
?>	

<script type="text/javascript">
    jQuery(document).ready(function () {
		
        jQuery("#formID").validate()
		
        function nFormatter(num)
        {
            num = Math.abs(num)
            if (num >= 10000000)
            {
                formattedNumber = (num / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
			} else if (num >= 100000)
            {
                formattedNumber = (num / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
			} else if (num >= 1000)
            {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
			} else
            {
                formattedNumber = num;
			}
			
            return formattedNumber;
		}
        format = nFormatter('<?php echo $propertydetails['Property']['offer_price']; ?>');
        document.getElementById("offerpriceformater").innerHTML = format;
	});
</script>				
<style>
    .detail_header .h_feature 
    {
	font-size: 20px;
	color: #29333d;
    }
    .green{color:green; font-weight:bold;border:1px solid; padding:7px;}
    .error{color:red;}
</style>
<style>
    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    .jssora05l.jssora05lds      (disabled)
    .jssora05r.jssora05rds      (disabled)
    */
    .jssora05l, .jssora05r {
	display: block;
	position: absolute;
	/* size of arrow element */
	width: 40px;
	height: 40px;
	cursor: pointer;
	background: url('img/a17.png') no-repeat;
	overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }
    .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
    .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-12 breadCrumbs ">
			<div class="crumb"><a href="">Home</a> > <a href="">Property in Noida</a> > Sector-107 Noida </div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-4 header-price">
			<div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i><span id="offerpriceformater"></span><span class="price_sq"><br/>@ <?php
				if (!empty($features['superbuilt_area_insq'])) {
					$insq = $features['superbuilt_area_insq'];
					} else {
					$insq = $features['plot_area_insq'];
				}
				echo round($propertydetails['Property']['offer_price'] / $insq, 3);
			?> per Sq.Ft</span></div>
		</div>
		<div class="col-md-5 text-center header-feature">
			
			<div class="h_feature"><?php
				if (!empty($features['Configure'])) {
					echo $features['Configure'];
				}
				?>  <span class="h_feature_sub"><?php
					if ($propertyoption['PropertyTypeOptions']['transaction_type'] == '1') {
						echo "Residential";
						} else {
						echo "Commercial";
					}
					?> <?php echo $propertyoption['PropertyTypeOptions']['option_name'] ?>  for <?php
					if ($propertyoption['PropertyTypeOptions']['transaction_type']) {
						echo "Sale";
						} else {
						echo "Rent";
					}
				?></span></div>
				<div class="h_location"><?php echo $project['Project']['project_name']; ?>, <?php echo $project['Project']['locality']; ?>, <?php echo $project['Project']['city_data']; ?>, <?php echo $project['Project']['state_data']; ?></div>	
		</div>
		<div class="col-md-3 text-center header-other-feature">
			<a href="#calyourprice" class="h_postcal">Calculate Price</a>
			<div class="h_postdate">Post on <?php echo $newDate = date("d M Y", strtotime($propertydetails['Property']['created_date'])); ?></div>
			<div class="h_availability"><?php
				if ($propertydetails['Property']['construction_status'] == '1') {
					echo "Ready to move in";
					} else {
					echo "Under Construction";
				};
			?></div>	
		</div>
	</div>
	
	<div class=" property_detail_area col-sm-12 nopaddingside">
		
		<div class="col-sm-12 nopaddingside property_detail_inner">
			<div class="col-sm-9  property-images-main">
				<div class="row">
					
					
					<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
						<!-- Loading Screen -->
						<div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
							<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
							<div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
						</div>
						<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
							<a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
							<?php
								foreach ($propertyImages as $property_value) {
								?>
								<div>
									<img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
									<img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
								</div>
							<?php } ?>
						</div>
						<!-- Thumbnail Navigator -->
						<div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
							<!-- Thumbnail Item Skin Begin -->
							<div data-u="slides" style="cursor: default;">
								<div data-u="prototype" class="p">
									<div class="w">
										<div data-u="thumbnailtemplate" class="t"></div>
									</div>
									<div class="c"></div>
								</div>
							</div>
							<!-- Thumbnail Item Skin End -->
						</div>
						<!-- Arrow Navigator -->
						<span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
						<span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
					</div>
					
					
					
					
				</div>
				<!-- basic detail -->
				<div id="detail" class="detail-list detail-block target-block">
					<div class="detail-title">
						<h2 class="title-left">Detail</h2>
						
						
						
					</div>
					<div class="">
						<ul class="list-three-col">
							<?php
								if (!empty($features['sbua'])) {
								?>
								<li><strong>Super Built Up Area:</strong> <?php echo $features['sbua']; ?> <?php echo $features['sbua_m']; ?></li>
							<?php } ?>
							<?php if (!empty($features['build_up_area'])) {
							?>
							<li><strong>Built Up Area:</strong> <?php echo $features['build_up_area']; ?> <?php echo $features['build_up_sq']; ?></li>
							<?php } ?>
							<?php if (!empty($features['carpet_area'])) {
							?>
							<li><strong>Carpet Area:</strong> <?php echo $features['carpet_area']; ?> <?php echo $features['carpet_area_sq']; ?></li>
							<?php } ?>
							
							<?php if (!empty($features['plot_area'])) {
							?>
							<li><strong>Plot Area:</strong> <?php echo $features['plot_area']; ?> <?php echo $features['plot_area_sq']; ?></li>
							<?php } ?>
							<?php if (!empty($features['length_plot'])) {
							?>
							<li><strong>Length of plot:</strong> <?php echo $features['length_plot']; ?> Sq.ft</li>
							<?php } ?>
							<?php if (!empty($features['length_plot'])) {
							?>
							<li><strong>Width of plot:</strong> <?php echo $features['length_plot']; ?> Sq.ft</li>
							<?php } ?>
							
							<?php if (!empty($features['bedrooms'])) {
							?>
							<li><strong>Bedrooms:</strong> <?php echo $features['bedrooms']; ?></li>
							<?php } ?>
							<?php if (!empty($features['bathrooms'])) {
							?>
							<li><strong>Bathrooms:</strong> <?php echo $features['bathrooms']; ?></li>
							<?php } ?>
							<?php if (!empty($features['washrooms'])) {
							?>
							<li><strong>Washrooms:</strong> <?php echo $features['washrooms']; ?></li>
							<?php } ?>
							<?php if (!empty($features['balconies'])) {
							?>
							<li><strong>Balconies:</strong> <?php echo $features['balconies']; ?></li>
							<?php } ?>
							
							
						</ul>
					</div>
					<div class="clear"></div>
					
					<div class="detail-title-inner">
						<h4 class="title-inner">Additional details</h4>
					</div>
					
					<ul class="list-three-col list_odetail">
						<?php if (!empty($project['Project']['project_name'])) {
						?>
						<li><strong>Project name:</strong> <span><?php echo $project['Project']['project_name']; ?></span></li>
						<?php } ?>
						<?php if (!empty($project['Project']['dtp1'])) {
						?>
						<li><strong>Launch date:</strong> <span><?php echo $project['Project']['dtp1']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['offer_price'])) {
						?>
						<li><strong>Offer Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['offer_price']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['market_price'])) {
						?>
						<li><strong>Market Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_price']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['market_rent'])) {
						?>
						<li><strong>Market Rent:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_rent']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['expected_monthly'])) {
						?>
						<li><strong>Expected Monthly Rent:</strong> <span><?php echo $propertydetails['Property']['expected_monthly']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['deposit'])) {
						?>
						<li><strong>Deposit Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['deposit']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['booking_amount'])) {
						?>
						<li><strong>Booking Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['booking_amount']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($propertydetails['Property']['maintance_amount'])) {
						?>
						<li><strong>Maintance Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['maintance_amount']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($project['Project']['dtp2'])) {
						?>
						<li><strong>Possession Date:</strong> <span><?php echo $project['Project']['dtp2']; ?></span></li>
						<?php } ?>
						<?php if (!empty($propertydetails['Property']['ownership_type'])) {
						?>
						<li><strong>Ownership Type:</strong> <span><?php
							if ($propertydetails['Property']['ownership_type'] == '1') {
								echo "FreeHold";
								}if ($propertydetails['Property']['ownership_type'] == '2') {
								echo "Lease Hold";
								}if ($propertydetails['Property']['ownership_type'] == '3') {
								echo "Power of Attorney";
								}if ($propertydetails['Property']['ownership_type'] == '4') {
								echo "Co-operative Society";
							}
						?></span></li>
						<?php } ?>
						<?php if (!empty($propertydetails['Property']['sale_type'])) {
						?>
						<li><strong>Sale Type:</strong> <span><?php
							if ($propertydetails['Property']['sale_type'] == '1') {
								echo "Resale";
								} else {
								echo "New Booking";
							}
						?></span></li>
						<?php } ?>
						
						<?php
							if (!empty($features['on_floor'])) {
							?>
							<li><strong>Property on Floor :</strong> <span><?php echo $features['on_floor']; ?></span></li>
						<?php } ?>
						<?php if (!empty($features['in_floor'])) {
						?>
						<li><strong>Floors in property:</strong> <span><?php echo $features['in_floor']; ?></span></li>
						<?php } ?>
						
						<?php if (!empty($features['address'])) {
						?>
						<li><strong>Address:</strong> <span><?php echo $propertydetails['Property']['address']; ?>,<?php echo $locality[1]; ?>,<?php echo $city[1]; ?>,<?php echo $state[1]; ?></span></li>
						<?php } ?>
						<?php if (!empty($features['age_of_property'])) {
						?>
						<li><strong>Age of property:</strong> <span><?php echo $features['age_of_property']; ?></span></li>
						<?php } ?>
						<?php if (!empty($features['furnishing'])) {
						?>
						<li><strong>Furnishing:</strong> <span><?php echo $features['furnishing']; ?></span></li>
						<?php } ?>
						<?php if (!empty($features['allowed_floor'])) {
						?>
						<li><strong>Floor Allowed For Construction:</strong> <span><?php echo $features['allowed_floor']; ?></span></li>
						<?php } ?>
						<?php
							if (!empty($features['res_parking_1'])) {
							?>
							<li><strong>Reserved Parking:</strong> <span>None</span></li>
							<?php
								} else {
							?>
							<?php if (!empty($features['res_parking_2'])) {
							?>
							<li><strong>No of Covered Parking:</strong> <span><?php echo $features['no_covered']; ?></span></li>
							<?php } ?>
							<?php if (!empty($features['res_parking_3'])) {
							?>
							<li><strong>No of Open Parking:</strong> <span><?php echo $features['no_open']; ?></span></li>
							<?php
							}
							}
						?>
						<?php
							$otherroom = '';
							if (!empty($features['other_room1']) && $features['other_room1'] == '1') {
								$otherroom .= ' Pooja,';
							}
							if (!empty($features['other_room2']) && $features['other_room2'] == '1') {
								$otherroom .= ' Study,';
							}
							if (!empty($features['other_room3']) && $features['other_room3'] == '1') {
								$otherroom .= ' Servant,';
							}
							if (!empty($features['other_room4']) && $features['other_room4'] == '1') {
								$otherroom .= ' Other,';
							}
						?>
						<?php
							if (!empty($otherroom)) {
							?>
							<li><strong>Other Rooms:</strong> <span><?php echo rtrim($otherroom, ','); ?></span></li>
						<?php } ?>
						
						
						
						
						
						
						
						
					</ul>
					
				</div>
				<div id="features" class="detail-features detail-block target-block" style="min-width:825px;">
					<div class="detail-title">
						<h2 class="title-left">Description</h2>
					</div>
					
					
					<div class="property_des"><?php echo $features['property_description']; ?>.</div>
					
					
					
				</div>
				<div id="detail" class="detail-list detail-block target-block">
					<div class="detail-title">
						<h2 class="title-left">Project Detail</h2>
						
						
						
					</div>
					<div class="builder-view">
						<div class="col-md-4">
							<?php
								if (!empty($projectdetail['ProjectDetail']['proj_logo'])) {
								?>
								<img src="<?php echo $this->webroot; ?>upload/project_logo/<?php echo $projectdetail['ProjectDetail']['proj_logo']; ?>" style="border: 1px solid #eaeaea;"> 
								<?php
									} else {
								?>
								<img src="<?php echo $this->webroot; ?>img/front/projectImg.png" style="    border: 1px solid #eaeaea;"> 
								<?php }
							?>
						</div>
						<div class="col-md-8">
							<ul class="list-three-col">
								<li><strong>Launch date:</strong><?php echo $project['Project']['dtp1']; ?></li>
								<li><strong>Expected Completion date:</strong><?php echo $project['Project']['dtp2']; ?></li>
								<li><strong>Land Area: </strong><?php echo $projectdetail['ProjectDetail']['land_area']; ?> Acres</li>
								
								<li><strong>Number Of Towers:</strong><?php echo $projectdetail['ProjectDetail']['num_of_tow']; ?></li>
								<?php if (!empty($projectdetail['ProjectDetail']['total_units'])) { ?>
									<li><strong>Total Units:</strong><?php echo $projectdetail['ProjectDetail']['total_units']; ?></li>
								<?php } ?>
								<li><strong>Launch Units:</strong><?php echo $projectdetail['ProjectDetail']['launch_units']; ?></li>
								<?php if (!empty($projectdetail['ProjectDetail']['web_link'])) { ?>
									<li><strong>WebLink:</strong><?php echo $projectdetail['ProjectDetail']['web_link']; ?></li>
								<?php } ?>
								<li><strong>Construction Status:</strong><?php
									if ($projectdetail['ProjectDetail']['cons_status'] == '1') {
										echo "Under Construction";
										} else {
										echo "Ready to move";
									}
								?></li>
								<?php
									$paymentplan = json_decode($project_pricing['ProjectPricing']['payment_plan'], true);
									$plan = '';
									if ($paymentplan['price_payplan1'] == '1') {
										$plan .= ' Construction Linked Plan ,';
									}
									if ($paymentplan['price_payplan2'] == '1') {
										$plan .= ' Flexi Payment Plan ,';
									}
									if ($paymentplan['price_payplan3'] == '1') {
										$plan .= ' Down Payment Plan ,';
									}
									if ($paymentplan['price_payplan4'] == '1') {
										$plan .= ' Subvention Plan ,';
									}
									if (!empty($plan)) {
									?>
									<li><strong>Payment Plan:</strong><?php
										echo rtrim($plan, ',');
									?></li>
								<?php } ?>
							</ul>
						</div>
						<div class="clear"></div>
						<hr />
						<div class="builder-item"><strong>Highlights of Project:</strong><br>
							<?php echo $project['Project']['project_highlights']; ?>
						</div>
						<div class="builder-item"><strong>Description of Project:</strong><br>
							<?php echo $project['Project']['project_description']; ?>
						</div>
						<?php
							$banks = json_decode($project_financer['bank'], true);
							$bank = array_values($banks);
							
							if (!empty($bank)) {
							?>
							<div class="block-item">
								<strong>Financers:</strong><br>
								<ul class="banklogo">
									<?php
										if (in_array("0", $bank)) {
										?>
										<li><img src="<?php echo $this->webroot; ?>img/front/boblogo.jpg"></li>
									<?php } ?>
									<?php
										if (in_array("0", $bank)) {
										?>
										<li><img src="<?php echo $this->webroot; ?>img/front/axis.jpg"></li>
									<?php } ?>
									<?php
										if (in_array("4", $bank)) {
										?>
										<li><img src="<?php echo $this->webroot; ?>img/front/sbilogo.jpg"></li>
									<?php } ?>
									<?php
										if (in_array("1", $bank)) {
										?>
										<li><img src="<?php echo $this->webroot; ?>img/front/icicilogo.jpg"></li>
									<?php } ?>
									
								</ul>
							</div>
						<?php } ?>
					</div>
					
					
					
					
					
					
					
				</div>
				
				<!-- basic detail end -->
				
				<!-- feature -->
				<div id="features" class="detail-features detail-block target-block" style="min-width:825px;">
					<div class="detail-title">
						<h2 class="title-left">Features</h2>
					</div>
					<ul class="list-three-col list-features">
						<?php if (!empty($features['type_of_flooring'])) {
						?>
						<li><strong>Type of Flooring:</strong> <?php echo $features['type_of_flooring']; ?></li>
						<?php } ?>
						<?php if (!empty($features['power_backup'])) {
						?>
						<li><strong>Power Backup:</strong> <?php echo $features['power_backup']; ?></li>
						<?php } ?>
						<?php if (!empty($features['watersource1']) || !empty($features['watersource2'])) {
						?>
						<li><strong>Water Source:</strong> <?php
							if (!empty($features['watersource1'])) {
								$source = "Municipal ,";
								} if (!empty($features['watersource2'])) {
								$source = " Borewell Tank,";
							}
						?><?php echo $source; ?></li>
						<?php } ?>
						<?php if (!empty($features['facing'])) {
						?>
						<li><strong>Facing:</strong> <?php echo $features['facing']; ?></li>
						<?php } ?>
						
						<?php if (!empty($features['width_of_fac_road'])) {
						?>
						<li><strong>Width of facing road:</strong> <?php echo $features['width_of_fac_road']; ?></li>
						<?php } ?>
						<?php if (!empty($features['overlook1']) || !empty($features['overlook2']) || !empty($features['overlook3']) || !empty($features['overlook4'])) {
						?>
						<li><strong>Overlooking:</strong> <?php
							if (!empty($features['overlook1'])) {
								echo "Park/Garden";
								} if (!empty($features['overlook2'])) {
								"Main Road";
								}if (!empty($features['overlook3'])) {
								echo "Club";
								}if (!empty($features['overlook4'])) {
								echo "Pool";
							}
						?></li>
						<?php } ?>
						<?php if (!empty($features['bachelor_all'])) {
						?>
						<li><strong>Bachelors Allowed:</strong>Yes</li>
						<?php } ?>
						<?php if (!empty($features['pet_all'])) {
						?>
						<li><strong>Pet allowed:</strong>Yes</li>
						<?php } ?>
						<?php if (!empty($features['non_veg'])) {
						?>
						<li><strong>Non vegetarian:</strong>Yes</li>
						<?php } ?>
						<?php if (!empty($features['boundary_wall'])) {
						?>
						<li><strong>Boundary wall:</strong>Yes</li>
						<?php } ?>
						<?php if (!empty($features['society_type'])) {
						?>
						<li><strong>Society Type:</strong> <span><?php echo $features['society_type']; ?></span></li>
						<?php } ?>
					</ul>
				</div>
				
				<!-- feature end -->
				<!-- Amenities -->
				<div id="amenities" class="detail-amenities detail-block target-block col-md-12">
					<div class="detail-title">
						<h2 class="title-left">Amenities</h2>
					</div>
					<ul class=" list-amenities">
						
						
						<div class="row">
							<div class ="col-md-3">
								<?php
									$count = 0;
									
									foreach ($amenities as $key => $amevalu) {
									?>
									
									<!--<div class= "customDiv">column1</div>-->
									<?php
										if ($key == 'vaastu' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_vaastu">Feng shui/Vaastu compliant</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									
									<?php
										if ($key == 'lift' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_lift">Lift</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'park' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_park">Park</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'staff' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_staff">Maintenance Staff</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'parking' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_parking">Visitor Parking</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'waterstorage' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_water_storage">Water Storage</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									
									
									
									<!-- <div class= "customDiv">column2</div>-->
									
									<?php
										if ($key == 'intercomm' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_intercomm">Intercomm Facility</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									
									<?php
										if ($key == 'waste_disposal' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_waste">Waste Disposal</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'tarrace' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_tarrace">Private Garden/Terrace</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'security_personal' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_securityp">Security Personnel</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'goods_lift' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_lift">Service/Goods Lift</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									
									
									<!-- <div class= "customDiv">column3</div>-->
									<?php
										if ($key == 'conroom' && $amevalu == '1') {
										?>   
										<li><span class="icon_amen icon_conroom">Conference Room</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'swimming' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_pool">Swimming Pool</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'atm' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_atm">ATM</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'ac' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_airc">Centrally Airconditioned</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'internet' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_internet">High Speed Internet</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'food' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_foodc">Cafeteria/Food court</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									
									
									<!-- <div class= "customDiv">column4</div>-->
									
									<?php
										if ($key == 'gym' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_gym">Gymnasium</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'water_softening_plant' && $amevalu == '1') {
										?>	
										<li><span class="icon_amen icon_water_plant">Water Softening Plant</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'community_hall' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_com_hall">Community Hall</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'shopping_center' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_shopping">Shopping Center</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									<?php
										if ($key == 'rainwater' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_rainwater">Rain water harvesting</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
									
									<?php
										if ($key == 'fire_alarm' && $amevalu == '1') {
										?>
										<li><span class="icon_amen icon_security">Security/Fire alarm</span></li>
										<?php
											echo "</div>";
											echo '<div class ="col-md-3">';
										}
									?>
								<?php } ?>
								
							</div>
						</div>
						
					</ul>
				</div>
				<!-- Amenities end -->
				<div id="" class="detail-map detail-block target-block col-md-12">
					<div class="detail-title">
						<h2 class="title-left">Location</h2>
					</div>
					<div class="inner-block">
						
						<div id="map_canvas" style="height:500px;border:0" ></div>
					</div>
				</div>
				<div id="calyourprice" class="detail-amenities detail-block target-block col-md-12">
					
					<div class="">
						
						<div class=" col-md-12">
							<div class="detail-title">
								<h2  class="title-left">Project Price Calculator</h2>
							</div>
						</div>
						
						<div id="amenities" class="detail-amenities detail-block target-block col-md-12" style="margin-top:0px;margin-bottom:0px;padding-top:0px;">
							
							<div class="row">
								<div class="col-sm-6">
									<label for="" class="form-group"><strong>Super Built up area : </strong> <?php echo $gSuperBuiltUpArea ?>&nbsp;sq ft </label>
								</div>
								<div class="col-sm-6">
									<label for="" class="form-group"><strong>Base Rate : </strong> <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $gBaseRate ?>&nbsp;/ sq ft</label>
								</div>
								
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<label for="" class="form-group"><strong>GST ( on Base ) : </strong> <?php echo $gServiceTaxOnBSP ?>&nbsp;&#37; </label>
								</div>
								
								<div class="col-sm-6">
									<label for="" class="form-group"><strong>GST ( on Others ) : </strong> <?php echo $gServiceTaxOnOTH ?>&nbsp;&#37; </label>
								</div>								
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<label for="" class="form-group"><strong>Registration Charges : </strong> 
										<?php
											if (2 == $gaRegistration[0][0]) {
												// Percentage
												echo $gaRegistration[0][1] . "&nbsp;&#37";
												} else {
												// Flat Charges
												echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaRegistration[0][1] . "&nbsp;/ sq ft";
											}
										?>
									</label>
								</div>
								
								<div class="col-sm-6">
									<label for="" class="form-group"><strong>Stamp Duty : </strong> 
										<?php
											if (2 == $gaStampDuty[0][0]) {
												// Percentage
												echo $gaStampDuty[0][1] . "&nbsp;&#37";
												} else {
												// Flat Charges
												echo "<i class='fa fa-inr' aria-hidden='true'></i>&nbsp;" . $gaStampDuty[0][1] . "&nbsp;/ sq ft";
											}
										?>
									</label>
								</div>								
							</div>
							
							<!-- If no plc charges ,  donot display the below div -->
							<div class="row" <?php if ($gaFloorPLC[0][1] <= 0) echo "style='display:none;'" ?> >
								<div class="col-sm-6">
									<div class="form-group">
										<label for="" class="label_title"><strong>Floor Choice </strong></label>
										<div class="select-style">
											<div class="input select">
												
												<select name="floor choice" id="id_floor_choice" onchange="ReCalculateOfferPrice()";>
													<option value="0" selected="selected" >Ground</option>
													<?php
														for ($opt = 1; $opt < $gaTotalFloors; $opt++) {
															echo '<option value=\'' . $opt . '\'>' . $opt . '</option>';
														}
													?>
												</select>
											</div>
										</div>	
									</div>	
								</div>
							</div>	
							
							
						</div>
						
					</div>
					
					
					
					
					<!--builder name -->
					
					
					
					
					
					
					<!-- builder name end-->
					
					
					<div class="col-md-6">
						
						<div class="detail-title">
							<h2 class="title-left"> Mandatory Charges</h2>
						</div> 
						
						<div class="form-group">
							<div class="row" id="id_mandatory_charges">
								
								<?php echo $gaMandatoryCharges; ?>
								
							</div>
						</div>
						
					</div>
					
					<div class="col-md-6">
						
						<div class="detail-title">
							<h2 class="title-left">Optional Charges</h2>
						</div> 
						
						<div class="form-group ">
							<div class="row" id="id_optional_charges">
								
								<?php echo $gaOptionalCharges; ?>
								
							</div>                          
							
						</div>
						
						
					</div>
					
					<div class="col-md-12 text-center">
						
						<div class="caltotprice">
							<div class="caltitle">Your Final Calculated Price</div>
							<span class="calprice" id="id_calprice"><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo round($gOfferPrice, 2); ?></span>
							<!-- <div class="caltitle">GST ( Others ) : <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $gServiceTax; ?> </div> -->
							<div id="id_chart"></div>
							<div id="container" style="width:100%; height:300px;" >
								
							</div>	
						</div>
						
					</div>
					
				</div>
			</div>	
			<div  class="detail-amenities detail-block target-block col-md-12">
				
				<div class="builder-view col-md-12">
					<div class="detail-title">
						
						<h2 class="title-left">Organization Name: <?php echo $Websiteuser['Websiteuser']['userorgname']; ?></h2>
					</div> 
					
					<div class="builder-item"><strong>Established year:</strong> <?php echo $builder['Builder']['borg_year']; ?></div>
					<div class="builder-item"><strong>About:</strong> <?php echo $builder['Builder']['borg_about']; ?></div>
					<?php if (!empty($pastprojectdata)) { ?>	
						<div class="builder-item"><strong>Past Projects: </strong><?php echo $pastprojectdata; ?></div>
					<?php } ?>
					
					<!-- <ul class="list-three-col list-features">
						
						<li><strong>About:</strong></li>
						<li><strong>Past Projects:</strong></li>
						
						
						
					</ul> -->
					
					
					
					
					
				</div>
			</div>		
		</div>
		
		<?php if ($propertydetails['Property']['user_id'] != $this->Session->read('Auth.Websiteuser.id')) { ?>
			<!-- sidebar -->
			<div id="sidebar" class="col-sm-3 nopaddingside detail_sidebar">
				<div class="col-sm-12  sidebar-inner">
					
					<form class="fpform">
						<div class="row ">
							<h3>Contact - Fairpockets</h3>
							<div class="col-sm-12 col-xs-12">
								<div class="form-group">
									
									<input id="user1" class="radio-custom" name="pet_all" type="radio" >
									<label for="user1" class="radio-custom-label">Individual</label>
									
									
									
									<input id="user2" class="radio-custom" name="pet_all" type="radio" >
									<label for="user2" class="radio-custom-label">Dealer</label>
									
									
								</div>
							</div>
							<div class="col-sm-12 col-xs-12">
								<div class="form-group">
									<input class="form-control error" placeholder="Your Name" type="text">
									<div class="error has-error" style="display:block;" >Box must be filled out</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-12">
								<div class="form-group">
									<input class="form-control" placeholder="Phone" type="text">
								</div>
							</div>
							<div class="col-sm-12 col-xs-12">
								<div class="form-group">
									<input class="form-control" placeholder="Email" type="email">
								</div>
							</div>
							<div class="col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="label_title">Interested in (Optional)</label>
									<div class="col-sm-12">
										<input id="interest1" class="checkbox-custom" name="interest[]" type="checkbox" >
										<label for="interest1" class="checkbox-custom-label">Assisted Search</label>
									</div>
									
									<div class="col-sm-12">
										<input id="interest2" class="checkbox-custom" name="interest[]" type="checkbox" >
										<label for="interest2" class="checkbox-custom-label">Immediate Purchase</label>
										
									</div>
									<div class="col-sm-12">
										<input id="interest3" class="checkbox-custom" name="interest[]" type="checkbox" >
										<label for="interest3" class="checkbox-custom-label">Portfolio Management</label>
										
									</div>
									
								</div>  
								
							</div>
							
							<div class="text-center col-sm-12 col-xs-12 ">
								<div class="form-group">
								<button class="btn btn-primary">Submit</button></div></div>
								
								<div class="text-center col-sm-12 col-xs-12 " style="padding:10px 0px;">
									<div class="form-group">
									I agree with Fairpockets <a href="#">T&C </a> </div></div>	
						</div>
						
						
					</form>
					
				</div>
			</div>
			<!-- sidebar end -->
		<?php } ?>
	</div>
</div>	

<!--add calculator-->

<!--add calculator end -->

<!-- similar property  -->

<div class="property-listing grid-view  col-md-12">
	<div class="detail-title">
		
		<h2 class="title-left">Similar properties</h2>
	</div>
	<div class="row">
		<div class="item-wrap">
			<div class="property-item table-list">
				<div class="table-cell">
					<div class="figure-block">
						<figure class="item-thumb">
							
							
							<div class="price hide-on-list">
								
								<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
								<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
							</div>
							<a href="#" class="hover-effect">
								<img src="<?php echo $this->webroot; ?>img/front//property/01_434x290.jpg" alt="thumb">
							</a>
							
						</figure>
					</div>
				</div>
				<div class="item-body table-cell">
					
					<div class="body-left table-cell">
						<div class="info-row">
							<div class="label-wrap hide-on-grid">
								<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span>
							</div>
							<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
							<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
							<div class="pro_tt">Residential Apartment for Sale</div>
							
							<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
						</div>
						
						
					</div>
					<div class="body-right table-cell hidden-gird-cell">
						<div class="info-row price">
							<p class="price-start">Start from</p>
							<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
							<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
						</div>
						<div class="info-row phone text-right">
							<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
							<p><a href="#">+1 (786) 225-0199</a></p>
						</div>
					</div>
					<div class="table-list full-width hide-on-list">
						<div class="cell">
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="item-foot date hide-on-list">
				<div class="item-foot-left">
					<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
				</div>
				<div class="item-foot-right">
					<p><i class="fa fa-calendar"></i> 12 Days ago </p>
				</div>
			</div>
		</div>
		
		<div class="item-wrap">
			<div class="property-item table-list">
				<div class="table-cell">
					<div class="figure-block">
						<figure class="item-thumb">
							
							
							<div class="price hide-on-list">
								
								<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
								<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
							</div>
							<a href="#" class="hover-effect">
								<img src="<?php echo $this->webroot; ?>img/front//property/01_434x290.jpg" alt="thumb">
							</a>
							
						</figure>
					</div>
				</div>
				<div class="item-body table-cell">
					
					<div class="body-left table-cell">
						<div class="info-row">
							<div class="label-wrap hide-on-grid">
								<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span>
							</div>
							<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
							<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
							<div class="pro_tt">Residential Apartment for Sale</div>
							
							<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
						</div>
						
						
					</div>
					<div class="body-right table-cell hidden-gird-cell">
						<div class="info-row price">
							<p class="price-start">Start from</p>
							<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
							<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
						</div>
						<div class="info-row phone text-right">
							<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
							<p><a href="#">+1 (786) 225-0199</a></p>
						</div>
					</div>
					<div class="table-list full-width hide-on-list">
						<div class="cell">
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="item-foot date hide-on-list">
				<div class="item-foot-left">
					<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
				</div>
				<div class="item-foot-right">
					<p><i class="fa fa-calendar"></i> 12 Days ago </p>
				</div>
			</div>
		</div>
		
		<div class="item-wrap">
			<div class="property-item table-list">
				<div class="table-cell">
					<div class="figure-block">
						<figure class="item-thumb">
							
							
							<div class="price hide-on-list">
								
								<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
								<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
							</div>
							<a href="#" class="hover-effect">
								<img src="<?php echo $this->webroot; ?>img/front//property/01_434x290.jpg" alt="thumb">
							</a>
							
						</figure>
					</div>
				</div>
				<div class="item-body table-cell">
					
					<div class="body-left table-cell">
						<div class="info-row">
							<div class="label-wrap hide-on-grid">
								<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span>
							</div>
							<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
							<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
							<div class="pro_tt">Residential Apartment for Sale</div>
							
							<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
						</div>
						
						
					</div>
					<div class="body-right table-cell hidden-gird-cell">
						<div class="info-row price">
							<p class="price-start">Start from</p>
							<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
							<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
						</div>
						<div class="info-row phone text-right">
							<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
							<p><a href="#">+1 (786) 225-0199</a></p>
						</div>
					</div>
					<div class="table-list full-width hide-on-list">
						<div class="cell">
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="item-foot date hide-on-list">
				<div class="item-foot-left">
					<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
				</div>
				<div class="item-foot-right">
					<p><i class="fa fa-calendar"></i> 12 Days ago </p>
				</div>
			</div>
		</div>
		
		<div class="item-wrap">
			<div class="property-item table-list">
				<div class="table-cell">
					<div class="figure-block">
						<figure class="item-thumb">
							
							
							<div class="price hide-on-list">
								
								<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
								<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
							</div>
							<a href="#" class="hover-effect">
								<img src="<?php echo $this->webroot; ?>img/front/property/01_434x290.jpg" alt="thumb">
							</a>
							
						</figure>
					</div>
				</div>
				<div class="item-body table-cell">
					
					<div class="body-left table-cell">
						<div class="info-row">
							<div class="label-wrap hide-on-grid">
								<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span>
							</div>
							<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
							<h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
							<div class="pro_tt">Residential Apartment for Sale</div>
							
							<div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
						</div>
						
						
					</div>
					<div class="body-right table-cell hidden-gird-cell">
						<div class="info-row price">
							<p class="price-start">Start from</p>
							<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
							<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
						</div>
						<div class="info-row phone text-right">
							<a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
							<p><a href="#">+1 (786) 225-0199</a></p>
						</div>
					</div>
					<div class="table-list full-width hide-on-list">
						<div class="cell">
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="item-foot date hide-on-list">
				<div class="item-foot-left">
					<p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
				</div>
				<div class="item-foot-right">
					<p><i class="fa fa-calendar"></i> 12 Days ago </p>
				</div>
			</div>
		</div>
		
		
		
		
	</div>
</div>

<script type="text/javascript">
    jssor_1_slider_init = function () {
		
        var jssor_1_SlideshowTransitions = [
		{$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
        ];
		
        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
			},
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
			},
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
			}
		};
		
        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
		
        /*responsive code begin*/
        /*you can remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
				} else {
                window.setTimeout(ScaleSlider, 30);
			}
		}
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		/*responsive code end*/
	};
</script>
<script type="text/javascript">jssor_1_slider_init();</script>

<?php
	// Generate client side GMap part
	
	echo "
	<input type='hidden' id='id_gLat' value='" . $gLat . "'>
	<input type='hidden' id='id_gLng' value='" . $gLng . "'>
	<input type='hidden' id='id_gTitle' value='" . $gTitle . "'>
	";
?>	

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<script type="text/javascript">
	
    var map;
    var lat = parseFloat(document.getElementById("id_gLat").value);
    var lng = parseFloat(document.getElementById("id_gLng").value);
    var tmp = document.getElementById("id_gTitle").value;
	
    var infowindow = new google.maps.InfoWindow({});
	
    function initialize()
    {
        geocoder = new google.maps.Geocoder();
		
        var latlng = new google.maps.LatLng(lat, lng);
		
        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Property Location"
		});
		
        marker['infowindow'] = tmp;
		
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
		});
		
	}
	
    window.onload = initialize;
	
</script>

<?php
	// Generate client side offer price calculator part
	
	echo "
	<input type='hidden' id='id_gProjectChargesAmt' value='" . $gProjectCharges_a . "'>
	";
	
	if (1 == $gaStampDuty[0][0]) {
		// Percentage
		echo "<input type='hidden' id='id_gaStampDuty' name='percent' value='" . $gaStampDuty[0][1] . "'>";
		} else {
		// Flat Charges
		echo "<input type='hidden' id='id_gaStampDuty' name='amount' value='" . $gaStampDuty[0][1] . "'>";
	}
	
	if (1 == $gaRegistration[0][0]) {
		// Percentage
		echo "<input type='hidden' id='id_gaRegistration' name='percent' value='" . $gaRegistration[0][1] . "'>";
		} else {
		// Flat Charges
		echo "<input type='hidden' id='id_gaRegistration' name='amount' value='" . $gaRegistration[0][1] . "'>";
	}
	
	
	echo "
	
	<script>
	
	BaseCharges = 0;
	ServiceTaxCharges = 0;
	GovtCharges = 0;
	ProjectCharges = 0;
	
	function ReCalculateOfferPrice( elem )
	{
	
	BaseCharges = 0;
	GovtCharges = 0;
	
	tBaseRate = " . $gBaseRate . ";
	tServiceTaxOnBSP = " . $gServiceTaxOnBSP . ";
	
	tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
	tServiceTaxOnOTH = " . $gServiceTaxOnOTH . ";
	
	eStampDuty = document.getElementById('id_gaStampDuty');
	eRegistration = document.getElementById('id_gaRegistration');
	
	tBSPForFloor = " . $gaFloorPLC[0][0] . ";
	tBSPForFloorCharges = " . $gaFloorPLC[0][1] . ";
	tBSPForFloorOption = " . $gaFloorPLC[0][2] . ";
	
	tOfferPriceAmt = tBaseRate * " . $gSuperBuiltUpArea . " ;
	
	tFloorPLC = 0;
	
	if ( tBSPForFloorCharges > 0 )
	{	
	tFloorPLC = tBSPForFloorCharges * " . $gSuperBuiltUpArea . " ;
	
	if ( tBSPForFloor == 2 )
	{
	// BSP w.r.t Ground floor
	tempFloor = parseInt(document.getElementById('id_floor_choice').value) ; //0 - Ground Floor
	tFloorPLC = tFloorPLC * tempFloor ;
	
	}
	else
	{
	// BSP w.r.t TOP floor;
	tempFloor = " . $gaTotalFloors . " - parseInt(document.getElementById('id_floor_choice').value) + 1; //0 - Ground Floor
	tFloorPLC = tFloorPLC * tempFloor ;
	}			
	
	if ( tBSPForFloorOption == 2 )
	{
	// sub option
	tFloorPLC = tFloorPLC*(-1);
	}	
	
	}
	
	nOfferPriceAmt = tOfferPriceAmt + tFloorPLC ;
	BaseCharges += nOfferPriceAmt;
	
	ServiceTaxCharges +=  (nOfferPriceAmt * ( tServiceTaxOnBSP / 100));
	nOfferPriceAmt = nOfferPriceAmt + ( nOfferPriceAmt * ( tServiceTaxOnBSP / 100) ) ;
	
	if ( elem )
	{	
	// Change in optional element
	
	if ( true == elem.checked )
	{	
	//alert('Checked  - '+elem.value);
	tProjectChargesAmt = tProjectChargesAmt + parseInt(elem.value);
	document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
	
	}	
	else
	{	
	//alert('Unchecked - '+elem.value);
	tProjectChargesAmt = tProjectChargesAmt - parseInt(elem.value);
	document.getElementById('id_gProjectChargesAmt').value = tProjectChargesAmt.toString();
	}
	}
	else
	{
	// Change in choice of floor
	
	tProjectChargesAmt = parseInt(document.getElementById('id_gProjectChargesAmt').value);
	} 					
	
	nServiceChargesAmt = tProjectChargesAmt * (tServiceTaxOnOTH/100) ; 
	nOfferPriceAmt = nOfferPriceAmt + tProjectChargesAmt + nServiceChargesAmt ; 	
	
	if ( 'amount' == eStampDuty.name )
	{
	GovtCharges += parseInt(eStampDuty.value);
	nOfferPriceAmt = nOfferPriceAmt + parseInt(eStampDuty.value);
	}
	else
	{
	GovtCharges +=  tProjectChargesAmt * (parseInt(eStampDuty.value)/100) ;
	nOfferPriceAmt = nOfferPriceAmt + ( tProjectChargesAmt * (parseInt(eStampDuty.value)/100) );
	}		
	
	if ( 'amount' == eRegistration.name )
	{
	GovtCharges +=  parseInt(eRegistration.value);
	nOfferPriceAmt = nOfferPriceAmt + parseInt(eRegistration.value);
	}
	else
	{
	GovtCharges +=  (tBaseCharge + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100);
	nOfferPriceAmt = nOfferPriceAmt + ((tBaseCharge + tProjectChargesAmt ) * (parseInt(eRegistration.value)/100) );
	}
	
	//alert('Final Offer Price ='+nOfferPriceAmt);
	
	
	document.getElementById('id_calprice').innerHTML = '<i class=\'fa fa-inr\' aria-hidden=\'true\'></i>&nbsp;'+nOfferPriceAmt.toFixed(2);
	
	//document.getElementById('id_chart').innerHTML ='BaseCharges = '+ BaseCharges.toFixed(2) + '<br>' +
	'ServiceTaxBSP = '+ (BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2) + '<br>' +
	'GovtCharges = '+ GovtCharges.toFixed(2) + '<br>' +
	'ProjectCharges = '+ tProjectChargesAmt.toFixed(2) + '<br>' +
	'ServiceTaxOTH = '+ (tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2);   
	
	$('#container').highcharts({
	
	chart: {
	type: 'pie',
	options3d: {
	enabled: true,
	alpha: 45
	}
	},
	title: {
	text: 'Property Charges'
	},
	tooltip: {
	//pointFormat: '{series.name}: <b>{point.value}%</b>'
	},
	plotOptions: {
	pie: {
	innerSize: 100,
	depth: 45
	}
	},
	series: [{
	data: [
	['Base Charge', eval(BaseCharges.toFixed(2)) ],
	['GST on BSP', eval((BaseCharges*(tServiceTaxOnBSP/100)).toFixed(2)) ],
	['Government Charges', eval( GovtCharges.toFixed(2) )],
	['Project Charges', eval(tProjectChargesAmt.toFixed(2)) ],
	['GST on Others', eval((tProjectChargesAmt*(tServiceTaxOnOTH/100)).toFixed(2))	 ]
	]
	
	}]
	});	
	
	//$('.place-desc-large').find(':first-child').first().text('new text');
	//$('.place-desc-large DIV:first-child').attr('html', 'abc');
	//$('.place-name' ).html( 'background-color');
	//$('.place-desc-large div.place-name').text('MY NEW TEXT');
	}
	
	ReCalculateOfferPrice();
	
	</script>
	
	";
?>	 

