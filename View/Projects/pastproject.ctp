        <div id="id_top" tabindex="-10"  class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h2>Post Your Project</h2>
            
        </div>	
<br>
        <div class="add_property_form">

            <?php
            echo $this->Form->create(
                    'Project', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_basic',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block">

                <div class="add-title-tab">
                    <h3>Project - Basic</h3>
                    <div class="add-expand active" id="id_proj_basic_exp"></div>
                </div>

					<div class="add-tab-content" id="id_proj_basic_content" style="display:block;">
						<div class="detail-block">	
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="project_name" class="label_title">Name of Project <span class="mand_field">*</span></label>
										<?php
										echo $this->Form->input(
												'project_name', array(
											'id' => 'project_name',
											'type' => 'text',
											'class' => 'form-control',
											'placeholder' => 'Project Name',
											'label' => false
												)
										);
										?>

									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="project_type" class="label_title">Project Type</label>
										<div class="select-style ">
											<?php
											echo $this->Form->input(
													'project_type', array(
												'id' => 'project_type',
												'options' => array(
													'1' => 'Residential',
													'2' => 'Commercial'
												),
												'class' => 'selectpicker',
												'label' => false
													)
											);
											?>

										</div>		
									</div>
								</div>
							</div>
						</div>
						<div class="detail-block">
			<div class="row">
								<div class="col-sm-4">
									<div class="form-group">

										<label for="city" class="label_title">City<span class="mand_field">*</span></label>

										<div class="select-style">
											<?php
											echo $this->Form->input(
													'project_city', array(
												'id' => 'id_select_city', 'options' => $city_master,
												'class' => 'selectpicker bs-select-hidden',
												'label' => false,
												'empty' => 'Select'
													)
											);
											?>
										</div>

									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label for="project_locality" class="label_title">Select Locality<span class="mand_field">*</span></label>
										<?php
										echo $this->Form->input(
												'project_locality', array(
											'id' => 'searchInput',
											'type' => 'text',
											'class' => 'form-control',
											'placeholder' => 'Locality',
											'label' => false
												)
										);
										?>

										<div id="result" class="input text gMapResults" style="display:none";>

										</div>


									</div>


								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label for="countryState" class="label_title">Address<span class="mand_field">*</span></label>
										<?php
										echo $this->Form->input(
												'project_address', array(
											'id' => 'address',
											'type' => 'text',
											'class' => 'form-control',
											'placeholder' => 'House No, Street No, etc.',
											'label' => false
												)
										);
										?>
									</div>
								</div>
							</div>	
							</div>
							<div class="detail-block">
						<div class="row">
								<div class="col-md-12">
									<div class="map" id="map" style="border-style: none;border:none;width:100%;"></div>                                
								</div>
							</div>
			</div>
			<div class="detail-block">
		<div class="row">
								<div class="col-sm-6">
                                <div class="form-group">
                                    <label for="project_highlights" class="label_title">Highlights of the Project <span class="mand_field">*</span></label>
                                    <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Please describe unique selling proposition of the Project"></i>
                                    <?php
                                    echo $this->Form->textarea(
                                            'project_highlights', array(
                                        'id' => 'project_highlights',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter your Project Highlights',
                                        'rows' => '5',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
								<div class="col-sm-6">
                                <div class="form-group">
                                    <label for="project_description" class="label_title">Description of Project <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->textarea(
                                            'project_description', array(
                                        'id' => 'project_description',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter your Project Description',
                                        'rows' => '5',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
							</div>
				</div>
				<div class="row">

								<input id="id_search_adm_area_1" class="form-controls" type="hidden" >
								<input id="id_search_adm_area_2" class="form-controls" type="hidden" >

								<!-- <div class="map" id="map" style="width: 100%; height: 300px;"></div> -->

								<div class="form_area">

									<?php echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
									<!-- Latitude -->
									<?php echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>
									<?php echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
									<?php echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
									<?php echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
									<?php echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
									<?php echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
									<?php echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
									<?php echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

								</div>
							</div>
                        </div>
                        
                        <div class="account-block text-center submit_area">
                            <div id='loadingmessage1' style='display:none'>
                                <img src='../img/ajax-loader.gif'/>
                            </div>									
							<button type="submit" id="id_next1" class="btn btn-primary btn-next ">NEXT</button>
                        </div>

                </div>

            </div>

            <?php
            echo $this->Form->end();
            ?>


            <?php
            echo $this->Form->create(
                    'ProjectDetail', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_feature',
                'novalidate' => 'novalidate'
                    )
            );
            ?>

            <div class="account-block property_feature">

                <div class="add-title-tab">
                    <h3>Project Details</h3>
                    <div class="add-expand" id="id_proj_detail_exp" ></div>
                </div>

                <div class="add-tab-content" id="id_proj_detail_content">
                    <div class="detail-block">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="web_link" class="label_title">Web Link</label>
                                    <?php
                                    echo $this->Form->input(
                                            'web_link', array(
                                        'id' => 'web_link',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'placeholder' => 'Web Link',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="num_of_tow" class="label_title"> Number Of Towers <span class="mand_field">*</span> </label>
                                    <div class="select-style">
                                        <?php
                                        echo $this->Form->input(
                                                'num_of_tow', array(
                                            'id' => 'num_of_tow',
                                            'options' => array(
                                                '1' => '1',
                                                '2' => '2',
                                                '3' => '3',
                                                '4' => '4',
                                                '5' => '5',
                                                '6' => '6',
                                                '7' => '7',
                                                '8' => '8',
                                                '9' => '9',
                                                '10' => '10',
                                                '11' => '11',
                                                '12' => '12',
                                                '13' => '13',
                                                '14' => '14',
                                                '15' => '15'),
                                            'empty' => 'Select Number of Towers',
                                            'class' => 'selectpicker',
                                            'label' => false
                                                )
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
							<div class="col-sm-4">
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <label for="land_area" class="label_title">Land area <span class="mand_field">*</span></label>
                                        </div>
                                        <div class="col-sm-12">
                                            <?php
                                            echo $this->Form->input(
                                                    'land_area', array(
                                                'id' => 'land_area',
                                                'type' => 'text',
                                                'class' => 'form-control parea_box',
                                                'placeholder' => 'Land Area',
                                                'label' => false
                                                    )
                                            );
                                            ?>
                                            <div class="parea_val"> Acres </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>		
                        <?php
                        echo $this->Form->input('project_id', array(
                            'class' => 'project_id',
                            'type' => 'hidden',
                            'label' => false
                                )
                        );
                        ?>
                        <div class="account-block text-center submit_area">
                            <div id='loadingmessage2' style='display:none'>
                                <img src='../img/ajax-loader.gif'/>
                            </div>									
							<button type="submit" id="id_next2" class="btn btn-primary btn-next ">NEXT</button>
                        </div>

                    </div>
                </div>

            </div>

            <?php
            echo $this->Form->end();
            ?>
            <?php
            echo $this->Form->create(
                    'Project', array(
                'class' => 'fpform',
                'role' => 'form',
                'id' => 'id_form_project_feature',
                'novalidate' => 'novalidate',
                'url' => 'projectimages'
                    )
            );
            ?>
            <?php
            echo $this->Form->input('project_id', array(
                'class' => 'project_id',
                'type' => 'hidden',
                'label' => false
                    )
            );
            ?>

            <?php
            echo $this->Form->input('project_type', array(
                'type' => 'hidden',
                'label' => false,
                'value' => '1'
                    )
            );
            ?>						
            <div class="account-block text-center submit_area">
                <button type="submit" id="id_submit_project" class="btn btn-primary btn-red">Submit Project</button>
            </div>
            <?php
            echo $this->Form->end();
            ?>	

        </div>		

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('fp-loc8.js');
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
$project_base_url = '/Projects/pastproject';
$project_detail_url = '/Projects/pastprojectdetail';
?>

<?php
echo $this->Html->scriptBlock("
		$(document).ready(function()
		{
			// Disable Project Detail Section
			$('#id_proj_detail_exp').css('display','none');
			
			// Disable submit button
			$('#id_submit_project').css('display','none');
			
		});
	");


// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("


		$.validator.addMethod('map_check', function(value, element) 
		{
			var ret_val = checkGmapErrors();
			return ret_val ;

		}, 'Please tick a locality on the map.');
	
		$.validator.setDefaults({ ignore: '' });


		$('#searchInput').on('input',function() {
			var validator = $( '#id_form_project_basic' ).validate();
			validator.element( '#searchInput' );
		
    		});
	
		$.validator.addMethod('nameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z ]+/.test(value);
		}, 'Please use English alphabets only.');
	
		$.validator.addMethod('pnameCustom', function(value, element) 
		{
			return this.optional(element) || /^[a-zA-Z0-9]+/.test(value);
		}, 'Please use Alphanumeric characters only.');	
	
		$.validator.addMethod('mobileCustom', function(value, element) 
		{
			return this.optional(element) || /^[0-9]+/.test(value);  
		}, 'Please use numerals only.');

                $.validator.addMethod('curl', function(value, element) 
		{
			return this.optional(element) || /^(?:(http|https)?:\/\/)?(?:[\w-]+\.)+([a-z]|[A-Z]|[0-9]){2,6}$/gi.test(value);
		}, 'URL format not correct.');
	
	
	
		$('#id_form_project_feature').validate({
		
			// Specify the validation rules
			rules: {
				'data[ProjectDetail][web_link]': {
					curl : true
				},
				'data[ProjectDetail][num_of_tow]': {
					required : true
				},
				'data[ProjectDetail][project_highlights]': {
					required : true
				},
				'data[ProjectDetail][land_area]': {
					required : true,
					number   : true,
					min      : 1,
					max      : 999999
				}
			},
		
			// Specify the validation error messages
			messages: {
				'data[ProjectDetail][num_of_tow]': {
					required : 'Select number of towers in the Project'
				},
				'data[ProjectDetail][project_highlights]': {
					required : 'Project Highlights is required.'
				},
				'data[ProjectDetail][land_area]': {
					required : 'Land Area is required.',
					number   : 'Only Numeric digits allowed.',
					min      : 'Min value allowed is 1',
			        max      : 'Max value allowed is 999999'
				}
			},
			
			submitHandler: function(form){

                            $('#loadingmessage2').show();
			
				$.ajax
			({
				type: 'POST',
				url: '$project_detail_url',
				data: $('#id_form_project_feature').serialize(), // serializes the form's elements.
                                complete: function(status)
				{
                                    $('#loadingmessage2').hide();
                                },
				success: function(data)
				{
					// TODO : Ajax call
				
				// Changes of Page when Ajax call is successful
				
				// 1. Change NEXT -> SAVED
				$('#id_next2').html('Saved');
				
				// 1. Change NEXT button style
				$('#id_next2').css('background','#ffc000');
				$('#id_next2').css('color','#29333D');
				
				// 3. Freeze the form content
				$('#id_form_project_feature :input').prop('disabled', true);
				
                                // 4. Close current block
				$('#id_proj_detail_exp').removeClass('active');
				$('#id_proj_detail_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();

				// 5. Show Submit button
				$('#id_submit_project').css('display','inline');
				
				// To remain on same page after submit
				  
				}
		});
				
				// Validations OK. Form can be submitted
				
				
				return false;
				
			}
			
		});

		$('#id_form_project_basic').validate({
		
			// Specify the validation rules
			rules: {
				'data[Project][project_name]': {
					required : true,
					pnameCustom : true
				},
				'data[Project][project_highlights]': {
					required : true
				},
				'data[Project][project_description]': {
					required : true
				},
				'data[Project][project_address]': {
					required : true,
					map_check : true
				}
			},
		
			// Specify the validation error messages
			messages: {
				'data[Project][project_name]': {
					required : 'Project Name is required.'
				},
				'data[Project][project_highlights]': {
					required : 'Project Highlights is required.'
				},
				'data[Project][project_description]': {
					required : 'Project Description is required.'
				},
				'data[Project][project_address]': {
					required : 'Project address is required.'
				}
			},
			
			submitHandler: function(form){


				$('#loadingmessage1').show();
			
				$.ajax
			({
				type: 'POST',
				url: '$project_base_url',
				data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
				complete: function(status)
				{
				    $('#loadingmessage1').hide();
                                },
				error: function(xhr, textStatus, errorThrown) {
     					alert('Error recived');
  				},
				success: function(data)
				{

				$('.project_id').val(data);
				
				// 1. Change NEXT -> SAVED
				$('#id_next1').html('Saved');
				
				// 1. Change NEXT button style
				$('#id_next1').css('background','#ffc000');
				$('#id_next1').css('color','#333');
				
				// 3. Freeze the form content
				$('#id_form_project_basic :input').prop('disabled', true);
				
				// 4. Expand the next block
				$('#id_proj_detail_exp').css('display','block');
				$('#id_proj_detail_exp').addClass('active');
				$('#id_proj_detail_content').css('display','block');

                                // 5. Close current block
				$('#id_proj_basic_exp').removeClass('active');
				$('#id_proj_basic_content').css('display','none');
				$('#id_top').attr('tabindex',-10).focus();
				  
				}
		});
		
				
				// Validations OK. Form can be submitted
				
				// TODO : Ajax call
				
				// Changes of Page when Ajax call is successful
				
				
			
				// To remain on same page after submit
				return false;
			}

			
		});




		
	");
?>




<script>
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script type="text/javascript">

    function gMapOptionClick(elem)
    {
		document.getElementById('searchInput').value = elem.innerHTML;
        document.getElementById("result").style.display = 'none';

        //Generate click event
        document.getElementById('searchInput').click();
    }



    $(document).ready(function () {
        /*   $('#myModal').modal('hide');
         var options = [];*/

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                    val = $target.attr('data-value'),
                    $inp = $target.find('input'),
                    idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });

        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });
    });



</script>


