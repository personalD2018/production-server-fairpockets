     <div class="page-content text-center">
		   <div class="row user-responce">
              <div class="col-md-12">
                <h1 style="text-align:center;">Updates</h1>
              </div>
            <div class="col-md-4 col-sm-4 list">
            	<h3>Project Listing</h3>
                <div class="count"><?php echo $totalProjects; ?></div>
            </div>
            <div class="col-md-4 col-sm-4 list">
            	<h3>Property Listing</h3>
                <div class="count"><?php echo $totalProperty; ?></div>
            </div>
            <div class="col-md-4 col-sm-4 list">
            	<h3>View Response</h3>
                <div class="count"><?php echo $totalresponses; ?></div>
            </div>
          </div>
          
          <div class="space-x"></div>
		  <?php if ($this->Session->read('Auth.Websiteuser.userrole') != '2') { ?>
			  <div class="row user-request-services" style="display:none;">
				<div class="col-md-12">
					<h1>Request Services</h1>
				</div>
				<div class="col-md-6 col-sm-6 list">
					<img src="/img/about-research-report.png">
					<h3>Assisted Buy/Rent</h3>
					<p>Buy or Rent out your home at the right price and fastest possible time.</p>
					<p><a href="/accounts/AsstBuyRent_AddRequirement" class="button btn-add pd10">Click Here</a></p>
				</div>
				<div class="col-md-6 col-sm-6 list">
					<img src="/img/about-our-research-focus.png">
					<h3>Assisted Sell/Rent</h3>
					<p>Sell or get a tenant at the right price and fastest possible time.</p>
					<p><a href="/accounts/asstSellRent_servicerequest" class="button btn-add pd10">Click Here</a></p>
				</div>
				<div class="clearfix"></div>
				<div class="space-x"></div>
				<div class="col-md-6 col-sm-6 list">
					<img src="/img/about-property-management.png">
					<h3>Property Management</h3>
					<p>Get a local caretaker to manage your property by keeping it well managed through our services.</p>
					<p><a href="/accounts/Property_AddProperty" class="button btn-add pd10">Click Here</a></p>
				</div>
				<div class="col-md-6 col-sm-6 list">
					<img src="/img/about-portfolio-management.png">
					<h3>Portfolio Management</h3>
					<p>We will optimize your current property portfolio and help you decide whether to sell or hold or reinvest to grow the pie</p>
					<p><a href="/accounts/portfolio_addProperty" class="button btn-add pd10">Click Here</a></p>
				</div>
			  </div>
			<?php } ?>
      </div>
	<div class="space-m"></div>
