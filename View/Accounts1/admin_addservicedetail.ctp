<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) > Number($(params).val()));
                }, 'Must be greater than Launch date.');

        jQuery("#formID").validate();



    });
    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
        $('#datetimepicker5').datetimepicker({
            pickTime: false
        });
    });





</script>	

<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Add Property Service Details</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>FP Property Service</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('PropServicestage', array('id' => "formID", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>



                        <div class="control-group">
                            <label class="control-label">Date</label>
                            <div class="controls">
                                <div id="datetimepicker4" class="input-append">
                                    <?php echo $this->Form->input('date', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'project_status', 'options' => array('1' => 'Active', '2' => 'Closed'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Remarks</label>
                            <div class="controls">
                                <?php echo $this->Form->input('remarks', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Add Remarks', 'class' => 'small required')); ?>
                            </div>
                        </div>



                        <div class="control-group">
                            <label class="control-label">Upload Photos</label>
                            <div class="controls">
                                <?php echo $this->Form->input('servicepics.', array('type' => 'file', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Upload Pics', 'class' => 'small required', 'multiple' => 'multiple')); ?>

                                <div class="uploading none">
                                    <label>&nbsp;</label>

                                </div>

                            </div>
                        </div>




                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Add</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               