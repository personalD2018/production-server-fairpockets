<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Research Reports</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>View Research Reports</h3>
                </div>
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>
                                <th class="center">Project Name</th>
                                <th class="center">Type of Project</th>
                                <th class="center">Plot Area</th>
                                <th class="center">Launch Date</th>
                                <th class="center">Possession Date</th>
                                <th class="center">
                                    Project Status
                                </th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($researchreport as $data) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $data['ResearchReport']['report_name']; ?></td>		  

                                    <td class="center"><?php
                                        if ($data['ResearchReport']['type_of_project'] == 1) {
                                            echo "Residential";
                                        }
                                        if ($data['ResearchReport']['type_of_project'] == 2) {
                                            echo "Commercial";
                                        }
                                        ?></td>
                                    <td class="center"><?php echo $data['ResearchReport']['plot_area']; ?></td>
                                    <td class="center"><?php echo $data['ResearchReport']['doj1']; ?></td>

                                    <td class="center"><?php echo $data['ResearchReport']['doj2']; ?></td>
                                    <td class="center"><?php
                                        if ($data['ResearchReport']['project_status'] == 1) {
                                            echo "Ready To Move";
                                        }
                                        if ($data['ResearchReport']['project_status'] == 2) {
                                            echo "Under Construction";
                                        }
                                        ?></td>


                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <a href="<?php echo $this->webroot; ?>admin/accounts/editResearchReport/<?php echo $data['ResearchReport']['id']; ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>
                                                <a href="<?php echo $this->webroot; ?>accounts/research_detail/<?php echo $data['ResearchReport']['id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail" onclick="window.location.href = '<?php echo $this->webroot; ?>admin/accounts/research_detail/<?php echo $data['ResearchReport']['id']; ?>'"><i class="icon-eye-open"></i></button></a>


                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


