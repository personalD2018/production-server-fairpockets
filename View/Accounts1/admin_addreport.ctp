<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) > Number($(params).val()));
                }, 'Must be greater than Launch date.');

        jQuery("#formID").validate(
                {

                    rules: {

                        'data[ResearchReport][report_price]': {
                            required: true,
                            number: true,
                            min: 0
                        },
                        'data[ResearchReport][doj2]': {
                            greaterThan: '#ResearchReportDoj1'
                        },
                    },
                    messages: {
                        'data[ResearchReport][report_price]': {
                            required: 'Report Price is required.',
                            number: 'Only Numeric Digits allowed.',
                            min: 'Min value allowed is 0s',

                        }
                    }


                }
        );

        $('#add_exercise').on('click', function () {
            $('#exercises').append('<div class="exercise">      <input type="text" name="unittype[]" placeholder="Please Enter Unit Type"><input type="text" name="totalunit[]" placeholder="Please Enter TotalUnit"><input type="text" name="areabaseprice[]" placeholder="Please Enter area Base Price"><button class="remove">x</button></div>');
            return false; //prevent form submission
        });

        $('#exercises').on('click', '.remove', function () {
            $(this).parent().remove();
            return false; //prevent form submission
        });

    });
    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
        $('#datetimepicker5').datetimepicker({
            pickTime: false
        });
    });





</script>	

<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Add Report</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>FP Research Reports</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('ResearchReport', array('id' => "formID", 'class' => 'form-horizontal left-align', 'enctype' => "multipart/form-data")); ?>
                        <div class="control-group">
                            <label class="control-label">Project  Name</label>
                            <div class="controls">
                                <?php echo $this->Form->input('report_name', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Report Name', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Type of Project</label>
                            <div class="controls">
                                <?php echo $this->Form->input('type_of_project', array('id' => 'type_of_project', 'options' => array('1' => 'Residential', '2' => 'Commercial'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Type Project', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Plot Area</label>
                            <div class="controls">
                                <?php echo $this->Form->input('plot_area', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Plot Area', 'class' => 'small required')); ?> 

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">No. of Floors</label>
                            <div class="controls">
                                <?php echo $this->Form->input('no_of_floors', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'title' => 'Please Enter No. of Floors', 'class' => 'small required')); ?>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Launch Date</label>
                            <div class="controls">
                                <div id="datetimepicker4" class="input-append">
                                    <?php echo $this->Form->input('doj1', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Possession Date</label>
                            <div class="controls">
                                <div id="datetimepicker5" class="input-append">
                                    <?php echo $this->Form->input('doj2', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Land Holding</label>
                            <div class="controls">
                                <?php echo $this->Form->input('land_holding', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Land holding', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Project Status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('project_status', array('id' => 'project_status', 'options' => array('1' => ' Under Construction', '2' => 'Ready to move'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Project Status', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">No. of Towers</label>
                            <div class="controls">
                                <?php echo $this->Form->input('no_of_towers', array('id' => 'no_of_towers', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Unit Sizes</label>
                            <div class="controls">
                                <?php echo $this->Form->input('unit_size', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Unit Size', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">No. of Units</label>
                            <div class="controls">
                                <?php echo $this->Form->input('no_of_unit', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter no.of Units', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Area Livability</label>
                            <div class="controls">
                                <?php echo $this->Form->input('area_livability', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Area Livability', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Density/Acre</label>
                            <div class="controls">
                                <?php echo $this->Form->input('dentisy', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Density/Acre', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Carpet Area</label>
                            <div class="controls">
                                <?php echo $this->Form->input('carpet_area', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Carpet Area', 'class' => 'small required')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">About Project</label>
                            <div class="controls">
                                <?php echo $this->Form->input('about_project', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'About Project', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">About Locality</label>
                            <div class="controls">
                                <?php echo $this->Form->input('about_locality', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'About Locality', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">About Builder</label>
                            <div class="controls">
                                <?php echo $this->Form->input('about_builder', array('type' => 'textarea', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'About Builder', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">City</label>
                            <div class="controls">
                                <?php
                                echo $this->Form->input(
                                        'city', array(
                                    'id' => 'id_select_city', 'options' => $city,
                                    'label' => false,
                                    'empty' => 'Select'
                                        )
                                );
                                ?>		
                            </div>
                        </div>


                        <div class="control-group">

                            <div class="controls">
                                <input id="id_search_adm_area_1" class="form-controls" type="hidden" >
                                <input id="id_search_adm_area_2" class="form-controls" type="hidden" >

                                <input id="searchInput" class="input-controls" type="text" type="hidden" > 
                                <div class="map" id="map" style="width: 100%; height: 300px;"></div> 

                                <div class="form_area">

                                    <?php echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
                                    <!-- Latitude -->
                                    <?php echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>
                                    <?php echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

                                </div>

                            </div>

                        </div>	

                        <div class="control-group">
                            <label class="control-label">Unit</label>
                            <div class="controls">
                                <fieldset id="exercises">
                                    <div class="exercise">
                                        <input type="text" name="unittype[]" class="required" placeholder="Please Enter Unit Type"><input type="text" name="totalunit[]" class="required" placeholder="Please Enter TotalUnit"><input type="text" class="required" name="areabaseprice[]" placeholder="Please Enter area Base Price">
                                    </div>
                                </fieldset>
                                <button id="add_exercise">Add Unit Types</button>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Upload Report</label>
                            <div class="controls">
                                <?php echo $this->Form->input('reportpdf', array('type' => 'file', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Upload Report', 'class' => 'small required')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Report Price</label>
                            <div class="controls">
                                <?php echo $this->Form->input('report_price', array('type' => 'text', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'report price', 'class' => 'small required')); ?>
                            </div>
                        </div>						

                        <div class="control-group">
                            <label class="control-label">Upload Photos</label>
                            <div class="controls">
                                <?php echo $this->Form->input('reportpics.', array('type' => 'file', 'div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Upload Pics', 'class' => 'small required', 'multiple' => 'multiple')); ?>

                                <div class="uploading none">
                                    <label>&nbsp;</label>

                                </div>

                            </div>
                        </div>




                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Add</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               