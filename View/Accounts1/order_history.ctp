    <div class="page-content">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Order History</h1>
                </div>
            </div>

            <div id="build" class="list-detail detail-block target-block">

                <table width="100%" class="table table-striped table-responsive">

                    <thead>
                        <tr style="background-color:#29333d;color:#fff;">

                            <th>Order ID</th>
                            <th>Service</th>
                            <th>Started on</th>
                            <th>Service End</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        foreach ($order as $order_history) {

                            if ($order_history['OrderHistory']['status'] == '3') {
                                $status = "Approved";
                            } else if ($order_history['OrderHistory']['status'] == '4') {
                                $status = "Rejected";
                            } else if ($order_history['OrderHistory']['status'] == '5') {
                                $status = "Closed";
                            } else if ($order_history['OrderHistory']['status'] == '6') {
                                $status = "Payment Pending";
                            } else {
                                $status = "Under Approval";
                            }
                            ?>
                            <tr>
                                <td>#<?php echo $order_history['OrderHistory']['order_id']; ?></td>
                                <td><?php echo $order_history['OrderHistory']['service_name']; ?></td>
                                <td><?php echo $order_history['OrderHistory']['service_start']; ?></td>
                                <td><?php echo $order_history['OrderHistory']['service_end']; ?></td>
                                <td><?php echo $order_history['OrderHistory']['Amount']; ?></td>

                                <td><?php echo $status; ?></td>
                            </tr>
                        <?php } ?>

                    </tbody>

                </table>

            </div>	

        </div>


