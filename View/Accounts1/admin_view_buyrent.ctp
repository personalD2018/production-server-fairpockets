<style>
    .controls{
        margin-top: 5px;
    }
</style>
<?php
if ($data['BuyRentRequirement']['prop_type'] == '1') {
    $pro_type = "Residential";
} else {
    $pro_type = "Commercial";
}
if ($data['BuyRentRequirement']['prop_reqt'] == '1') {
    $prop_reqt = "Rent";
} else {
    $prop_reqt = "Buy";
}
?>	
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Buy/Rent Service</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>View Buy/Rent Service</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('BuyRentRequirement', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">Requirement</label>
                            <div class="controls">
                                <?php echo $prop_reqt; ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Property Type</label>
                            <div class="controls">
                                <?php echo $pro_type; ?>
                            </div>
                        </div>



                        <div class="control-group">
                            <label class="control-label">Completion status</label>
                            <div class="controls">
                                <?php
                                if ($data['BuyRentRequirement']['prop_cstat'] == '1') {
                                    echo $prop_reqt = "Ready to move in";
                                } else {
                                    echo $prop_reqt = "Under Construction";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Purpose</label>
                            <div class="controls">
                                <?php
                                if ($data['BuyRentRequirement']['prop_purpose'] == '1') {
                                    echo $prop_purpose = "End Of Use";
                                } else {
                                    echo $prop_purpose = "Investment";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">State</label>
                            <div class="controls">
                                <?php echo (isset($data['MstAreaState'])) ? $data['MstAreaState']['stateName'] : 'NA'; ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">City </label>
                            <div class="controls">
                                <?php echo (isset($data['MstAreaCity'])) ? $data['MstAreaCity']['cityName'] : 'NA'; ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Locality</label>
                            <div class="controls">
                                <?php echo $data['BuyRentRequirement']['prop_locality']; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Budget</label>
                            <div class="controls">
                                <?php echo $data['BuyRentRequirement']['prop_budget']; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Have you seen any property ? </label>
                            <div class="controls">
                                <?php echo $data['BuyRentRequirement']['prop_oth']; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Remarks </label>
                            <div class="controls">
                                <?php echo $data['BuyRentRequirement']['prop_rmks']; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Status </label>
                            <div class="controls">
                                <?php echo $data['BuyRentRequirement']['prop_rmks']; ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Comment </label>
                            <div class="controls">
                                <?php echo $this->Form->input('comment', array('type' => 'textarea', 'class' => 'small required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter About')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Status </label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'role', 'options' => array('3' => 'Active', '4' => 'Rejected', '5' => 'Close'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Status', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update</button>

                        </div>
                        </form>

                    </div>				               
                </div>
            </div>
        </div>
    </div>
</div>