<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate(
                {
                    rules: {
                        "data[PortService][payment]": {
                            required: true,
                            number: true,
                            min: 1
                        },
                        messages: {

                            "data[PropService][payment]": {

                                number: "Enter Only Numeric",
                                min: "Value must be greater than 0",

                            }
                        }


                    }
                }
        )
    });

    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
    });
</script>	

<script>
    $(document).ready(function () {
<?php
if ($PropService['PropService']['status'] == '2') {
    ?>
            $("#rejection_hide").hide();
<?php }
?>

<?php
if ($PropService['PropService']['status'] == '4') {
    ?>
            $("#payment_hide").hide();
<?php }
?>
        $("#status").on('change', function () {
            var id = $(this).val();

            if (id == '2') {
                $("#rejection_hide").hide();
            } else
            {
                $("#rejection_hide").show();
            }
            if (id == '4') {
                $("#payment_hide").hide();
            } else
            {
                $("#payment_hide").show();
            }

        });


    });
</script>


<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Property Services</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Update FP Property Services</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('PropService', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>
                        <div class="control-group">
                            <label class="control-label">Service Name</label>
                            <div class="controls">
                                <?php echo $PropService['PropService']['prop_sname']; ?>
                            </div>
                        </div>
                        <div id="rejection_hide">
                            <div class="control-group">
                                <label class="control-label">Payment</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('payment', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'class' => 'span6 required')); ?>
                                </div>
                            </div>

                            <div id="payment_hide">



                                <div class="control-group">
                                    <label class="control-label">Created On</label>
                                    <div class="controls">
                                        <div id="datetimepicker4" class="input-append">
                                            <?php echo $this->Form->input('Created_on', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please enter Date of Creation', 'class' => 'datepicker small required span12', 'readonly' => 'true')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Service status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'status', 'options' => array('1' => 'Approved', '2' => 'Rejected', '3' => 'Closed', '4' => 'Payment Pending'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>					














