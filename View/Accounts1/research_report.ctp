<?php echo $this->Html->css('home/report_detail');
?>
<section class="research-reports-top inside-pages">
        <div class="container">              
            <div class="space-xxl"></div>
            <h1 class="text-center">Know Your Property Before You Invest</h1>
            <form class="search" id="search" method="post"  action="<?php echo $this->webroot; ?>accounts/research_report" >
                <div class="row">
					<div class="col-md-8 col-sm-12 col-md-offset-2 col-sm-offset-0">
						<div class="row">
							<div class="form-group col-md-3 col-sm-3">
								<?php
									echo $this->Form->input('city', array('id' => 'city', 'class' => 'selectpicker show-tick form-control', 'options' => $city, 'label' => false, 'div' => false));
								?>
							</div>
						
							<div class="form-group col-md-7 col-sm-6">
								<input name="proname" value="<?php echo @$this->request->data ['proname']; ?>" placeholder="Search for Property" type="text" class="form-control">
							</div>
					   
							<div class="form-group col-md-2 col-sm-3">
								<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search </button>  
							</div>
						</div>
					</div>
                </div>
            </form>
			<div class="clearfix"></div>
            <div class="space-m"></div>
            <div class="row">
			<div class="col-md-12">
				<h2 class="text-center">Some important datapoints of our reports</h2>
				<ul class="reports text-center">
					<li><img src="<?php echo $this->webroot; ?>img/search_icon1.png" alt="Appreciation Potential"> Appreciation Potential</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon2.png" alt="Expected Target Price"> Expected Target Price</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon3.png" alt="Value for Money"> Value for Money</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon4.png" alt="Rental Return"> Rental Return</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon5.png" alt="Market Liquidity"> Market Liquidity</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon6.png" alt="Locality Review"> Locality Review</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon5.png" alt="Builder Trackrecord"> Builder Trackrecord</li>
					<li><img src="<?php echo $this->webroot; ?>img/search_icon6.png" alt="Competition Projects"> Competition Projects</li>
				</ul>            
				<div class="link"><a href="<?php echo $this->webroot; ?>samplereport.pdf" class="drawer-menu-item button" target="_blank">Download Sample Report</a></div>
			</div>
			</div>
        </div>
    <div class="space-l"></div>
</section>
<?php if(!empty($searchreportdata)):?>
<div class="property-listing grid-view">
<div class="space-l"></div>
  <div class="container">
    <div class="wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
		<h1>Sample Reports </h1>	  
    </div>
    <div class="row reports">
	<?php foreach($searchreportdata as $reportdata){ ?>
      <div class="col-md-3">
        <div class="property-item table-list well">
          <div class="table-cell">
            <div class="figure-block">
              <figure class="item-thumb"> <a href="<?php echo $this->webroot;?>accounts/research_detail/<?php echo $reportdata['ResearchReport']['id'];?>" class="hover-effect">
					<?php if(!empty($reportdata['ReasearchreportPic']['pic']))
					{
					?>
			  <img src="<?php echo $this->webroot.$reportdata['ReasearchreportPic']['pic'];?>">		<?php }
			else
			{?>
			 <img src="<?php echo $this->webroot;?>img/01_434x290.jpg">
			<?php }
			?>
			  </a> </figure>
            </div>
          </div>
          <div class="item-body table-cell">
            <div class="body-left table-cell">
              <div class="info-row">
                <h2 class="property-title"><a href="<?php echo $this->webroot;?>accounts/research_detail/<?php echo $reportdata['ResearchReport']['id'];?>"><?php echo $reportdata['ResearchReport']['report_name'];?></a></h2>
              
                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $reportdata['ResearchReport']['locality'];?>, <?php echo $reportdata['ResearchReport']['city_data'];?></h4>
                <div class="pro_tt"><b><?php echo $reportdata['ResearchReport']['plot_area'];?> acres</b></div>
              </div>
            </div>
          </div>
        </div>
      </div>
	<?php } ?>
    </div>
  </div>
  <div class="space-l"></div>
</div>
<!--/#services-->
<?php endif; ?>
<div id="services" class="service-item">
    <div class="space-s"></div>
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
            <h1>Our Research Focus</h1>
            <h3>Three pillars of our research report are Project, Builder and Location</h3>
        </div>
        <div class="row text-center">
            <div class="col-md-4 col-xs-12">
                <div class="media services-wrap wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <div class="text-center my_details"> <img class="img-ico" src="<?php echo $this->webroot; ?>img/pt_1.png" alt="Project Detail"> </div>
                    <div class="media-bodys">
                        <h3 class="media-heading">Project Detail	
                        </h3>
                        <p class="more">We track relevant metrics like area livability, carpet area efficiency, unit density, appreciation potential, estimated target price, market liquidity, project proximity to corporate hub, shopping areas, hospitals, rail/metro stations, comparison with other nearby projects etc.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="media services-wrap wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <div class="text-center my_details"> <img class="img-ico" src="<?php echo $this->webroot; ?>img/pt_2.png" alt="Builder Detail"> </div>
                    <div class="media-bodys">
                        <h3 class="media-heading">Builder Detail</h3>
                        <p class="more">We cover track record of developer, performance of their past project, the architect they appoint, current project quality etc. All these facts reveal the intentions and quality policy of the builder.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="media services-wrap wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <div class="text-center my_details"> <img class="img-ico" src="<?php echo $this->webroot; ?>img/pt_3.png" alt="Location Insight"> </div>
                    <div class="media-bodys">
                        <h3 class="media-heading">Location Insight
                        </h3>
                        <p class="more">Choosing right city and right locality can make you richer by over 100% in a short span of few years. Spending more and more time in researching about location is the key to successful investing in real estate. Our reports elaborately describe details of location for better decision making.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space-m"></div>
</div>
