<?php
if ($this->Session->read('Auth.Websiteuser.userrole') == 1) {
    $user_role = 'Individual';
}
if ($this->Session->read('Auth.Websiteuser.userrole') == 2) {
    $user_role = 'Agent';
}
if ($this->Session->read('Auth.Websiteuser.userrole') == 3) {
    $user_role = 'Builder';
}

echo $this->Html->script('validate_1.9_jquery.validate.min');
?>  
<!--./advertisement-Card-->
    <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View/Edit Profile (<?php echo $user_role; ?>) </h1>
                </div>
                <!--/.col-lg-12 -->
            </div>
            	<?php echo $this->Session->flash(); ?> 
			<div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo $this->Form->create(
                            'Websiteuser', array(
                        'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'register_form',
                        'novalidate' => 'novalidate',
						'enctype' => 'multipart/form-data'
                            )
                    );
                    ?>


                    <div class="account-block">

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">

                                    <label for="username" class="label_title">Name <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'username', array(
                                        'id' => 'username',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                            <?php
                            if ($this->Session->read('Auth.Websiteuser.userrole') == '2' || $this->Session->read('Auth.Websiteuser.userrole') == '3') {
                                ?>
                                <div class="col-sm-4">
                                    <div class="form-group">

                                        <label for="userorgname" class="label_title">Organization <span class="mand_field">*</span></label>
                                        <?php
                                        echo $this->Form->input(
                                                'userorgname', array(
                                            'id' => 'userorgname',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-sm-4">
                                <div class="form-group">

                                    <label for="email" class="label_title">Email <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'email', array(
                                        'id' => 'email',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">

                                    <label for="usermobile" class="label_title">Mobile<span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'usermobile', array(
                                        'id' => 'usermobile',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
							
							<div class="col-sm-8">
                                <div class="form-group">
                                    <label for="userlogo" class="label_title">Logo<span class="mand_field">*</span></label>
                                    <div style="width:200px;">
										<?php
										echo $this->Form->input(
												'borg_logo', array(
											'id' => 'borg_logo',
											'type' => 'file',
											'class' => 'form-control',
											'label' => false,
											'style' => 'float:left;'
												)
										);
										?>
									</div>
									<?php if($borg_logo!=''){
										$builder_logo = $borg_logo;
										}else{
											$builder_logo = 'logo-default.png';
										} ?>
									<img src="<?php echo $this->webroot; ?>upload/builder_logo/<?php echo $builder_logo; ?>" style="width:200px;">
                                                    
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="account-block  submit_area">

                                    <button type="submit" class="btn btn-primary" >Update</button>

                                </div>
                            </div>

                        </div>

                    </div>

                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>



                </div>
            </div>



            <div class="clear"></div>



        </div>

		<?= $this->Html->script('profile', array('block' => 'scriptBottom'));?>
