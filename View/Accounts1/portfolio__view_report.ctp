<!--./advertisement-Card-->
    <div class="page-content">

        <?php
        $paginator = $this->Paginator;
        ?>
            <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Portfolio Management - My Reports</h1>
                </div>
                <div class="col-md-3">
                    <div class="user_panel_search">
                            <form class="search" id="search" method="post"  action="Portfolio_ViewReport" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                           </form>
                       </div>
                   </div>
               </div>
			<div class="space-x"></div>	
			<!--            
			<div class="user_panel_search">
                <form class="search" id="search" method="post" action="Portfolio_ViewReport">
                    <input type="text" name="search" placeholder="Search..">
                    <a onclick="document.getElementById('search').submit()" ><i class="fa fa-search fa-lg" aria-hidden="true" style="color:#000;"></i></a>
                </form>
            </div>-->
            <?php
            if (!empty($requirement)) {

                foreach ($requirement as $data) {

                    if ($data['PortService']['status'] == '1') {
                        $payment = "Approved";
                    } else if ($data['PortService']['status'] == '2') {
                        $payment = "Rejected";
                    } else if ($data['PortService']['status'] == '3') {
                        $payment = "Closed";
                    } else if ($data['PortService']['status'] == '4') {
                        $payment = "Payment Pending";
                    } else {
                        $payment = "Under Approval";
                    }
                    ?>
                    <div id="build" class="list-detail detail-block target-block">

                        <div class="detail-title">
                            <a href="#">Portfolio <?php echo $data['PortService']['port_name']; ?> Summary (<?php echo $payment; ?>)</a>
                        </div>


                        <div class="row">
                            <?php
                            if (!empty($data['PortService']['service_start'])) {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service started on: </strong><?php echo $data['PortService']['service_start']; ?> </div>
                                </div>
                                <?php if (!empty($data['PortService']['service_end'])) {
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-control" ><strong>Service closed: </strong><?php echo $data['PortService']['service_end']; ?>  </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-control" ><strong>Service expiry on: </strong><?php echo date($data['PortService']['service_end'], strtotime('+3 month')); ?>  </div>
                                    </div>

                                    <?php
                                }
                            } else {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service Posted on: </strong><?php
                                        $old_date_timestamp = strtotime($data['PortService']['posted_date']);
                                        echo $new_date = date('d-m-Y', $old_date_timestamp);
                                        ?></div>
                                </div>
                               <?php
								if ($data['PortService']['status'] == '4' || $data['PortService']['status'] == '')
								{
									if($data['PortService']['payment'] != '')
									{
								?>
								<div class="col-md-6">
                                    <div class="form-control" >
									<a href="<?php echo $this->webroot; ?>accounts/make_payment/<?php echo $data['PortService']['id'];?>"><button id="id_submit" class="btn btn-primary btn-red pull-right" value="1">Pay Now</button></a></div>
                                </div>
                                <?php
								}
								}
                            }
                            ?>	
                        </div>
                        <div class="clear"></div>
                        <?php
                        if ($data['PortService']['status'] == 1) {
                            ?>
                            <div class="block-inner-link">
                                <ul>
                                    <li><a href="<?php echo $this->webroot; ?>accounts/Portfolio_ViewReportDetail/<?php echo $data['PortService']['id']; ?>">View Details</a></li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>

                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">

                    No Service Listing

                </div>
            <?php }
            ?>

            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">

                    <?php
                    //   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

                    // the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
                        'tag' => 'li',
                        'currentClass' => 'active', 'class' => 'pagi_nu'));

                    // for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

                    // the 'last' page button
                    // echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>		

                </ul>

            </div>

        </div>		

    </div>

