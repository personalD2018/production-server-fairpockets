<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Project list By <br><?php echo $this->Number->getUserOrgNameByUserId($builderId); ?></h1>
        </div>
        <div class="col-md-4">
            <div class="input-group custom-search-form">
                <input type="text" id="searchbox" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-yellow" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="align-right" style="display:none;">
            <button class="btn btn-primary" style="margin-right:16px;" onclick="javascript: window.location.href='<?php echo $this->Html->url('/accounts/inventory/add')?>'"> Add New Inventory</button>
        </div>
        <div class="col-md-12">
            <table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Project Name</th>
						<th>Maximum Discount Allowed</th>
						<th>Discount Unit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($builderProjectsArray as $key=>$row):
                    ?>
                    <tr> <?php if($row['ProjectsAndroidApp']['proj_discount_unit']==1){ $projectUnit = ' Rs'; } if($row['ProjectsAndroidApp']['proj_discount_unit']==2){ $projectUnit = ' %'; } if($row['ProjectsAndroidApp']['proj_discount_unit']==3){ $projectUnit = ' Sq Ft'; } ?>
                        <td><?php echo isset($row['ProjectsAndroidApp']['project_name']) ? $row['ProjectsAndroidApp']['project_name'] : "N/A";?></td>
                        <td><?php echo isset($row['ProjectsAndroidApp']['proj_discount']) ? $row['ProjectsAndroidApp']['proj_discount'].$projectUnit : "N/A";?></td>
						<td><?php echo isset($row['ProjectsAndroidApp']['proj_discount_unit']) ? $row['ProjectsAndroidApp']['proj_discount_unit'] : "N/A";?></td>						
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        deleteSalesEmployees = function(){
            var sales_id = arguments[0];
            jQuery.confirm({
                title: 'Confirm!',
                content: 'Want To Delete This Sales Employees From Your Network.?',
                buttons: {
                    confirm: function () {
                        window.location.href = '<?php echo Router::url('/accounts/deleteSalesEmployees/',true);?>' + sales_id
                    }
                }
            });
        }
        var dataTable = jQuery('#sales_list').DataTable({
            "columnDefs": [{
                "targets": [2], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        //jQuery( "div#MainMenu a.inventory" ).addClass('active');
        //jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        //jQuery( "div#demo100" ).addClass('in');
        //jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>