<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Builders List</h1>
        </div>
        <div class="col-md-4">
            <div class="input-group custom-search-form">
                <input type="text" id="searchbox" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-yellow" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="align-right" style="display:none;">
            <button class="btn btn-primary" style="margin-right:16px;" onclick="javascript: window.location.href='<?php echo $this->Html->url('/accounts/inventory/add')?>'"> Add New Inventory</button>
        </div>
        <div class="col-md-12">
            <table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Builder Organization Name</th>
						<th>Creation Date</th>
						<!--<th>Actions</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($buildersListArray as $key=>$row):
                    ?>
                    <tr>
                        <td><?php echo isset($row['Websiteuser']['userorgname']) ? $row['Websiteuser']['userorgname'] : "N/A";?></td>
                        <td><?php echo isset($row['Websiteuser']['acccreatedate']) ? $row['Websiteuser']['acccreatedate'] : "N/A";?></td>
						<td style="text-align: center; display:none;">
                            <a href="<?php echo Router::url('/accounts/view_builder_list_broker/'.$row['Websiteuser']['id'],true);?>" title="View Assigned Projects By <?php echo $row['Websiteuser']['userorgname']; ?>" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                             </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        deleteSalesEmployees = function(){
            var sales_id = arguments[0];
            jQuery.confirm({
                title: 'Confirm!',
                content: 'Want To Delete This Sales Employees From Your Network.?',
                buttons: {
                    confirm: function () {
                        window.location.href = '<?php echo Router::url('/accounts/deleteSalesEmployees/',true);?>' + sales_id
                    }
                }
            });
        }
        var dataTable = jQuery('#sales_list').DataTable({
            "columnDefs": [{
                "targets": [2], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        //jQuery( "div#MainMenu a.inventory" ).addClass('active');
        //jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        //jQuery( "div#demo100" ).addClass('in');
        //jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>