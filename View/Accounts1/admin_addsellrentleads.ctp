<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate();
    });
    $(function () {
        $('#datetimepicker4').datetimepicker({
            pickTime: false
        });
        $('#datetimepicker5').datetimepicker({
            pickTime: false
        });
    });
</script>	
<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>						
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Sell/Rent Leads</h3>

            </div>
            <?php echo $this->Session->flash(); ?> 
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Add FP Leads</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('AsstSellLead', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">Contact Person Name</label>
                            <div class="controls">
                                <?php echo $this->Form->input('name', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Name', 'class' => 'span12 required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Price Discussion</label>
                            <div class="controls">
                                <?php echo $this->Form->input('price_discussion', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'title' => 'Please Enter Price Discussion', 'class' => 'span12 required')); ?>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Call Type</label>
                            <div class="controls">
                                <?php echo $this->Form->input('call_type', array('id' => 'role', 'options' => array('3' => 'Telephone', '4' => 'Site Visit'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Role', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Last Call</label>

                            <div id="datetimepicker4" class="input-append">
                                <?php echo $this->Form->input('doj1', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Date of Joining', 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Next Call</label>

                            <div id="datetimepicker5" class="input-append">
                                <?php echo $this->Form->input('doj2', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Date of Joining', 'class' => 'datepicker small required span12')); ?><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Activity</label>
                            <div class="controls">
                                <?php echo $this->Form->input('activity', array('type' => 'textarea', 'class' => 'span12 required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter Address')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'role', 'options' => array('1' => 'Hot', '2' => 'Warm', '3' => 'Cold'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Role', 'label' => false)); ?>
                            </div>
                        </div>
                        <?php echo $this->Form->input('property_id', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'hidden', 'title' => 'Please Enter Price Discussion', 'value' => $property_id)); ?>

                        <?php echo $this->Form->input('websiteuser_id', array('type' => 'hidden', 'label' => false, 'div' => false)); ?>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Add Lead</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               