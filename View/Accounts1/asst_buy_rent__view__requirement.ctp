   <div class="page-content">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Assisted Buy / Rent - Requirements</h1>
                </div>
                <div class="col-md-3">
                    <div class="user_panel_search">
                        <form class="search" id="search" method="post"  action="AsstBuyRent_View_Requirement" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>

                           </form>
                       </div>
                   </div>
               </div>
			<div class="space-x"></div>
            <?php
            if (!empty($allprojdata)) {

                foreach ($allprojdata as $data) {
					
                    if ($data['BuyRentRequirement']['prop_type'] == '1') {
                        $pro_type = "Residential";
                    } else {
                        $pro_type = "Commercial";
                    }
                    if ($data['BuyRentRequirement']['prop_reqt'] == '1') {
                        $prop_reqt = "Rent";
                    } else {
                        $prop_reqt = "Buy";
                    }
                    ?>
                    <div id="build" class="list-detail detail-block target-block">

                        <div class="detail-title">
                            <a href="#">Requirement for <?php echo $pro_type ?> Property ( <?php echo $prop_reqt; ?> ) in 
                                <?php
                                echo $data['MstAreaCity']['cityName'];



                                switch ($data['BuyRentRequirement']['status']) {
                                    case 0:
                                        echo ' [ Under Approval ]';
                                        break;
                                    case 4:
                                        echo ' [ Rejected ]';
                                        break;
                                    case 5:
                                        echo ' [ Closed ]';
                                        break;
                                    default:
                                        echo ' [ Active ]';
                                }
                                ?>
                            </a>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-control" > <strong>Completion Status : </strong>

                                    <?php
                                    if ($data['BuyRentRequirement']['prop_cstat'] == '1') {
                                        echo $prop_reqt = "Ready to move in";
                                    } else {
                                        echo $prop_reqt = "Under Construction";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Budget : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['BuyRentRequirement']['prop_budget'] ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Purpose : </strong>		
                                    <?php
                                    if ($data['BuyRentRequirement']['prop_purpose'] == '1') {
                                        echo $prop_purpose = "End use";
                                    } else {
                                        echo $prop_purpose = "Investment";
                                    }
                                    ?></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Locality : </strong><?php echo $data['BuyRentRequirement']['prop_locality']; ?></div>
                            </div>

                        </div>
                        <?php if ($data['BuyRentRequirement']['status'] == '3') { ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service started on: </strong><?php echo $data['BuyRentRequirement']['service_start']; ?> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service expiry on: </strong><?php echo $data['BuyRentRequirement']['service_end']; ?></div>
                                </div>

                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service Posted on: </strong><?php
                                        $old_date_timestamp = strtotime($data['BuyRentRequirement']['posted date']);
                                        echo $new_date = date('d-m-Y', $old_date_timestamp);
                                        ?> </div>
                                </div>
                            </div>
                        <?php }
                        ?>
                        <div class="clear"></div>
                        <?php if ($data['BuyRentRequirement']['status'] == '1') { ?>
                            <div class="block-inner-link">
                                <ul>
                                    <li><a href="<?php echo $this->webroot ?>accounts/AsstBuyRent_EditRequirement/<?php echo $data['BuyRentRequirement']['id']; ?>">Edit</a></li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">

                    No Requirement Listing

                </div>
            <?php }
            ?>


            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">

                    <?php
						//$this->Paginator->params()['count'];die;
						if($this->Paginator->params()['count']) {
							if ($this->Paginator->hasPrev()) {

								echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
							}
							
							// the 'number' page buttons
							echo $this->Paginator->numbers(array('modulus' => 2, 'separator' => '',
							'tag' => 'li',
							'currentClass' => 'active', 'class' => 'pagi_nu'));
							
							// for the 'next' button
							if ($this->Paginator->hasNext()) {
								echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
							}
						}
					?>		

                </ul>
            </div>

        </div>		

    </div>
