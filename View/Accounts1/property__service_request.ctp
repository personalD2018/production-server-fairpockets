<style>

    .multiselect-container > li {
        width : 100%;
    }

    .block-inner-link li a {
        background:white;
        color:black;
    }

    .dropdown-menu > .active > a {
        background-color:black;
    }	

    .dropdown-menu > .active > a:hover,
    .dropdown-menu > .active > a:focus {
        color:black;
        background-color:#f5f5f5;
    }

</style>

<?php
$option = array();
if (!empty($Portmgmt)) {

    foreach ($Portmgmt as $data) {
        if ($data['Portmgmt']['prop_type'] == '1') {
            $pro_type = "Residential ";
        } else {
            $pro_type = "Commercial ";
        }
        $option[$data['Portmgmt']['id']] = $pro_type . "Property in " . $data['Portmgmt']['prop_locality'] . " " . $data['MstAreaCity']['cityName'];
    }
}
?>
    <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> Property Management - Request Service </h1>
                </div>
            </div>

            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo $this->Form->create(
                            'PropService', array(
                        'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'id_form_PropService',
                        'novalidate' => 'novalidate'
                            )
                    );
                    ?>


                    <div class="account-block">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_sname" class="label_title">Service Name <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->input(
                                                'prop_sname', array(
                                            'id' => 'prop_sname',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_prop" class="label_title">Select Property </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <select id="id_prop_prop" name="data[PropService][port_prop]" style="display: none;">
                                            <?php
                                            if (!empty($option)) {
                                                foreach ($option as $x => $x_value) {
                                                    ?>

                                                    <option value = "<?php echo $x; ?>"><?php echo $x_value; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>

                                </div>
                            </div>

                        </div>	

                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_sdesc" class="label_title">Service Description <span class="mand_field">*</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <?php
                                        echo $this->Form->textarea(
                                                'prop_sdesc', array(
                                            'id' => 'prop_sdesc',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false,
                                            'rows' => '3'
                                                )
                                        );
                                        ?>	

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <label for="prop_serv" class="label_title">Select Service </label>
                                    </div>

                                    <div class="col-sm-12">

                                        <select id="id_prop_serv" multiple="multiple" name="data[PropService][services][]" style="display: none;">
                                            <option value="Repair" selected="selected" >Repair</option>
                                            <option value="Interiors">Interiors</option>
                                            <option value="Utility Bills">Utility Bills</option>
                                            <option value="Others">Others</option>
                                        </select>


                                    </div>

                                </div>
                            </div>

                        </div>	


                        <br>
                        <div class="row">

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <!--
                                        <label><span style="color:RED;">*</span> In a Portfolio, you can add upto max 5 properties </label> 
                                        <br><br>
                                        -->
                                        <button type="submit" class="btn btn-primary" >Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
           <div class="clear"></div>
        </div>
<?php
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
//echo $this->Html->script('additional-methods.min');

// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("

		var rulesMTarea = {
					required : true
		};
		
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		$('#id_form_PropService').validate({
			
						// Specify the validation rules
			rules: {
				'data[PropService][prop_sname]': rulesMTarea,
				'data[PropService][prop_sdesc]': rulesMTarea
			},
			
			// Specify the validation error messages
			messages: {
				'data[PropService][prop_name]': messageMTarea,
				'data[PropService][prop_sdesc]': messageMTarea
			}
		});	
	");
?>

<script>
    $(document).ready(function () {
        $("#id_prop_prop").multiselect({
            includeSelectAllOption: false
        });

        $("#id_prop_serv").multiselect({
            includeSelectAllOption: true,
            selectAllText: 'Select all!'
        });

    });
</script>
