<!--./advertisement-Card-->
    <div class="page-content">
   <?php
        $paginator = $this->Paginator;
        ?>
          <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Property Management - My Requested Services</h1>
                </div>
              <div class="col-md-3">
                    <div class="user_panel_search">
                        <form class="search" id="search" method="post"  action="Porperty_ViewService" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                                    </div>
                           </form>
                       </div>
                   </div>
               </div>
            
<!--
            <div class="user_panel_search">
                <form class="search" id="search" method="post" action="Porperty_ViewService">
                    <input type="text" name="search" placeholder="Search..">
                    <a onclick="document.getElementById('search').submit()" ><i class="fa fa-search fa-lg" aria-hidden="true" style="color:#000;"></i></a>
                </form>
            </div>-->

            <?php
            if (!empty($requirement)) {

                foreach ($requirement as $data) {

                    if ($data['PropService']['status'] == '1') {
                        $payment = "Approved";
                    } else if ($data['PropService']['status'] == '2') {
                        $payment = "Rejected";
                    } else if ($data['PropService']['status'] == '3') {
                        $payment = "Closed";
                    } else if ($data['PropService']['status'] == '4') {
                        $payment = "Payment Pending";
                    } else {
                        $payment = "Under Approval";
                    }
                    ?>
                    <div id="build" class="list-detail detail-block target-block">

                        <div class="detail-title">
                            <a href="#"><?php echo $data['PropService']['prop_sname']; ?> Summary (<?php echo $payment; ?>)</a>
                        </div>


                        <div class="row">
                            <?php
                            if (!empty($data['PropService']['service_start'])) {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service started on: </strong><?php echo $data['PropService']['service_start']; ?> </div>
                                </div>
                                <?php if (!empty($data['PropService']['service_end'])) {
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-control" ><strong>Service closed: </strong><?php echo $data['PropService']['service_end']; ?>  </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-control" ><strong>Service expiry on: </strong><?php echo date($data['PropService']['service_end'], strtotime('+3 month')); ?>  </div>
                                    </div>

                                    <?php
                                }
                            } else {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service Posted on: </strong><?php
                                        $old_date_timestamp = strtotime($data['PropService']['posted_date']);
                                        echo $new_date = date('d-m-Y', $old_date_timestamp);
                                        ?></div>
                                </div>
                                <?php
                            }
                            ?>	
                        </div>

                        <div class="clear"></div>
                        <?php
                        if ($data['PropService']['status'] == 1 || $data['PropService']['status'] == 3) {
                            ?>
                            <div class="block-inner-link">
                                <ul>
                                    <li><a href="<?php echo $this->webroot; ?>accounts/Property_ServiceDetails/<?php echo $data['PropService']['id']; ?>">View Details</a></li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>

                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">

                    No Service Listing

                </div>
            <?php }
            ?>


            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">

                    <?php
//   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
// 'prev' page button, 
// we can check using the paginator hasPrev() method if there's a previous page
// save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

// the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
                        'tag' => 'li',
                        'currentClass' => 'active', 'class' => 'pagi_nu'));

// for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

// the 'last' page button
// echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>		

                </ul>

            </div>

        </div>		

    </div>



