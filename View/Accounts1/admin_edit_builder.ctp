<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate();
    });

</script>	

<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Builder</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Update FP Builder Organization</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('Builder', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">Contact Person</label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_contact', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Contact Person', 'class' => 'small required')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Landline</label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_landline', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'title' => 'Please Enter Landline', 'class' => 'small required')); ?>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Official Website</label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_web', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'small')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Established Year </label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_year', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Enter Established Year', 'class' => 'small required number')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">About</label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_about', array('type' => 'textarea', 'class' => 'small required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter About')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Management</label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_mgmt', array('type' => 'textarea', 'class' => 'small required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter Management')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Address</label>
                            <div class="controls">
                                <?php echo $this->Form->input('borg_addr', array('type' => 'textarea', 'class' => 'small required', 'label' => false, 'div' => false, 'rows' => '1', 'title' => 'Please Enter Address')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'role', 'options' => array('3' => 'Approved', '4' => 'Rejected'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Role', 'label' => false)); ?>
                            </div>
                        </div>
                        <?php echo $this->Form->input('websiteuser_id', array('type' => 'hidden', 'label' => false, 'div' => false)); ?>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update Builder</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>				               