<?php if (@$status != '3') { ?>
    <style>
        #disable{
            pointer-events: none;
            background:#e6e6e6 !important;
        }
    </style>
<?php } ?>

<script>
    $(function ()
    {
<?php if ($status == '2' || $status == '3') { ?>
            $("#id_form_builderorg_profile :input").prop("disabled", true);
            $(".submit_area").hide();
    <?php
} else {
    ?>
            $("#id_form_builderorg_profile :input").prop("disabled", false);
<?php }
?>
$("#id_form_builderorg_profile :input").prop("disabled", false);
    });
</script> 
    <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> Builder Organization Profile (<?php echo $builder_Status; ?>) </h1>
                </div>
                <!--/.col-lg-12 -->
            </div>
            <?php echo $this->Session->flash(); ?> 

            <div id="build" class="list-detail detail-block target-block">

                <div class="block-inner-link">

                    <?php
                    echo $this->Form->create(
                            'Builder', array(
                        'class' => 'fpform',
                        'role' => 'form',
                        'id' => 'id_form_builderorg_profile',
                        'novalidate' => 'novalidate',
                        'url' => 'builderapproval'
                            )
                    );
                    ?>


                    <div class="account-block">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="borg_contact" class="label_title">Contact Person <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'borg_contact', array(
                                        'id' => 'borg_contact',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="borg_landline" class="label_title">Landline <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'borg_landline', array(
                                        'id' => 'borg_landline',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false,
										'maxlength' => 20
                                            )
                                    );
                                    ?>	

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="borg_web" class="label_title">Official Website <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'borg_web', array(
                                        'id' => 'borg_web',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="borg_year" class="label_title">Established Year <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'borg_year', array(
                                        'id' => 'borg_year',
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false
                                            )
                                    );
                                    ?>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group" style="overflow:hidden;">

                                    <label for="borg_about" class="label_title">About <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'borg_about', array(
                                        'id' => 'borg_about',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'rows' => '1',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group" style="overflow:hidden;">

                                    <label for="borg_mgmt" class="label_title">Management <span class="mand_field">*</span></label>
                                    <?php
                                    echo $this->Form->input(
                                            'borg_mgmt', array(
                                        'id' => 'borg_mgmt',
                                        'type' => 'textarea',
                                        'class' => 'form-control',
                                        'rows' => '1',
                                        'label' => false
                                            )
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-12 add-tab-content" style="display:block;border:none;margin-top:10px;">
                                <div class="add-tab-row  push-padding-bottom" style="padding:0px;">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <div class="form-group">

                                                        <label for="city" class="label_title">Organization City <span class="mand_field">*</span></label>

                                                        <div class="select-style">
                                                            <?php
                                                            echo $this->Form->input(
                                                                    'city', array(
                                                                'id' => 'id_select_city', 'options' => $city_master,
                                                                'class' => 'selectpicker bs-select-hidden',
                                                                'label' => false,
                                                                'empty' => 'Select'
                                                                    )
                                                            );
                                                            ?>

                                                        </div> 

                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="searchInput" class="label_title">Organization Locality <span class="mand_field">*</span></label>
                                                        <?php
                                                        echo $this->Form->input(
                                                                'searchInput', array(
                                                            'id' => 'searchInput',
                                                            'type' => 'text',
                                                            'class' => 'form-control',
                                                            'label' => false
                                                                )
                                                        );
                                                        ?>
                                                        <div id="result" class="gMapResults" style="display:none"; ></div>		
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="borg_addr" class="label_title">Organization Address <span class="mand_field">*</span></label>
                                                        <?php
                                                        echo $this->Form->input(
                                                                'borg_addr', array(
                                                            'id' => 'address',
                                                            'type' => 'text',
                                                            'class' => 'form-control',
                                                            'label' => false
                                                                )
                                                        );
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>	
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="map" id="map" style="border-style: none;border:none;width:100%;" ></div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <?php echo $this->Form->input('id_search_adm_area_1', array('id' => 'id_search_adm_area_1', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                        <?php echo $this->Form->input('id_search_adm_area_2', array('id' => 'id_search_adm_area_2', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	

                                        <div class="form_area">
                                            <?php echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
                                            <!-- Latitude -->
                                            <?php echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('tbUnit0', array('id' => 'tbUnit0', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('tbUnit1', array('id' => 'tbUnit1', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('tbUnit2', array('id' => 'tbUnit2', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('tbUnit3', array('id' => 'tbUnit3', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('tbUnit4', array('id' => 'tbUnit4', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
                                            <?php echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

                                        </div>
                                    </div>
                                </div>
                            </div>									

                            <div class="col-sm-12">
                                <div class="account-block  submit_area">
                                    <?php echo $this->Form->input('buider_id', array('id' => 'buider_id', 'label' => false, 'type' => 'hidden', 'value' => $buider_id)); ?>	
                                    <button type="submit" class="btn btn-primary " name="save" value='1'>Save Profile</button>
                                    <button type="submit" class="btn btn-success" name="save" value='2' onclick="submitform" >Submit Profile</a>


                                </div>
                            </div>

                        </div>

                    </div>

                    <!-- </form> -->	
                    <?php
                    echo $this->Form->end();
                    ?>


                </div>
            </div>



            <div class="clear"></div>



        </div>
<?php
// Jquery 
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');


// Script  : To set validation rules for FORMS
echo $this->Html->scriptBlock("
	
		var rulesMTarea = {
					required : true
		};
		
		var messageMTarea = {
					required : 'Field is required.'
		};
		
		var rulesMName = {
					required : true
		};
		
		var messageMName = {
					required : 'Name is required.'
		};
		
		var rulesMMobile = {
					required : true,
					number   : true,
					minlength: 10,
					maxlength: 20
		};
		
		var messageMMobile = {
					required : 'Landline is required.',
					number   : 'Only numbers allowed.',
					minlength: 'Min 10 digits',
					maxlength: 'Max 20 digits'
		};
		
		var rulesMYear = {
					required : true,
					number   : true,
					min      : 1000,
					max      : 9999
		};
		
		var messageMYear = {
					required : 'Year is required.',
					number   : 'Only numbers allowed.',
					min      : 'Min 4 digits',
					max      : 'Max 4 digits'
		};
		
		var ruleMWeb = {
					required : true,
					curl: true
		};
		
		var messageMWeb = {	
					required : 'Web URL is required.'
		}

		$.validator.addMethod('map_check', function(value, element) 
		{
			var ret_val = checkGmapErrors();
			return ret_val ;

		}, 'Please tick a locality on the map.');		

		
		$.validator.addMethod('curl', function(value, element) 
		{
			return this.optional(element) || /^(?:(http|https)?:\/\/)?(?:[\w-]+\.)+([a-z]|[A-Z]|[0-9]){2,6}$/gi.test(value);
		}, 'URL format not correct.');

		$('#id_form_builderorg_profile').validate({
			
			// Specify the validation rules
			rules: {
				'data[Builder][borg_contact]': rulesMName,
				'data[Builder][borg_landline]': rulesMMobile,
				'data[Builder][borg_web]': ruleMWeb,
				'data[Builder][borg_year]': rulesMYear,
				'data[Builder][borg_about]': rulesMTarea,
				'data[Builder][borg_mgmt]': rulesMTarea,
				'data[Builder][borg_addr]': {
					required : true,
					map_check : true
				}
				
			},
			
			// Specify the validation error messages
			messages: {
				'data[Builder][borg_contact]': messageMName,
				'data[Builder][borg_landline]': messageMMobile,
				'data[Builder][borg_web]': messageMWeb,
				'data[Builder][borg_year]': messageMYear,
				'data[Builder][borg_about]': messageMTarea,
				'data[Builder][borg_mgmt]': messageMTarea,
				'data[Builder][borg_addr]': {
					required : 'Address is required.'
				}
			},
			
			submitHandler: function(form){
				

                                  // Validations OK. Form can be submitted
				  form.submit();
											
								
			}
		})
	");
?>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('fp-loc8.js');
?>

<script type="text/javascript">

	function gMapOptionClick(elem)
	{
		document.getElementById('searchInput').value = elem.innerHTML;
		document.getElementById("result").style.display = 'none';

		//Generate click event
		document.getElementById('searchInput').click();
	}

	$(document).ready(function () {


		$('.dropdown-menu a').on('click', function (event) {

			var $target = $(event.currentTarget),
					val = $target.attr('data-value'),
					$inp = $target.find('input'),
					idx;

			if ((idx = options.indexOf(val)) > -1) {
				options.splice(idx, 1);
				setTimeout(function () {
					$inp.prop('checked', false)
				}, 0);
			} else {
				options.push(val);
				setTimeout(function () {
					$inp.prop('checked', true)
				}, 0);
			}

			$(event.target).blur();

			console.log(options);
			return false;
		});

	});
</script>

