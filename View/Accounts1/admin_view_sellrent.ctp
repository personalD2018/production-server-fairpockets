<style>
    .controls{
        margin-top: 5px;
    }
</style>
<?php
$project_feature = json_decode($data['Property']['features'], 'true');
if (!empty($project_feature['Configure'])) {
    $option = $project_feature['Configure'];
} else {
    if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
        $option = "Residential Property";
    } else {
        $option = "Commercial Property";
    }
}

$option .= " for ";
if ($data['Property']['transaction_type'] == '1') {
    $option .= "Sale";
} else {
    $option .= "Rent";
}
$option .= " in ";

$option .= $data['Property']['city_data'];
$option .= " , ";
$option .= $data['Property']['state_data'];
if ($data['AsstSellRent']['prop_type'] == '1') {
    $prop_reqt = "Rent Out";
} else {
    $prop_reqt = "Sell";
}
?>	
<div class="container-fluid">

    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Sell/Rent Service</h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>View Buy/Rent Service</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('AsstSellRent', array("type" => "file", 'id' => "formID", 'class' => 'form-horizontal left-align')); ?>

                        <div class="control-group">
                            <label class="control-label">Requirement</label>
                            <div class="controls">
                                <?php echo $prop_reqt; ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Property </label>
                            <div class="controls">
                                <?php echo $option; ?>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Status </label>
                            <div class="controls">
                                <?php echo $this->Form->input('status', array('id' => 'role', 'options' => array('3' => 'Active', '4' => 'Rejected', '5' => 'Close'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Status', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update</button>

                        </div>
                        </form>

                    </div>				               
                </div>
            </div>
        </div>
    </div>
</div>