<!--./advertisement-Card-->
    <div class="page-content">
        <?php $paginator = $this->Paginator;?>
            <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Assisted Sell / Rent - Leads</h1>
                </div>
                <div class="col-md-3">
                    <div class="user_panel_search">
                        <form class="search" id="search" method="post"  action="AsstBuyRent_View_Requirement" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                                     </div>

                           </form>
                       </div>
                   </div>
               </div>
           
<!--            <div class="user_panel_search">
                <form class="search" id="search" method="post">
                    <input type="text" name="search" placeholder="Search..">
                    <a onclick="document.getElementById('search').submit()" ><i class="fa fa-search fa-lg" aria-hidden="true" style="color:#000;"></i></a>
                </form>
            </div>-->
            <?php
            $project_feature = json_decode($data['Property']['features'], 'true');
            ?>	

            <div id="build" class="list-detail detail-block target-block">

                <div class="detail-title">
                    <a>
                        <?php
                        if (!empty($project_feature['Configure'])) {
                            echo $project_feature['Configure']; /* echo " / "; echo $project_feature['bathrooms']; echo " bath"; */
                        } else {
                            if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
                                echo "Residential Property";
                            } else {
                                echo "Commercial Property";
                            }
                        }
                        ?>
                        for <?php
                        if ($data['Property']['transaction_type'] == '1') {
                            echo "Sale";
                        } else {
                            echo "Rent";
                        }
                        ?> in <?php
                        if (!empty($data['Project']['project_name'])) {
                            echo $data['Project']['project_name']
                            ?> ,<?php } ?> <?php
                        if ($this->Session->read('Auth.Websiteuser.userrole') == 3) {
                            echo $data['Project']['city_data'];
                        } else {
                            echo $data['Property']['city_data'];
                        }
                        ?></a>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-control" ><?php if ($data['Property']['transaction_type'] == '1') { ?>
                                <strong>Market Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                if (!empty($data['Property']['market_price'])) {
                                    echo " " . $data['Property']['market_price'];
                                }
                            } else {
                                ?>
                                <strong>Market Rent: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                if (!empty($data['Property']['market_rent'])) {
                                    echo " " . $data['Property']['market_rent'];
                                }
                            }
                            ?></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-control" ><?php if ($data['Property']['transaction_type'] == '1') { ?>
                                <strong>Offer Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                if (!empty($data['Property']['offer_price'])) {
                                    echo " " . $data['Property']['offer_price'];
                                }
                            } else {
                                ?>
                                <strong>Expected Rent: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                if (!empty($data['Property']['expected_monthly'])) {
                                    echo " " . $data['Property']['expected_monthly'];
                                }
                            }
                            ?> </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?php if (!empty($project_feature['sbua'])) {
                            ?>
                            <div class="form-control" > <strong>Super Built Up Area : </strong> <?php
                                echo $project_feature['sbua'];
                                echo " " . $project_feature['sbua_m'];
                                ?></div>
                        <?php } ?>

                        <?php if (!empty($project_feature['plot_area'])) {
                            ?>
                            <div class="form-control" > <strong>Plot Area : </strong> <?php
                                echo $project_feature['plot_area'];
                                echo " " . $project_feature['plot_area_sq'];
                                ?></div>
                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-control" > <strong>Property Type : </strong> <?php
                            if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
                                echo "Residential";
                            } else {
                                echo "Commercial";
                            }
                            ?> </div>
                    </div>

                </div>

                <div class="row">
                    <?php
                    if (!empty($data['PropertyTypeOptions']['option_name'])) {
                        ?>
                        <div class="col-md-6">
                            <div class="form-control" ><strong>Property Type Option : </strong><?php echo $data['PropertyTypeOptions']['option_name']; ?> </div>
                        </div>
                    <?php } ?>	
                    <div class="col-md-6">
                        <div class="form-control" > <strong>Address : </strong> <?php echo $data['Property']['address'];
                    ?> </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-control" ><strong>Locality: </strong><?php echo $data['Property']['locality']; ?> </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-control" ><strong>Posted on : </strong><?php echo $newDate = date("d M Y", strtotime($data['Property']['created_date'])); ?></div>
                    </div>

                </div>
                <?php
                if ($data['AsstSellRent']['status'] != 0) {
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-control" ><strong>Service started on: </strong>01 Apr 2017 </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-control" ><strong>Service expiry on: </strong>01 Apr 2017 </div>
                        </div>

                    </div>
                <?php } ?>


            </div>
            <?php
            foreach ($AsstSellLead as $data) {
                ?>
                <div  class="list-response detail-block target-block">

                    <div class="detail-title">
                        Lead 1 
                        <div class="pull-right res-date"><?php echo $data['AsstSellLead']['created_date']; ?></div>
                    </div>


                    <ul  class="list-three-col inner-res-contact">
                        <li><strong>Name:</strong><?php echo $data['AsstSellLead']['name']; ?> </li>
                        <li><strong>Price Discussion :</strong><?php echo $data['AsstSellLead']['price_discussion']; ?></li>
                        <li><strong>Call type</strong><?php
                            if ($data['AsstSellLead']['call_type'] == 3) {
                                echo "Telephone";
                            }
                            if ($data['AsstSellLead']['call_type'] == 4) {
                                echo "Site Visit";
                            }
                            ?></li>
                    </ul>

                    <ul  class="list-three-col inner-res-contact">
                        <li><strong>Last Call:</strong><?php echo $data['AsstSellLead']['doj1']; ?></li>
                        <li><strong>Next call schedule :</strong><?php echo $data['AsstSellLead']['doj2']; ?></li>
                    </ul>

                    <div class="clear"></div>

                    <h3>Activities so far : </h3>
                    <p>
                        <?php echo $data['AsstSellLead']['activity']; ?>
                    </p>

                    <div class="clear"></div>

                    <div class="block-inner-link col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Status:</strong> <?php
                                if ($data['AsstSellLead']['status'] == 1) {
                                    echo "Hot";
                                }
                                if ($data['AsstSellLead']['status'] == 2) {
                                    echo "Warm";
                                }
                                if ($data['AsstSellLead']['status'] == 3) {
                                    echo "Cold";
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>

            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">

                    <?php
//   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
// 'prev' page button, 
// we can check using the paginator hasPrev() method if there's a previous page
// save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

// the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
                        'tag' => 'li',
                        'currentClass' => 'active', 'class' => 'pagi_nu'));

// for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

// the 'last' page button
// echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>		

                </ul>


            </div>





        </div>
    </div>

