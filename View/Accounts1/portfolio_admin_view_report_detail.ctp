<!--./advertisement-Card-->

<div class="user_pa_area col-sm-12">
    <div class="container user_inner">
        <div id="page-wrapper" class="col-sm-12 user-page-area">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Portfolio <?php echo $report_detail['PortService']['port_name']; ?> Summary </h1>
                </div>
            </div>

            <div id="build" class="list-detail detail-block target-block" style="background-color:#e5e5e5;padding-bottom:20px;">

                <ul >
                    <li class="col-md-6"><strong>Latest Valuation:</strong> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $report_detail['PortService']['Valuation']; ?></li>
                    <li class="col-md-6"><strong>Latest Overall CAGR:</strong> <?php echo $report_detail['PortService']['CAGR']; ?>%</li>
                    <li class="col-md-6"><strong>Created on:</strong><?php echo $report_detail['PortService']['Created_on']; ?></li>
                    <li class="col-md-6"><strong>View latest report:</strong> <a href="<?php echo @$this->webroot . @$report_detail['PortService']['latest_pdf']; ?>">Pdf Download</a></li>
                </ul>

            </div>

            <?php
            if (!empty($properties)) {

                foreach ($properties as $data) {
                    if ($data['Portfolio']['prop_type'] == '1') {
                        $pro_type = "Residential";
                    } else {
                        $pro_type = "Commercial";
                    }
                    ?>
                    <div id="build" class="list-detail detail-block target-block">

                        <div class="detail-title">
                            <a href="#"><?php echo $pro_type ?> Property in <?php echo $data['Portfolio']['prop_locality']; ?> ,  <?php echo $data['MstAreaCity']['cityName']; ?></a>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" > <strong> Area : </strong> <?php echo $data['Portfolio']['prop_area']; ?> /sq feet </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Status : </strong> <?php echo $data['Portfolio']['prop_state']; ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Locality: </strong><?php echo $data['Portfolio']['prop_locality']; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Address : </strong> <?php echo $data['Portfolio']['prop_addr']; ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Current Valuation : </strong><?php echo $data['Portfolio']['prop_curr_val']; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Current Rental: </strong><?php echo $data['Portfolio']['prop_curr_rent']; ?></div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" > <strong>5 years value: : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_5yr_val']; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>5 years rental : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Portfolio']['prop_5yr_rent']; ?></div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Advice: </strong><?php echo $data['Portfolio']['prop_advice']; ?></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" ><strong>CAGR: </strong><?php echo $data['Portfolio']['prop_cagr']; ?> % </div>
                            </div>

                        </div>

                        <div class="clear"></div>
                        <div class="block-inner-link">
                            <ul>
                                <li><a href="<?php echo $this->webroot; ?>accounts/editportfolio/<?php echo $data['Portfolio']['id']; ?>">Edit</a></li>
                            </ul>
                        </div>
                    </div>
                <?php
                }
            }
            ?>


            <div class="clear"></div>

        </div>

    </div>	


