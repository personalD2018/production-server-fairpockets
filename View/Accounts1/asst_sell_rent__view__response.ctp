    <div class="page-content">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Assisted Sell / Rent - Responses</h1>
                </div>
                <div class="col-md-3">
                    <div class="user_panel_search">
                        <form class="search" id="search" method="post"  action="AsstBuyRent_View_Requirement" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                                     </div>

                           </form>
                       </div>
                   </div>
               </div>
			   <div class="space-x"></div>
            <?php
            if (!empty($allprojdata)) {

                foreach ($allprojdata as $data) {

                    $project_feature = json_decode($data['Property']['features'], 'true');
                    ?>	

                    <div id="build" class="list-detail detail-block target-block">

                        <div class="detail-title">
                            <a>
                                <?php
                                if (!empty($project_feature['Configure'])) {
                                    echo $project_feature['Configure']; /* echo " / "; echo $project_feature['bathrooms']; echo " bath"; */
                                } else {
                                    if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
                                        echo "Residential Property";
                                    } else {
                                        echo "Commercial Property";
                                    }
                                }
                                ?>
                                for <?php
                                if ($data['Property']['transaction_type'] == '1') {
                                    echo "Sale";
                                } else {
                                    echo "Rent";
                                }
                                ?> in <?php
                                if (!empty($data['Project']['project_name'])) {
                                    echo $data['Project']['project_name']
                                    ?> ,<?php } ?> <?php
                                if ($this->Session->read('Auth.Websiteuser.userrole') == 3) {
                                    echo $data['Project']['city_data'];
                                } else {
                                    echo $data['Property']['city_data'];
                                }


                                switch ($data['AsstSellRent']['status']) {
                                    case 0:
                                        echo ' [ Under Approval ]';
                                        break;
                                    case 4:
                                        echo ' [ Rejected ]';
                                        break;
                                    case 5:
                                        echo ' [ Closed ]';
                                        break;
                                    default:
                                        echo ' [ Active ]';
                                }
                                ?></a>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" ><?php if ($data['Property']['transaction_type'] == '1') { ?>
                                        <strong>Market Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                        if (!empty($data['Property']['market_price'])) {
                                            echo " " . $data['Property']['market_price'];
                                        }
                                    } else {
                                        ?>
                                        <strong>Market Rent: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                        if (!empty($data['Property']['market_rent'])) {
                                            echo " " . $data['Property']['market_rent'];
                                        }
                                    }
                                    ?></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" ><?php if ($data['Property']['transaction_type'] == '1') { ?>
                                        <strong>Offer Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                        if (!empty($data['Property']['offer_price'])) {
                                            echo " " . $data['Property']['offer_price'];
                                        }
                                    } else {
                                        ?>
                                        <strong>Expected Rent: </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
                                        if (!empty($data['Property']['expected_monthly'])) {
                                            echo " " . $data['Property']['expected_monthly'];
                                        }
                                    }
                                    ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?php if (!empty($project_feature['sbua'])) {
                                    ?>
                                    <div class="form-control" > <strong>Super Built Up Area : </strong> <?php
                                        echo $project_feature['sbua'];
                                        echo " " . $project_feature['sbua_m'];
                                        ?></div>
                                <?php } ?>

                                <?php if (!empty($project_feature['plot_area'])) {
                                    ?>
                                    <div class="form-control" > <strong>Plot Area : </strong> <?php
                                        echo $project_feature['plot_area'];
                                        echo " " . $project_feature['plot_area_sq'];
                                        ?></div>
                                <?php } ?>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Property Type : </strong> <?php
                                    if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
                                        echo "Residential";
                                    } else {
                                        echo "Commercial";
                                    }
                                    ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <?php
                            if (!empty($data['PropertyTypeOptions']['option_name'])) {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Property Type Option : </strong><?php echo $data['PropertyTypeOptions']['option_name']; ?> </div>
                                </div>
                            <?php } ?>	
                            <div class="col-md-6">
                                <div class="form-control" > <strong>Address : </strong> <?php echo $data['Property']['address'];
                            ?> </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Locality: </strong><?php echo $data['Property']['locality']; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control" ><strong>Posted on : </strong><?php
                                    $old_date_timestamp = strtotime($data['AsstSellRent']['posted_date']);
                                    echo $new_date = date('d-m-Y', $old_date_timestamp);
                                    ?> </div>
                            </div>

                        </div>
                        <?php
                        if (!empty($data['AsstSellRent']['service_start'])) {
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service started on: </strong><?php echo $data['AsstSellRent']['service_start']; ?></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-control" ><strong>Service expiry on: </strong><?php echo $data['AsstSellRent']['service_end']; ?></div>
                                </div>

                            </div>
                        <?php } ?>


                        <div class="clear"></div>
                        <?php
                        if (!empty($data['AsstSellRent']['service_start'])) {
                            ?>
                            <div class="block-inner-link">
                                <ul>
                                    <li><a href="<?php echo $this->webroot; ?>accounts/AsstSellRent_View_Lead/<?php echo $data['AsstSellRent']['prop_id']; ?>">View Leads</a></li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">

                    No Requirement Listing

                </div>
            <?php }
            ?>

            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">
                <ul class="inner_pagi">
					<?php
						if($this->Paginator->params()['count']) {
							if ($this->Paginator->hasPrev()) {
								echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
							}
							
							echo $this->Paginator->numbers(array('modulus' => 2, 'separator' => '',
							'tag' => 'li',
							'currentClass' => 'active', 'class' => 'pagi_nu'));
							
							if ($this->Paginator->hasNext()) {
								echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
							}
						}
					?>	

                </ul>
            </div>

        </div>		

    </div>


