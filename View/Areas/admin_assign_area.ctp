<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate();
    });


</script>	
<script type="text/javascript">
    $(document).ready(function () {

        $('#country_id').on('change', function() {
            var url = '/admin/areas/getAllAreaRegionsByCountry/' + $(this).val();
            $.getJSON(url, function(data) {
                if(data.length > 0) {
                    $("#region").html('<option value="">--select--</option>');
                        $.each(data, function(key, value) {
                        $("#region").append($('<option>', {
                            'value': value.MstAreaRegion.regionCode,
                            'text' : value.MstAreaRegion.regionName
                        }));
                    });                
                }
                else {
                    $("#region").html('<option value="">--No Region Found--</option>');
                }
                
            });            
        });

        $("#region").on('change', function () {
            var id = $(this).val();
            var url = '/admin/areas/getAllAreaStatesByRegion/' + $(this).val();
            $.getJSON(url, function(data) {
                if(data.length > 0) {
                    $("#state").html('<option value="">--select--</option>');
                        $.each(data, function(key, value) {
                        $("#state").append($('<option>', {
                            'value': value.MstAreaState.stateCode,
                            'text' : value.MstAreaState.stateName
                        }));
                    });                
                }
                else {
                    $("#state").html('<option value="">--No State Found--</option>');
                }
                
            });
            $("#city").html('<option value="">--select state--</option>');
            $("#local").html('<option value="">--select city--</option>');
            /*if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "countries", "action" => "getstates")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('#manger_state').html(html);
                    }
                }).html;
            }*/
        });
        $('#state').on('change', function() {
            var url = '/admin/areas/getAllAreaCitiesByState/' + $(this).val();
            $.getJSON(url, function(data) {
                if(data.length > 0) {
                    $("#city").html('<option value="">--select--</option>');
                        $.each(data, function(key, value) {
                        $("#city").append($('<option>', {
                            'value': value.MstAreaCity.cityCode,
                            'text' : value.MstAreaCity.cityName
                        }));
                    });                
                }
                else {
                    $("#city").html('<option value="">--No City Found--</option>');
                }
                
            });            
        });
        $('#city').on('change', function() {
            var url = '/admin/areas/getAllAreaLocalitiesByCity/' + $(this).val();
            $.getJSON(url, function(data) {
                if(data.length > 0) {
                    $("#local").html('<option value="">--select--</option>');
                        $.each(data, function(key, value) {
                        $("#local").append($('<option>', {
                            'value': value.MstAreaLocality.localityCode,
                            'text' : value.MstAreaLocality.localityName
                        }));
                    });                
                }
                else {
                    $("#local").html('<option value="">--No Locality Found--</option>');
                }
                
            });            
        });

    });

    /*function getcity(id)
    {
        $("#city").find('option').remove();
        $("#local").find('option').remove();
        if (id) {
            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: '<?php echo Router::url(array("controller" => "countries", "action" => "getCities")); ?>',
                data: dataString,
                cache: false,
                success: function (html) {
                    $('#manger_city').html(html);
                }
            }).html;
        }
    }

    function getlocal(id)
    {

        $("#local").find('option').remove();
        if (id) {
            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: '<?php echo Router::url(array("controller" => "countries", "action" => "getLocality")); ?>',
                data: dataString,
                cache: false,
                success: function (html) {
                    $('#manger_local').html(html);
                }
            }).html;
        }
    }*/
</script>
<script>
    $(document).ready(function () {
        $(".get_manger").on('change', function () {
            var department = $("#department").val();
            var role = $("#role").val();
            $("#manager").find('option').remove();
            if (department != '' && role != '') {


                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "areas", "action" => "getuser")); ?>',
                    data: {department: department, role: role},
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#manager"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#manager"));
                        });
                    }
                })
            }
        });
    });
</script>
<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Add AreaWise Assignment</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('Area', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>
                        <div class="control-group">
                            <label class="control-label">Department</label>
                            <div class="controls">
                                <?php echo $this->Form->input('employee_department_id', array('id' => 'department', 'options' => $employeedepartment, 'empty' => 'Select', 'class' => 'chzn-select small get_manger required', 'title' => 'Please Select Department', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">
                                <?php echo $this->Form->input('employee_role_id', array('id' => 'role', 'options' => $employeerole, 'empty' => 'Select', 'class' => 'chzn-select small get_manger required', 'title' => 'Please Select Role', 'label' => false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">User Name</label>
                            <div class="controls">
                                <?php echo $this->Form->input('username', array('div' => false, 'label' => false, 'hiddenField' => false, 'title' => 'Please Select User Name', 'class' => 'chzn-select small required', 'empty' => 'Select', 'options' => $user_list, 'id' => 'manager')); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Country</label>

                            <div class="controls">
                            <?php 
                            $properties = array(
                                'id' => 'country_id', 
                                'options' => $countryList, 
                                'class' => 'small', 
                                'title' => 'Please Select Department', 
                                'label' => false
                            );
                            echo $this->Form->input('country_id', $properties); 

                            ?>        
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Region</label>   
                            <div class="controls"><?php echo $this->Form->input('region_id', array('id' => 'region', 'options' => $region, 'empty' => 'Select', 'class' => 'small', 'title' => 'Please Select Role', 'label' => false)); ?></div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">State</label>   
                            <div class="controls">
                                <?php 
                                    $properties = array(
                                        'id' => 'state', 
                                        'empty' => 'Select', 
                                        'class' => 'small', 
                                        'title' => 'Please Select Manager', 
                                        'label' => false, 
                                        'hiddenfield' => false
                                    );
                                    echo $this->Form->input('state_id', $properties); ?>
                                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">City</label>   
                            <div class="controls" id="manger_city"> <?php echo $this->Form->input('city_id', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'small', 'options' => '', 'id' => 'city', 'empty' => 'Select')); ?></div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Local</label>   
                            <div class="controls" id="manger_local"> <?php echo $this->Form->input('local_id', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'small', 'options' => '', 'id' => 'local', 'empty' => 'Select')); ?></div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Add</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>			

