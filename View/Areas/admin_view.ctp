<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3> AreaWise Assignment</h3>
                </div>
                <div class="widget-container">
                    <?php if (!empty($countryarea)) { ?>						
                        <div class="control-group">
                            <label class="control-label"><h4>Country</h4></label>
                            <div class="controls">						
                                <?php
                                foreach ($countryarea as $country) {
                                    echo $country['AreaCountryMaster']['countryName'];
                                    ?>  <i class=" icon-remove-sign"></i><?php
                                    echo"<br/>";
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($regionarea)) { ?>
                        <div class="control-group">
                            <label class="control-label"><h4>Region</h4></label>
                            <div class="controls">	
                                <?php
                                foreach ($regionarea as $region) {
                                    echo $region['AreaCountryMaster']['countryName'];
                                    ?> >><?php echo $region['AreaRegionMaster']['RegionName']; ?>  <i class=" icon-remove-sign"></i><?php
                                    echo"<br/>";
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($statearea)) { ?>
                        <div class="control-group">
                            <label class="control-label"><h4>State</h4></label>
                            <div class="controls">	
                                <?php
                                foreach ($statearea as $state) {
                                    echo $state['AreaCountryMaster']['countryName'];
                                    ?> >  <?php echo $state['AreaRegionMaster']['RegionName']; ?> >>  <?php echo $state['AreaStateMaster']['stateName']; ?>  <i class=" icon-remove-sign"></i><?php
                                    echo"<br/>";
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($city_area)) { ?>
                        <div class="control-group">
                            <label class="control-label"><h4>City</h4></label>
                            <div class="controls">	
                                <?php
                                foreach ($city_area as $city) {
                                    echo $city['AreaCountryMaster']['countryName'];
                                    ?> > > > <?php echo $city['AreaRegionMaster']['RegionName']; ?> > > > <?php echo $city['AreaStateMaster']['stateName']; ?> > > > <?php echo $city['AreaCityMaster']['cityname']; ?>  <i class=" icon-remove-sign"></i><?php
                                    echo"<br/>";
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($local_area)) { ?>
                        <div class="control-group">
                            <label class="control-label"><h4>Location</h4></label>
                            <div class="controls">
                                <?php
                                foreach ($local_area as $local) {
                                    echo $local['AreaCountryMaster']['countryName'];
                                    ?> > > > <?php echo $local['AreaRegionMaster']['RegionName']; ?> > > > <?php echo $local['AreaStateMaster']['stateName']; ?> > > > <?php echo $local['AreaCityMaster']['cityname']; ?> > > > <?php echo $local['AreaLocalMaster']['localityName']; ?>  <i class=" icon-remove-sign"></i><?php
                                    echo"<br/>";
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>