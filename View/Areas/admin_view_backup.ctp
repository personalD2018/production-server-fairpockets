<div class="row-fluid">

    <!-- Table widget -->
    <div class="widget  span12 clearfix">

        <div class="widget-header">
            <span><i class="icon-table"></i>Employee Area</span>
        </div><!-- End widget-header -->	

        <div class="widget-content">
            <?php echo $this->Flash->render(); ?>

            <!-- Table UITab -->
            <div id="UITab" style="position:relative;">
                <ul class="tabs" >
                    <li ><a href="#tab1">List</li>  
                    <!-- <li ><a href="#tab2"> inTab</a></li>   -->         
                </ul>
                <div class="tab_container" >

                    <div id="tab1" class="tab_content"> 
                        <div class="btn-group pull-top-right btn-square">

                        </div>
                        <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                            <thead align="center">
                                <tr>

                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Region</th>
                                    <th>State</th>
                                    <th>City</th>
                                    <th>Local</th>

                                    <th>Management</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                <?php
                                foreach ($viewAreauser as $area => $value) {
                                    ?>
                                    <tr>

                                        <td><?php echo $value; ?></td>
                                        <td onclick="return last_msg_funtion();" class="message_box" >View</td>
                                        <td onclick="return last_msg_funtion();" class="message_box" >View</td>
                                        <td onclick="return last_msg_funtion();" class="message_box" >View</td>
                                        <td onclick="return last_msg_funtion();" class="message_box" >View</td>
                                        <td onclick="return last_msg_funtion();" class="message_box" >View</td>

                                        <td>
                                            <span class="tip" ><a href="<?php echo $this->webroot; ?>admin/areas/assignArea/<?php echo $area; ?>" title="Edit" ><img src="<?php echo $this->webroot; ?>backend/images/icon/icon_edit.png" ></a></span> 

                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div><!--tab2-->

                </div>
            </div><!-- End UITab -->
            <div class="clearfix"></div>

        </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->

</div><!-- row-fluid -->