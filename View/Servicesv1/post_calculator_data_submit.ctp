<?php
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'PDF', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
//$pdf->SetTitle('TCPDF Example 006');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(2, 2, 2);
$pdf->SetHeaderMargin(9);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 15);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();

// create some HTML content
//$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
$pdf->Image('https://www.fairpockets.com/upload/builder_logo/'.$builderlogo, 5, 6, 45, 16, 'JPG', 'https://www.fairpockets.com/', '', true, 150, '', false, false, 1, false, false, false);
$pdf->Image('https://www.fairpockets.com/upload/builder_logo/logo.jpg', 160, 6, 45, 16, 'JPG', 'https://www.fairpockets.com/', '', true, 150, '', false, false, 1, false, false, false);

if(isset($PdfImageForAppByProjectId) && $PdfImageForAppByProjectId != ''){
	$pdf->Image('https://www.fairpockets.com/upload/project_media/'.$PdfImageForAppByProjectId, 5, 40, 200, 240, 'JPG', 'https://www.fairpockets.com/', '', true, 150, '', false, false, 2, false, false, false);	
}


$html = '
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
	<tr>
		<td style="background-color:#eaeaea">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="20">&nbsp;</td>
				
				<td width="600">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
				<td>&nbsp;</td>
				<td height="20">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
					 <tr>										  
						<td width="180" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000; ">
						
						</td>
						<td width="340" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000; ">
						<font style="padding-top:42px;">'.$projectName.'</font> <br><br>
						<font style="font-size:12px;">Price Breakup From Fair Pockets - '.date('l jS  Y').'</font>
						</td>
						<td width="170" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000; ">
						
						</td>
					  </tr>
					  			  
					</table>
				</td>
				<td width="22">&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td height="10">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
		</td>
	  </tr>';
	 if(isset($PdfImageForAppByProjectId) && $PdfImageForAppByProjectId != ''){ 
	  $html.='<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
	  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>';
	 }
	  
	  $html.='<tr>
		<td>
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333;"><strong>Dear '.$visitorName.',</strong></td>
					  </tr>
					</table>
				</td>
			  </tr>
			  <tr>
				<td height="10"></td>
			  </tr>
			  <tr>
			  <td width="20">&nbsp;</td>
				<td width="580">Price Breakup for '.$projectName.' By '.$buildername.' , The above values are indicative and subject to change with the changes in builder prices or govt charges.</td>
			  </tr>
			  <tr>
				<td height="30">&nbsp;</td>
			  </tr>
			  <tr>
			  <td width="20">&nbsp;</td>
				<td width="220">
					<table align="left" width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Project Name<br>
						 - <font style="font-weight:bold; margin-left:10px; font-size:14px;">'.$projectName.'</font></td>
					  </tr>
					  
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Rera Id<br>
						 - <font style="font-weight:bold; margin-left:10px; font-size:14px;">'.$getreraByProjectId.'</font></td>
					  </tr>
					  
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Base Rate<br>
						  - <font style="font-weight:bold; margin-left:10px; font-size:14px;">Rs '.number_format($baseRate).'</font></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Property Area<br>
						  - <font style="font-weight:bold; margin-left:10px; font-size:14px;">'.$propertyArea.' per sqft</font></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Property on floor<br>
						  - <font style="font-weight:bold; margin-left:10px; font-size:14px;"> '.$propertyOnFloor.' </font></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  
					  
					  
					 
					  
					  <tr><td width="10" bgcolor="#fff2cd">&nbsp;</td>
						<td bgcolor="#fff2cd" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:20px;">
						Sales Name: <strong style="font-weight:bold;">'.$userName.'</strong>
						</td>
					  </tr>
					  <tr><td width="10" bgcolor="#fff2cd">&nbsp;</td>
						<td bgcolor="#fff2cd" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:20px;">
						Sales Contact: <strong style="font-weight:bold;">'.$userMobile.'</strong>
						</td>
					  </tr>
					  
					  
					  
					</table>
				</td>
				<td width="680">
				<table width="60%" border="0" align="center" cellpadding="18" cellspacing="0">
                <tr>
                  <td bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000;"><strong>Type of charges</strong></td>
                  <td bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000;"><strong>Price</strong></td>
                </tr>
                <tr>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Basic charges</td>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($baseCharges).'</td>
                </tr>
                <tr>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Additional charges</td>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($additionalCharges).' </td>
                </tr>
                <tr>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">GST (Total)</td>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($gstTotal).'</td>
                </tr>
                <tr>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Stamp duty &amp; Registration charges</td>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($stampRegTotal).'</td>
                </tr>
                <tr>
                  <td bgcolor="#303030" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffc000;"><strong>Total Charges</strong></td>
                  <td bgcolor="#303030" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffc000;"><strong>Rs '.number_format($finalCalculatePrice).'</strong></td>
                </tr>
              </table>
				
				
				</td>
				<td width="20">&nbsp;</td>
				
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>';

 if(isset($ProjectPricingPaymentPlan) && !empty($ProjectPricingPaymentPlan)){
	$html.=  
	 '<tr>
	 <td width="3%">&nbsp;</td>
		<td width="90%">
			<table width="100%" border="" align="center" cellpadding="2" cellspacing="2">
				<tr>
					<td width="180" style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;"><strong>Payment Plans : </strong></td>					
					<td width="180">&nbsp;</td>
					<td width="180">&nbsp;</td>
				</tr>
				<tr>
					<td width="180">&nbsp;</td>
					<td width="180">&nbsp;</td>
					<td width="180">&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="1" align="center" cellpadding="2" cellspacing="2">			
				<tr>
					<th width="180" bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">Base charges</th>
					<th width="150" bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">% Amount</th>
					<th width="180" bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">Other Charges</th>
					<th width="150" bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">% Amount</th>
				</tr>
			';
			 for($i=0;$i<=count($ProjectPricingPaymentPlan)-1;$i++) {				 
				 $plan_bc_amount_calculated = ($bspTotal*$ProjectPricingPaymentPlan[$i]['ProjectPricingPaymentPlan']['plan_bc_amount'])/100;
				 $plan_tc_amount = ($otherChargeTotal*$ProjectPricingPaymentPlan[$i]['ProjectPricingPaymentPlan']['plan_bc_amount'])/100;
				 
				 $sum1 += $plan_bc_amount_calculated;
				 $sum2 += $plan_tc_amount;
				 $sum3 = $sum1+$sum2+$regChargesTotal;
			$html.=
			  '<tr>
				<td width="180">'.$ProjectPricingPaymentPlan[$i]['ProjectPricingPaymentPlan']['plan_base_charge'].'</td>
				<td width="150">Rs '.number_format($plan_bc_amount_calculated).'</td>
				<td width="180">'.$ProjectPricingPaymentPlan[$i]['ProjectPricingPaymentPlan']['plan_total_charges'].'</td>
				<td width="150">Rs '.number_format($plan_tc_amount).'</td>
			  </tr>';
			 }
			 $html.=
			  '<tr>
				<td width="180" bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Total Base Charges</td>
				<td width="150" bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Rs '.number_format($sum1).'</td>
				<td width="180" bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Total Other Charges</td>
				<td width="150" bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Rs '.number_format($sum2).' Rs</td>
			  </tr>';
			  
			  $html.=
			  '<tr>				
					<td colspan="4" bgcolor="#303030" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffc000; border-right:1px solid #bbb; border-left:1px solid #bbb; text-align:right;">Registration Charges Amount : Rs '.number_format($regChargesTotal).'</td>
			  </tr>';
			  
			  $html.=
			  '<tr>				
					<td colspan="4" bgcolor="#303030" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffc000; border-right:1px solid #bbb; border-left:1px solid #bbb; text-align:right;">Total Calculated Price : Rs '.number_format($TotalcalculatedPrice).'</td>
			  </tr>';
			  
			  
	$html.=	'</table>
		</td>
		<td width="2%">&nbsp;</td>
	</tr>';
	
 }
 
 if(isset($PdfImageForAppByProjectId) && $PdfImageForAppByProjectId != ''){
 $html.='
 
 <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  
 
 ';
 
 }elseif(isset($ProjectPricingPaymentPlan) && !empty($ProjectPricingPaymentPlan)){
 
 $html.='
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  
			  
 
 ';
 }else{
	$html.='
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  
 
 '; 
	 
 }
			  
			  $html.='
			  
			  
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">&nbsp;
				</td>
			  </tr>
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;"><strong>Disclaimer :-</strong></td>
					  </tr>
					</table>
				</td>
			  </tr>
			  <tr>
					<td width="650" height="10"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="650" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">1. Stamp duty, registration and other government charges are payable, based on the then prevailing guidelines. The above values are indicative and subject to change with the changes in rates of taxation.</td>
				  </tr>
				  <tr>
					<td width="650"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="650"style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">2. Prices are subject to change by the builder without any prior notice.</td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="650" height="10"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="650" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">3. The PLC for may vary for that particular unit, additional power backup, car parking charges will be charges extra.</td>
				</tr>
				<tr>
					<td width="20">&nbsp;</td>
					<td width="650" height="10"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="650" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">4. Customers are expected to consider the prices indicated here at their sole discretion. Purvanchal or Fairpockets shall not be liable under any circumstances for any decisions taken by the customer based on the indicative pricing calculated here. The pricing initiative is to merely educate the customer on the components of standard pricing by the builder as per the prevailing laws as interpreted by our team.</td>
				</tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  
			  
			</table>
		</td>
	  </tr>';
	 
if(isset($PdfImageForAppByProjectId) && $PdfImageForAppByProjectId != ''){	
$html.=
	'<tr><td height="630">
		<table>
		   <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		   
		   		  
		</table></td>
	</tr>';
}elseif(isset($ProjectPricingPaymentPlan) && !empty($ProjectPricingPaymentPlan)){
$html.=
	'<tr><td height="530">
		<table>
		   <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		   
		   		  
		</table></td>
	</tr>';

}else{
	$html.=
	'<tr><td height="40">
		<table>
		   <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		   
		   		  
		</table></td>
	</tr>';
}	
	  
	 $html.= 
	  '<tr>
		<td bgcolor="#29333d">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td width="30">&nbsp;</td>
				<td width="640" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#fff;">For any further enquiries, please email us at <strong style="color:#ffc000;">'.$builderIdEmail.'</strong></td>
				
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
		</td>
	</tr>
 </table>';

$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table



//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/app/webroot/files/workorders/project-'.$linkid.'.pdf', 'F');

//============================================================+
// END OF FILE
//============================================================+
