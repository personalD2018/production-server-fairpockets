<!--./disable tab button-->
<script>
    $(document).ready(function ()
    {
        $("#submit_disable").hide(); //submit button disable by default
        $("#property_detail_button").css("display", "none");
        $("#property_pricing_button").css("display", "none");
        $("#property_amenities_button").css("display", "none");
        $("#property_photo_button").css("display", "none");

        $("input[id='offer_price']").on('keyup', function (e) {

            //alert ( toWords(this.value) );
            $("div[id='offer_price_error']").css('display', 'inline');
            $("div[id='offer_price_error']").text(convert_number(this.value));

            if (e.which === 13) {

                //Disable textbox to prevent multiple submit
                //$(this).attr("disabled", "disabled");

                //Do Stuff, submit, etc..
            }
        });

        $("input[id='market_price']").on('keyup', function (e) {

            //alert ( toWords(this.value) );
            $("div[id='market_price_error']").css('display', 'inline');
            $("div[id='market_price_error']").text(convert_number(this.value));

            if (e.which === 13) {

                //Disable textbox to prevent multiple submit
                //$(this).attr("disabled", "disabled");

                //Do Stuff, submit, etc..
            }
        });

    });
</script>

<!--Get property aminities and feature by selecting of property option --> 
<script>
    $(document).ready(function () {

        $("#property_type_val").change(function ()
        {
            var id = $(this).val();
            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyfeature")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#property_feature_ajax').html(html); // show response from the php script.
                        $("#cover_parking_form").hide();// cover Parking disable by default
                        $("#open_parking_form").hide();// open Parking disable by default
                    }
                })


                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyaminities")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#aminities_ajax').html(html); // show response from the php script.
                    }
                })
            }
        });
    });
</script>



<!-- jQuery Form Validation code and submit basic code-->
<script>

// When the browser is ready...
    $(function () {
        var url = "/aliveasset/Properties/addproperty"; // the script where you handle the form input.
        // Setup form validation on the #register-form element
        $("#idForm").validate({

            rules: {
                "data[pincode]": {
                    required: true,
                    minlength: 6,
                    digits: true
                },
                agree: "required"
            },

            // Specify the validation error messages
            messages: {

                "data[pincode]": {

                    minlength: "Your Pincode must be at least 6 characters long",
                    digits: "Please Enter Only Numeric Data"
                }
            },

            errorPlacement: function (error, element) {

                if (element.attr("name") == "data[country]") {

                    error.appendTo("#country_id_error");
                }
                if (element.attr("name") == "data[property_type]") {

                    error.appendTo("#property_type_error");
                }
                if (element.attr("name") == "data[state]") {

                    error.appendTo("#state_error");
                }
                if (element.attr("name") == "data[city]") {


                    error.appendTo("#city_error");
                }
                if (element.attr("name") == "data[locality]") {

                    error.appendTo("#locality_error");
                }
                if (element.attr("name") == "data[address]") {

                    error.appendTo("#address_error");
                }
                if (element.attr("name") == "data[pincode]") {

                    error.appendTo("#pincode_error");
                }
                if (element.attr("name") == "data[project_id]") {

                    error.appendTo("#project_id_error");
                }
            },

            submitHandler: function (form) {
                $.ajax
                        ({
                            type: "POST",
                            url: url,
                            data: $("#idForm").serialize(), // serializes the form's elements.
                            success: function (data)
                            {
                                $(".property_id").val(data);
                                $("#property_detail_button").css("display", "block");
                                $("#idForm :input").prop("disabled", true);
                                $("#next1").html('Saved');

                            }
                        });
                return false;
            }
        })

    });

</script>


<!-- jQuery Form Validation code and submit Property Details Form-->
<script>

// When the browser is ready...
    $(function () {
        var url = "/aliveasset/Properties/addpropertyfeaturedetail"; // the script where you handle the form input.

        // Setup form validation on the #register-form element
        $("#property_feature_form").validate({

            rules: {
                "data[Feature][sbua]": {
                    required: true,
                    number: true,
                    min: 1
                },
                "data[Feature][build_up_area]": {
                    number: true,
                    min: 1
                },
                "data[Feature][carpet_area]": {
                    number: true,
                    min: 1
                },
                "data[Feature][plot_area]": {
                    required: true,
                    number: true,
                    min: 1
                },
                "data[Feature][age_of_property]": {
                    number: true,
                    min: 1,
                    max: 200
                },

            },

            // Specify the validation error messages
            messages: {

                "data[Feature][sbua]": {

                    number: "Enter Only Numeric",
                    min: "Super Built UP Area must be greater than 0"
                },
                "data[Feature][build_up_area]": {

                    number: "Enter Only Numeric",
                    min: "Built UP Area must be greater than 0"
                },
                "data[Feature][carpet_area]": {

                    number: "Enter Only Numeric",
                    min: "Carpet must be greater than 0"

                },
                "data[Feature][plot_area]": {

                    number: "Enter Only Numeric",
                    min: "Plot area must be greater than 0"
                },
                "data[Feature][age_of_property]": {

                    number: "Enter Only Numeric",
                    min: "Age must be greater than 0",
                    max: "Age Not be Less than 200"
                }
            },

            errorPlacement: function (error, element)
            {

                if (element.attr("name") == "data[Feature][sbua]") {

                    error.appendTo("#super_built_error");
                }
                if (element.attr("name") == "data[Feature][build_up_area]") {

                    error.appendTo("#build_up_area_error");
                }
                if (element.attr("name") == "data[Feature][plot_area]") {

                    error.appendTo("#plot_area_error");
                }

                if (element.attr("name") == "data[Feature][carpet_area]") {

                    error.appendTo("#carpet_area_error");
                }
                if (element.attr("name") == "data[Feature][bedrooms]") {


                    error.appendTo("#bedrooms_error");
                }
                if (element.attr("name") == "data[Feature][bathrooms]") {

                    error.appendTo("#bathrooms_error");
                }
                if (element.attr("name") == "data[Feature][balconies]") {

                    error.appendTo("#balconies_error");
                }

                if (element.attr("name") == "data[Feature][property_description]") {

                    error.appendTo("#property_description_error");
                }
                if (element.attr("name") == "data[Feature][age_of_property]") {

                    error.appendTo("#age_error");
                }

            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#property_feature_form").serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        $("#property_pricing_button").css("display", "block");
                        ; // show response from the php script.
                        $("#property_feature_form :input").prop("disabled", true);
                        $("#next2").html('Saved');
                    }
                });
                return false;
            }
        })

    });

</script>

<!-- jQuery price Form Validation code and submit pricing tab-->
<script>



// Script for add property price
    $(function ()
    {
        var url = "/aliveasset/Properties/addpropertyPrice"; // the script where you handle the form input.
        // Setup form validation on the #register-form element
        jQuery.validator.addMethod('greaterThan', function (value, element) {
            return (+$("#market_price").val() > +$("#offer_price").val());
        }, 'Must be greater than start');

        jQuery.validator.addMethod('LesserThan', function (value, element) {
            return (+$("#offer_price").val() < +$("#market_price").val());
        }, 'Must be Lesser than start');


        $("#form_price").validate({

            rules: {
                "data[offer_price]": {
                    required: true,
                    number: true,
                    LesserThan: true,
                    min: 1
                },
                "data[market_price]": {
                    required: true,
                    number: true,
                    greaterThan: true,
                    min: 1
                },
                "data[booking_amount]": {
                    number: true,
                    min: 1
                },
                "data[deposit]": {
                    number: true,
                    min: 1
                },

                "data[offer_price_sqt]": {
                    number: true
                },
                "data[market_price_sqt]": {
                    number: true
                },
                "data[maintance_amount]": {
                    number: true,
                    min: 1
                }
            },

            // Specify the validation error messages
            messages: {

                "data[offer_price]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    LesserThan: "Offer Price must be Lesser Than Market Price"

                },

                "data[market_price]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    greaterThan: "Market Price must be Greater Than Offer Price"
                },

                "data[booking_amount]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                },
                "data[deposit]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                },

                "data[offer_price_sqt]": {

                    number: "Enter Only Numeric"
                },
                "data[market_price_sqt]": {

                    number: "Enter Only Numeric"
                },
                "data[maintance_amount]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                }


            },

            errorPlacement: function (error, element) {

                if (element.attr("name") == "data[offer_price]") {

                    error.appendTo("#offer_price_error");
                }
                if (element.attr("name") == "data[market_price]") {

                    error.appendTo("#market_price_error");
                }
                if (element.attr("name") == "data[booking_amount]") {

                    error.appendTo("#booking_amount_error");
                }
                if (element.attr("name") == "data[offer_price_sqt]") {

                    error.appendTo("#offer_price_sqt_error");
                }
                if (element.attr("name") == "data[market_price_sqt]") {

                    error.appendTo("#market_price_sqt_error");
                }
                if (element.attr("name") == "data[maintance_amount]") {

                    error.appendTo("#maintance_amount_error");
                }
                if (element.attr("name") == "data[construction_status]") {


                    error.appendTo("#construction_status_error");
                }
                if (element.attr("name") == "data[possession_date]") {

                    error.appendTo("#possession_date_error");
                }
                if (element.attr("name") == "data[ownership_type]") {

                    error.appendTo("#ownership_type_error");
                }
                if (element.attr("name") == "data[sale_type]") {

                    error.appendTo("#sale_type_error");
                }
                if (element.attr("name") == "data[deposit]") {

                    error.appendTo("#deposit_error");
                }
            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form_price").serialize(), // serializes the form's elements.
                    success: function (data)
                    {

                        $("#property_amenities_button").css("display", "block");
                        $("#form_price :input").prop("disabled", true);
                        $("#next3").html('Saved');
                        $("#submit_disable").show(); //Show submit button
                    }
                });
                return false;
            }
        })

    });

</script>


<!-- Property for on change enable/disable price option -->
<script>
    $(document).ready(function ()
    {
        $("#transaction_type").change(function () {
            var id = $(this).val();
            $("#property_type_val").find('option').removeAttr("selected");
            if (id == '2')
            {
                $('#bookamount').css('display', 'none');
                $('#depositamount').css('display', 'block');
                $('#expected_monthly_show').css('display', 'block');
                $('#offer_price_show').css('display', 'none');
                $('#expected_market_rent_show').css('display', 'block');
                $('#market_price_show').css('display', 'none');
                $("#property_type_val option[value='3']").hide(); // hide residential land
                $("#property_type_val option[value='27']").hide(); // hide Commercial land
                $("#disable_on_rent").hide(); // hide floor construction 
                $("#anmities_hide_rent").show();//hide amenities (pet,veg,Bachelors)


            }
            if (id == '1')
            {
                $('#bookamount').css('display', 'block');
                $('#depositamount').css('display', 'none');
                $('#expected_monthly_show').css('display', 'none');
                $('#offer_price_show').css('display', 'block');
                $('#expected_market_rent_show').css('display', 'none');
                $('#market_price_show').css('display', 'block');
                $("#property_type_val option[value='3']").show(); //show residential land
                $("#property_type_val option[value='27']").hide(); // show Commercial land
                $("#disable_on_rent").show(); // show floor construction
                $("#anmities_hide_rent").hide();//hide amenities (pet,veg,Bachelors)
            }
        });
    });
</script>

<!-- submit aminities form -->
<script>
    $(function () {

        var url = "/aliveasset/Properties/addpropertyamenities"; // the script where you handle the form input.

        // Setup form validation on the #register-form element
        $("#aminities_form").validate({

            rules: {
                "data[Feature][width_of_fac_road]": {
                    number: true,
                    min: 1
                }


            },

            // Specify the validation error messages
            messages: {

                "data[Feature][width_of_fac_road]": {

                    number: "Enter Only Numeric",
                    min: "Width of facing must be greater than 0"
                }
            },

            errorPlacement: function (error, element)
            {

                if (element.attr("name") == "data[Feature][width_of_fac_road]") {

                    error.appendTo("#width_of_fac_road_error");
                }

            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#aminities_form").serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        $("#property_photo_button").css("display", "block");
                        ; // show response from the php script.
                        $("#aminities_form :input").prop("disabled", true);
                        $("#next4").html('Saved');

                    }
                })
                return false;
            }
        })

    });

</script> 



<script>

    function number2text(value)
    {
        var fraction = Math.round(frac(value) * 100);
        var f_text = "";

        if (fraction > 0)
        {
            f_text = "AND " + convert_number(fraction) + " PAISE";
        }

        return convert_number(value) + " RUPEE " + f_text + " ONLY";
    }

    function frac(f)
    {
        return f % 1;
    }

    function convert_number(number)
    {
        if ((number < 0) || (number > 999999999))
        {
            return "NUMBER OUT OF RANGE!";
        }

        var Gn = Math.floor(number / 10000000);  /* Crore */
        number -= Gn * 10000000;
        var kn = Math.floor(number / 100000);     /* lakhs */
        number -= kn * 100000;
        var Hn = Math.floor(number / 1000);      /* thousand */
        number -= Hn * 1000;
        var Dn = Math.floor(number / 100);       /* Tens (deca) */
        number = number % 100;               /* Ones */
        var tn = Math.floor(number / 10);
        var one = Math.floor(number % 10);
        var res = "";

        if (Gn > 0)
        {
            res += (convert_number(Gn) + " CRORE");
        }
        if (kn > 0)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " LAKH");
        }
        if (Hn > 0)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " THOUSAND");
        }

        if (Dn)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " HUNDRED");
        }


        var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN");
        var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY");

        if (tn > 0 || one > 0)
        {
            if (!(res == ""))
            {
                res += " AND ";
            }
            if (tn < 2)
            {
                res += ones[tn * 10 + one];
            } else
            {

                res += tens[tn];
                if (one > 0)
                {
                    res += ("-" + ones[one]);
                }
            }
        }

        if (res == "")
        {
            res = "ZERO";
        }

        return res;
    }
</script>


<!-- css style to disable button -->

<style>
    .btn-primary.disabled, .btn-primary[disabled] {
        background: #ffc000;
        color: #333;
        outline: none;
        box-shadow: none;
    }
    .add-tab-content .add-tab-row {
        border-bottom: none;
    }
</style> 
<!--./advertisement-Card-->
<section class="services_details no-margin">
    <div class="container">
        <div class="center wow fadeInDown no-padding-bottom">
            <h2>Post Your Properties</h2>
            <p class="lead">We've over 15 Lac buyers for you!
                It's Easy...It's Simple.. 
                <br> & Yes It's FREE*
            </p>
        </div>

        <div class="add_property_form">
            <form class="fpform" id="idForm">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Basic Information</h3>
                        <div class="add-expand active"></div>
                    </div>
                    <div class="add-tab-content" style="display:block;">
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">List Property for</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('transaction_type', array('id' => 'transaction_type', 'options' => array('1' => 'Sell', '2' => 'Rent'), 'class' => 'selectpicker', 'placeholder' => 'Select Property for', 'label' => false)); ?>

                                        </div>		
                                        <div class="error has-error" id="transaction_type_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property_type" class="label_title">Property type</label>
                                        <div class="select-style ">
                                            <?php echo $this->Form->input('property_type_op', array('id' => 'property-type', 'options' => array('1' => 'Residential', '2' => 'Commercial'), 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" >Box must be filled out</div>



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Property Type Option<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('property_type', array('id' => 'property_type_val', 'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'), 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="property_type_error" style="display:block;"></div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="add-tab-row  push-padding-bottom">
                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="city" class="label_title">Country<span class="mand_field">*</span></label>

                                        <div class="select-style">
                                            <?php echo $this->Form->input('country', array('id' => 'country', 'options' => array('1' => 'India'), 'empty' => 'Select Country', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select State', 'title' => 'Please Select Country', 'label' => false)); ?>
                                        </div>		



                                        <!-- <div id="selction-ajax"></div> -->
                                        <div class="error has-error" style="display:block;" id="country_id_error"></div>
                                    </div>

                                </div> 
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">State<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('state', array('id' => 'state', 'options' => $states, 'empty' => 'Select State', 'class' => 'selectpicker bs-select-hidden required', 'title' => 'Please Select State', 'placeholder' => 'Select State', 'label' => false)); ?>
                                        </div>
                                        <div class="error has-error" id="state_error" style="display:block;"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="city" class="label_title">City<span class="mand_field">*</span></label>

      <!-- <input class="form-control" type="text" name="city" placeholder="City" id="autocomplete-city" style=""/> -->
                                        <div class="select-style">
                                            <?php echo $this->Form->input('city', array('id' => 'city', 'options' => '$city', 'empty' => 'Select City', 'class' => 'selectpicker bs-select-hidden required', 'title' => 'Please Select City', 'placeholder' => 'Select City', 'label' => false)); ?>			
                                        </div>	 


                                        <!-- <div id="selction-ajax"></div> -->
                                        <div class="error has-error" id="city_error" style="display:block;"></div>
                                    </div>

                                </div> 
                            </div>
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">Select Locality<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('locality', array('id' => 'locality', 'options' => '$locality', 'empty' => 'Select Locality', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Locality', 'title' => 'Please Select Locality', 'label' => false)); ?>	
                                        </div>
                                        <div class="error has-error" id="locality_error" style="display:block;"></div>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">Address<span class="mand_field">*</span></label>
                                        <?php echo $this->Form->input('address', array('id' => 'address', 'class' => 'form-control error required', 'placeholder' => 'House No,Street No,etc.', 'title' => 'Please Insert Address', 'label' => false)); ?>	

                                        <div class="error has-error" id="address_error" style="display:block;"></div>
                                    </div>
                                </div>




                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pin" class="label_title">Pincode<span class="mand_field">*</span></label>
                                        <?php echo $this->Form->input('pincode', array('id' => 'pincode', 'class' => 'form-control error required', 'placeholder' => 'Pin Code', 'title' => 'Please Insert Pin Code', 'label' => false)); ?>

                                        <div class="error has-error" id="pincode_error" style="display:block;"></div>
                                    </div>
                                </div>

                                <div class="col-sm-6">


                                </div>



                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <div class="col-sm-12">
                                        <label for="pin">Project Name</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="select-style">
                                            <?php echo $this->Form->input('project_id', array('id' => 'project_id', 'options' => $project, 'empty' => 'Select Project', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select Project', 'title' => 'Please Select Project', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="project_id_error" style="display:block;"></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#addproject" class="button btn-add">Add Project</a>
                                    </div>



                                </div>
                            </div>
                            <div class="account-block text-center submit_area">
                                <button type="submit" class="btn btn-primary btn-next " id="next1">NEXT</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

            <form class="fpform" id="property_feature_form">
                <div class="account-block property_feature" >
                    <div class="add-title-tab">
                        <h3>Property Details</h3>
                        <div class="add-expand" id="property_detail_button"></div>
                    </div>
                    <div class="add-tab-content" >
                        <div class="add-tab-row push-padding-bottom" id="property_feature_ajax">

                        </div>
                    </div>
                </div>
            </form>
            <form class="fpform" id="form_price">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Pricing</h3>
                        <div class="add-expand" id="property_pricing_button"></div>
                    </div>
                    <div class="add-tab-content" id="property_pricing_ajax">

                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="offer_price_show">
                                        <label for="property-price" class="label_title">Offer Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Asking Price ( All Inclusive Price ). It has to be lower or equal to Market Price."></i>
                                        <?php echo $this->Form->input('offer_price', array('id' => 'offer_price', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Asking Price', 'title' => 'Please Enter Offer Price', 'label' => false)); ?>
                                        <div class="error has-error" id="offer_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_monthly_show" style="display:none">
                                        <label for="property-price" class="label_title">Expected Monthly Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Expected Monthy Rent ( All Inclusive Price ). It has to be lower or equal to Market Rent."></i>
                                        <?php echo $this->Form->input('expected_monthly', array('id' => 'expected_monthly', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Expected Monthly Rent', 'title' => 'Please Enter Monthly Rent', 'label' => false)); ?>
                                        <div class="error has-error" id="expected_monthly_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price" class="label_title">Offer Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('offer_price_sqt', array('id' => 'offer_price_sqt', 'class' => 'form-control box_price', 'label' => false)); ?>
                                        <div class="error has-error" id="offer_price_sqt_error" style="display:block"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="row ">


                                <div class="col-sm-4" id="">
                                    <div class="form-group" id="market_price_show">
                                        <label for="property-price-before" class="label_title">Market Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Current Approx. Market Price of your property. This will be validated by our experts."></i>

                                        <?php echo $this->Form->input('market_price', array('id' => 'market_price', 'class' => 'form-control box_price required', 'label' => false, 'placeholder' => 'Enter Approx Market Price', 'title' => 'Please Enter Market Price')); ?>

                                        <div class="error has-error" id="market_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_market_rent_show" style="display:none">
                                        <label for="property-price-before" class="label_title"> Expected Market Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Current Approx. Market Rent of your property. This will be validated by our experts."></i>

                                        <?php echo $this->Form->input('market_rent', array('id' => 'market_rent', 'class' => 'form-control box_price required', 'label' => false, 'placeholder' => 'Enter Approx Market Rent', 'title' => 'Please Enter Market Rent')); ?>

                                        <div class="error has-error" id="market_rent_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price" class="label_title">Market Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('market_price_sqt', array('id' => 'market_price_sqt', 'class' => 'form-control box_price', 'label' => false)); ?>
                                        <div class="error has-error" id="market_price_sqt_error" style="display:block"></div>												

                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-sm-4">
                                    <div class="form-group" id="bookamount">
                                        <label for="property-price-before" class="label_title">Booking Amount</label>
                                        <?php echo $this->Form->input('booking_amount', array('id' => 'booking_amount', 'class' => 'form-control box_price', 'title' => 'Please Enter Booking Amount', 'label' => false, 'placeholder' => 'Enter booking amount')); ?>

                                        <div class="error has-error" id="booking_amount_error" style="display:block"></div>
                                    </div>
                                    <div class="form-group" id="depositamount" style="display:none">
                                        <label for="property-price-before" class="label_title"> Security Deposit Amount</label>

                                        <?php echo $this->Form->input('deposit', array('id' => 'deposit-amount-before', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Security Deposit Amount')); ?>

                                        <div class="error has-error" id="deposit_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price-maintenance" class="label_title">Maintenance </label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Monthly recurring expenses "></i>
                                        <?php echo $this->Form->input('maintance_amount', array('id' => 'maintance_amount', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Maintenance Amount', 'title' => 'Please Enter Maintenance')); ?>
                                        <div class="error has-error" id="maintance_amount_error" style="display:block"></div>	
                                        <div class="error has-error">Box must be filled out</div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="add-tab-row push-padding-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Availability</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('construction_status', array('id' => 'constructuion_status', 'options' => array('1' => 'Ready to move in', '2' => 'Under Construction'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Construction Status', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" id="construction_status_error" style="display:block" ></div>



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Possession Date</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('possession_date', array('id' => 'possession_date', 'options' => array('3' => 'Within 3 Months', '6' => 'Within 6 Months'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Possession Date', 'label' => false)); ?>



                                        </div>	
                                        <div class="error has-error" id="possession_date_error" style="display:block" ></div>	

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Ownership Type</label>

                                        <div class="select-style">

                                            <?php echo $this->Form->input('ownership_type', array('id' => 'ownership_type', 'options' => array('1' => 'Freehold', '2' => 'Lease Hold', '3' => 'Power of attorney', '4' => 'Co-operative Society'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Ownership Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="ownership_type_error" style="display:block" ></div>	



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Sale Type</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('sale_type', array('id' => 'sale_type', 'options' => array('1' => 'Resale', '2' => 'New Booking'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Sale Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="sale_type_error" style="display:block" ></div>	



                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
                        <div class="account-block text-center submit_area">
                            <button type="submit" id="next3" class="btn btn-primary btn-next ">NEXT</button>
                        </div>  


                    </div>
                </div>
            </form>
            <form class="fpform" id="aminities_form">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Features and Amenities</h3>
                        <div class="add-expand" id="property_amenities_button"></div>
                    </div>
                    <div class="add-tab-content" id="aminities_ajax">


                    </div>

                </div>
            </form>

            <form class="fpform" method="post" enctype="multipart/form-data" action="upload_image" >
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Add Photos</h3>
                        <div class="add-expand" id="property_photo_button"></div>
                    </div>
                    <div class="add-tab-content">
                        <div class="add-tab-row">
                            <div class="property-media">
                                <div class="media-gallery">
                                    <div class="row">

                                        <div id="image_preview"></div> 

                                    </div>
                                </div>
                                <div class="account-block text-center">
                                    <div class="row">
                                        <div class="col-sm-12">	We recommend minimum image size to be 10KB and maximum size to be 5MB. Accepted file format are .png .jpg .jpeg</div>
                                    </div>
                                </div>
                                <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
                                <div class="upload_btn_area">

                                    <label class="btn btn-default btn-file">
                                        <?php
                                        echo $this->Form->input('upload_file.', ['type' => 'file', 'label' => false, 'multiple' => 'multiple', 'style' => 'display:none', 'onchange' => 'preview_image()']);
                                        ?>
                                        Add Property Picture 
                                    </label>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="account-block text-center submit_area" id="submit_disable">
                    <button type="submit" class="btn btn-primary btn-red">Submit Property</button>
                </div>
            </form>

            <div class="modal fade" id="addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                �</button>
                            <h4 class="modal-title" id="myModalLabel">
                                Add Project
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form class="fpform">
                                    <div class="col-md-12" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">



                                        <div class="add-tab-row push-padding-bottom ">
                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Project Name<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="projectname" placeholder="Enter your project name">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="projectname" class="label_title">Project Description<span class="mand_field">*</span></label>
                                                            <textarea class="form-control" rows="1" id="project_des" placeholder="Enter your project description"></textarea>
                                                            <div class="error has-error">Box must be filled out</div>
                                                        </div>
                                                    </div>
                                                </div >
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Highlights<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="project_highlight" placeholder="Enter your Highlights">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Acres<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="project_acre" placeholder="Enter project Acres">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>

                                        <div class="add-tab-row push-padding-bottom text-center">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <button type="button" class="btn btn-primary btn-red">
                                                        Save & Continue</button>
                                                </div>


                                            </div>

                                        </div>


                                    </div>



                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/our-products-->
    </div>
    <!--/.container-->

</section>

