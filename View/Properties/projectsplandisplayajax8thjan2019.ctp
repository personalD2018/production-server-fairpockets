<!--<?php //echo $this->Form->input('Feature.res_parking_1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_none', 'hiddenField' => false)); ?>

	<label for="id_chk_none" class="checkbox-custom-label" style="padding-right:160px;">None</label>

	<?php //echo $this->Form->input('Feature.res_parking_2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_cvrd', 'hiddenField' => false)); ?>
	<label for="id_chk_cvrd" class="checkbox-custom-label" style="padding-right:160px;" >Covered</label>


	<?php //echo $this->Form->input('Feature.res_parking_3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_open', 'hiddenField' => false)); ?>
	<label for="id_chk_open" class="checkbox-custom-label" style="padding-right:160px;" >Open</label>-->
	
	
<table id="plans_list" class="table table-striped table-bordered" style="width:100%">                
	<tbody>
		<?php  if (!empty($projectPricingPlansArr)) {
			for ($i = 0; $i < count($projectPricingPlansArr); $i++) {
		?>
		<?php echo $i % 4 == 0 ? "<tr>\n" : ""; ?>
			<td>
				<?php 
				if($projectPricingPlansArr[$i]['ProjectPricing']['payment_plan1'] != ''){
					$planName = $projectPricingPlansArr[$i]['ProjectPricing']['payment_plan1'];
				}else{
					$planName = 'Default Plan';
				}
				?>
				
				<input type="hidden" name="data[planchecklist][plan_id<?php echo $i; ?>]" 
				value="<?php echo $projectPricingPlansArr[$i]['ProjectPricing']['id']; ?>" id="plansel<?php echo $i; ?>">
							
				<?php echo $this->Form->input('planchecklist.plans_check'.$i, array(
						'id' => 'planchecklist.plans_check'.$i,
						'type' => 'checkbox',
						'div' => array('class' => 'col-sm-12'),
						'class' => 'checkbox-custom',
						'label' => array('class' => 'checkbox-custom-label', 'text' => $planName),
							)
					);
				?>
				
				
				
			</td>
			<?php echo $i % 4 == 3 ? "</tr>\n" : ""; ?>
			<?php }}else{ ?>
		<tr>
			<td colspan="4" style="text-align:center;">
			<input type="hidden" name="check_list_plans" value='<?php //echo $toweridStrings; ?>'>
				No Plan Available.</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
