<!-- css style to disable button -->

<style>
    .btn-primary.disabled, .btn-primary[disabled] {
        background: #ffc000;
        color: #333;
        outline: none;
        box-shadow: none;
    }
    .add-tab-content .add-tab-row {
        border-bottom: none;
    }
</style> 

<!--./advertisement-Card-->
<div class="page-content">
    
        <div class="center wow fadeInDown no-padding-bottom">
            <h2>Post Your Properties</h2>
            <p class="lead">We've over 15 Lac buyers for you!
                It's Easy...It's Simple.. 
                <br> & Yes It's FREE*
            </p>
        </div>

        <div class="add_property_form">
            <form class="fpform" id="idForm">

                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Basic information</h3>
                        <div class="add-expand active" id="property_basic_info"></div>
                    </div>
                    <div class="add-tab-content" style="display:block;" id="property_basic_content">
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">List Property for</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('transaction_type_id', array('id' => 'transaction_type', 'options' => array('1' => 'Sell'), 'class' => 'selectpicker', 'placeholder' => 'Select Property for', 'label' => false)); ?>

                                        </div>		
                                        <div class="error has-error" id="transaction_type_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property_type" class="label_title">Property type</label>
                                        <div class="select-style ">
                                            <?php echo $this->Form->input('property_type_op', array('id' => 'property-type', 'options' => array('1' => 'Residential', '2' => 'Commercial'), 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" >Box must be filled out</div>



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property_type_id" class="label_title">Property Type Option<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('property_type_id', array('id' => 'property_type_val', 'options' => $propertyoptiondata, 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="property_type_error" style="display:block;"></div>

                                    </div>
                                </div>
								
								<div class="add-tab-row  push-padding-bottom">
                             <div class="row">
                              
                              <div class="col-sm-4" style="display:none;">
                              <div class="form-group">
                              <label for="city" class="label_title">Country<span class="mand_field">*</span></label>

                              <div class="select-style">
                              <?php echo $this->Form->input('country', array('id' =>'country','options' => array('1' => 'India'),'type' => 'hidden','selected' =>1,'class' =>'selectpicker bs-select-hidden required','placeholder'=>'Select country','title'=>'Please Select Country','label' => false));?>
                              </div>



                              
                              <div class="error has-error" style="display:block;" id="country_id_error"></div>
                              </div>

                              </div>
                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="countryState" class="label_title">State<span class="mand_field">*</span></label>
                              <div class="select-style">
                              <?php echo $this->Form->input('state_data', array(
                                  'id' =>'state',
                                  'options' => $states,
                                  // 'empty' => 'Select State', 
                                  'class' =>'selectpicker bs-select-hidden required',
                                  'title'=>'Please Select State',
                                  'placeholder'=>'Select State',
                                  'label' => false
                                ));?>
                              </div>
                              <div class="error has-error" id="state_error" style="display:block;"></div>
                              </div>
                              </div>
                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="city" class="label_title">City<span class="mand_field">*</span></label>

                              <div class="select-style">
                              <?php echo $this->Form->input('property_city', array('id' =>'city','options' => $city_master,'empty' => 'Select City','class' =>'selectpicker bs-select-hidden required','title'=>'Please Select City','placeholder'=>'Select City','label' => false));?>
                              </div>


                              <div class="error has-error" id="city_error" style="display:block;"></div>
                              </div>

                              </div>
							  <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pin" class="label_title">Project Name<span class="mand_field">*</span></label>
                                    
                                        <div class="select-style">
                                            <?php echo $this->Form->input('project_id', array('id' => 'project_id', 'options' => $project, 'empty' => 'Select Project', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Project', 'title' => 'Please Select Project', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="project_id_error" style="display:block;"></div>
                                    </div>



                                </div>
                              </div>
                              <div class="row">

                              <!-- <div class="col-sm-4">
                              <div class="form-group">
                              <label for="countryState" class="label_title">Select Locality<span class="mand_field">*</span></label>
                              <div class="select-style">
                              
                              </div>
                              <div class="error has-error" id="locality_error" style="display:block;"></div>
                              </div>
                              </div> -->


                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="countryState" class="label_title">Address<span class="mand_field">*</span></label>
                              <?php echo $this->Form->input('address', array('id' =>'address','class' =>'form-control error required','placeholder'=>'House No,Street No,etc.','title'=>'Please Insert Address','label' => false));?>

                              <div class="error has-error" id="address_error" style="display:block;"></div>
                              </div>
                              </div>




                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="pin" class="label_title">Pincode<span class="mand_field">*</span></label>
                              <?php echo $this->Form->input('pincode', array('id' =>'pincode','class' =>'form-control error required','placeholder'=>'Pin Code','title'=>'Please Insert Pin Code','label' => false));?>

                              <div class="error has-error" id="pincode_error" style="display:block;"></div>
                              </div>
                              </div>

                              </div>
								<?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => $this->request->data['property_id'])); ?>
                            <div class="account-block text-center submit_area">
                                <button type="submit" class="btn btn-primary btn-next" id="next1">Save</button>
                            </div>
                            </div>
							
							
							
							
                        </div>

                    </div>
                </div>
            </form>

            <form class="fpform" id="property_feature_form">
                <div class="account-block property_feature" >
                    <div class="add-title-tab">
                        <h3>Property Details</h3>
                        <div class="add-expand" id="property_detail_button"></div>
                    </div>
                    <div class="add-tab-content" id="property_detail_content">
                        <div class="add-tab-row push-padding-bottom" id="property_feature_ajax">
                            <div class="row">
                                <?php
                                if (in_array("1", $propertyoption)) {
								if (array_key_exists(1, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][1];
									$feature_unit_val = $this->request->data['AllFeaturesUnit'][1];
								}
								else
								{
									$feature_val = '';
									$feature_unit_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">

                                        <div class="col-sm-12"><label class="label_title">Super Built Up Area<span class="mand_field">*</span> </label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the sum of Carpet area, Wall area and Common area"></i>
                                        </div>
                                        <div class="col-sm-6"><?php echo $this->Form->input('Feature.sbua', array('id' => 'sbua', 'class' => 'form-control required', 'placeholder' => '', 'label' => false, 'div' => false,'value' => $feature_val)); ?>
                                            <div class="error has-error" style="display:block" id="super_built_error"></div>

                                        </div>

                                        <div class="col-sm-3"><div class="select-style">
                                                <?php echo $this->Form->input('Feature.sbua_m', array('id' => 'sbua_m', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => 'Super Built Up Area', 'label' => false, 'value' => $feature_unit_val)); ?>

                                            </div>

                                        </div>



                                    </div>

                                <?php } ?>

                                <?php if (in_array("2", $propertyoption)) {
								if (array_key_exists(2, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][2];
									$feature_unit_val = $this->request->data['AllFeaturesUnit'][2];
								}
								else
								{
									$feature_val = '';
									$feature_unit_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">

                                        <div class="col-sm-12"><label class="label_title">Built Up Area</label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the sum of Carpet area and Wall area"></i>
                                        </div>
                                        <div class="col-sm-6"><?php echo $this->Form->input('Feature.build_up_area', array('id' => 'build_up_area', 'class' => 'form-control', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>
                                            <div class="error has-error" style="display:block" id="build_up_area_error"></div>

                                        </div>

                                        <div class="col-sm-3"><div class="select-style">

                                                <?php echo $this->Form->input('Feature.build_up_sq', array('id' => 'build_up_sq', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => '', 'label' => false, 'value' => $feature_unit_val)); ?>

                                            </div></div>


                                    </div>


                                <?php } ?>

                                <?php if (in_array("3", $propertyoption)) {
								if (array_key_exists(3, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][3];
									$feature_unit_val = $this->request->data['AllFeaturesUnit'][3];
								}
								else
								{
									$feature_val = '';
									$feature_unit_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">

                                        <div class="col-sm-12"><label class="label_title">Carpet Area</label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the actual Liveable area "></i>
                                        </div>
                                        <div class="col-sm-6"><?php echo $this->Form->input('Feature.carpet_area', array('id' => 'carpet_area', 'class' => 'form-control', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>
                                            <div class="error has-error" style="display:block" id="carpet_area_error"></div>

                                        </div>
                                        <div class="col-sm-3"><div class="select-style">

                                                <?php echo $this->Form->input('Feature.carpet_area_sq', array('id' => 'build_up_sq', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => '', 'label' => false, 'value' => $feature_unit_val)); ?>

                                            </div></div>


                                    </div>
                                <?php } ?>

                                <?php if (in_array("20", $propertyoption)) {
								if (array_key_exists(20, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][20];
									$feature_unit_val = $this->request->data['AllFeaturesUnit'][20];
								}
								else
								{
									$feature_val = '';
									$feature_unit_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">

                                        <div class="col-sm-12"><label class="label_title">Plot area<span class="mand_field">*</span> </label>
                                            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the total Measured area of the land."></i>
                                        </div>
                                        <div class="col-sm-6"><?php echo $this->Form->input('Feature.plot_area', array('id' => 'carpet_area', 'class' => 'form-control required', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>
                                            <div class="error has-error" style="display:block" id="plot_area_error"></div>
                                        </div>
                                        <div class="col-sm-3"><div class="select-style">

                                                <?php echo $this->Form->input('Feature.plot_area_sq', array('id' => 'plot_area_sq', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => '', 'label' => false, 'value' => $feature_unit_val)); ?>

                                            </div></div>



                                    </div>

                                <?php } ?>
                                <?php if (in_array("21", $propertyoption)) {
								if (array_key_exists(21, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][21];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">

                                        <div class="col-sm-12"><label class="label_title">Length of plot </label></div>
                                        <div class="col-sm-6"><?php echo $this->Form->input('Feature.length_plot', array('id' => 'length_plot', 'class' => 'form-control', 'placeholder' => 'Length of plot in Sq.ft', 'label' => false,'value' => $feature_val)); ?></div>
                                        <div class="col-sm-3"> Sq.ft.</div>
                                        <div class="error has-error">Box must be filled out</div>


                                    </div>
                                <?php } ?>
                                <?php if (in_array("22", $propertyoption)) {
								if (array_key_exists(22, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][22];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">

                                        <div class="col-sm-12"><label class="label_title">Width of plot</label></div>
                                        <div class="col-sm-6"><?php echo $this->Form->input('Feature.width_plot', array('id' => 'width_plot', 'class' => 'form-control', 'placeholder' => 'Width of plot in Sq.ft', 'label' => false,'value' => $feature_val)); ?></div>
                                        <div class="col-sm-3"> Sq.ft.</div>
                                        <div class="error has-error">Box must be filled out</div>


                                    </div>
                                <?php } ?>
                                <?php if (in_array("4", $propertyoption)) {
								if (array_key_exists(4, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][4];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title">Bedrooms<span class="mand_field">*</span> </label>

                                            <div class="select-style">

                                                <?php echo $this->Form->input('Feature.bedrooms', array('id' => 'bedrooms', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4'), 'empty' => 'Select No of Bedrooms', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Bedrooms', 'label' => false,'value' => $feature_val)); ?>

                                            </div>
                                            <div class="error has-error" style="display:block" id="bedrooms_error"></div>
                                        </div>

                                    </div>
                                <?php } ?>
                                <?php if (in_array("5", $propertyoption)) {
								if (array_key_exists(5, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][5];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title">Bathrooms<span class="mand_field">*</span>  </label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.bathrooms', array('id' => 'bathrooms', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4'), 'empty' => 'Select No of Bathrooms', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Bathrooms', 'label' => false,'value' => $feature_val)); ?>

                                            </div>
                                            <div class="error has-error" style="display:block" id="bathrooms_error"></div>

                                        </div>

                                    </div>
                                <?php } ?>

                                <?php if (in_array("33", $propertyoption)) {
								if (array_key_exists(33, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][33];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title">Washrooms<span class="mand_field">*</span></label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.washrooms', array('id' => 'washrooms', 'options' => array('None' => 'None', 'Shared' => 'Shared', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '9+' => '9+'), 'empty' => 'Select No of Bathrooms', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Washroom', 'label' => false,'value' => $feature_val)); ?>

                                            </div>
                                            <div class="error has-error" style="display:block" id="washrooms_error"></div>

                                        </div>

                                    </div>
                                <?php } ?>

                                <?php if (in_array("34", $propertyoption)) {
								if (array_key_exists(34, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][34];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title">Configure<span class="mand_field">*</span> </label>
                                            <div class="select-style required">
                                                <?php echo $this->Form->input('Feature.Configure', array('id' => 'Configure', 'options' => array('None' => 'None', 'Shared' => 'Shared', '1RK' => '1RK', 'Studio' => 'Studio', '1BHK' => '1BHK', '2BHK' => '2BHK', '3BHK' => '3BHK', '4BHK' => '4BHK', '5BHK' => '5BHK', '6BHK' => '6BHK', 'PentHouse' => 'PentHouse'), 'empty' => 'Configure', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Configure', 'label' => false,'value' => $feature_val)); ?>

                                            </div>
                                            <div class="error has-error" style="display:block" id="Configure_error"></div>

                                        </div>

                                    </div>
                                <?php } ?>
                                <?php if (in_array("6", $propertyoption)) {
								if (array_key_exists(6, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][6];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"> <label class="label_title">Balconies<span class="mand_field">*</span> </label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.balconies', array('id' => 'balconies', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4'), 'empty' => 'Select No of Balconies', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Balconies', 'label' => false,'value' => $feature_val)); ?>

                                            </div>
                                            <div class="error has-error" style="display:block" id="balconies_error"></div>
                                        </div>

                                    </div>
                                <?php } ?>
                                <?php if (in_array("7", $propertyoption)) {
								if (array_key_exists(7, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][7];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8">  <label class="label_title"> Age of Property </label>
                                            <?php echo $this->Form->input('Feature.age_of_property', array('id' => 'age_of_property', 'class' => 'form-control', 'placeholder' => 'Age', 'label' => false,'value' => $feature_val)); ?>										   
                                            <div class="error has-error" style="display:block" id="age_error"></div>
                                        </div>

                                    </div>

                                <?php } ?>

                                <?php if (in_array("9", $propertyoption)) {
								if (array_key_exists(9, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][9];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title"> Total floors</label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.total_floors', array('id' => 'total_floors', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>

                                            </div>

                                        </div>

                                    </div>

                                <?php } ?>

                                <?php if (in_array("10", $propertyoption)) {
								if (array_key_exists(10, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][10];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title">Property on floor </label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.on_floor', array('id' => 'on_floor', 'options' => array('basement' => 'Basement', 'Lower Ground' => 'Lower Ground', 'Ground' => 'Ground', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>

                                            </div>
                                            <div class="error has-error">Box must be filled out</div>
                                        </div>

                                    </div>
                                <?php } ?>

                                <?php if (in_array("30", $propertyoption)) {
								if (array_key_exists(30, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][30];
								}
								else
								{
									$feature_val = '';
								}
                                    ?>
                                    <div class="form-group col-sm-6">
                                        <div class="col-sm-8"><label class="label_title">Floors in property </label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.in_floor', array('id' => 'in_floor', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>


                                            </div>
                                            <div class="error has-error">Box must be filled out</div>
                                        </div>

                                    </div>
                                <?php } ?>
                                <?php if (in_array("23", $propertyoption)) {
                                 if (array_key_exists(23, $this->request->data['AllFeatures'])) {
									$feature_val = $this->request->data['AllFeatures'][23];
								}
								else
								{
									$feature_val = '';
								}
								    ?>
                                    <div class="form-group col-sm-6" id="disable_on_rent">
                                        <div class="col-sm-8"><label class="label_title">Floors allowed for construction </label>
                                            <div class="select-style">

                                                <?php echo $this->Form->input('Feature.allowed_floor', array('id' => 'allowed_floor', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>


                                            </div>
                                            <div class="error has-error">Box must be filled out</div>
                                        </div>

                                    </div>
                                <?php } ?>

                                <?php if (in_array("11", $propertyoption)) {
                                 if (array_key_exists(11, $this->request->data['AllFeatures'])) {
									$feature_val1 = $this->request->data['AllFeatures'][11];
								}
								else
								{
									$feature_val1 = '';
								}
								
								if (array_key_exists(45, $this->request->data['AllFeatures'])) {
									$feature_val2 = $this->request->data['AllFeatures'][45];
								}
								else
								{
									$feature_val2 = '';
								}
								
								if (array_key_exists(46, $this->request->data['AllFeatures'])) {
									$feature_val3 = $this->request->data['AllFeatures'][46];
								}
								else
								{
									$feature_val3 = '';
								}
								    ?>
                                    <div class="form-group col-sm-12">
                                        <div class="col-sm-12"><label class="label_title">Reserved Parking</label></div>

                                        <div class="col-sm-12">

                                            <?php 
											if($feature_val1 == 1)
											{
												echo $this->Form->input('Feature.res_parking_1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_none', 'hiddenField' => false,'checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.res_parking_1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_none', 'hiddenField' => false,'checked'=>false));
											}	
											 ?>

                                            <label for="id_chk_none" class="checkbox-custom-label" style="padding-right:160px;">None</label>

                                            <?php 
											if($feature_val2 == 1)
											{
												echo $this->Form->input('Feature.res_parking_2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_cvrd', 'hiddenField' => false,'checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.res_parking_2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_cvrd', 'hiddenField' => false,'checked'=>false));
											}
											 ?>
                                            <label for="id_chk_cvrd" class="checkbox-custom-label" style="padding-right:160px;" >Covered</label>


                                            <?php 
											if($feature_val3 == 1)
											{
												echo $this->Form->input('Feature.res_parking_3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_open', 'hiddenField' => false,'checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.res_parking_3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_open', 'hiddenField' => false,'checked'=>false));
											}
											 ?>
                                            <label for="id_chk_open" class="checkbox-custom-label" style="padding-right:160px;" >Open</label>

                                        </div>

                                        <div class="error has-error">Box must be filled out</div>


                                    </div>

                                    <div class="form-group col-sm-6" id="id_select_cvrd" <?php if($feature_val2 != 1){?>style="display:none;"<?php }?>>

                                        <div class="col-sm-8"><label class="label_title">No. of covered parking </label>
										<?php
										if (array_key_exists(37, $this->request->data['AllFeatures'])) {
											$feature_val = $this->request->data['AllFeatures'][37];
										}
										else
										{
											$feature_val = '';
										}
										?>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.no_covered', array('id' => 'no_covered', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>
                                            </div>
                                            <div class="error has-error" id="covered_parking_error" style="display:block""></div>
                                        </div>

                                    </div>

                                    <div class="form-group col-sm-6" id="id_select_open" <?php if($feature_val3 != 1){?>style="display:none;"<?php }?> >

                                        <div class="col-sm-8"><label class="label_title">No. of open parking</label>
										<?php
										if (array_key_exists(38, $this->request->data['AllFeatures'])) {
											$feature_val = $this->request->data['AllFeatures'][38];
										}
										else
										{
											$feature_val = '';
										}
										?>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.no_open', array('id' => 'no_open', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false,'value' => $feature_val)); ?>
                                            </div>
                                            <div class="error has-error" id="open_parking_error" style="display:block""></div>
                                        </div>

                                    </div>

                                <?php } ?>
                                <?php if (in_array("12", $propertyoption)) {
								if (array_key_exists(12, $this->request->data['AllFeatures'])) {
									$feature_val1 = $this->request->data['AllFeatures'][12];
								}
								else
								{
									$feature_val1 = '';
								}								
								if (array_key_exists(39, $this->request->data['AllFeatures'])) {
									$feature_val2 = $this->request->data['AllFeatures'][39];
								}
								else
								{
									$feature_val2 = '';
								}								
								if (array_key_exists(40, $this->request->data['AllFeatures'])) {
									$feature_val3 = $this->request->data['AllFeatures'][40];
								}
								else
								{
									$feature_val3 = '';
								}
								
								if (array_key_exists(41, $this->request->data['AllFeatures'])) {
									$feature_val4 = $this->request->data['AllFeatures'][41];
								}
								else
								{
									$feature_val4 = '';
								}
								
                                    ?>
                                    <div class="form-group col-sm-12">
                                        <div class="col-sm-12">
                                            <label class="label_title">Other rooms </label ></div>
                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val1 == 1)
											{
												echo $this->Form->input('Feature.other_room1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room1','checked' => true));
											}
											else
											{
												echo $this->Form->input('Feature.other_room1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room1','checked' => false));
											}
											 ?>

                                            <label for="other_room1" class="checkbox-custom-label">Pooja</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val2 == 1)
											{
												echo $this->Form->input('Feature.other_room2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room2','checked' => true));
											}
											else
											{
												echo $this->Form->input('Feature.other_room2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room2','checked' => false));
											}
											 ?>
                                            <label for="other_room2" class="checkbox-custom-label">Study</label>

                                        </div>
                                        <div class="col-sm-3">
                                            <?php
											if($feature_val3 == 1)
											{ 
												echo $this->Form->input('Feature.other_room3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room3','checked' => true));
											}
											else
											{
												echo $this->Form->input('Feature.other_room3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room3','checked' => false));
											}
											 ?>
                                            <label for="other_room3" class="checkbox-custom-label">Servant</label>

                                        </div>
                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val4 == 1)
											{
												echo $this->Form->input('Feature.other_room4', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room4','checked' => true));
											}
											else
											{
												echo $this->Form->input('Feature.other_room4', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room4','checked' => false));
											} ?>
                                            <label for="other_room4" class="checkbox-custom-label">Other</label>

                                        </div>





                                    </div> 
                                <?php } ?>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">Description<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Describe your property, Highlight USP of your property, Nearby Landmarks, Market area , Office area etc"></i>
                                        <?php echo $this->Form->input('description', array('type' => 'textarea', 'id' => 'countryState', 'placeholder' => 'Add property description', 'class' => 'form-control required', 'label' => false, 'div' => false, 'rows' => '1')); ?>

                                        <div class="error has-error" id="property_description_error" style="display:block"></div>
                                    </div>
                                </div>

                            </div>





                            <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => $this->request->data['property_id'])); ?>
                            <div class="account-block text-center submit_area">
                                <button type="submit" class="btn btn-primary btn-next" id="next2">Save</button>
                            </div>

                        </div>

                    </div>
                </div>

            </form>
            <form class="fpform" id="form_price">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Pricing</h3>
                        <div class="add-expand" id="property_pricing_button"></div>
                    </div>
                    <div class="add-tab-content" id="property_pricing_ajax">

                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="offer_price_show">
                                        <label for="property-price" class="label_title">Offer Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Asking Price ( All Inclusive Price ). It has to be lower or equal to Market Price."></i>
                                        <?php echo $this->Form->input('offer_price', array('id' => 'offer_price', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Asking Price', 'title' => 'Please Enter Offer Price', 'label' => false, 'onchange' => 'document.getElementById(\'offer_price_sqt\').value= (this.value/document.getElementById(\'areinsq\').value)')); ?>
                                        <div class="error has-error" id="offer_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_monthly_show" style="display:none">
                                        <label for="property-price" class="label_title">Expected Monthly Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Expected Monthy Rent ( All Inclusive Price ). It has to be lower or equal to Market Rent."></i>
                                        <?php echo $this->Form->input('expected_monthly', array('id' => 'expected_monthly', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Expected Monthly Rent', 'title' => 'Please Enter Monthly Rent', 'label' => false)); ?>
                                        <div class="error has-error" id="expected_monthly_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price" class="label_title">Offer Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('offer_price_sqt', array('id' => 'offer_price_sqt', 'class' => 'form-control box_price', 'label' => false, 'value' => ($this->request->data['offer_price_sqt']))); ?>
                                        <div class="error has-error" id="offer_price_sqt_error" style="display:block"></div>
                                    </div>

                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-sm-4">
                                    <div class="form-group" id="bookamount">
                                        <label for="property-price-before" class="label_title">Booking Amount</label>
                                        <?php echo $this->Form->input('booking_amount', array('id' => 'booking_amount', 'class' => 'form-control box_price', 'title' => 'Please Enter Booking Amount', 'label' => false, 'placeholder' => 'Enter booking amount')); ?>

                                        <div class="error has-error" id="booking_amount_error" style="display:block"></div>
                                    </div>
                                    <div class="form-group" id="depositamount" style="display:none">
                                        <label for="property-price-before" class="label_title"> Security Deposit Amount</label>

                                        <?php echo $this->Form->input('deposit', array('id' => 'deposit-amount-before', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Security Deposit Amount')); ?>

                                        <div class="error has-error" id="deposit_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price-maintenance" class="label_title">Maintenance </label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Monthly recurring expenses "></i>
                                        <?php echo $this->Form->input('maintance_amount', array('id' => 'maintance_amount', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Maintenance Amount', 'title' => 'Please Enter Maintenance')); ?>
                                        <div class="error has-error" id="maintance_amount_error" style="display:block"></div>	
                                        <div class="error has-error">Box must be filled out</div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="add-tab-row push-padding-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Availability</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('construction_status', array('id' => 'constructuion_status', 'options' => array('1' => 'Ready to move in', '2' => 'Under Construction'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Construction Status', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" id="construction_status_error" style="display:block" ></div>



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Possession Date</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('possession_date', array('id' => 'possession_date', 'options' => array('3' => 'Within 3 Months', '6' => 'Within 6 Months', '12' => '1 Year', '24' => '2 Year', '36' => '3 Year', '48' => '4 Year', '60' => '5 Year'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Possession Date', 'label' => false)); ?>



                                        </div>	
                                        <div class="error has-error" id="possession_date_error" style="display:block" ></div>	

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Ownership Type</label>

                                        <div class="select-style">

                                            <?php echo $this->Form->input('ownership_type_id', array('id' => 'ownership_type', 'options' => array('1' => 'Freehold', '2' => 'Lease Hold', '3' => 'Power of attorney', '4' => 'Co-operative Society'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Ownership Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="ownership_type_error" style="display:block" ></div>	



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Sale Type</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('sale_type', array('id' => 'sale_type', 'options' => array('1' => 'Resale', '2' => 'New Booking'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Sale Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="sale_type_error" style="display:block" ></div>	



                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php echo $this->Form->input('areinsq', array('class' => 'form-control', 'type' => 'hidden', 'label' => false, 'id' => 'areinsq', 'value' => $areainsq)); ?>
                        <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => $this->request->data['property_id'])); ?>
                        <div class="account-block text-center submit_area">
                            <button type="submit" id="next3" class="btn btn-primary btn-next ">Save</button>
                        </div>  


                    </div>
                </div>
            </form>
            <form class="fpform" id="aminities_form">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Features and Amenities</h3>
                        <div class="add-expand" id="property_amenities_button"></div>
                    </div>
                    <div class="add-tab-content" id="aminities_ajax">
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">

                                <?php
                                if (in_array("31", $propertyoption)) {
									if (array_key_exists(31, $this->request->data['AllFeatures'])) {
										$feature_val = $this->request->data['AllFeatures'][31];
										}
										else
										{
											$feature_val = '';
										}
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="label_title">Type of Flooring</label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.type_of_flooring', array('id' => 'type_of_flooring', 'options' => array('vitrified' => 'Vitrified', 'marble' => 'Marble', 'Ceramic' => 'Ceramic', 'Mosaic' => 'Mosaic', 'Wood' => 'Wood', 'Granite' => 'Granite', 'Spartex' => 'Spartex', 'Cement' => 'Cement', 'Stone' => 'Stone', 'Vinyl' => 'Vinyl', 'IPSFinish' => 'IPSFinish', 'Other' => 'Other'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false, 'value' => $feature_val)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("32", $propertyoption)) {
									if (array_key_exists(32, $this->request->data['AllFeatures'])) {
										$feature_val = $this->request->data['AllFeatures'][32];
										}
										else
										{
											$feature_val = '';
										}
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="label_title">Power Backup</label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.power_backup', array('id' => 'power_backup', 'options' => array('none' => 'None', 'partial' => 'Partial', 'full' => 'Full'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false, 'value' => $feature_val)); ?>

                                            </div>



                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("8", $propertyoption)) {
									if (array_key_exists(8, $this->request->data['AllFeatures'])) {
										$feature_val = $this->request->data['AllFeatures'][8];
										}
										else
										{
											$feature_val = '';
										}
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="label_title">Furnishing</label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.furnishing', array('id' => 'furnishing', 'options' => array('furnished' => 'Furnished', 'semi-furnished' => 'Semi-furnished', 'unfurnished' => 'Unfurnished'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false, 'value' => $feature_val)); ?>

                                            </div>



                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("28", $propertyoption)) {
									if (array_key_exists(28, $this->request->data['AllFeatures'])) {
										$feature_val = $this->request->data['AllFeatures'][28];
										}
										else
										{
											$feature_val = '';
										}
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="label_title">Facing</label>
                                            <div class="select-style">
                                                <?php echo $this->Form->input('Feature.facing', array('id' => 'facing', 'options' => array('North' => 'North', 'East' => 'East', 'South' => 'South', 'West' => 'West', 'North-East' => 'North-East', 'South-East' => 'South-East', 'North-West' => 'North-West', 'South-West' => 'South-West'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false, 'value' => $feature_val)); ?>

                                            </div>


                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("26", $propertyoption)) {
									if (array_key_exists(26, $this->request->data['AllFeatures'])) {
										$feature_val = $this->request->data['AllFeatures'][26];
										}
										else
										{
											$feature_val = '';
										}
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="label_title">Society Type</label>
                                            <div class="select-style">

                                                <?php echo $this->Form->input('Feature.society_type', array('id' => 'society_type', 'options' => array('Gated society,' => 'Gated Society,', 'corner property' => 'Corner Property'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false, 'value' => $feature_val)); ?>
                                            </div>



                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("13", $propertyoption)) {
									if (array_key_exists(13, $this->request->data['AllFeatures'])) {
										$feature_val = $this->request->data['AllFeatures'][13];
										$feature_unit_val = $this->request->data['AllFeaturesUnit'][13];
										}
										else
										{
											$feature_val = '';
											$feature_unit_val = '';
										}
                                    ?>
                                    <div class="form-group col-sm-4">
                                        <div class="col-sm-12"><label class="label_title">Width of facing road</label></div>
                                        <div class="col-sm-8"> <?php echo $this->Form->input('Feature.width_of_fac_road', array('id' => 'book-amount-before', 'class' => 'form-control', 'placeholder' => '', 'label' => false, 'value' => $feature_val)); ?></div>
                                        <div class="col-sm-4">
                                            <div class="select-style">

                                                <?php echo $this->Form->input('Feature.width_of_sq', array('id' => 'width_of_sq', 'options' => array('feet' => 'Feet', 'meter' => 'Meter'), 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false, 'value' => $feature_unit_val)); ?>
                                            </div>
                                        </div>
                                        <div class="error has-error" style="display:block" id="width_of_fac_road_error"></div>


                                    </div>
                                <?php } ?>

                                <div class="form-group col-sm-12">
                                    <?php
                                    if (in_array("18", $propertyoption)) {
									if (array_key_exists(18, $this->request->data['AllFeatures'])) {
									$feature_val1 = $this->request->data['AllFeatures'][18];
									}
									else
									{
										$feature_val1 = '';
									}
									
									if (array_key_exists(36, $this->request->data['AllFeatures'])) {
										$feature_val2 = $this->request->data['AllFeatures'][36];
									}
									else
									{
										$feature_val2 = '';
									}
									
									
                                        ?>
                                        <div class="col-sm-12"><label class="label_title">Water Source</label></div>
                                        <div class="col-sm-3">
                                            <?php
											if($feature_val1 == 1)
											{											
												echo $this->Form->input('Feature.watersource1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'watersource1', 'hiddenfield' => false, 'checked'=>true)); 
											}
											else
											{
												echo $this->Form->input('Feature.watersource1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'watersource1', 'hiddenfield' => false, 'checked'=>false)); 
											}
											?>

                                            <label for="watersource1" class="checkbox-custom-label">Municipal</label>
                                        </div>

                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val2 == 1)
											{
												echo $this->Form->input('Feature.watersource2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'watersource2', 'hiddenfield' => false, 'checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.watersource2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'watersource2', 'hiddenfield' => false, 'checked'=>false));
											}
											 ?>

                                            <label for="watersource2" class="checkbox-custom-label">Borewell Tank</label>

                                        </div>  

                                    <?php } ?>


                                </div>

                                <div class="form-group col-sm-12">
                                    <?php
                                    if (in_array("14", $propertyoption)) {
										if (array_key_exists(14, $this->request->data['AllFeatures'])) {
										$feature_val1 = $this->request->data['AllFeatures'][14];
										}
										else
										{
											$feature_val1 = '';
										}
										
										if (array_key_exists(42, $this->request->data['AllFeatures'])) {
											$feature_val2 = $this->request->data['AllFeatures'][42];
										}
										else
										{
											$feature_val2 = '';
										}
										if (array_key_exists(43, $this->request->data['AllFeatures'])) {
										$feature_val3 = $this->request->data['AllFeatures'][43];
										}
										else
										{
											$feature_val3 = '';
										}
										
										if (array_key_exists(44, $this->request->data['AllFeatures'])) {
											$feature_val4 = $this->request->data['AllFeatures'][44];
										}
										else
										{
											$feature_val4 = '';
										}
										
                                        ?>
                                        <div class="col-sm-12"><label class="label_title">Overlooking</label></div>
                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val1 == 1)
											{
												echo $this->Form->input('Feature.overlook1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook1','checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.overlook1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook1','checked'=>false));
											}
											 ?>

                                            <label for="overlook1" class="checkbox-custom-label">Park/Garden</label>
                                        </div>

                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val2 == 1)
											{
												echo $this->Form->input('Feature.overlook2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook2','checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.overlook2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook2','checked'=>false));
											}
											?>

                                            <label for="overlook2" class="checkbox-custom-label">Main Road</label>

                                        </div>
                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val3 == 1)
											{
												echo $this->Form->input('Feature.overlook3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook3','checked'=>true));
											}
											else
											{	
												echo $this->Form->input('Feature.overlook3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook3','checked'=>false));
											}
											 ?>

                                            <label for="overlook3" class="checkbox-custom-label">Club</label>

                                        </div>
                                        <div class="col-sm-3">
                                            <?php 
											if($feature_val4 == 1)
											{
												echo $this->Form->input('Feature.overlook4', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook4','checked'=>true));
											}
											else
											{
												echo $this->Form->input('Feature.overlook4', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook4','checked'=>false));
											}
											?>

                                            <label for="overlook4" class="checkbox-custom-label">Pool</label>

                                        </div>
                                    <?php } ?> 

                                </div>

                                <div id ="anmities_hide_rent">
                                    <div class="form-group col-sm-4">
                                        <?php
                                        if (in_array("15", $propertyoption)) {
											if (array_key_exists(15, $this->request->data['AllFeatures'])) {
											$feature_val = $this->request->data['AllFeatures'][15];
											}
											else
											{
												$feature_val = '';
											}
                                            ?>
                                            <div class="col-sm-12"><label class="label_title">Bachelors Allowed</label></div>
                                            <div class="col-sm-3">


                                                <?php echo $this->Form->input('Feature.bachelor_all', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                                <label for="FeatureBachelorAll1" class="radio-custom-label">Yes</label>

                                            </div>
                                            <div class="col-sm-3">
                                                <?php echo $this->Form->input('Feature.bachelor_all', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                                <label for="FeatureBachelorAll2" class="radio-custom-label">No</label>

                                            </div>
                                        <?php } ?>		
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <?php
                                        if (in_array("16", $propertyoption)) {
											if (array_key_exists(16, $this->request->data['AllFeatures'])) {
											$feature_val = $this->request->data['AllFeatures'][16];
											}
											else
											{
												$feature_val = '';
											}
                                            ?>

                                            <div class="col-sm-12"><label class="label_title">Pet allowed  </label></div>
                                            <div class="col-sm-3">
                                                <?php echo $this->Form->input('Feature.pet_all', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                                <label for="FeaturePetAll1" class="radio-custom-label">Yes</label>

                                            </div>
                                            <div class="col-sm-3">
                                                <?php echo $this->Form->input('Feature.pet_all', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                                <label for="FeaturePetAll2" class="radio-custom-label">No</label>

                                            </div>
                                        <?php } ?>			
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <?php
                                        if (in_array("17", $propertyoption)) {
											if (array_key_exists(17, $this->request->data['AllFeatures'])) {
											$feature_val = $this->request->data['AllFeatures'][17];
											}
											else
											{
												$feature_val = '';
											}
                                            ?>
                                            <div class="col-sm-12"><label class="label_title">Non vegetarian</label></div>
                                            <div class="col-sm-3">
                                                <?php echo $this->Form->input('Feature.non_veg', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>


                                                <label for="FeatureNonVeg1" class="radio-custom-label">Yes</label>

                                            </div>
                                            <div class="col-sm-3">
                                                <?php echo $this->Form->input('Feature.non_veg', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                                <label for="FeatureNonVeg2" class="radio-custom-label">No</label>
                                            </div>

                                        <?php } ?>	

                                    </div>
                                </div>

                                <div class="form-group col-sm-4">
                                    <?php
                                    if (in_array("25", $propertyoption)) {
										if (array_key_exists(25, $this->request->data['AllFeatures'])) {
											$feature_val = $this->request->data['AllFeatures'][25];
											}
											else
											{
												$feature_val = '';
											}
                                        ?>
                                        <div class="col-sm-12"><label class="label_title">Is Boundary Wall Made</label></div>
                                        <div class="col-sm-3">
                                            <?php echo $this->Form->input('Feature.boundary_wall', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                            <label for="FeatureBoundaryWall1" class="radio-custom-label">Yes</label>

                                        </div>
                                        <div class="col-sm-3">
                                            <?php echo $this->Form->input('Feature.boundary_wall', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'value' => $feature_val)); ?>

                                            <label for="FeatureBoundaryWall2" class="radio-custom-label">No</label>
                                        </div>
                                    <?php } ?>	
                                </div>

                            </div>
                        </div>

                        <div class="add-tab-row push-padding-bottom">
                            <div class="row pro_amen">
                                <?php
                                if (in_array("1", $aminitesoption)) {
									if (array_key_exists(1, $this->request->data['AllAmenity'])) {
										$aminity_val = $this->request->data['AllAmenity'][1];
										}
										else
										{
											$aminity_val = '';
										}
                                    ?>
                                    <div class="col-sm-2 text-center">

                                        <div class="checkbox">
                                            <label>

                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.lift', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_lift', 'multiple' => 'checkbox', 'checked' => true));
												}
												else
												{
													echo $this->Form->input('amenities.lift', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_lift', 'multiple' => 'checkbox', 'checked' => false));
												}
												 ?>

                                                <label for="amenities_lift" class="checkbox-amenities icon_lift"><div>Lifts</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("2", $aminitesoption)) {
									if (array_key_exists(2, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][2];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.park', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_park', 'multiple' => 'checkbox', 'checked' => true));
												}
												else
												{	
													echo $this->Form->input('amenities.park', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_park', 'multiple' => 'checkbox', 'checked' => false));
												}
												?>

                                                <label for="amenities_park" class="checkbox-amenities icon_park"><div>Park</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("3", $aminitesoption)) {
									if (array_key_exists(3, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][3];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.staff', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_staff', 'checked' => true));
												}
												else
												{	
													echo $this->Form->input('amenities.staff', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_staff', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_staff" class="checkbox-amenities icon_staff"><div>Maintenance Staff</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("4", $aminitesoption)) {
									if (array_key_exists(4, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][4];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.parking', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_parking', 'checked' => true));
												}
												else
												{	
													echo $this->Form->input('amenities.parking', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_parking', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_parking" class="checkbox-amenities icon_parking"><div>Visitor Parking</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("5", $aminitesoption)) {
									if (array_key_exists(5, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][5];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.waterstorage', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_water', 'checked' => true));
												}
												else
												{	
													echo $this->Form->input('amenities.waterstorage', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_water', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_water" class="checkbox-amenities icon_water_storage"><div>Water Storage</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("6", $aminitesoption)) {
									if (array_key_exists(6, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][6];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.vaastu', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_vaastu', 'checked' => true));
												}
												else
												{	
													echo $this->Form->input('amenities.vaastu', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_vaastu', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_vaastu" class="checkbox-amenities icon_vaastu"><div>Feng shui/Vaastu compliant</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>


                            <div class="row pro_amen">
                                <?php
                                if (in_array("7", $aminitesoption)) {
									if (array_key_exists(7, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][7];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.intercomm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_intercomm', 'checked' => true));
												}
												else
												{	
													echo $this->Form->input('amenities.intercomm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_intercomm', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_intercomm" class="checkbox-amenities icon_intercomm"><div>Intercomm Facility</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("8", $aminitesoption)) {
									if (array_key_exists(8, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][8];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	 
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.fire_alarm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_security', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.fire_alarm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_security', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_security" class="checkbox-amenities icon_security"><div>Security/Fire alarm </div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("16", $aminitesoption)) {
									if (array_key_exists(16, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][16];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.waste_disposal', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_waste', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.waste_disposal', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_waste', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_waste" class="checkbox-amenities icon_waste"><div>Waste Disposal </div> </label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("17", $aminitesoption)) {
									if (array_key_exists(17, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][17];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.tarrace', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_tarrace', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.tarrace', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_tarrace', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_tarrace" class="checkbox-amenities icon_tarrace"><div>Private Garden/Terrace</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("18", $aminitesoption)) {
									if (array_key_exists(18, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][18];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.security_personal', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_security1', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.security_personal', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_security1', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_security1" class="checkbox-amenities icon_securityp"><div>Security Personnel</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("21", $aminitesoption)) {
									if (array_key_exists(21, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][21];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.goods_lift', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_lift1', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.goods_lift', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_lift1', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_lift1" class="checkbox-amenities icon_lift"><div>Service/Goods Lift</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>


                            <div class="row pro_amen">

                                <?php
                                if (in_array("22", $aminitesoption)) {
									if (array_key_exists(22, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][22];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label>
                                               <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.conroom', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_conroom', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.conroom', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_conroom', 'checked' => false));
												}
												?>
											    
                                                <label for="amenities_conroom" class="checkbox-amenities icon_conroom"><div>Conference Room</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("19", $aminitesoption)) {
									if (array_key_exists(19, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][19];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.swimming', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_pool', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.swimming', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_pool', 'checked' => false));
												}
												?>
												

                                                <label for="amenities_pool" class="checkbox-amenities icon_pool"><div>Swimming Pool</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("20", $aminitesoption)) {
									if (array_key_exists(20, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][20];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                               <?php 
												if($aminity_val == 'Yes')
												{
													 echo $this->Form->input('amenities.atm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_atm', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.atm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_atm', 'checked' => false));
												}
												?>
											   

                                                <label for="amenities_atm" class="checkbox-amenities icon_atm"><div>ATM</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("23", $aminitesoption)) {
									if (array_key_exists(23, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][23];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													 echo $this->Form->input('amenities.ac', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_airc', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.ac', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_airc', 'checked' => false));
												}
												?>

                                                <label for="amenities_airc" class="checkbox-amenities icon_airc"><div>Centrally Airconditioned </div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("24", $aminitesoption)) {
									if (array_key_exists(24, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][24];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													 echo $this->Form->input('amenities.internet', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_internet', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.internet', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_internet', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_internet" class="checkbox-amenities icon_internet"><div>High Speed Internet </div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("25", $aminitesoption)) {
									if (array_key_exists(25, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][25];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                               <?php 
												if($aminity_val == 'Yes')
												{
													 echo $this->Form->input('amenities.food', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_foodc', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.food', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_foodc', 'checked' => false));
												}
												?>
											   
                                                <label for="amenities_foodc" class="checkbox-amenities icon_foodc"><div>Cafeteria/Food court</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>

                            <div class="row pro_amen">

                                <?php
                                if (in_array("gym", $aminitesoption)) {
									if (array_key_exists("gym", $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity']["gym"];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													 echo $this->Form->input('amenities.gym', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_gym', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.gym', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_gym', 'checked' => false));
												}
												?>
												
                                                <label for="amenities_gym" class="checkbox-amenities icon_gym"><div>Gymnasium</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("water_softening_plant", $aminitesoption)) {
									if (array_key_exists("water_softening_plant", $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity']["water_softening_plant"];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
												<?php 
												if($aminity_val == 'Yes')
												{
													 echo $this->Form->input('amenities.water_plant', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_waterp', 'checked' => true));
												}
												else
												{
													 echo $this->Form->input('amenities.water_plant', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_waterp', 'checked' => false));
												}
												?>
                                                
                                                <label for="amenities_waterp" class="checkbox-amenities icon_water_plant"><div>Water Softening Plant</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("community_hall", $aminitesoption)) {
									if (array_key_exists("community_hall", $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity']["community_hall"];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
												<?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.commhall', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_commhall', 'checked' => true));
												}
												else
												{
													echo $this->Form->input('amenities.commhall', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_commhall', 'checked' => false));
												}
												?>
                                                
                                                <label for="amenities_commhall" class="checkbox-amenities icon_com_hall"><div>Community Hall</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("shopping_center", $aminitesoption)) {
									if (array_key_exists("shopping_center", $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity']["shopping_center"];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
												<?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.shopping', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_shopping', 'checked' => true));
												}
												else
												{
													echo $this->Form->input('amenities.shopping', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_shopping', 'checked' => false));
												}
												?>
                                               

                                                <label for="amenities_shopping" class="checkbox-amenities icon_shopping"><div>Shopping Center</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                if (in_array("15", $aminitesoption)) {
									if (array_key_exists(15, $this->request->data['AllAmenity'])) {
									$aminity_val = $this->request->data['AllAmenity'][15];
									}
									else
									{
										$aminity_val = '';
									}
                                    ?>	
                                    <div class="col-sm-2 text-center">
                                        <div class="checkbox">
                                            <label >
                                                <?php 
												if($aminity_val == 'Yes')
												{
													echo $this->Form->input('amenities.rainwater', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_rain', 'checked' => true));
												}
												else
												{
													echo $this->Form->input('amenities.rainwater', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_rain', 'checked' => false));
												}
												?>

                                                <label for="amenities_rain" class="checkbox-amenities icon_rainwater"><div>Rain water harvesting</div></label>

                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>



                        </div> 
                        <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => $this->request->data['property_id'])); ?>
                        <div class="account-block text-center submit_area">

                            <button type="submit" class="btn btn-primary btn-next " id="next4">Save</button>

                        </div>							



                    </div>

                </div>
            </form>

            <form class="fpform" method="post" enctype="multipart/form-data" action="<?php echo $this->webroot; ?>properties/upload_image">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Add Photos</h3>
                        <div class="add-expand" id="property_photo_button"></div>
                    </div>
                    <div class="add-tab-content" id="property_photo_content">
                        <div class="add-tab-row">
                            <div class="property-media">
                                <div class="media-gallery">
                                    <div class="row">
									 <?php
									 if(!empty($propertyImages))
									 {
                                    foreach ($propertyImages as $property_value) {
                                        ?>
                                        <div>
                                            <img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                           
                                        </div>
                                    <?php }
										}
									 ?>
                                        <div id="image_preview"></div> 

                                    </div>
                                </div>
                                <div class="account-block text-center">
                                    <div class="row">
                                        <div class="col-sm-12">	We recommend minimum image size to be 10KB and maximum size to be 5MB. Accepted file format are .png .jpg .jpeg</div>
                                    </div>
                                </div>
                                <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => $this->request->data['property_id'])); ?>
                                <div class="upload_btn_area">

                                    <label class="btn btn-default btn-file">
                                        <?php
                                        echo $this->Form->input('upload_file.', ['type' => 'file', 'label' => false, 'multiple' => 'multiple', 'style' => 'display:none', 'onchange' => 'preview_image()']);
                                        ?>
                                        Add Property Picture 
                                    </label>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="account-block text-center submit_area" id="submit_disable">
                    <?php
                    if ($this->request->data['status_id'] == '3') {
                        ?>

                        <button type="submit"  class="btn btn-primary btn-next ">Save</button>

                        <?php
                    } else {
                        ?> 

                        <div class="account-block text-center submit_area">
                            <button type="submit" id="id_submit_project" class="btn btn-primary btn-red"><?php echo "Submit"; ?> Property</button>
                        </div>
                    <?php }
                    ?>
                    <?php
                    echo $this->Form->input('editpr', array(
                        'type' => 'hidden',
                        'label' => false,
                        'value' => 1
                            )
                    );
                    ?>
                </div>
            </form>

            <div class="modal fade" id="addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                ×</button>
                            <h4 class="modal-title" id="myModalLabel">
                                Add Project
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form class="fpform">
                                    <div class="col-md-12" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">



                                        <div class="add-tab-row push-padding-bottom ">
                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Project Name<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="projectname" placeholder="Enter your project name">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="projectname" class="label_title">Project Description<span class="mand_field">*</span></label>
                                                            <textarea class="form-control" rows="1" id="project_des" placeholder="Enter your project description"></textarea>
                                                            <div class="error has-error">Box must be filled out</div>
                                                        </div>
                                                    </div>
                                                </div >
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Highlights<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="project_highlight" placeholder="Enter your Highlights">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Acres<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="project_acre" placeholder="Enter project Acres">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>

                                        <div class="add-tab-row push-padding-bottom text-center">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <button type="button" class="btn btn-primary btn-red">
                                                        Save & Continue</button>
                                                </div>


                                            </div>

                                        </div>


                                    </div>



                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/our-products-->
    </div>
    </div>
    <!--/.container-->

</div>

<?php
echo $this->Html->script('add_property_7');
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
?>	
<script>
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                    val = $target.attr('data-value'),
                    $inp = $target.find('input'),
                    idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });

        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });
    });



</script>

<!--./disable tab button-->
<script>
    $(document).ready(function ()
    {
        $("#anmities_hide_rent").hide();
<?php
if (!empty($this->request->data['Feature']['res_parking_2']) && $this->request->data['Feature']['res_parking_2'] == '1') {
    ?>
            $('#id_select_cvrd').css('display', 'block');
<?php } ?>
<?php
if (!empty($this->request->data['Feature']['res_parking_2']) && $this->request->data['Feature']['res_parking_2'] == '1') {
    ?>
            $('#id_select_open').css('display', 'block');
<?php } ?>
        //$("#property_detail_button").css("display","none");
        //$("#property_pricing_button").css("display","none");
        //$("#property_amenities_button").css("display","none");
        //$("#property_photo_button").css("display","none");

        $("input[id='offer_price']").on('keyup', function (e) {

            //alert ( toWords(this.value) );
            $("div[id='offer_price_error']").css('display', 'inline');
            $("div[id='offer_price_error']").text(convert_number(this.value));

            if (e.which === 13) {

                //Disable textbox to prevent multiple submit
                //$(this).attr("disabled", "disabled");

                //Do Stuff, submit, etc..
            }
        });

        /*$("input[id='market_price']").on('keyup', function (e) {
         
         //alert ( toWords(this.value) );
         $("div[id='market_price_error']").css('display','inline');
         $("div[id='market_price_error']").text(convert_number(this.value));
         
         if(e.which === 13){
         
         //Disable textbox to prevent multiple submit
         //$(this).attr("disabled", "disabled");
         
         //Do Stuff, submit, etc..
         }
         });*/

    });
</script>

<!--Get property aminities and feature by selecting of property option --> 
<script>
    $(document).ready(function () {

        $("#property_type_val").change(function ()
        {
            var id = $(this).val();
            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyfeature")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#property_feature_ajax').html(html); // show response from the php script.
                        $("#cover_parking_form").hide();// cover Parking disable by default
                        $("#open_parking_form").hide();// open Parking disable by default
						$("#property_feature_form .property_id").val($("#idForm .property_id").val());
                    }
                })


                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyaminities")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#aminities_ajax').html(html); // show response from the php script.
						$("#aminities_form .property_id").val($("#idForm .property_id").val());
                    }
                })
            }
        });
    });
</script>



<!-- jQuery Form Validation code and submit basic code-->
<script>

// When the browser is ready...
    $(function () {
        var url = "<?php echo $this->webroot; ?>Properties/addproperty"; // the script where you handle the form input.
        // Setup form validation on the #register-form element
        $("#idForm").validate({

            
            errorPlacement: function (error, element) {

               
                if (element.attr("name") == "data[property_type_id]") {

                    error.appendTo("#property_type_error");
                }
               
            },

            submitHandler: function (form) {
                $.ajax
                        ({
                            type: "POST",
                            url: url,
                            data: $("#idForm").serialize(), // serializes the form's elements.
                            success: function (data)
                            {
                                // $(".property_id").val(data);
                                $("#property_detail_button").css("display", "block");
                                //$("#idForm :input").prop("disabled", true);
                                $("#next1").html('Saved');
								
								// 4. Expand the next block
				
								$('#property_detail_button').css('display','block');
								$('#property_detail_button').addClass('active');
								$('#property_detail_content').css('display','block');
								
												// 5. Close current block
								$('#property_basic_info').removeClass('active');
								$('#property_basic_content').css('display','none');
								$('#id_top').attr('tabindex',-10).focus();

                            }
                        });
                return false;
            }
        })

    });

</script>


<!-- jQuery Form Validation code and submit Property Details Form-->
<script>

// When the browser is ready...
    $(function () {
        var url = "<?php echo $this->webroot; ?>Properties/addpropertyfeaturedetail"; // the script where you handle the form input.

        // Setup form validation on the #register-form element
        $("#property_feature_form").validate({

            rules: {
                "data[Feature][sbua]": {
                    required: true,
                    number: true,
                    min: 1
                },
                "data[Feature][build_up_area]": {
                    number: true,
                    min: 1
                },
                "data[Feature][carpet_area]": {
                    number: true,
                    min: 1
                },
                "data[Feature][plot_area]": {
                    required: true,
                    number: true,
                    min: 1
                },
                "data[Feature][age_of_property]": {
                    number: true,
                    min: 1,
                    max: 200
                },

                "data[Feature][no_covered]": {
                    required: true,
                },
                "data[Feature][no_open]": {
                    required: true,
                }

            },

            // Specify the validation error messages
            messages: {

                "data[Feature][sbua]": {

                    number: "Enter Only Numeric",
                    min: "Super Built UP Area must be greater than 0"
                },
                "data[Feature][build_up_area]": {

                    number: "Enter Only Numeric",
                    min: "Built UP Area must be greater than 0"
                },
                "data[Feature][carpet_area]": {

                    number: "Enter Only Numeric",
                    min: "Carpet must be greater than 0"

                },
                "data[Feature][plot_area]": {

                    number: "Enter Only Numeric",
                    min: "Plot area must be greater than 0"
                },
                "data[Feature][age_of_property]": {

                    number: "Enter Only Numeric",
                    min: "Age must be greater than 0",
                    max: "Age Not be Less than 200"
                }
            },

            errorPlacement: function (error, element)
            {

                if (element.attr("name") == "data[Feature][sbua]") {

                    error.appendTo("#super_built_error");
                }
                if (element.attr("name") == "data[Feature][build_up_area]") {

                    error.appendTo("#build_up_area_error");
                }
                if (element.attr("name") == "data[Feature][plot_area]") {

                    error.appendTo("#plot_area_error");
                }

                if (element.attr("name") == "data[Feature][carpet_area]") {

                    error.appendTo("#carpet_area_error");
                }
                if (element.attr("name") == "data[Feature][bedrooms]") {


                    error.appendTo("#bedrooms_error");
                }
                if (element.attr("name") == "data[Feature][bathrooms]") {

                    error.appendTo("#bathrooms_error");
                }
                if (element.attr("name") == "data[Feature][balconies]") {

                    error.appendTo("#balconies_error");
                }

                if (element.attr("name") == "data[Feature][property_description]") {

                    error.appendTo("#property_description_error");
                }
                if (element.attr("name") == "data[Feature][age_of_property]") {

                    error.appendTo("#age_error");
                }
                if (element.attr("name") == "data[Feature][Configure]") {

                    error.appendTo("#Configure_error");
                }
                if (element.attr("name") == "data[Feature][washrooms]") {

                    error.appendTo("#washrooms_error");
                }
                if (element.attr("name") == "data[Feature][no_covered]") {

                    error.appendTo("#covered_parking_error");
                }
                if (element.attr("name") == "data[Feature][no_open]") {

                    error.appendTo("#open_parking_error");
                }

            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#property_feature_form").serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        $("#areinsq").val(data);
                        $("#property_pricing_button").css("display", "block");
                        ; // show response from the php script.
                        //$("#property_feature_form :input").prop("disabled", true);
                        $("#next2").html('Saved');
						
						// 4. Expand the next block
				
							$('#property_pricing_button').css('display','block');
							$('#property_pricing_button').addClass('active');
							$('#property_pricing_ajax').css('display','block');
							
											// 5. Close current block
							$('#property_detail_button').removeClass('active');
							$('#property_detail_content').css('display','none');
							$('#id_top').attr('tabindex',-10).focus();
                    }
                });
                return false;
            }
        })

    });

</script>

<!-- jQuery price Form Validation code and submit pricing tab-->
<script>



// Script for add property price
    $(function ()
    {
        var url = "<?php echo $this->webroot; ?>Properties/addpropertyPrice"; // the script where you handle the form input.
        // Setup form validation on the #register-form element
        /*	jQuery.validator.addMethod('greaterThan', function(value, element) {
         return (+$("#market_price").val() > +$("#offer_price").val());
         }, 'Must be greater than start' );
         
         jQuery.validator.addMethod('LesserThan', function(value, element) {
         return (+$("#offer_price").val() < +$("#market_price").val());
         }, 'Must be Lesser than start' );
         */

        $("#form_price").validate({

            rules: {
                "data[offer_price]": {
                    required: true,
                    number: true,
                    min: 1
                },

                "data[booking_amount]": {
                    number: true,
                    min: 1
                },
                "data[deposit]": {
                    number: true,
                    min: 1
                },

                "data[offer_price_sqt]": {
                    number: true
                },

                "data[maintance_amount]": {
                    number: true,
                    min: 1
                }
            },

            // Specify the validation error messages
            messages: {

                "data[offer_price]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    LesserThan: "Offer Price must be Lesser Than Market Price"

                },

                "data[booking_amount]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                },
                "data[deposit]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                },

                "data[offer_price_sqt]": {

                    number: "Enter Only Numeric"
                },

                "data[maintance_amount]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                }


            },

            errorPlacement: function (error, element) {

                if (element.attr("name") == "data[offer_price]") {

                    error.appendTo("#offer_price_error");
                }

                if (element.attr("name") == "data[booking_amount]") {

                    error.appendTo("#booking_amount_error");
                }
                if (element.attr("name") == "data[offer_price_sqt]") {

                    error.appendTo("#offer_price_sqt_error");
                }

                if (element.attr("name") == "data[maintance_amount]") {

                    error.appendTo("#maintance_amount_error");
                }
                if (element.attr("name") == "data[construction_status]") {


                    error.appendTo("#construction_status_error");
                }
                if (element.attr("name") == "data[possession_date]") {

                    error.appendTo("#possession_date_error");
                }
                if (element.attr("name") == "data[ownership_type]") {

                    error.appendTo("#ownership_type_error");
                }
                if (element.attr("name") == "data[sale_type]") {

                    error.appendTo("#sale_type_error");
                }
                if (element.attr("name") == "data[deposit]") {

                    error.appendTo("#deposit_error");
                }
            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form_price").serialize(), // serializes the form's elements.
                    success: function (data)
                    {

                        //$("#property_amenities_button").css("display", "block");
                        //$("#form_price :input").prop("disabled", true);
                        $("#next3").html('Saved');
                        $("#submit_disable").show(); //Show submit button
						
						// 4. Expand the next block
				
							$('#property_amenities_button').css('display','block');
							$('#property_amenities_button').addClass('active');
							$('#aminities_ajax').css('display','block');
							
											// 5. Close current block
							$('#property_pricing_button').removeClass('active');
							$('#property_pricing_ajax').css('display','none');
							$('#id_top').attr('tabindex',-10).focus();
                    }
                });
                return false;
            }
        })

    });

</script>


<!-- Property for on change enable/disable price option -->
<script>
    $(document).ready(function ()
    {

        $("#transaction_type").change(function () {
            var id = $(this).val();
            $("#property_type_val").find('option').removeAttr("selected");
            if (id == '2')
            {
                $('#bookamount').css('display', 'none');
                $('#depositamount').css('display', 'block');
                $('#expected_monthly_show').css('display', 'block');
                $('#offer_price_show').css('display', 'none');
                $('#expected_market_rent_show').css('display', 'block');

                $("#property_type_val option[value='3']").hide(); // hide residential land
                $("#property_type_val option[value='27']").hide(); // hide Commercial land
                $("#disable_on_rent").hide(); // hide floor construction 
                $("#anmities_hide_rent").hide();//hide amenities (pet,veg,Bachelors)


            }
            if (id == '1')
            {
                $('#bookamount').css('display', 'block');
                $('#depositamount').css('display', 'none');
                $('#expected_monthly_show').css('display', 'none');
                $('#offer_price_show').css('display', 'block');
                $('#expected_market_rent_show').css('display', 'none');

                $("#property_type_val option[value='3']").show(); //show residential land
                $("#property_type_val option[value='27']").hide(); // show Commercial land
                $("#disable_on_rent").show(); // show floor construction
                $("#anmities_hide_rent").show();//hide amenities (pet,veg,Bachelors)
            }
        });
    });
</script>

<!-- submit aminities form -->
<script>
    $(function () {

        var url = "<?php echo $this->webroot; ?>Properties/addpropertyamenities"; // the script where you handle the form input.

        // Setup form validation on the #register-form element
        $("#aminities_form").validate({

            rules: {
                "data[Feature][width_of_fac_road]": {
                    number: true,
                    min: 1
                }


            },

            // Specify the validation error messages
            messages: {

                "data[Feature][width_of_fac_road]": {

                    number: "Enter Only Numeric",
                    min: "Width of facing must be greater than 0"
                }
            },

            errorPlacement: function (error, element)
            {

                if (element.attr("name") == "data[Feature][width_of_fac_road]") {

                    error.appendTo("#width_of_fac_road_error");
                }

            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#aminities_form").serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        $("#property_photo_button").css("display", "block");
                        ; // show response from the php script.
                        //$("#aminities_form :input").prop("disabled", true);
                        $("#next4").html('Saved');
						
						$('#property_photo_button').css('display','block');
							$('#property_photo_button').addClass('active');
							$('#property_photo_content').css('display','block');
							
											// 5. Close current block
							$('#property_amenities_button').removeClass('active');
							$('#aminities_ajax').css('display','none');
							$('#id_top').attr('tabindex',-10).focus();

                    }
                })
                return false;
            }
        })

    });

</script> 



<script>

    function number2text(value)
    {
        var fraction = Math.round(frac(value) * 100);
        var f_text = "";

        if (fraction > 0)
        {
            f_text = "AND " + convert_number(fraction) + " PAISE";
        }

        return convert_number(value) + " RUPEE " + f_text + " ONLY";
    }

    function frac(f)
    {
        return f % 1;
    }

    function convert_number(number)
    {
        if ((number < 0) || (number > 999999999))
        {
            return "NUMBER OUT OF RANGE!";
        }

        var Gn = Math.floor(number / 10000000);  /* Crore */
        number -= Gn * 10000000;
        var kn = Math.floor(number / 100000);     /* lakhs */
        number -= kn * 100000;
        var Hn = Math.floor(number / 1000);      /* thousand */
        number -= Hn * 1000;
        var Dn = Math.floor(number / 100);       /* Tens (deca) */
        number = number % 100;               /* Ones */
        var tn = Math.floor(number / 10);
        var one = Math.floor(number % 10);
        var res = "";

        if (Gn > 0)
        {
            res += (convert_number(Gn) + " CRORE");
        }
        if (kn > 0)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " LAKH");
        }
        if (Hn > 0)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " THOUSAND");
        }

        if (Dn)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " HUNDRED");
        }


        var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN");
        var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY");

        if (tn > 0 || one > 0)
        {
            if (!(res == ""))
            {
                res += " AND ";
            }
            if (tn < 2)
            {
                res += ones[tn * 10 + one];
            } else
            {

                res += tens[tn];
                if (one > 0)
                {
                    res += ("-" + ones[one]);
                }
            }
        }

        if (res == "")
        {
            res = "ZERO";
        }

        return res;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
	$("#state").on('change', function () {
            var id = $(this).val();
            if (id != '') {
                $.getJSON('/countries/getpecity/' + id, function(data) {
                    $('#city').empty();
                    $("#locality").empty();
                    for(var cityCode in data) {
                        $('#city').append($('<option>', { 
                            'value': cityCode, 
                            'text': data[cityCode] 
                        }));
                    }
                    $("#city").trigger('change');
                });
            }
        });
       /* $("#state").on('change', function () {
            var id = $(this).val();
            $("#city").find('option').remove();
            $("#locality").find('option').remove();
            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "countries", "action" => "getCities")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#city"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#city"));
                        });
                    }
                }).responseJSON;
            }
        });*/
		
		 $("#city").on('change', function () {
          var id = $(this).val();
          $("#locality").empty();
          if (id) {
            $.getJSON('/countries/getLocalitiesByCity/' + id, function(data) {
              $("#locality").empty();
              if(Object.keys(data).length > 0) {
                for(var localityCode in data) {
                  $('#locality').append($('<option>', { 
                      'value': localityCode, 
                      'text': data[localityCode] 
                  }));
                }  
              } else {
                $('#locality').append($('<option>', { 
                    'value': '0', 
                    'text': '--No Locality Found--'
                }));
              }              
            });

            $.getJSON('/projects/getProjectListByCityId/' + id, function(data) {                
                $("#project_id").empty();
                if(Object.keys(data).length > 0) {
                  for(var project_id in data) {
                    $('#project_id').append($('<option>', { 
                        'value': project_id, 
                        'text': data[project_id] 
                    }));
                  } 
                } else {
                  $('#project_id').append($('<option>', { 
                      'value': '', 
                      'text': '--No Projects Found--'
                  }));
                }       
            });
          }
        });
        /*$("#city").on('change', function () {
            var id = $(this).val();
            $("#locality").find('option').remove();
            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "countries", "action" => "getLocality")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#locality"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#locality"));
                        });
                    }
                }).responseJSON;
            }
        });*/
    });
</script>
<script>
    $(document).ready(function () {
        $("#property-type").on('change', function () {
            var id = $(this).val();
            var transaction = $("#transaction_type").val();

            $("#property_type_val").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyoption")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#property_type_val"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#property_type_val"));
                        });
                        if (transaction == '2')
                        {
                            $("#property_type_val option[value='3']").hide();
                            $("#property_type_val option[value='27']").hide();
                        }
                        if (transaction == '1')
                        {
                            $("#property_type_val option[value='3']").show();
                            $("#property_type_val option[value='27']").show();
                        }
                    }
                }).responseJSON;
            }



        });
		
		// GET PROJECT NAME ON THE BASIS OF SELECTED CITY.
		
		$("#id_select_city").on('change', function () {
            var city = $("#id_select_city").val();
			$("#project_id").find('option').remove();

            if (city) {
                var dataString = 'citycode=' + city;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "projects", "action" => "getProjects")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#project_id"));
                        $.each(html, function (key, value) {
                            $('<option>').val(key).text(value).appendTo($("#project_id"));
                        });
                    }
                }).responseJSON;
            }
        });
		
		//Get locality based on the project.
		
		$("#project_id").on('change', function () {
            var project_id = $("#project_id").val();
			if (project_id) {
                var dataString = 'project_id=' + project_id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "projects", "action" => "getProjectLocality")); ?>',
                    data: dataString,
                    cache: false,
					dataType:'json',
                    success: function (data) {
						$('#searchInput').val(data.locality).trigger('click');
                    }
                }).responseJSON;
            }
        });

    });


</script>	
<script>
    $(document).ready(function () {

        // Parking Checkbox - Special functionality
        $("#property_feature_ajax").on("click", ".resp", function () {

            var $inputs = $('input.resp:checkbox')

            if (($(this).attr('id') == 'id_chk_none') && ($(this).is(':checked')))
            {
                $inputs.not(this).prop('disabled', true); // <-- disable all but checked one
                $inputs.not(this).prop('checked', false); // <-- uncheck all but checked one

                $('#id_select_cvrd').css('display', 'none');
                $('#id_select_open').css('display', 'none');

            } else if (($(this).attr('id') == 'id_chk_cvrd') && ($(this).is(':checked')))
            {
                //alert("Covered Parking is Checked"); 
                // Do action as  covered parking checked

                $('#id_select_cvrd').css('display', 'inline');
            } else if (($(this).attr('id') == 'id_chk_cvrd') && (!($(this).is(':checked'))))
            {
                //alert("Covered Parking is UnChecked"); 
                // Do action as covered parking is unchecked

                $('#id_select_cvrd').css('display', 'none');
            } else if (($(this).attr('id') == 'id_chk_open') && ($(this).is(':checked')))
            {
                //alert("Open Parking is clicked");  	
                // Do action as open parking is checked

                $('#id_select_open').css('display', 'inline');

            } else if (($(this).attr('id') == 'id_chk_open') && (!($(this).is(':checked'))))
            {
                //alert("Open Parking is clicked");  	
                // Do action for open parking is unchecked

                $('#id_select_open').css('display', 'none');
            } else
            {
                $inputs.prop('disabled', false);

            }
        });

$('.page-sidebar style').remove();
    });
</script>
