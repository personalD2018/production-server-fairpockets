<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');
?>

<script type="text/javascript">
jQuery(document).ready(function () {

	jQuery.validator.addMethod("greaterThan",
			function (value, element, params) {

				if (!/Invalid|NaN/.test(new Date(value))) {
					return new Date(value) > new Date($(params).val());
				}

				return isNaN(value) && isNaN($(params).val())
						|| (Number(value) > Number($(params).val()));
			}, 'Must be greater than Launch date.');

	jQuery("#formID").validate(
			{

		rules: {

					'data[ResearchReport][report_price]': {
						required: true,
						number: true,
						min: 0
					},
					'data[ResearchReport][doj2]': {
						greaterThan: '#ResearchReportDoj1'
					},
				},
				messages: {
					'data[ResearchReport][report_price]': {
						required: 'Report Price is required.',
			number: 'Only Numeric Digits allowed.',
						min: 'Min value allowed is 0s',

					}
				}


			}
	);

	$('#add_exercise').on('click', function () {
		$('#exercises').append('<div class="exercise">      <input type="text" name="unittype[]" placeholder="Please Enter Unit Type"><input type="text" name="totalunit[]" placeholder="Please Enter TotalUnit"><input type="text" name="areabaseprice[]" placeholder="Please Enter area Base Price"><button class="remove">x</button></div>');
		return false; //prevent form submission
	});

	$('#exercises').on('click', '.remove', function () {
		$(this).parent().remove();
		return false; //prevent form submission
	});

});
$(function () {
	$('#datetimepicker4').datetimepicker({
		pickTime: false
	});
	$('#datetimepicker5').datetimepicker({
		pickTime: false
	});
});

</script>	


<script type="text/javascript">

$(document).ready(function () {
		$("#broker_id").on('change', function () {
		  var brokerId = $(this).val();
		 // alert(brokerId);
		   $.ajax({
				type: "POST",
				 data: brokerId,
				url: '<?php echo $this->webroot; ?>admin/properties/getMobileByBrokerId/' + brokerId,
				cache: false,
				success: function (data) {
					//alert(data);
				$("#mobile_no").val(data);
				 
				}
			});
	});

});

$(document).ready(function () {
		$("#broker_id2").on('change', function () {
		  var brokerId = $(this).val();
		 // alert(brokerId);
		   $.ajax({
				type: "POST",
				 data: brokerId,
				url: '<?php echo $this->webroot; ?>admin/properties/getMobileByBrokerId/' + brokerId,
				cache: false,
				success: function (data) {
					//alert(data);
				$("#mobile_no2").val(data);
				 
				}
			});
	});

});

$(document).ready(function () {
		$("#broker_id3").on('change', function () {
		  var brokerId = $(this).val();
		 // alert(brokerId);
		   $.ajax({
				type: "POST",
				 data: brokerId,
				url: '<?php echo $this->webroot; ?>admin/properties/getMobileByBrokerId/' + brokerId,
				cache: false,
				success: function (data) {
					//alert(data);
				$("#mobile_no3").val(data);
				 
				}
			});
	});

});

</script>



<style>
.message {
	margin-left: 165px;
	color: green;
	font-size: 20px;
}

textarea {
    width: 150%;
    height: 35px;  
    box-sizing: border-box;   
    background-color: #f8f8f8;
    resize: none;
}

</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>


<script>
$(document).ready(function () {
 var sbua_val = $("#calculatingprice").val();
	$("input[id='offer_price']").on('keyup', function (e) {
		document.getElementById('offer_price_sqt').value= (this.value/sbua_val);
	});
	$("input[id='market_price']").on('keyup', function (e) {           
		document.getElementById('market_price_sqt').value= (this.value/sbua_val);
	});
});
</script>
<?php //echo '<pre>';print_r($propertyDeails);

//echo $this->request->data['Property']['description'];
//echo $calculatingrate;
//echo '<pre>';print_r($this->request->data);die();
 $offer_price_persqfeet = round($this->request->data['Property']['offer_price']/$calculatingrate , 4);
 $market_price_persqfeet = round($this->request->data['Property']['market_price']/$calculatingrate , 4);
?>

<div class="container-fluid">
<div class="row-fluid ">
	<div class="span12">
		<div class="primary-head">
			<h3 class="page-header">Approval For <?php //echo $propertyDeails['Property']['property_id']; ?></h3>

		</div>

	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="content-widgets gray">
			<div class="widget-head blue">
				<h3>Update Property <?php //echo $propertyDeails['Property']['property_id']; ?></h3>
			</div>
			<?php echo $this->Session->flash(); ?> 
			<div class="widget-container">
				<div class="form-container grid-form form-background">
					<?php echo $this->Form->create('Property', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>
					<input id="calculatingprice" class="form-controls" type="hidden" value="<?php echo $calculatingrate; ?>" >
					<div class="row-fluid">
					<div class="span3">
						<div class="control-group">
							<label class="control-label">Offer Price</label>
							<div class="controls">
								<?php echo $this->Form->input('offer_price', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'offer_price', 'class' => 'required')); ?>
							</div>
						</div>
					  </div>
						
						<div class="span3">
						<div class="control-group">
							<label class="control-label">Offer Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('offer_price_sqt', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'offer_price', 'value'=>$offer_price_persqfeet, 'class' => 'required','disabled' => 'disabled')); ?>
							</div>
						</div>
					  </div>
						
					<div class="span3">
						<div class="control-group">
							<label class="control-label">Market Price</label>
							<div class="controls">
								<?php echo $this->Form->input('market_price', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					<div class="span3">
						<div class="control-group">
							<label class="control-label">Market Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('market_price_sqt', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price', 'value'=>$market_price_persqfeet, 'class' => 'required','disabled' => 'disabled')); ?>
							</div>
						</div>
					</div>
					
					
					
					</div>
					
					<div class="row-fluid">
					
					<div class="span3">
					<div class="control-group">
						<label class="control-label">Service status</label>
						<div class="controls">
							<?php echo $this->Form->input('status_id', array('id' => 'status_id', 'options' => array('3' => 'Approved'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
						</div>
					</div>
					</div>
					
					<div class="span3">
					<div class="control-group">
						<label class="control-label">Property detail / type</label>
						<div class="controls">
							<?php echo $this->Form->input('property_type_id', array('id' => 'property_type_id', 'options' => array('1' => 'type1','2'=>'type2'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Property type', 'label' => false)); ?>
						</div>
					</div>
					</div>
					
					<div class="span3">
						<div class="control-group">
							<label class="control-label">Project name</label>
							<div class="controls">
								<!--<?php //echo $this->Form->input('project_id', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'offer_price', 'class' => 'required')); ?>-->
							<input type="text" name="project_id" value="<?php echo $this->Number->getProjectName($this->request->data[Property][project_id]); ?>" />
							
							</div>
						</div>
					  </div>
						
					<div class="span3">
						<div class="control-group">
							<label class="control-label">Locality</label>
							<div class="controls">
								<?php echo $this->Form->input('locality', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					
					
					<!--<div class="span3">
					<div class="control-group">
						<label class="control-label">Description</label>
						<div class="controls">
							<?php echo $this->Form->input('description', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price', 'class' => 'required')); ?>
						</div>
					</div>
					</div> -->
					
					</div>
					
					<div class="row-fluid">
						<div class="span3">
						<div class="control-group">
							<label class="control-label">Description</label>
							<div class="controls">
								<?php echo $this->Form->input('description', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price', 'class' => 'required')); ?>
							</div>
						</div>
						</div>
						
						<div class="span3" style=" width: 172px;height: 17px; background-color:#d4d9dc;    margin-top: 27px;text-align:center;margin-left: 588px;border: 2px solid;">
						<a target="_blank" href="<?php echo $this->webroot; ?>properties/viewproperty/<?php echo $propertyDeails['Property']['property_id']; ?>">
                          Click here for view property</a>
						</div>
						
					</div>
					
					<!-----------------new proerty----------------------- -->
					
					<div class="widget-head blue">
				<h3>Recent Updated Property <?php //echo $propertyDeails['Property']['property_id']; ?></h3>
			</div>
			
					<div class="row-fluid">
					
					<table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>
                                <th class="center">Project Name</th>
                                <th class="center">Locality</th>
                                <th class="center">City</th>
                                <th class="center">Market price</th>
                                <th class="center">Market price per sqft</th>
								 <th class="center">Date valued</th>
								 <th class="center">Remarks</th>
                                <th class="center">
                                    Project Status
                                </th>
                              
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($lastthreeProperty as $data) {
                                ?>
                                <tr>
                                  <!--  <td class="center"><?php echo isset($data['Property']['project_id']) ? $data['Property']['project_id'] : "N/A";?></td>-->
								   
									<td><?php echo $this->Number->getProjectName($data['Property']['project_id']); ?></td>
                                    <td class="center"><?php echo isset($data['Property']['locality']) ? $data['Property']['locality'] : "N/A" ;?></td>
                                  <!--  <td class="center"><?php echo $data['Property']['property_city']; ?></td>-->
									<td><?php echo $this->Number->getCityName($data['Property']['property_city']); ?></td>
									
                                    <td class="center"><?php echo isset($data['Property']['market_price']) ? $data['Property']['market_price'] : "N/A"; ?></td>
                                   <td class="center"><?php echo isset($data['Property']['market_price_sqt']) ? $data['Property']['market_price_sqt'] : "N/A"?></td>
									<td class="center"><?php echo isset($data['Property']['created_date']) ? $data['Property']['created_date'] : "N/A" ; ?></td>
									<td class="center"><?php echo isset($data['admin_prop_broker_research_prices']['remark1']) ? $data['admin_prop_broker_research_prices']['remark1'] : "N/A"; ?></td>
								   <td class="center"><?php
                                        if ($data['Property']['status_id'] == 1) {
                                            echo "Under Approval";
                                        }
                                        if ($data['Property']['status_id'] == 3) {
                                            echo "Approved";
                                        }
										if ($data['Property']['status_id'] == '') {
                                            echo "N/A";
                                        }
                                        ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
					
					
					</div>
					
					
					
					
					<!-- ..................................................................... -->
					<div class="widget-head blue">
						<h3>Online research price<?php //echo $propertyDeails['Property']['property_id']; ?></h3>
					</div>
					
					
					
	
			<div class="row-fluid" style="margin-top: 12px;">
			<div class="span4">
					<div class="control-group">
						<label class="control-label">Website name</label>
						<div class="controls">
							<?php echo $this->Form->input('website_name1', array('id' => 'web1_id', 'options' => array('1' => 'wbsite1','2' => 'wbsite2','3' => 'wbsite3'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Website', 'label' => false)); ?>
						</div>
					</div>
				</div>
					<div class="span4">
					   <div class="control-group">
							<label class="control-label">Market price</label>
							<div class="controls">
								<?php echo $this->Form->input('market_price1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mkt_price','title' => 'Please enter market price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label">Market Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('market_price_sqr_fit1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mkt_price_sqrft_1','title' => 'Please enter market price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					</div>
					
											
				
			<div class="row-fluid">
			<div class="span4">
			<div class="control-group">
					<label class="control-label">Website Name</label>
					<div class="controls">
						<?php echo $this->Form->input('website_name2', array('id' => 'web2_id', 'options' => array('1' => 'wbsite1','2' => 'wbsite2','3' => 'wbsite3'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Website', 'label' => false)); ?>
					</div>
				</div>
			</div>
			<div class="span4">
			<div class="control-group">
					<label class="control-label">Market Price</label>
					<div class="controls">
						<?php echo $this->Form->input('market_price2', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text','title' => 'Please enter market price', 'id' => 'mkt_price_2', 'class' => 'required')); ?>
					</div>
				</div>
			</div>
			<div class="span4">
			<div class="control-group">
					<label class="control-label">Market Price/sqft</label>
					<div class="controls">
						<?php echo $this->Form->input('market_price_sqr_fit2', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mkt_price_sqrft_2','title' => 'Please enter market price', 'class' => 'required')); ?>
					</div>
				</div>
			</div>
			</div>


			<div class="row-fluid">
			<div class="span4">
			<div class="control-group">
			<label class="control-label">Website Name</label>
			<div class="controls">
			   <?php echo $this->Form->input('website_name3', array('id' => 'web3_id', 'options' => array('1' => 'wbsite1','2' => 'wbsite2','3' => 'wbsite3'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Website', 'label' => false)); ?>
			</div>
			</div>
			</div>

			<div class="span4">
			<div class="control-group">
			<label class="control-label">Market Price</label>
			<div class="controls">
				<?php echo $this->Form->input('market_price3', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mkt_price_3', 'onblur' => 'myFunction()' ,'title' => 'Please enter market price', 'class' => 'required')); ?>
			</div>
			</div>
			</div>

			<div class="span4">
			<div class="control-group">
			<label class="control-label">Market Price/sqft</label>
			<div class="controls">
				<?php echo $this->Form->input('market_price_sqr_fit3', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mkt_price_sqrft_3','onblur' => 'myFunction2()','title' => 'Please enter market price', 'class' => 'required')); ?>
			</div>
			</div>
			</div>
			</div>





			<div class="row-fluid">
			<div class="span4">

				<h4>Average</h4>
			  

			</div>
			<div class="span4">
			<div class="control-group">
				<label class="control-label">Average Market Price</label>
				<div class="controls">
					<?php echo $this->Form->input('avg_market_price4', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price4')); ?>
				</div>
			</div>
			</div>
			<div class="span4">
			<div class="control-group">
				<label class="control-label">Average Market Price/sqft</label>
				<div class="controls">
					<?php echo $this->Form->input('avg_market_price_sqr_fit4', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'avge_market_price4')); ?>
				</div>
			</div>
			</div>
			</div>

			
			
			


			<!-- ....................................................................... -->

			<div class="widget-head blue">
						<h3>Broker Research Price<?php //echo $propertyDeails['Property']['property_id']; ?></h3>
					</div>
					
					
					
	
			<div class="row-fluid" style="margin-top: 12px;">
			<div class="span3">
					<div class="control-group">
						<label class="control-label">Broker name</label>
						<div class="controls">
							<?php echo $this->Form->input('broker_name1', array('id' => 'broker_id', 'options' => $brokerDeails, 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please select broker Name', 'label' => false)); ?>
						</div>
					</div>
				</div>
				
				<div class="span2">
					   <div class="control-group">
							<label class="control-label">Mobile No</label>
							<div class="controls">
								<?php echo $this->Form->input('mobile_first1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mobile_no')); ?>
							</div>
						</div>
					</div>
				
					<div class="span2">
					   <div class="control-group">
							<label class="control-label">Market price</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_market_price1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'broker_market_price1','title' => 'Please enter market price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Market Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_market_price_sqr_fit1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'broker_market_price_sqr_fit1','title' => 'Please enter market price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Remarks if any</label>
							<div class="controls">
								<?php echo $this->Form->input('remark1', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'textarea', 'id' => 'mkt_price22')); ?>
							</div>
						</div>
					</div>
					
			  </div>
			  
			
				<div class="row-fluid" style="margin-top: 12px;">
					<div class="span3">
						<div class="control-group">
							<label class="control-label">Broker name</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_name2', array('id' => 'broker_id2', 'options' => $brokerDeails, 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please select broker Name', 'label' => false)); ?>
							</div>
						</div>
					</div>
				
				<div class="span2">
					   <div class="control-group">
							<label class="control-label">Mobile No</label>
							<div class="controls">
								<?php echo $this->Form->input('mobile_first2', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mobile_no2')); ?>
							</div>
						</div>
					</div>
				
					<div class="span2">
					   <div class="control-group">
							<label class="control-label">Market price</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_market_price2', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'broker_market_price2','title' => 'Please enter market price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Market Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_market_price_sqr_fit2', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'broker_market_price_sqr_fit2','title' => 'Please enter market price', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Remarks if any</label>
							<div class="controls">
								<?php echo $this->Form->input('remark2', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'textarea', 'id' => 'remark2', )); ?>
							</div>
						</div>
					</div>
					
			  </div>
					
					
				<div class="row-fluid" style="margin-top: 12px;">
			<div class="span3">
					<div class="control-group">
						<label class="control-label">Broker name</label>
						<div class="controls">
							<?php echo $this->Form->input('broker_name3', array('id' => 'broker_id3', 'options' => $brokerDeails, 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please select broker name', 'label' => false)); ?>
						</div>
					</div>
				</div>
				
				<div class="span2">
					   <div class="control-group">
							<label class="control-label">Mobile No</label>
							<div class="controls">
								<?php echo $this->Form->input('mobile_first3', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'mobile_no3')); ?>
							</div>
						</div>
					</div>
				
					<div class="span2">
					   <div class="control-group">
							<label class="control-label">Market price</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_market_price3', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'broker_market_price3','title' => 'Please enter market price','onblur' => 'myFunction3()', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Market Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('broker_market_price_sqr_fit3', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'broker_market_price_sqr_fit3','title' => 'Please enter market price','onblur' => 'myFunction4()', 'class' => 'required')); ?>
							</div>
						</div>
					</div>
					
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Remarks if any</label>
							<div class="controls">
								<?php echo $this->Form->input('remark3', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'textarea', 'id' => 'remark3_id')); ?>
							</div>
						</div>
					</div>
					
			  </div>	
	
				<div class="row-fluid" style="margin-top: 12px;">
					<div class="span3">
						<div class="control-group">
							<h4 class="control-label">Average</h4>
							
						</div>
					</div>
				
				<div class="span2">
					   <div class="control-group">
							
						</div>
					</div>
				
					<div class="span2">
					   <div class="control-group">
							<label class="control-label">Average Market price</label>
							<div class="controls">
								<?php echo $this->Form->input('avg_broker_market_price4', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'avg_broker_market_price4')); ?>
							</div>
						</div>
					</div>
					
					<div class="span2">
						<div class="control-group">
							<label class="control-label">Average Market Price/sqft</label>
							<div class="controls">
								<?php echo $this->Form->input('avg_broker_market_price_sqr_fit4', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'avg_broker_market_price_sqr_fit4')); ?>
							</div>
						</div>
					</div>
					
			  </div>
	
					<div class="form-actions">
						<button type="submit" class="btn btn-primary">Update</button>

					</div>
					

					
					
					
					</form>
				
			</div>
		</div>
	</div>
</div>
</div>					



<script>
function myFunction() {
//alert('hello');
 var a=jQuery("#mkt_price").val();
 var b=jQuery("#mkt_price_2").val();
 var c=jQuery("#mkt_price_3").val();
 var e=(parseInt(a)+parseInt(b)+parseInt(c))/3;
 $("#market_price4").val(e);
 //alert(c);
}

function myFunction2() {
//alert('hello');
 var mkt_price_sqrft_1=jQuery("#mkt_price_sqrft_1").val();
 var mkt_price_sqrft_2=jQuery("#mkt_price_sqrft_2").val();
 var mkt_price_sqrft_3=jQuery("#mkt_price_sqrft_3").val();
 var g=(parseInt(mkt_price_sqrft_1)+parseInt(mkt_price_sqrft_2)+parseInt(mkt_price_sqrft_3))/3;
 $("#avge_market_price4").val(g);
 //alert(c);
}

function myFunction3() {
//alert('hello');
 var a=jQuery("#broker_market_price1").val();
 var b=jQuery("#broker_market_price2").val();
 var c=jQuery("#broker_market_price3").val();
 var e=(parseInt(a)+parseInt(b)+parseInt(c))/3;
 $("#avg_broker_market_price4").val(e);
 //alert(c);
}

function myFunction4() {
//alert('hello');
 var mkt_price_sqrft_1=jQuery("#broker_market_price_sqr_fit1").val();
 var mkt_price_sqrft_2=jQuery("#broker_market_price_sqr_fit2").val();
 var mkt_price_sqrft_3=jQuery("#broker_market_price_sqr_fit3").val();
 var g=(parseInt(mkt_price_sqrft_1)+parseInt(mkt_price_sqrft_2)+parseInt(mkt_price_sqrft_3))/3;
 $("#avg_broker_market_price_sqr_fit4").val(g);
 //alert(c);
}

</script>










