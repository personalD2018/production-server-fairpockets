<style>
#build{padding-top:15px;}
</style>
<div class="page-content">
	<?php $paginator = $this->Paginator; ?>
	
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">Listed Properties</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="allproperties" >
					<div class="input-group custom-search-form">
						<input type="text" name="search" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		<!--<div class="user_panel_search">
			<form class="search" id="search" method="post"  action="allproperties" >
			<input type="text" name="search" placeholder="Search..">
			<a onclick="document.getElementById('search').submit()" ><i class="fa fa-search fa-lg" aria-hidden="true" style="color:#000;"></i></a>
			</form>
		</div>-->
		
		<div class="detail-features detail-block menu-block target-block col-md-12">
				<div class="user_inner_menu">
					<ul class="panel_menu_top">
						<li><a href="<?php echo $this->webroot; ?>properties/allproperties" <?php
							if ($property_user_inner_menu == 0)
							echo "class=\"menu-item-active\" ";
						?> >All (<?php echo $total; ?>)</a></li>
						<li><a href="<?php echo $this->webroot; ?>properties/allproperties/1" <?php
							if ($property_user_inner_menu == 1)
							echo "class=\"menu-item-active\" ";
						?> >Saved (<?php echo $saved; ?>)</a></li>
						<li><a href="<?php echo $this->webroot; ?>properties/allproperties/2" <?php
							if ($property_user_inner_menu == 2)
							echo "class=\"menu-item-active\" ";
						?> >Approved (<?php echo $approved; ?>)</a></li> 
						<li><a href="<?php echo $this->webroot; ?>properties/allproperties/3" <?php
							if ($property_user_inner_menu == 3)
							echo "class=\"menu-item-active\" ";
						?> >Deactivated (<?php echo $deactived; ?>)</a></li> 
					</ul> 
				</div>
		</div>
		<?php
            if (!empty($allproj)) {
				
                foreach ($allproj as $data) {
					
                    // $project_feature = json_decode($data['Property']['features'], 'true');
				?>	
				<div id="build" class="list-detail target-block">
					
					<div class="detail-title">
						<a>
							<?php
                                if (!empty($project_feature['Configure'])) {
                                    echo $project_feature['Configure']; /* echo " / "; echo $project_feature['bathrooms']; echo " bath"; */
									} else {
                                    if ($data['Property']['transaction_type_id'] == '1') {
                                        echo "Residential Property";
										} else {
                                        echo "Commercial Property";
                                        echo $data['Property']['transaction_type_id'];
									}
								}
							?>
						For sale in <?php if(!empty($data['Project'])){ echo $data['Project']['project_name'];} ?>  <?php if(!empty($data['Project'])){ echo $data['Project']['city_data'];} ?></a>
						
					</div>
					<div class="row">
						
						<div class="col-md-6">
							
							<div class="form-control" > <strong>Offer Price : </strong><i class="fa fa-inr" aria-hidden="true"></i><?php
								if (!empty($data['Property']['offer_price'])) {
									echo " " . $data['Property']['offer_price'];
								}
							?> </div>
							
						</div>
						
						
						<div class="col-md-6">
							<?php if (!empty($project_feature['sbua'])) {
							?>
							<div class="form-control" > <strong>Super Built Up Area : </strong> <?php
								echo $project_feature['sbua'];
								echo " " . $project_feature['sbua_m'];
							?></div>
							<?php } ?>
							
							<?php if (!empty($project_feature['plot_area'])) {
							?>
							<div class="form-control" > <strong>Plot Area : </strong> <?php
								echo $project_feature['plot_area'];
								echo " " . $project_feature['plot_area_sq'];
							?></div>
							<?php } ?>
						</div>		
						
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" > <strong>Property Type : </strong><?php
								if ($data['Property']['transaction_type_id'] == '1') {
									echo "Residential";
                                    } else {
									echo "Commercial";
								}
							?> </div>
						</div>
						<?php
                            if (!empty($data['PropertyTypeOptions']['option_name'])) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Property Type Option : </strong><?php echo $data['PropertyTypeOptions']['option_name']; ?> </div>
							</div>		
						<?php } ?>	
					</div>
					
					
					<div class="row">
						<?php
                            if (!empty($data['Project']['project_address'])) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Address : </strong><?php echo $data['Project']['project_address'];
								?></div>
							</div>
						<?php } ?>	
						<?php
                            if (!empty($data['Project']['locality'])) {
							?>
							<div class="col-md-6">
								<div class="form-control" ><strong>Locality: </strong><?php echo $data['Project']['locality']; ?></div>
							</div>	
						<?php } ?>						
					</div>
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-control" ><strong>Posted on : </strong><?php echo $newDate = date("d M Y", strtotime($data['Property']['created_date'])); ?></div>
						</div>
						<div class="col-md-6">
							
						</div>		
					</div>
					
					<div class="clear"></div>
						<?php $leadsArr = $this->Number->getleadsByPropId($data['Property']['property_id']); ?>
					<div class="block-inner-link">
						<ul>
							<?php if ($data['Property']['status_id'] != 4) { ?>
								<li><a href="<?php echo $this->webroot; ?>properties/editproperty/<?php echo base64_encode($data['Property']['property_id']) ?>">Edit</a></li>
							<?php } ?>
							<?php if ($data['Property']['status_id'] == 3) { ?>
								<li><a href="<?php echo $this->webroot; ?>properties/view_detail_builder/<?php echo $data['Property']['property_id']; ?>" target="_blank">View</a></li>
							<?php } ?>
							<?php if ($data['Property']['status_id'] == 3) { ?>
								<li><a href="<?php echo $this->webroot; ?>properties/deactivateapproved/<?php echo $data['Property']['property_id']; ?>">Deactivate</a></li>
                                <?php } else if ($data['Property']['status_id'] == 4) {
								?>
								<li><a href="<?php echo $this->webroot; ?>properties/activateapproved/<?php echo $data['Property']['property_id']; ?>">Activate</a></li>
                                <?php } else if ($data['Property']['status_id'] == 5) {
								?>
								<li><a href="<?php echo $this->webroot; ?>properties/activate/<?php echo $data['Property']['property_id']; ?>">Activate</a></li>
								<?php
									} else {
								?>
								<li><a href="<?php echo $this->webroot; ?>properties/deactivate/<?php echo $data['Property']['property_id']; ?>">Deactivate</a></li>	
                                <?php }
							?>
							<?php if (!empty($leadsArr)) { ?>
								<li><a href="<?php echo $this->webroot; ?>properties/allresponses/<?php echo $data['Property']['property_id']; ?>" target="_blank">View Response</a></li>
							<?php } ?>
						</ul>
						
					</div>
					<div class="space-m"></div>
				</div>
				
				<?php
				}
				} else {
			?>
			<div id="build" class="list-detail detail-block target-block">
				
				No Properties Listing
				
			</div>
            <?php }
		?>
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php
                    //   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					
                    // the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 4, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					
                    // for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					
                    // the 'last' page button
                    // echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				?>		
				
			</ul>
			
		</div>
		
	</div>
	
</div>
