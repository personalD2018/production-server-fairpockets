<div class="add-tab-row push-padding-bottom">
    <div class="row">

        <?php
        if (in_array("31", $propertyoption)) {
            ?>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="label_title">Type of Flooring</label>
                    <div class="select-style">
                        <?php echo $this->Form->input('Feature.type_of_flooring', array('id' => 'type_of_flooring', 'options' => array('vitrified' => 'Vitrified', 'marble' => 'Marble', 'Ceramic' => 'Ceramic', 'Mosaic' => 'Mosaic', 'Wood' => 'Wood', 'Granite' => 'Granite', 'Spartex' => 'Spartex', 'Cement' => 'Cement', 'Stone' => 'Stone', 'Vinyl' => 'Vinyl', 'IPSFinish' => 'IPSFinish', 'Other' => 'Other'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false)); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("32", $propertyoption)) {
            ?>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="label_title">Power Backup</label>
                    <div class="select-style">
                        <?php echo $this->Form->input('Feature.power_backup', array('id' => 'power_backup', 'options' => array('none' => 'None', 'partial' => 'Partial', 'full' => 'Full'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false)); ?>

                    </div>



                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("8", $propertyoption)) {
            ?>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="label_title">Furnishing</label>
                    <div class="select-style">
                        <?php echo $this->Form->input('Feature.furnishing', array('id' => 'furnishing', 'options' => array('furnished' => 'Furnished', 'semi-furnished' => 'Semi-furnished', 'unfurnished' => 'Unfurnished'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false)); ?>

                    </div>



                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("28", $propertyoption)) {
            ?>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="label_title">Facing</label>
                    <div class="select-style">
                        <?php echo $this->Form->input('Feature.facing', array('id' => 'facing', 'options' => array('North' => 'North', 'East' => 'East', 'South' => 'South', 'West' => 'West', 'North-East' => 'North-East', 'South-East' => 'South-East', 'North-West' => 'North-West', 'South-West' => 'South-West'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false)); ?>

                    </div>


                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("26", $propertyoption)) {
            ?>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="label_title">Society Type</label>
                    <div class="select-style">

                        <?php echo $this->Form->input('Feature.society_type', array('id' => 'society_type', 'options' => array('Gated society,' => 'Gated Society,', 'corner property' => 'Corner Property'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false)); ?>
                    </div>



                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("13", $propertyoption)) {
            ?>
            <div class="col-md-8">
                <div class="col-md-12"><label class="label_title">Width of facing road</label></div>
                <div class="col-md-8"> <?php echo $this->Form->input('Feature.width_of_fac_road', array('id' => 'book-amount-before', 'class' => 'form-control', 'placeholder' => '', 'label' => false)); ?></div>
                <div class="col-md-4" style="top:9px;">
                    <div class="select-style">

                        <?php echo $this->Form->input('Feature.width_of_sq', array('id' => 'width_of_sq', 'options' => array('feet' => 'Feet', 'meter' => 'Meter'), 'class' => 'selectpicker', 'placeholder' => 'Select', 'label' => false)); ?>
                    </div>
                </div>
                <div class="error has-error" style="display:block" id="width_of_fac_road_error"></div>


            </div>
        <?php } ?>

        <div class="form-group col-md-12">
            <?php
            if (in_array("18", $propertyoption)) {
                ?>
                <div class="col-md-12"><label class="label_title">Water Source</label></div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.watersource1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'watersource1', 'hiddenfield' => false)); ?>

                    <label for="watersource1" class="checkbox-custom-label">Municipal</label>
                </div>

                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.watersource2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'watersource2', 'hiddenfield' => false)); ?>

                    <label for="watersource2" class="checkbox-custom-label">Borewell Tank</label>

                </div>  

            <?php } ?>


        </div>

        <div class="form-group col-md-12">
            <?php
            if (in_array("14", $propertyoption)) {
                ?>
                <div class="col-md-12"><label class="label_title">Overlooking</label></div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.overlook1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook1')); ?>

                    <label for="overlook1" class="checkbox-custom-label">Park/Garden</label>
                </div>

                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.overlook2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook2')); ?>

                    <label for="overlook2" class="checkbox-custom-label">Main Road</label>

                </div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.overlook3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook3')); ?>

                    <label for="overlook3" class="checkbox-custom-label">Club</label>

                </div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.overlook4', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'overlook4')); ?>

                    <label for="overlook4" class="checkbox-custom-label">Pool</label>

                </div>
            <?php } ?> 

        </div>

        <div id ="anmities_hide_rent">
            <div class="form-group col-md-4">
                <?php
                if (in_array("15", $propertyoption)) {
                    ?>
                    <div class="col-md-12"><label class="label_title">Bachelors Allowed</label></div>
                    <div class="col-md-3">


                        <?php echo $this->Form->input('Feature.bachelor_all', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>

                        <label for="FeatureBachelorAll1" class="radio-custom-label">Yes</label>

                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('Feature.bachelor_all', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>

                        <label for="FeatureBachelorAll2" class="radio-custom-label">No</label>

                    </div>
                <?php } ?>		
            </div>




            <div class="form-group col-md-4">
                <?php
                if (in_array("16", $propertyoption)) {
                    ?>

                    <div class="col-md-12"><label class="label_title">Pet allowed  </label></div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('Feature.pet_all', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>

                        <label for="FeaturePetAll1" class="radio-custom-label">Yes</label>

                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('Feature.pet_all', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>

                        <label for="FeaturePetAll2" class="radio-custom-label">No</label>

                    </div>
                <?php } ?>			
            </div>


            <div class="form-group col-md-4">
                <?php
                if (in_array("17", $propertyoption)) {
                    ?>
                    <div class="col-md-12"><label class="label_title">Non vegetarian</label></div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('Feature.non_veg', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'id' => 'FeatureNonVeg1')); ?>


                        <label for="FeatureNonVeg11" class="radio-custom-label">Yes</label>

                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('Feature.non_veg', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'id' => 'FeatureNonVeg2')); ?>

                        <label for="FeatureNonVeg21" class="radio-custom-label">No</label>
                    </div>

                <?php } ?>

            </div>
        </div>

        <div class="form-group col-md-4">
            <?php
            if (in_array("25", $propertyoption)) {
                ?>
                <div class="col-md-12"><label class="label_title">Is Boundary Wall Made</label></div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.boundary_wall', array('type' => 'radio', 'options' => array('1' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>

                    <label for="FeatureBoundaryWall1" class="radio-custom-label">Yes</label>

                </div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('Feature.boundary_wall', array('type' => 'radio', 'options' => array('2' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>

                    <label for="FeatureBoundaryWall2" class="radio-custom-label">No</label>
                </div>
            <?php } ?>	
        </div>

    </div>
</div>

<div class="add-tab-row push-padding-bottom">
    <div class="row pro_amen">
        <?php
        if (in_array("1", $aminitesoption)) {
            ?>
            <div class="col-md-3 text-center">

                <div class="checkbox">
                    <label>

                        <?php echo $this->Form->input('amenities.lift', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_lift', 'multiple' => 'checkbox')); ?>

                        <label for="amenities_lift" class="checkbox-amenities icon_lift"><div>Lifts</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("2", $aminitesoption)) {
            ?>
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.park', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_park', 'multiple' => 'checkbox')); ?>

                        <label for="amenities_park" class="checkbox-amenities icon_park"><div>Park</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("3", $aminitesoption)) {
            ?>
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.staff', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_staff')); ?>

                        <label for="amenities_staff" class="checkbox-amenities icon_staff"><div>Maintenance Staff</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("4", $aminitesoption)) {
            ?>
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.parking', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_parking')); ?>

                        <label for="amenities_parking" class="checkbox-amenities icon_parking"><div>Visitor Parking</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("5", $aminitesoption)) {
            ?>
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.waterstorage', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_water')); ?>

                        <label for="amenities_water" class="checkbox-amenities icon_water_storage"><div>Water Storage</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("6", $aminitesoption)) {
            ?>
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.vaastu', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_vaastu')); ?>

                        <label for="amenities_vaastu" class="checkbox-amenities icon_vaastu"><div>Feng shui/Vaastu compliant</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
    </div>


    <div class="row pro_amen">
        <?php
        if (in_array("7", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.intercomm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_intercomm')); ?>

                        <label for="amenities_intercomm" class="checkbox-amenities icon_intercomm"><div>Intercomm Facility</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("8", $aminitesoption)) {
            ?>	 
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.fire_alarm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_security')); ?>

                        <label for="amenities_security" class="checkbox-amenities icon_security"><div>Security/Fire alarm </div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("16", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.waste_disposal', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_waste')); ?>

                        <label for="amenities_waste" class="checkbox-amenities icon_waste"><div>Waste Disposal </div> </label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("17", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.tarrace', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_tarrace')); ?>

                        <label for="amenities_tarrace" class="checkbox-amenities icon_tarrace"><div>Private Garden/Terrace</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("18", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.security_personal', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_security1')); ?>

                        <label for="amenities_security1" class="checkbox-amenities icon_securityp"><div>Security Personnel</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("21", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.goods_lift', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_lift1')); ?>

                        <label for="amenities_lift1" class="checkbox-amenities icon_lift"><div>Service/Goods Lift</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>

    </div>


    <div class="row pro_amen">

        <?php
        if (in_array("22", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label>
                        <?php echo $this->Form->input('amenities.conroom', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_conroom')); ?>

                        <label for="amenities_conroom" class="checkbox-amenities icon_conroom"><div>Conference Room</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("19", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.swimming', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_pool')); ?>

                        <label for="amenities_pool" class="checkbox-amenities icon_pool"><div>Swimming Pool</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("20", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.atm', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_atm')); ?>

                        <label for="amenities_atm" class="checkbox-amenities icon_atm"><div>ATM</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("23", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.ac', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_airc')); ?>

                        <label for="amenities_airc" class="checkbox-amenities icon_airc"><div>Centrally Airconditioned </div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("24", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.internet', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_internet')); ?>

                        <label for="amenities_internet" class="checkbox-amenities icon_internet"><div>High Speed Internet </div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("25", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.food', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_foodc')); ?>

                        <label for="amenities_foodc" class="checkbox-amenities icon_foodc"><div>Cafeteria/Food court</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>

    </div>

    <div class="row pro_amen">

        <?php
        if (in_array("gym", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.gym', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_gym')); ?>

                        <label for="amenities_gym" class="checkbox-amenities icon_gym"><div>Gymnasium</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("water_softening_plant", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.water_plant', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_waterp')); ?>

                        <label for="amenities_waterp" class="checkbox-amenities icon_water_plant"><div>Water Softening Plant</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("community_hall", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.commhall', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_commhall')); ?>

                        <label for="amenities_commhall" class="checkbox-amenities icon_com_hall"><div>Community Hall</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("shopping_center", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.shopping', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_shopping')); ?>

                        <label for="amenities_shopping" class="checkbox-amenities icon_shopping"><div>Shopping Center</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>
        <?php
        if (in_array("15", $aminitesoption)) {
            ?>	
            <div class="col-md-3 text-center">
                <div class="checkbox">
                    <label >
                        <?php echo $this->Form->input('amenities.rainwater', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-ame', 'id' => 'amenities_rain')); ?>

                        <label for="amenities_rain" class="checkbox-amenities icon_rainwater"><div>Rain water harvesting</div></label>

                    </label>
                </div>
            </div>
        <?php } ?>

    </div>



</div> 
<?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
<div class="account-block text-center submit_area">

    <button type="submit" class="btn btn-primary btn-next " id="next4">NEXT</button>

</div>							

