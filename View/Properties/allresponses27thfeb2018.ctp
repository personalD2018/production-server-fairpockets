<!--./advertisement-Card-->
    <div class="page-content">

        <?php
        $paginator = $this->Paginator;
        ?>
            <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Property Leads</h1>
                </div>
                <div class="col-md-3" style="display:none;">
                    <div class="user_panel_search">
                            <form class="search" id="search" method="post"  action="Portfolio_ViewReport" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                           </form>
                       </div>
                   </div>
               </div>
			<div class="space-x"></div>	
			
            <?php
            if (!empty($allresponse)) {
                foreach ($allresponse as $data) {

                    
                    ?>
                    
					<div id="build" class="list-detail detail-block target-block">
        <div class="detail-title"> <span class="col-md-6">3 bedroom / 4 bath</span> <span class="col-md-6">Area:</span><br>
        </div>
         <ul class="list-three-col list-features">
          <li><strong>Name:</strong> <?php echo $data['Lead']['name']; ?></li>
          <li><strong>Phone No:</strong><?php echo $data['Lead']['phone']; ?></li>
          <li><strong>Email:</strong><?php echo $data['Lead']['email']; ?></li>
          <li><strong>Type: <?php echo $data['Lead']['user_type']; ?></strong></li>
          <li><strong>Date:</strong><?php echo $data['Lead']['created']; ?></li>
        </ul>
		
        <div class="clearfix"></div>
        <br>
      </div>
					

                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">

                    No Service Listing

                </div>
            <?php }
            ?>

            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">

                    <?php
                    //   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

                    // the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
                        'tag' => 'li',
                        'currentClass' => 'active', 'class' => 'pagi_nu'));

                    // for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

                    // the 'last' page button
                    // echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>		

                </ul>

            </div>

        </div>		

    </div>

