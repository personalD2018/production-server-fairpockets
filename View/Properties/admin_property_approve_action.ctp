<style>
    .message {
        margin-left: 165px;
        color: green;
        font-size: 20px;
    }
</style>
<?php
echo $this->Html->script('validate_1.9_jquery.validate.min');
?>
<script>
$(document).ready(function () {
     var sbua_val = $("#calculatingprice").val();
        $("input[id='offer_price']").on('keyup', function (e) {
            document.getElementById('offer_price_sqt').value= (this.value/sbua_val);
        });
        $("input[id='market_price']").on('keyup', function (e) {           
            document.getElementById('market_price_sqt').value= (this.value/sbua_val);
        });
    });
</script>
<?php //echo '<pre>';print_r($propertyDeails);
echo $calculatingrate;
//echo '<pre>';print_r($this->request->data);
echo $offer_price_persqfeet = round($this->request->data['Property']['offer_price']/$calculatingrate , 4);
echo $market_price_persqfeet = round($this->request->data['Property']['market_price']/$calculatingrate , 4);
 ?>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">Approval For <?php //echo $propertyDeails['Property']['property_id']; ?></h3>

            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue">
                    <h3>Update Property <?php //echo $propertyDeails['Property']['property_id']; ?></h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">
                    <div class="form-container grid-form form-background">
                        <?php echo $this->Form->create('Property', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>
                        <input id="calculatingprice" class="form-controls" type="hidden" value="<?php echo $calculatingrate; ?>" >
                        <div id="rejection_hide">
                            <div class="control-group">
                                <label class="control-label">Offer Price</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('offer_price', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'offer_price', 'class' => 'span6 required')); ?>
                                </div>
                            </div>
							
							<div class="control-group">
                                <label class="control-label">Offer Price per Sq.Ft</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('offer_price_sqt', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'offer_price_sqt', 'class' => 'span6 required', 'value'=> $offer_price_persqfeet, 'disabled' => 'disabled')); ?>
                                </div>
                            </div>
							
							<div class="control-group">
                                <label class="control-label">Market Price</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('market_price', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price', 'class' => 'span6 required')); ?>
                                </div>
                            </div>
							
							<div class="control-group">
                                <label class="control-label">Market Price per Sq.Ft</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('market_price_sqt', array('div' => false, 'label' => false, 'hiddenField' => false, 'type' => 'text', 'id' => 'market_price_sqt', 'class' => 'span6 required', 'value'=> $market_price_persqfeet, 'disabled' => 'disabled')); ?>
                                </div>
                            </div>

                            
                        </div>
                        <div class="control-group">
                            <label class="control-label">Service status</label>
                            <div class="controls">
                                <?php echo $this->Form->input('status_id', array('id' => 'status', 'options' => array('3' => 'Approved'), 'empty' => 'Select', 'class' => 'chzn-select small required', 'title' => 'Please Select Service Status', 'label' => false)); ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Update</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>					














