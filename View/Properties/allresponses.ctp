<!--./advertisement-Card-->
    <div class="page-content">

        <?php
        $paginator = $this->Paginator;
        ?>
            <div class="row">
                <div class="col-md-9">
                    <h1 class="page-header">Property Leads</h1>
                </div>
                <div class="col-md-3" style="display:none;">
                    <div class="user_panel_search">
                            <form class="search" id="search" method="post"  action="Portfolio_ViewReport" >
                            <div class="input-group custom-search-form">
                                <input type="text" name="search" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                           </form>
                       </div>
                   </div>
               </div>
			<div class="space-x"></div>	
			
            <?php //echo $this->Session->read('Auth.Websiteuser.userrole');
            if (!empty($allresponse)) {
                foreach ($allresponse as $data) {
					
					$propFeatures = $this->Number->getPropFeaturesForResponses($data['Lead']['property_id']);
					$propDetails = $this->Number->getPropertyDetailsById($data['Lead']['property_id']);
					if(!empty($propDetails[0]['Property']['project_id']!=0)){
						$projectName = $this->Number->getProjectNameByProjectId($propDetails[0]['Property']['project_id']);
						$displayProjectName = $projectName[0]['Project']['project_name'];
					}else{
						$displayProjectName = '';
					}
                    
                    ?>
                    <input type="hidden" value="<?php echo $data['Lead']['property_id']; ?>">
					<?php //echo $data['Lead']['property_id']; ?>
					<div id="build" class="list-detail detail-block target-block">
						<div class="detail-title">
						<span class="col-md-6">
						<?php if (!empty($propFeatures[4])) { ?>
							<?php echo $propFeatures[4]; ?> Bedroom /  
						<?php } ?>
						<?php if (!empty($propFeatures[5])) { ?>
							<?php echo $propFeatures[5]; ?> Bathrooms
						<?php } ?>
						
						</span>
						<span class="col-md-6"> Super Built Up Area:
						<?php if (!empty($propFeatures[1])) { ?>
							<?php echo $propFeatures[1]; ?>
						<?php } ?>
						</span><br>
						</div>
						 <ul class="list-three-col list-features">
							  <li><strong>Name: </strong> <?php echo $data['Lead']['name']; ?></li>
							  <li><strong>Phone No: </strong><?php echo $data['Lead']['phone']; ?></li>
							  <li><strong>Email: </strong><?php echo $data['Lead']['email']; ?></li>
							  <li><strong>Type: </strong><?php echo $data['Lead']['user_type']; ?></strong></li>
							  <li><strong>Date: </strong><?php echo $data['Lead']['created']; ?></li>
							  <?php if($displayProjectName!=''){ ?>
								<li>
									<strong>Project :</strong>
									<?php echo $displayProjectName; ?>
								</li>
							  <?php } ?>
							  <li>
							  <strong>Link:</strong>
							  <?php							
							  if($this->Session->read('Auth.Websiteuser.userrole') == 3){ ?>
								http://fairpockets.com/builderPropertyDetails/<?php echo base64_encode($data['Lead']['property_id']) ?>
							  <?php }else{ ?>
								http://fairpockets.com/searchListDetail/<?php echo base64_encode($data['Lead']['property_id']) ?>
							  <?php } ?>
							  </li>
						</ul>
						
						<div class="clearfix"></div>
						<br>
					 </div>
					

                    <?php
                }
            } else {
                ?>
                <div id="build" class="list-detail detail-block target-block">
                    No Service Listing
                </div>
            <?php }
            ?>

            <div class="clear"></div>

            <div class="panel-pagination col-sm-12">

                <ul class="inner_pagi">

                    <?php
                    //   echo $this->Paginator->first(__('First'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if ($paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

                    // the 'number' page buttons
                    echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
                        'tag' => 'li',
                        'currentClass' => 'active', 'class' => 'pagi_nu'));

                    // for the 'next' button
                    if ($paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    }

                    // the 'last' page button
                    // echo $this->Paginator->last(__('Last'), array('tag' => 'li','Class' => 'pagi_nu'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>		

                </ul>

            </div>

        </div>		

    </div>

