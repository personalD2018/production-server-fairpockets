<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>
<?php
if (@$userrole == '3') {
    $view = 'view_detail_builder';
} else {
    $view = 'viewproperty';
}
?>
<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue clearfix">
                    <h3 class="pull-left">SEARCH PROPERTY BY MOBILE</h3>
                    <div class="btn-toolbar top-toolbar pull-right">
                        <div class="btn-group"> <a href="#" id="gridview" class="switcher btn btn-icon"><i class="icon-th-large"></i></a> </div>
                        <div class="btn-group "> <a href="#" id="listview" class="switcher active btn btn-icon"><i class=" icon-list"></i></a> </div>
                    </div>
                </div>
                <div class="widget-container">
                    <div>
                        <?php echo $this->Form->create('Property', array('id' => "formID", 'class' => 'form-horizontal left-align')); ?>
                        <div class="clearfix list-search-bar">
                            <div class="input-append pull-left">
                                <input type="text" size="16" class="input-medium" name="search" Placeholder="Enter Mobile Number">
                                <button type="submit" class="btn">Search</button>
                            </div>

                        </div>
                        </form>

                        <div class="list-wrap">
                            <ul id="products" class="list clearfix">
                                <!-- row 1 -->
                                <?php
                                if (!empty($this->request->data['search'])) {
                                    if (!empty($searchproperty)) {
                                        foreach ($searchproperty as $data) {
                                            $features = json_decode($data['Property']['features'], true);
                                            ?>
                                            <li class="clearfix">
                                                <section class="left"> 
                                                    <h3><?php
                                                        if (!empty($features['Configure']) && $data['PropertyTypeOptions']['transaction_type'] == '1') {
                                                            echo $features['Configure'];
                                                        }
                                                        ?> 
                                                        <?php
                                                        if ($data['PropertyTypeOptions']['transaction_type'] == '1') {
                                                            echo "Residential";
                                                        } else {
                                                            echo "Commercial";
                                                        }
                                                        ?>

                                                        <?php echo $data['PropertyTypeOptions']['option_name'] ?>  for <?php
                                                        if ($data['Property']['transaction_type'] == '1') {
                                                            echo "Sale";
                                                        } else {
                                                            echo "Rent";
                                                        }
                                                        ?>
                                                    </h3>
                                                    <span class="meta">Location: <?php echo $data['Property']['locality']; ?>,<?php echo $data['Property']['city_data']; ?>,<?php echo $data['Property']['state_data']; ?></span>
                                                    <p><?php echo $data['Property']['description']; ?></p>
                                                </section>
                                                <section class="right"> <span class="price"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $data['Property']['offer_price']; ?><br/>@<?php
                                                        if (!empty($features['superbuilt_area_insq'])) {
                                                            $insq = $features['superbuilt_area_insq'];
                                                        } else {
                                                            $insq = $features['plot_area_insq'];
                                                        }

                                                        echo round($data['Property']['offer_price'] / $insq, 3);
                                                        ?> per Sq.Ft</span> <span class="rate-it"></span> <span class="darkview"> <a target="_blank" href="<?php echo $this->webroot; ?>properties/<?php echo $view; ?>/<?php echo $data['Property']['id']; ?>" class="btn btn-extend">View</a> <!--<a href="javascript:void(0);" class="btn btn-info">Add to list</a>--> </span> </section>
                                            </li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <div>
                                            No Record Found!
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


