<?php
echo $this->Html->script('front/jssor.slider-22.2.10.min');
$features = json_decode($propertydetails['Property']['features'], true);
$amenities = json_decode($propertydetails['Property']['amenities'], true);
?>
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate()
    });
</script>				
<style>
    .detail_header .h_feature 
    {
        font-size: 20px;
        color: #29333d;
    }
    .green{color:green; font-weight:bold;border:1px solid; padding:7px;}
    .error{color:red;}
</style>
<style>
    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    .jssora05l.jssora05lds      (disabled)
    .jssora05r.jssora05rds      (disabled)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('img/a17.png') no-repeat;
        overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }
    .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
    .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
</style>

<!--./advertisement-Card-->


<section class=" no-margin">
    <div class="property_detail_header col-sm-12 nopaddingside">
        <div class="container">
            <div class="detail_header col-sm-12 nopaddingside">
                <div class="col-sm-12 breadCrumbs ">
                    <div class="crumb"><a href="">Home</a> > <a href="">Property in Noida</a> > Sector-107 Noida </div>
                </div>

                <!--   <div class="col-sm-1">
                         <div class="circle">20%</div>
                  </div> -->


                <div class="col-sm-4 header-price">

                    <div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $propertydetails['Property']['offer_price']; ?> Lac  <span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i>20% less</span> <span class="price_sq">@ 5,086 per Sq.Ft</span></div>
                    <div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i>25.5 Lac @5,086 per Sq.Ft </div>
                </div>
                <div class="col-sm-5 text-center header-feature">

                    <div class="h_feature"><?php
                        if (!empty($features['bedrooms'])) {
                            echo $features['bedrooms'];
                            echo " Bedroom";
                        }
                        ?> <?php
                        if (!empty($features['bathrooms'])) {
                            echo $features['bathrooms'];
                            echo " Bathroom";
                        }
                        ?> <span class="h_feature_sub"><?php
                        if ($propertyoption['PropertyTypeOptions']['transaction_type'] == '1') {
                            echo "Residential";
                        } else {
                            echo "Commercial";
                        }
                        ?> <?php echo $propertyoption['PropertyTypeOptions']['option_name'] ?>  for <?php
                            if ($propertyoption['PropertyTypeOptions']['transaction_type']) {
                                echo "Sell";
                            } else {
                                echo "Rent";
                            }
                            ?></span></div>
                    <div class="h_location"><?php echo $project['Project']['locality']; ?>,<?php echo $project['Project']['city_data']; ?>,<?php echo $project['Project']['state_data']; ?>,<?php echo $project['Project']['country_data']; ?></div>	
                </div>
                <div class="col-sm-3 text-center header-other-feature">
                    <a href="#calyourprice" class="h_postcal">Calculate Price</a>
                    <div class="h_postdate">Post on <?php echo $newDate = date("d M Y", strtotime($propertydetails['Property']['created_date'])); ?></div>
                    <div class="h_availability"><?php
                        if ($propertydetails['Property']['construction_status'] == '1') {
                            echo "Ready to move in";
                        } else {
                            echo "Under Construction";
                        };
                        ?></div>	
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="no-padding-bottom">

            <div class=" property_detail_area col-sm-12 nopaddingside">

                <div class="col-sm-12 nopaddingside property_detail_inner">
                    <div class="col-sm-9  property-images-main">
                        <div class="row">


                            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                                <!-- Loading Screen -->
                                <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                    <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                </div>
                                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
                                    <a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
                                    <?php
                                    foreach ($propertyImages as $property_value) {
                                        ?>
                                        <div>
                                            <img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                            <img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Thumbnail Navigator -->
                                <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                                    <!-- Thumbnail Item Skin Begin -->
                                    <div data-u="slides" style="cursor: default;">
                                        <div data-u="prototype" class="p">
                                            <div class="w">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                            </div>
                                            <div class="c"></div>
                                        </div>
                                    </div>
                                    <!-- Thumbnail Item Skin End -->
                                </div>
                                <!-- Arrow Navigator -->
                                <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                                <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                            </div>




                        </div>
                        <!-- basic detail -->
                        <div id="detail" class="detail-list detail-block target-block">
                            <div class="detail-title">
                                <h2 class="title-left">Detail</h2>



                            </div>
                            <div class="">
                                <ul class="list-three-col">
                                    <?php
                                    if (!empty($features['sbua'])) {
                                        ?>
                                        <li><strong>Super Built Up Area:</strong> <?php echo $features['sbua']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($features['build_up_area'])) {
                                        ?>
                                        <li><strong>Built Up Area:</strong> <?php echo $features['build_up_area']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($features['carpet_area'])) {
                                        ?>
                                        <li><strong>Carpet Area:</strong> <?php echo $features['carpet_area']; ?> Sq.ft</li>
                                    <?php } ?>

                                    <?php if (!empty($features['plot_area'])) {
                                        ?>
                                        <li><strong>Plot Area:</strong> <?php echo $features['plot_area']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($features['length_plot'])) {
                                        ?>
                                        <li><strong>Length of plot:</strong> <?php echo $features['length_plot']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($features['length_plot'])) {
                                        ?>
                                        <li><strong>Width of plot:</strong> <?php echo $features['length_plot']; ?> Sq.ft</li>
                                    <?php } ?>

                                    <?php if (!empty($features['bedrooms'])) {
                                        ?>
                                        <li><strong>Bedrooms:</strong> <?php echo $features['bedrooms']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['bathrooms'])) {
                                        ?>
                                        <li><strong>Bathrooms:</strong> <?php echo $features['bathrooms']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['washrooms'])) {
                                        ?>
                                        <li><strong>Washrooms:</strong> <?php echo $features['washrooms']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['balconies'])) {
                                        ?>
                                        <li><strong>Balconies:</strong> <?php echo $features['balconies']; ?></li>
                                    <?php } ?>


                                </ul>
                            </div>
                            <div class="clear"></div>

                            <div class="detail-title-inner">
                                <h4 class="title-inner">Additional details</h4>
                            </div>

                            <ul class="list-three-col list_odetail">
                                <?php if (!empty($propertydetails['Property']['project_id'])) {
                                    ?>
                                    <li><strong>Project name:</strong> <span><?php echo $propertydetails['Property']['project_id']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['possession_date'])) {
                                    ?>
                                    <li><strong>Launch date:</strong> <span><?php echo $propertydetails['Property']['possession_date']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['offer_price'])) {
                                    ?>
                                    <li><strong>Offer Price:</strong> <span><?php echo $propertydetails['Property']['offer_price']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['market_price'])) {
                                    ?>
                                    <li><strong>Market Price:</strong> <span><?php echo $propertydetails['Property']['market_price']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['market_rent'])) {
                                    ?>
                                    <li><strong>Market Rent:</strong> <span><?php echo $propertydetails['Property']['market_rent']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['expected_monthly'])) {
                                    ?>
                                    <li><strong>Expected Monthly Rent:</strong> <span><?php echo $propertydetails['Property']['expected_monthly']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['deposit'])) {
                                    ?>
                                    <li><strong>Deposit Amount:</strong> <span><?php echo $propertydetails['Property']['deposit']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['booking_amount'])) {
                                    ?>
                                    <li><strong>Booking Amount:</strong> <span><?php echo $propertydetails['Property']['booking_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['maintance_amount'])) {
                                    ?>
                                    <li><strong>Maintance Amount:</strong> <span><?php echo $propertydetails['Property']['maintance_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($propertydetails['Property']['possession_date'])) {
                                    ?>
                                    <li><strong>Possession Date:</strong> <span><?php echo $propertydetails['Property']['possession_date']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['ownership_type'])) {
                                    ?>
                                    <li><strong>Ownership Type:</strong> <span><?php
                                            if ($propertydetails['Property']['ownership_type'] == '1') {
                                                echo "FreeHold";
                                            }if ($propertydetails['Property']['ownership_type'] == '2') {
                                                echo "Lease Hold";
                                            }if ($propertydetails['Property']['ownership_type'] == '3') {
                                                echo "Power of Attorney";
                                            }if ($propertydetails['Property']['ownership_type'] == '4') {
                                                echo "Co-operative Society";
                                            }
                                            ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['sale_type'])) {
                                    ?>
                                    <li><strong>Sale Type:</strong> <span><?php
                                            if ($propertydetails['Property']['sale_type'] == '1') {
                                                echo "Resale";
                                            } else {
                                                echo "New Booking";
                                            }
                                            ?></span></li>
                                <?php } ?>

                                <?php if (!empty($features['on_floor'])) {
                                    ?>
                                    <li><strong>Property on Floor :</strong> <span><?php echo $features['on_floor']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['in_floor'])) {
                                    ?>
                                    <li><strong>Floors in property:</strong> <span><?php echo $features['in_floor']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($features['address'])) {
                                    ?>
                                    <li><strong>Address:</strong> <span><?php echo $propertydetails['Property']['address']; ?>,<?php echo $locality[1]; ?>,<?php echo $city[1]; ?>,<?php echo $state[1]; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['age_of_property'])) {
                                    ?>
                                    <li><strong>Age of property:</strong> <span><?php echo $features['age_of_property']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['furnishing'])) {
                                    ?>
                                    <li><strong>Furnishing:</strong> <span><?php echo $features['furnishing']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['allowed_floor'])) {
                                    ?>
                                    <li><strong>Floor Allowed For Construction:</strong> <span><?php echo $features['allowed_floor']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['res_parking_1'])) {
                                    ?>
                                    <li><strong>Reserved Parking:</strong> <span><?php echo $features['res_parking_1']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['open_parking'])) {
                                    ?>
                                    <li><strong>No of Open Parking:</strong> <span><?php echo $features['open_parking']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['coverd_parking'])) {
                                    ?>
                                    <li><strong>No of Covered Parking:</strong> <span><?php echo $features['coverd_parking']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['other_room'])) {
                                    ?>
                                    <li><strong>Other Rooms:</strong> <span><?php echo $features['other_room']; ?></span></li>
                                <?php } ?>








                            </ul>


                        </div>
                        <div id="features" class="detail-features detail-block target-block">
                            <div class="detail-title">
                                <h2 class="title-left">Description</h2>
                            </div>


                            <div class="property_des"><?php echo $features['property_description']; ?>.</div>



                        </div>
                        <div id="detail" class="detail-list detail-block target-block">
                            <div class="detail-title">
                                <h2 class="title-left">Project Detail</h2>



                            </div>
                            <div class="builder-view">
                                <div class="col-md-4">
                                    <img src="<?php echo $this->webroot; ?>img/front/projectImg.png" style="border: 1px solid #eaeaea;"> 
                                </div>
                                <div class="col-md-8">
                                    <ul class="list-three-col">
                                        <li><strong>Launch date:</strong> 12-Nov-2017</li>
                                        <li><strong>Expected Completion date:</strong> 12-Nov-2017</li>
                                        <li><strong>Land Area:</strong> 3410 Sq.ft</li>

                                        <li><strong>Number Of Towers:</strong> 12</li>
                                        <li><strong>Total Units:</strong> 3410 Sq.ft</li>
                                        <li><strong>Construction Status:</strong> Status</li>



                                    </ul>
                                </div>
                                <div class="clear"></div>
                                <hr />
                                <div class="builder-item"><strong>Highlights of Project:</strong><br>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                </div>
                                <div class="builder-item"><strong>Description of Project:</strong><br>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                </div>
                                <div class="block-item">
                                    <strong>Financers:</strong><br>
                                    <ul class="banklogo">
                                        <li><img src="<?php echo $this->webroot; ?>img/front/boblogo.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/axis.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/sbilogo.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/icicilogo.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/boblogo.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/axis.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/sbilogo.jpg"></li>
                                        <li><img src="<?php echo $this->webroot; ?>img/front/icicilogo.jpg"></li>
                                    </ul>
                                </div>
                            </div>







                        </div>

                        <!-- basic detail end -->

                        <!-- feature -->
                        <div id="features" class="detail-features detail-block target-block">
                            <div class="detail-title">
                                <h2 class="title-left">Features</h2>
                            </div>
                            <ul class="list-three-col list-features">
                                <?php if (!empty($features['type_of_flooring'])) {
                                    ?>
                                    <li><strong>Type of Flooring:</strong> <?php echo $features['type_of_flooring']; ?></li>
                                <?php } ?>
                                <?php if (!empty($features['power_backup'])) {
                                    ?>
                                    <li><strong>Power Backup:</strong> <?php echo $features['power_backup']; ?></li>
                                <?php } ?>
                                <?php if (!empty($features['watersource1']) || !empty($features['watersource2'])) {
                                    ?>
                                    <li><strong>Water Source:</strong> <?php
                                        if (!empty($features['watersource1'])) {
                                            echo "Municipal";
                                        } if (!empty($features['watersource2'])) {
                                            echo ",Borewell Tank";
                                        }
                                        ?></li>
                                <?php } ?>
                                <?php if (!empty($features['facing'])) {
                                    ?>
                                    <li><strong>Facing:</strong> <?php echo $features['facing']; ?></li>
                                <?php } ?>

                                <?php if (!empty($features['width_of_fac_road'])) {
                                    ?>
                                    <li><strong>Width of facing road:</strong> <?php echo $features['width_of_fac_road']; ?></li>
                                <?php } ?>
                                <?php if (!empty($features['overlook1']) || !empty($features['overlook2']) || !empty($features['overlook3']) || !empty($features['overlook4'])) {
                                    ?>
                                    <li><strong>Overlooking:</strong> <?php
                                        if (!empty($features['overlook1'])) {
                                            echo "Park/Garden";
                                        } if (!empty($features['overlook2'])) {
                                            "Main Road";
                                        }if (!empty($features['overlook3'])) {
                                            echo "Club";
                                        }if (!empty($features['overlook4'])) {
                                            echo "Pool";
                                        }
                                        ?></li>
                                <?php } ?>
                                <?php if (!empty($features['bachelor_all'])) {
                                    ?>
                                    <li><strong>Bachelors Allowed:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($features['pet_all'])) {
                                    ?>
                                    <li><strong>Pet allowed:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($features['non_veg'])) {
                                    ?>
                                    <li><strong>Non vegetarian:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($features['boundary_wall'])) {
                                    ?>
                                    <li><strong>Boundary wall:</strong>Yes</li>
                                <?php } ?>
                                <?php if (!empty($features['society_type'])) {
                                    ?>
                                    <li><strong>Society Type:</strong> <span><?php echo $features['society_type']; ?></span></li>
                                <?php } ?>
                            </ul>
                        </div>

                        <!-- feature end -->
                        <!-- Amenities -->
                        <div id="amenities" class="detail-amenities detail-block target-block col-md-12">
                            <div class="detail-title">
                                <h2 class="title-left">Amenities</h2>
                            </div>
                            <ul class=" list-amenities">
                                <div class ="col-md-3">
                                    <?php
                                    $count = 0;

                                    foreach ($amenities as $key => $amevalu) {
                                        $count++;
                                        if ($count % 1 == 0) {
                                            echo "</div>";
                                            echo '<div class ="col-md-3">';
                                        }
                                        ?>

                                        <!--<div class= "customDiv">column1</div>-->
                                        <?php
                                        if ($key == 'vaastu') {
                                            ?>
                                            <li><span class="icon_amen icon_vaastu">Feng shui/Vaastu compliant</span></li>
                                        <?php } ?>

                                        <?php
                                        if ($key == 'lift') {
                                            ?>
                                            <li><span class="icon_amen icon_lift">Lift</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'park') {
                                            ?>
                                            <li><span class="icon_amen icon_park">Park</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'staff') {
                                            ?>
                                            <li><span class="icon_amen icon_staff">Maintenance Staff</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'parking') {
                                            ?>
                                            <li><span class="icon_amen icon_parking">Visitor Parking</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'waterstorage') {
                                            ?>
                                            <li><span class="icon_amen icon_water_storage">Water Storage</span></li>
                                        <?php } ?>



                                        <!-- <div class= "customDiv">column2</div>-->

                                        <?php
                                        if ($key == 'intercomm') {
                                            ?>
                                            <li><span class="icon_amen icon_intercomm">Intercomm Facility</span></li>
                                        <?php } ?>

                                        <?php
                                        if ($key == 'waste_disposal') {
                                            ?>
                                            <li><span class="icon_amen icon_waste">Waste Disposal</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'tarrace') {
                                            ?>
                                            <li><span class="icon_amen icon_tarrace">Private Garden/Terrace</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'security_personal') {
                                            ?>
                                            <li><span class="icon_amen icon_securityp">Security Personnel</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'goods_lift') {
                                            ?>
                                            <li><span class="icon_amen icon_lift">Service/Goods Lift</span></li>
                                        <?php } ?>


                                        <!-- <div class= "customDiv">column3</div>-->
                                        <?php
                                        if ($key == 'conroom') {
                                            ?>   
                                            <li><span class="icon_amen icon_conroom">Conference Room</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'swimming') {
                                            ?>
                                            <li><span class="icon_amen icon_pool">Swimming Pool</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'atm') {
                                            ?>
                                            <li><span class="icon_amen icon_atm">ATM</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'ac') {
                                            ?>
                                            <li><span class="icon_amen icon_airc">Centrally Airconditioned</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'internet') {
                                            ?>
                                            <li><span class="icon_amen icon_internet">High Speed Internet</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'food') {
                                            ?>
                                            <li><span class="icon_amen icon_foodc">Cafeteria/Food court</span></li>
                                        <?php } ?>


                                        <!-- <div class= "customDiv">column4</div>-->

                                        <?php
                                        if ($key == 'gym') {
                                            ?>
                                            <li><span class="icon_amen icon_gym">Gymnasium</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'water_softening_plant') {
                                            ?>	
                                            <li><span class="icon_amen icon_water_plant">Water Softening Plant</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'community_hall') {
                                            ?>
                                            <li><span class="icon_amen icon_com_hall">Community Hall</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'shopping_center') {
                                            ?>
                                            <li><span class="icon_amen icon_shopping">Shopping Center</span></li>
                                        <?php } ?>
                                        <?php
                                        if ($key == 'rainwater') {
                                            ?>
                                            <li><span class="icon_amen icon_rainwater">Rain water harvesting</span></li>
                                        <?php } ?>

                                        <?php
                                        if ($key == 'fire_alarm') {
                                            ?>
                                            <li><span class="icon_amen icon_security">Security/Fire alarm</span></li>
                                        <?php } ?>
                                    <?php } ?>

                                </div>
                        </div>
                        </ul>
                    </div>
                    <!-- Amenities end -->
                    <div id="" class="detail-map detail-block target-block col-md-12">
                        <div class="detail-title">
                            <h2 class="title-left">Location</h2>
                        </div>
                        <div class="inner-block">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1751.7708179162832!2d77.31362898135131!3d28.583523684277832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce45edf153243%3A0x329d325ab41b08aa!2sByinches+Web+solutions!5e0!3m2!1sen!2sin!4v1488443265073" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div id="calyourprice" class="detail-amenities detail-block target-block col-md-12">

                        <div class="">

                            <div class=" col-md-12">
                                <div class="detail-title">
                                    <h2  class="title-left">Project Price Calculator</h2>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <div class="col-sm-12"><label class="label_title">Unit size </label></div>
                                    <div class="col-sm-3">
                                        <input id="pet_all1" class="radio-custom" name="pet_all" type="radio" >
                                        <label for="pet_all1" class="radio-custom-label">1 BHK - 1000SqFt</label>

                                    </div>
                                    <div class="col-sm-3">
                                        <input id="pet_all2" class="radio-custom" name="pet_all" type="radio" >
                                        <label for="pet_all2" class="radio-custom-label">2 BHK - 1200SqFt</label>

                                    </div>
                                    <div class="col-sm-3">
                                        <input id="pet_all2" class="radio-custom" name="pet_all" type="radio" >
                                        <label for="pet_all2" class="radio-custom-label">3 BHK - 1500SqFt</label>

                                    </div>
                                    <div class="col-sm-3">
                                        <input id="pet_all2" class="radio-custom" name="pet_all" type="radio" >
                                        <label for="pet_all2" class="radio-custom-label">3 BHK - 1800SqFt</label>

                                    </div>


                                </div>

                            </div>

                            <div id="amenities" class="detail-amenities detail-block target-block col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="list-three-col">
                                            <li><strong>Base Selling Price:</strong> <i class="fa fa-inr" aria-hidden="true"></i>5200/Sqft</li>
                                            <li><strong>GST: </strong> <i class="fa fa-inr" aria-hidden="true"></i>230000</li>

                                        </ul>

                                    </div>
                                </div>
                            </div>




                            <!--builder name -->






                            <!-- builder name end-->


                            <div class="col-md-6">
                                <div class="detail-title">

                                    <h2 class="title-left"> Mandatory Charges</h2>
                                </div> 

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">

                                            <label for="" class="checkbox-custom-label"><i class="fa fa-check-square-o" aria-hidden="true"></i> Car Park  <span class="plc_amount"><i class="fa fa-inr" aria-hidden="true"></i>2 Lac</span></label>  


                                        </div>
                                        <div class="col-sm-12">


                                            <label for="" class="checkbox-custom-label"><i class="fa fa-check-square-o" aria-hidden="true"></i> EEC <span class="plc_amount"><i class="fa fa-inr" aria-hidden="true"></i>2 Lac</span></label>


                                        </div>
                                        <div class="col-sm-12">


                                            <label for="" class="checkbox-custom-label"><i class="fa fa-check-square-o" aria-hidden="true"></i> Interest free maintenance security <span class="plc_amount"><i class="fa fa-inr" aria-hidden="true"></i>2 Lac</span></label>


                                        </div>
                                    </div>
                                </div>




                            </div>




                            <div class="col-md-6">
                                <div class="detail-title">

                                    <h2 class="title-left">Optional Charges</h2>
                                </div> 

                                <div class="form-group ">

                                    <div class="col-sm-12">
                                        <input id="overlook1" class="checkbox-custom" name="overlook[]" type="checkbox" >
                                        <label for="overlook1" class="checkbox-custom-label">Floor PlC  <span class="plc_amount"><i class="fa fa-inr" aria-hidden="true"></i>2 Lac</span></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <input id="overlook2" class="checkbox-custom" name="overlook[]" type="checkbox" >
                                        <label for="overlook2" class="checkbox-custom-label">EEC <span class="plc_amount"><i class="fa fa-inr" aria-hidden="true"></i>2 Lac</span></label>

                                    </div>
                                    <div class="col-sm-12">
                                        <input id="overlook3" class="checkbox-custom" name="overlook[]" type="checkbox" >
                                        <label for="overlook3" class="checkbox-custom-label">IIC <span class="plc_amount"><i class="fa fa-inr" aria-hidden="true"></i>2 Lac</span></label>

                                    </div>


                                </div>


                            </div>
                            <div class="col-md-12 text-center">

                                <div class="caltotprice">
                                    <div class="caltitle">Your Final Calculated Price</div>
                                    <span class="calprice"><i class="fa fa-inr" aria-hidden="true"></i>48.64 Lac</span>
                                </div>
                            </div>

                        </div>
                    </div>	
                    <div  class="detail-amenities detail-block target-block col-md-12">

                        <div class="builder-view col-md-12">
                            <div class="detail-title">

                                <h2 class="title-left">Builder Name</h2>
                            </div> 

                            <div class="builder-item"><strong>Established year:</strong> 2012</div>
                            <div class="builder-item"><strong>About:</strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</div>
                            <div class="builder-item"><strong>Past Projects: </strong>Arihant</div>


                            <!-- <ul class="list-three-col list-features">
            
             <li><strong>About:</strong></li>
             <li><strong>Past Projects:</strong></li>
              
            
            
            </ul> -->





                        </div>
                    </div>		
                </div>


                <!-- sidebar -->
                <div id="sidebar" class="col-sm-3 nopaddingside detail_sidebar">
                    <div class="col-sm-12  sidebar-inner">

                        <form class="fpform">
                            <div class="row ">
                                <h3>Contact - Fairpockets</h3>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">

                                        <input id="user1" class="radio-custom" name="pet_all" type="radio" >
                                        <label for="user1" class="radio-custom-label">Individual</label>



                                        <input id="user2" class="radio-custom" name="pet_all" type="radio" >
                                        <label for="user2" class="radio-custom-label">Dealer</label>


                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input class="form-control error" placeholder="Your Name" type="text">
                                        <div class="error has-error" style="display:block;" >Box must be filled out</div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Phone" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Email" type="email">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="label_title">Interested in (Optional)</label>
                                        <div class="col-sm-12">
                                            <input id="interest1" class="checkbox-custom" name="interest[]" type="checkbox" >
                                            <label for="interest1" class="checkbox-custom-label">Assisted Search</label>
                                        </div>

                                        <div class="col-sm-12">
                                            <input id="interest2" class="checkbox-custom" name="interest[]" type="checkbox" >
                                            <label for="interest2" class="checkbox-custom-label">Immediate Purchase</label>

                                        </div>
                                        <div class="col-sm-12">
                                            <input id="interest3" class="checkbox-custom" name="interest[]" type="checkbox" >
                                            <label for="interest3" class="checkbox-custom-label">Portfolio Management</label>

                                        </div>

                                    </div>  

                                </div>

                                <div class="text-center col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <button class="btn btn-primary">Submit</button></div></div>

                                <div class="text-center col-sm-12 col-xs-12 " style="padding:10px 0px;">
                                    <div class="form-group">
                                        I agree with Fairpockets <a href="#">T&C </a> </div></div>	
                            </div>


                        </form>

                    </div>
                </div>
                <!-- sidebar end -->
            </div>
        </div>	


        <!--add calculator-->




        <!--add calculator end -->

        <!-- similar property  -->

        <div class="property-listing grid-view  col-md-12">
            <div class="detail-title">

                <h2 class="title-left">Similar properties</h2>
            </div>
            <div class="row">
                <div class="item-wrap">
                    <div class="property-item table-list">
                        <div class="table-cell">
                            <div class="figure-block">
                                <figure class="item-thumb">


                                    <div class="price hide-on-list">

                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                    </div>
                                    <a href="#" class="hover-effect">
                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                    </a>

                                </figure>
                            </div>
                        </div>
                        <div class="item-body table-cell">

                            <div class="body-left table-cell">
                                <div class="info-row">
                                    <div class="label-wrap hide-on-grid">
                                        <div class="label-status label label-default">For Sale</div>
                                        <span class="label label-danger">Sold</span>
                                    </div>
                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                </div>


                            </div>
                            <div class="body-right table-cell hidden-gird-cell">
                                <div class="info-row price">
                                    <p class="price-start">Start from</p>
                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                </div>
                                <div class="info-row phone text-right">
                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                </div>
                            </div>
                            <div class="table-list full-width hide-on-list">
                                <div class="cell">

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="item-foot date hide-on-list">
                        <div class="item-foot-left">
                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                        </div>
                        <div class="item-foot-right">
                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                        </div>
                    </div>
                </div>

                <div class="item-wrap">
                    <div class="property-item table-list">
                        <div class="table-cell">
                            <div class="figure-block">
                                <figure class="item-thumb">


                                    <div class="price hide-on-list">

                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                    </div>
                                    <a href="#" class="hover-effect">
                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                    </a>

                                </figure>
                            </div>
                        </div>
                        <div class="item-body table-cell">

                            <div class="body-left table-cell">
                                <div class="info-row">
                                    <div class="label-wrap hide-on-grid">
                                        <div class="label-status label label-default">For Sale</div>
                                        <span class="label label-danger">Sold</span>
                                    </div>
                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                </div>


                            </div>
                            <div class="body-right table-cell hidden-gird-cell">
                                <div class="info-row price">
                                    <p class="price-start">Start from</p>
                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                </div>
                                <div class="info-row phone text-right">
                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                </div>
                            </div>
                            <div class="table-list full-width hide-on-list">
                                <div class="cell">

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="item-foot date hide-on-list">
                        <div class="item-foot-left">
                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                        </div>
                        <div class="item-foot-right">
                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                        </div>
                    </div>
                </div>

                <div class="item-wrap">
                    <div class="property-item table-list">
                        <div class="table-cell">
                            <div class="figure-block">
                                <figure class="item-thumb">


                                    <div class="price hide-on-list">

                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                    </div>
                                    <a href="#" class="hover-effect">
                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                    </a>

                                </figure>
                            </div>
                        </div>
                        <div class="item-body table-cell">

                            <div class="body-left table-cell">
                                <div class="info-row">
                                    <div class="label-wrap hide-on-grid">
                                        <div class="label-status label label-default">For Sale</div>
                                        <span class="label label-danger">Sold</span>
                                    </div>
                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                </div>


                            </div>
                            <div class="body-right table-cell hidden-gird-cell">
                                <div class="info-row price">
                                    <p class="price-start">Start from</p>
                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                </div>
                                <div class="info-row phone text-right">
                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                </div>
                            </div>
                            <div class="table-list full-width hide-on-list">
                                <div class="cell">

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="item-foot date hide-on-list">
                        <div class="item-foot-left">
                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                        </div>
                        <div class="item-foot-right">
                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                        </div>
                    </div>
                </div>

                <div class="item-wrap">
                    <div class="property-item table-list">
                        <div class="table-cell">
                            <div class="figure-block">
                                <figure class="item-thumb">


                                    <div class="price hide-on-list">

                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                    </div>
                                    <a href="#" class="hover-effect">
                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                    </a>

                                </figure>
                            </div>
                        </div>
                        <div class="item-body table-cell">

                            <div class="body-left table-cell">
                                <div class="info-row">
                                    <div class="label-wrap hide-on-grid">
                                        <div class="label-status label label-default">For Sale</div>
                                        <span class="label label-danger">Sold</span>
                                    </div>
                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                </div>


                            </div>
                            <div class="body-right table-cell hidden-gird-cell">
                                <div class="info-row price">
                                    <p class="price-start">Start from</p>
                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                </div>
                                <div class="info-row phone text-right">
                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                </div>
                            </div>
                            <div class="table-list full-width hide-on-list">
                                <div class="cell">

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="item-foot date hide-on-list">
                        <div class="item-foot-left">
                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                        </div>
                        <div class="item-foot-right">
                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                        </div>
                    </div>
                </div>




            </div>
        </div>

        <!-- similar property end -->		
    </div>



</div>
<!--/.container-->

</section>


<script type="text/javascript">
    jssor_1_slider_init = function () {

        var jssor_1_SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*you can remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>
<script type="text/javascript">jssor_1_slider_init();</script>
