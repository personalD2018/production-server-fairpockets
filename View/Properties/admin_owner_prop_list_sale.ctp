<script>

    $(function () {
        $(".paper-table").tablecloth({
            theme: "paper",
            striped: true,
            sortable: true,
            condensed: false
        });
    });
    $(function () {
        $('.data-grid').dataTable({"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        });
    });

</script>

<div class="container-fluid">
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">FP Properties</h3>

            </div>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head orange">
                    <h3>Owner Properties List Sale</h3>
                </div>
                <?php echo $this->Session->flash(); ?> 
                <div class="widget-container">

                    <table class="table data-grid table-bordered paper-table tbl-serach responsive">
                        <thead>
                            <tr>

                                <th class="center">Property for</th>
								<th class="center">User</th>
                                <th class="center">Property type</th>

                                <th class="center">City</th>
                                <th class="center">Locality </th>
								<th class="center">Dated</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($ownerPropListSale as $data) {
                                ?>
                                <tr>

                                    <td class="center"><?php
                                        if ($data['Property']['transaction_type_id'] == '1') {
                                            echo "Sale";
                                        } else {
                                            echo "Rent";
                                        }
                                        ?></td>
										<td class="center"><?php echo $this->Number->getUserOrgNameByUserId($data['Property']['user_id']); ?></td>
                                    <td class="center"><?php
                                        if ($data['Property']['property_type_id'] < 8) {
                                            echo "Residential";
                                        } else {
                                            echo "Commercial";
                                        }
                                        ?></td>

                                    <td class="center"><?php echo $data['Property']['city_data']; ?></td>
                                    <td class="center"><?php echo $data['Property']['locality']; ?></td>
									<td class="center"><?php echo $data['Property']['created_date']; ?></td>
                                    <td class="center"><div class="btn-toolbar row-action">
                                            <div class="btn-group">
                                                <!--<a target="_blank" href="<?php //echo $this->webroot; ?>properties/editproperty/<?php echo base64_encode($data['Property']['property_id']) ?>"> <button class="btn btn-info" title="Edit"><i class="icon-edit"></i></button></a>-->
                                                <a target="_blank" href="<?php echo $this->webroot; ?>properties/viewproperty/<?php echo $data['Property']['property_id']; ?>">
                                                    <button class="btn btn-danger" title="View Detail" ><i class="icon-eye-open"></i></button></a>
												<?php if ($data['Property']['status_id'] != 3){ ?>
													<a href="<?php echo $this->webroot; ?>admin/properties/salePropertyApproveAction/<?php echo $data['Property']['property_id']; ?>">			
														<button class="btn btn-warning" title="Unapprove"><i class="icon-thumbs-down"></i></button>
													</a>
												<?php }else{ ?>
													<a href="<?php echo $this->webroot; ?>admin/properties/salePropertyApproveAction/<?php echo $data['Property']['property_id']; ?>">			
														<button class="btn btn-success" title="Approved"><i class="icon-thumbs-up"></i></button>
													</a>
												<?php } ?>
												<?php //} ?>
                                                <!--<a href="#">
                                                    <button class="btn btn-inverse" title="Reject"><i class=" icon-remove-sign"></i></button>
                                                </a>-->

                                            </div>
                                        </div></td>	

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>


