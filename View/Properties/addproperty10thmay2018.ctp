<!--./disable tab button-->
<script>
    $(document).ready(function ()
    {
        $("#transaction_type").find('option').removeAttr("selected");
        $("#property_type_val").find('option').removeAttr("selected");
        $("#property-type").find('option').removeAttr("selected");
        $("#submit_disable").hide(); //submit button disable by default
        $("#property_detail_button").css("display", "none");
        $("#property_pricing_button").css("display", "none");
        $("#property_amenities_button").css("display", "none");
        $("#property_photo_button").css("display", "none");

        /*$("input[id='offer_price']").on('keyup', function (e) {

            //alert ( toWords(this.value) );
            $("div[id='offer_price_error']").css('display', 'inline');
            $("div[id='offer_price_error']").text(convert_number(this.value));

            if (e.which === 13) {

                //Disable textbox to prevent multiple submit
                //$(this).attr("disabled", "disabled");

                //Do Stuff, submit, etc..
            }
			
			var sbua_val = $("#sbua").val();
            var plot_area_val = $("#plot_area").val();
            if(sbua_val != null ){
                document.getElementById('offer_price_sqt').value= (this.value/sbua_val);
            }else{
                 document.getElementById('offer_price_sqt').value= (this.value/plot_area_val);
            }
			
        });*/

        /*$("input[id='market_price']").on('keyup', function (e) {

            //alert ( toWords(this.value) );
            $("div[id='market_price_error']").css('display', 'inline');
            $("div[id='market_price_error']").text(convert_number(this.value));

            if (e.which === 13) {

                //Disable textbox to prevent multiple submit
                //$(this).attr("disabled", "disabled");

                //Do Stuff, submit, etc..
            }
			
			var sbua_val = $("#sbua").val();
            var plot_area_val = $("#plot_area").val();
            if(sbua_val != null ){
                document.getElementById('market_price_sqt').value= (this.value/sbua_val);
            }else{
                 document.getElementById('market_price_sqt').value= (this.value/plot_area_val);
            }
        });*/

    });
</script>

<!--Get property aminities and feature by selecting of property option --> 
<script>
    $(document).ready(function () {

        $("#property_type_val").change(function ()
        {
            var id = $(this).val();
            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyfeature")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#property_feature_ajax').html(html); // show response from the php script.
                        $("#cover_parking_form").hide();// cover Parking disable by default
                        $("#open_parking_form").hide();// open Parking disable by default
                    }
                })


                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyaminities")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {

                        $('#aminities_ajax').html(html); // show response from the php script.
                    }
                })
            }
        });
		
		
    });
</script>



<!-- jQuery Form Validation code and submit basic code-->
<script>

// When the browser is ready...
    $(function () {
        var url = "<?php echo $this->webroot; ?>Properties/addproperty"; // the script where you handle the form input.
        // Setup form validation on the #register-form element
        $.validator.addMethod('map_check', function(value, element) 
		{
			var ret_val = checkGmapErrors();
			return ret_val ;

		}, 'Please tick a locality on the map.');
		$("#idForm").validate({
            rules: {
				"data[searchInput]": {
					required : true,
					map_check : true
				},
                "data[pincode]": {
                    required: true,
                    minlength: 6,
                    digits: true
                },
                agree: "required"
            },

            // Specify the validation error messages
            messages: {

                "data[pincode]": {

                    minlength: "Your Pincode must be at least 6 characters long",
                    digits: "Please Enter Only Numeric Data"
                },
				"data[searchInput]": {
                   required : 'Locality is required.'
                }
				
            },            

            submitHandler: function (form) {
                $.ajax
                        ({
                            type: "POST",
                            url: url,
                            data: $("#idForm").serialize(), // serializes the form's elements.
                            success: function (data)
                            {
                                $(".property_id").val(data);
                                $("#property_detail_button").css("display", "block");
                                $("#idForm :input").prop("disabled", true);
                                $(".btn-green").prop("disabled", true);
                                $("#next1").html('Saved');

                            }
                        });
                return false;
            }
        })

    });

</script>
<!-- jQuery Form Validation code and submit Property Details Form-->
<script>

// When the browser is ready...
    $(function () {
        var url = "<?php echo $this->webroot; ?>Properties/addpropertyfeaturedetail"; // the script where you handle the form input.

        // Setup form validation on the #register-form element
		
		/*jQuery.validator.addMethod('greaterThan1', function (value, element) {           
                return (+$("#total_floors").val() > +$("#on_floor").val());          
            }, 'Must be greater than start');

        jQuery.validator.addMethod('LesserThan1', function (value, element) {
        //if ($("#total_floors").val()>0) {
            return (+$("#on_floor").val() < +$("#total_floors").val());  
          //  }
           // else
            //{
                return FALSE;
            //}       
        }, 'Must be Lesser than start');*/
		
		
        $("#property_feature_form").validate({

            rules: {
				"data[Feature][on_floor]": {
                    required: true,
                    //number: true,
                    //LesserThan1: true,
                    //min: 1
                },
                "data[Feature][total_floors]": {
                    required: true,
                    //number: true,
                    //greaterThan1: true,
                    //min: 1
                },
                "data[Feature][sbua]": {
                    required: true,
                    number: true,
                    min: 1
                },
                "data[Feature][build_up_area]": {
                    number: true,
                    min: 1
                },
                "data[Feature][carpet_area]": {
                    number: true,
                    min: 1
                },
                "data[Feature][plot_area]": {
                    required: true,
                    number: true,
                    min: 1
                },
                /*"data[Feature][age_of_property]": {
                    number: true,
                    min: 1,
                    max: 200
                },*/				
                "data[Feature][no_covered]": {
                    required: true,
                },
                "data[Feature][no_open]": {
                    required: true,
                }
            },

            // Specify the validation error messages
            messages: {

                "data[Feature][sbua]": {

                    number: "Enter Only Numeric",
                    min: "Super Built UP Area must be greater than 0"
                },
                "data[Feature][build_up_area]": {

                    number: "Enter Only Numeric",
                    min: "Built UP Area must be greater than 0"
                },
                "data[Feature][carpet_area]": {

                    number: "Enter Only Numeric",
                    min: "Carpet must be greater than 0"

                },
                "data[Feature][plot_area]": {

                    number: "Enter Only Numeric",
                    min: "Plot area must be greater than 0"
                },
                /*"data[Feature][age_of_property]": {

                    number: "Enter Only Numeric",
                    min: "Age must be greater than 0",
                    max: "Age Not be Less than 200"
                },*/
                "data[Feature][on_floor]": {
                    //number: "Enter Only Numeric",
                    //min: "Value must be greater than 0",
                    //LesserThan1: "Property on floor must be Lesser Than Total floors."

                },
                "data[Feature][total_floors]": {
                    //number: "Enter Only Numeric",
                    //min: "Value must be greater than 0",
                    //greaterThan1: "Total Floor must be Greater Than Property on floors."
                },
            },

            errorPlacement: function (error, element)
            {

                if (element.attr("name") == "data[Feature][sbua]") {

                    error.appendTo("#super_built_error");
                }
                if (element.attr("name") == "data[Feature][build_up_area]") {

                    error.appendTo("#build_up_area_error");
                }
                if (element.attr("name") == "data[Feature][plot_area]") {

                    error.appendTo("#plot_area_error");
                }

                if (element.attr("name") == "data[Feature][carpet_area]") {

                    error.appendTo("#carpet_area_error");
                }
                if (element.attr("name") == "data[Feature][bedrooms]") {


                    error.appendTo("#bedrooms_error");
                }
                if (element.attr("name") == "data[Feature][bathrooms]") {

                    error.appendTo("#bathrooms_error");
                }
                if (element.attr("name") == "data[Feature][balconies]") {

                    error.appendTo("#balconies_error");
                }

                if (element.attr("name") == "data[Feature][property_description]") {

                    error.appendTo("#property_description_error");
                }
                /*if (element.attr("name") == "data[Feature][age_of_property]") {

                    error.appendTo("#age_error");
                }*/
                if (element.attr("name") == "data[Feature][Configure]") {

                    error.appendTo("#Configure_error");
                }
                if (element.attr("name") == "data[Feature][washrooms]") {

                    error.appendTo("#washrooms_error");
                }
                if (element.attr("name") == "data[Feature][no_covered]") {

                    error.appendTo("#covered_parking_error");
                }
                if (element.attr("name") == "data[Feature][no_open]") {

                    error.appendTo("#open_parking_error");
                }
				if (element.attr("name") == "data[Feature][total_floors]") {

                    error.appendTo("#total_floors_error");
                }
                if (element.attr("name") == "data[Feature][on_floor]") {

                    error.appendTo("#on_floor_error");
                }
            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#property_feature_form").serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        $("#areinsq").val(data);
                        $("#property_pricing_button").css("display", "block");
                         // show response from the php script.
                        //$("#property_feature_form :input").prop("disabled", true);
						// call function to fetch project prices start //
						getBasePrice();
						// call function to fetch project prices end //
						
                        $("#next2").html('Saved');
                    }
                });
                return false;
            }
        })

    });
	function getBasePrice(){
		$(document).ready( function(){
			var project1 = $("#project_id").val();
			//alert(project1);
			if (project1) {
				var dataString = 'projectid=' + project1;
				
				$.ajax({
					type: "POST",
					url: '<?php echo Router::url(array("controller" => "properties", "action" => "getBasePriceByProjectId")); ?>',
					
					data: dataString,
					cache: false,
					success: function(data){
						//alert(data);
						var superbuiltuparea = $('#sbua').val();
						var baseprice = data*superbuiltuparea;
						$('#offer_price').val(baseprice);
						$('#offer_price_sqt').val(data);
						
					}
				});
			}
		});
	}

</script>

<!-- jQuery price Form Validation code and submit pricing tab-->
<script>



// Script for add property price
    $(function ()
    {
        var url = "<?php echo $this->webroot; ?>Properties/addpropertyPrice"; // the script where you handle the form input.
        // Setup form validation on the #register-form element
		
        /*jQuery.validator.addMethod('greaterThan', function (value, element) {			
			return (+$("#market_price").val() >= +$("#offer_price").val());			
        }, 'Must be greater than start');

        jQuery.validator.addMethod('LesserThan', function (value, element) {
		if ($("#market_price").val()>0) {
			return (+$("#offer_price").val() <= +$("#market_price").val());	
			}
			else
			{
				return FALSE;
			}		
        }, 'Must be Lesser than start');*/

        jQuery.validator.addMethod('greatermThan', function (value, element) {
            debugger;
            if ($("#market_rent").val() && $("#expected_monthly").val()) {
                return (+$("#market_rent").val() > +$("#expected_monthly").val());
            }
        }, 'Must be greater than start');

        jQuery.validator.addMethod('LessermThan', function (value, element) {
            if ($("#market_rent").val() && $("#expected_monthly").val()) {
                return (+$("#expected_monthly").val() < +$("#market_rent").val());
            }
        }, 'Must be Lesser than start');

        $("#form_price").validate({

            rules: {
                /*"data[offer_price]": {
                    required: true,
                    number: true,
                    LesserThan: true,
                    min: 1
                },
                "data[market_price]": {
                    required: true,
                    number: true,
                    greaterThan: true,
                    min: 1
                },*/
                "data[expected_monthly]": {
                    required: true,
                    number: true,
                    LessermThan: true,
                    min: 1
                },
                "data[market_rent]": {
                    required: true,
                    number: true,
                    greatermThan: true,
                    min: 1
                },

                "data[booking_amount]": {
                    number: true,
                    min: 1
                },
                "data[deposit]": {
                    number: true,
                    min: 1
                },

                /*"data[offer_price_sqt]": {
                    number: true
                },
                "data[market_price_sqt]": {
                    number: true
                },*/
                "data[maintance_amount]": {
                    number: true,
                    min: 1
                }
            },

            // Specify the validation error messages
            messages: {

               /* "data[offer_price]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    LesserThan: "Offer Price must be Lesser Than Market Price"

                },

                "data[market_price]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    greaterThan: "Market Price must be Greater Than Offer Price"
                },*/

                "data[expected_monthly]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    LessermThan: "Expected Monthly Rent must be Lesser Than Expected Market Rent"

                },

                "data[market_rent]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0",
                    greatermThan: "Market Rent must be Greater Than Expected Monthly Rent"
                },

                "data[booking_amount]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                },
                "data[deposit]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                },

               /* "data[offer_price_sqt]": {

                    number: "Enter Only Numeric"
                },
                "data[market_price_sqt]": {

                    number: "Enter Only Numeric"
                },*/
                "data[maintance_amount]": {

                    number: "Enter Only Numeric",
                    min: "Value must be greater than 0"
                }


            },

            errorPlacement: function (error, element) {

                /*if (element.attr("name") == "data[offer_price]") {

                    error.appendTo("#offer_price_error");
                }
                if (element.attr("name") == "data[market_price]") {

                    error.appendTo("#market_price_error");
                }*/
                if (element.attr("name") == "data[expected_monthly]") {

                    error.appendTo("#expected_monthly_error");
                }
                if (element.attr("name") == "data[market_rent]") {

                    error.appendTo("#market_rent_error");
                }



                if (element.attr("name") == "data[booking_amount]") {

                    error.appendTo("#booking_amount_error");
                }
                /*if (element.attr("name") == "data[offer_price_sqt]") {

                    error.appendTo("#offer_price_sqt_error");
                }
                if (element.attr("name") == "data[market_price_sqt]") {

                    error.appendTo("#market_price_sqt_error");
                }*/
                if (element.attr("name") == "data[maintance_amount]") {

                    error.appendTo("#maintance_amount_error");
                }
                if (element.attr("name") == "data[construction_status]") {


                    error.appendTo("#construction_status_error");
                }
                if (element.attr("name") == "data[possession_date]") {

                    error.appendTo("#possession_date_error");
                }
                if (element.attr("name") == "data[ownership_type]") {

                    error.appendTo("#ownership_type_error");
                }
                if (element.attr("name") == "data[sale_type]") {

                    error.appendTo("#sale_type_error");
                }
                if (element.attr("name") == "data[deposit]") {

                    error.appendTo("#deposit_error");
                }
            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form_price").serialize(), // serializes the form's elements.
                    success: function (data)
                    {

                        $("#property_amenities_button").css("display", "block");
                        //$("#form_price :input").prop("disabled", true);
                        $("#next3").html('Saved');

                    }
                });
                return false;
            }
        })

    });

</script>


<!-- Property for on change enable/disable price option -->
<script>
    $(document).ready(function ()
    {
        $("#transaction_type").change(function () {
            var id = $(this).val();
            $("#property_type_val").find('option').removeAttr("selected");
            if (id == '2')
            {
                $('#bookamount').css('display', 'none');
                $('#depositamount').css('display', 'block');
                $('#expected_monthly_show').css('display', 'block');
                $('#offer_price_show').css('display', 'none');
                $('#offer_price_sqt_show').css('display', 'none');
                $('#expected_market_rent_show').css('display', 'block');
                $('#market_price_show').css('display', 'none');
                $('#market_price_sqt_show').css('display', 'none');
                $("#property_type_val option[value='3']").hide(); // hide residential land
                $("#property_type_val option[value='27']").hide(); // hide Commercial land
                $("#disable_on_rent").hide(); // hide floor construction 
                $("#anmities_hide_rent").show();//hide amenities (pet,veg,Bachelors)
				$('#sale_type_show').css('display', 'none');
				$('#ownership_show').css('display', 'none');


            }
            if (id == '1')
            {
                $('#bookamount').css('display', 'block');
                $('#depositamount').css('display', 'none');
                $('#expected_monthly_show').css('display', 'none');
                $('#offer_price_show').css('display', 'block');
                $('#offer_price_sqt_show').css('display', 'block');
                $('#expected_market_rent_show').css('display', 'none');
                $('#market_price_show').css('display', 'block');
                $('#market_price_sqt_show').css('display', 'block');
                $("#property_type_val option[value='3']").show(); //show residential land
                $("#property_type_val option[value='27']").hide(); // show Commercial land
                $("#disable_on_rent").show(); // show floor construction
                $("#anmities_hide_rent").hide();//hide amenities (pet,veg,Bachelors)
            }
        });
    });
</script>

<!-- submit aminities form -->
<script>
    $(function () {

        var url = "<?php echo $this->webroot; ?>Properties/addpropertyamenities"; // the script where you handle the form input.

        // Setup form validation on the #register-form element
        $("#aminities_form").validate({

            rules: {
                "data[Feature][width_of_fac_road]": {
                    number: true,
                    min: 1
                }


            },

            // Specify the validation error messages
            messages: {

                "data[Feature][width_of_fac_road]": {

                    number: "Enter Only Numeric",
                    min: "Width of facing must be greater than 0"
                }
            },

            errorPlacement: function (error, element)
            {

                if (element.attr("name") == "data[Feature][width_of_fac_road]") {

                    error.appendTo("#width_of_fac_road_error");
                }

            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#aminities_form").serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        $("#property_photo_button").css("display", "block");
                        ; // show response from the php script.
                        //$("#aminities_form :input").prop("disabled", true);
                        $("#next4").html('Saved');
                        $("#submit_disable").show(); //Show submit button
                    }
                })
                return false;
            }
        })

    });

</script> 



<script>

    function number2text(value)
    {
        var fraction = Math.round(frac(value) * 100);
        var f_text = "";

        if (fraction > 0)
        {
            f_text = "AND " + convert_number(fraction) + " PAISE";
        }

        return convert_number(value) + " RUPEE " + f_text + " ONLY";
    }

    function frac(f)
    {
        return f % 1;
    }

    function convert_number(number)
    {
        if ((number < 0) || (number > 999999999))
        {
            return "NUMBER OUT OF RANGE!";
        }

        var Gn = Math.floor(number / 10000000);  /* Crore */
        number -= Gn * 10000000;
        var kn = Math.floor(number / 100000);     /* lakhs */
        number -= kn * 100000;
        var Hn = Math.floor(number / 1000);      /* thousand */
        number -= Hn * 1000;
        var Dn = Math.floor(number / 100);       /* Tens (deca) */
        number = number % 100;               /* Ones */
        var tn = Math.floor(number / 10);
        var one = Math.floor(number % 10);
        var res = "";

        if (Gn > 0)
        {
            res += (convert_number(Gn) + " CRORE");
        }
        if (kn > 0)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " LAKH");
        }
        if (Hn > 0)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " THOUSAND");
        }

        if (Dn)
        {
            res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " HUNDRED");
        }


        var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN");
        var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY");

        if (tn > 0 || one > 0)
        {
            if (!(res == ""))
            {
                res += " AND ";
            }
            if (tn < 2)
            {
                res += ones[tn * 10 + one];
            } else
            {

                res += tens[tn];
                if (one > 0)
                {
                    res += ("-" + ones[one]);
                }
            }
        }

        if (res == "")
        {
            res = "ZERO";
        }

        return res;
    }
</script>


<!-- css style to disable button -->

<style>
    .btn-primary.disabled, .btn-primary[disabled] {
        background: #ffc000;
        color: #333;
        outline: none;
        box-shadow: none;
    }
    .add-tab-content .add-tab-row {
        border-bottom: none;
    }
	
	#image_preview .property_image{
		padding: 10px;
	}
	
	#image_preview .property_image .icon-delete {
		font-size: 18px;
		color: red;
		position: absolute;
		top: 85%;
		right: 0;
		cursor: pointer;
	}
</style> 

<div class="page-content">
       <div class="text-center wow fadeInDown no-padding-bottom">
			<h1 class="page-header">Post Your Property </h1><br />
        </div>   
        <div class="add_property_form">
            <form class="fpform" id="idForm">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Basic Information</h3>
                        <div class="add-expand active"></div>
                    </div>
                    <div class="add-tab-content detail-block" style="display:block;">
                        <div class="add-tab-row push-padding-bottom">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">List Property for</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('transaction_type_id', array('id' => 'transaction_type', 'options' => array('1' => 'Sell'), 'class' => 'selectpicker', 'placeholder' => 'Select Property for', 'label' => false)); ?>

                                        </div>		
                                        <div class="error has-error" id="transaction_type_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="property_type" class="label_title">Property type</label>
                                        <div class="select-style ">
                                            <?php echo $this->Form->input('property_type_op', array('id' => 'property-type', 'options' => array('1' => 'Residential', '2' => 'Commercial'), 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" >Box must be filled out</div>



                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Property Type Option<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('property_type_id', array('id' => 'property_type_val', 'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder Floor', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'), 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="property_type_val_error" style="display:block;"></div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="add-tab-row  push-padding-bottom">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="city" class="label_title">City <span class="mand_field">*</span></label>
										<div class="select-style">
											<?php
											echo $this->Form->input(
												'property_city', array(
													'id' => 'id_select_city', 
													'options' => $city_master,
													'class' => 'selectpicker bs-select-hidden required',
													'label' => false,
													'empty' => 'Select',
													'data-live-search' => true
												)
											);
											?>		
										</div>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<div class="col-md-12">
											<label class="label_title">Project Name</label>
										</div>
										<div class="col-sm-12">
											<div class="select-style">
												<?php echo $this->Form->input(
															'project_id', array(
															'id' => 'project_id', 
															'options' => $userproject, 
															'empty' => 'Select Project', 
															'class' => 'selectpicker bs-select-hidden required', 
															'placeholder' => 'Select Project', 
															'title' => 'Please Select Project', 
															'label' => false));
												?>
											</div>
											
										</div>
										<div class="col-sm-3" style="display:none;">
											<a href="javascript:void(0)" data-toggle="modal" data-target="#addproject" class="btn btn-success">Add Project</a>
										</div>
									</div>
								</div>
                            </div>
                            <div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="searchInput" class="label_title">Select Locality <span class="mand_field">*</span></label>
											<!-- <input class="form-control" id="searchInput" placeholder="Locality"> -->
											<?php
											echo $this->Form->input(
													'searchInput', array(
												'id' => 'searchInput',
												'type' => 'text',
												'class' => 'form-control required',
												'placeholder' => 'Locality',
												'label' => false
													)
											);
											?>
											<div id="result" class="input text gMapResults" style="display:none";></div>
										</div>
									</div>
									<div class="col-md-8">
                                        <div class="form-group">
                                                <label for="countryState" class="label_title">Address</label>
                                                <!-- <input class="form-control" id="countryState" placeholder="House No, Street No, etc."> -->
                                                <?php
                                                echo $this->Form->input(
                                                    'address', array(
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'House No, Street No, etc.',
                                                    'label' => false
                                                        )
                                                );
                                                ?>
                                            </div>
									</div>
											<div class="col-md-4">
								</div>
								<div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="map" id="map" style="border-style: none;border:none;width:100%;" ></div>
                                </div>

                            </div>


                            <div class="row">

                                <input id="id_search_adm_area_1" class="form-controls" type="hidden" >
                                <input id="id_search_adm_area_2" class="form-controls" type="hidden" >

<!-- <input id="searchInput" class="input-controls" type="hidden" > -->
                                <!-- <div class="map" id="map" style="width: 100%; height: 300px;"></div> -->

                                <div class="form_area">

                                    <?php echo $this->Form->input('lat', array('id' => 'lat', 'label' => false, 'value' => @$this->request->data['Builder']['lat'], 'type' => 'hidden')); ?>	
                                    <!-- Latitude -->
                                    <?php echo $this->Form->input('lng', array('id' => 'lng', 'label' => false, 'value' => @$this->request->data['Builder']['lng'], 'type' => 'hidden')); ?>
                                    <?php echo $this->Form->input('block', array('id' => 'block', 'label' => false, 'value' => @$this->request->data['Builder']['block'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('locality', array('id' => 'locality', 'label' => false, 'value' => @$this->request->data['Builder']['locality'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('city_data', array('id' => 'city_data', 'label' => false, 'value' => @$this->request->data['Builder']['city_data'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('tbUnit8', array('id' => 'tbUnit8', 'label' => false, 'value' => '', 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('state_data', array('id' => 'state_data', 'label' => false, 'value' => @$this->request->data['Builder']['state_data'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('country_data', array('id' => 'country_data', 'label' => false, 'value' => @$this->request->data['Builder']['country_data'], 'type' => 'hidden')); ?>	
                                    <?php echo $this->Form->input('pincode', array('id' => 'pincode', 'label' => false, 'value' => @$this->request->data['Builder']['pincode'], 'type' => 'hidden')); ?>	

                                </div>
                            </div>
                            
                            <div class="account-block text-center submit_area">
                                <button type="submit" class="btn btn-primary btn-next " id="next1">NEXT</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            <form class="fpform" id="property_feature_form">
                <div class="account-block property_feature" >
                    <div class="add-title-tab">
                        <h3>Property Details</h3>
                        <div class="add-expand" id="property_detail_button"></div>
                    </div>
                    <div class="add-tab-content detail-block" >
                        <div class="add-tab-row push-padding-bottom" id="property_feature_ajax">

                        </div>
                    </div>
                </div>
            </form>
            <form class="fpform" id="form_price">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Pricing</h3>
                        <div class="add-expand" id="property_pricing_button"></div>
                    </div>
                    <div class="add-tab-content detail-block" id="property_pricing_ajax">

                        <div class="add-tab-row push-padding-bottom">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="offer_price_show">
                                        <label for="property-price" class="label_title">Base Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Asking Price ( All Inclusive Price ). It has to be lower or equal to Market Price."></i>
                                        <?php echo $this->Form->input('offer_price', array('id' => 'offer_price', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Asking Price', 'title' => 'Please Enter Offer Price', 'label' => false, 'readonly' => 'readonly')); ?>
                                        <div class="error has-error" id="offer_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_monthly_show" style="display:none">
                                        <label for="property-price" class="label_title">Expected Monthly Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Expected Monthy Rent ( All Inclusive Price ). It has to be lower or equal to Market Rent."></i>
                                        <?php echo $this->Form->input('expected_monthly', array('id' => 'expected_monthly', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Expected Monthly Rent', 'title' => 'Please Enter Monthly Rent', 'label' => false)); ?>
                                        <div class="error has-error" id="expected_monthly_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="offer_price_sqt_show">
                                        <label for="property-price" class="label_title">Base Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('offer_price_sqt', array('id' => 'offer_price_sqt', 'class' => 'form-control box_price', 'label' => false, 'readonly' => 'readonly')); ?>
                                        <div class="error has-error" id="offer_price_sqt_error" style="display:block"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="row ">


                                <div class="col-md-6" id="">
                                    <div class="form-group" id="market_price_show" style="display:none">
                                        <label for="property-price-before" class="label_title">Market Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Current Approx. Market Price of your property. This will be validated by our experts."></i>

                                        <?php echo $this->Form->input('market_price', array('id' => 'market_price', 'class' => 'form-control box_price required', 'label' => false, 'placeholder' => 'Enter Approx Market Price', 'title' => 'Please Enter Market Price')); ?>

                                        <div class="error has-error" id="market_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_market_rent_show" style="display:none">
                                        <label for="property-price-before" class="label_title"> Expected Market Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Current Approx. Market Rent of your property. This will be validated by our experts."></i>

                                        <?php echo $this->Form->input('market_rent', array('id' => 'market_rent', 'class' => 'form-control box_price required', 'label' => false, 'placeholder' => 'Enter Approx Market Rent', 'title' => 'Please Enter Market Rent')); ?>

                                        <div class="error has-error" id="market_rent_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="market_price_sqt_show" style="display:none">
                                        <label for="property-price" class="label_title">Market Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('market_price_sqt', array('id' => 'market_price_sqt', 'class' => 'form-control box_price', 'label' => false, 'disabled' => 'disabled')); ?>
                                        <div class="error has-error" id="market_price_sqt_error" style="display:block"></div>												

                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div class="form-group" id="bookamount">
                                        <label for="property-price-before" class="label_title">Booking Amount</label>
                                        <?php echo $this->Form->input('booking_amount', array('id' => 'booking_amount', 'class' => 'form-control box_price', 'title' => 'Please Enter Booking Amount', 'label' => false, 'placeholder' => 'Enter booking amount')); ?>

                                        <div class="error has-error" id="booking_amount_error" style="display:block"></div>
                                    </div>
                                    <div class="form-group" id="depositamount" style="display:none">
                                        <label for="property-price-before" class="label_title"> Security Deposit Amount</label>

                                        <?php echo $this->Form->input('deposit', array('id' => 'deposit-amount-before', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Security Deposit Amount')); ?>

                                        <div class="error has-error" id="deposit_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="property-price-maintenance" class="label_title">Maintenance </label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Monthly recurring expenses "></i>
                                        <?php echo $this->Form->input('maintance_amount', array('id' => 'maintance_amount', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Maintenance Amount', 'title' => 'Please Enter Maintenance')); ?>
                                        <div class="error has-error" id="maintance_amount_error" style="display:block"></div>	
                                        <div class="error has-error">Box must be filled out</div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="add-tab-row push-padding-bottom">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Availability</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('construction_status', array('id' => 'constructuion_status', 'options' => array('1' => 'Ready to move in', '2' => 'Under Construction'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Construction Status', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" id="construction_status_error" style="display:block" ></div>



                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Possession Date</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('possession_date', array('id' => 'possession_date', 'options' => array('3' => 'Within 3 Months', '6' => 'Within 6 Months', '12' => '1 Year', '24' => '2 Year', '36' => '3 Year', '48' => '4 Year', '60' => '5 Year'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Possession Date', 'label' => false, 'disabled' => 'disabled')); ?>



                                        </div>	
                                        <div class="error has-error" id="possession_date_error" style="display:block" ></div>	

                                    </div>
                                </div>
                                <div class="col-md-3" id="ownership_show">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Ownership Type</label>

                                        <div class="select-style">

                                            <?php echo $this->Form->input('ownership_type_id', array('id' => 'ownership_type', 'options' => array('1' => 'Freehold', '2' => 'Lease Hold', '3' => 'Power of attorney', '4' => 'Co-operative Society'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Ownership Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="ownership_type_error" style="display:block" ></div>	



                                    </div>
                                </div>
                                <div class="col-md-3" id="sale_type_show">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Sale Type</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('sale_type', array('id' => 'sale_type', 'options' => array('1' => 'Resale', '2' => 'New Booking'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Sale Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="sale_type_error" style="display:block" ></div>	



                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php echo $this->Form->input('areinsq', array('class' => 'form-control', 'type' => 'hidden', 'label' => false, 'id' => 'areinsq')); ?>
                        <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
                        <div class="account-block text-center submit_area">
                            <button type="submit" id="next3" class="btn btn-primary btn-next ">NEXT</button>
                        </div>  


                    </div>
                </div>
            </form>
            <form class="fpform" id="aminities_form">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Features and Amenities</h3>
                        <div class="add-expand" id="property_amenities_button"></div>
                    </div>
                    <div class="add-tab-content detail-block" id="aminities_ajax">


                    </div>

                </div>
            </form>
            <form class="fpform" method="post" action="upload_image" enctype="multipart/form-data">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Add Photos</h3>
                        <div class="add-expand" id="property_photo_button"></div>
                    </div>
                    <div class="add-tab-content detail-block">
                        <div class="add-tab-row">
                            <div class="property-media">
                                <div class="media-gallery">
                                    <div class="row">

                                        <div id="image_preview"></div> 

                                    </div>
                                </div>
                                <div class="account-block text-center">
                                    <div class="row">
                                        <div class="col-md-12">	We recommend minimum image size to be 10KB and maximum size to be 5MB and image resolution should be 800*356 for better view. Accepted file format are .png .jpg .jpeg</div>
                                    </div>
                                </div>
                                <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
                                <div class="upload_btn_area">

                                    <label class="btn btn-default btn-file">
                                        <?php
                                        echo $this->Form->input('upload_file.', ['type' => 'file', 'label' => false, 'multiple' => 'multiple', 'style' => 'display:none', 'onchange' => 'preview_image()']);
                                        ?>
                                        Add Property Picture 
                                    </label>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="account-block text-center submit_area" id="submit_disable">
                    <button type="submit" class="btn btn-primary btn-red">Submit Property</button>
                </div>
            </form>
            <div class="modal fade" id="addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h4 class="modal-title" id="myModalLabel">
                                Add Project
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row" id="projectform">
                                <?php
                                echo $this->Form->create(
                                        'Project', array(
                                    'class' => 'fpform',
                                    'role' => 'form',
                                    'id' => 'id_form_project_basic',
                                    'novalidate' => 'novalidate'
                                        )
                                );
                                ?>

                                <div class="col-md-12" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">



                                    <div class="add-tab-row push-padding-bottom ">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname" class="label_title">Project Name<span class="mand_field">*</span></label>
                                                    <?php
                                                    echo $this->Form->input(
                                                            'project_name', array(
                                                        'id' => 'project_name',
                                                        'type' => 'text',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Project Name',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                    <div class="error has-error">Box must be filled out</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Project Description</label>
                                                        <?php
                                                        echo $this->Form->textarea(
                                                                'project_description', array(
                                                            'id' => 'project_description',
                                                            'class' => 'form-control',
                                                            'placeholder' => 'Enter your Project Description',
                                                            'rows' => '1',
                                                            'label' => false
                                                                )
                                                        );
                                                        ?>
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                            </div >
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname" class="label_title">Highlights</label>
                                                    <?php
                                                    echo $this->Form->textarea(
                                                            'project_highlights', array(
                                                        'id' => 'project_highlights',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Enter your Project Highlights',
                                                        'rows' => '1',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                    <div class="error has-error">Box must be filled out</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname" class="label_title">Acres</label>
                                                    <?php
                                                    echo $this->Form->input(
                                                            'land_area', array(
                                                        'id' => 'land_area',
                                                        'type' => 'text',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Land Area',
                                                        'label' => false
                                                            )
                                                    );
                                                    ?>
                                                    <div class="error has-error">Box must be filled out</div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                    <div class="add-tab-row push-padding-bottom text-center">
										<div class="space-m"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary btn-red">
                                                    Save & Continue</button>
                                            </div>
										<div class="space-m"></div>
                                        </div>

                                    </div>


                                </div>
                                <?php echo $this->Form->input('userproject', array('id' => 'userproject', 'label' => false, 'value' => 1, 'type' => 'hidden')); ?>	

                                <?php
                                echo $this->Form->end();
                                ?>

                            </div>
                            <div class="row" id="Successform" style="display:none">
                                <strong>Project has been Added sucessfully</strong>
                            </div>
							<div class="row sucsok" id="Successform" style="display:none">
						<button type="button" id="btn_ok_1" data-dismiss="modal" class="btn btn-primary">OK</button>
						</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/our-products-->
    </div>
 
<script>
function gMapOptionClick(elem)
	{
		document.getElementById('searchInput').value = elem.innerHTML;
		document.getElementById("result").style.display = 'none';

		//Generate click event
		document.getElementById('searchInput').click();
	}
    $(document).ready(function () {
        $.validator.addMethod('nameCustom', function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z ]+/.test(value);
        }, 'Please use English alphabets only.');

        $.validator.addMethod('pnameCustom', function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9]+/.test(value);
        }, 'Please use Alphanumeric characters only.');


        $('#id_form_project_basic').validate({

            // Specify the validation rules
            rules: {
                'data[Project][project_name]': {
                    required: true,
                    pnameCustom: true
                },
                /*'data[Project][project_highlights]': {
                    required: true
                },
                'data[Project][project_description]': {
                    required: true
                },*/
                'data[Project][land_area]': {
                    //required: true,
                    number: true,
                    min: 1,
                    max: 999999
                }
            },

            // Specify the validation error messages
            messages: {
                'data[Project][project_name]': {
                    required: 'Project Name is required.'
                },
                'data[Project][project_highlights]': {
                    required: 'Project Highlights is required.'
                },
                'data[Project][project_description]': {
                    required: 'Project Description is required.'
                },

                'data[ProjectDetail][land_area]': {
                    required: 'Land Area is required.',
                    number: 'Only Numeric digits allowed.',
                    min: 'Min value allowed is 1',
                    max: 'Max value allowed is 999999'
                }
            },

            submitHandler: function (form) {

                $.ajax
                        ({
                            type: 'POST',
                            url: '<?php echo $this->webroot; ?>Projects/pastproject',
                            data: $('#id_form_project_basic').serialize(), // serializes the form's elements.
                            success: function (data)
                            {
                                $("#project_id").find('option').remove();
                                $("#Successform").css("display", "block");
								$(".sucsok").css("display", "block");
                                $("#projectform").css("display", "none");
                                $('<option>').val('').text('Select').appendTo($("#project_id"));
                                $.each(data, function (key, value) {

                                    $('<option>').val(key).text(value).appendTo($("#project_id"));
                                });
                            }
                        });


                // Validations OK. Form can be submitted

                // TODO : Ajax call

                // Changes of Page when Ajax call is successful



                // To remain on same page after submit
                return false;
            }

        });
    });
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<?php
echo $this->Html->script('add_property_7');
// Jquery Script for Validation of form fields    
echo $this->Html->script('validate_1.9_jquery.validate.min');
echo $this->Html->script('front/additional-methods.min');
echo $this->Html->script('fp-loc8.js');
?>
<script>

	
$(document).ready(function(){   
//$('#possession_date').prop('disabled', false); 
    $('#constructuion_status').on('change',function(){
         var selectid = $(this).val();
		 //alert(selectid);
         if (selectid == 1) {
             $('#possession_date').prop('disabled', true);
             $("#possession_date").css("cursor", "not-allowed");
        }
        else if(selectid == 2) {
			 $('#possession_date').prop('disabled', false);
             $("#possession_date").css("cursor", "auto");
        }
		else{
			$('#possession_date').prop('disabled', true);
            $("#possession_date").css("cursor", "not-allowed");
		}
    });
});

$(document).ready(function () {
	$("#property_type_val").on('change', function () {
		var id = $(this).val();
	    //alert(id);
		if(id == 1){
		$("#project_id").addClass("required error");
		}else if(id == 2){
		$("#project_id").addClass("required error");
		}else{
		$("#project_id").removeClass("required error");
		}
	});
});
    $(document).ready(function () {
        $("#property-type").on('change', function () {
            var id = $(this).val();
            var transaction = $("#transaction_type").val();

            $("#property_type_val").find('option').remove();

            if (id) {
                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "properties", "action" => "getpropertyoption")); ?>',
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $('<option>').val('').text('Select').appendTo($("#property_type_val"));
                        $.each(html, function (key, value) {

                            $('<option>').val(key).text(value).appendTo($("#property_type_val"));
                        });
                        if (transaction == '2')
                        {
                            $("#property_type_val option[value='3']").hide();
                            $("#property_type_val option[value='27']").hide();
                        }
                        if (transaction == '1')
                        {
                            $("#property_type_val option[value='3']").show();
                            $("#property_type_val option[value='27']").show();
                        }
                    }
                }).responseJSON;
            }
        });
		
		// GET PROJECT NAME ON THE BASIS OF SELECTED CITY.
		
		$("#id_select_city").on('change', function () {
			$("#project_id").find('option').remove();

            var cityId = $(this).val();
            if(cityId) {
                $.getJSON('/projects/getProjectListByCityId/' + cityId, function(data) {
                    $('#project_id').empty();

                    if(data.length == 0) {
                        $('#project_id').append($('<option>', { value: '', text: '--No Projects Found--' }));
						$('#project_id').append($('<option>', { value: '0', text: '--Please add Project/Select--' }));

                    } else {
							$('#project_id').append($('<option>', { value: '0', text: '--Please add Project/Select--' }));
                        for(var id in data) {
                            $('#project_id').append($('<option>', { value: id, text: data[id] }));
                        }    
                    }
                });        
            } else {
                $('#project_id').append($('<option>', { value: '', text: '--No Projects Found--' }));
				$('#project_id').append($('<option>', { value: '0', text: '--Please add Project/Select--' }));
            }
        });
		$('#id_select_city').trigger('change');
		//Get locality based on the project.
		
		$("#project_id").on('change', function () {
            var project_id = $("#project_id").val();
			if (project_id) {
                var dataString = 'project_id=' + project_id;
				//alert('hello1');
                $.ajax({
                    type: "POST",
                    url: '<?php echo Router::url(array("controller" => "projects", "action" => "getProjectLocality")); ?>',
                    data: dataString,
                    cache: false,
					dataType:'json',
                    success: function (data) {
					//alert('hello');
						$('#searchInput').val(data.locality).trigger('click');
                    }
                }).responseJSON;
            }
        });

    });


</script>	
<script>
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                    val = $target.attr('data-value'),
                    $inp = $target.find('input'),
                    idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });

        $('.add-title-tab > .add-expand').on('click', function () {
            $(this).toggleClass('active').parent().next('.add-tab-content').slideToggle();
        });
    });



</script>
<script>
    $(document).ready(function () {

        // Parking Checkbox - Special functionality
        $("#property_feature_ajax").on("click", ".resp", function () {

            var $inputs = $('input.resp:checkbox')

            if (($(this).attr('id') == 'id_chk_none') && ($(this).is(':checked')))
            {
                //$inputs.not(this).prop('disabled', true); // <-- disable all but checked one
                $inputs.not(this).prop('checked', false); // <-- uncheck all but checked one

                $('#id_select_cvrd').css('display', 'none');
                $('#id_select_open').css('display', 'none');

            } else if (($(this).attr('id') == 'id_chk_cvrd') && ($(this).is(':checked')))
            {
                //alert("Covered Parking is Checked"); 
                // Do action as  covered parking checked

                $('#id_select_cvrd').css('display', 'inline');
            } else if (($(this).attr('id') == 'id_chk_cvrd') && (!($(this).is(':checked'))))
            {
                //alert("Covered Parking is UnChecked"); 
                // Do action as covered parking is unchecked

                $('#id_select_cvrd').css('display', 'none');
            } else if (($(this).attr('id') == 'id_chk_open') && ($(this).is(':checked')))
            {
                //alert("Open Parking is clicked");  	
                // Do action as open parking is checked

                $('#id_select_open').css('display', 'inline');

            } else if (($(this).attr('id') == 'id_chk_open') && (!($(this).is(':checked'))))
            {
                //alert("Open Parking is clicked");  	
                // Do action for open parking is unchecked

                $('#id_select_open').css('display', 'none');
            } else
            {
                $inputs.prop('disabled', false);

            }
        });


    });
</script>
