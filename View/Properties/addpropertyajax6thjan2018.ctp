			
<div class="row">    
	<div class="row">
		<?php if (in_array("34", $propertyoption)) { ?>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="label_title">Configure<span class="mand_field">*</span> </label>
					<div class="select-style">
						<?php echo $this->Form->input('Feature.Configure', array('id' => 'Configure', 'options' => array('None' => 'None', 'Shared' => 'Shared', '1RK' => '1RK', 'Studio' => 'Studio', '1BHK' => '1BHK', '2BHK' => '2BHK', '3BHK' => '3BHK', '4BHK' => '4BHK', '5BHK' => '5BHK', '6BHK' => '6BHK', 'PentHouse' => 'PentHouse'), 'empty' => 'Select', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Configure', 'label' => false)); ?>
					</div>
					<div class="error has-error" style="display:block" id="Configure_error"></div>
				</div>
			</div>
    	<?php } ?>
		<?php if (in_array("1", $propertyoption)) { ?>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">Super Built Up Area<span class="mand_field">*</span> </label>
					<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the sum of Carpet area, Wall area and Common area"></i>
					<div class="input text"><?php echo $this->Form->input('Feature.sbua', array('id' => 'sbua', 'class' => 'form-control required', 'placeholder' => '', 'label' => false, 'div' => false)); ?></div>
					<div class="error has-error" style="display:block" id="super_built_error"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">&nbsp; </label>
					<div class="select-style">
						<?php echo $this->Form->input('Feature.sbua_m', array('id' => 'sbua_m', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => 'Super Built Up Area', 'label' => false)); ?>
					</div>
				</div>
			</div>
	    <?php } ?>

		
	</div>    

    
    <div class="row">
    	<?php if (in_array("2", $propertyoption)) { ?>        
	       <div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">Built Up Area</label>
					<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the sum of Carpet area and Wall area"></i>
					<div class="input text"><?php echo $this->Form->input('Feature.build_up_area', array('id' => 'build_up_area', 'class' => 'form-control', 'placeholder' => '', 'label' => false)); ?></div>
					<div class="error has-error" style="display:block" id="build_up_area_error"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">&nbsp; </label>
					<div class="select-style">
						<?php echo $this->Form->input('Feature.build_up_sq', array('id' => 'build_up_sq', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
					</div>
				</div>
			</div>
	  	<?php } ?>

    	<?php if (in_array("3", $propertyoption)) { ?>
	       <div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">Carpet Area</label>
					<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the actual Liveable area "></i>
					<div class="input text"><?php echo $this->Form->input('Feature.carpet_area', array('id' => 'carpet_area', 'class' => 'form-control', 'placeholder' => '', 'label' => false)); ?></div>
	                <div class="error has-error" style="display:block" id="carpet_area_error"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">&nbsp; </label>
					<div class="select-style">
						<?php echo $this->Form->input('Feature.build_up_sq', array('id' => 'build_up_sq', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
    
    <div class="row">
		<?php if (in_array("20", $propertyoption)) { ?>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">Plot area<span class="mand_field">*</span> </label>
					<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the total Measured area of the land."></i>
					<div class="input text">
						<?php echo $this->Form->input('Feature.plot_area', array('id' => 'carpet_area', 'class' => 'form-control required', 'placeholder' => '', 'label' => false)); ?>
					</div>
					<div class="error has-error" style="display:block" id="plot_area_error"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label class="label_title">&nbsp; </label>
					<div class="select-style">
						<?php echo $this->Form->input('Feature.plot_area_sq', array('id' => 'plot_area_sq', 'options' => array('Sq.Ft' => 'Sq.Ft', 'Sq.Yards' => 'Sq.Yards', 'Acres' => 'Acres', 'Hactares' => 'Hactares', 'Sq. Meter' => 'Sq. Meter', 'Marla' => 'Marla', 'Cent' => 'Cent', 'Bigha 1' => 'Bigha 2', 'Bigha 1' => 'Bigha 2', 'Kottah' => 'Kottah', 'Kanal' => 'Kanal', 'Grounds' => 'Grounds', 'Ares' => 'Ares', 'Biswa1' => 'Biswa1', 'Biswa2' => 'Biswa2', 'Guntha' => 'Guntha', 'Aanakadam' => 'Aanakadam', 'Rood' => 'Rood', 'Chataks' => 'Chataks', 'Perch' => 'Perch'), 'class' => 'selectpicker', 'placeholder' => 'Super Built Up Area', 'label' => false)); ?>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="row">
		<?php if (in_array("21", $propertyoption)) { ?>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">Length of plot </label>
					<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the total Measured area of the land."></i>
					<div class="input text">
						<?php echo $this->Form->input('Feature.length_plot', array('id' => 'length_plot', 'class' => 'form-control', 'placeholder' => 'Length of plot in Sq.ft', 'label' => false)); ?>
					</div>
					<div class="error has-error" style="display:block" id="plot_area_error"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">&nbsp; </label>
					Sq.ft.					
					<div class="error has-error">Box must be filled out</div>
				</div>
			</div>
		<?php } ?>
		
		<?php if (in_array("22", $propertyoption)) { ?>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">Width of plot </label>
					<i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="It is the total Measured area of the land."></i>
					<div class="input text">
						<?php echo $this->Form->input('Feature.width_plot', array('id' => 'width_plot', 'class' => 'form-control', 'placeholder' => 'Width of plot in Sq.ft', 'label' => false)); ?>
					</div>
					<div class="error has-error" style="display:block" id="plot_area_error"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label class="label_title">&nbsp; </label>
					Sq.ft.					
					<div class="error has-error">Box must be filled out</div>
				</div>
			</div>
		<?php } ?>
		
	</div>
	
    
    <?php if (in_array("4", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Bedrooms<span class="mand_field">*</span> </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.bedrooms', array('id' => 'bedrooms', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4'), 'empty' => 'Select No of Bedrooms', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Bedrooms', 'label' => false)); ?>
				</div>
				<div class="error has-error" style="display:block" id="bedrooms_error"></div>
			</div>
		</div>
    <?php } ?>
	
    <?php if (in_array("5", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Bathrooms<span class="mand_field">*</span> </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.bathrooms', array('id' => 'bathrooms', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4'), 'empty' => 'Select No of Bathrooms', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Bathrooms', 'label' => false)); ?>
				</div>
				<div class="error has-error" style="display:block" id="bathrooms_error"></div>
			</div>
		</div>
    <?php } ?>

    <?php if (in_array("33", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Washrooms<span class="mand_field">*</span> </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.washrooms', array('id' => 'washrooms', 'options' => array('None' => 'None', 'Shared' => 'Shared', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '9+' => '9+'), 'empty' => 'Select No of Bathrooms', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Washroom', 'label' => false)); ?>
				</div>
				<div class="error has-error" style="display:block" id="washrooms_error"></div>
			</div>
		</div>
    <?php } ?>

    
	
    <?php if (in_array("6", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Balconies<span class="mand_field">*</span> </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.balconies', array('id' => 'balconies', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4'), 'empty' => 'Select No of Balconies', 'class' => 'selectpicker required', 'placeholder' => '', 'title' => 'Please Select Balconies', 'label' => false)); ?>
				</div>
				<div class="error has-error" style="display:block" id="balconies_error"></div>
			</div>
		</div>
    <?php } ?>

    <?php if (in_array("7", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title"> Age of Property </label>
				<div class="input text">
					<?php echo $this->Form->input('Feature.age_of_property', array('id' => 'age_of_property', 'class' => 'form-control', 'placeholder' => 'Age', 'label' => false)); ?>
				</div>
				<div class="error has-error" style="display:block" id="age_error"></div>
			</div>
		</div>
    <?php } ?>

    
    <?php if (in_array("9", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title"> Total floors </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.total_floors', array('id' => 'total_floors', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
				</div>
				
			</div>
		</div>
    <?php } ?>

    <?php if (in_array("10", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Property on floor </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.on_floor', array('id' => 'on_floor', 'options' => array('basement' => 'Basement', 'Lower Ground' => 'Lower Ground', 'Ground' => 'Ground', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
				</div>
				<div class="error has-error">Box must be filled out</div>
			</div>
		</div>
    <?php } ?>

    <?php if (in_array("30", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Floors in property</label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.in_floor', array('id' => 'in_floor', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
				</div>
				<div class="error has-error">Box must be filled out</div>
			</div>
		</div>
    <?php } ?>
	
    <?php if (in_array("23", $propertyoption)) {
        ?>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="label_title">Floors allowed for construction </label>
				<div class="select-style">
					<?php echo $this->Form->input('Feature.allowed_floor', array('id' => 'allowed_floor', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '40+' => '40+'), 'empty' => 'Select', 'class' => 'selectpicker', 'placeholder' => '', 'label' => false)); ?>
				</div>
				<div class="error has-error">Box must be filled out</div>
			</div>
		</div>
    <?php } ?>

    <?php if (in_array("11", $propertyoption)) {
        ?>
        <div class="form-group col-md-12">
            <div class="col-md-12"><label class="label_title">Reserved Parking</label></div>

            <div class="col-md-12">

                <?php echo $this->Form->input('Feature.res_parking_1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_none', 'hiddenField' => false)); ?>

                <label for="id_chk_none" class="checkbox-custom-label" style="padding-right:160px;">None</label>

                <?php echo $this->Form->input('Feature.res_parking_2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_cvrd', 'hiddenField' => false)); ?>
                <label for="id_chk_cvrd" class="checkbox-custom-label" style="padding-right:160px;" >Covered</label>


                <?php echo $this->Form->input('Feature.res_parking_3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom resp', 'id' => 'id_chk_open', 'hiddenField' => false)); ?>
                <label for="id_chk_open" class="checkbox-custom-label" style="padding-right:160px;" >Open</label>

            </div>



        </div>

        <div class="form-group col-md-6" id="id_select_cvrd" style="display:none;">

            <div class="col-md-12"><label class="label_title">No. of covered parking </label><span class="mand_field">*</span>
                <div class="select-style">
                    <?php echo $this->Form->input('Feature.no_covered', array('id' => 'no_covered', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20'), 'empty' => 'Select', 'class' => 'selectpicker required', 'placeholder' => '', 'label' => false)); ?>
                </div>
                <div class="error has-error" id="covered_parking_error" style="display:block"></div>
            </div>

        </div>

        <div class="form-group col-md-6" id="id_select_open" style="display:none;" >

            <div class="col-md-12"><label class="label_title">No. of open parking</label><span class="mand_field">*</span>
                <div class="select-style">
                    <?php echo $this->Form->input('Feature.no_open', array('id' => 'no_open', 'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20'), 'empty' => 'Select', 'class' => 'selectpicker required', 'placeholder' => '', 'label' => false)); ?>

                </div>
                <div class="error has-error" id="open_parking_error" style="display:block"></div>
            </div>

        </div>



    <?php } ?>
    <?php if (in_array("12", $propertyoption)) {
        ?>
        <div class="form-group col-md-12">
            <div class="col-md-12">
                <label class="label_title">Other rooms </label ></div>
            <div class="col-md-3">
                <?php echo $this->Form->input('Feature.other_room1', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room1')); ?>

                <label for="other_room1" class="checkbox-custom-label">Pooja</label>
            </div>
            <div class="col-md-3">
                <?php echo $this->Form->input('Feature.other_room2', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room2')); ?>
                <label for="other_room2" class="checkbox-custom-label">Study</label>

            </div>
            <div class="col-md-3">
                <?php echo $this->Form->input('Feature.other_room3', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room3')); ?>
                <label for="other_room3" class="checkbox-custom-label">Servant</label>

            </div>
            <div class="col-md-3">
                <?php echo $this->Form->input('Feature.other_room4', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'other_room4')); ?>
                <label for="other_room4" class="checkbox-custom-label">Other</label>

            </div>
        </div> 
    <?php } ?>
    <div class="col-md-8">
        <div class="form-group">
            <label for="countryState" class="label_title">Description<span class="mand_field">*</span></label>
            <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Describe your property, Highlight USP of your property, Nearby Landmarks, Market area , Office area etc"></i>
            <?php echo $this->Form->input('description', array('type' => 'textarea', 'id' => 'countryState', 'placeholder' => 'Add property description', 'class' => 'form-control required', 'label' => false, 'div' => false, 'rows' => '1')); ?>

            <div class="error has-error" id="property_description_error" style="display:block"></div>
        </div>
    </div>

</div>
<?php 
echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
<div class="account-block text-center submit_area">
    <button type="submit" class="btn btn-primary btn-next" id="next2">NEXT</button>
</div>
</div>