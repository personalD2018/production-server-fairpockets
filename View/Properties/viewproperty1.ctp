<?php
echo $this->Html->script('front/jssor.slider-22.2.10.min');

$features = json_decode($propertydetails['Property']['features'], true);
$amenities = json_decode($propertydetails['Property']['amenities'], true);
?>
<script>
    jQuery(document).ready(function () {
        function nFormatter(num)
        {
            num = Math.abs(num)
            if (num >= 10000000)
            {
                formattedNumber = (num / 10000000).toFixed(2).replace(/\.0$/, '') + ' Crore';
            } else if (num >= 100000)
            {
                formattedNumber = (num / 100000).toFixed(2).replace(/\.0$/, '') + ' Lakh';
            } else if (num >= 1000)
            {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + ' Thousand';
            } else
            {
                formattedNumber = num;
            }

            return formattedNumber;
        }

<?php
if ($propertydetails['Property']['transaction_type_id'] == '1') {
    ?>
            format = nFormatter('<?php echo $propertydetails['Property']['offer_price']; ?>');
            document.getElementById("offerpriceformater").innerHTML = format;
            mformat = nFormatter('<?php echo $propertydetails['Property']['market_price']; ?>');
            document.getElementById("marketpriceformater").innerHTML = mformat;
    <?php
} else {
    ?>
            rformat = nFormatter('<?php echo $propertydetails['Property']['market_rent']; ?>');

            document.getElementById("marketrentformater").innerHTML = rformat;

            eformat = nFormatter('<?php echo $propertydetails['Property']['expected_monthly']; ?>');

            document.getElementById("expectedmonthlyformat").innerHTML = eformat;
<?php } ?>

    });

</script>		 

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#formID").validate()
    });
</script>				
<style>
    .detail_header .h_feature 
    {
        font-size: 20px;
        color: #29333d;
    }
    .green{color:green; font-weight:bold;border:1px solid; padding:7px;}
    .error{color:red;}
</style>
<style>
    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    .jssora05l.jssora05lds      (disabled)
    .jssora05r.jssora05rds      (disabled)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('img/a17.png') no-repeat;
        overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }
    .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
    .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
</style>
<div class="page-content">
    
            <div class="detail_header nopaddingside">
                <div class="col-sm-12 breadCrumbs ">
                    <div class="crumb"><a href="">Home</a> > <a href="">Property in Noida</a> > Sector-107 Noida </div>
                </div>

                <!--   <div class="col-sm-1">
                         <div class="circle">20%</div>
                  </div> -->


                <div class="col-sm-4 header-price">
                    <?php
                    if ($propertydetails['Property']['transaction_type_id'] == '1') {
                        ?>  
                        <div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i> <span id="offerpriceformater"></span><span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo round(($propertydetails['Property']['market_price'] - $propertydetails['Property']['offer_price']) / $propertydetails['Property']['market_price'] * 100, 0); ?>% less</span><span class="price_sq"><br/>@<?php
                                if (!empty($features['superbuilt_area_insq'])) {
                                    $insq = $features['superbuilt_area_insq'];
                                } else {
                                    $insq = $features['plot_area_insq'];
                                }
                                echo round($propertydetails['Property']['offer_price'] / $insq, 3);
                                ?> per Sq.Ft</span></div>
                        <div class="market_price"><span>Market Price:</span><i class="fa fa-inr" aria-hidden="true"></i><span id="marketpriceformater"></span> @<?php
                            if (!empty($features['superbuilt_area_insq'])) {
                                $insq = $features['superbuilt_area_insq'];
                            } else {
                                $insq = $features['plot_area_insq'];
                            }
                            echo round($propertydetails['Property']['market_price'] / $insq, 3);
                            ?> per Sq.Ft</div>
                            <?php
                        } else {
                            ?>  
                        <div class="offer_price"><i class="fa fa-inr" aria-hidden="true"></i> <span id="expectedmonthlyformat"></span><span class="prooffer"><i class="fa fa-arrow-down" aria-hidden="true"></i><?php echo round(($propertydetails['Property']['market_rent'] - $propertydetails['Property']['expected_monthly']) / $propertydetails['Property']['market_rent'] * 100, 0); ?>% less</span></div>
                        <div class="market_price"><span>Market Rent:</span><i class="fa fa-inr" aria-hidden="true"></i><span id="marketrentformater"></span>
                        </div>
                        <?php
                    }
                    ?> 
                </div>
                <div class="col-sm-5 text-center header-feature">

                    <div class="h_feature"><?php
                        if (!empty($features['Configure']) && $propertyoption['PropertyTypeOptions']['transaction_type_id'] == '1') {
                            echo $features['Configure'];
                        }
                        ?> <span class="h_feature_sub"><?php
                        if ($propertyoption['PropertyTypeOptions']['transaction_type_id'] == '1') {
                            echo "Residential";
                        } else {
                            echo "Commercial";
                        }
                        ?> <?php echo $propertyoption['PropertyTypeOptions']['option_name'] ?>  for <?php
                            if ($propertydetails['Property']['transaction_type_id'] == '1') {
                                echo "Sale";
                            } else {
                                echo "Rent";
                            }
                            ?></span></div>
                    <div class="h_location"><?php echo $propertydetails['Property']['locality']; ?>,<?php echo $propertydetails['Property']['city_data']; ?>,<?php echo $propertydetails['Property']['state_data']; ?></div>	
                </div>
                <div class="col-sm-3 text-center header-other-feature">

                    <div class="h_postdate">Post on <?php echo $newDate = date("d M Y", strtotime($propertydetails['Property']['created_date'])); ?></div>
                    <div class="h_availability"><?php
                        if ($propertydetails['Property']['construction_status'] == '1') {
                            echo "Ready to move in";
                        } else {
                            echo "Under Construction";
                        };
                        ?></div>	
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="no-padding-bottom">

            <div class=" property_detail_area col-sm-12 nopaddingside">

                <div class="col-sm-12 nopaddingside property_detail_inner">
                    <div class="col-sm-9  property-images-main">
                        <div class="row">


                            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:456px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                                <!-- Loading Screen -->
                                <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                    <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                </div>
                                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:356px;overflow:hidden;">
                                    <a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery</a>
                                    <?php
                                    foreach ($propertyImages as $property_value) {
                                        ?>
                                        <div>
                                            <img data-u="image" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                            <img data-u="thumb" src="<?php echo $this->base; ?>/<?php echo $property_value; ?>" />
                                        </div>
                                    <?php } ?>

                                </div>
                                <!-- Thumbnail Navigator -->
                                <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                                    <!-- Thumbnail Item Skin Begin -->
                                    <div data-u="slides" style="cursor: default;">
                                        <div data-u="prototype" class="p">
                                            <div class="w">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                            </div>
                                            <div class="c"></div>
                                        </div>
                                    </div>
                                    <!-- Thumbnail Item Skin End -->
                                </div>
                                <!-- Arrow Navigator -->
                                <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                                <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                            </div>




                        </div>
                        <!-- basic detail -->
                        <div id="detail" class="detail-list detail-block target-block">
                            <div class="detail-title">
                                <h2 class="title-left">Detail</h2>



                            </div>
                            <div class="">
                                <ul class="list-three-col">
                                    <?php
                                    if (!empty($features['sbua'])) {
                                        ?>
                                        <li><strong>Super Built Up Area:</strong> <?php echo $features['sbua']; ?> <?php echo $features['sbua_m']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['build_up_area'])) {
                                        ?>
                                        <li><strong>Built Up Area:</strong> <?php echo $features['build_up_area']; ?> <?php echo $features['build_up_sq']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['carpet_area'])) {
                                        ?>
                                        <li><strong>Carpet Area:</strong> <?php echo $features['carpet_area']; ?> <?php echo $features['carpet_area_sq']; ?></li>
                                    <?php } ?>

                                    <?php if (!empty($features['plot_area'])) {
                                        ?>
                                        <li><strong>Plot Area:</strong> <?php echo $features['plot_area']; ?> <?php echo $features['plot_area_sq']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['length_plot'])) {
                                        ?>
                                        <li><strong>Length of plot:</strong> <?php echo $features['length_plot']; ?> Sq.ft</li>
                                    <?php } ?>
                                    <?php if (!empty($features['length_plot'])) {
                                        ?>
                                        <li><strong>Width of plot:</strong> <?php echo $features['length_plot']; ?> Sq.ft</li>
                                    <?php } ?>

                                    <?php if (!empty($features['bedrooms'])) {
                                        ?>
                                        <li><strong>Bedrooms:</strong> <?php echo $features['bedrooms']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['bathrooms'])) {
                                        ?>
                                        <li><strong>Bathrooms:</strong> <?php echo $features['bathrooms']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['washrooms'])) {
                                        ?>
                                        <li><strong>Washrooms:</strong> <?php echo $features['washrooms']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['balconies'])) {
                                        ?>
                                        <li><strong>Balconies:</strong> <?php echo $features['balconies']; ?></li>
                                    <?php } ?>


                                </ul>
                            </div>
                            <div class="clear"></div>

                            <div class="detail-title-inner">
                                <h4 class="title-inner">Additional details</h4>
                            </div>
                            <ul class="list-three-col list_odetail">
                                <?php if (!empty($project['Project']['project_name'])) {
                                    ?>
                                    <li><strong>Project name:</strong> <span><?php echo $project['Project']['project_name']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($project['Project']['dtp1'])) {
                                    ?>
                                    <li><strong>Launch date:</strong> <span><?php echo $project['Project']['dtp1']; ?></span></li>
                                <?php } ?>

                                <?php
                                if ($propertydetails['Property']['transaction_type_id'] == '1') {

                                    if (!empty($propertydetails['Property']['offer_price'])) {
                                        ?>
                                        <li><strong>Offer Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['offer_price']; ?></span></li>
                                    <?php } ?>

                                    <?php if (!empty($propertydetails['Property']['market_price'])) {
                                        ?>
                                        <li><strong>Market Price:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_price']; ?></span></li>
                                    <?php } ?>
                                    <?php if (!empty($propertydetails['Property']['booking_amount'])) {
                                        ?>
                                        <li><strong>Booking Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['booking_amount']; ?></span></li>
                                    <?php } ?>

                                    <?php
                                } else {
                                    ?>
                                    <?php if (!empty($propertydetails['Property']['market_rent'])) {
                                        ?>
                                        <li><strong>Market Rent:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['market_rent']; ?></span></li>
                                    <?php } ?>

                                    <?php if (!empty($propertydetails['Property']['expected_monthly'])) {
                                        ?>
                                        <li><strong>Expected Monthly Rent:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $propertydetails['Property']['expected_monthly']; ?></span></li>
                                    <?php } ?>

                                    <?php if (!empty($propertydetails['Property']['deposit'])) {
                                        ?>
                                        <li><strong>Deposit Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['deposit']; ?></span></li>
                                    <?php } ?>
                                <?php } ?>


                                <?php if (!empty($propertydetails['Property']['maintance_amount'])) {
                                    ?>
                                    <li><strong>Maintance Amount:</strong> <span><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?php echo $propertydetails['Property']['maintance_amount']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($project['Project']['dtp2'])) {
                                    ?>
                                    <li><strong>Possession Date:</strong> <span><?php echo $project['Project']['dtp2']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['ownership_type'])) {
                                    ?>
                                    <li><strong>Ownership Type:</strong> <span><?php
                                            if ($propertydetails['Property']['ownership_type'] == '1') {
                                                echo "FreeHold";
                                            }if ($propertydetails['Property']['ownership_type'] == '2') {
                                                echo "Lease Hold";
                                            }if ($propertydetails['Property']['ownership_type'] == '3') {
                                                echo "Power of Attorney";
                                            }if ($propertydetails['Property']['ownership_type'] == '4') {
                                                echo "Co-operative Society";
                                            }
                                            ?></span></li>
                                <?php } ?>
                                <?php if (!empty($propertydetails['Property']['sale_type'])) {
                                    ?>
                                    <li><strong>Sale Type:</strong> <span><?php
                                            if ($propertydetails['Property']['sale_type'] == '1') {
                                                echo "Resale";
                                            } else {
                                                echo "New Booking";
                                            }
                                            ?></span></li>
                                <?php } ?>

                                <?php
                                if (!empty($features['on_floor'])) {
                                    ?>
                                    <li><strong>Property on Floor :</strong> <span><?php echo $features['on_floor']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['in_floor'])) {
                                    ?>
                                    <li><strong>Floors in property:</strong> <span><?php echo $features['in_floor']; ?></span></li>
                                <?php } ?>

                                <?php if (!empty($features['address'])) {
                                    ?>
                                    <li><strong>Address:</strong> <span><?php echo $propertydetails['Property']['address']; ?>,<?php echo $locality[1]; ?>,<?php echo $city[1]; ?>,<?php echo $state[1]; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['age_of_property'])) {
                                    ?>
                                    <li><strong>Age of property:</strong> <span><?php echo $features['age_of_property']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['furnishing'])) {
                                    ?>
                                    <li><strong>Furnishing:</strong> <span><?php echo $features['furnishing']; ?></span></li>
                                <?php } ?>
                                <?php if (!empty($features['allowed_floor'])) {
                                    ?>
                                    <li><strong>Floor Allowed For Construction:</strong> <span><?php echo $features['allowed_floor']; ?></span></li>
                                <?php } ?>
                                <?php
                                if (!empty($features['res_parking_1'])) {
                                    ?>
                                    <li><strong>Reserved Parking:</strong> <span>None</span></li>
                                    <?php
                                } else {
                                    ?>
                                    <?php if (!empty($features['res_parking_2'])) {
                                        ?>
                                        <li><strong>No of Covered Parking:</strong> <span><?php echo $features['no_covered']; ?></span></li>
                                    <?php } ?>
                                    <?php if (!empty($features['res_parking_3'])) {
                                        ?>
                                        <li><strong>No of Open Parking:</strong> <span><?php echo $features['no_open']; ?></span></li>
                                        <?php
                                    }
                                }
                                ?>
                                <?php
                                $otherroom = '';
                                if (!empty($features['other_room1']) && $features['other_room1'] == '1') {
                                    $otherroom .= ' Pooja,';
                                }
                                if (!empty($features['other_room2']) && $features['other_room2'] == '1') {
                                    $otherroom .= ' Study,';
                                }
                                if (!empty($features['other_room3']) && $features['other_room3'] == '1') {
                                    $otherroom .= ' Servant,';
                                }
                                if (!empty($features['other_room4']) && $features['other_room4'] == '1') {
                                    $otherroom .= ' Other,';
                                }
                                ?>
                                <?php
                                if (!empty($otherroom)) {
                                    ?>
                                    <li><strong>Other Rooms:</strong> <span><?php echo rtrim($otherroom, ','); ?></span></li>
                                <?php } ?>








                            </ul>
                        </div>
                        <!-- basic detail end -->
                        <div id="features" class="detail-features detail-block target-block" style="min-width:825px;">
                            <div class="detail-title">
                                <h2 class="title-left">Description</h2>
                            </div>


                            <div class="property_des"><?php echo $features['property_description']; ?></div>



                        </div>
                        <div class="clear"></div>

                        <?php if (!empty($project['Project']['project_name'])) { ?>
                            <div id="detail" class="detail-list detail-block target-block">
                                <div class="detail-title">
                                    <h2 class="title-left">Project Detail</h2>
                                </div>
                                <div class="builder-view">
                                    <ul class="list-two-col">

                                        <li><strong>Project Name: </strong><?php echo $project['Project']['project_name']; ?></li>
                                        <li><strong>Land Area: </strong><?php echo $projectdetail['ProjectDetails']['land_area']; ?>
                                            Acres</li>
                                        <li><strong>Highlights: </strong><?php echo $project['Project']['project_highlights']; ?></li>
                                        <li><strong>Project Description:</strong><?php echo $project['Project']['project_description']; ?></li>

                                    </ul>
                                </div>
                                <div class="clear"></div>		
                            <?php } ?>				




                            <!-- feature -->
                            <div id="features" class="detail-features detail-block target-block" style="min-width:825px;">
                                <div class="detail-title">
                                    <h2 class="title-left">Features</h2>
                                </div>
                                <ul class="list-three-col list-features">
                                    <?php if (!empty($features['type_of_flooring'])) {
                                        ?>
                                        <li><strong>Type of Flooring:</strong> <?php echo $features['type_of_flooring']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['power_backup'])) {
                                        ?>
                                        <li><strong>Power Backup:</strong> <?php echo $features['power_backup']; ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['watersource1']) || !empty($features['watersource2'])) {
                                        ?>
                                        <li><strong>Water Source:</strong> <?php
                                            $source = '';
                                            if (!empty($features['watersource1'])) {
                                                $source .= "Municipal,";
                                            } if (!empty($features['watersource2'])) {
                                                $source .= " Borewell Tank ";
                                            }
                                            ?><?php echo rtrim($source, ','); ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['facing'])) {
                                        ?>
                                        <li><strong>Facing:</strong> <?php echo $features['facing']; ?></li>
                                    <?php } ?>

                                    <?php if (!empty($features['width_of_fac_road'])) {
                                        ?>
                                        <li><strong>Width of facing road:</strong> <?php echo $features['width_of_fac_road']; ?> <?php echo $features['width_of_sq']; ?></li>
                                    <?php } ?>
                                    <?php
                                    if (!empty($features['overlook1']) || !empty($features['overlook2']) || !empty($features['overlook3']) || !empty($features['overlook4'])) {
                                        $overlook = '';

                                        if (!empty($features['overlook1'])) {
                                            $overlook .= "Park/Garden, ";
                                        } if (!empty($features['overlook2'])) {
                                            $overlook .= "Main Road, ";
                                        }if (!empty($features['overlook3'])) {
                                            $overlook .= "Club, ";
                                        }if (!empty($features['overlook4'])) {
                                            $overlook .= "Pool, ";
                                        }
                                        ?>
                                        <li><strong>Overlooking:</strong> <?php echo rtrim($overlook, ','); ?></li>
                                    <?php } ?>
                                    <?php if (!empty($features['bachelor_all']) && $features['bachelor_all'] == '1') {
                                        ?>
                                        <li><strong>Bachelors Allowed:</strong>Yes</li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><strong>Bachelors Allowed:</strong>No</li>
                                    <?php }
                                    ?>
                                    <?php if (!empty($features['pet_all']) && $features['pet_all'] == '1') {
                                        ?>
                                        <li><strong>Pet allowed:</strong>Yes</li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><strong>Pet allowed:</strong>No</li>
                                    <?php }
                                    ?>
                                    <?php if (!empty($features['non_veg']) && $features['non_veg'] == '1') {
                                        ?>
                                        <li><strong>Non vegetarian:</strong>Yes</li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><strong>Non vegetarian:</strong>No</li>
                                    <?php }
                                    ?>
                                    <?php if (!empty($features['boundary_wall'])) {
                                        ?>
                                        <li><strong>Boundary wall:</strong>Yes</li>
                                    <?php } ?>
                                    <?php if (!empty($features['society_type'])) {
                                        ?>
                                        <li><strong>Society Type:</strong> <span><?php echo $features['society_type']; ?></span></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <!-- feature end -->
                            <!-- Amenities -->
                            <div id="amenities" class="detail-amenities detail-block target-block col-md-12">
                                <div class="detail-title">
                                    <h2 class="title-left">Amenities</h2>
                                </div>
                                <ul class=" list-amenities">


                                    <div class="row">
                                        <div class ="col-md-3">
                                            <?php
                                            $count = 0;

                                            foreach ($amenities as $key => $amevalu) {
                                                ?>

                                                <!--<div class= "customDiv">column1</div>-->
                                                <?php
                                                if ($key == 'vaastu' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_vaastu">Feng shui/Vaastu compliant</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>

                                                <?php
                                                if ($key == 'lift' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_lift">Lift</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'park' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_park">Park</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'staff' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_staff">Maintenance Staff</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'parking' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_parking">Visitor Parking</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'waterstorage' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_water_storage">Water Storage</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>



                                                <!-- <div class= "customDiv">column2</div>-->

                                                <?php
                                                if ($key == 'intercomm' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_intercomm">Intercomm Facility</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>

                                                <?php
                                                if ($key == 'waste_disposal' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_waste">Waste Disposal</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'tarrace' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_tarrace">Private Garden/Terrace</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'security_personal' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_securityp">Security Personnel</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'goods_lift' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_lift">Service/Goods Lift</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>


                                                <!-- <div class= "customDiv">column3</div>-->
                                                <?php
                                                if ($key == 'conroom' && $amevalu == '1') {
                                                    ?>   
                                                    <li><span class="icon_amen icon_conroom">Conference Room</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'swimming' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_pool">Swimming Pool</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'atm' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_atm">ATM</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'ac' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_airc">Centrally Airconditioned</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'internet' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_internet">High Speed Internet</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'food' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_foodc">Cafeteria/Food court</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>


                                                <!-- <div class= "customDiv">column4</div>-->

                                                <?php
                                                if ($key == 'gym' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_gym">Gymnasium</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'water_softening_plant' && $amevalu == '1') {
                                                    ?>	
                                                    <li><span class="icon_amen icon_water_plant">Water Softening Plant</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'community_hall' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_com_hall">Community Hall</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'shopping_center' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_shopping">Shopping Center</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                                <?php
                                                if ($key == 'rainwater' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_rainwater">Rain water harvesting</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>

                                                <?php
                                                if ($key == 'fire_alarm' && $amevalu == '1') {
                                                    ?>
                                                    <li><span class="icon_amen icon_security">Security/Fire alarm</span></li>
                                                    <?php
                                                    echo "</div>";
                                                    echo '<div class ="col-md-3">';
                                                }
                                                ?>
                                            <?php } ?>

                                        </div>
                                    </div>

                                </ul>

                            </div>
                            <!-- Amenities end -->
                            <div id="" class="detail-map detail-block target-block col-md-12">
                                <div class="detail-title">
                                    <h2 class="title-left">Location</h2>
                                </div>
                                <div class="inner-block">

                                    <div id="map_canvas" style="height:500px;border:0" ></div>
                                </div>
                            </div>	
                        </div>
                        <!-- sidebar -->
                        <?php if ($propertydetails['Property']['user_id'] != $this->Session->read('Auth.Websiteuser.id')) { ?>
                            <div id="sidebar" class="col-sm-3 nopaddingside detail_sidebar">
                                <div class="col-sm-12  sidebar-inner">
                                    <?php echo $this->Form->create('PropertyInquirie', array("type" => "file", 'id' => "formID", 'class' => 'fpform')); ?>
                                    <?php echo $this->Session->flash(); ?> 
                                    <div class="row">
                                        <h3>Contact - Fairpockets</h3>

                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">

                                                <?php echo $this->Form->input('user_type', array('type' => 'radio', 'options' => array('Individual' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false, 'checked' => 'checked')); ?>
                                                <label for="PropertyInquirieUserTypeIndividual" class="radio-custom-label">Individual</label>

                                                <?php echo $this->Form->input('user_type', array('type' => 'radio', 'options' => array('Dealer' => false), 'div' => false, 'label' => false, 'class' => 'radio-custom', 'hiddenField' => false)); ?>
                                                <label for="PropertyInquirieUserTypeDealer" class="radio-custom-label">Dealer</label>


                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <?php echo $this->Form->input('name', array('id' => 'name', 'class' => 'form-control  required', 'placeholder' => 'Your Name', 'title' => 'Please Insert Your Name', 'label' => false)); ?>


                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <?php echo $this->Form->input('phone', array('id' => 'phone', 'class' => 'form-control  required number', 'placeholder' => 'Phone', 'title' => 'Please Insert Phone Number', 'label' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <?php echo $this->Form->input('email', array('id' => 'email', 'class' => 'form-control  required', 'placeholder' => 'Email', 'title' => 'Please Insert Email', 'label' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12">
                                            <label class="label_title">Interested in (Optional)</label>
                                            <div class="col-sm-12">
                                                <?php echo $this->Form->input('Assisted', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'interest1', 'hiddenField' => false)); ?>

                                                <label for="interest1" class="checkbox-custom-label">Assisted Search</label>
                                            </div>

                                            <div class="col-sm-12">
                                                <?php echo $this->Form->input('Immediate', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'interest2', 'hiddenField' => false)); ?>
                                                <label for="interest2" class="checkbox-custom-label">Immediate Purchase</label>

                                            </div>
                                            <div class="col-sm-12">
                                                <?php echo $this->Form->input(' 	Portfolio', array('div' => false, 'label' => false, 'type' => 'checkbox', 'class' => 'checkbox-custom', 'id' => 'interest3', 'hiddenField' => false)); ?>
                                                <label for="interest3" class="checkbox-custom-label">Portfolio Management</label>

                                            </div>

                                        </div>




                                    </div>
                                    <div class="text-center col-sm-12 "><button class="btn btn-primary">Submit</button></div>

                                    <div class="text-center col-sm-12 " style="padding:10px 0px;">I agree with Fairpockets <a href="#">T&C</a> </div>
                                    <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => $propertydetails['Property']['id'])); ?>
                                    </form>

                                </div>
                            </div>
                            <!-- sidebar end -->
                        <?php } ?>
                    </div>
                </div>	
                <!-- similar property  -->


                <div class="">
                    <?php if ($propertydetails['Property']['user_id'] != $this->Session->read('Auth.Websiteuser.id')) { ?>
                        <div class="property-listing grid-view  col-md-12">
                            <div class="detail-title">

                                <h2 class="title-left">Similar properties</h2>
                            </div>
                            <div class="row">
                                <div class="item-wrap">
                                    <div class="property-item table-list">
                                        <div class="table-cell">
                                            <div class="figure-block">
                                                <figure class="item-thumb">


                                                    <div class="price hide-on-list">

                                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                                    </div>
                                                    <a href="#" class="hover-effect">
                                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                                    </a>

                                                </figure>
                                            </div>
                                        </div>
                                        <div class="item-body table-cell">

                                            <div class="body-left table-cell">
                                                <div class="info-row">
                                                    <div class="label-wrap hide-on-grid">
                                                        <div class="label-status label label-default">For Sale</div>
                                                        <span class="label label-danger">Sold</span>
                                                    </div>
                                                    <h2 class="property-title"><a href="#"><?php
                                                            if (!empty($features['bedrooms'])) {
                                                                echo $features['bedrooms'];
                                                            }
                                                            ?> Bedroom 4 Baths</a></h2>
                                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                                </div>


                                            </div>
                                            <div class="body-right table-cell hidden-gird-cell">
                                                <div class="info-row price">
                                                    <p class="price-start">Start from</p>
                                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                                </div>
                                                <div class="info-row phone text-right">
                                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                                </div>
                                            </div>
                                            <div class="table-list full-width hide-on-list">
                                                <div class="cell">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-foot date hide-on-list">
                                        <div class="item-foot-left">
                                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                                        </div>
                                        <div class="item-foot-right">
                                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-wrap">
                                    <div class="property-item table-list">
                                        <div class="table-cell">
                                            <div class="figure-block">
                                                <figure class="item-thumb">


                                                    <div class="price hide-on-list">

                                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                                    </div>
                                                    <a href="#" class="hover-effect">
                                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                                    </a>

                                                </figure>
                                            </div>
                                        </div>
                                        <div class="item-body table-cell">

                                            <div class="body-left table-cell">
                                                <div class="info-row">
                                                    <div class="label-wrap hide-on-grid">
                                                        <div class="label-status label label-default">For Sale</div>
                                                        <span class="label label-danger">Sold</span>
                                                    </div>
                                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                                </div>


                                            </div>
                                            <div class="body-right table-cell hidden-gird-cell">
                                                <div class="info-row price">
                                                    <p class="price-start">Start from</p>
                                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                                </div>
                                                <div class="info-row phone text-right">
                                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                                </div>
                                            </div>
                                            <div class="table-list full-width hide-on-list">
                                                <div class="cell">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-foot date hide-on-list">
                                        <div class="item-foot-left">
                                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                                        </div>
                                        <div class="item-foot-right">
                                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-wrap">
                                    <div class="property-item table-list">
                                        <div class="table-cell">
                                            <div class="figure-block">
                                                <figure class="item-thumb">


                                                    <div class="price hide-on-list">

                                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                                    </div>
                                                    <a href="#" class="hover-effect">
                                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                                    </a>

                                                </figure>
                                            </div>
                                        </div>
                                        <div class="item-body table-cell">

                                            <div class="body-left table-cell">
                                                <div class="info-row">
                                                    <div class="label-wrap hide-on-grid">
                                                        <div class="label-status label label-default">For Sale</div>
                                                        <span class="label label-danger">Sold</span>
                                                    </div>
                                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                                </div>


                                            </div>
                                            <div class="body-right table-cell hidden-gird-cell">
                                                <div class="info-row price">
                                                    <p class="price-start">Start from</p>
                                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                                </div>
                                                <div class="info-row phone text-right">
                                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                                </div>
                                            </div>
                                            <div class="table-list full-width hide-on-list">
                                                <div class="cell">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-foot date hide-on-list">
                                        <div class="item-foot-left">
                                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                                        </div>
                                        <div class="item-foot-right">
                                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-wrap">
                                    <div class="property-item table-list">
                                        <div class="table-cell">
                                            <div class="figure-block">
                                                <figure class="item-thumb">


                                                    <div class="price hide-on-list">

                                                        <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                                        <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
                                                    </div>
                                                    <a href="#" class="hover-effect">
                                                        <img src="assets/images/property/01_434x290.jpg" alt="thumb">
                                                    </a>

                                                </figure>
                                            </div>
                                        </div>
                                        <div class="item-body table-cell">

                                            <div class="body-left table-cell">
                                                <div class="info-row">
                                                    <div class="label-wrap hide-on-grid">
                                                        <div class="label-status label label-default">For Sale</div>
                                                        <span class="label label-danger">Sold</span>
                                                    </div>
                                                    <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                                    <h4 class="property-location">Sunworld Vanalika, Sector-107 Noida</h4>
                                                    <div class="pro_tt">Residential Apartment for Sale</div>

                                                    <div class="pro_area">1000 Sq.ft. Super Built Up Area  </div>
                                                </div>


                                            </div>
                                            <div class="body-right table-cell hidden-gird-cell">
                                                <div class="info-row price">
                                                    <p class="price-start">Start from</p>
                                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                                    <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
                                                </div>
                                                <div class="info-row phone text-right">
                                                    <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                                    <p><a href="#">+1 (786) 225-0199</a></p>
                                                </div>
                                            </div>
                                            <div class="table-list full-width hide-on-list">
                                                <div class="cell">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-foot date hide-on-list">
                                        <div class="item-foot-left">
                                            <p><i class="fa fa-user"></i> <a href="#">Fairpockets</a></p>
                                        </div>
                                        <div class="item-foot-right">
                                            <p><i class="fa fa-calendar"></i> 12 Days ago </p>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- similar property end -->		
            </div>



        </div>
        <!--/.container-->

</div>


<script type="text/javascript">
    jssor_1_slider_init = function () {

        var jssor_1_SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*you can remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>
<script type="text/javascript">jssor_1_slider_init();</script>
<?php
$gLng = $propertydetails['Property']['lng'];
$gLat = $propertydetails['Property']['lat'];
// Generate client side GMap part
$gTitle = $propertydetails['Property']['locality'] . ', ' . $propertydetails['Property']['city_data'];
echo "
		<input type='hidden' id='id_gLat' value='" . $gLat . "'>
		<input type='hidden' id='id_gLng' value='" . $gLng . "'>
		<input type='hidden' id='id_gTitle' value='" . $gTitle . "'>
	";
?>	

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4t-W6l4Uxzeb1YrzgEsapBaHeZnvhCmE&libraries=places"></script>
<script type="text/javascript">

    var map;
    var lat = parseFloat(document.getElementById("id_gLat").value);
    var lng = parseFloat(document.getElementById("id_gLng").value);
    var tmp = document.getElementById("id_gTitle").value;

    var infowindow = new google.maps.InfoWindow({});

    function initialize()
    {
        geocoder = new google.maps.Geocoder();

        var latlng = new google.maps.LatLng(lat, lng);

        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Property Location"
        });

        marker['infowindow'] = tmp;

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
        });

    }

    window.onload = initialize;

</script>
