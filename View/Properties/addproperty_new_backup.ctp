
<!-- css style to disable button -->

<style>
    .btn-primary.disabled, .btn-primary[disabled] {
        background: #ffc000;
        color: #333;
        outline: none;
        box-shadow: none;
    }
    .add-tab-content .add-tab-row {
        border-bottom: none;
    }
</style> 
<!--./advertisement-Card-->
<section class="services_details no-margin">
    <div class="container">
        <div class="center wow fadeInDown no-padding-bottom">
            <h2>Post Your Properties</h2>
            <p class="lead">We've over 15 Lac buyers for you!
                It's Easy...It's Simple.. 
                <br> & Yes It's FREE*
            </p>
        </div>

        <div class="add_property_form">
            <form class="fpform" id="idForm">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Basic Information</h3>
                        <div class="add-expand active"></div>
                    </div>
                    <div class="add-tab-content" style="display:block;">
                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryState" class="label_title">List Property for</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('transaction_type', array('id' => 'transaction_type', 'options' => array('1' => 'Sell', '2' => 'Rent'), 'class' => 'selectpicker', 'placeholder' => 'Select Property for', 'label' => false)); ?>

                                        </div>		
                                        <div class="error has-error" id="transaction_type_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property_type" class="label_title">Property type</label>
                                        <div class="select-style ">
                                            <?php echo $this->Form->input('property_type_op', array('id' => 'property-type', 'options' => array('1' => 'Residential', '2' => 'Commercial'), 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" >Box must be filled out</div>



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Property Type Option<span class="mand_field">*</span></label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('property_type', array('id' => 'property_type_val', 'options' => array('1' => 'Apartment', '2' => 'Studio Apartment', '3' => 'Residential Land', '4' => 'Independent Builder', '5' => 'Independent House', '6' => 'Independent Villa', '7' => 'Farm House'), 'empty' => 'Select', 'title' => 'Please Select Property Type Option', 'class' => 'selectpicker bs-select-hidden required', 'placeholder' => 'Select Property Type Option', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="property_type_error" style="display:block;"></div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="add-tab-row  push-padding-bottom">
                            <?php /* ?>
                              <div class="row">


                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="city" class="label_title">Country<span class="mand_field">*</span></label>

                              <div class="select-style">
                              <?php echo $this->Form->input('country', array('id' =>'country','options' => array('1' => 'India'),'empty' => 'Select Country','class' =>'selectpicker bs-select-hidden required','placeholder'=>'Select State','title'=>'Please Select Country','label' => false));?>
                              </div>



                              <!-- <div id="selction-ajax"></div> -->
                              <div class="error has-error" style="display:block;" id="country_id_error"></div>
                              </div>

                              </div>
                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="countryState" class="label_title">State<span class="mand_field">*</span></label>
                              <div class="select-style">
                              <?php echo $this->Form->input('state', array('id' =>'state','options' => $states,'empty' => 'Select State','class' =>'selectpicker bs-select-hidden required','title'=>'Please Select State','placeholder'=>'Select State','label' => false));?>
                              </div>
                              <div class="error has-error" id="state_error" style="display:block;"></div>
                              </div>
                              </div>
                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="city" class="label_title">City<span class="mand_field">*</span></label>

                              <!-- <input class="form-control" type="text" name="city" placeholder="City" id="autocomplete-city" style=""/> -->
                              <div class="select-style">
                              <?php echo $this->Form->input('city', array('id' =>'city','options' =>'$city','empty' => 'Select City','class' =>'selectpicker bs-select-hidden required','title'=>'Please Select City','placeholder'=>'Select City','label' => false));?>
                              </div>


                              <!-- <div id="selction-ajax"></div> -->
                              <div class="error has-error" id="city_error" style="display:block;"></div>
                              </div>

                              </div>
                              </div>
                              <div class="row">

                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="countryState" class="label_title">Select Locality<span class="mand_field">*</span></label>
                              <div class="select-style">
                              <?php echo $this->Form->input('locality', array('id' =>'locality','options' =>'$locality','empty' => 'Select Locality','class' =>'selectpicker bs-select-hidden required','placeholder'=>'Select Locality','title'=>'Please Select Locality','label' => false));?>
                              </div>
                              <div class="error has-error" id="locality_error" style="display:block;"></div>
                              </div>
                              </div>


                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="countryState" class="label_title">Address<span class="mand_field">*</span></label>
                              <?php echo $this->Form->input('address', array('id' =>'address','class' =>'form-control error required','placeholder'=>'House No,Street No,etc.','title'=>'Please Insert Address','label' => false));?>

                              <div class="error has-error" id="address_error" style="display:block;"></div>
                              </div>
                              </div>




                              <div class="col-sm-4">
                              <div class="form-group">
                              <label for="pin" class="label_title">Pincode<span class="mand_field">*</span></label>
                              <?php echo $this->Form->input('pincode', array('id' =>'pincode','class' =>'form-control error required','placeholder'=>'Pin Code','title'=>'Please Insert Pin Code','label' => false));?>

                              <div class="error has-error" id="pincode_error" style="display:block;"></div>
                              </div>
                              </div>

                              <div class="col-sm-6">


                              </div>



                              </div>
                              <?php */ ?>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <div class="col-sm-12">
                                        <label for="pin">Project Name</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="select-style">
                                            <?php echo $this->Form->input('project_id', array('id' => 'project_id', 'options' => $project, 'empty' => 'Select Project', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select Project', 'title' => 'Please Select Project', 'label' => false)); ?>

                                        </div>
                                        <div class="error has-error" id="project_id_error" style="display:block;"></div>
                                    </div>
                                    <!--<div class="col-sm-4">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#addproject" class="button btn-add">Add Project</a>
                                    </div>-->



                                </div>
                            </div>
                            <div class="account-block text-center submit_area">
                                <button type="submit" class="btn btn-primary btn-next " id="next1">NEXT</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

            <form class="fpform" id="property_feature_form">
                <div class="account-block property_feature" >
                    <div class="add-title-tab">
                        <h3>Property Details</h3>
                        <div class="add-expand" id="property_detail_button"></div>
                    </div>
                    <div class="add-tab-content" >
                        <div class="add-tab-row push-padding-bottom" id="property_feature_ajax">

                        </div>
                    </div>
                </div>
            </form>
            <form class="fpform" id="form_price">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Pricing</h3>
                        <div class="add-expand" id="property_pricing_button"></div>
                    </div>
                    <div class="add-tab-content" id="property_pricing_ajax">

                        <div class="add-tab-row push-padding-bottom row-color-gray">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="offer_price_show">
                                        <label for="property-price" class="label_title">Offer Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Asking Price ( All Inclusive Price ). It has to be lower or equal to Market Price."></i>
                                        <?php echo $this->Form->input('offer_price', array('id' => 'offer_price', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Asking Price', 'title' => 'Please Enter Offer Price', 'label' => false)); ?>
                                        <div class="error has-error" id="offer_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_monthly_show" style="display:none">
                                        <label for="property-price" class="label_title">Expected Monthly Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Its your Expected Monthy Rent ( All Inclusive Price ). It has to be lower or equal to Market Rent."></i>
                                        <?php echo $this->Form->input('expected_monthly', array('id' => 'expected_monthly', 'class' => 'form-control box_price required', 'placeholder' => 'Enter Your Expected Monthly Rent', 'title' => 'Please Enter Monthly Rent', 'label' => false)); ?>
                                        <div class="error has-error" id="expected_monthly_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price" class="label_title">Offer Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('offer_price_sqt', array('id' => 'offer_price_sqt', 'class' => 'form-control box_price', 'label' => false)); ?>
                                        <div class="error has-error" id="offer_price_sqt_error" style="display:block"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="row ">


                                <div class="col-sm-4" id="">
                                    <div class="form-group" id="market_price_show">
                                        <label for="property-price-before" class="label_title">Market Price<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Current Approx. Market Price of your property. This will be validated by our experts."></i>

                                        <?php echo $this->Form->input('market_price', array('id' => 'market_price', 'class' => 'form-control box_price required', 'label' => false, 'placeholder' => 'Enter Approx Market Price', 'title' => 'Please Enter Market Price')); ?>

                                        <div class="error has-error" id="market_price_error" style="display:block"></div>
                                    </div>

                                    <div class="form-group" id="expected_market_rent_show" style="display:none">
                                        <label for="property-price-before" class="label_title"> Expected Market Rent<span class="mand_field">*</span></label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Current Approx. Market Rent of your property. This will be validated by our experts."></i>

                                        <?php echo $this->Form->input('market_rent', array('id' => 'market_rent', 'class' => 'form-control box_price required', 'label' => false, 'placeholder' => 'Enter Approx Market Rent', 'title' => 'Please Enter Market Rent')); ?>

                                        <div class="error has-error" id="market_rent_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price" class="label_title">Market Price / Sq.ft.</label>
                                        <?php echo $this->Form->input('market_price_sqt', array('id' => 'market_price_sqt', 'class' => 'form-control box_price', 'label' => false)); ?>
                                        <div class="error has-error" id="market_price_sqt_error" style="display:block"></div>												

                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-sm-4">
                                    <div class="form-group" id="bookamount">
                                        <label for="property-price-before" class="label_title">Booking Amount</label>
                                        <?php echo $this->Form->input('booking_amount', array('id' => 'booking_amount', 'class' => 'form-control box_price', 'title' => 'Please Enter Booking Amount', 'label' => false, 'placeholder' => 'Enter booking amount')); ?>

                                        <div class="error has-error" id="booking_amount_error" style="display:block"></div>
                                    </div>
                                    <div class="form-group" id="depositamount" style="display:none">
                                        <label for="property-price-before" class="label_title"> Security Deposit Amount</label>

                                        <?php echo $this->Form->input('deposit', array('id' => 'deposit-amount-before', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Security Deposit Amount')); ?>

                                        <div class="error has-error" id="deposit_error" style="display:block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-price-maintenance" class="label_title">Maintenance </label>
                                        <i class="fa fa-info-circle hidden-xs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Monthly recurring expenses "></i>
                                        <?php echo $this->Form->input('maintance_amount', array('id' => 'maintance_amount', 'class' => 'form-control box_price', 'label' => false, 'placeholder' => 'Enter Maintenance Amount', 'title' => 'Please Enter Maintenance')); ?>
                                        <div class="error has-error" id="maintance_amount_error" style="display:block"></div>	
                                        <div class="error has-error">Box must be filled out</div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="add-tab-row push-padding-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Availability</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('construction_status', array('id' => 'constructuion_status', 'options' => array('1' => 'Ready to move in', '2' => 'Under Construction'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Construction Status', 'label' => false)); ?>


                                        </div>		
                                        <div class="error has-error" id="construction_status_error" style="display:block" ></div>



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-status" class="label_title">Possession Date</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('possession_date', array('id' => 'possession_date', 'options' => array('3' => 'Within 3 Months', '6' => 'Within 6 Months'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Possession Date', 'label' => false)); ?>



                                        </div>	
                                        <div class="error has-error" id="possession_date_error" style="display:block" ></div>	

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Ownership Type</label>

                                        <div class="select-style">

                                            <?php echo $this->Form->input('ownership_type', array('id' => 'ownership_type', 'options' => array('1' => 'Freehold', '2' => 'Lease Hold', '3' => 'Power of attorney', '4' => 'Co-operative Society'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Ownership Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="ownership_type_error" style="display:block" ></div>	



                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="property-type" class="label_title">Sale Type</label>
                                        <div class="select-style">
                                            <?php echo $this->Form->input('sale_type', array('id' => 'sale_type', 'options' => array('1' => 'Resale', '2' => 'New Booking'), 'empty' => 'Select', 'class' => 'selectpicker bs-select-hidden', 'placeholder' => 'Select', 'title' => 'Please Enter Sale Type', 'label' => false)); ?>



                                        </div>		
                                        <div class="error has-error" id="sale_type_error" style="display:block" ></div>	



                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
                        <div class="account-block text-center submit_area">
                            <button type="submit" id="next3" class="btn btn-primary btn-next ">NEXT</button>
                        </div>  


                    </div>
                </div>
            </form>
            <form class="fpform" id="aminities_form">
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Features and Amenities</h3>
                        <div class="add-expand" id="property_amenities_button"></div>
                    </div>
                    <div class="add-tab-content" id="aminities_ajax">


                    </div>

                </div>
            </form>

            <form class="fpform" method="post" enctype="multipart/form-data" action="upload_image" >
                <div class="account-block">
                    <div class="add-title-tab">
                        <h3>Add Photos</h3>
                        <div class="add-expand" id="property_photo_button"></div>
                    </div>
                    <div class="add-tab-content">
                        <div class="add-tab-row">
                            <div class="property-media">
                                <div class="media-gallery">
                                    <div class="row">

                                        <div id="image_preview"></div> 

                                    </div>
                                </div>
                                <div class="account-block text-center">
                                    <div class="row">
                                        <div class="col-sm-12">	We recommend minimum image size to be 10KB and maximum size to be 5MB. Accepted file format are .png .jpg .jpeg</div>
                                    </div>
                                </div>
                                <?php echo $this->Form->input('property_id', array('class' => 'form-control error property_id', 'type' => 'hidden', 'label' => false, 'value' => '')); ?>
                                <div class="upload_btn_area">

                                    <label class="btn btn-default btn-file">
                                        <?php
                                        echo $this->Form->input('upload_file.', ['type' => 'file', 'label' => false, 'multiple' => 'multiple', 'style' => 'display:none', 'onchange' => 'preview_image()']);
                                        ?>
                                        Add Property Picture 
                                    </label>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="account-block text-center submit_area" id="submit_disable">
                    <button type="submit" class="btn btn-primary btn-red">Submit Property</button>
                </div>
            </form>

            <div class="modal fade" id="addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                �</button>
                            <h4 class="modal-title" id="myModalLabel">
                                Add Project
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form class="fpform">
                                    <div class="col-md-12" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">



                                        <div class="add-tab-row push-padding-bottom ">
                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Project Name<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="projectname" placeholder="Enter your project name">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="projectname" class="label_title">Project Description<span class="mand_field">*</span></label>
                                                            <textarea class="form-control" rows="1" id="project_des" placeholder="Enter your project description"></textarea>
                                                            <div class="error has-error">Box must be filled out</div>
                                                        </div>
                                                    </div>
                                                </div >
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Highlights<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="project_highlight" placeholder="Enter your Highlights">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="projectname" class="label_title">Acres<span class="mand_field">*</span></label>
                                                        <input class="form-control" id="project_acre" placeholder="Enter project Acres">
                                                        <div class="error has-error">Box must be filled out</div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>

                                        <div class="add-tab-row push-padding-bottom text-center">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <button type="button" class="btn btn-primary btn-red">
                                                        Save & Continue</button>
                                                </div>


                                            </div>

                                        </div>


                                    </div>



                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/our-products-->
    </div>
    <!--/.container-->

</section>

