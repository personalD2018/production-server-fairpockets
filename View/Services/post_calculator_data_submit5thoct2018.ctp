<?php
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'PDF', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
//$pdf->SetTitle('TCPDF Example 006');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(2, 2, 2);
$pdf->SetHeaderMargin(9);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();

// create some HTML content
//$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
$pdf->Image('https://www.fairpockets.com/upload/builder_logo/'.$builderlogo, 5, 6, 45, 16, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
$pdf->Image('https://www.fairpockets.com/upload/builder_logo/logo.jpg', 160, 6, 45, 16, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);

$html = '
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
	 <tr>
		<td style="background-color:#eaeaea">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="20">&nbsp;</td>
				
				<td width="600">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
				<td>&nbsp;</td>
				<td height="20">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
					 <tr>										  
						<td width="180" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000; ">
						
						</td>
						<td width="340" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000; ">
						<font style="padding-top:42px;">'.$projectName.'</font> <br><br>
						<font style="font-size:12px;">Price Breakup From Fair Pockets - '.date('l jS  Y').'</font>
						</td>
						<td width="170" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000; ">
						
						</td>
					  </tr>
					  			  
					</table>
				</td>
				<td width="22">&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td height="10">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
		</td>
	  </tr>
	  <tr>
		<td>
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333;"><strong>Dear '.$visitorName.',</strong></td>
					  </tr>
					</table>
				</td>
			  </tr>
			  <tr>
				<td height="10"></td>
			  </tr>
			  <tr>
			  <td width="20">&nbsp;</td>
				<td width="580">Price Breakup for '.$projectName.' By '.$buildername.' , The above values are indicative and subject to change with the changes in builder prices or govt charges.</td>
			  </tr>
			  <tr>
				<td height="30">&nbsp;</td>
			  </tr>
			  <tr>
			  <td width="20">&nbsp;</td>
				<td width="220">
					<table align="left" width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Project Name<br>
						 - <font style="font-weight:bold; margin-left:10px; font-size:14px;">'.$projectName.'</font></td>
					  </tr>
					  
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Rera Id<br>
						 - <font style="font-weight:bold; margin-left:10px; font-size:14px;">'.$getreraByProjectId.'</font></td>
					  </tr>
					  
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Base Rate<br>
						  - <font style="font-weight:bold; margin-left:10px; font-size:14px;">Rs '.$baseRate.' unit/per sqft</font></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Property Area<br>
						  - <font style="font-weight:bold; margin-left:10px; font-size:14px;">'.$propertyArea.' per sqft</font></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;">Property on floor<br>
						  - <font style="font-weight:bold; margin-left:10px; font-size:14px;"> '.$propertyOnFloor.' </font></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  
					  
					  
					  <tr><td width="10" bgcolor="#fff2cd">&nbsp;</td>
						<td bgcolor="#fff2cd" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:20px;">
						&nbsp;
						</td>
					  </tr>
					  
					  <tr><td width="10" bgcolor="#fff2cd">&nbsp;</td>
						<td bgcolor="#fff2cd" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:20px;">
						Sales Name: <strong style="font-weight:bold;">'.$userName.'</strong>
						</td>
					  </tr>
					  <tr><td width="10" bgcolor="#fff2cd">&nbsp;</td>
						<td bgcolor="#fff2cd" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:20px;">
						Sales Contact: <strong style="font-weight:bold;">'.$userMobile.'</strong>
						</td>
					  </tr>
					  
					  <tr><td width="10" bgcolor="#fff2cd">&nbsp;</td>
						<td bgcolor="#fff2cd" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:20px;">
						&nbsp;
						</td>
					  </tr>
					  
					</table>
				</td>
				<td width="680">
				<table width="60%" border="0" align="center" cellpadding="18" cellspacing="0">
                <tr>
                  <td bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000;"><strong>Type of charges</strong></td>
                  <td bgcolor="#c0c0c0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000;"><strong>Price</strong></td>
                </tr>
                <tr>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Basic charges</td>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($baseCharges).'</td>
                </tr>
                <tr>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Additional charges</td>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($additionalCharges).' </td>
                </tr>
                <tr>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">GST (Total)</td>
                  <td bgcolor="#f3f3f3" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($gstTotal).'</td>
                </tr>
                <tr>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb; border-left:1px solid #bbb">Stamp duty &amp; Registration charges</td>
                  <td bgcolor="#e5e5e5" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; border-right:1px solid #bbb">Rs '.number_format($stampRegTotal).'</td>
                </tr>
                <tr>
                  <td bgcolor="#303030" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffc000;"><strong>Total Charges</strong></td>
                  <td bgcolor="#303030" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffc000;"><strong>Rs '.number_format($finalCalculatePrice).'</strong></td>
                </tr>
              </table>
				
				
				</td>
				<td width="20">&nbsp;</td>
				
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;"><strong>Disclaimer :-</strong></td>
					  </tr>
					</table>
				</td>
			  </tr>
			  <tr>
					<td width="580" height="10"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="580" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">1. Stamp duty, registration and other government charges are payable, based on the then prevailing guidelines. The above values are indicative and subject to change with the changes in rates of taxation.</td>
				  </tr>
				  <tr>
					<td width="580"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="580"style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">2. Prices are subject to change by the builder without any prior notice.</td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="580" height="10"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="580" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">3. The PLC for may vary for that particular unit, additional power backup, car parking charges will be charges extra.</td>
				</tr>
				<tr>
					<td width="20">&nbsp;</td>
					<td width="580" height="10"></td>
				  </tr>
				  <tr>
					<td width="20">&nbsp;</td>
					<td width="580" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;">4. Customers are expected to consider the prices indicated here at their sole discretion. Purvanchal or Fairpockets shall not be liable under any circumstances for any decisions taken by the customer based on the indicative pricing calculated here. The pricing initiative is to merely educate the customer on the components of standard pricing by the builder as per the prevailing laws as interpreted by our team.</td>
				</tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			   <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			   <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			   <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  
			</table>
		</td>
	  </tr>
	  <tr>
    <td bgcolor="#29333d"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="30">&nbsp;</td>
        <td width="640" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#fff;">For any further enquiries, please email us at <strong style="color:#ffc000;">support@fairpockets.com</strong></td>
        
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
 </table>';

$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table



//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/app/webroot/files/workorders/project-'.$linkid.'.pdf', 'F');

//============================================================+
// END OF FILE
//============================================================+
