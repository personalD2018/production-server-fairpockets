<?php
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'PDF', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();

// create some HTML content
//$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';

$html = '<h2>HTML TABLE:</h2>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
	 <tr>
		<td style="background-color:#eaeaea">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>			  
						<td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#000;">
						<img src="https://www.fairpockets.com/img/front/logo.png">Elite Project</td>
					  </tr>
					  <tr>
						<td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000;">Price Breakup From Fair Pockets - </td>
					</tr>			  
					</table>
				</td>
				<td width="22">&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td height="30">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
		</td>
	  </tr>
	  <tr>
		<td>
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td width="20">&nbsp;</td>
				<td width="580">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333;"><strong>Dear <?php echo $visitorName; ?>,</strong></td>
					  </tr>
					</table>
				</td>
			  </tr>
			  <tr>
				<td height="10"></td>
			  </tr>
			  <tr>
			  <td width="20">&nbsp;</td>
				<td width="580">Price Breakup for from Fair Pockets.Registration charges, GST and all other statutory taxes. The above values
				  are indicative and subject to change with the changes in rates of taxation.</td>
			  </tr>
			  <tr>
				<td height="30">&nbsp;</td>
			  </tr>
			  <tr>
			  <td width="20">&nbsp;</td>
				<td width="580">
					<table align="left" width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333; line-height:20px;">Project Name<br>
						  project name</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333; line-height:20px;">Base Rate<br>
						  Rs  1234 unit/per sqft</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333; line-height:20px;">Property Area<br>
						  2345 per sqft</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#333; line-height:20px;">Property on floor<br>
						  3th</td>
					  </tr>
					</table>
				</td>
				<td width="580">2</td>
				<td width="20">&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
		</td>
	  </tr>
 </table>';

$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table



//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/app/webroot/files/workorders/project123456.pdf', 'F');

//============================================================+
// END OF FILE
//============================================================+
