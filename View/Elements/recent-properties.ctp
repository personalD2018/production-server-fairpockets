<div class="space-l"></div>
    <div class="container">
        <div class="wow fadeInDown no-padding-bottom">
            <h1>Recent Properties</h1>
		</div>
		<div class="owl-carousel">
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
								<h2 class="property-title"><a href="view_detail_builder.html">3 Bedroom 4 Baths</a></h2>
								<h4 class="property-location">Sunworld Vanalika</h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
								<div class="pro_tt">Residential Apartment for Sale</div>
								<div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
						<div class="table-list full-width hide-on-list">
							<div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
								<h2 class="property-title"><a href="view_detail.html">3 Bedroom 4 Baths</a></h2>
								<h4 class="property-location">Sunworld Vanalika</h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
								<div class="pro_tt">Residential Apartment for Sale</div>
								<div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
						<div class="table-list full-width hide-on-list">
							<div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
								<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
								<h4 class="property-location">Sunworld Vanalika</h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
								<div class="pro_tt">Residential Apartment for Sale</div>
								<div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
						<div class="table-list full-width hide-on-list">
							<div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
			    <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">
                                <div class="price hide-on-list">
                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
                                <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                <h4 class="property-location">Sunworld Vanalika</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
                                <div class="pro_tt">Residential Apartment for Sale</div>
                                <div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
                            <div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
                        <div class="table-list full-width hide-on-list">
                            <div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
								<h2 class="property-title"><a href="view_detail_builder.html">3 Bedroom 4 Baths</a></h2>
								<h4 class="property-location">Sunworld Vanalika</h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
								<div class="pro_tt">Residential Apartment for Sale</div>
								<div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
						<div class="table-list full-width hide-on-list">
							<div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
								<h2 class="property-title"><a href="view_detail.html">3 Bedroom 4 Baths</a></h2>
								<h4 class="property-location">Sunworld Vanalika</h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
								<div class="pro_tt">Residential Apartment for Sale</div>
								<div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
						<div class="table-list full-width hide-on-list">
							<div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="property-item table-list">
					<div class="table-cell">
						<div class="figure-block">
							<figure class="item-thumb">
								<div class="price hide-on-list">
									<h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
									<p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
					<div class="item-body table-cell">
						<div class="body-left table-cell">
							<div class="info-row">
								<div class="label-wrap hide-on-grid">
									<div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
								<h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
								<h4 class="property-location">Sunworld Vanalika</h4>
								<h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
								<div class="pro_tt">Residential Apartment for Sale</div>
								<div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
						<div class="body-right table-cell hidden-gird-cell">
							<div class="info-row price">
								<p class="price-start">Start from</p>
								<h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
								<p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
							<div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
								<p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
						<div class="table-list full-width hide-on-list">
							<div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
			    <div class="property-item table-list">
                    <div class="table-cell">
                        <div class="figure-block">
                            <figure class="item-thumb">
                                <div class="price hide-on-list">
                                    <h3><i class="fa fa-inr" aria-hidden="true"></i>35 Lac</h3>
                                    <p class="offer"><i class="fa fa-arrow-down" aria-hidden="true"></i>25%</p>
								</div>
							<a href="#" class="hover-effect"> <img src="<?php echo $this->webroot; ?>css/home/images/property/01_434x290.jpg" alt=" Bedroom 4 Baths"> </a> </figure>
						</div>
					</div>
                    <div class="item-body table-cell">
                        <div class="body-left table-cell">
                            <div class="info-row">
                                <div class="label-wrap hide-on-grid">
                                    <div class="label-status label label-default">For Sale</div>
								<span class="label label-danger">Sold</span> </div>
                                <h2 class="property-title"><a href="#">3 Bedroom 4 Baths</a></h2>
                                <h4 class="property-location">Sunworld Vanalika</h4>
                                <h4 class="pro_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> Sector-107 Noida</h4>
                                <div class="pro_tt">Residential Apartment for Sale</div>
                                <div class="pro_area">1000 Sq.ft. Super Built Up Area </div>
							</div>
						</div>
                        <div class="body-right table-cell hidden-gird-cell">
                            <div class="info-row price">
                                <p class="price-start">Start from</p>
                                <h3><i class="fa fa-inr" aria-hidden="true"></i>350,000</h3>
                                <p class="rant"><i class="fa fa-inr" aria-hidden="true"></i>21,000/mo</p>
							</div>
                            <div class="info-row phone text-right"> <a href="#" class="btn btn-primary">Details <i class="fa fa-angle-right fa-right"></i></a>
                                <p><a href="#">+1 (786) 225-0199</a></p>
							</div>
						</div>
                        <div class="table-list full-width hide-on-list">
                            <div class="cell"> </div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="space-l"></div>
	<?= $this->Html->script('front/owl.carousel.min');?>
