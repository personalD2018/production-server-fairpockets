<style>
	.displayNone {
		display : none;
	}
</style>
<div class="col-sm-3 listing-filter">
	<div id="mobile-sortby" class="panel-collapse collapse in side-filter mobile-sortby ">
		<div id="sort" class=" filter-area ">
			<?php
				$sort_by_default = ' ';
				$sort_by = array('0' => 'Sort By', 'score' => 'Relevance',
					'date'	=> 'Date',
					'discount' => 'Discount',
					'offer_price_low' => 'Offer Price: Low to High',
					'offer_price_high' => 'Offer Price: High to Low',
					'offer_price_sqft_low' => 'Offer Price/sqft: Low to High',
					'offer_price_sqft_high' => 'Offer Price/sqft: High to Low'
				);
				echo $this->Form->input('sort_by', array(
					'options' => $sort_by, 
					'default' => $sort_by_default, 
					'class' => 'filt-list filter-title', 
					'label' => false, 
					'div' => false, 
					'id' =>'sort_by'
				));
			?>
		</div>
	</div>
	
	<div id="mobile-filter" class="panel-collapse collapse in side-filter mobile-filter">
		<div class="filter-title mobile-hide">
			<strong>Filter</strong>
			<div class="pull-right">
				<a class="cle_fil" href="" data-filter="all">Clear All</a>
			</div>
		</div>
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#bedroom">Bedroom 
				<a class="cle_fil" href="" data-filter="bedroom-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="bedroom" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<ul class="fil_val">
							<li>
								<input id="bedrun_1" class="fil-checkbox" name="bedroom-filters" type="checkbox" value="1BHK">
								<label for="bedrun_1" class="fil-checkbox-label">1 BHK</label>
							</li>
							<li>
								<input id="bedrun_2" class="fil-checkbox" name="bedroom-filters" type="checkbox" value="2BHK">
								<label for="bedrun_2" class="fil-checkbox-label">2 BHK</label>
							</li>
							<li>
								<input id="bedrun_3" class="fil-checkbox" name="bedroom-filters" type="checkbox" value="3BHK">
								<label for="bedrun_3" class="fil-checkbox-label">3 BHK</label>
							</li>
							<li>
								<input id="bedrun_4" class="fil-checkbox" name="bedroom-filters" type="checkbox" value="4BHK">
								<label for="bedrun_4" class="fil-checkbox-label">4 BHK</label>
							</li>
							<li>
								<input id="bedrun_5" class="fil-checkbox" name="bedroom-filters" type="checkbox" value="5BHK">
								<label for="bedrun_5" class="fil-checkbox-label">5 BHK</label>
							</li>
							<li>
								<input id="bedrun_6" class="fil-checkbox" name="bedroom-filters" type="checkbox" value="6BHK+">
								<label for="bedrun_6" class="fil-checkbox-label">5+ BHK</label>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#budget">Budget 
				<a class="cle_fil" href="" data-filter="offer_price">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="budget" class="filter-area in">
				<div class="fil-container">
					<div class="col-md-6">
						<div class="select-style">
							<select class="min-budget" name="min-price" title="Select">
								<option value="0">Min</option>
								<option value="5">5 Lacs</option>
								<option value="10">10 Lacs</option>
								<option value="15">15 Lacs</option>
								<option value="20">20 Lacs</option>
								<option value="25">25 Lacs</option>
								<option value="30">30 Lacs</option>
								<option value="35">35 Lacs</option>
								<option value="40">40 Lacs</option>
								<option value="45">45 Lacs</option>
								<option value="50">50 Lacs</option>
								<option value="55">55 Lacs</option>
								<option value="60">60 Lacs</option>
								<option value="65">65 Lacs</option>
								<option value="70">70 Lacs</option>
								<option value="75">75 Lacs</option>
								<option value="80">80 Lacs</option>
								<option value="85">85 Lacs</option>
								<option value="90">90 Lacs</option>
								<option value="95">95 Lacs</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="select-style">
							<select class="max-budget" name="max-price" title="Select">
								<option value="0">Max</option>
								<option value="10">10 Lacs</option>
								<option value="15">15 Lacs</option>
								<option value="20">20 Lacs</option>
								<option value="25">25 Lacs</option>
								<option value="30">30 Lacs</option>
								<option value="35">35 Lacs</option>
								<option value="40">40 Lacs</option>
								<option value="45">45 Lacs</option>
								<option value="50">50 Lacs</option>
								<option value="55">55 Lacs</option>
								<option value="60">60 Lacs</option>
								<option value="65">65 Lacs</option>
								<option value="70">70 Lacs</option>
								<option value="75">75 Lacs</option>
								<option value="80">80 Lacs</option>
								<option value="85">85 Lacs</option>
								<option value="90">90 Lacs</option>
								<option value="95">95 Lacs</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#postby">Posted By 
				<a class="cle_fil" href="" data-filter="posted-by-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="postby" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<ul class="fil_val one_col">
							<li>
								<input id="postby_1" class="fil-checkbox" name="posted-by-filters" type="checkbox" value="owner">
								<label for="postby_1" class="fil-checkbox-label">Owner</label>
							</li>
							<li>
								<input id="postby_2" class="fil-checkbox" name="posted-by-filters" type="checkbox" value="builder">
								<label for="postby_2" class="fil-checkbox-label">Builder</label>
							</li>
							<li>
								<input id="postby_3" class="fil-checkbox" name="posted-by-filters" type="checkbox" value="broker">
								<label for="postby_3" class="fil-checkbox-label">Dealer</label>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div> 
		
		
		
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#availability">Availability 
				<a class="cle_fil" href="" data-filter="availability-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="availability" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<div class="inner-filter-title"  data-toggle="collapse" data-target="#undercon">
							<input id="undercon_9" class="fil-checkbox" name="availability-filters" type="checkbox" value="under construction">
							<label for="undercon_9" class="fil-checkbox-label">Under Construction</label>
						<span class="collaps-icon pull-right"></span></div>
						<div id="undercon" class="in">
							<div class="col-md-12"> Possession in
								<ul class="fil_val one_cols">
									<li>
										<input id="uc_po_in_1" class="fil-checkbox" name="posession_date-filters" type="checkbox" value="1">
										<label for="uc_po_in_1" class="fil-checkbox-label">1 Years</label>
									</li>
									<li>
										<input id="uc_po_in_2" class="fil-checkbox" name="posession_date-filters" type="checkbox" value="2">
										<label for="uc_po_in_2" class="fil-checkbox-label">2 Years</label>
									</li>
									<li>
										<input id="uc_po_in_3" class="fil-checkbox" name="posession_date-filters" type="checkbox" value="3">
										<label for="uc_po_in_3" class="fil-checkbox-label">3 Years</label>
									</li>
									<li>
										<input id="uc_po_in_4" class="fil-checkbox" name="posession_date-filters" type="checkbox" value="4">
										<label for="uc_po_in_4" class="fil-checkbox-label">4 Years</label>
									</li>
									<li>
										<input id="uc_po_in_5" class="fil-checkbox" name="posession_date-filters" type="checkbox" value="5">
										<label for="uc_po_in_5" class="fil-checkbox-label">5 Years</label>
									</li>
								</ul>
							</div>
						</div>
						<div class="inner-filter-title" data-toggle="collapse" data-target="#readytomove">
							<input id="readytomove_1" class="fil-checkbox" name="availability-filters" type="checkbox" value="ready to move">
							<label for="readytomove_1" class="fil-checkbox-label">Ready to move</label>
						<span class="collaps-icon pull-right"></span></div>
						<div id="readytomove" class="in">
							<div class="col-md-12"> Age of property
								<ul class="fil_val one_col">
									<li>
										<input id="age_pro_1" class="fil-checkbox" name="age_of_property-filters" type="checkbox" value="0 to 1 yaer">
										<label for="age_pro_1" class="fil-checkbox-label">0 to 1 years old</label>
									</li>
									<li>
										<input id="age_pro_2" class="fil-checkbox" name="age_of_property-filters" type="checkbox" value="1 to 5 years">
										<label for="age_pro_2" class="fil-checkbox-label">1 to 5 years old</label>
									</li>
									<li>
										<input id="age_pro_3" class="fil-checkbox" name="age_of_property-filters" type="checkbox" value="5 to 10 years">
										<label for="age_pro_3" class="fil-checkbox-label">5 to 10 years old</label>
									</li>
									<li>
										<input id="age_pro_4" class="fil-checkbox" name="age_of_property-filters" type="checkbox" value="10+ years">
										<label for="age_pro_4" class="fil-checkbox-label">10+ years old</label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#locality">Locality 
				<a class="cle_fil" href="" data-filter="locality-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="locality" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						
						<input type="text" id="locality-search" placeholder="Search for names..">
						<div class="clear"></div>
						Select Locality
						
						<ul class="fil_val one_col" id="locality-list">
							<?php
									//pr($result);die;
									if(isset($result)) {
										$localities = $result->getFacetSet()->getFacet('Locality');
										$locality_items = '';
										$index = 1;
										foreach($localities as $name => $count) {
											$locality_items .= '<li>
													<input id="locality_list_'.$index.'" class="fil-checkbox" name="locality-filters" type="checkbox" value="'.$name.'">
													<label for="locality_list_'.$index.'" class="fil-checkbox-label">'.ucwords($name).'</label>
												</li>';
											$index++;
										}
										
										echo $locality_items;										
									}
							?>
							
						</ul>
					</div>
				</div>
			</div>
		</div> 
		
		
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#society">Society 
				<a class="cle_fil" href="" data-filter="society-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="society" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<input type="text" id="society-search" placeholder="Search for names..">
						<div class="clear"></div>
						Select Societies
						
						<ul class="fil_val one_col" id="society-list">
						
						<?php
								if(isset($result)) {
									$societies = $result->getFacetSet()->getFacet('Society');
									$society_items = '';
									$index = 1;
									foreach($societies as $name => $count) {
										$society_items .= '<li>
												<input id="society_list_'.$index.'" class="fil-checkbox" name="society-filters" type="checkbox" value="'.$name.'">
												<label for="society_list_'.$index.'" class="fil-checkbox-label">'.ucwords($name).'</label>
											</li>';
										$index++;
									}
									
									echo $society_items;
								}
						?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#saletype">Sale Type 
				<a class="cle_fil" href="" data-filter="sale-type-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="saletype" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<ul class="fil_val one_col">
							<li>
								<input id="sale_type_1" class="fil-checkbox" name="sale-type-filters" type="checkbox" value="resale">
								<label for="sale_type_1" class="fil-checkbox-label">Resale</label>
							</li>
							<li>
								<input id="sale_type_2" class="fil-checkbox" name="sale-type-filters" type="checkbox" value="new booking">
								<label for="sale_type_2" class="fil-checkbox-label">New Booking</label>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#area">Area 
				<a class="cle_fil" href="">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="area" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-6">
						<input type="text" class="fil-field" name="area_min" placeholder="Min">
					</div>
					<div class="col-md-6">
						<input type="text" class="fil-field" name="area_max" placeholder="Max">
					</div>
					<!--<div class="col-md-6">
						<div class="select-style">
							<select name="area_type" title="Select">
								<option value="sq_ft">Sq.Ft.</option>
								<option value="Sq.Yards">Sq. Yards</option>
								<option value="Sq.Meter">Sq. Meter</option>
								<option value="Grounds">Grounds</option>
								<option value="Aanakadam">Aankadam</option>
								<option value="Rood">Rood</option>
								<option value="Chataks">Chataks</option>
							</select>
						</div>
					</div>-->
					<div class="col-md-6">
						<input type="submit" name="go_area_submit" class="fil-submit" value="GO">
					</div>
				</div>
			</div>
		</div>  
		
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#furnishing">Furnishing 
				<a class="cle_fil" href="" data-filter="furnishing-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="furnishing" class="filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<ul class="fil_val one_col">
							<li>
								<input id="furnishing_1" class="fil-checkbox" name="furnishing-filters" type="checkbox" value="furnished">
								<label for="furnishing_1" class="fil-checkbox-label">Furnished</label>
							</li>
							<li>
								<input id="furnishing_2" class="fil-checkbox" name="furnishing-filters" type="checkbox" value="unfurnished">
								<label for="furnishing_2" class="fil-checkbox-label">Unfurnished</label>
							</li>
							<li>
								<input id="furnishing_3" class="fil-checkbox" name="furnishing-filters" type="checkbox" value="semifurnished">
								<label for="furnishing_3" class="fil-checkbox-label">Semifurnished</label>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>  
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#amenities">Amenities 
				<a class="cle_fil" href="" data-filter="amenities-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="amenities" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-12">
						<ul class="fil_val one_col">
							<li>
								<input id="amenities_1" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Bank Attached Property">
								<label for="amenities_1" class="fil-checkbox-label">Bank Attached Property</label>
							</li>
							<li>
								<input id="amenities_2" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Centrally Air Conditioned">
								<label for="amenities_2" class="fil-checkbox-label">Centrally Air Conditioned</label>
							</li>
							<li>
								<input id="amenities_3" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Club House/Community">
								<label for="amenities_3" class="fil-checkbox-label">Club House/Community</label>
							</li>
							<li>
								<input id="amenities_4" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Feng Shui/Vasstu Compliant">
								<label for="amenities_4" class="fil-checkbox-label">Feng Shui/Vasstu Compliant</label>
							</li>
							<li>
								<input id="amenities_5" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Fitness Centre/GYM">
								<label for="amenities_5" class="fil-checkbox-label">Fitness Centre/GYM</label>
							</li>
							<li>
								<input id="amenities_6" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Intercom Facility">
								<label for="amenities_6" class="fil-checkbox-label">Intercom Facility</label>
							</li>
							<li>
								<input id="amenities_7" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Internet/wi-fi connectivity">
								<label for="amenities_7" class="fil-checkbox-label">Internet/wi-fi connectivity</label>
							</li>
							<li>
								<input id="amenities_8" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Lift(s)">
								<label for="amenities_8" class="fil-checkbox-label">Lift</label>
							</li>
							<li>
								<input id="amenities_9" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Maintenance Staff">
								<label for="amenities_9" class="fil-checkbox-label">Maintenance Staff</label>
							</li>
							<li>
								<input id="amenities_10" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Park">
								<label for="amenities_10" class="fil-checkbox-label">Park</label>
							</li>
							<li>
								<input id="amenities_11" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Piped-Gas">
								<label for="amenities_11" class="fil-checkbox-label">Piped-Gas</label>
							</li>
							<li>
								<input id="amenities_12" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Piped-gas">
								<label for="amenities_12" class="fil-checkbox-label">Piped-gas</label>
							</li>
							<li>
								<input id="amenities_13" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Rain Water Harvesting">
								<label for="amenities_13" class="fil-checkbox-label">Rain Water Harvesting</label>
							</li>
							<li>
								<input id="amenities_14" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Security Personnel">
								<label for="amenities_14" class="fil-checkbox-label">Security Personnel</label>
							</li>
							<li>
								<input id="amenities_15" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Security/Fire Alarm">
								<label for="amenities_15" class="fil-checkbox-label">Security/Fire Alarm</label>
							</li>
							<li>
								<input id="amenities_16" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Shopping /Center">
								<label for="amenities_16" class="fil-checkbox-label">Shopping /Center</label>
							</li>
							<li>
								<input id="amenities_17" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Swimming Pool">
								<label for="amenities_17" class="fil-checkbox-label">Swimming Pool</label>
							</li>
							<li>
								<input id="amenities_18" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Visitor Parking">
								<label for="amenities_18" class="fil-checkbox-label">Visitor Parking</label>
							</li>
							<li>
								<input id="amenities_19" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Water Purifier">
								<label for="amenities_19" class="fil-checkbox-label">Water Purifier</label>
							</li>
							<li>
								<input id="amenities_20" class="fil-checkbox" name="amenities-filters" type="checkbox" value="Water storage">
								<label for="amenities_20" class="fil-checkbox-label">Water storage</label>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="sort" class=" filter-inner">
			<div class="filter-title " data-toggle="collapse" data-target="#floorno">Floor Number 
				<a class="cle_fil" href="" data-filter="amenities-filters">Clear</a>
				<span class="collaps-icon pull-right"></span>
			</div>
			<div id="floorno" class=" filter-area in">
				<div class="fil-container">
					<div class="col-md-6">
						<input type="text" class="fil-field" name="floor_min" placeholder="Min">
					</div>
					<div class="col-md-6">
						<input type="text" class="fil-field" name="floor_max" placeholder="Max">
					</div>
					<div class="col-md-6">
						<input type="submit" name="go_floor_submit" class="fil-submit" value="GO">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

