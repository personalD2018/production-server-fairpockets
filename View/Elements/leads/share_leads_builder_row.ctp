	<?php $this->Paginator->options(array(
			'update' => '#clientLists',
			'evalScripts' => true,
			'before' => $this->Js->get('#loaderID')->effect(
				'fadeIn',
				array('buffer' => false)
			),
			'complete' => $this->Js->get('#loaderID')->effect(
				'fadeOut',
				array('buffer' => false)
			),
		));
$ascImage = $this->Html->image('up_arrow.gif',array('border'=>0));
$descImage = $this->Html->image('down_arrow.gif',array('border'=>0));
?>

<style>
    .thead{
		background-color: #29333d;
    background: :red;
    color: #ffffff;
	}
	thead a{color:#ffffff !important;}
	thead a:hover{color:#ffffff !important;}
	
		*, *:before, *:after {-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
input, select, input[type="checkbox"], input[type="radio"] {-webkit-border-radius:0;-webkit-appearance:none;
    outline:none;}
li {  margin-bottom:1.5em;  padding-left:2.5em;  	position:relative;}

input[type="checkbox"],
input[type="radio"],
li label::before {cursor:pointer;height:40px;left:0;margin-top:-20px;position:absolute;width:40px;top:50%;}

input[type="checkbox"],
input[type="radio"] {	  display:inline-block;	  opacity:0;	  vertical-align:middle;}

li label::before {	  border:2px solid lighten(#2a4a59,10%);	  border-radius:4px;	  color:darken(#48CFAD, 20%);
	  content:'';	  font-size:1.5em;	  padding:.1em 0 0 .2em;}

li input.error + label::before {	  border-color:#f93337;}

li input[type="checkbox"]:checked + label::before {border-color:darken(#48CFAD, 20%);content:'\2714';}

li input[type="radio"] + label::before {	  border-radius:50%;}

li input[type="radio"]:checked + label::before {border-color:darken(#48CFAD, 20%);content:'\25CF';font-size:1.5em;
  	padding:0 0 0 .3em;}

ul {  	margin-bottom:1em;  padding-top:1em;  	overflow:hidden;}

li label {	  display:inline-block;	  vertical-align:top;}

.js-errors {
    background:#d62540;
    border-radius:8px;
    color:#FFF;
    font-size:.9em;
    list-style-type:none;
    margin-bottom:1em;
    padding:1em;
	font-weight:bold;
}

.js-errors {	  display:none;}

.js-errors li {	  margin-left:1em;  margin-bottom:.5em;  padding-left:0;}
	
	
	
</style>
	
	<?php //$paginator = $this->Paginator;?>
	
	
	
		<div class="space-x"></div>
		<ul class="js-errors"></ul>
		<div class="space-x"></div>
		<?php
			echo $this->Form->create('ShareLeads', array(
				'url' => '/leads/share_leads_app_users/',
				'id' => 'frm_lead', 
				'name'=>'frm_lead',
				'method'=>'POST'
			));
			
		?>
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead class="thead">
                    <tr>                        
						<th>
						<?php echo $this->Paginator->sort('AppClientLead.email', 'Contact Details'); ?>
						<?php if($this->Paginator->sortKey() == "AppClientLead.email") { echo $this->Paginator->sortDir() === 'asc' ? 
						$ascImage : $descImage;}?>
						</th>
                        <th>City</th>
						<th>Locality</th>
						<th>Budget</th>
						<th>Source</th>
						<th>
						<?php echo $this->Paginator->sort('AppClientLead.created', 'Created Date'); ?>
						<?php if($this->Paginator->sortKey() == "AppClientLead.created") { echo $this->Paginator->sortDir() === 'asc' ? 
						$ascImage : $descImage;}?>
						</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
					<?php if (!empty($leads)) {
                        for ($i = 0; $i < count($leads); $i++) {
                    ?>
                    <tr id="row<?php echo $leads[$i]['AppClientLead']['id']?>">
                        <td>
						<b>Name</b> &nbsp;&nbsp;: <?php echo isset($leads[$i]['AppClientLead']['name']) ? $leads[$i]['AppClientLead']['name'] : "N/A";?><br>
						<b>Email</b> &nbsp;&nbsp; : <?php echo isset($leads[$i]['AppClientLead']['email']) ? $leads[$i]['AppClientLead']['email'] : "N/A";?><br>
						<b>Mobile</b> &nbsp;: <?php echo isset($leads[$i]['AppClientLead']['mobile']) ? $leads[$i]['AppClientLead']['mobile'] : "N/A";?>
						</td>
                        <td><?php echo isset($leads[$i]['AppClientLead']['city']) ? $leads[$i]['AppClientLead']['city'] : "N/A";?></td>
                        <td><?php echo isset($leads[$i]['AppClientLead']['locality']) ? $leads[$i]['AppClientLead']['locality'] : "N/A";?></td>
						<td><?php echo isset($leads[$i]['AppClientLead']['budget']) ? $leads[$i]['AppClientLead']['budget'] : "N/A";?></td>
						<td><?php echo isset($leads[$i]['AppClientLead']['source_lead']) ? $leads[$i]['AppClientLead']['source_lead'] : "N/A";?></td>
						<td><?php echo isset($leads[$i]['AppClientLead']['created']) ? $leads[$i]['AppClientLead']['created'] : "N/A";?></td>
						
						<td style="text-align: center;">
							<input type="checkbox" name="check_list[]" id="shareleads<?php echo $i; ?>.leads_check" class="checkbox-custom" value="<?php echo $leads[$i]['AppClientLead']['id']; ?>">
							<label for="shareleads<?php echo $i; ?>.leads_check" class="checkbox-custom-label"></label>    
						</td>
                    </tr>
					<?php }
					}else{ ?>
					
					<tr>
					<td colspan="8" style="text-align:center;color:#d62540;"><b>No Records Found</b></td>
					</tr>
					
					<?php } ?>
                </tbody>
            </table>
		<div class="clear"></div>
		<?php if (!empty($leads)) { ?>
		<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<div class="col-sm-3">&nbsp;</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-primary" name="submitaction" value="sales">Share With Sales Users</button>
						</div>						
						<!--<div class="col-sm-3">
							<button type="submit" class="btn btn-primary" name="submitaction" value="broker">Share With Brokers</button>
						</div>-->
						<div class="col-sm-3">&nbsp;</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="pagination">
	<?php
	 if($this->Paginator->numbers())
	 {
	 echo $this->Paginator->prev();
	 ?>
	<?php	    
		 echo $this->Paginator->numbers(); 
	?>
	<!-- Shows the next and previous links -->	
	<?php

	 echo $this->Paginator->next();
	 }
	 ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>
		
	</div>
	
	<?php
		echo $this->Html->script('validate_1.9_jquery.validate.min');
		echo $this->Html->script('front/additional-methods.min');
	?>
		<script type="text/javascript">
		
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/leads/deleteLeadsBuilder/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
	
	
	
	
	 /*$('#frm_lead').validate({ // initialize the plugin
        rules: {
            'check_list[]': {
                required: true,
                maxlength: 2
            }
        },
        messages: {
            'check_list[]': {
                required: "You must check at least 1 box",
                maxlength: "Check no more than {0} boxes"
            }
        }
    });*/
	
	
	
		$("#frm_lead").validate({
        rules: {
          "check_list[]": {
            required: true,
            minlength: 1
          }
        },

        // FIX
        // Using highlight and unhighlight options we can add the error class to the parent ul which can then be selected and styled
        			highlight: function(element, errorClass, validClass) {
        				  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
        				  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
        			},
        			unhighlight: function(element, errorClass, validClass) {
          				$(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
          				$(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
			        },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",

        messages: {
          "check_list[]": "Please select at least one checkbox"
        },
      });
	
	
	
	
	
});
</script>	
	
		
