<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
    .thead{
		background-color: #29333d;
    background: :red;
    color: #ffffff;
	}
	thead a{color:#ffffff !important;}
	thead a:hover{color:#ffffff !important;}
	
		*, *:before, *:after {-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
input, select, input[type="checkbox"], input[type="radio"] {-webkit-border-radius:0;-webkit-appearance:none;
    outline:none;}
li {  margin-bottom:1.5em;  padding-left:2.5em;  	position:relative;}

input[type="checkbox"],
input[type="radio"],
li label::before {cursor:pointer;height:40px;left:0;margin-top:-20px;position:absolute;width:40px;top:50%;}

input[type="checkbox"],
input[type="radio"] {	  display:inline-block;	  opacity:0;	  vertical-align:middle;}

li label::before {	  border:2px solid lighten(#2a4a59,10%);	  border-radius:4px;	  color:darken(#48CFAD, 20%);
	  content:'';	  font-size:1.5em;	  padding:.1em 0 0 .2em;}

li input.error + label::before {	  border-color:#f93337;}

li input[type="checkbox"]:checked + label::before {border-color:darken(#48CFAD, 20%);content:'\2714';}

li input[type="radio"] + label::before {	  border-radius:50%;}

li input[type="radio"]:checked + label::before {border-color:darken(#48CFAD, 20%);content:'\25CF';font-size:1.5em;
  	padding:0 0 0 .3em;}

ul {  	margin-bottom:1em;  padding-top:1em;  	overflow:hidden;}

li label {	  display:inline-block;	  vertical-align:top;}

.js-errors {
    background:#d62540;
    border-radius:8px;
    color:#FFF;
    font-size:.9em;
    list-style-type:none;
    margin-bottom:1em;
    padding:1em;
	font-weight:bold;
}

.js-errors {	  display:none;}

.js-errors li {	  margin-left:1em;  margin-bottom:.5em;  padding-left:0;}
	
	
	
</style>

<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Share Leads To App Users1</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('ShareLead', array(
			'url' => '/leads/reassign_leads_app_users_assign/',
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		
	?>
	
	<input type="hidden" name="broker_id" value="broker_id">
	<input type="hidden" name="builder_id" value="builder_id">
	
	<input type="hidden" name="leadsjson" value='<?php echo $leadsjson; ?>'>
    <div class="row">
	
	<div class="col-md-12">
            <table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($BuilderSalesList)) {
                        for ($i = 0; $i < count($BuilderSalesList); $i++) {
                    ?>
                    <tr>
                        <td><?php echo $BuilderSalesList[$i]['sales_users']['name']; ?></td>
                        <td><?php echo $BuilderSalesList[$i]['sales_users']['email']; ?></td>
                        <td><?php echo $BuilderSalesList[$i]['sales_users']['mobile']; ?></td>
						<td style="text-align: center;">
							<input type="checkbox" name="check_list[]" id="shareinv<?php echo $i; ?>.sales_check" class="checkbox-custom" value="<?php echo $BuilderSalesList[$i]['sales_users']['id']; ?>">
							<label for="shareinv<?php echo $i; ?>.sales_check" class="checkbox-custom-label"></label>    
						</td>
                    </tr>
					<?php }}else{ ?>
					<tr>
                        <td colspan="4" style="text-align:center;">
							No User available, please add sales employees. <a href="/accounts/add_sales_employees1">Click Here</a>
						</td>
					</tr>
					<?php } ?>
                </tbody>
            </table>
        </div>
           
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
    </div>
</div>
<?php
		echo $this->Html->script('validate_1.9_jquery.validate.min');
		echo $this->Html->script('front/additional-methods.min');
	?>
<script type="text/javascript">
    jQuery(document).ready(function() {		
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
		
		
		$("#frm_inventory_first").validate({
        rules: {
          "check_list[]": {
            required: true,
            minlength: 1,
			maxlength:1
          }
        },

        // FIX
        // Using highlight and unhighlight options we can add the error class to the parent ul which can then be selected and styled
        			highlight: function(element, errorClass, validClass) {
        				  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
        				  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
        			},
        			unhighlight: function(element, errorClass, validClass) {
          				$(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
          				$(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
			        },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",

        messages: {
          "check_list[]": "Please select at least one checkbox"
        },
      });
		
		
		
    });
</script>