<?php $this->Paginator->options(array(
			'update' => '#clientLists',
			'evalScripts' => true,
			'before' => $this->Js->get('#loaderID')->effect(
				'fadeIn',
				array('buffer' => false)
			),
			'complete' => $this->Js->get('#loaderID')->effect(
				'fadeOut',
				array('buffer' => false)
			),
		));
$ascImage = $this->Html->image('up_arrow.gif',array('border'=>0));
$descImage = $this->Html->image('down_arrow.gif',array('border'=>0));
?>
<style>
.rounded a{
color:#ffffff;
text-decoration:none;
}
</style>
<?php echo '884'; ?>
<?php if(empty($leads))
{ ?>
	<table id="rounded-corner" summary="2007 Major IT Companies' Profit">
		<thead>
			<tr>
				<th scope="col" class="rounded-company"></th>
				<th scope="col" class="rounded">Client Name</th>
				<th scope="col" class="rounded">Created</th>           
				<th scope="col" class="rounded-q4">Action</th>            
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="8" class="rounded-foot-left"><em></em></td>
				<td class="rounded-foot-right">&nbsp;</td>
			</tr>
		</tfoot>
		<tbody>
			<tr>
				<td colspan="9">
				<div  align="center" class="msg"><?php echo 'No record found.';?></div>
				</td>
			</tr>
		</tbody>
	</table>
			   
		
<?php 
} else
{ ?>   

<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Creation Date</th>
						<th>Actions</th>
                    </tr>
                </thead>
				<tbody>
                    <?php
                        foreach ($leads as $key=>$row):
                    ?>
                    <tr id="row<?php echo $row['AppClientLead']['id']?>">
                        <td><?php echo isset($row['AppClientLead']['name']) ? $row['AppClientLead']['name'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['email']) ? $row['AppClientLead']['email'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['mobile']) ? $row['AppClientLead']['mobile'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['created']) ? $row['AppClientLead']['created'] : "N/A";?></td>
						
						<td style="text-align: center;">
                            <a href="<?php echo Router::url('/leads/editLeadsBuilder/'.$row['AppClientLead']['id'],true);?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a class='ajaxDelete' href="javascript:void(0)" id='<?php echo $row['AppClientLead']['id'];?>' title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

			
			<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>		
				
			</ul>
			
		</div>
           
	<table id="rounded-corner" summary="2007 Major IT Companies' Profit" style="display:none;">
		<thead>
			<tr>
			<th scope="col" class="rounded-company"></th>
			<th scope="col" class="rounded"><?php echo $this->Paginator->sort('AppClientLead.name', 'Client Name'); ?><?php if($this->Paginator->sortKey() == "AppClientLead.name") { echo $this->Paginator->sortDir() === 'asc' ? $ascImage : $descImage;}?></th>
			<th scope="col" class="rounded"><?php echo $this->Paginator->sort('AppClientLead.business_type', 'Business Type'); ?><?php if($this->Paginator->sortKey() == "AppClientLead.business_type") { echo $this->Paginator->sortDir() === 'asc' ? $ascImage : $descImage;}?></th>
			<th scope="col" class="rounded">AppClientLead Contact</th>
			<th scope="col" class="rounded-q4">Action</th>               
			</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="8" class="rounded-foot-left"><em></em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
	<?php foreach ($leads as $lead):?>
		<tr>
			<td>&nbsp;</td>
			<td>
				<?php echo $lead['AppClientLead']['name'];?>
			</td>
			<td>
				<?php echo $lead['AppClientLead']['name'];?>
			</td>
			<td>
				<?php echo $lead['AppClientLead']['name'];?>
			</td>
			<td>
				<?php echo $lead['AppClientLead']['name'];?>
			</td>
		</tr>
	<?php endforeach; ?>    	        
	</tbody>
</table>
<?php } ?>
</span>	
<div class="pagination" style="display:none;">
	<?php
	 if($this->Paginator->numbers())
	 {
	 echo $this->Paginator->prev();
	 ?>
	<?php	    
		 echo $this->Paginator->numbers(); 
	?>
	<!-- Shows the next and previous links -->	
	<?php

	 echo $this->Paginator->next();
	 }
	 ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>