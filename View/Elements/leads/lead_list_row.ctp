	<?php $this->Paginator->options(array(
			'update' => '#clientLists',
			'evalScripts' => true,
			'before' => $this->Js->get('#loaderID')->effect(
				'fadeIn',
				array('buffer' => false)
			),
			'complete' => $this->Js->get('#loaderID')->effect(
				'fadeOut',
				array('buffer' => false)
			),
		));
$ascImage = $this->Html->image('up_arrow.gif',array('border'=>0));
$descImage = $this->Html->image('down_arrow.gif',array('border'=>0));
?>

<style>
    .thead{
		background-color: #29333d;
    background: :red;
    color: #ffffff;
	}
	thead a{color:#ffffff !important;}
	thead a:hover{color:#ffffff !important;}
</style>
	
	<?php //$paginator = $this->Paginator;?>
	
	
	
		<div class="space-x"></div>
		
		<div class="space-x"></div>
		
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead class="thead">
                    <tr>
                        <th>Name</th>
						<th>
						<?php echo $this->Paginator->sort('AppClientLead.email', 'Email'); ?>
						<?php if($this->Paginator->sortKey() == "AppClientLead.email") { echo $this->Paginator->sortDir() === 'asc' ? 
						$ascImage : $descImage;}?>
						</th>
                        <th>Mobile</th>
						<th>Sales/Broker Name</th>
						<th>Creation Date</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($leads as $key=>$row):
                    ?>
                    <tr id="row<?php echo $row['AppClientLead']['id']?>">
                        <td><?php echo isset($row['AppClientLead']['name']) ? $row['AppClientLead']['name'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['email']) ? $row['AppClientLead']['email'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['mobile']) ? $row['AppClientLead']['mobile'] : "N/A";?></td>
						<td><?php echo isset($row['SalesUser']['name']) ? $row['SalesUser']['name'] : "N/A";?></td>
                        <td><?php echo isset($row['AppClientLead']['created']) ? $row['AppClientLead']['created'] : "N/A";?></td>
						
						<td style="text-align: center;">
                            <a href="<?php echo Router::url('/leads/editLeadsBuilder/'.$row['AppClientLead']['id'],true);?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;                           
                            <a class='ajaxDelete' href="javascript:void(0)" id='<?php echo $row['AppClientLead']['id'];?>' title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
		<div class="clear"></div>
		
		<div class="pagination">
	<?php
	 if($this->Paginator->numbers())
	 {
	 echo $this->Paginator->prev();
	 ?>
	<?php	    
		 echo $this->Paginator->numbers(); 
	?>
	<!-- Shows the next and previous links -->	
	<?php

	 echo $this->Paginator->next();
	 }
	 ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>
		
	</div>
	
		<script type="text/javascript">
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/leads/deleteLeadsBuilder/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
});
</script>
