<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
//App::import('Controller', 'Countries');
//$countries = new CountriesController;
//$countries->constructClasses();

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ServicesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array(
        'Property', 
        'MstAreaState', 
        'MstPropertyTypeOption', 
        'FeatureMasters', 
        'PropertyFeatures', 
        'AmentieMaster', 
        'PropertyPic', 
        'MstAreaRegion','MstAreaState','MstAreaCity', 'MstAreaLocality', 
        'PropertyInquirie', 
        'User', 
        'PropertyNotifiction', 
        'MstAreaCountry', 
        'Project', 
        'Builder', 
        'ProjectDetails', 
        'Bank', 
        'ProjectFinancer', 
        'ProjectPricing', 
        'ProjectOfficeUse', 
        'Builder', 
        'Websiteuser', 
        'MstAssignCity', 
        'MstAssignCountry', 
        'MstAssignLocality', 
        'MstAssignRegion', 
        'MstAssignState', 
        'FeaturePropertyOptionView', 
        'AmenityPropertyOptionView', 
        'PropertyAmenity',
		'PropertyFeature', 
        'MstAmenity', 
		'MstFeature',
        'VAmenityPropertyOption', 
        'VFeaturePropertyOption',
        'FeatureAminityPropertyOptionView',
		'Lead');
    public $components = array('Paginator','Propertydata');

    //$countries->constructClasses();
    /**
     * Displays a view
     *
     * @return void
     * @throws ForbiddenException When a directory traversal attempt.
     * @throws NotFoundException When the view file could not be found
     *   or MissingViewException in debug mode.
     */
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('viewproperty', 'view_detail_builder','view_detail_builder1', 'editproperty', 'addproperty', 'addpropertyfeaturedetail', 'addpropertyPrice', 'addpropertyamenities', 'upload_image', 'propertysubmit', 'getpropertyoption');
    }

    /* get state for add property */

    public function salesLoginAuth(){
		
		$this->loadModel('SalesUser');
		print_r($_GET['name']);
		$_GET['name'];
		$_GET['password'];
		if($this->SalesUser->hasAny(array("email"=>$_GET['email']))){
			$usersExist = $this->SalesUser->find('all');
				//echo '<pre>'; print_r($usersExist);
			//$usersDetails = json_encode($usersExist);
			//print_r(json_encode($usersExist));
			$message = "Success";
			$code = 1;
			$data = array("id"=>1,"builder_id"=>45);
			//$userStatus = 1;
			//$userInfo = "User Exist";
			$resp = array("message"=>$message,"code"=>$code,"data"=>$data);
			//$usersDetails = json_encode($resp);
			print json_encode($resp); exit;
		}else{
			$userStatus = 0;
			$userInfo = "User Not Exist";
			$resp = array("userStatus"=>$userStatus,"userInfo"=>$userInfo);
			//$usersDetails = json_encode($resp);
			print json_encode($resp); exit;
			
		}
		
	}
}
