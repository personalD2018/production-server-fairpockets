<?php if (@$status != '3' && $this->Session->read('Auth.Websiteuser.userrole') == '3') { ?>
<?php //if (@$status != '3' && $this->Session->read('Auth.Websiteuser.userrole') == '3') { ?>
    <style>
        #disable{
            pointer-events: none;
            background:#e6e6e6 !important;
        }
    </style>
<?php }
?>
	<div id="dashboard_menus">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<div id="MainMenu">
					<div class="list-group">

						<a href="<?php echo $this->webroot; ?>accounts/dashboard" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'dashboard') ? 'active' : '';?>" id="disable" data-parent="#MainMenu">My Dashboard</a>
						<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') {
							?>	
							<a href="#demo1" class="list-group-item list-group-item-success <?php echo ($this->params['controller'] == 'projects') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">My Projects<i class="fa fa-caret-down"></i></a>

							<div class="dropdown <?php echo ($this->params['controller'] == 'projects') ? 'collapse in' : 'collapse';?>" id="demo1">
								<a href="<?php echo $this->webroot; ?>projects/allprojects/" class="list-group-item <?php echo ($this->params['action'] == 'allprojects') ? 'active' : '';?>"> View All Listings</a>
								<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') { ?>
								<a href="<?php echo $this->webroot; ?>projects/currentproject1/" class="list-group-item <?php echo ($this->params['action'] == 'currentproject') ? 'active' : '';?>">Add Current Project</a>
								<?php }else{ ?>
								<a href="<?php echo $this->webroot; ?>projects/currentproject/" class="list-group-item <?php echo ($this->params['action'] == 'currentproject') ? 'active' : '';?>">Add Current Project</a>
								<?php } ?>
								<a href="<?php echo $this->webroot; ?>projects/pastproject/" class="list-group-item <?php echo ($this->params['action'] == 'pastproject') ? 'active' : '';?>">Add Past Project</a>

							</div>
							<?php //if ($this->Session->read('Auth.Websiteuser.id') == 45 || $this->Session->read('Auth.Websiteuser.id') == 597 || $this->Session->read('Auth.Websiteuser.id') == 447) { ?>
							
							<a href="#demo18" class="list-group-item list-group-item-success <?php //echo ($th) ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Project Pricing<i class="fa fa-caret-down"></i></a>


							<div class="dropdown <?php echo ($this->params['action'] == 'projectPricingLists' || $this->params['action'] == 'addProjectPricing') ? 'collapse in' : 'collapse';?>" id="demo18">
								<a href="<?php echo $this->webroot; ?>projects/projectPricingLists/" class="list-group-item <?php echo ($this->params['action'] == 'projectPricingLists') ? 'active' : '';?>"> All Pricing List</a>
								<a href="<?php echo $this->webroot; ?>projects/addProjectPricing/" class="list-group-item <?php echo ($this->params['action'] == 'addProjectPricing') ? 'active' : '';?>">Add New Pricing</a>

							</div>
							
							<?php //} ?>
							
						<?php } ?>

						<a href="#demo2" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['controller'] == 'properties') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Listed Properties<i class="fa fa-caret-down"></i></a>
						<div class="dropdown <?php echo ($this->params['controller'] == 'properties') ? 'collapse in' : 'collapse';?>" id="demo2">
							<a href="<?php echo $this->webroot; ?>properties/allproperties/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'allproperties') ? 'active' : '';?>"> View All Listings</a>
							<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') { ?>
								<a href="<?php echo $this->webroot; ?>properties/addproperty1/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'addproperty1') ? 'active' : '';?>">Add New Property</a>
							<?php }else{ ?>
							<!-- Old url before 7th jan 2019 change by akhilesh start-->
							
							<!--<a href="<?php //echo $this->webroot; ?>properties/addproperty/" id="disable" class="list-group-item <?php // echo ($this->params['action'] == 'addproperty') ? 'active' : '';?>">Add New Property</a> -->
							
							<!-- ----------Old url before 7th jan 2019 change by akhilesh End---------- -->
							
							<!-- New url after 7th jan 2019 change by akhilesh start-->
							
							<a href="<?php echo $this->webroot; ?>properties/addproperty2/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'addproperty2') ? 'active' : '';?>">Add New Property</a>
							
							<!-- New url after 7th jan 2019 change by akhilesh End-->
							
							<?php } ?>
							<a href="<?php echo $this->webroot; ?>properties/allresponses/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'allproperties') ? 'active' : '';?>">View Responses</a>
						</div>
						<?php if ($this->Session->read('Auth.Websiteuser.userrole') != '3') {
							?>
							<?php if ($this->Session->read('Auth.Websiteuser.userrole') != '2') { ?>
							<a href="#demo12" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'portfolio_addProperty' || $this->params['action'] == 'Portfolio_ViewProperty' || $this->params['action'] == 'Portfolio_ServiceRequest' || $this->params['action'] == 'Portfolio_ViewReport') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">My Portfolio<i class="fa fa-caret-down"></i></a>
							<div class="dropdown <?php echo ($this->params['action'] == 'portfolio_addProperty' || $this->params['action'] == 'Portfolio_ViewProperty' || $this->params['action'] == 'Portfolio_ServiceRequest' || $this->params['action'] == 'Portfolio_ViewReport') ? 'collapse in' : 'collapse';?>" id="demo12">
								<a href="<?php echo $this->webroot; ?>accounts/portfolio_addProperty/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'portfolio_addProperty') ? 'active' : '';?>">Add Property</a>
								<a href="<?php echo $this->webroot; ?>accounts/Portfolio_ViewProperty/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Portfolio_ViewProperty') ? 'active' : '';?>">View Property</a>
								<a href="<?php echo $this->webroot; ?>accounts/Portfolio_ServiceRequest/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Portfolio_ServiceRequest') ? 'active' : '';?>">Request Service</a>
								<a href="<?php echo $this->webroot; ?>accounts/Portfolio_ViewReport/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Portfolio_ViewReport') ? 'active' : '';?>">View Service</a>
							</div>
							

							<a href="#demo15" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'Property_AddProperty' || $this->params['action'] == 'Property_ViewProperty' || $this->params['action'] == 'property_ServiceRequest' || $this->params['action'] == 'Porperty_ViewService') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">My Managed Properties<i class="fa fa-caret-down"></i></a>
							<div class="dropdown <?php echo ($this->params['action'] == 'Property_AddProperty' || $this->params['action'] == 'Property_ViewProperty' || $this->params['action'] == 'property_ServiceRequest' || $this->params['action'] == 'Porperty_ViewService') ? 'collapse in' : 'collapse';?>" id="demo15">
								<a href="<?php echo $this->webroot; ?>accounts/Property_AddProperty/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Property_AddProperty') ? 'active' : '';?>">Add Property</a>
								<a href="<?php echo $this->webroot; ?>accounts/Property_ViewProperty/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Property_ViewProperty') ? 'active' : '';?>">View Property</a>
								<a href="<?php echo $this->webroot; ?>accounts/property_ServiceRequest/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'property_ServiceRequest') ? 'active' : '';?>"> Request Service</a>
								<a href="<?php echo $this->webroot; ?>accounts/Porperty_ViewService/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Porperty_ViewService') ? 'active' : '';?>">View Report</a>
							</div>

							<a href="#demo9" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'AsstBuyRent_View_Requirement' || $this->params['action'] == 'AsstBuyRent_AddRequirement') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Assisted Buy/Rent<i class="fa fa-caret-down"></i></a>
							<div class="dropdown <?php echo ($this->params['action'] == 'AsstBuyRent_View_Requirement' || $this->params['action'] == 'AsstBuyRent_AddRequirement') ? 'collapse in' : 'collapse';?>" id="demo9">
								<a href="<?php echo $this->webroot; ?>accounts/AsstBuyRent_View_Requirement/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'AsstBuyRent_View_Requirement') ? 'active' : '';?>"> View Requirements</a>
								<a href="<?php echo $this->webroot; ?>accounts/AsstBuyRent_AddRequirement/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'AsstBuyRent_AddRequirement') ? 'active' : '';?>">Add Requirements</a>

							</div>

							<a href="#demo10" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'AsstSellRent_View_Response' || $this->params['action'] == 'asstSellRent_servicerequest') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Assisted Sell/Rent<i class="fa fa-caret-down"></i></a>
							<div class="dropdown <?php echo ($this->params['action'] == 'AsstSellRent_View_Response' || $this->params['action'] == 'asstSellRent_servicerequest') ? 'collapse in' : 'collapse';?>" id="demo10">
								<a href="<?php echo $this->webroot; ?>accounts/AsstSellRent_View_Response/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'AsstSellRent_View_Response') ? 'active' : '';?>"> View Requirements</a>
								<a href="<?php echo $this->webroot; ?>accounts/asstSellRent_servicerequest/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'asstSellRent_servicerequest') ? 'active' : '';?>">Add Requirements</a>
							</div>
							
				 <!-- <a href="<?php echo $this->webroot; ?>accounts/research_report" id="disable" class="list-group-item list-group-item-success" data-parent="#MainMenu">Research Report</a>-->
							<a href="<?php echo $this->webroot; ?>accounts/order_history" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'order_history') ? 'active' : 'collapsed';?>" data-parent="#MainMenu">Order History</a>
							<a href="<?php echo $this->webroot; ?>accounts/transactions" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'transactions') ? 'active' : 'collapsed';?>" data-parent="#MainMenu">My Transactions </a>
							<?php } ?>
						<?php } ?>
						
						<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3' || $this->Session->read('Auth.Websiteuser.userrole') == '2') {
							?>	
							<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') { ?>
								<a href="#demo99" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'add_sales_employees1' || $this->params['action'] == 'add_broker_employees1' || $this->params['action'] == 'add_broker_employees' || $this->params['action'] == 'broker_employees_lists' || $this->params['action'] == 'sales_employees_lists') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Add and Search User<i class="fa fa-caret-down"></i></a>
							<?php } ?>
							<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '2') { ?>
								<a href="#demo99" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'add_sales_employees1' || $this->params['action'] == 'add_broker_employees' || $this->params['action'] == 'broker_employees_lists' || $this->params['action'] == 'sales_employees_lists') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Add and List Of User<i class="fa fa-caret-down"></i></a>
							<?php } ?>
							<div class="dropdown <?php echo ($this->params['action'] == 'add_sales_employees1' || $this->params['action'] == 'add_broker_employees1' ||$this->params['action'] == 'add_broker_employees' || $this->params['action'] == 'broker_employees_lists' || $this->params['action'] == 'sales_employees_lists') ? 'collapse in' : 'collapse';?>" id="demo99">
								
								<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') { ?>
								
								<?php //if ($this->Session->read('Auth.Websiteuser.id') == 45) { ?>
									<!--<a href="<?php echo $this->webroot; ?>accounts/add_sales_employees1" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'add_sales_employees1') ? 'active' : '';?>">Add Sales Employees</a>
									<a href="<?php echo $this->webroot; ?>accounts/add_broker_employees1" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'add_broker_employees1') ? 'active' : '';?>"> Add Brokers</a>-->
								<?php //}else{ ?>
									<a href="<?php echo $this->webroot; ?>accounts/add_sales_employees1/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'add_sales_employees1') ? 'active' : '';?>">Add Sales Employees</a>
									<a href="<?php echo $this->webroot; ?>accounts/sales_employees_lists/" id="disable" class="list-group-item">List of Sales Employees</a>
									<a href="<?php echo $this->webroot; ?>accounts/add_broker_employees1/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'add_broker_employees1') ? 'active' : '';?>"> Add Brokers</a>
								<?php //} ?>
								<a href="<?php echo $this->webroot; ?>accounts/broker_employees_lists/" id="disable" class="list-group-item">List of Brokers</a>
								<?php } ?>
								
								<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '2') { ?>
								<a href="<?php echo $this->webroot; ?>accounts/add_sales_employees_broker/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'add_sales_employees_broker') ? 'active' : 'collapsed';?>">Add Sales Employees</a>
								
								<a href="<?php echo $this->webroot; ?>accounts/sales_employees_lists_broker/" id="disable" class="list-group-item">List of Sales Employees</a>
								
								<?php } ?>
							</div>
							
							<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '2') { ?>
								<a href="#brokerpanel2" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'builder_lists_broker' || $this->params['action'] == 'myProjects' || $this->params['action'] == 'myInventories') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Builder and Projects<i class="fa fa-caret-down"></i></a>
								<div class="dropdown <?php echo ($this->params['action'] == 'builder_lists_broker' || $this->params['action'] == 'myProjects' || $this->params['action'] == 'myInventories') ? 'collapse in' : 'collapse';?>" id="brokerpanel2">								
									<a href="<?php echo $this->webroot; ?>accounts/builder_lists_broker/" id="disable" class="list-group-item">Project & Price Calculator</a>								
								    <a href="<?php echo $this->webroot; ?>accounts/myProjects/" id="disable" class="list-group-item">Channel Partnership Invite</a>
								    <a href="<?php echo $this->webroot; ?>accounts/myInventories/" id="disable" class="list-group-item">Inventories shared</a>
								</div>								
							<?php } ?>
							
							<?php //echo $this->params['action']; ?>
							<?php //if ($this->Session->read('Auth.Websiteuser.id') == 45 || $this->Session->read('Auth.Websiteuser.id') == 141) { ?>
								<a href="#p-share" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'price_calculator_list_brokers' || $this->params['action'] == 'price_calculator_list_employees' || $this->params['action'] == 'shareListProjects' || $this->params['action'] == 'shareMessageAppusersAll' || $this->params['action'] == 'share_projects_form') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Project Details Share<i class="fa fa-caret-down"></i></a>
								<div class="dropdown <?php echo ($this->params['action'] == 'price_calculator_list_brokers' || $this->params['action'] == 'price_calculator_list_employees' || $this->params['action'] == 'shareListProjects' || $this->params['action'] == 'shareMessageAppusersAll' || $this->params['action'] == 'share_projects_form') ? 'collapse in' : 'collapse';?>" id="p-share">
								
										<a href="<?php echo $this->webroot; ?>accounts/projects/share-list-projects/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'shareListProjects') ? 'active' : '';?>">Invite - Channel Partner</a>
										<a href="<?php echo $this->webroot; ?>accounts/projects/share-message-appusers-all/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'shareMessageAppusersAll') ? 'active' : '';?>">Message To App Users</a>
										<?php if ($this->Session->read('Auth.Websiteuser.id') == 45) { ?>
											<a href="<?php echo $this->webroot; ?>accounts/share_projects_form/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'share_projects_form') ? 'active' : '';?>"> Assign Price Calculator Service</a>
											<a href="<?php echo $this->webroot; ?>accounts/price_calculator_list_brokers/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'price_calculator_list_brokers') ? 'active' : '';?>"> Price Calculator - Brokers</a>
											<a href="<?php echo $this->webroot; ?>accounts/price_calculator_list_employees/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'price_calculator_list_employees') ? 'active' : '';?>"> Price Calculator - Employees</a>
											
										<?php }else{ ?>
											<a href="<?php echo $this->webroot; ?>accounts/share_projects_form/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'share_projects_form') ? 'active' : '';?>"> Assign Price Calculator Service</a>
											<a href="<?php echo $this->webroot; ?>accounts/price_calculator_list_brokers/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'price_calculator_list_brokers') ? 'active' : '';?>"> Price Calculator - Brokers</a>
											<a href="<?php echo $this->webroot; ?>accounts/price_calculator_list_employees/" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'price_calculator_list_employees') ? 'active' : '';?>"> Price Calculator - Employees</a>
										<?php } ?>
										
										
									
									
								</div>
								<?php //if($this->Session->read('Auth.Websiteuser.id') == 45) { ?>
								<a href="#i-share" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'share_projects_inventory_view' || $this->params['action'] == 'addInventory' || $this->params['action'] == 'getInventories' || $this->params['action'] == 'inventoriesHistoryBuilderPanel' || $this->params['action'] == 'inventoriesHistorySalesBuilderPanel' || $this->params['action'] == 'inventory/view_inventory') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Inventory Management<i class="fa fa-caret-down"></i></a>
								<div class="dropdown <?php echo ($this->params['action'] == 'share_projects_inventory_view' || $this->params['action'] == 'addInventory' || $this->params['action'] == 'getInventories' || $this->params['action'] == 'inventoriesHistoryBuilderPanel' || $this->params['action'] == 'inventoriesHistorySalesBuilderPanel') ? 'collapse in' : 'collapse';?>" id="i-share">
										<a href="<?php echo $this->webroot; ?>accounts/share_projects_inventory_view/" id="disable" class="list-group-item inventory-management"> Configure Project's Inventory</a>										
										<a href="<?php echo $this->webroot; ?>accounts/inventory/add/" id="disable" class="list-group-item">Add Inventory</a>
										<a href="<?php echo $this->webroot; ?>accounts/inventory/" id="disable" class="list-group-item">Modify Inventory</a>
										<a href="<?php echo $this->webroot; ?>accounts/inventoriesHistoryBuilderPanel/" id="disable" class="list-group-item">Share History - Brokers</a>
										<a href="<?php echo $this->webroot; ?>accounts/inventoriesHistorySalesBuilderPanel/" id="disable" class="list-group-item">Share History - Sales</a>
								</div>
								
								<a href="#i-share11" id="disable" class=" servicemenu list-group-item list-group-item-success <?php echo ($this->params['action'] == 'inventorySummeryReport' || $this->params['action'] == 'calculatorChannelPartner' || $this->params['action'] == 'inventoryStatusReport'  || $this->params['action'] == 'calculatorSalesEmployees') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu11">Reports<i class="fa fa-caret-down"></i></a>
									<div class="dropdown <?php echo ($this->params['action'] == 'inventorySummeryReport' || $this->params['action'] == 'calculatorChannelPartner' || $this->params['action'] == 'inventoryStatusReport' ||  $this->params['action'] == 'calculatorSalesEmployees') ? 'collapse in' : 'collapse';?>" id="i-share11">
										<a href="<?php echo $this->webroot; ?>accounts/calculatorSalesEmployees/" id="disable" class="list-group-item inventory-management1133"> Calculator-Sales Employees</a>          
										<a href="<?php echo $this->webroot; ?>accounts/calculatorChannelPartner/" id="disable" class="list-group-item">Calculator-Channel Partner</a>
										<a href="<?php echo $this->webroot; ?>accounts/inventorySummeryReport/" id="disable" class="list-group-item">Inventory-Summary Report</a>
									</div>
									
								<?php //}?>
								
								
							<?php //} ?>
						<?php } ?>
						
						<?php if ($this->Session->read('Auth.Websiteuser.id') == 45) { ?>
								<a href="#leadblock" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'depositLeadBuilder' || $this->params['action'] == 'listsLeadsBuilder' || $this->params['action'] == 'shareLeadsBuilder1') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Lead Management<i class="fa fa-caret-down"></i></a>
								<div class="dropdown <?php echo ($this->params['action'] == 'depositLeadBuilder' || $this->params['action'] == 'listsLeadsBuilder' || $this->params['action'] == 'shareLeadsBuilder1') ? 'collapse in' : 'collapse';?>" id="leadblock">								
									<a href="<?php echo $this->webroot; ?>leads/depositLeadBuilder/" id="disable" class="list-group-item">Add New Lead</a>								
								    <a href="<?php echo $this->webroot; ?>leads/listsLeadsBuilder/" id="disable" class="list-group-item">All Leads</a>
								    <a href="<?php echo $this->webroot; ?>leads/shareLeadsBuilder1/" id="disable" class="list-group-item">Share New Leads</a>
									<a href="<?php echo $this->webroot; ?>leads/reassignLeadsBuilder/" id="disable" class="list-group-item">Reassign Leads</a>
									<a href="<?php echo $this->webroot; ?>leads/uploadLeads/" id="disable" class="list-group-item">Upload Leads</a>
								</div>								
							<?php } ?>
						
						
						<a href="#p-menu" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'change_profile' || $this->params['action'] == 'builderapproval') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Profile<i class="fa fa-caret-down"></i></a>
						<div class="dropdown <?php echo ($this->params['action'] == 'change_profile' || $this->params['action'] == 'builderapproval') ? 'collapse in' : 'collapse';?>" id="p-menu">
							<a href="<?php echo $this->webroot; ?>accounts/change_profile" class="list-group-item" data-parent="#MainMenu">Edit Profile </a>
							<?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') {
								?>	
								<a  href="<?php echo $this->webroot; ?>accounts/builderapproval/" class="list-group-item" data-parent="#MainMenu">Builder Organization Profile</a>
							<?php } ?>
						</div>
						
						
					</div>
				</div>
			</ul>
		</div>
	</div>
<?php //echo $this->element('admin-footer');?>
