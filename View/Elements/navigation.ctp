<nav class="drawer-nav" role="navigation">
	<ul class="nav" id="side-menu">
		<div id="MainMenu">
			<div class="list-group">
				<a href="<?php echo $this->webroot; ?>" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'display') ? 'active' : '';?>">Home</a>
				<a href="#demoaa" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'emi_calc' || $this->params['action'] == 'rentvbuy_calc') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Calculators<i class="fa fa-caret-down"></i></a>
				<div class="dropdown <?php echo ($this->params['action'] == 'emi_calc' || $this->params['action'] == 'rentvbuy_calc') ? 'in' : 'collapse';?>" id="demoaa">
					<a href="<?php echo $this->webroot; ?>emi-calc/" class="list-group-item <?php echo ($this->params['action'] == 'emi_calc/') ? 'active' : '';?>"> EMI Calculator</a>
					<a href="<?php echo $this->webroot; ?>rentvbuy-calc/" class="list-group-item <?php echo ($this->params['action'] == 'rentvbuy_calc/') ? 'active' : '';?>">Rent Vs Buy Analysis</a>
				</div>
				<a href="#demobb" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'research_report/' || $this->params['action'] == 'property_management/' || $this->params['action'] == 'portfolio_management/' || $this->params['action'] == 'advisory/') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Services<i class="fa fa-caret-down"></i></a>
				<div class="dropdown <?php echo ($this->params['action'] == 'research_report' || $this->params['action'] == 'property_management' || $this->params['action'] == 'portfolio_management' || $this->params['action'] == 'advisory') ? 'in' : 'collapse';?>" id="demobb">
					<a href="<?php echo $this->webroot; ?>research-reports/" class="list-group-item <?php echo ($this->params['action'] == 'research_report/') ? 'active' : '';?>"> Research Reports</a>
					<a href="<?php echo $this->webroot; ?>property-management/" class="list-group-item <?php echo ($this->params['action'] == 'property_management/') ? 'active' : '';?>">Property Management</a>
					<a href="<?php echo $this->webroot; ?>portfolio-management/" class="list-group-item <?php echo ($this->params['action'] == 'portfolio_management/') ? 'active' : '';?>">Portfolio Management</a>
					<a href="<?php echo $this->webroot; ?>advisory/" class="list-group-item <?php echo ($this->params['action'] == 'advisory') ? 'active' : '';?>">Assisted Buying/Selling</a>
				</div>
				<a href="<?php echo $this->webroot; ?>pricing/" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'pricing/') ? 'active' : 'collapsed';?>">Pricing</a>
				<a href="<?php echo $this->webroot; ?>blog" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'blog') ? 'active' : 'collapsed';?>">Blog</a>
				<?php if (!empty($this->Session->read('Auth.Websiteuser'))) { ?>
				<a href="<?php echo $this->webroot; ?>Websiteusers/logout" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'logout') ? 'active' : 'collapsed';?>">Logout</a>
				<?php } ?>	
			</div>
		</div>
	</ul>
</nav>
