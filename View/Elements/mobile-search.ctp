<div class="mobilesearch" id="mobilesearch">
    <a href="javascript:void(0);" class="btn-searchform">
        <i class="fa fa-search" aria-hidden="true"></i>
    </a>
	<div class="searchform" style="display:none;">
		<div class="top-searchs">
			<?php echo $this->Form->create(null, array('url' => '/searchList', 'class' => '', 'type' => 'get'));?>
				<div class="row">
					<div class="col-md-6 col-xs-12 col-md-offset-3 col-xs-offset-0">
						<div class="row">
							<div class="form-group col-md-2">
						<?php 
							$default_location = '';
							if(isset($this->params->query['default_location'])) {
								$default_location = $this->params->query['default_location'];
							}
							
							echo $this->Form->input('default_location', array(
							'label' 	=> false, 
							'div' 		=> false, 
							'hidden'	=> true,
							'id' 		=> 'defaultLocation_mobile',
							'value'		=> $default_location
							));
							
							$property_default = 'buy';
							$property_type_default = [];
							
							if(isset($this->params->query['property_for'])) {
								$property_default = $this->params->query['property_for'];
							}
							
							if(isset($this->params->query['property_type'])) {
								$property_type_default = $this->params->query['property_type'];
							}
							
							$property_for = array('rent' => 'Rent', 'buy' => 'Buy', 'project' => 'New project');
							echo $this->Form->input('property_for', array(
							'options' => $property_for, 
							'default' => $property_default, 
							'class' => 'selectpicker show-tick', 
							'label' => false, 
							'div' => false, 
							'id' =>'inputpropertyfor'
							));
						?>
					</div>
					<div class="form-group col-md-4 property-type">
						<?php 
							$options = array(
							'All Residential' => array(
							'Apartment' 				=> 'Apartment',
							'Studio Apartment' 			=> 'Studio Apartment',
							'Residential Land' 			=> 'Residential Land',
							'Independent Builder Floor' => 'Independent Builder Floor',
							'Independent House' 		=> 'Independent House',
							'Independent Villa' 		=> 'Independent Villa',
							'Farm House'				=> 'Farm House'
							),
							'All Commercial' => array(
							'Commercial Office Space' 	=> 'Commercial Office Space',
							'Office In IT Park' 		=> 'Office In IT Park',
							'Commercial Shop' 			=> 'Commercial Shop',
							'Commercial Showroom' 		=> 'Commercial Showroom',
							'Commercial Land' 			=> 'Commercial Land',
							'Industrial Land' 			=> 'Industrial Land',
							'Agricultural Land' 		=> 'Agricultural Land',
							'Factory' 					=> 'Factory',
							'Ware House' 				=> 'Ware House'
							)
							);
							echo $this->Form->input('property_type[]', array(
							'options' => $options, 
							'default' => $property_type_default, 
							'class' => 'selectpicker show-tick', 
							'label' => false, 
							'div' => false, 
							'id' =>'inputpropertytype', 
							'multiple'
							));
						?>
					</div>
					<div class="form-group col-md-4">
						<?php
							$property_name = '';
							if(isset($this->params->query['property_name'])) {
								$property_name = $this->params->query['property_name'];
							}
							
							echo $this->Form->input('property_name', array(
							'class' => 'form-control', 
							'value' => $property_name,
							'label' => false, 
							'div' => false, 
							'placeholder' => 'Search for Property'
							));
						?>
					</div>
					<div class="form-group col-md-2">
						<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search1</button>
					</div>
					</div>
					</div>
				</div>
			</form>
		
		</div>
	</div>
    <script>
	$(document).ready(function(){
		$(".btn-searchform").click(function(){
			$(".searchform").fadeToggle(500);
		});
	});
	</script>
</div>
