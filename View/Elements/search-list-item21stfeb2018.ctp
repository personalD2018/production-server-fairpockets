<?php   
	if(isset($result) && $result->getNumFound()) {
		echo '<div class="alert alert-info">'.$result->getNumFound().' Properties found for your search.</div>';
		foreach($result as $item):
		if($item['transaction_type'] == 'Sell'){
			$item_transaction_type = 'Sale';
		}else{
			$item_transaction_type = $item['transaction_type'];
		}
		$title = (strpos($item['configure'], 'BHK') !== false ? $item['configure'] : $item['configure'] . 'BHK') .' '. $item['property_type'] . ' for '. $item_transaction_type;	
		
		$address = (!empty($item['sublocality3'])) ? $item['sublocality3'].',&nbsp;' : '';
		$address .= (!empty($item['sublocality2'])) ? $item['sublocality2'].',&nbsp;' : '';
		$address .= (!empty($item['sublocality1'])) ? $item['sublocality1'].',&nbsp;' : '';
		$address .= (!empty($item['city'])) ? $item['city'] : '';
		$url_address  = (!empty($item['sublocality1'])) ? $item['sublocality1'].'&nbsp;' : '';	
	?>
	
	<style>
	figure img{height:180px !important;}
	</style>
	
	<div class="search-box">
		<div class="row">
			<div class="col-md-3">
				<figure>
					<figure>
					<?php $firstpic = $this->Number->getPropertyPic($item['id']);
					//echo '<pre>';print_r($firstpic);
					if(!empty($firstpic) && $firstpic != ''){
					$firstpic1 = $firstpic[0]['PropertyPic']['pic'];
					}else{
						$firstpic1 = 'upload/default.png';
					}
						//echo '<pre>'; print_r($firstpic1);
					?>
					<?php if($item['posted_by'] == 'Owner' || $item['posted_by'] == 'Broker'){ ?>
						<?= $this->Html->link($this->html->image('/'.$firstpic1, array('alt'=>$title)), array("controller" => false,"action" => "/searchListDetail", base64_encode($item->id)), array('escape' => false, 'title' => $title, 'target' => '_blank'));?>
					<?php }else{ ?>
						<?= $this->Html->link($this->html->image('/'.$firstpic1, array('alt'=>$title)), array("controller" => false,"action" => "/builderPropertyDetails", base64_encode($item->id)), array('escape' => false, 'title' => $title, 'target' => '_blank'));?>
					<?php } ?>
				</figure>
				</figure>
			</div>
			<div class="col-md-9">
				<div class="row">
					
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="property-info">
							<div class="heading">								
								<?php if($item['posted_by'] == 'Owner' || $item['posted_by'] == 'Broker'){ ?>
									<?php
									echo $this->Html->link($title, array("controller"=>false,"action" => "/searchListDetail", base64_encode($item->id)), array('escapeTitle' => false, 'title' => $title, 'target' => '_blank'));
									?>
								<?php }else{ ?>
									<?php
									echo $this->Html->link($title, array("controller"=>false,"action" => "/builderPropertyDetails", base64_encode($item->id)), array('escapeTitle' => false, 'title' => $title, 'target' => '_blank'));
									?>
								<?php } ?>
							</div>
							<div class="location"><?php echo $address;?></div>
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-12">
						<div class="price property-info">
							<?php echo (!empty($item['offer_price'])) ? $this->Number->format($item['offer_price']) : $item['market_price'];?> 
							<?php if($item['posted_by'] == 'Owner' || $item['posted_by'] == 'Broker'){ ?>
							<?php if(!empty($item['discount'])):?><span>-<?php echo ceil(($item['discount']/$item['market_price']) * 100);?>%</span><?php endif;?>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-7 col-sm-7">
						<div class="property-info">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-7"><strong>Super built up area</strong><span class="pull-right"> : </span></div>
								<div class="col-md-6 col-sm-6 col-xs-5"><?php echo ($item['super_builtup_area'] ? $item['super_builtup_area'] : $item['builtup_area']);?> Sq.ft</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-7"><strong>Project</strong><span class="pull-right"> : </span></div>
								<div class="col-md-6 col-sm-6 col-xs-5"><?php echo (!empty($item['project_name'])) ? $item['project_name'] : 'N/A';?></div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-7"><strong>Property status</strong><span class="pull-right"> : </span></div>
								<div class="col-md-6 col-sm-6 col-xs-5"><?php echo (!empty($item['availability'])) ? $item['availability'] : 'N/A';?></div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-7"><strong>Transaction Type</strong><span class="pull-right"> : </span></div>
								<div class="col-md-6 col-sm-6 col-xs-5"><?php echo (!empty($item['sale_type'])) ? $item['sale_type'] : 'N/A';?></div>
							</div>
							
						</div>
					</div>
					<div class="col-md-5 col-sm-5">
						<div class="discription property-info">
							<p><?= (strpos($item->configure, 'BHK') !== false ? $item->configure : $item->configure . 'BHK') .' '. $item->property_type; ?> is available for <?= $item->transaction_type; ?> in <?php echo $url_address; ?> one of the best society in <?php echo (!empty($item['city'])) ? $item['city'] : 'N/A';?> 
							<?php if($item['posted_by'] == 'Owner' || $item['posted_by'] == 'Broker'){ ?>
								<?php echo $this->Html->link('Read more...', array("controller"=>false,"action" => "/searchListDetail", base64_encode($item['id'])), array('escapeTitle' => false, 'title' => 'Read More...'));?></p>
							<?php }else{ ?>
								<?php echo $this->Html->link('Read more...', array("controller"=>false,"action" => "/builderPropertyDetails", base64_encode($item['id'])), array('escapeTitle' => false, 'title' => 'Read More...'));?></p>
							<?php } ?>
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="property-info">
							<?php 
								$contact = $item['posted_by'];
								if($contact == 'Owner') {
									$contact = 'Contact Owner';
									} else if($contact == 'Broker') {
									$contact = 'Contact Dealer';
									} else {
									$contact = 'Contact Builder';
								}
							?>
							<p>
							<?php
								$userid= $this->Number->getPropertyUserId($item['id']);
					//echo '<pre>';print_r($userid);
							?>
								<button type="button" class="button1 btn" data-title="<?= $contact;?>" data-propid="<?=$item->id;?>" data-userid="<?=$userid;?>" data-toggle="modal" data-target="#contact-biulder"> <?= $contact;?></button>
							</p>
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-12 post">
						<div class="property-info">
							<p>Post Date :  <strong><?php echo date('d-M-Y', strtotime($item['post_date']));?></strong><br>
							Posted By :  <strong><?php echo (!empty($item['posted_by_user_name'])) ? $item['posted_by_user_name'] : 'N/A';?></strong></p>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
	<?php 
		endforeach; 
		
		echo $this->Number->paginate($limit, $start, $result->getNumFound(), ceil($result->getNumFound()/$limit), '/searchList?'.http_build_query($this->params->query));
		} else {
		echo '<div class="alert alert-danger">Oh Snap! Zero Results found for your search.</div>';
	}
	?>
<div class="displayNone">
	<div class="society-list">
		<?php
			if(isset($result)) {		
				$societies = $result->getFacetSet()->getFacet('Society');
				$society_items = '';
				$index = 1;
				foreach($societies as $name => $count) {
					$society_items .= '<li>
							<input id="society_list_'.$index.'" class="fil-checkbox" name="society-filters" type="checkbox" value="'.$name.'">
							<label for="society_list_'.$index.'" class="fil-checkbox-label">'.ucwords($name).'</label>
						</li>';
					$index++;
				}
				
				echo $society_items;
			}
		?>
	</div>
	<div class="locality-list">
		<?php
				if(isset($result)) {
					$localities = $result->getFacetSet()->getFacet('Locality');
					$locality_items = '';
					$index = 1;
					foreach($localities as $name => $count) {
						$locality_items .= '<li>
								<input id="locality_list_'.$index.'" class="fil-checkbox" name="locality-filters" type="checkbox" value="'.$name.'">
								<label for="locality_list_'.$index.'" class="fil-checkbox-label">'.ucwords($name).'</label>
							</li>';
						$index++;
					}
					
					echo $locality_items;
				}
		?>
	</div>
</div>
