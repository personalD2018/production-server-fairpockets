<style>
.resend-code {
	position: absolute;
    right: 0;
    vertical-align: middle;
    line-height: 40px;
    cursor: pointer;
    color: #29333d;
    font-weight: 600;
}
</style>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.loaderdiv{
	text-align:center;
}
</style>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dial">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <div class="modal-content">
            <div class="modal-body modalpadd">
                <div class="row">
                    <div class="col-md-6 hidden-xs hidden-sm">
                        <div class="modulsideback ">
                            <div class="modalbacklidiv">
                                <h3>Register with us and avail services like:</h3>
                                <div class="col-sm-12">
                                    <ul>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Post free properties
											</div>
										</li>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Get Free valuation of your property
											</div>
										</li>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Sell fast Top Rated Buyers &amp; Sellers
											</div>
										</li>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Access to research reports
											</div>
										</li>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Assisted Search
											</div>
										</li>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Property Management
											</div>
										</li>
                                        <li>
                                            <div class="sing-l-tet fl">
                                                <i class="fa fa-check" aria-hidden="true"></i> &nbsp;&nbsp;Portfolio Management
											</div>
										</li>
									</ul>
								</div>
								
							</div>
						</div>
					</div>
					
                    <div class="col-md-6 col-xs-12">
						<div class="loginsidediv">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#Login" data-toggle="tab" onclick="InitLoginPane();" >Login</a>
								</li>
								
								<li>
									<a href="#Registration" data-toggle="tab" onclick="InitRegisterPane();" >Registration</a>
								</li>
							</ul>
							<!-- Nav tabs Ends-->
							<div class="tab-content" id="id_tab_panes">
								
								<div class="tab-pane active" id="Login">
									
									<div id="id_login_form" >
										
										<h2>Sign <span>In</span></h2>
										
										<?php echo $this->Session->flash('auth'); ?>
										<!-- Login Form -->
										
										<?php
											echo $this->Form->create(
                                            'Websiteuser', array(
											'class' => 'form-horizontal fpform',
											'id' => 'login-form',
											'autocomplete' => 'off'
                                            )
											);
										?>
										
										<div class="form-group">
											<div class="col-sm-12">
												<!-- 
                                                    <input type="text" class="form-control" id="email1" placeholder="Email" />
                                                    <div class="error has-error" style="display:block;">Box must be filled out</div> 
												-->
												<?php
													echo $this->Form->input(
                                                    'email', array(
													'label' => false,
													'type' => 'text',
													'class' => 'form-control',
													'placeholder' => 'Email'
                                                    )
													);
												?>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
                                                <!-- <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" /> -->
												<?php
													echo $this->Form->input(
                                                    'password', array(
													'label' => false,
													'type' => 'password',
													'class' => 'form-control',
													'placeholder' => 'Password',
													'title' => 'Password is required'
                                                    )
													);
												?>														
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
												<label class="form-error-label" id="id_lform-error-label" ></label>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-6">
												<?php
													echo $this->Form->input(
                                                    'remember_me', array(
													'label' => false,
													'div' => false,
													'type' => 'checkbox',
													'class' => 'checkbox-custom',
													'id' => 'Remember'
                                                    )
													);
												?>		
												
												<label for="Remember" class="checkbox-custom-label">Remember Me</label>
											</div>
											
											<div class="col-sm-6 text-right">
												<a href="#Forgotpassword" class="color-red" data-toggle="tab" onclick="document.getElementById('id_forgotpass_form').style.display = 'inline';">Forgot your password?</a>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-6">
												<button type="submit" class="btn btn-primary btn-red" onclick="">
													<i class="fa fa-sign-in"></i> &nbsp;Sign in
												</button>
											</div>
										</div>
										<!-- </form> -->
										<?php
											echo $this->Form->end();
										?>
										<!-- Login Form Ends -->
									</div>
								</div>
								
								
								<div class="tab-pane" id="Registration">
									
									<div class="text-center" id="id_register_succ" style="display:none;"></div> 
									
									<div id="id_register_form" >
										
										<h2>Sign <span>up</span></h2>
										
										<?php
											echo $this->Session->flash();
										?>
										
										<!-- Registration Form -->
										
										<?php
											echo $this->Form->create(
                                            'Websiteuser', array(
											'class' => 'form-horizontal fpform',
											'role' => 'form',
											'id' => 'register-form',
											'novalidate' => 'novalidate',
											'autocomplete' => 'off'
                                            )
											);
										?>
										
										<div class="form-group" style="">
											
											<div class="col-sm-12">
												
												<!-- <input id="usertype1" class="radio-custom" name="bachelor_all" type="radio"  checked="checked" onclick="SetCompanyOption(1);" > -->
												<!-- <label for="usertype1" class="radio-custom-label">Individual</label> -->
												<?php
													$options = array(
													'1' => 'Individual',
													'2' => 'Agent',
													'3' => 'Builder'
													);
													
													$attributes = array(
													'legend' => false,
													'class' => 'radio-custom',
													'label' => array('class' => 'radio-custom-label'),
													'default' => '1',
													'onclick' => 'SetCompanyOption(this.value)'
													);
													
													echo $this->Form->radio('userrole', $options, $attributes);
												?>
												
												
												<?php
													/* 	
														$options = array('1' => 'Individual');
														$attributes = array(
														'class'   => 'radio-custom',
														'label'   => array ('class' => 'radio-custom-label'),
														'legend'  => false,
														'value'   => '1',
														'onclick' => 'SetCompanyOption(this.value)'
														);
														echo $this->Form->radio('userrole', $options, $attributes);
														
														$options = array('2' => 'Agent');
														$attributes = array(
														'class'   => 'radio-custom',
														'label'   => array ('class' => 'radio-custom-label'),
														'legend'  => false,
														'value'   => '2',
														'onclick' => 'SetCompanyOption(this.value)'
														);
														echo $this->Form->radio('userrole', $options, $attributes);
														
														$options = array('3' => 'Builder');
														$attributes = array(
														'class'   => 'radio-custom',
														'label'   => array ('class' => 'radio-custom-label'),
														'legend'  => false,
														'value'   => '3',
														'onclick' => 'SetCompanyOption(this.value)'
														);
														echo $this->Form->radio('userrole', $options, $attributes);
													*/
													
													/*
														echo $this->form->radio(
														'userRole',
														array(
														'1' => 'Individual',
														'2' => 'Agent',
														'3' => 'Builder'
														),
														array(
														'name'    => 'userRole',
														'class'   => 'radio-custom',
														'label'   => array ('class' => 'radio-custom-label'),
														'legend'  => false,
														'onclick' => 'SetCompanyOption()',
														'selected' => '1'
														)
														);
													*/
												?>
												
											</div>
											
										</div>
										
										
										
										
										<div class="form-group">
											<div class="col-sm-12">
                                                <!-- <input type="text" class="form-control" placeholder="Name" /> -->
												<?php
													echo $this->Form->input(
                                                    'username', array(
													'label' => false,
													'type' => 'text',
													'class' => 'form-control',
													'placeholder' => 'Name'
                                                    )
													);
												?>														
											</div>
										</div>
										
										<div id="id_group_reg_company" class="form-group" style="display:none;" >
											<div id="id_reg_company" class="col-sm-12">
                                                <!-- <input type="text" class="form-control" placeholder="Company Name" /> -->
												<?php
													/* This will be dynamically added
														echo $this->Form->input(
														'userOrganizatioName',
														array(
														'label' => false,
														'type'  => 'text',
														'class' => 'form-control',
														'id'    => 'userOrgName',
														'name'  => 'userOrgName',
														'placeholder' => 'Organization Name'
														
														)
														);
													*/
												?>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
                                                <!-- <input type="text" class="form-control" id="email" name="email" placeholder="Email"/> -->
												<?php
													echo $this->Form->input(
	                                                    'email', array(
															'label' => false,
															'type' => 'text',
															'id'	=> 'reg_email',
															'class' => 'form-control',
															'placeholder' => 'Email'
	                                                    )
													);
												?>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
                                                <!-- <input type="text" class="form-control" id="mobile" placeholder="Mobile" /> -->
												<?php
													echo $this->Form->input(
                                                    'usermobile', array(
													'label' => false,
													'type' => 'text',
													'class' => 'form-control',
													'placeholder' => 'Mobile'
                                                    )
													);
												?>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
                                                <!-- <input type="password" class="form-control" id="password" placeholder="Password" /> --> 
												<?php
													echo $this->Form->input(
                                                    'password', array(
													'label' => false,
													'type' => 'password',
													'class' => 'form-control',
													'placeholder' => 'Password'
                                                    )
													);
												?>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
												<label class="form-error-label" id="id_rform-error-label" ></label>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
												<span >By Signing up, I agree with the <a class="color-red" href="/terms-conditions/">T&C </a></span>
											</div>
										</div>
										
										<div class="form-group signupbtn">
											<div class="col-sm-12">
												<button type="submit" class="btn btn-primary btn-red">
													<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Sign up
												</button>
												
												<!-- <a href="#SuccessMsg" class="btn btn-primary " data-toggle="tab">SuccessMSG</a> -->
											</div>
										</div>
										
										<!-- </form> -->
										<?php
											echo $this->Form->end();
										?>	
										<!-- Registration Form Ends-->
										
									</div>
								</div>
								
								
								<div class="tab-pane" id="Forgotpassword">
									
									<div class="text-center" id="id_forgotpass_succ" style="display:none;" >
									</div> 
									<div class="loaderdiv" style="text-align:center; display:block;">
										<i class="fa fa-spinner fa-spin" style="font-size: 18px;color: green;"></i>
									</div>
									<div id="id_forgotpass_form">
										
										<h2>Forgot <span>Password</span></h2>
										
										<!-- Forgot Password Form -->
										
										<?php
											echo $this->Form->create(
                                            'Websiteuser', array(
											'class' => 'form-horizontal fpform',
											'id' => 'forgotpass-form'
                                            )
											);
										?>
										
										<div class="form-group">
											<div class="col-sm-12">
												<!-- < input type="text" class="form-control" id="email1" placeholder="Email" /> -->
												<?php
													echo $this->Form->input(
                                                    'email', array(
													'label' => false,
													'type' => 'text',
													'class' => 'form-control',
													'placeholder' => 'Email'
                                                    )
													);
												?>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-12">
												<label class="form-error-label" id="id_fform-error-label" ></label>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-6">
												<button type="submit" class="btn btn-primary btn-red">
													<i class="fa fa-paper-plane"></i> &nbsp;Send
												</button>
											</div>
											
											<div class="col-sm-6 text-right">
												<a href="#Login" class="color-red" onclick="InitLoginPane();"   data-toggle="tab">Login</a>
											</div>
										</div>
										
										<!-- </form> -->
										<?php
											echo $this->Form->end();
										?>
										<!-- Forgot Password Form Ends-->
										
									</div>
								</div>				
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	// Jquery Script for Validation of form fields    
	echo $this->Html->script('validate_1.9_jquery.validate.min');
	echo $this->Html->script('front/additional-methods.min');
?>	
<script>

	var $rgf = $('#id_register_form'), $wum = $('#WebsiteuserUsermobile'), $rgs = $('#id_register_succ');
	
	$('#myModal').on('hidden.bs.modal', function (e){
		var tabPanel = document.getElementById('id_tab_panes');
		if(( tabPanel.childNodes[1].className == 'tab-pane active' ) || ( tabPanel.childNodes[5].className == 'tab-pane active' )){
			InitLoginPane();
		}
		else{
			InitRegisterPane();
		}	
	});

	function showMesssage($data) {
		return '<div class="col-sm-12 status" style="padding: 10px;"><div class="alert alert-'+$data.cls+'">'+$data.msg+'</div></div>';
	}


	function resendOtp(elem) {

		if(confirm('Are you sure you want to resend OTP ?')) {
			//$(elem).prepend('<i class="fa fa-spinner fa-pulse fa-fw"></i>').attr('disabled', 'disabled');

			$rgs.find('.col-sm-12.status').remove();

			$.ajax({
				type: 'POST',
				url: '/websiteusers/resendOtp/',
				data: {
					'mobile' : $wum.val()
				},
				dataType :'json',
				success: function(data) {
					if(data.type == 'success' ) {
	                    $rgs.append(showMesssage({'cls':'success', 'msg':'OTP has been sent, Please verify OTP for registration.'}));
					} else {
	                    //$(elem).html('Resend').removeAttr('disabled');
	                    $rgs.append(showMesssage({'cls':'danger', 'msg':'Error while sending OTP, Please try again!.'}));
					}
				}
			});		
		}
	}
	
	function verifyOtp(elem) {
		$(elem).prepend('<i class="fa fa-spinner fa-pulse fa-fw"></i>').attr('disabled', 'disabled');

        $rgs.find('.col-sm-12.status').remove();

		$.ajax({
			type: 'POST',
			url: '/websiteusers/verifyOtp/',
			data: {
				'id' : $wum.val(),
				'otp'	: $('#otp_verify').val()
			},
			dataType :'json',
			success: function(data) {
				//$rgs.html('').hide();
				if(data.type == 'success' ) {
                    $rgs.append(showMesssage({'cls':'success', 'msg':'OTP has been verified, Please wait for registration.'}));
					UserRegistrationFormSubmit();
				} else {
                    $(elem).html('Verify').removeAttr('disabled');
                    $rgs.append(showMesssage({'cls':'danger', 'msg':'Invalid OTP, Please try again!.'}));
				}
			}
		});
	}
	
	function sendOTP() {
		
		$rgf.hide();

        $rgs.html('').append('<h5>A OTP has been sent on your mobile please verify.</h5><div class="row form-group"><div class="col-sm-12"><a class="col-sm-4 resend-code" onclick="resendOtp(this)">Resend Code</a><input name="otp_verify" class="form-control" placeholder="Verify OTP" maxlength="4" type="text" id="otp_verify" required="required"></div></div><div class="row form-group" style="margin-top: 20px;"><div class="col-sm-12"><button type="button" onclick="verifyOtp(this)" class="btn btn-primary col-sm-12 btn-red">Verify</button></div></div>').show();

		$.ajax({
			type: 'POST',
			url: '/websiteusers/sendOtp/',
			data: $rgf.find('form').serialize(),
			dataType :'json',
			success: function(data) {
				if(data.type != 'success' ) {
					$rgs.html('').hide();
					$rgf.show();
				}
			}
		});
	}
	
	//Function : To clear/set registration from fields when registration TAB is clicked
	function InitRegisterPane(){
		document.getElementById('id_register_form').style.display = 'inline' ;
		document.getElementById('register-form').reset();
		var validator = $( '#register-form' ).validate();
		validator.resetForm();
		$('.form-group input').removeClass('error');
		document.getElementById('id_rform-error-label').style.display = 'none';
		document.getElementById('id_register_succ').style.display = 'none' ;
	}

	//Function : To clear/set login from fields when registration TAB is clicked
	function InitLoginPane()
	{
		var tabPanel = document.getElementById('id_tab_panes');
		tabPanel.childNodes[1].className = 'tab-pane active';
		tabPanel.childNodes[5].className = 'tab-pane';
		document.getElementById('id_login_form').style.display = 'inline' ;
		document.getElementById('login-form').reset();
		document.getElementById('forgotpass-form').reset();
		var validator = $( '#login-form' ).validate();
		validator.resetForm();
		validator = $( '#forgotpass-form' ).validate();
		validator.resetForm();
		$('.form-group input').removeClass('error');
		document.getElementById('id_lform-error-label').style.display = 'none';
		document.getElementById('id_fform-error-label').style.display = 'none';
		document.getElementById('id_forgotpass_succ').style.display = 'none' ;
	}		
	
	//Function : To set company name option for register form
	//Company Name Mandatory - if user is agent(usr_type - 2) / builder( usr_type - 3)
	//Company Name Optional - if user is individual(usr_type - 1)		
	function SetCompanyOption( usr_type ){
		var validator = $( '#register-form' ).validate();
		validator.resetForm();
		$('.form-group input').removeClass('error');
		if( usr_type == '1'){
			document.getElementById('id_group_reg_company').style.display = 'none'; 
			document.getElementById('id_reg_company').innerHTML = ''; 				
		}else{
			document.getElementById('id_group_reg_company').style.display = 'block';
			document.getElementById('id_reg_company').innerHTML = 
			'<input type=' +'\"text\"' +
			' class='+'\"form-control\"' +
			' name=' +'\"data[Websiteuser][userorgname]\"' +
			' placeholder=' +'\"Organization\"' +
			' \/>'; 
		}
	}
	
	// Script  : To set validation rules for FORMS
	
	//Function : Submit ForgotPassword Form using Ajax
	function UserForgotPassFormSubmit(){ 
		var data_str = $('#forgotpass-form').serialize();
		$('#id_fform-error-label').html('');
		$('#id_fform-error-label').css('display', 'none');
		$('.loaderdiv').css('display', 'block');
		$.ajax({
			type: 'POST',
			url: '/websiteusers/forgotpass',
			data: data_str,
			dataType :'json',
			success: function(data){
				if(data.status == 'success' ){
					$('.loaderdiv').css('display', 'none');
					$('#id_forgotpass_form').css('display', 'none');
					temp_str = '<h3 class=\'regsucs\'>Password Reset Successful. </h3>';
					temp_str = temp_str + '<div>';
					temp_str = temp_str + 'We have shared your new password on your registered email id.' 
					temp_str = temp_str + '</div>'
					
					$('#id_forgotpass_succ').html(temp_str);
					$('#id_forgotpass_succ').css('display', 'inline');
					$('#id_forgotpass_succ').css('visibility', 'visible');
					
				}
				else if(data.status == 'error' ){
					err_str = 'Entered Email does not exist in our records';
					$('#id_fform-error-label').html(err_str);
					$('#id_fform-error-label').css('display','inline');
				}else{
					$('#id_fform-error-label').html('Form submission unsuccessful. Please try after some time');
					$('#id_fform-error-label').css('display', 'inline');
				}
			}
		});
	}
	
	//Function : Submit Login Form using Ajax
	function UserLoginFormSubmit()
	{ 
		var data_str = $('#login-form').serialize();
		$('#id_lform-error-label').html('');
		$('#id_lform-error-label').css('display', 'none');
			$.ajax({
			type: 'POST',
			url: '/websiteusers/login',
			data: data_str,
			dataType: 'json',
			success: function(data){
				if(data.status == 'success' ){
					window.location = '/accounts/dashboard';
				}
				else if(data.status == 'error'){
					err_str = 'Entered Email & password does not match in our records';
					$('#id_lform-error-label').html(err_str);
					$('#id_lform-error-label').css('display','inline');
				}else{
					$('#id_lform-error-label').html('Login unsuccessful. Please try after some time');
					$('#id_lform-error-label').css('display', 'inline');
				}
			}
		});
	}
	
	//Function : Submit Registration Form using Ajax
	function UserRegistrationFormSubmit() { 
		$('button').prop('disabled', true);
		var data_str = $('#register-form').serialize();
		$('#id_rform-error-label').html('');
		$('#id_rform-error-label').css('display', 'none');
	
		$.ajax({	
			type: 'POST',
			url: '/websiteusers/register',
			data: data_str,
			dataType:'json',
			success: function(data){
				if( data.status == 'success'){
					$('button').prop('disabled', false);
					$('#id_register_form').css('display', 'none');
					temp_str = '<h3 class=\'regsucs\'>Thank you for registering with us. </h3>';
					temp_str = temp_str + '<div>';
					temp_str = temp_str + 'We have send you confirmation mail to your registered email id. Please click on the link in mail to confirm your registration.' 
					temp_str = temp_str + '</div>'
				
					$('#id_register_succ').html(temp_str);
					$('#id_register_succ').css('display', 'inline');
					$('#id_register_succ').css('visibility', 'visible');
					UserRegistrationSendEmail();
				}else if( data.status == 'error'){
					$('button').prop('disabled', false);
					err_str = 'Your registration was not successful';
					err_str = err_str;
					$.each(data.userData, function(model, errors){
						for (fieldName in this){
							err_str = err_str + '<br>' + '- ' + this[fieldName][0];
						}
					});

					$('#id_rform-error-label').html(err_str);
					$('#id_rform-error-label').css('display', 'inline');
				}else{
					$('button').prop('disabled', false);
					$('#id_rform-error-label').html('Registeration unsuccessful. Please try after some time');
					$('#id_rform-error-label').css('display', 'inline');
					$('button').prop('disabled', false);
				}	
			}
		});
	}
	
	
	function UserRegistrationSendEmail(){
		var data_str = $('#register-form').serialize();		
		$.ajax({	
			type: 'POST',
			url: '/websiteusers/sendemailtest',
			data: data_str,
			dataType:'json',
			success: function(data){
				if( data.status == 'success'){
					$('button').prop('disabled', false);					
				}else{
					$('button').prop('disabled', false);
					$('#id_rform-error-label').html('Registeration unsuccessful. Please try after some time');
					$('#id_rform-error-label').css('display', 'inline');
					$('button').prop('disabled', false);
				}	
			}
		});
		
	}
	
	
	$(function(){
	
		$.validator.addMethod('nameCustom', function(value, element){
			return this.optional(element) || /^[a-zA-Z ]+/.test(value);
		}, 'Please use English alphabets only.');
	
		$.validator.addMethod('onameCustom', function(value, element){
			return this.optional(element) || /^[a-zA-Z0-9]+/.test(value);
		}, 'Please use Alphanumeric characters only.');	
	
		$.validator.addMethod('mobileCustom', function(value, element) 
		{
			return this.optional(element) || /^[0-9]+/.test(value);  
		}, 'Please use numerals only.');	
		
		$('#register-form').validate({
			rules: {
				'data[Websiteuser][username]': {
					required : true
				},
				'data[Websiteuser][userorgname]': {
					required : true,
					minlength : 5
				},
				'data[Websiteuser][email]': {
					required : true,
					email : true,
					remote: "/websiteusers/checkField/"
				},
				'data[Websiteuser][usermobile]': {
					required : true,
					number : true,
					mobileCustom : true,
					minlength : 10,
					maxlength : 10,
					remote: "/websiteusers/checkField/"
				},
				'data[Websiteuser][password]': {
					required : true
				},
				'data[Websiteuser][userrole]': {
					required : true
				}
			},
			messages: {
				'data[Websiteuser][username]': {
					required : 'Name is required.'
				},
				'data[Websiteuser][userorgname]': {
					required : 'Organization Name is required.',
					minlength : 'Min 5 alphabets.'
				},
				'data[Websiteuser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address',
					remote: 'Email id aready exist, please try another email.'
				},
				'data[Websiteuser][usermobile]': {
					required : 'Mobile Number is required.',
					number : ' Only Numeric digits allowed.',
					minlength : 'Min 10 numbers.',
					maxlength : 'Max 10 numbers.',
					remote: 'A user with same mobile number already exist, please try another.'
				},
				'data[Websiteuser][password]': {
					required : 'Password is required.'
				},
				'data[Websiteuser][userrole]': {
					required : 'Member Type is required.'
				}
			},
			submitHandler: function() {
				sendOTP();
				// UserRegistrationFormSubmit();
				return false;
			}
		});
		
		$('#login-form').validate({
			rules: {
				'data[Websiteuser][email]': {
					required : true,
					email : true
				},
				'data[Websiteuser][password]': {
					required : true
				}
			},
			messages: {
				'data[Websiteuser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address'
				},
				'data[Websiteuser][password]': {
					required : 'Password is required.'
				}
			},
		
		submitHandler: function(){
			UserLoginFormSubmit();
				return false;
			}			
		});
		
		$('#forgotpass-form').validate({
			rules: {
				'data[Websiteuser][email]': {
					required : true,
					email : true
				}
			},
			messages: {
				'data[Websiteuser][email]': {
					required : 'Email is required.',
					email : 'Please enter a valid email address'
				}
			},
			submitHandler: function(){
				UserForgotPassFormSubmit();
				return false;
			}
		});		
	});	
</script>
