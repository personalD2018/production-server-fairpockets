<?php
$notifi_count = count($notifiction) + count($buildernotifiction) + count($propertyapproval);
?>
<div class="btn-toolbar pull-right notification-nav">
    <div class="btn-group">
        <div class="dropdown">

            <a class="btn btn-notification dropdown-toggle" data-toggle="dropdown"  id="read_notify"><i class="icon-globe"><?php if ($notifi_count > 0) { ?><span class="notify-tip"><?php echo $notifi_count; ?></span><?php } ?></i></a>
            <div class="dropdown-menu pull-right ">
                <span class="notify-h"> You have <?php echo $notifi_count; ?> notifications</span>
                <?php
                foreach ($notifiction as $notify) {
                    ?>
                    <a class="msg-container clearfix"><span class="notification-thumb"><img src="<?php echo $this->webroot; ?>admin_theme/images/notify-thumb.png" width="50" height="50" alt="user-thumb"></span><span class="notification-intro"><?php echo $notify['PropertyInquiries']['name']; ?> User Interested in <?php
                            if ($notify['PropertyInquiries']['Assisted'] == '1') {
                                echo "<b>Assisted Search</b>";
                            } if ($notify['PropertyInquiries']['Immediate'] == '1') {
                                echo " <b>,Immediate Purchase</b>";
                            }if ($notify['PropertyInquiries']['Portfolio'] == '1') {
                                echo " <b>,Portfolio Management</b>";
                            }
                            if ($notify['PropertyInquiries']['Assisted'] == '0' && $notify['PropertyInquiries']['Immediate'] == '0' && $notify['PropertyInquiries']['Portfolio'] == '0'
                            ) {
                                echo "<b>Inquiry for property</b>";
                            }
                            ?> <span class="notify-time"><?php echo $notify['PropertyInquiries']['date']; ?></span></span></a>
                            <?php } ?>
                            <?php
                            foreach ($buildernotifiction as $buildernotifictionnotify) {
                                ?>
                    <a class="msg-container clearfix"><span class="notification-thumb"><img src="<?php echo $this->webroot; ?>admin_theme/images/notify-thumb.png" width="50" height="50" alt="user-thumb"></span><span class="notification-intro">New Builder has Sent Profile For Approval <span class="notify-time"><?php echo $buildernotifictionnotify['BuilderApprovalNotifiction']['date']; ?></span></span></a>
                <?php } ?>

                <?php
                foreach ($propertyapproval as $propertyapprovalnotify) {
                    ?>
                    <a class="msg-container clearfix"><span class="notification-thumb"><img src="<?php echo $this->webroot; ?>admin_theme/images/notify-thumb.png" width="50" height="50" alt="user-thumb"></span><span class="notification-intro">New Property has Pending For Approval <span class="notify-time"><?php echo $propertyapprovalnotify['PropertyInquiries']['date']; ?></span></span></a>
                        <?php } ?>

                <!--<button class="btn btn-primary btn-large btn-block"> View All</button>-->
            </div>
        </div>
    </div>

</div>