<?php
$fillterOptions = array("keyword" => $keyword, "scriteria" => $scriteria);
$this->Paginator->options(array(
    'update' => '#inventory_list',
    'evalScripts' => true,
    'url' => $fillterOptions,
    'before' => $this->Js->get('#loaderID')->effect(
            'fadeIn', array('buffer' => false)
    ),
    'complete' => $this->Js->get('#loaderID')->effect(
            'fadeOut', array('buffer' => false)
    ),
));
$ascImage = $this->Html->image('up_arrow.gif', array('border' => 0));
$descImage = $this->Html->image('down_arrow.gif', array('border' => 0));
?>

<table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name Projects</th>
                        <th>No of Inventories</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
					   foreach ($inventories as $key=>$row):
                    ?>
                    <tr>
						
						<td><?php echo isset($row['Project']['project_name']) ? $row['Project']['project_name'] : "N/A";?></td>
                   
                        <td><?php echo 10; //$this->Number->getCountOfPdfs($row['SalesUser']['id']); ?></td>
                   
                        <td style="text-align: center;">
                           <a href="<?php echo Router::url('/accounts/inv_assign/'.$row['Project']['project_id'],true);?>" title="View" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            &nbsp;
                            </td>
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
			
			<div class="pagination">
    <?php if ($this->Paginator->numbers()) {
        echo $this->Paginator->prev(); ?>
        <?php echo $this->Paginator->numbers(); ?>
        <?php echo $this->Paginator->next();
    } ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>