<?php
$fillterOptions = array("keyword" => $keyword, "scriteria" => $scriteria);
$this->Paginator->options(array(
    'update' => '#inventory_list',
    'evalScripts' => true,
    'url' => $fillterOptions,
    'before' => $this->Js->get('#loaderID')->effect(
            'fadeIn', array('buffer' => false)
    ),
    'complete' => $this->Js->get('#loaderID')->effect(
            'fadeOut', array('buffer' => false)
    ),
));
$ascImage = $this->Html->image('up_arrow.gif', array('border' => 0));
$descImage = $this->Html->image('down_arrow.gif', array('border' => 0));
?>

<table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
						<th>Id</th>
                        <th>Project Name</th>
					
                        <th>Project Type</th>
                        <th>Project highlights</th>
						<th>Project description</th>
                        <th>Project city</th>
						<th>Project State</th>
                        <th>Project Country</th>
						<th>Project Address</th>
                        <th>Project Locality</th>
					    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
					//print_r(userid_count);die;
                       // foreach ($salesuserArray as $key=>$row):
					   foreach ($list as $key=>$row):					   
						//echo $no_sale_emp=count($row);
                    ?>
                    <tr>
						<!--<td><?php //echo $this->Number->getSalesNameBySalesId($row['InvBuilderSalesShareInventory']['sales_id']); ?></td>-->
						
						<td><?php echo isset($row['Project']['project_id']) ? $row['Project']['project_id'] : "N/A";?></td>
						
						<td><?php echo isset($row['Project']['project_name']) ? $row['Project']['project_name'] : "N/A";?></td>
                       
                        <td><?php echo isset($row['Project']['project_type']) ? $row['Project']['project_type'] : "N/A";?></td>
					
						<td><?php echo isset($row['Project']['project_highlights']) ? $row['Project']['project_highlights'] : "N/A";?></td>
						
						<td><?php echo isset($row['Project']['project_description']) ? $row['Project']['project_description'] : "N/A";?></td>
						<td><?php echo isset($row['Project']['city_data']) ? $row['Project']['city_data'] : "N/A";?></td>
						<td><?php echo isset($row['Project']['state_data']) ? $row['Project']['state_data'] : "N/A";?></td>
						<td><?php echo isset($row['Project']['country_data']) ? $row['Project']['country_data'] : "N/A";?></td>
						<td><?php echo isset($row['Project']['project_address']) ? $row['Project']['project_address'] : "N/A";?></td>
						
						<td><?php echo isset($row['Project']['locality']) ? $row['Project']['locality'] : "N/A";?></td>				
						
                        <td style="text-align: center;">
                            <!--<a href="<?php echo Router::url('/accounts/inventory/edit/'.$row['InvInventory']['id'],true);?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
                            &nbsp;
                            <a href="<?php echo Router::url('/accounts/salesPersonDetails/'.$row['SalesUser']['id'],true);?>" title="View" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            &nbsp;
                            <!--<a href="javascript:void(0);" onclick="javascript:deleteInventory('<?php echo $row['InvInventory']['id']?>')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
			
			<div class="pagination">
    <?php if ($this->Paginator->numbers()) {
        echo $this->Paginator->prev(); ?>
        <?php echo $this->Paginator->numbers(); ?>
        <?php echo $this->Paginator->next();
    } ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>