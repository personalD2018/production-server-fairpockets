	<?php $this->Paginator->options(array(
			'update' => '#clientLists',
			'evalScripts' => true,
			'before' => $this->Js->get('#loaderID')->effect(
				'fadeIn',
				array('buffer' => false)
			),
			'complete' => $this->Js->get('#loaderID')->effect(
				'fadeOut',
				array('buffer' => false)
			),
		));
$ascImage = $this->Html->image('up_arrow.gif',array('border'=>0));
$descImage = $this->Html->image('down_arrow.gif',array('border'=>0));
?>

<style>
    .thead{
		background-color: #29333d;
    background: :red;
    color: #ffffff;
	}
	thead a{color:#ffffff !important;}
	thead a:hover{color:#ffffff !important;}
	
		*, *:before, *:after {-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
input, select, input[type="checkbox"], input[type="radio"] {-webkit-border-radius:0;-webkit-appearance:none;
    outline:none;}
li {  margin-bottom:1.5em;  padding-left:2.5em;  	position:relative;}

input[type="checkbox"],
input[type="radio"],
li label::before {cursor:pointer;height:40px;left:0;margin-top:-20px;position:absolute;width:40px;top:50%;}

input[type="checkbox"],
input[type="radio"] {	  display:inline-block;	  opacity:0;	  vertical-align:middle;}

li label::before {	  border:2px solid lighten(#2a4a59,10%);	  border-radius:4px;	  color:darken(#48CFAD, 20%);
	  content:'';	  font-size:1.5em;	  padding:.1em 0 0 .2em;}

li input.error + label::before {	  border-color:#f93337;}

li input[type="checkbox"]:checked + label::before {border-color:darken(#48CFAD, 20%);content:'\2714';}

li input[type="radio"] + label::before {	  border-radius:50%;}

li input[type="radio"]:checked + label::before {border-color:darken(#48CFAD, 20%);content:'\25CF';font-size:1.5em;
  	padding:0 0 0 .3em;}

ul {  	margin-bottom:1em;  padding-top:1em;  	overflow:hidden;}

li label {	  display:inline-block;	  vertical-align:top;}

.js-errors {
    background:#d62540;
    border-radius:8px;
    color:#FFF;
    font-size:.9em;
    list-style-type:none;
    margin-bottom:1em;
    padding:1em;
	font-weight:bold;
}

.js-errors {	  display:none;}

.js-errors li {	  margin-left:1em;  margin-bottom:.5em;  padding-left:0;}
	
	
	
</style>
	
	<?php //$paginator = $this->Paginator;?>
	
	
	
		<div class="space-x"></div>
		<ul class="js-errors"></ul>
		<div class="space-x"></div>
		<?php //echo '<pre>'; print_r($allInvLists); die(); ?>
		<table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead class="thead">
                    <tr>                        
						<th>
						<?php echo $this->Paginator->sort('InvProject.project_name', 'Project Name'); ?>
						<?php if($this->Paginator->sortKey() == "InvProject.project_name") { echo $this->Paginator->sortDir() === 'asc' ? 
						$ascImage : $descImage;}?>
						</th>
                        <th>Tower </th>
						<th>
						<?php echo $this->Paginator->sort('InvInventory.created', 'Created Date'); ?>
						<?php if($this->Paginator->sortKey() == "InvInventory.created") { echo $this->Paginator->sortDir() === 'asc' ? 
						$ascImage : $descImage;}?>
						</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
					<?php if (!empty($allInvLists)) {
                        for ($i = 0; $i < count($allInvLists); $i++) {
                    ?>
                    <tr id="row<?php echo $allInvLists[$i]['InvProject']['project_id']?>">
                        <td>
						<?php echo isset($allInvLists[$i]['InvProject']['project_name']) ? $allInvLists[$i]['InvProject']['project_name'] : "N/A";?>
						</td>
						<td>
						<?php echo isset($allInvLists[$i][0]['TotalTower']) ? $allInvLists[$i][0]['TotalTower'] : "N/A";?>
						</td>
						
						<td><?php //echo isset($allInvLists[$i]['AppClientLead']['created']) ? $allInvLists[$i]['AppClientLead']['created'] : "N/A";?></td>
						
						<td style="text-align: center;">
                            <a href="<?php echo $this->webroot.'accounts/inventory/view_inventory_by_project/'.$allInvLists['InvProject']['project_id'];?>" title="View/Edit" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            &nbsp;
                        </td>
                    </tr>
					<?php }
					}else{ ?>
					
					<tr>
					<td colspan="8" style="text-align:center;color:#d62540;"><b>No Records Found</b></td>
					</tr>
					
					<?php } ?>
                </tbody>
            </table>
		<div class="clear"></div>
		
		<div class="pagination">
	<?php
	 if($this->Paginator->numbers())
	 {
	 echo $this->Paginator->prev();
	 ?>
	<?php	    
		 echo $this->Paginator->numbers(); 
	?>
	<!-- Shows the next and previous links -->	
	<?php

	 echo $this->Paginator->next();
	 }
	 ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>
		
	</div>
	
	<?php
		echo $this->Html->script('validate_1.9_jquery.validate.min');
		echo $this->Html->script('front/additional-methods.min');
	?>
		<script type="text/javascript">
		
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/leads/deleteLeadsBuilder/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
	
	
	
	
	 /*$('#frm_lead').validate({ // initialize the plugin
        rules: {
            'check_list[]': {
                required: true,
                maxlength: 2
            }
        },
        messages: {
            'check_list[]': {
                required: "You must check at least 1 box",
                maxlength: "Check no more than {0} boxes"
            }
        }
    });*/
	
	
	
		$("#frm_lead").validate({
        rules: {
          "check_list[]": {
            required: true,
            minlength: 1
          }
        },

        // FIX
        // Using highlight and unhighlight options we can add the error class to the parent ul which can then be selected and styled
        			highlight: function(element, errorClass, validClass) {
        				  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
        				  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
        			},
        			unhighlight: function(element, errorClass, validClass) {
          				$(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
          				$(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
			        },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",

        messages: {
          "check_list[]": "Please select at least one checkbox"
        },
      });
	
	
	
	
	
});
</script>	
	
		
