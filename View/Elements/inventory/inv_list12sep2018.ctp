<?php
$fillterOptions = array("keyword" => $keyword, "scriteria" => $scriteria);
$this->Paginator->options(array(
    'update' => '#inventory_list',
    'evalScripts' => true,
    'url' => $fillterOptions,
    'before' => $this->Js->get('#loaderID')->effect(
            'fadeIn', array('buffer' => false)
    ),
    'complete' => $this->Js->get('#loaderID')->effect(
            'fadeOut', array('buffer' => false)
    ),
));
$ascImage = $this->Html->image('up_arrow.gif', array('border' => 0));
$descImage = $this->Html->image('down_arrow.gif', array('border' => 0));
?>

<table class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Project</th>
                        <th>Tower</th>
                        <th>Wing</th>
                        <th>Floors</th>
                        <th>Flats/Floor</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($inventories as $key=>$row): ?>
                    <tr>
                        <td><?php echo isset($row['Project']['project_name']) ? $row['Project']['project_name'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['tower_name']) ? $row['InvInventory']['tower_name'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['wing']) ? $row['InvInventory']['wing'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['floor_start']) ? $row['InvInventory']['floor_start'] : "G";?> + <?php echo isset($row['InvInventory']['floor_end']) ? intval($row['InvInventory']['floor_end']) : "0";?></td>
                        <td><?php echo isset($row['InvInventory']['flats_per_floor']) ? $row['InvInventory']['flats_per_floor'] : "N/A";?></td>
                        <td style="text-align: center;">
                            <!--<a href="<?php //echo $this->webroot.'accounts/inventory/edit/'.$row['InvInventory']['id'];?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
                            &nbsp;
                            <a href="<?php echo $this->webroot.'accounts/inventory/view_inventory/'.$row['InvInventory']['id'];?>" title="View/Edit" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            &nbsp;
                            <a href="javascript:void(0);" onclick="javascript:deleteInventory('<?php echo $row['InvInventory']['id']?>')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
			
			<div class="pagination">
    <?php if ($this->Paginator->numbers()) {
        echo $this->Paginator->prev(); ?>
        <?php echo $this->Paginator->numbers(); ?>
        <?php echo $this->Paginator->next();
    } ?>
</div>
<?php echo $this->Js->writeBuffer(); ?>