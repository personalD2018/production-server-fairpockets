<div class="locationbtn open">
	<select id="propertysearch" name="propertysearch" class="selectpicker show-tick">
		<option value="Ahmedabad">Ahmedabad</option>
		<option value="Bengaluru">Bengaluru</option>
		<option value="Chennai">Chennai</option>
		<option value="Delhi">Delhi</option>
		<option value="Ghaziabad">Ghaziabad</option>
		<option value="Hyderabad">Hyderabad</option>
		<option value="Kolkata">Kolkata</option>
		<option value="Mumbai">Mumbai</option>
		<option value="Noida">Noida</option>
		<option value="Pune">Pune</option>
	</select>
</div>

<script>
	$(document).ready(function() {
		var location = 'Noida';
		if (typeof(Storage) !== "undefined") {
			// Code for localStorage/sessionStorage.
			if (localStorage.getItem("defaultLocation") !== null) {
				//.
				location = localStorage.getItem("defaultLocation");
			}
			
		} else {
			// Sorry! No Web Storage support..
			console.log('Web storage API not found.');
		}
		console.log(location);
		$('#propertysearch').val(location);
		$('#PropertyNotifictionPropertyName').val(location);
		$('#defaultLocation').val(location);
		$('#defaultLocation_mobile').val(location);
		
		$('#propertysearch').change(function() {
			var $val = $(this).val();
			localStorage.setItem("defaultLocation", $val);
			$('#PropertyNotifictionPropertyName').val($val);
			$('#defaultLocation').val($val);
			$('#defaultLocation_mobile').val($val);
			$('#PropertyNotifictionSearchListForm').submit();
			if($('.recent-properties').length) {
				$('.recent-properties').load('/pages/recentProperties/'+$val);
				$('.owl-carousel').owlCarousel('refresh');
			}
		});
		
		$('.recent-properties').load('/pages/recentProperties/'+location);
	});
</script>