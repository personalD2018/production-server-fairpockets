<section id="bottom">
    <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="space-l"></div>

        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="<?php echo $this->webroot; ?>about-us">About us</a></li>
                        <li><a href="<?php echo $this->webroot; ?>our-team">Our Team</a></li>
                        <li><a href="<?php echo $this->webroot; ?>blog" target="_blank" >Official Blog</a></li>

                        <li><a href="<?php echo $this->webroot; ?>career">Career</a></li>
                        <li><a href="<?php echo $this->webroot; ?>faq">FAQ</a></li> 
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Product &amp; Services</h3>
                    <ul>
                        <li><a href="<?php echo $this->webroot; ?>research-reports">Research Reports</a></li>
                        <li><a href="<?php echo $this->webroot; ?>advisory">Advisory (Buy/Sell)</a></li>
                        <li><a href="<?php echo $this->webroot; ?>property-management">Property Management</a></li>
                        <li><a href="<?php echo $this->webroot; ?>portfolio-management">Portfolio Management</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Legal &amp; Support</h3>
                    <ul>
                        <li><a href="<?php echo $this->webroot; ?>feedback">Feedback</a></li>
                        <!-- <li><a href="javascript:void(0);">My Account</a></li>
                        <li><a href="<?php echo $this->webroot; ?>disclaimer">Disclaimer</a></li> -->
                        <li><a href="<?php echo $this->webroot; ?>privacy-policy">Privacy &amp; Policy</a></li>
                        <li><a href="<?php echo $this->webroot; ?>terms-conditions">Terms &amp; Conditions</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Social Links</h3>
                    <div class="social">
                        <ul class="social-share">
                            <li><a href="
                                   https://www.facebook.com/Fair-Pockets-1995002104119801/" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/fairpocket" target="_blank" ><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/fair-pockets-9646b9145/" target="_blank" ><i class="fa fa-linkedin"></i></a></li> 
                            <!-- <li><a href=""><i class="fa fa-skype"></i></a></li> -->
                        </ul>
						 <!--
                        <ul class="list-inline pay-icons">
                           
                                    <li><a href="javascript:void(0);"><i class="fa fa-cc-visa"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-cc-mastercard"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-cc-amex"></i></a></li>
                           
                            <li><a href="javascript:void(0);"></a></li>							
                        </ul>
						 -->
                        <div class="msg91">
							<a href="https://msg91.com/startups/?utm_source=startup-banner"><img src="https://msg91.com/images/startups/msg91Badge.png" width="120" height="90" title="MSG91 - SMS for Startups" alt="Bulk SMS - MSG91"></a>
						</div>
						
                    </div><!--Social-Media-->
                </div>    
            </div><!--/.col-md-3-->
        </div>

        <div class="row">
            <div class="col-md-12 disclaimer">
		100% Secure Online Payments</b> accepted, powered by PayU money.<br/><br/>
                <strong>Disclaimer:</strong> Chilin Prop Pvt. Ltd. is merely a platform to facilitate the transactions between Seller/Owner and Buyer/Tenant (parties or customer or user) and/or to provide assistance by way of opinions, information, content or certain specialized services which are based upon material and data collected from public sources and for which Chilin Prop Pvt. Ltd. or any of its affiliates shall neither be responsible nor liable and shall not be obliged to mediate or resolve any disputes or disagreements between the parties, who shall settle all such disputes amongst themselves without involving Chilin Prop Pvt. Ltd. in any manner/
			<br/><br/>	<p>All trademarks, logos and names are properties of their respective owners.</p>
            </div>
        </div>
    </div>
</section><!--/#bottom-->
<footer id="footer" class="midnight-blue col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; 2017 <a target="_blank" href="/">Chilin Prop Pvt. Ltd. </a> All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <ul class="pull-right">
                    <li><a href="<?php echo $this->webroot; ?>">Home</a> 
                    </li>
                    <li><a href="<?php echo $this->webroot; ?>about-us">About Us</a>
                    </li>
                    <li><a href="<?php echo $this->webroot; ?>faq">Faq</a>
                    </li>
                    <li>
					<?php echo $this->Html->link('Contact Us', ['controller' => 'pages', 'action' => 'contact']); ?>
										
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
