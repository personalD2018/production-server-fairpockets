<nav class="drawer-nav" role="navigation">
	<ul class="nav" id="side-menu">
		 <div id="MainMenu">
                        <div class="list-group">
                            <a href="<?php echo $this->webroot; ?>accounts/dashboard" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'dashboard') ? 'active' : '';?>" id="disable" data-parent="#MainMenu">My Dashboard</a>
                            <?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') {
                                ?>	
                                <a href="#demoa" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['controller'] == 'projects') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">My Projects<i class="fa fa-caret-down"></i></a>

                                <div class="dropdown <?php echo ($this->params['controller'] == 'projects') ? 'collapse in' : 'collapse';?>" id="demoa">
                                    <a href="<?php echo $this->webroot; ?>projects/allprojects" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'allprojects') ? 'active' : '';?>"> View All Listings</a>
                                    <a href="<?php echo $this->webroot; ?>projects/currentproject" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'currentproject') ? 'active' : '';?>">Add Current Project</a>
                                    <a href="<?php echo $this->webroot; ?>projects/pastproject" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'pastproject') ? 'active' : '';?>">Add Past Project</a>

                                </div>
                            <?php } ?>

                            <a href="#demob" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['controller'] == 'properties') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Listed Properties<i class="fa fa-caret-down"></i></a>
                            <div class="dropdown <?php echo ($this->params['controller'] == 'properties') ? 'in' : 'collapse';?>" id="demob">
                                <a href="<?php echo $this->webroot; ?>properties/allproperties" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'allproperties') ? 'active' : '';?>"> View All Listings</a>
                                <a href="<?php echo $this->webroot; ?>properties/addproperty" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'addproperty') ? 'active' : '';?>">Add New Property</a>
                                <a href="" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'allproperties') ? 'active' : '';?>">View Responses</a>
                            </div>
                            <?php if ($this->Session->read('Auth.Websiteuser.userrole') != '3') {
                                ?>
                                <a href="#democ" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'portfolio_addProperty' || $this->params['action'] == 'Portfolio_ViewProperty' || $this->params['action'] == 'Portfolio_ServiceRequest' || $this->params['action'] == 'Portfolio_ViewReport') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">My Portfolio<i class="fa fa-caret-down"></i></a>
                                <div class="dropdown <?php echo ($this->params['action'] == 'portfolio_addProperty' || $this->params['action'] == 'Portfolio_ViewProperty' || $this->params['action'] == 'Portfolio_ServiceRequest' || $this->params['action'] == 'Portfolio_ViewReport') ? 'collapse in' : 'collapse';?>" id="democ">
                                    <a href="<?php echo $this->webroot; ?>accounts/portfolio_addProperty" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'portfolio_addProperty') ? 'active' : '';?>">Add Property</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/Portfolio_ViewProperty" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Portfolio_ViewProperty') ? 'active' : '';?>">View Property</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/Portfolio_ServiceRequest" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Portfolio_ServiceRequest') ? 'active' : '';?>">Request Service</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/Portfolio_ViewReport" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Portfolio_ViewReport') ? 'active' : '';?>">View Service</a>
                                </div>

                                <a href="#demod" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'Property_AddProperty' || $this->params['action'] == 'Property_ViewProperty' || $this->params['action'] == 'property_ServiceRequest' || $this->params['action'] == 'Porperty_ViewService') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">My Managed Properties<i class="fa fa-caret-down"></i></a>
                                <div class="dropdown <?php echo ($this->params['action'] == 'Property_AddProperty' || $this->params['action'] == 'Property_ViewProperty' || $this->params['action'] == 'property_ServiceRequest' || $this->params['action'] == 'Porperty_ViewService') ? 'collapse in' : 'collapse';?>" id="demod">
                                    <a href="<?php echo $this->webroot; ?>accounts/Property_AddProperty" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Property_AddProperty') ? 'active' : '';?>">Add Property</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/Property_ViewProperty" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Property_ViewProperty') ? 'active' : '';?>">View Property</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/property_ServiceRequest" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'property_ServiceRequest') ? 'active' : '';?>"> Request Service</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/Porperty_ViewService" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'Porperty_ViewService') ? 'active' : '';?>">View Report</a>
                                </div>

                                <a href="#demoe" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'AsstBuyRent_View_Requirement' || $this->params['action'] == 'AsstBuyRent_AddRequirement') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Assisted Buy/Rent<i class="fa fa-caret-down"></i></a>
                                <div class="dropdown <?php echo ($this->params['action'] == 'AsstBuyRent_View_Requirement' || $this->params['action'] == 'AsstBuyRent_AddRequirement') ? 'collapse in' : 'collapse';?>" id="demoe">
                                    <a href="<?php echo $this->webroot; ?>accounts/AsstBuyRent_View_Requirement" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'AsstBuyRent_View_Requirement') ? 'active' : '';?>"> View Requirements</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/AsstBuyRent_AddRequirement" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'AsstBuyRent_AddRequirement') ? 'active' : '';?>">Add Requirements</a>

                                </div>

                                <a href="#demof" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'AsstSellRent_View_Response' || $this->params['action'] == 'asstSellRent_servicerequest') ? 'active' : 'collapsed';?>" data-toggle="collapse" data-parent="#MainMenu">Assisted Sell/Rent<i class="fa fa-caret-down"></i></a>
                                <div class="dropdown <?php echo ($this->params['action'] == 'AsstSellRent_View_Response' || $this->params['action'] == 'asstSellRent_servicerequest') ? 'collapse in' : 'collapse';?>" id="demof">
                                    <a href="<?php echo $this->webroot; ?>accounts/AsstSellRent_View_Response" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'AsstSellRent_View_Response') ? 'active' : '';?>"> View Requirements</a>
                                    <a href="<?php echo $this->webroot; ?>accounts/asstSellRent_servicerequest" id="disable" class="list-group-item <?php echo ($this->params['action'] == 'asstSellRent_servicerequest') ? 'active' : '';?>">Add Requirements</a>
                                </div>

                     <!-- <a href="<?php echo $this->webroot; ?>accounts/research_report" id="disable" class="list-group-item list-group-item-success" data-parent="#MainMenu">Research Report</a>-->
                                <a href="<?php echo $this->webroot; ?>accounts/order_history" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'order_history') ? 'active' : 'collapsed';?>" data-parent="#MainMenu">Order History</a>
                                <a href="<?php echo $this->webroot; ?>accounts/transactions" id="disable" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'transactions') ? 'active' : 'collapsed';?>" data-parent="#MainMenu">My Transactions </a>
                            <?php } ?>
                            <a href="<?php echo $this->webroot; ?>accounts/change_profile" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'change_profile') ? 'active' : 'collapsed';?>" data-parent="#MainMenu">Edit Profile </a>
                            <?php if ($this->Session->read('Auth.Websiteuser.userrole') == '3') {
                                ?>	
                                <a  href="<?php echo $this->webroot; ?>accounts/builderapproval" class="list-group-item list-group-item-success <?php echo ($this->params['action'] == 'builderapproval') ? 'active' : 'collapsed';?>" data-parent="#MainMenu">Builder Organization Profile</a>
                            <?php } ?>	
                        </div>
                    </div>


	</ul>
</nav>