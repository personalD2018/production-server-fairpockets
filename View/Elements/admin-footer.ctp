<style>
	#footer{position: absolute; bottom: 0;background: #29333d;padding:10px 0;border:none;left:0;}
</style>
<footer id="footer" class="midnight-blue col-md-12">
    <div class="container">
        <div class="text-center">
			&copy; 2017 <a target="_blank" href="javascript:void(0)">Fair Pockets</a>. All Rights Reserved.
		</div>
    </div>
</footer>