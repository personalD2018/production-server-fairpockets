<?php
	class NumberHelper extends AppHelper {
		
		public function format($number){
			echo $this->convertNumberToWord($number);
			
		}
		
		public function getPropertyPic($id){
			$model = ClassRegistry::init('PropertyPic');
			$property_features = $model->find('all', array(
                'conditions' => array(
                    'property_id' => $id
                )
			));
			return  $property_features;
		}
		
		public function getPropertyUserId($id){
			$model = ClassRegistry::init('Property');

			$propert_user_id = $model->find('all', array(
				'fields' => array(
					'user_id'
				),
				'conditions' => array(
					'property_id' => $id
				)
			));
			return  $propert_user_id[0]['Property']['user_id'];
		}
		
		public function getPropertyDetailsById($id){
			$model = ClassRegistry::init('Property');

			$propert_details = $model->find('all', array(
				'conditions' => array(
					'property_id' => $id
				)
			));
			return  $propert_details;
		}
		
		public function getPropFeaturesForResponses($id){
			$model = ClassRegistry::init('PropertyFeature');

			$property_features = $model->find('all', array(
						'conditions' => array(
							'properties_id' => $id
						)
				));

				foreach($property_features as $property_val)
			{
				$all_property_features[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['features_Value'];
				$all_property_features_unit[$property_val['PropertyFeature']['features_id']] = $property_val['PropertyFeature']['Unit'];
			}
				return  $all_property_features;
		}
		
		public function getProjectNameByProjectId($id){
			$model = ClassRegistry::init('Project');

			$projectName = $model->find('all', array(
				'fields' => array(
					'project_name'
				),
				'conditions' => array(
                    'project_id' => $id
                )
			));
			return  $projectName;
		}
		
		public function getleadsByPropId($propid){
			$model = ClassRegistry::init('Lead');
			$allLeads = $model->find('all',array(
				'conditions' => array(
                    'property_id' => $propid
                )
			));
			
			
			return  $allLeads;
		}
		
		
		public function getUserOrgNameByUserId($id){
			$model = ClassRegistry::init('Websiteuser');

			$user_org_name = $model->find('all', array(
            'fields' => array(
                'userorgname'
            ),
            'conditions' => array(
                'id' => $id
            )
        ));
        return  $user_org_name[0]['Websiteuser']['userorgname'];
		}
		
		public function getUserRoleByUserId($id){
			$model = ClassRegistry::init('MstUserrole');
			$user_role = $model->find('all', array(
            'fields' => array(
                'Description'
            ),
            'conditions' => array(
                'userrole_ID' => $id
            )
        ));
        return  $user_role[0]['MstUserrole']['Description'];
		}
		
		public function getBspByProjectId($projectId){
			
			$model = ClassRegistry::init("ProjectBspDetail");
			
		    $latestBsp = $model->query("SELECT bsp_charges FROM `project_bsp_details` WHERE project_id = ".$projectId." ORDER BY id ASC LIMIT 0,1");			
			return  $latestBsp[0]['project_bsp_details']['bsp_charges'];
			//return $latestBsp
			
			
		}
		
		
		public function convertNumberFormatedRupees($num){
			
			$explrestunits = "" ;
			if(strlen($num)>3) {
				$lastthree = substr($num, strlen($num)-3, strlen($num));
				$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
				$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
				$expunit = str_split($restunits, 2);
				for($i=0; $i<sizeof($expunit); $i++) {
					// creates each of the 2's group and adds a comma to the end
					if($i==0) {
						$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
					} else {
						$explrestunits .= $expunit[$i].",";
					}
				}
				$thecash = $explrestunits.$lastthree;
			} else {
				$thecash = $num;
			}
			return $thecash;
			
		}
		
		private function convertNumberToWord($num = false) {
			$ext = "";//thousand,lac, crore
			$number_of_digits = strlen($num); //this is call :)
			if($number_of_digits>3)
			{
				if($number_of_digits % 2 != 0)
				$divider = $this->divider($number_of_digits-1);
				else
				$divider = $this->divider($number_of_digits);
			} else
			$divider = 1;
			
			$fraction = $num / $divider;
			$fraction = number_format($fraction, 2);
			
			if($number_of_digits == 4 || $number_of_digits == 5)
			$ext = "thousand";
			if($number_of_digits == 6 || $number_of_digits == 7)
			$ext = "Lac";
			if($number_of_digits == 8 || $number_of_digits == 9)
			$ext = "Cr";
		
			return ($fraction+0)." ".$ext;
		}
		
		private function divider($number_of_digits) {
			$tens = "1";
			
			if($number_of_digits > 8)
			return 10000000;
			
			while(($number_of_digits-1) > 0)
			{
				$tens .= "0";
				$number_of_digits--;
			}
			return $tens;
		}
		
		function paginate($item_per_page, $current_page, $total_records, $total_pages, $page_url)
		{
			$pagination = '';
			if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
				$pagination .= '<div class="row pagination"><ul>';
				
				$right_links    = $current_page + 10; 
				$previous       = $current_page - 1; //previous link 
				$next           = $current_page + 1; //next link
				$first_link     = true; //boolean var to decide our first link
				
				if($current_page > 1){
					$previous_link = ($previous==0)?1:$previous;
					$pagination .= '<li class="first"><a href="'.$page_url.'&page=1" title="First">First</a></li>'; //first link
					$pagination .= '<li><a href="'.$page_url.'&page='.$previous_link.'" title="Previous">Prev</a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="'.$page_url.'&page='.$i.'">'.$i.'</a></li>';
						}
					}   
					$first_link = false; //set first link to false
				}
				
				if($first_link){ //if current active page is first link
					$pagination .= '<li class="Previous disabled"><a href="javascript:void(0);">Prev</a></li>';
					$pagination .= '<li class="active"><a href="javascript:void(0);">'.$current_page.'</a></li>';
				} elseif($current_page == $total_pages) { //if it's the last active link
					$pagination .= '<li class="last active"><a href="javascrip"void(0);">'.$current_page.'</a></li>';
				} else { //regular current link
					$pagination .= '<li class="active"><a href="javascript:void(0);">'.$current_page.'</a></li>';
				}
                
				for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
					if($i<=$total_pages){
						$pagination .= '<li><a href="'.$page_url.'&page='.$i.'">'.$i.'</a></li>';
					}
				}
				if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages)? $total_pages : $i;
					$pagination .= '<li><a href="'.$page_url.'&page='.$next_link.'" >Next</a></li>'; //next link
					$pagination .= '<li class="last"><a href="'.$page_url.'&page='.$total_pages.'" title="Last">Last</a></li>'; //last link
				}
				
				$pagination .= '</ul></div>'; 
			}
			return $pagination; //return pagination links
		}
		
		public function appUserGetUsertypeByUserId($id){
			$model = ClassRegistry::init('SalesUser');

			$appUserGetUsertypeByUserIdArr = $model->find('all', array(
            'fields' => array(
                'userorgname'
            ),
            'conditions' => array(
                'id' => $id
            )
        ));
        return  $appUserGetUsertypeByUserIdArr[0]['SalesUser']['usertype'];
		}
		
		public function shareProjectStatusProjectId($project_id,$builderId){
			$model = ClassRegistry::init('ShareProjectsHistory');
			//echo $builderId;
			//echo $project_id;die();

			$ShareProjectsHistoryData = $model->find('all', array(
            'conditions' => array(
                'builder_id' => $builderId,
				'project_id' => $project_id
            )
			));
			//echo '<pre>'; print_r($ShareProjectsHistoryData);
			if(!empty($ShareProjectsHistoryData)){
					return 'update';
			}else{
				return 'add';
			}
			
			//return  $projectName;
		}
		
		
		public function getBrokerNameByBrokerId($salesUserId){
			$model = ClassRegistry::init('SalesUser');

			$getBrokerNameByBrokerId = $model->find('all', array(
            'fields' => array(
                'name'
            ),
            'conditions' => array(
                'id' => $salesUserId
            )
        ));
        return  $getBrokerNameByBrokerId[0]['SalesUser']['name'];
			
			//return  $projectName;
		}
		
		
		public function getSalesNameBySalesId($salesUserId){
			$model = ClassRegistry::init('SalesUser');

			$getSalesNameBySalesId = $model->find('all', array(
            'fields' => array(
                'name'
            ),
            'conditions' => array(
                'id' => $salesUserId
            )
        ));
        return  $getSalesNameBySalesId[0]['SalesUser']['name'];
			
			//return  $projectName;
		}
		
		public function getBuilderIdBySalesUserId($salesUserId){
			$model = ClassRegistry::init('SalesUser');
			
			//if($salesUserType == 'sales' || $salesUserType == 'Broker'){
				$getBuilderIdBySalesUserId = $model->find('all', array(
				'fields' => array(
					'builder_id'
				),
				'conditions' => array(
					'id' => $salesUserId
				)
			));				
			//}

			
        return  $getBuilderIdBySalesUserId[0]['SalesUser']['builder_id'];
			
			//return  $projectName;
		
		
	}
	
	
	public function userInvStatusSales($appUserId){
		$model = ClassRegistry::init('BuilderSalesShareInventory');
				$getInvArray = $model->find('list',
				array('fields'=> array('inventory_id'),
					'conditions'=>array('sales_id' => $appUserId)));
				if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
		
		return  $UserInvStatus;
	}
	
	
	public function UserCalStatusSales($appUserId){
		$model = ClassRegistry::init('ProjectsAndroidApp');
		
				$getCalArray = $model->find('list',
				array('fields'=> array('id'),
					'conditions'=>array('sales_or_broker_employee_id' => $appUserId)));
				if(isset($getCalArray) && (!empty($getCalArray))){
						$UserCalStatusSales = 'true';
					}else{
						$UserCalStatusSales = 'false';
					}
		
		return  $UserCalStatusSales;
	}
	
	public function userInvStatusBrokers($appUserId){
		$model = ClassRegistry::init('BuilderBrokerShareInventory');
				$getInvArray = $model->find('list',
					array('fields'=> array('inventory_id'),
						'conditions'=>array('broker_id' => $appUserId)));
				if(isset($getInvArray) && (!empty($getInvArray))){
						$UserInvStatus = 'true';
					}else{
						$UserInvStatus = 'false';
					}
		
		return  $UserInvStatus;
	}
	
	
	public function UserCalStatusBrokers($appUserId){
		$model = ClassRegistry::init('ProjectsAndroidApp');
		
				$getCalArray = $model->find('list',
				array('fields'=> array('id'),
					'conditions'=>array('sales_or_broker_employee_id' => $appUserId)));
				if(isset($getCalArray) && (!empty($getCalArray))){
						$UserCalStatusSales = 'true';
					}else{
						$UserCalStatusSales = 'false';
					}
		
		return  $UserCalStatusSales;
	}
	
	
	
	public function getStatusInv($invid,$floor_no,$flat_no,$statArr){
		
		//echo '<pre>'; print_r($statArr); die();
			$model = ClassRegistry::init('InvInventoryProperty');
			$property_features = $model->find('all', array(
			'fields' => array(
                //'flat_area',
				//'flat_bhk',
				'flat_status'
				//'flat_position'
            ),
                'conditions' => array(
                   'inventory_id' => $invid,
				   'floor_no' => $floor_no,
				   'flat_no' => $flat_no,
				   'flat_status' => $statArr
                )
			));
			//echo '<pre>'; print_r($property_features);die();
			return  $property_features[0]['InvInventoryProperty']['flat_status'];
		}
		
		public function getInvByArea($invid,$area,$status){
			
			//echo $invid;
			//echo $area;
			//die();
		
		//echo '<pre>'; print_r($statArr); die();
			$model = ClassRegistry::init('InvInventoryProperty');
			$property_features = $model->find('all', array(
			'fields' => array(
                'DISTINCT(flat_no)',
				'flat_area',
				'flat_bhk',
				'flat_position'
            ),
                'conditions' => array(
                   'inventory_id' => $invid,
				   'flat_area' => $area,
				   'flat_status' => $status
                )
			));
			//echo '<pre>'; print_r($property_features);die();
			$things = Set::extract('/InvInventoryProperty/.', $property_features);
			return  $things;
		}
		
		
		
		
		public function invFloorByInvId($inventory_id){
			$model1 = ClassRegistry::init('InventoryPropertyStatus');
			$model2 = ClassRegistry::init('InventoryPropertyPosition');
			$model3 = ClassRegistry::init('InvInventoryProperty');
			
			$status_arr = $model1->find('all');
			$temp_arr = array();
			
			if (is_array($status_arr) && count($status_arr) > 0) {
				foreach ($status_arr as $row) {
					$temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
				}
			}
			$status_arr = $temp_arr;
			
			
			$position_arr =$model2->find('all');
			$temp_arr = array();
			if (is_array($position_arr) && count($position_arr) > 0) {
				foreach ($position_arr as $row) {
					$temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
				}
			}
			$position_arr = $temp_arr;
			
			
			
			//$inventory_property = $this->InvInventoryProperty->getInventoryProperty($inventory_id);
			
			$inventory_property = $model3->find('all', array(
            'conditions' => array("InvInventoryProperty.inventory_id = '{$inventory_id}'"
			//"InvInventoryProperty.flat_area"=>1245
			),
            'order'=>array("InvInventoryProperty.floor_no asc", "InvInventoryProperty.flat_no asc")
			));
			
			$temp_arr = array();
			if (is_array($inventory_property) && count($inventory_property) > 0) {
				foreach ($inventory_property as $property) {
					$temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
				}
			}
			
			//echo '<pre>'; print_r($temp_arr); die();
			return $temp_arr;
			
				//echo '<pre>'; print_r($property_features);die();
				//return  $property_features[0]['InvInventoryProperty']['flat_status'];
			}
			
			
			
			public function getCountOfPdfs($id){
			$model = ClassRegistry::init('AppClientLead');
			$totalcount = $model->find('count',array(
			'conditions' => array('id'=>$id)
			));
			return  $totalcount;
		}
		
		public function getCountOfProposal($id){
			
			//echo $dateStart = date_format(date_create($startdate),"Y-m-d H:i:s");
//echo $dateEnd = date_format(date_create($enddate),"Y-m-d H:i:s");
			$model = ClassRegistry::init('AppClientLeadsHistory');
				$totalcount = $model->find('count',array(
			'conditions' => array('user_id'=>$id)
			
			));
			//echo '<pre>'; print_r($totalcount);die();
		return  $totalcount;
		}
		
		
		public function getCountOfProposal1($id,$startdate,$enddate){
			//echo $id.','.$startdate.','.$enddate;
			 //$startdate = date_create($startdate);
			 //$enddate =date_create($enddate);
			//echo date($startdate,'m/d/y');
			// $startdate.'-'.$enddate;
			//echo 
			 $dateStart = date_format(date_create($startdate),"Y-m-d H:i:s");
			 $dateEnd = date_format(date_create($enddate),"Y-m-d H:i:s");
			$model = ClassRegistry::init('AppClientLeadsHistory');
				$totalcount = $model->find('count',array(
			'conditions' => array('user_id'=>$id,
			'created between ? and ?' => array($dateStart, $dateEnd))
			
			));
			//echo '<pre>'; print_r($totalcount);die();
			return  $totalcount;
		}
		
		public function getCountOfSalesuserCreatedByBroker($id){
			$model = ClassRegistry::init('SalesUser');
			$totalcount = $model->find('count',array(
			'conditions' => array('broker_id'=>$id,'user_type'=>'Broker_sales')
			));
			return  $totalcount;
		}
	    
	
	
	
	}