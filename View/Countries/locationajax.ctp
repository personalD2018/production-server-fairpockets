<?php

if (isset($states)) {

    echo $this->Form->input('Area.state_id', array('id' => 'state', 'onchange' => 'getcity(this.value)', 'empty' => 'Select', 'class' => 'small', 'title' => 'Please Select Manager', 'label' => false, 'options' => $states, 'hiddenfield' => false));
}
if (isset($cities)) {
    echo $this->Form->input('Area.city_id', array('div' => false, 'onchange' => 'getlocal(this.value)', 'label' => false, 'hiddenField' => false, 'class' => 'chzn-select small', 'options' => $cities, 'id' => 'city', 'empty' => 'Select'));
}
if (isset($localities)) {
    echo $this->Form->input('Area.local_id', array('div' => false, 'label' => false, 'hiddenField' => false, 'class' => 'chzn-select small', 'options' => $localities, 'id' => 'local', 'empty' => 'Select'));
}
if (isset($pcities)) {
    echo $this->Form->input('Portfolio.prop_city', array('div' => false, 'onchange' => 'getlocal(this.value)', 'label' => false, 'hiddenField' => false, 'class' => 'selectpicker required', 'options' => $pcities, 'id' => 'city', 'empty' => 'Select'));
}
if (isset($recities)) {
    echo $this->Form->input('BuyRentRequirement.prop_city', array('id' => 'prop_city', 'options' => $recities, 'class' => 'selectpicker required', 'label' => false));
}

?>