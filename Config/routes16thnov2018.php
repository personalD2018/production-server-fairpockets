<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display'));

Router::connect('/about-us/', array('controller' => 'pages', 'action' => 'about_us'));
Router::connect('/our-team/', array('controller' => 'pages', 'action' => 'our_team'));
Router::connect('/faq/', array('controller' => 'pages', 'action' => 'faq'));
Router::connect('/research-reports/', array('controller' => 'accounts', 'action' => 'research_report'));
Router::connect('/advisory/', array('controller' => 'pages', 'action' => 'advisory'));
Router::connect('/property-management/', array('controller' => 'pages', 'action' => 'property_management'));
Router::connect('/portfolio-management/', array('controller' => 'pages', 'action' => 'portfolio_management'));
Router::connect('/career', array('controller' => 'pages', 'action' => 'career'));
Router::connect('/feedback/', array('controller' => 'pages', 'action' => 'feedback'));
Router::connect('/privacy-policy/', array('controller' => 'pages', 'action' => 'pap'));
Router::connect('/terms-conditions/', array('controller' => 'pages', 'action' => 'tac'));
Router::connect('/pricing/', array('controller' => 'pages', 'action' => 'pricing'));
Router::connect('/contact', array('controller' => 'pages', 'action' => 'contact'));
Router::connect('/emi-calc/', array('controller' => 'pages', 'action' => 'emi_calc'));
Router::connect('/rentvbuy-calc/', array('controller' => 'pages', 'action' => 'rentvbuy_calc'));

Router::connect('/admin', array('controller' => 'users', 'action' => 'dashboard', 'admin' => true));
Router::connect('/admin/builder', array('controller' => 'accounts', 'action' => 'builderIndex', 'admin' => true));
Router::connect('/login/', array('controller' => 'users', 'action' => 'login'));
Router::connect('/logout/', array('controller' => 'users', 'action' => 'logout', 'admin' => true));
Router::connect('/setup/', array('controller' => 'users', 'action' => 'setup'));

Router::connect('/websiteusers/logout/', array('controller' => 'websiteusers', 'action' => 'logout'));
Router::connect('/websiteusers/index/', array('controller' => 'websiteusers', 'action' => 'index'));


Router::connect('/emailVerify/*', array('controller' => 'websiteusers', 'action' => 'emailVerify'));

Router::connect('/fp_pgform/', array('controller' => 'pages', 'action' => 'fp_pgform'));
Router::connect('/fp_pgsuccess/', array('controller' => 'pages', 'action' => 'fp_pgsuccess'));
Router::connect('/fp_pgfailure/', array('controller' => 'pages', 'action' => 'fp_pgfailure'));

//Router::connect('/index.php', array('controller' => 'pages', 'action' => 'about_us'));

Router::connect('/searchList', array('controller' => 'pages', 'action' => 'search_list'));
Router::connect('/searchListDetail/:property_id', array('controller' => 'pages', 'action' => 'search_list_detail'), array('pass' => array('property_id')));
Router::connect('/searchListDetailOld/:property_id', array('controller' => 'pages', 'action' => 'search_list_detail_old'), array('pass' => array('property_id')));
Router::connect('/:property_id', array('controller' => 'pages', 'action' => 'builder_property_details'), array('pass' => array('property_id')));
Router::connect('/builderPropertyDetailsTesting/:property_id', array('controller' => 'pages', 'action' => 'builder_property_details_testing'), array('pass' => array('property_id')));
Router::connect('/calculator/:property_id', array('controller' => 'pages', 'action' => 'calculator'), array('pass' => array('property_id')));





Router::connect('/', array('controller' => 'posts', 'action' => 'index', 'home'));

//Router::connect('/myProjects/', array('controller' => 'accounts', 'action' => 'myProjects'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
//CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
 
 
 /* For Inventory Management START */
# Router::connect('/accounts/inventory', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'index'));
Router::connect('/accounts/inventory', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'getInventories'));

Router::connect('/accounts/pdfinv', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'pdf_inv'));

//Router::connect('/accounts/inventory1', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'getInventories1'));

Router::connect('/accounts/inventory/add', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'addInventory'));
Router::connect('/accounts/inventory/add_floor/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'addInventoryFloor'), array('pass' => array('inventory_id')));
Router::connect('/accounts/inventory/save_floor', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'saveInventoryFloor'));
Router::connect('/accounts/inventory/view_inventory/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'viewInventoryFloors'), array('pass' => array('inventory_id')));


Router::connect('/accounts/inventory/view_inventory_testing/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'viewInventoryFloorsTesting'), array('pass' => array('inventory_id')));

Router::connect('/accounts/inventory/view_inventory1/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'viewInventoryFloors1'), array('pass' => array('inventory_id')));
Router::connect('/accounts/inventory/view_inventory1/:inventory_id/:print', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'viewInventoryFloors1'), array('pass' => array('inventory_id','print')));

Router::connect('/accounts/inventory/save_property_inventory', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'savePropertyInventory'));
Router::connect('/accounts/inventory/edit/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'editInventory'), array('pass' => array('inventory_id')));
Router::connect('/accounts/inventory/delete/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'deleteInventory'), array('pass' => array('inventory_id')));

Router::connect('/accounts/inventory/share_inventory_to_brokers/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareInventoryToBrokers'),array('pass'=>array('inventory_id')));

Router::connect('/accounts/inventory/share_inventory_to_sales/:inventory_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareInventoryToSales'),array('pass'=>array('inventory_id')));


/* Ajax Call */
Router::connect('/accounts/inventory/edit_property_inventory', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'editPropertyInventoryForm'));
/* For Inventory Management END */

/* for search broker */
Router::connect('/accounts/search-broker', array('plugin' => 'InventoryManager', 'controller' => 'brokers', 'action' => 'searchbrokers'));
Router::connect('/accounts/list-brokers', array('plugin' => 'InventoryManager', 'controller' => 'brokers', 'action' => 'listbrokers'));
Router::connect('/accounts/brokers/delete/:builder_id/:broker_id', array('plugin' => 'InventoryManager', 'controller' => 'brokers', 'action' => 'deleteBrokerNetwork'), array('pass' => array('builder_id','broker_id')));

/* share projects info,calculator,inventory */
Router::connect('/accounts/projects/share-list-projects', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareListProjects'));
Router::connect('/accounts/projects/share-project-info/:project_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectInfo'), array('pass'=>array('project_id')));
Router::connect('/accounts/projects/share-project-info-appusers-sales/:project_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectInfoAppusersSales'), array('pass'=>array('project_id')));
Router::connect('/accounts/projects/share-project-info-appusers-brokers-all/:project_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectInfoAppusersBrokersAll'), array('pass'=>array('project_id')));

Router::connect('/accounts/projects/share-project-info-update-appusers/:project_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectInfoUpdateAppusers'), array('pass'=>array('project_id')));

Router::connect('/accounts/projects/share-message-appusers-all', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareMessageAppusersAll'));
Router::connect('/accounts/projects/share-message-appusers-all-assign/:share_message_info_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareMessageAppusersAllAssign'), array('pass'=>array('share_message_info_id')));

Router::connect('/accounts/projects/share-project-info-update/:project_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectInfoUpdate'), array('pass'=>array('project_id')));
Router::connect('/accounts/projects/view-projects-broker/:share_project_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectInfo'), array('pass'=>array('share_project_id')));

Router::connect('/accounts/projects/share-project-to-sales/:share_project_info_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectToSales'), array('pass'=>array('share_project_info_id')));

Router::connect('/accounts/projects/share-project-to-brokers/:share_project_info_id', array('plugin' => 'InventoryManager', 'controller' => 'inventories', 'action' => 'shareProjectToBrokers'), array('pass'=>array('share_project_info_id')));
/* Ajax Call */
Router::connect('/accounts/inventory/search-broker-list', array('plugin' => 'InventoryManager', 'controller' => 'brokers', 'action' => 'searchBrokerList'));
Router::connect('/accounts/inventory/save_brokers_to_network', array('plugin' => 'InventoryManager', 'controller' => 'brokers', 'action' => 'saveBrokersToNetwork'));


 
 
require CAKE . 'Config' . DS . 'routes.php';
