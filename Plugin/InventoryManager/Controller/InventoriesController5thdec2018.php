<?php
class InventoriesController extends InventoryManagerAppController {
    public $uses = array(
        'InventoryManager.InvProject',
        'InventoryManager.InvProperty',
        'InventoryManager.InvInventory',
        'InventoryManager.InvInventoryPropertyStatus',
        'InventoryManager.InvInventoryPropertyPosition',
        'InventoryManager.InvWebsiteuser',
        'InventoryManager.InvInventoryProperty');
    public $components = array('Paginator', 'Session', 'InventoryManager.InventoryCustom');
	var $helpers = array('Number');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authorize = array();
        $this->Auth->allow('pdf_inv','getInventories1','shareInventoryToSales','viewInventoryFloors1','viewInventoryFloorsTesting','viewInventoryFloors',
		'index', 'getInventories', 'addInventory', 'addInventoryFloor','shareProjectToBrokers','viewProjectsBroker');
    }

    public function index() {
        echo "inventory manager index";
        exit;
    }
	
	public function updateinv() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        //echo '<pre>'; print_r($_POST);die();
		
		$data_arr = array();
        $data_arr['InvInventoryProperty']['id'] = isset($this->request->data['id']) ? $this->request->data['id'] : false;
        //$data_arr['InvInventoryProperty']['flat_no'] = isset($this->request->data['flat_no']) ? $this->request->data['flat_no'] : false;
        $data_arr['InvInventoryProperty']['flat_area'] = isset($this->request->data['flat_area']) ? $this->request->data['flat_area'] : false;
        $data_arr['InvInventoryProperty']['flat_bhk'] = isset($this->request->data['flat_bhk']) ? $this->request->data['flat_bhk'] : false;
        $data_arr['InvInventoryProperty']['flat_status'] = isset($this->request->data['status']) ? $this->request->data['status'] : false;
		
		if($this->request->data['flat_position'][0]== ''){
		$data_arr['InvInventoryProperty']['flat_position'] = '';	
		}else{
		$data_arr['InvInventoryProperty']['flat_position'] = isset($this->request->data['flat_position']) ? json_encode($this->request->data['flat_position']) : '';	
		}
        //$data_arr['InvInventoryProperty']['flat_position'] = isset($this->request->data['flat_position']) ? json_encode($this->request->data['flat_position']) : '';
        if (isset($data_arr['InvInventoryProperty']['id']) && !empty($data_arr['InvInventoryProperty']['id'])) {
            $this->InvInventoryProperty->create();
			//echo '<pre>'; print_r($data_arr);die();
            $this->InvInventoryProperty->save($data_arr);
            echo json_encode(array("status" => "1"));
            exit;
        }
        echo json_encode(array("status" => "0"));
        exit;
		die();
        //$inventories = $this->InvInventory->getInventoriesByUserId($builder_id);
        //$this->set('inventories', $inventories);
    }

    public function getInventoriesTesting() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);  
       
			$keyword = "";
			$scriteria = "";
		   // if (isset($this->request->params["named"]["page"])) {
				if (isset($this->request->params["named"]['keyword'])) {
					$this->request->data['AppClientLead']['keyword'] = trim($this->request->params["named"]['keyword']);
				}
		   // }
			if (empty($this->request->data)) {				
				$this->Paginator->settings = array(
				'fields' => array("COUNT(InvInventory.tower_name) AS TotalTower", "COUNT(InvInventory.wing) AS TotalWing",
				"InvInventory.created AS date_created", "InvProject.project_name", "InvProject.project_id"),
                'conditions' => array("InvWebsiteuser.id = '{$builder_id}'","InvInventory.tower_name !=" =>''),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'group' => 'InvInventory.project_id'
				);				
			} else {			
				if ($this->request->data['InvInventory']['keyword'] != '') {
					//echo '884'; die();
					$conditions = array('OR' => array(
							'InvProject.name LIKE' => $this->request->data['InvInventory']['keyword'] . '%',
							'InvInventory.city LIKE' => $this->request->data['InvInventory']['keyword'] . '%'
						),"InvInventory.builder_id" => $this->Session->read('Auth.Websiteuser.id'));
					 
				}
			}
					
			if (isset($this->request->data['InvInventory']['date_from']) and $this->request->data['InvInventory']['date_from'] != '') {
					$otherfilterConditions .= "date_from:" . $this->request->data['InvInventory']['date_from'];
					$conditions['Date(InvInventory.created) >='] = $this->request->data['InvInventory']['date_from'];
				}

				if (isset($this->request->data['InvInventory']['date_to']) and $this->request->data['InvInventory']['date_to'] != '') {
					$otherfilterConditions .= "date_to:" . $this->request->data['InvInventory']['date_to'];
					$conditions['Date(InvInventory.created) <='] = $this->request->data['InvInventory']['date_to'];
				}
			//$allsalesUsers = $this->Paginator->paginate('InvInventory');
			//echo '<pre>'; print_r($allsalesUsers);die();
			//$this->paginate = array('limit' => 8, 'order' => array('InvInventory.created' => 'desc'));
			$this->set('keyword', $keyword);
			$this->set('scriteria', $scriteria);
			//$AppClientLeads = $this->paginate('InvInventory',$conditions,$joins);
			$allInvLists = $this->Paginator->paginate('InvInventory');
			$this->set('otherfilterConditions', $otherfilterConditions);
			$this->set('allInvLists', $allInvLists);
			echo '<pre>'; print_r($allInvLists);die();
			if ($this->RequestHandler->isAjax()) {
				$this->layout = "ajax";
				$this->viewPath = 'Elements' . DS . 'inventory';
				$this->render('invListBuilderRow');
			}

		
		
    }
	
	public function getInventories() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        
        $inventories = $this->InvInventory->getInventoriesByUserId($builder_id);
        $this->set('inventories', $inventories);
    }
	
    protected function modifyFloors($mode, $inventory_id, $floor_start, $floor_end){
        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $template = isset($inventory[0]['InvInventory']['template']) ? json_decode($inventory[0]['InvInventory']['template'], true) : array();
        
        if($mode == 'remove'){
            for($i = $floor_start; $i <= $floor_end; $i++){
                $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id, "InvInventoryProperty.floor_no" => $i), false);
            }
        }
        if($mode == 'add'){
            for ($i = intval($floor_start); $i <= intval($floor_end); $i++) {
                if (is_array($template['flat_no']) && count($template['flat_no']) > 0) {
                    foreach ($template['flat_no'] as $key => $row) {
                        $inv_arr['InvInventoryProperty']['inventory_id'] = $inventory_id;
                        $inv_arr['InvInventoryProperty']['floor_no'] = $i;
                        $inv_arr['InvInventoryProperty']['flat_no'] = $template['flat_no'][$key];
                        $inv_arr['InvInventoryProperty']['flat_area'] = $template['flat_area'][$key];
                        $inv_arr['InvInventoryProperty']['flat_bhk'] = $template['flat_bhk'][$key];
                        $inv_arr['InvInventoryProperty']['flat_status'] = $template['flat_status'][$key];
                        $inv_arr['InvInventoryProperty']['flat_position'] = isset($template['flat_position'][$key]) ? json_encode($template['flat_position'][$key]) : "";
                        $inv_arr['InvInventoryProperty']['created_at'] = date("Y-m-d H:i:s");                        
                        try {
                            $this->InvInventoryProperty->create();
                            $this->InvInventoryProperty->save($inv_arr);
                        } catch (Exception $exc) {
                            echo $exc->getMessage(); exit;
                        }
                    }
                }
            }
        }
    }
    public function addInventory() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        # get projects
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $projects = $this->InvProject->getProjectByUser($builder_id);

        $this->set('projects', $projects);

        $mode = isset($this->request->data['mode']) ? $this->request->data['mode'] : "";

        if ($this->request->is('post') && $mode == 'process') {
            
            $save_data_arr['InvInventory'] = $this->request->data['Inventory'];
            $save_data_arr['InvInventory']['created_at'] = date("Y-m-d H:i:s");
            
            # check if any changes regarding flat layout
            $flush_flag = 1;
            
            if(isset($this->request->data['Inventory']['id'])){
                $flush_flag = 0;
                $inventory_row = $this->InvInventory->getInventoryById($this->request->data['Inventory']['id']);
                # chaeck changes in flats/floor or project id
                if(
                        (isset($inventory_row[0]['InvInventory']['flats_per_floor']) && 
                        $inventory_row[0]['InvInventory']['flats_per_floor'] != $save_data_arr['InvInventory']['flats_per_floor'])
                        ||
                        (isset($inventory_row[0]['InvInventory']['project_id']) && 
                        $inventory_row[0]['InvInventory']['project_id'] != $save_data_arr['InvInventory']['project_id'])        
                                
                ){
                    $flush_flag++;
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_start']) > intval($save_data_arr['InvInventory']['floor_start'])){
                    $this->modifyFloors('add',$this->request->data['Inventory']['id'], intval($save_data_arr['InvInventory']['floor_start']), intval($inventory_row[0]['InvInventory']['floor_start']) - 1);
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_start']) < intval($save_data_arr['InvInventory']['floor_start'])){
                    $this->modifyFloors('remove',$this->request->data['Inventory']['id'], intval($inventory_row[0]['InvInventory']['floor_start']), intval($save_data_arr['InvInventory']['floor_start']) - 1);
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_end']) < intval($save_data_arr['InvInventory']['floor_end'])){
                    $this->modifyFloors('add', $this->request->data['Inventory']['id'], intval($inventory_row[0]['InvInventory']['floor_end']) + 1, intval($save_data_arr['InvInventory']['floor_end']));
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_end']) > intval($save_data_arr['InvInventory']['floor_end'])){
                    $this->modifyFloors('remove', $this->request->data['Inventory']['id'], intval($save_data_arr['InvInventory']['floor_end']) + 1, intval($inventory_row[0]['InvInventory']['floor_end']));
                }
            }
             
            $this->InvInventory->create();
            if(intval($save_data_arr['InvInventory']['floor_start']) > 0){
                $save_data_arr['InvInventory']['floor_start']++;
            }
            if ($this->InvInventory->save($save_data_arr)) {
                $this->Session->setFlash('Inventory Saved!');
                $inventory_id = $this->InvInventory->id;
                if($flush_flag){
                    $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id), false);
                    return $this->redirect('/accounts/inventory/add_floor/' . $inventory_id);
                }
                return $this->redirect('/accounts/inventory');
            }
        }
    }

    public function addInventoryFloor($inventory_id = null) {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');



        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyAreaByUser($builder_id,$inventory[0]['Project']['project_id'])['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyUnitByUser($builder_id,$inventory[0]['Project']['project_id'])['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);


        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
    }

    public function saveInventoryFloor() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        # get projects
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $projects = $this->InvProject->getProjectByUser($builder_id);
        $this->set('projects', $projects);

        if ($this->request->is('post')) {
            $flat_no_arr = $this->request->data['InvInventoryProperty']['flat_no'];
            $flat_area_arr = $this->request->data['InvInventoryProperty']['flat_area'];
            $flat_bhk_arr = $this->request->data['InvInventoryProperty']['flat_bhk'];
            $flat_status_arr = $this->request->data['InvInventoryProperty']['flat_status'];
            $flat_position_arr = isset($this->request->data['InvInventoryProperty']['flat_position']) ? $this->request->data['InvInventoryProperty']['flat_position'] : array();

            $template_str = json_encode($this->request->data['InvInventoryProperty']);
            $inventory_id = $this->request->data['inventory_id'];
            $inventory = $this->InvInventory->getInventoryById($inventory_id);
            try {
                # update template
                $this->InvInventory->updateAll(array("InvInventory.template" => "'{$template_str}'"), array("InvInventory.id" => $inventory_id));

                # add all floors
                $start_floor = $inventory[0]['InvInventory']['floor_start'];
                $end_floor = $inventory[0]['InvInventory']['floor_end'];
                if ($start_floor == 'G') {
                    $start_floor = '0';
                }
                $inv_arr = array();
                $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id), false);
                for ($i = intval($start_floor); $i <= intval($end_floor); $i++) {
                    if (is_array($flat_no_arr) && count($flat_no_arr) > 0) {
                        foreach ($flat_no_arr as $key => $row) {
                            $inv_arr['InvInventoryProperty']['inventory_id'] = $inventory_id;
                            $inv_arr['InvInventoryProperty']['floor_no'] = $i;
                            $inv_arr['InvInventoryProperty']['flat_no'] = $flat_no_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_area'] = $flat_area_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_bhk'] = $flat_bhk_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_status'] = $flat_status_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_position'] = isset($flat_position_arr[$key]) ? json_encode($flat_position_arr[$key]) : "";
                            $inv_arr['InvInventoryProperty']['created_at'] = date("Y-m-d H:i:s");
                            $this->InvInventoryProperty->create();
                            $this->InvInventoryProperty->save($inv_arr);
                        }
                    }
                }
                $this->Session->setFlash('Inventory updated!');
                $url = '/accounts/inventory/view_inventory/' . $inventory_id;
                return $this->redirect($url);
            } catch (Exception $exc) {
                $this->Session->setFlash('Inventory could not be updated!', 'default', array('class' => 'error'));
            }
        }
        return $this->redirect('/accounts/inventory');
    }
	public function pdf_inv(){
		
		$this->layout = "empty";			
			//echo ROOT . DS . "vendors/pdfapi".DS."tcpdf.php";
				require_once(ROOT . DS . "vendors/pdfapi".DS."tcpdf.php");
				$this->view = 'pdf_inv';
		//die();
	}
	
	public function viewInventoryFloors1() {
		//echo '<pre>'; print_r($params);
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        //$this->layout = 'account';
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		//echo '<pre>'; print_r($_POST);die();
		
		//$this->layout = "empty";			
			//echo ROOT . DS . "vendors/pdfapi".DS."tcpdf.php";
				//require_once(ROOT . DS . "vendors/pdfapi".DS."tcpdf.php");
				//$this->view = 'pdf_inv';
		
		
		
		/*$inventIdArr = $this->request->data['check_list'];
		$inventStatArr = $this->request->data['status'];
		$inventAreaArr = $this->request->data['check_list_area'];*/
		//echo '<pre>'; print_r($this->request->data);die();
		$inventIdArr = json_decode($this->request->data['selectedInvArrJson'],true);
		
		if(isset($this->request->data['check_list_status']) && !empty($this->request->data['check_list_status'])){
				$inventStatArr = $this->request->data['check_list_status'];
		}else{
				$inventStatArr = $this->request->data['check_list_status8'];
		}
		
		
		$inventAreaArr = json_decode($this->request->data['selectedAreaArrJson'],true);
		
		
		//$inventIdArr = array(2,12);
		//$inventStatArr = array(1);
		$this->set('statArr', $inventStatArr);
		$this->set('inventAreaArr', $inventAreaArr);
		//$inventIdArr = array(2);
		//print_r($inventIdArr);
        //$inventory = $this->InvInventory->getInventoryById($inventory_id);
		//$inventory_id =2;
	   //$invidArr = array(2,4);
	   $inventory = $this->InvInventory->find('all', array(
            'fields' => array(),			
            'conditions' => array('InvInventory.id'=>$inventIdArr)
        ));
		//echo '<pre>'; print_r($inventory);
		//die();
		
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
		
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;


        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $inventory_property = $this->InvInventoryProperty->getInventoryProperty($inventory_id);
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $temp_arr = array();
        if (is_array($inventory_property) && count($inventory_property) > 0) {
            foreach ($inventory_property as $property) {
                $temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
            }
        }
		
		
		$inventory1 = $this->InvInventory->find('all', array(
            'fields' => array(),
            'conditions' => array('InvInventory.id'=>$inventory_id)
        ));
		//echo '<pre>'; print_r($inventory1);die();
		$this->set('inventory1', $inventory1);
		
        $inventory_property_arr = $temp_arr;
        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
        $this->set('inventory_property', $inventory_property);
        $this->set('inventory_property_arr', $inventory_property_arr);
		$this->set('inventory_id', $inventory_id);
		
		
		foreach($inventory_property_arr as $key=>$row_el){
		
		$array[$key]['floor_id'] = $key == '0' ? 'G' : str_pad($key, 3, "0", STR_PAD_LEFT);
		//$row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
			//if (is_array($row_el) && count($row_el) > 0) {
                foreach($row_el as $row){
	//				$array1[$key][$key1]['floor_no'] = $row_el1['flat_no'];
//$array[$key]['floor_data'][] = $row;

				$flat_no = str_pad($row['flat_no'], 3, "0", STR_PAD_LEFT);
				$flat_area = isset($row['flat_area']) ? $row['flat_area'] . ' Sqft.' : "N/A";
				$config = isset($row['flat_bhk']) && strlen($row['flat_bhk']) > 3 ? $row['flat_bhk'] : 'N/A';
				$status_code = isset($row['flat_status']) ? $row['flat_status'] : "N/A";
				$status = isset($row['flat_status']) ? $statuses[$row['flat_status']] : "N/A";
				$flat_pos_arr = !empty($row['flat_position']) ? json_decode($row['flat_position'], true) : array();
                                                                $temp_pos_arr = array();
                                                                if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
                                                                    foreach($flat_pos_arr as $pos_el) {
                                                                        $temp_pos_arr[] = $positions[$pos_el];
                                                                    }
                                                                }
                                                                if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
                                                                    $position = implode(", ",$temp_pos_arr);
                                                                } else {
                                                                    $position = "N/A";
                                                                }

				$array[$key]['floor_data'][] = array('flat_no'=>$flat_no,
										'area'=>$flat_area,
										'config'=>$config,
										'status_code'=>$status_code,
										'status'=>$status,
										'position'=>$position
										);					
				}		
			}
		
		//echo '<pre>'; print_r($array); die();
		$message = "Success";
		$code = 1;
		$resp = array("message"=>$message,"code"=>$code,"data"=>$array);
		//print json_encode($resp);
		//echo '<pre>'; print_r($array); die();
        
        # summarry
        $flat_counter = $this->InvInventoryProperty->getCounterByInventoryId($inventory_id);
        $flat_count_by_status = $this->InvInventoryProperty->getCounterByStatus($inventory_id);
        
        $this->set('flat_counter', $flat_counter);
        $this->set('flat_count_by_status', $flat_count_by_status);
		//echo '<pre>'; print_r($this->params);
		if(isset($this->params["print"])) {
			//echo '884';
			$this->layout = 'empty';
			//$this->autoLayout = false;
			//$this->render("view_inventory_floors_html");
			require_once(ROOT . DS . "vendors/pdfapi".DS."tcpdf.php");
				$this->view = 'pdf_inv';
		}
		
        //die();
    }
	
	
	public function viewInventoryFloors($inventory_id = null) {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        //$this->layout = 'account';
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
		
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;


        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $inventory_property = $this->InvInventoryProperty->getInventoryProperty($inventory_id);
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $temp_arr = array();
        if (is_array($inventory_property) && count($inventory_property) > 0) {
            foreach ($inventory_property as $property) {
                $temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
            }
        }
		
        $inventory_property_arr = $temp_arr;
        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
        $this->set('inventory_property', $inventory_property);
        $this->set('inventory_property_arr', $inventory_property_arr);
		
		
		foreach($inventory_property_arr as $key=>$row_el){
		
		$array[$key]['floor_id'] = $key == '0' ? 'G' : str_pad($key, 3, "0", STR_PAD_LEFT);
		//$row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
			//if (is_array($row_el) && count($row_el) > 0) {
                foreach($row_el as $row){
	//				$array1[$key][$key1]['floor_no'] = $row_el1['flat_no'];
//$array[$key]['floor_data'][] = $row;

				$flat_no = str_pad($row['flat_no'], 3, "0", STR_PAD_LEFT);
				$flat_area = isset($row['flat_area']) ? $row['flat_area'] . ' Sqft.' : "N/A";
				$config = isset($row['flat_bhk']) && strlen($row['flat_bhk']) > 3 ? $row['flat_bhk'] : 'N/A';
				$status_code = isset($row['flat_status']) ? $row['flat_status'] : "N/A";
				$status = isset($row['flat_status']) ? $statuses[$row['flat_status']] : "N/A";
				$flat_pos_arr = !empty($row['flat_position']) ? json_decode($row['flat_position'], true) : array();
                                                                $temp_pos_arr = array();
                                                                if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
                                                                    foreach($flat_pos_arr as $pos_el) {
                                                                        $temp_pos_arr[] = $positions[$pos_el];
                                                                    }
                                                                }
                                                                if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
                                                                    $position = implode(", ",$temp_pos_arr);
                                                                } else {
                                                                    $position = "N/A";
                                                                }

				$array[$key]['floor_data'][] = array('flat_no'=>$flat_no,
										'area'=>$flat_area,
										'config'=>$config,
										'status_code'=>$status_code,
										'status'=>$status,
										'position'=>$position
										);					
				}		
			}
		
		//echo '<pre>'; print_r($array); die();
		$message = "Success";
		$code = 1;
		$resp = array("message"=>$message,"code"=>$code,"data"=>$array);
		//print json_encode($resp);
		//echo '<pre>'; print_r($array); die();
        
        # summarry
        $flat_counter = $this->InvInventoryProperty->getCounterByInventoryId($inventory_id);
        $flat_count_by_status = $this->InvInventoryProperty->getCounterByStatus($inventory_id);
        
        $this->set('flat_counter', $flat_counter);
        $this->set('flat_count_by_status', $flat_count_by_status);
        //die();
    }
	

    public function viewInventoryFloorsTesting($inventory_id = null) {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        //$this->layout = 'account';
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
		
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;


        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $inventory_property = $this->InvInventoryProperty->getInventoryProperty($inventory_id);
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $temp_arr = array();
        if (is_array($inventory_property) && count($inventory_property) > 0) {
            foreach ($inventory_property as $property) {
                $temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
            }
        }
		
        $inventory_property_arr = $temp_arr;
        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
        $this->set('inventory_property', $inventory_property);
        $this->set('inventory_property_arr', $inventory_property_arr);
		//echo '<pre>'; print_r($position_arr);die();
		
		foreach($inventory_property_arr as $key=>$row_el){
		
		$array[$key]['floor_id'] = $key == '0' ? 'G' : str_pad($key, 3, "0", STR_PAD_LEFT);
		//$row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
			//if (is_array($row_el) && count($row_el) > 0) {
                foreach($row_el as $row){
	//				$array1[$key][$key1]['floor_no'] = $row_el1['flat_no'];
//$array[$key]['floor_data'][] = $row;

				$flat_no = str_pad($row['flat_no'], 3, "0", STR_PAD_LEFT);
				$flat_area = isset($row['flat_area']) ? $row['flat_area'] . ' Sqft.' : "N/A";
				$config = isset($row['flat_bhk']) && strlen($row['flat_bhk']) > 3 ? $row['flat_bhk'] : 'N/A';
				$status_code = isset($row['flat_status']) ? $row['flat_status'] : "N/A";
				$status = isset($row['flat_status']) ? $statuses[$row['flat_status']] : "N/A";
				$flat_pos_arr = !empty($row['flat_position']) ? json_decode($row['flat_position'], true) : array();
                                                                $temp_pos_arr = array();
                                                                if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
                                                                    foreach($flat_pos_arr as $pos_el) {
                                                                        $temp_pos_arr[] = $positions[$pos_el];
                                                                    }
                                                                }
                                                                if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
                                                                    $position = implode(", ",$temp_pos_arr);
                                                                } else {
                                                                    $position = "N/A";
                                                                }

				$array[$key]['floor_data'][] = array('flat_no'=>$flat_no,
										'area'=>$flat_area,
										'config'=>$config,
										'status_code'=>$status_code,
										'status'=>$status,
										'position'=>$position
										);					
				}		
			}
		
		//echo '<pre>'; print_r($array); die();
		$message = "Success";
		$code = 1;
		$resp = array("message"=>$message,"code"=>$code,"data"=>$array);
		//print json_encode($resp);
		//echo '<pre>'; print_r($array); die();
        
        # summarry
        $flat_counter = $this->InvInventoryProperty->getCounterByInventoryId($inventory_id);
        $flat_count_by_status = $this->InvInventoryProperty->getCounterByStatus($inventory_id);
        
        $this->set('flat_counter', $flat_counter);
        $this->set('flat_count_by_status', $flat_count_by_status);
        //die();
    }

    public function editPropertyInventoryForm() {
        $this->autoRender = false;
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $inv_property_id = $this->request->data['id'];
        $inventory_property = $this->InvInventoryProperty->getInventoryPropertyById($inv_property_id);


        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;

        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $modal_title = isset($inventory_property[0]['InvInventoryProperty']['floor_no']) && $inventory_property[0]['InvInventoryProperty']['floor_no'] == '0' ? 'G' : str_pad($inventory_property[0]['InvInventoryProperty']['floor_no'], 3, '0', STR_PAD_LEFT);
        $modal_title = "Edit info for Floor - {$modal_title}";

        $view = new View($this, false);
        $view->set(compact('some', 'vars'));
        $view->set('areas', $area_arr);
        $view->set('bhks', $bhk_arr);
        $view->set('statuses', $status_arr);
        $view->set('positions', $position_arr);
        $view->set('inventory_property', $inventory_property);
        $html = $view->render();

        $ret_arr = array("modal_title" => $modal_title, "html" => $html);
        echo json_encode($ret_arr);
        exit;
    }

    public function savePropertyInventory() {
        $data_arr = array();
        $data_arr['InvInventoryProperty']['id'] = isset($this->request->data['id']) ? $this->request->data['id'] : false;
        $data_arr['InvInventoryProperty']['flat_no'] = isset($this->request->data['flat_no']) ? $this->request->data['flat_no'] : false;
        $data_arr['InvInventoryProperty']['flat_area'] = isset($this->request->data['flat_area']) ? $this->request->data['flat_area'] : false;
        $data_arr['InvInventoryProperty']['flat_bhk'] = isset($this->request->data['flat_bhk']) ? $this->request->data['flat_bhk'] : false;
        $data_arr['InvInventoryProperty']['flat_status'] = isset($this->request->data['flat_status']) ? $this->request->data['flat_status'] : false;
        $data_arr['InvInventoryProperty']['flat_position'] = isset($this->request->data['flat_position']) ? json_encode($this->request->data['flat_position']) : '';
        if (isset($data_arr['InvInventoryProperty']['id']) && !empty($data_arr['InvInventoryProperty']['id'])) {
            $this->InvInventoryProperty->create();
            $this->InvInventoryProperty->save($data_arr);
            echo json_encode(array("status" => "1"));
            exit;
        }
        echo json_encode(array("status" => "0"));
        exit;
    }
    
    public function editInventory($inventory_id = null){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        # get projects
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $projects = $this->InvProject->getProjectByUser($builder_id);
        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $this->set('projects', $projects);
        $this->set('inventory', $inventory);
    }
    
    public function deleteInventory($inventory_id = null){
        $this->autoRender = false;
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        # delete inventory properties
        $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id), false);
        
        # delete inventory
        $this->InvInventory->deleteAll(array("InvInventory.id"=>$inventory_id), false);
        $this->Session->setFlash('Inventory deleted!');
        return $this->redirect('/accounts/inventory');
        
    }
    public function shareListProjects(){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        
        $projects = $this->InvProject->getProjectByUserId($builder_id);
        $this->set('projects', $projects);
		//echo $builder_id;
		$this->set('builder_id', $builder_id);
    }
    public function shareProjectInfo($project_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			if(isset($ShareProjectsHistoryData) && !empty($ShareProjectsHistoryData)){
				
				$this->ShareProjectsHistory->updateAll(
                array(
                    'office_maxdis_check'=>$this->request->data['ShareProjectsHistory']['office_maxdis_check'],
                    'office_brkg_fp_check'=>$this->request->data['ShareProjectsHistory']['office_brkg_fp_check'],
                    'payment_release_check'=>$this->request->data['ShareProjectsHistory']['payment_release_check'],
					'brochure_check'=>$this->request->data['ShareProjectsHistory']['brochure_check'],
					'price_list_check'=>$this->request->data['ShareProjectsHistory']['price_list_check'],
					'builder_buyer_agreement_check'=>$this->request->data['ShareProjectsHistory']['builder_buyer_agreement_check'],
					'upload_doc_check'=>$this->request->data['ShareProjectsHistory']['upload_doc_check'],
					'message'=>"'{$this->request->data['ShareProjectsHistory']['message']}'"
                ),
                array(
                    'id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']
                ));
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
				
							
			}else{
				if(isset($this->request->data['btn_share_SB']) && $this->request->data['btn_share_SB'] != ''){
					$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);			
					$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
					$this->Session->setFlash('Now, Please Assign Broker.');
					return $this->redirect('/accounts/projects/share-project-to-brokers/'.$lastinsertId);
				}
				
				
				
			}
			
		}
		
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		
		
		//$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		/*
		
		echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
		if(!empty($ShareProjectsHistoryData) && $ShareProjectsHistoryData !=''){			
			//echo 'array';
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->ShareProjectsHistory->updateAll(array('id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']),array('project_name'=>'xyz'));
				$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
				
//exit();				
			//}else{
				//$this->request->data['ShareProjectsHistory'] = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
			//}
			//$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
			//exit();
		}
		}else{
			//$this->request->data['ShareProjectsHistory1']
			//echo '<pre>'; print_r($this->request->data);die();
			if ($this->request->is('post')) {
				$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			}
		}
	
		
		
		/*if ($this->request->is('post')) {			
			//$this->ShareProjectHistory->create();
			//$this->request->data['ShareProjectHistory']['builder_id'] = 'digpal singh';
			//print_r($this->request->data['ShareProjectHistory']);
			//echo '<pre>'; print_r($this->request->data);die();
			$this->ShareProjectsHistory->create();
            $this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			die();
			
		}*/
    }
	
	
	public function shareProjectInfoUpdate($project_id){
		
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>'xyz'));
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>$project_id));
		//echo '<pre>'; print_r($this->request->data);echo '884';
		 
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		
		//echo '<pre>'; print_r($ShareProjectsHistoryData);echo '884';
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			//$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			//echo '<pre>'; print_r($ShareProjectsHistoryData);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']; die();
			//$this->ShareProjectsHistory->id = $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//$this->ShareProjectsHistory->save($this->request->data);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//echo $this->request->data['ShareProjectsHistory']['office_maxdis_check'];
			//die();
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];die();
			$this->ShareProjectsHistory->updateAll(
                array(
                    'office_maxdis_check'=>$this->request->data['ShareProjectsHistory']['office_maxdis_check'],
                    'office_brkg_fp_check'=>$this->request->data['ShareProjectsHistory']['office_brkg_fp_check'],
                    'payment_release_check'=>$this->request->data['ShareProjectsHistory']['payment_release_check'],
					'brochure_check'=>$this->request->data['ShareProjectsHistory']['brochure_check'],
					'price_list_check'=>$this->request->data['ShareProjectsHistory']['price_list_check'],
					'builder_buyer_agreement_check'=>$this->request->data['ShareProjectsHistory']['builder_buyer_agreement_check'],
					'upload_doc_check'=>$this->request->data['ShareProjectsHistory']['upload_doc_check'],
					'message'=>"'{$this->request->data['ShareProjectsHistory']['message']}'"
                ),
                array(
                    'id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']
                ));
			
			//$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		}
		
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		//echo '<pre>'; print_r($this->request->data);echo '884';
		
    }
	
	public function shareProjectInfoAppUsersSales($project_id){
        
		if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistoriesSales');
		
		$ShareProjectsHistoryData = $this->ShareProjectsHistoriesSales->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			if(isset($ShareProjectsHistoryData) && !empty($ShareProjectsHistoryData)){
				
				$this->ShareProjectsHistoriesSales->updateAll(
                array(
                    'office_maxdis_check'=>$this->request->data['ShareProjectsHistoriesSales']['office_maxdis_check'],
                    'office_brkg_fp_check'=>$this->request->data['ShareProjectsHistoriesSales']['office_brkg_fp_check'],
                    'payment_release_check'=>$this->request->data['ShareProjectsHistoriesSales']['payment_release_check'],
					'brochure_check'=>$this->request->data['ShareProjectsHistoriesSales']['brochure_check'],
					'price_list_check'=>$this->request->data['ShareProjectsHistoriesSales']['price_list_check'],
					'builder_buyer_agreement_check'=>$this->request->data['ShareProjectsHistoriesSales']['builder_buyer_agreement_check'],
					'upload_doc_check'=>$this->request->data['ShareProjectsHistoriesSales']['upload_doc_check'],
					'message'=>"'{$this->request->data['ShareProjectsHistoriesSales']['message']}'"
                ),
                array(
                    'id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistoriesSales']['id']
                ));
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-sales/'.$ShareProjectsHistoryData[0]['ShareProjectsHistoriesSales']['id']);
				
							
			}else{
				if(isset($this->request->data['btn_share_brokers']) && $this->request->data['btn_share_brokers'] != ''){
					$this->ShareProjectsHistoriesSales->save($this->request->data['ShareProjectsHistoriesSales']);			
					$lastinsertId = $this->ShareProjectsHistoriesSales->getLastInsertId();
					$this->Session->setFlash('Now, Please Assign Broker.');
					return $this->redirect('/accounts/projects/share-project-to-sales/'.$lastinsertId);
				}
				
			}
			
		}
		
		$this->request->data = $this->ShareProjectsHistoriesSales->findById($ShareProjectsHistoryData[0]['ShareProjectsHistoriesSales']['id']);
		
		
    }
	
	
	public function shareProjectInfoAppUsersBrokersAll($project_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			
			if(isset($this->request->data['btn_share_SB']) && $this->request->data['btn_share_SB'] != ''){
				$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);			
				$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
				$this->Session->setFlash('Now, Please Assign Broker.');
				return $this->redirect('/accounts/projects/share-project-to-brokers/'.$lastinsertId);
			}
			
			if(isset($this->request->data['btn_share_SE']) && $this->request->data['btn_share_SE'] != ''){
				$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);			
				$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
				$this->Session->setFlash('Now, Please Assign Sales Employees.');
				return $this->redirect('/accounts/projects/share-project-to-sales/'.$lastinsertId);
			}
			
			
		}
		
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		
    }
	
	
	public function viewProjectsBroker($project_id){
		
       
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>'xyz'));
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>$project_id));
		//echo '<pre>'; print_r($this->request->data);echo '884';
		 
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		
		//echo '<pre>'; print_r($ShareProjectsHistoryData);echo '884';
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			//$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			//echo '<pre>'; print_r($ShareProjectsHistoryData);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']; die();
			//$this->ShareProjectsHistory->id = $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//$this->ShareProjectsHistory->save($this->request->data);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//echo $this->request->data['ShareProjectsHistory']['office_maxdis_check'];
			//die();
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];die();
			$this->ShareProjectsHistory->updateAll(
                array(
                    'office_maxdis_check'=>$this->request->data['ShareProjectsHistory']['office_maxdis_check'],
                    'office_brkg_fp_check'=>$this->request->data['ShareProjectsHistory']['office_brkg_fp_check'],
                    'payment_release_check'=>$this->request->data['ShareProjectsHistory']['payment_release_check'],
					'brochure_check'=>$this->request->data['ShareProjectsHistory']['brochure_check'],
					'price_list_check'=>$this->request->data['ShareProjectsHistory']['price_list_check'],
					'builder_buyer_agreement_check'=>$this->request->data['ShareProjectsHistory']['builder_buyer_agreement_check'],
					'upload_doc_check'=>$this->request->data['ShareProjectsHistory']['upload_doc_check']
                ),
                array(
                    'id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']
                ));
			
			//$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		}
		
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		//echo '<pre>'; print_r($this->request->data);echo '884';
		
    }
	
	public function shareProjectToBrokers($share_project_info_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		$this->loadModel('SalesUser');
		   $BuilderBrokersList = $this->SalesUser->find('all', array(
					'joins' => array(
						array(
							'table' => 'builder_brokers',
							'type' => 'LEFT',
							'conditions' => array(
								'builder_brokers.broker_id = SalesUser.id'
							)
						)
					),
					'conditions' => array(
						'builder_brokers.builder_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
				
		$this->set('BuilderBrokersList', $BuilderBrokersList);
        
		$builder_id = CakeSession::read('Auth.Websiteuser.id');
		$this->set('share_project_info_id', $share_project_info_id);
		$this->set('builder_id', $builder_id);
		$this->loadModel('BuilderBrokerShareProject');
		$this->loadModel('ShareProjectsHistory');
		//$this->set('broker_id', $builder_id);
		//$projectid = 701;
		
		
		$getProjectId = $this->ShareProjectsHistory->find('all', array(					
					'conditions' => array(
						'id' => $share_project_info_id
					)
				));
		//echo '<pre>'; print_r($getProjectId);
		//echo $getProjectId[0]['ShareProjectsHistory']['id'];
		
		if ($this->request->is('post')) {
			$this->BuilderBrokerShareProject->query("Delete from `builder_broker_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistory']['project_id'] . "");
		
			
			for ($i = 0; $i <= count($this->request->data['shareprojects1'])/2-1; $i++) {
				//$i=0;
				//foreach($this->request->data['shareprojects'] as $data){
					//if($this->request->data['shareprojects1']['broker_check'.$i.''] == 1){
						$this->request->data['BuilderBrokerShareProject']['share_project_history_id'] = $share_project_info_id;
						$this->request->data['BuilderBrokerShareProject']['builder_id'] = $builder_id;
						$this->request->data['BuilderBrokerShareProject']['broker_id'] = $this->request->data['shareprojects1']['broker_id'.$i.''];
						$this->request->data['BuilderBrokerShareProject']['project_id'] = $getProjectId[0]['ShareProjectsHistory']['project_id'];
						$this->request->data['BuilderBrokerShareProject']['status'] = $this->request->data['shareprojects1']['broker_check'.$i.''];
					//}
			$this->BuilderBrokerShareProject->create();
			$this->BuilderBrokerShareProject->save($this->request->data['BuilderBrokerShareProject']);	

			
				
			}
			//print_r(json_encode($this->request->data['shareprojects'], true));
			$brokersJson = json_encode($this->request->data['shareprojects1'], true);
			//echo $share_project_info_id;die();
			$this->ShareProjectsHistory->updateAll(
                array(
                    'brokers_assign'=>"'{$brokersJson}'"
                ),
                array(
                    'id'=>$share_project_info_id
                ));
			
			$this->Session->setFlash('Projects Shared Successfully.', 'default', array('class' => 'green'));
		}
		$getArrayForBrokerChecks = $this->ShareProjectsHistory->find('all', array(					
					'conditions' => array(
						'id' => $share_project_info_id
					)
				));
		
		//print_r($getArrayForBrokerChecks);
		$this->request->data['shareprojects1'] = json_decode($getArrayForBrokerChecks[0]['ShareProjectsHistory']['brokers_assign'], true);
		//$this->request->data['shareprojects'] = $this->request->data['BuilderBrokerShareProject'];
		
		
		//$getProjectId11 = $this->ShareProjectsHistory->find('all');
    }
	
	
	public function shareProjectToSales($share_project_info_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		$this->loadModel('SalesUser');
		   $BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'user_type' => 'sales'
					)
				));
				
		$this->set('BuilderSalesList', $BuilderSalesList);
        
		$builder_id = CakeSession::read('Auth.Websiteuser.id');
		$this->set('share_project_info_id', $share_project_info_id);
		$this->set('builder_id', $builder_id);
		$this->loadModel('BuilderSalesShareProjects');
		$this->loadModel('ShareProjectsHistoriesSales');
		//$this->set('broker_id', $builder_id);
		//$projectid = 701;
		
		
		$getProjectId = $this->ShareProjectsHistoriesSales->find('all', array(					
					'conditions' => array(
						'id' => $share_project_info_id
					)
				));
		//echo '<pre>'; print_r($getProjectId);
		//echo $getProjectId[0]['ShareProjectsHistoriesSales']['id'];
		
		if ($this->request->is('post')) {
			$this->BuilderSalesShareProjects->query("Delete from `builder_sales_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistoriesSales']['project_id'] . "");
		
			
			for ($i = 0; $i <= count($this->request->data['shareprojects1'])/2-1; $i++) {
				//$i=0;
				//foreach($this->request->data['shareprojects'] as $data){
					//if($this->request->data['shareprojects1']['broker_check'.$i.''] == 1){
						$this->request->data['BuilderSalesShareProjects']['share_project_history_id'] = $share_project_info_id;
						$this->request->data['BuilderSalesShareProjects']['builder_id'] = $builder_id;
						$this->request->data['BuilderSalesShareProjects']['broker_id'] = $this->request->data['shareprojects1']['broker_id'.$i.''];
						$this->request->data['BuilderSalesShareProjects']['project_id'] = $getProjectId[0]['ShareProjectsHistoriesSales']['project_id'];
						$this->request->data['BuilderSalesShareProjects']['status'] = $this->request->data['shareprojects1']['broker_check'.$i.''];
					//}
			$this->BuilderSalesShareProjects->create();
			$this->BuilderSalesShareProjects->save($this->request->data['BuilderSalesShareProjects']);	

			
				
			}
			//print_r(json_encode($this->request->data['shareprojects'], true));
			$brokersJson = json_encode($this->request->data['shareprojects1'], true);
			//echo $share_project_info_id;die();
			$this->ShareProjectsHistoriesSales->updateAll(
                array(
                    'brokers_assign'=>"'{$brokersJson}'"
                ),
                array(
                    'id'=>$share_project_info_id
                ));
		
			$this->Session->setFlash('Projects Shared Successfully.', 'default', array('class' => 'green'));
			
		}
		$getArrayForBrokerChecks = $this->ShareProjectsHistoriesSales->find('all', array(					
					'conditions' => array(
						'id' => $share_project_info_id
					)
				));
		
		//print_r($getArrayForBrokerChecks);
		$this->request->data['shareprojects1'] = json_decode($getArrayForBrokerChecks[0]['ShareProjectsHistoriesSales']['brokers_assign'], true);
		//$this->request->data['shareprojects'] = $this->request->data['BuilderBrokerShareProject'];
		
		
		//$getProjectId11 = $this->ShareProjectsHistory->find('all');
    }
	
	
	public function shareInventoryToBrokers($inventory_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
		$this->set('inventory_id', $inventory_id);
		//die();
		$this->loadModel('SalesUser');
		   $BuilderBrokersList = $this->SalesUser->find('all', array(
					'joins' => array(
						array(
							'table' => 'builder_brokers',
							'type' => 'LEFT',
							'conditions' => array(
								'builder_brokers.broker_id = SalesUser.id'
							)
						)
					),
					'conditions' => array(
						'builder_brokers.builder_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
				
		$this->set('BuilderBrokersList', $BuilderBrokersList);
		$this->loadModel('BuilderBrokerShareInventory');
		$this->loadModel('ShareInventoryHistory');
		
		$inventoryArray = $this->ShareInventoryHistory->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'inventory_id' => $inventory_id
					)
				));
				//echo '<pre>'; print_r($inventoryArray); die();
		
		if ($this->request->is('post')) {
			//$this->BuilderBrokerShareInventory->query("Delete from `builder_broker_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistory']['project_id'] . "");
		//echo '<pre>'; print_r($this->request->data); die();
		
				
				//echo '<pre>'; print_r($inventoryArray); die();
				$brokersJson = json_encode($this->request->data['shareprojects1'], true);
				//echo $brokersJson; die();
				if(!empty($inventoryArray)){
					//$brokersJson = json_encode($this->request->data['shareprojects1'], true);
					$this->ShareInventoryHistory->updateAll(
						array(
							'assign_brokers'=>"'{$brokersJson}'"
						),
						array(
							'builder_id'=>$this->Session->read('Auth.Websiteuser.id'),
							'inventory_id'=>$inventory_id,
						));
					
					//
					
				}else{
					$this->request->data['ShareInventoryHistory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
					$this->request->data['ShareInventoryHistory']['inventory_id'] = $inventory_id;
					$this->request->data['ShareInventoryHistory']['assign_brokers'] = $brokersJson;
					$this->request->data['ShareInventoryHistory']['status'] = 1;
					//}			
					//echo '<pre>'; print_r($this->request->data['ShareInventoryHistory']); die();
					$this->ShareInventoryHistory->create();
					$this->ShareInventoryHistory->save($this->request->data['ShareInventoryHistory']);
					
				}
		
				
		
		$this->BuilderBrokerShareInventory->query("Delete from `builder_broker_share_inventories` WHERE `builder_id` = " . $builder_id . " and `inventory_id` = " . $inventory_id . "");
			
			for ($i = 0; $i <= count($this->request->data['shareprojects1'])/2-1; $i++) {
				
				$this->request->data['BuilderBrokerShareInventory']['share_project_history_id'] = $inventory_id;
				$this->request->data['BuilderBrokerShareInventory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['BuilderBrokerShareInventory']['inventory_id'] = $inventory_id;
				$this->request->data['BuilderBrokerShareInventory']['broker_id'] = $this->request->data['shareprojects1']['broker_id'.$i.''];
				$this->request->data['BuilderBrokerShareInventory']['status'] = $this->request->data['shareprojects1']['broker_check'.$i.''];
					//}
			
			$this->BuilderBrokerShareInventory->create();
			$this->BuilderBrokerShareInventory->save($this->request->data['BuilderBrokerShareInventory']);	

			
				
			}
		$this->Session->setFlash('Inventory Shared Successfully.', 'default', array('class' => 'green'));			
		}
		$this->request->data['shareprojects1'] = json_decode($inventoryArray[0]['ShareInventoryHistory']['assign_brokers'], true);
	}
	
	
	public function shareInventoryToSales($inventory_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
		$this->set('inventory_id', $inventory_id);
		//die();
		$this->loadModel('SalesUser');
		   $BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'user_type' => 'sales'
					)
				));
			//echo '<pre>'; print_r($BuilderSalesList); die();	
		$this->set('BuilderSalesList', $BuilderSalesList);
		$this->loadModel('BuilderSalesShareInventory');
		$this->loadModel('ShareSalesInventoryHistory');
		
		$inventoryArray = $this->ShareSalesInventoryHistory->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'inventory_id' => $inventory_id
					)
				));
				//echo '<pre>'; print_r($inventoryArray); die();
		
		if ($this->request->is('post')) {
			//$this->BuilderBrokerShareInventory->query("Delete from `builder_broker_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistory']['project_id'] . "");
		//echo '<pre>'; print_r($this->request->data); die();
		
				
				//echo '<pre>'; print_r($inventoryArray); die();
				$salesJson = json_encode($this->request->data['shareinv1'], true);
				//echo $brokersJson; die();
				if(!empty($inventoryArray)){
					//$brokersJson = json_encode($this->request->data['shareprojects1'], true);
					$this->ShareSalesInventoryHistory->updateAll(
						array(
							'assign_sales'=>"'{$salesJson}'"
						),
						array(
							'builder_id'=>$this->Session->read('Auth.Websiteuser.id'),
							'inventory_id'=>$inventory_id,
						));
					
					//
					
				}else{
					$this->request->data['ShareSalesInventoryHistory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
					$this->request->data['ShareSalesInventoryHistory']['inventory_id'] = $inventory_id;
					$this->request->data['ShareSalesInventoryHistory']['assign_sales'] = $salesJson;
					$this->request->data['ShareSalesInventoryHistory']['status'] = 1;
					//}			
					//echo '<pre>'; print_r($this->request->data['ShareInventoryHistory']); die();
					$this->ShareSalesInventoryHistory->create();
					$this->ShareSalesInventoryHistory->save($this->request->data['ShareSalesInventoryHistory']);
					
				}
		
				
		
		$this->BuilderSalesShareInventory->query("Delete from `builder_sales_share_inventories` WHERE `builder_id` = " . $builder_id . " and `inventory_id` = " . $inventory_id . "");
			
			for ($i = 0; $i <= count($this->request->data['shareinv1'])/2-1; $i++) {
				
				$this->request->data['BuilderSalesShareInventory']['share_inventory_history_id'] = $inventory_id;
				$this->request->data['BuilderSalesShareInventory']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['BuilderSalesShareInventory']['inventory_id'] = $inventory_id;
				$this->request->data['BuilderSalesShareInventory']['sales_id'] = $this->request->data['shareinv1']['sales_id'.$i.''];
				$this->request->data['BuilderSalesShareInventory']['status'] = $this->request->data['shareinv1']['sales_check'.$i.''];
				$this->request->data['BuilderSalesShareInventory']['edit_permission'] = $this->request->data['shareperm1']['perm_check'.$i.''];
					//}
			
			$this->BuilderSalesShareInventory->create();
			$this->BuilderSalesShareInventory->save($this->request->data['BuilderSalesShareInventory']);	

			
				
			}
			$this->Session->setFlash('Inventory Shared Successfully.', 'default', array('class' => 'green'));
		//$this->redirect('/accounts/inventory/share_inventory_to_sales/'.$inventory_id);		
		}
		$this->request->data['shareinv1'] = json_decode($inventoryArray[0]['ShareSalesInventoryHistory']['assign_sales'], true);
		
	}
	
	
	
	public function shareMessageAppusersAll($project_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';		
		$this->loadModel('AppusersMessage');
		$this->loadModel('SalesUser');		
		
		$AppusersMessage = $this->AppusersMessage->find('all');
		//echo '<pre>'; print_r($AppusersMessage);		
		
		$buildersAllUsersArray = $this->SalesUser->find('all', array(
					'fields' => array('id'),
					'conditions' => array(						
						'builder_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
		//echo '<pre>'; print_r($buildersAllUsersArray);
		
		//for ($i = 0; $i <= count($buildersAllUsersArray)/2; $i++) {
		
		//echo count($buildersAllUsersArray);
		
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('MessageBoardAppusersAlls');
		if ($this->request->is('post') || $this->request->is('put')) {
			
			$this->request->data['MessageBoardAppusersAlls']['status'] = 0;
			
			$this->MessageBoardAppusersAlls->save($this->request->data['MessageBoardAppusersAlls']);			
					$lastinsertId = $this->MessageBoardAppusersAlls->getLastInsertId();
					
					for($i=0;$i < count($buildersAllUsersArray);$i++){
						//echo '884';
						$this->request->data['AppusersMessage']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['AppusersMessage']['project_id'] = $this->request->data['MessageBoardAppusersAlls']['project_id'];
						$this->request->data['AppusersMessage']['app_user_id'] = $buildersAllUsersArray[$i]['SalesUser']['id'];
						$this->request->data['AppusersMessage']['message_id'] = $lastinsertId;
						$this->request->data['AppusersMessage']['status'] = 0;
						$this->AppusersMessage->create();
						$this->AppusersMessage->save($this->request->data['AppusersMessage']);
					}
					
					
					$this->loadModel('MessageBoardNotification');
		
					$MessageBoardNotifications = $this->MessageBoardNotification->find('all', array(
						'conditions' => array(
							'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
							'status' => 'builder_message'
						)
					));
					if(isset($MessageBoardNotifications) && !empty($MessageBoardNotifications)){
						//echo '1';
						$this->MessageBoardNotification->id = $this->Session->read('Auth.Websiteuser.id');
						$this->MessageBoardNotification->updateAll(array(
						'MessageBoardNotification.counts' => 'MessageBoardNotification.counts + 1'),
						array('MessageBoardNotification.builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'MessageBoardNotification.status' => 'builder_message'));
						
					}else{
						//echo '2';
						$this->request->data['MessageBoardNotification']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
						$this->request->data['MessageBoardNotification']['counts'] = 1;
						$this->request->data['MessageBoardNotification']['status'] = 'builder_message';
						$this->MessageBoardNotification->save($this->request->data['MessageBoardNotification']);
						
					}
					
					$this->Session->setFlash('Shared Successfully.');
					//return $this->redirect('/accounts/projects/share-message-appusers-all/'.$lastinsertId);
					return $this->redirect('/accounts/projects/share-message-appusers-all');			
		}
		
		$MessageBoardAppusersAll = $this->MessageBoardAppusersAlls->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id')
            )
        ));
		$this->set('MessageBoardAppusersAll', $MessageBoardAppusersAll);
		
		$this->request->data = $this->MessageBoardAppusersAlls->findById($lastinsertId);		
		$this->loadModel('Project');		
		$getProjects = $this->Project->find('list',
			array('fields'=> array(
					'project_id',
					'project_name'			
				),
				  'conditions'=>array(
					'Project.approved' => '1', 'Project.user_id' => $this->Session->read('Auth.Websiteuser.id')
				)
			)
		);		
		
		
		$this->set('getProjects', $getProjects);
		
    }
	
	/*public function shareMessageAppusersAllAssign($share_message_info_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
		$this->set('inventory_id', $inventory_id);
		//die();
		$this->loadModel('SalesUser');
		   $BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'user_type' => 'sales'
					)
				));
			//echo '<pre>'; print_r($BuilderSalesList); die();	
		$this->set('BuilderSalesList', $BuilderSalesList);
		$this->loadModel('MessageBoardAppusersAllsHistories');
		//$this->loadModel('ShareSalesInventoryHistory');
		
		$inventoryArray = $this->ShareSalesInventoryHistory->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
						'inventory_id' => $inventory_id
					)
				));
				//echo '<pre>'; print_r($inventoryArray); die();
		
		if ($this->request->is('post')) {
			
		
		$this->MessageBoardAppusersAllsHistories->query("Delete from `builder_sales_share_inventories` WHERE `builder_id` = " . $builder_id . " and `inventory_id` = " . $inventory_id . "");
			
			for ($i = 0; $i <= count($this->request->data['shareinv1'])/2-1; $i++) {
				
				$this->request->data['MessageBoardAppusersAllsHistories']['share_inventory_history_id'] = $inventory_id;
				$this->request->data['MessageBoardAppusersAllsHistories']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				$this->request->data['MessageBoardAppusersAllsHistories']['inventory_id'] = $inventory_id;
				$this->request->data['MessageBoardAppusersAllsHistories']['sales_id'] = $this->request->data['shareinv1']['sales_id'.$i.''];
				$this->request->data['MessageBoardAppusersAllsHistories']['status'] = $this->request->data['shareinv1']['broker_check'.$i.''];
					//}
			
			$this->MessageBoardAppusersAllsHistories->create();
			$this->MessageBoardAppusersAllsHistories->save($this->request->data['MessageBoardAppusersAllsHistories']);	

			
				
			}
					
		}
		$this->request->data['shareinv1'] = json_decode($inventoryArray[0]['MessageBoardAppusersAllsHistories']['assign_sales'], true);
	}*/
	
	
	public function shareMessageAppusersAllAssign($share_message_info_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		
		$this->set('share_message_info_id', $share_message_info_id);
		//die();
		$this->loadModel('SalesUser');
		   $BuilderSalesList = $this->SalesUser->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
			//echo '<pre>'; print_r($BuilderSalesList); die();	
		$this->set('BuilderSalesList', $BuilderSalesList);
		$this->loadModel('MessageBoardAppusersAllsHistories');
		$this->loadModel('MessageBoardAppusersAlls');
		
		$MessageBoardAppusersAlls = $this->MessageBoardAppusersAlls->find('all', array(					
					'conditions' => array(
						'builder_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
				//echo '<pre>'; print_r($MessageBoardAppusersAlls); die();
		
		if ($this->request->is('post')) {
			//$this->BuilderBrokerShareInventory->query("Delete from `builder_broker_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistory']['project_id'] . "");
		//echo '<pre>'; print_r($this->request->data); die();
		
				
				//echo '<pre>'; print_r($this->request->data); die();
				$salesbrokerJson = json_encode($this->request->data['shareprojects1'], true);
				//echo $salesbrokerJson; die();
				if(!empty($MessageBoardAppusersAlls)){
					//$brokersJson = json_encode($this->request->data['shareprojects1'], true);
					$this->MessageBoardAppusersAlls->updateAll(
						array(
							'assign_broker_or_sales'=>"'{$salesbrokerJson}'"
						),
						array(
							'builder_id'=>$this->Session->read('Auth.Websiteuser.id')
						));
					
					//
					
				}
		
				
		
		$this->MessageBoardAppusersAllsHistories->query("Delete from `message_board_appusers_alls_histories` WHERE `builder_id` = " . $builder_id . "");
			
			for ($i = 0; $i <= count($this->request->data['shareprojects1'])/2-1; $i++) {
				
				$this->request->data['MessageBoardAppusersAllsHistories']['message_board_id'] = $share_message_info_id;
				$this->request->data['MessageBoardAppusersAllsHistories']['builder_id'] = $this->Session->read('Auth.Websiteuser.id');
				//$this->request->data['MessageBoardAppusersAllsHistories']['inventory_id'] = $inventory_id;
				$this->request->data['MessageBoardAppusersAllsHistories']['sales_or_broker_id'] = $this->request->data['shareprojects1']['sales_id'.$i.''];
				$this->request->data['MessageBoardAppusersAllsHistories']['status'] = $this->request->data['shareprojects1']['sales_check'.$i.''];
					//}
			
			$this->MessageBoardAppusersAllsHistories->create();
			$this->MessageBoardAppusersAllsHistories->save($this->request->data['MessageBoardAppusersAllsHistories']);	

			
				
			}
					
		}
		$this->request->data['shareprojects1'] = json_decode($MessageBoardAppusersAlls[0]['MessageBoardAppusersAlls']['assign_broker_or_sales'], true);
		//print_r($this->request->data['shareprojects1']);
	}
	
	
	public function getInventories1() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        
        //$inventories = $this->InvInventory->getInventoriesByUserId($builder_id);
		
		
		if (!empty($this->request->data['search'])) {
			$this->Paginator->settings = array(
				'fields' => array("InvInventory.*","InvProject.project_name"),
                'conditions' => array(
					'OR' => array(
                        'SalesUser.name LIKE' => $this->request->data['search'] . '%',
                        'SalesUser.email LIKE' => $this->request->data['search'] . '%',
                    ),
					"InvWebsiteuser.id = '{$builder_id}'"
				),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'order' => 'created_at DESC',
                'limit' => 8
            );
	    }else{
			$this->Paginator->settings = array(
                'fields' => array("InvInventory.*","InvProject.project_name"),
				'conditions' => array("InvWebsiteuser.id = '{$builder_id}'"),
				'joins' => array(
					array(
						'table' => 'projects',
						'alias' => 'InvProject',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvProject.project_id = InvInventory.project_id")
					),
					array(
						'table' => 'websiteusers',
						'alias' => 'InvWebsiteuser',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions' => array("InvWebsiteuser.id = InvProject.user_id")
					)
				),
				'order' => 'created_at DESC',
                'limit' => 8
            );
		}
		$inventories = $this->Paginator->paginate('InvInventory');
		
		
        $this->set('inventories', $inventories);
    }
	
}
