<?php
class InventoriesController extends InventoryManagerAppController {
    public $uses = array(
        'InventoryManager.InvProject',
        'InventoryManager.InvProperty',
        'InventoryManager.InvInventory',
        'InventoryManager.InvInventoryPropertyStatus',
        'InventoryManager.InvInventoryPropertyPosition',
        'InventoryManager.InvWebsiteuser',
        'InventoryManager.InvInventoryProperty');
    public $components = array('Session', 'InventoryManager.InventoryCustom');
	var $helpers = array('Number');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authorize = array();
        $this->Auth->allow('index', 'getInventories', 'addInventory', 'addInventoryFloor','shareProjectToBrokers','viewProjectsBroker');
    }

    public function index() {
        echo "inventory manager index";
        exit;
    }

    public function getInventories() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        
        $inventories = $this->InvInventory->getInventoriesByUserId($builder_id);
        $this->set('inventories', $inventories);
    }
    protected function modifyFloors($mode, $inventory_id, $floor_start, $floor_end){
        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $template = isset($inventory[0]['InvInventory']['template']) ? json_decode($inventory[0]['InvInventory']['template'], true) : array();
        
        if($mode == 'remove'){
            for($i = $floor_start; $i <= $floor_end; $i++){
                $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id, "InvInventoryProperty.floor_no" => $i), false);
            }
        }
        if($mode == 'add'){
            for ($i = intval($floor_start); $i <= intval($floor_end); $i++) {
                if (is_array($template['flat_no']) && count($template['flat_no']) > 0) {
                    foreach ($template['flat_no'] as $key => $row) {
                        $inv_arr['InvInventoryProperty']['inventory_id'] = $inventory_id;
                        $inv_arr['InvInventoryProperty']['floor_no'] = $i;
                        $inv_arr['InvInventoryProperty']['flat_no'] = $template['flat_no'][$key];
                        $inv_arr['InvInventoryProperty']['flat_area'] = $template['flat_area'][$key];
                        $inv_arr['InvInventoryProperty']['flat_bhk'] = $template['flat_bhk'][$key];
                        $inv_arr['InvInventoryProperty']['flat_status'] = $template['flat_status'][$key];
                        $inv_arr['InvInventoryProperty']['flat_position'] = isset($template['flat_position'][$key]) ? json_encode($template['flat_position'][$key]) : "";
                        $inv_arr['InvInventoryProperty']['created_at'] = date("Y-m-d H:i:s");
                        
                        try {
                            $this->InvInventoryProperty->create();
                            $this->InvInventoryProperty->save($inv_arr);
                        } catch (Exception $exc) {
                            echo $exc->getMessage(); exit;
                        }
                    }
                }
            }
        }
    }
    public function addInventory() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        # get projects
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $projects = $this->InvProject->getProjectByUser($builder_id);

        $this->set('projects', $projects);

        $mode = isset($this->request->data['mode']) ? $this->request->data['mode'] : "";

        if ($this->request->is('post') && $mode == 'process') {
            
            $save_data_arr['InvInventory'] = $this->request->data['Inventory'];
            $save_data_arr['InvInventory']['created_at'] = date("Y-m-d H:i:s");
            
            # check if any changes regarding flat layout
            $flush_flag = 1;
            
            if(isset($this->request->data['Inventory']['id'])){
                $flush_flag = 0;
                $inventory_row = $this->InvInventory->getInventoryById($this->request->data['Inventory']['id']);
                # chaeck changes in flats/floor or project id
                if(
                        (isset($inventory_row[0]['InvInventory']['flats_per_floor']) && 
                        $inventory_row[0]['InvInventory']['flats_per_floor'] != $save_data_arr['InvInventory']['flats_per_floor'])
                        ||
                        (isset($inventory_row[0]['InvInventory']['project_id']) && 
                        $inventory_row[0]['InvInventory']['project_id'] != $save_data_arr['InvInventory']['project_id'])        
                                
                ){
                    $flush_flag++;
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_start']) > intval($save_data_arr['InvInventory']['floor_start'])){
                    $this->modifyFloors('add',$this->request->data['Inventory']['id'], intval($save_data_arr['InvInventory']['floor_start']), intval($inventory_row[0]['InvInventory']['floor_start']) - 1);
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_start']) < intval($save_data_arr['InvInventory']['floor_start'])){
                    $this->modifyFloors('remove',$this->request->data['Inventory']['id'], intval($inventory_row[0]['InvInventory']['floor_start']), intval($save_data_arr['InvInventory']['floor_start']) - 1);
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_end']) < intval($save_data_arr['InvInventory']['floor_end'])){
                    $this->modifyFloors('add', $this->request->data['Inventory']['id'], intval($inventory_row[0]['InvInventory']['floor_end']) + 1, intval($save_data_arr['InvInventory']['floor_end']));
                }
                if(!$flush_flag && intval($inventory_row[0]['InvInventory']['floor_end']) > intval($save_data_arr['InvInventory']['floor_end'])){
                    $this->modifyFloors('remove', $this->request->data['Inventory']['id'], intval($save_data_arr['InvInventory']['floor_end']) + 1, intval($inventory_row[0]['InvInventory']['floor_end']));
                }
            }
             
            $this->InvInventory->create();
            if(intval($save_data_arr['InvInventory']['floor_start']) > 0){
                $save_data_arr['InvInventory']['floor_start']++;
            }
            if ($this->InvInventory->save($save_data_arr)) {
                $this->Session->setFlash('Inventory Saved!');
                $inventory_id = $this->InvInventory->id;
                if($flush_flag){
                    $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id), false);
                    return $this->redirect('/accounts/inventory/add_floor/' . $inventory_id);
                }
                return $this->redirect('/accounts/inventory1');
            }
        }
    }

    public function addInventoryFloor($inventory_id = null) {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');



        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);


        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
    }

    public function saveInventoryFloor() {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        # get projects
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $projects = $this->InvProject->getProjectByUser($builder_id);
        $this->set('projects', $projects);

        if ($this->request->is('post')) {
            $flat_no_arr = $this->request->data['InvInventoryProperty']['flat_no'];
            $flat_area_arr = $this->request->data['InvInventoryProperty']['flat_area'];
            $flat_bhk_arr = $this->request->data['InvInventoryProperty']['flat_bhk'];
            $flat_status_arr = $this->request->data['InvInventoryProperty']['flat_status'];
            $flat_position_arr = isset($this->request->data['InvInventoryProperty']['flat_position']) ? $this->request->data['InvInventoryProperty']['flat_position'] : array();

            $template_str = json_encode($this->request->data['InvInventoryProperty']);
            $inventory_id = $this->request->data['inventory_id'];
            $inventory = $this->InvInventory->getInventoryById($inventory_id);
            try {
                # update template
                $this->InvInventory->updateAll(array("InvInventory.template" => "'{$template_str}'"), array("InvInventory.id" => $inventory_id));

                # add all floors
                $start_floor = $inventory[0]['InvInventory']['floor_start'];
                $end_floor = $inventory[0]['InvInventory']['floor_end'];
                if ($start_floor == 'G') {
                    $start_floor = '0';
                }
                $inv_arr = array();
                $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id), false);
                for ($i = intval($start_floor); $i <= intval($end_floor); $i++) {
                    if (is_array($flat_no_arr) && count($flat_no_arr) > 0) {
                        foreach ($flat_no_arr as $key => $row) {
                            $inv_arr['InvInventoryProperty']['inventory_id'] = $inventory_id;
                            $inv_arr['InvInventoryProperty']['floor_no'] = $i;
                            $inv_arr['InvInventoryProperty']['flat_no'] = $flat_no_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_area'] = $flat_area_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_bhk'] = $flat_bhk_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_status'] = $flat_status_arr[$key];
                            $inv_arr['InvInventoryProperty']['flat_position'] = isset($flat_position_arr[$key]) ? json_encode($flat_position_arr[$key]) : "";
                            $inv_arr['InvInventoryProperty']['created_at'] = date("Y-m-d H:i:s");
                            $this->InvInventoryProperty->create();
                            $this->InvInventoryProperty->save($inv_arr);
                        }
                    }
                }
                $this->Session->setFlash('Inventory updated!');
                $url = '/accounts/inventory/view_inventory/' . $inventory_id;
                return $this->redirect($url);
            } catch (Exception $exc) {
                $this->Session->setFlash('Inventory could not be updated!', 'default', array('class' => 'error'));
            }
        }
        return $this->redirect('/accounts/inventory');
    }

    public function viewInventoryFloors($inventory_id = null) {
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');

        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $property_arr = $this->InvProperty->getPropertyByUser($builder_id);
        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;


        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $inventory_property = $this->InvInventoryProperty->getInventoryProperty($inventory_id);
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $temp_arr = array();
        if (is_array($inventory_property) && count($inventory_property) > 0) {
            foreach ($inventory_property as $property) {
                $temp_arr[$property['InvInventoryProperty']['floor_no']][] = $property['InvInventoryProperty'];
            }
        }
        $inventory_property_arr = $temp_arr;
        $this->set('inventory', $inventory);
        $this->set('properties', $property_arr);
        $this->set('areas', $area_arr);
        $this->set('bhks', $bhk_arr);
        $this->set('statuses', $status_arr);
        $this->set('positions', $position_arr);
        $this->set('user', $user);
        $this->set('inventory_property', $inventory_property);
        $this->set('inventory_property_arr', $inventory_property_arr);
        
        # summarry
        $flat_counter = $this->InvInventoryProperty->getCounterByInventoryId($inventory_id);
        $flat_count_by_status = $this->InvInventoryProperty->getCounterByStatus($inventory_id);
        
        $this->set('flat_counter', $flat_counter);
        $this->set('flat_count_by_status', $flat_count_by_status);
        
    }

    public function editPropertyInventoryForm() {
        $this->autoRender = false;
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $inv_property_id = $this->request->data['id'];
        $inventory_property = $this->InvInventoryProperty->getInventoryPropertyById($inv_property_id);


        $area_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['area']);
        $bhk_arr = array_unique($this->InvProperty->getPropertyByUser($builder_id)['bhk']);
        $status_arr = $this->InvInventoryPropertyStatus->getStatuses();
        $temp_arr = array();
        if (is_array($status_arr) && count($status_arr) > 0) {
            foreach ($status_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyStatus']['id']] = $row['InvInventoryPropertyStatus']['status'];
            }
        }
        $status_arr = $temp_arr;

        $position_arr = $this->InvInventoryPropertyPosition->getPositions();
        $temp_arr = array();
        if (is_array($position_arr) && count($position_arr) > 0) {
            foreach ($position_arr as $row) {
                $temp_arr[$row['InvInventoryPropertyPosition']['id']] = $row['InvInventoryPropertyPosition']['position'];
            }
        }
        $position_arr = $temp_arr;

        $modal_title = isset($inventory_property[0]['InvInventoryProperty']['floor_no']) && $inventory_property[0]['InvInventoryProperty']['floor_no'] == '0' ? 'G' : str_pad($inventory_property[0]['InvInventoryProperty']['floor_no'], 3, '0', STR_PAD_LEFT);
        $modal_title = "Edit info for Floor - {$modal_title}";

        $view = new View($this, false);
        $view->set(compact('some', 'vars'));
        $view->set('areas', $area_arr);
        $view->set('bhks', $bhk_arr);
        $view->set('statuses', $status_arr);
        $view->set('positions', $position_arr);
        $view->set('inventory_property', $inventory_property);
        $html = $view->render();

        $ret_arr = array("modal_title" => $modal_title, "html" => $html);
        echo json_encode($ret_arr);
        exit;
    }

    public function savePropertyInventory() {
        $data_arr = array();
        $data_arr['InvInventoryProperty']['id'] = isset($this->request->data['id']) ? $this->request->data['id'] : false;
        $data_arr['InvInventoryProperty']['flat_no'] = isset($this->request->data['flat_no']) ? $this->request->data['flat_no'] : false;
        $data_arr['InvInventoryProperty']['flat_area'] = isset($this->request->data['flat_area']) ? $this->request->data['flat_area'] : false;
        $data_arr['InvInventoryProperty']['flat_bhk'] = isset($this->request->data['flat_bhk']) ? $this->request->data['flat_bhk'] : false;
        $data_arr['InvInventoryProperty']['flat_status'] = isset($this->request->data['flat_status']) ? $this->request->data['flat_status'] : false;
        $data_arr['InvInventoryProperty']['flat_position'] = isset($this->request->data['flat_position']) ? json_encode($this->request->data['flat_position']) : '';
        if (isset($data_arr['InvInventoryProperty']['id']) && !empty($data_arr['InvInventoryProperty']['id'])) {
            $this->InvInventoryProperty->create();
            $this->InvInventoryProperty->save($data_arr);
            echo json_encode(array("status" => "1"));
            exit;
        }
        echo json_encode(array("status" => "0"));
        exit;
    }
    
    public function editInventory($inventory_id = null){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        # get projects
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $projects = $this->InvProject->getProjectByUser($builder_id);
        $inventory = $this->InvInventory->getInventoryById($inventory_id);
        $this->set('projects', $projects);
        $this->set('inventory', $inventory);
    }
    
    public function deleteInventory($inventory_id = null){
        $this->autoRender = false;
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        # delete inventory properties
        $this->InvInventoryProperty->deleteAll(array('InvInventoryProperty.inventory_id' => $inventory_id), false);
        
        # delete inventory
        $this->InvInventory->deleteAll(array("InvInventory.id"=>$inventory_id), false);
        $this->Session->setFlash('Inventory deleted!');
        return $this->redirect('/accounts/inventory');
        
    }
    public function shareListProjects(){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        
        $projects = $this->InvProject->getProjectByUserId($builder_id);
        $this->set('projects', $projects);
		//echo $builder_id;
		$this->set('builder_id', $builder_id);
    }
    public function shareProjectInfo($project_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);			
			$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$lastinsertId);
		}
		
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		/*
		
		echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
		if(!empty($ShareProjectsHistoryData) && $ShareProjectsHistoryData !=''){			
			//echo 'array';
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->ShareProjectsHistory->updateAll(array('id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']),array('project_name'=>'xyz'));
				$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
				
//exit();				
			//}else{
				//$this->request->data['ShareProjectsHistory'] = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
			//}
			//$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
			//exit();
		}
		}else{
			//$this->request->data['ShareProjectsHistory1']
			//echo '<pre>'; print_r($this->request->data);die();
			if ($this->request->is('post')) {
				$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			}
		}
	
		
		
		/*if ($this->request->is('post')) {			
			//$this->ShareProjectHistory->create();
			//$this->request->data['ShareProjectHistory']['builder_id'] = 'digpal singh';
			//print_r($this->request->data['ShareProjectHistory']);
			//echo '<pre>'; print_r($this->request->data);die();
			$this->ShareProjectsHistory->create();
            $this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			die();
			
		}*/
    }
	
	
	public function shareProjectInfoAppusers($project_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);			
			$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$lastinsertId);
		}
		
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		
    }
	
	
	public function shareProjectInfoUpdate($project_id){
		
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>'xyz'));
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>$project_id));
		//echo '<pre>'; print_r($this->request->data);echo '884';
		 
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		
		//echo '<pre>'; print_r($ShareProjectsHistoryData);echo '884';
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			//$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			//echo '<pre>'; print_r($ShareProjectsHistoryData);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']; die();
			//$this->ShareProjectsHistory->id = $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//$this->ShareProjectsHistory->save($this->request->data);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//echo $this->request->data['ShareProjectsHistory']['office_maxdis_check'];
			//die();
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];die();
			$this->ShareProjectsHistory->updateAll(
                array(
                    'office_maxdis_check'=>$this->request->data['ShareProjectsHistory']['office_maxdis_check'],
                    'office_brkg_fp_check'=>$this->request->data['ShareProjectsHistory']['office_brkg_fp_check'],
                    'payment_release_check'=>$this->request->data['ShareProjectsHistory']['payment_release_check'],
					'brochure_check'=>$this->request->data['ShareProjectsHistory']['brochure_check'],
					'price_list_check'=>$this->request->data['ShareProjectsHistory']['price_list_check'],
					'builder_buyer_agreement_check'=>$this->request->data['ShareProjectsHistory']['builder_buyer_agreement_check'],
					'upload_doc_check'=>$this->request->data['ShareProjectsHistory']['upload_doc_check'],
					'message'=>"'{$this->request->data['ShareProjectsHistory']['message']}'"
                ),
                array(
                    'id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']
                ));
			
			//$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		}
		
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		//echo '<pre>'; print_r($this->request->data);echo '884';
		
    }
	
	
	public function viewProjectsBroker($project_id){
		
       
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        $project_rec = array();
        $project_rec = $this->InvProject->getProjectById($project_id);
        $this->set('project_rec', $project_rec);
        $project_address = array();
        if(isset($project_rec['projects']['InvProject']['locality'])){
            $project_address[] = $project_rec['projects']['InvProject']['locality'];
        }
        if(isset($project_rec['projects']['InvProject']['city_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['city_data'];
        }
        if(isset($project_rec['projects']['InvProject']['state_data'])){
            $project_address[] = $project_rec['projects']['InvProject']['state_data'];
        }
        $project_address = implode(", ",$project_address);
        $this->set('project_address', $project_address);
		
		$this->set('project_id', $project_id);
		$this->set('builder_id', $builder_id);
		//$this->set('broker_id', $builder_id);
		$this->loadModel('ShareProjectsHistory');
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>'xyz'));
		//$this->ShareProjectsHistory->updateAll(array('id'=>46),array('project_name'=>$project_id));
		//echo '<pre>'; print_r($this->request->data);echo '884';
		 
		$ShareProjectsHistoryData = $this->ShareProjectsHistory->find('all', array(
            'conditions' => array(
                'builder_id' => $this->Session->read('Auth.Websiteuser.id'),
				'project_id' => $project_id
            )
        ));
		
		//echo '<pre>'; print_r($ShareProjectsHistoryData);echo '884';
		if ($this->request->is('post') || $this->request->is('put')) {
			//echo '<pre>'; print_r($this->request->data);die();
			//$this->ShareProjectsHistory->save($this->request->data['ShareProjectsHistory']);
			//echo '<pre>'; print_r($ShareProjectsHistoryData);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']; die();
			//$this->ShareProjectsHistory->id = $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//$this->ShareProjectsHistory->save($this->request->data);
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];
			//echo $this->request->data['ShareProjectsHistory']['office_maxdis_check'];
			//die();
			//echo $ShareProjectsHistoryData[0]['ShareProjectsHistory']['id'];die();
			$this->ShareProjectsHistory->updateAll(
                array(
                    'office_maxdis_check'=>$this->request->data['ShareProjectsHistory']['office_maxdis_check'],
                    'office_brkg_fp_check'=>$this->request->data['ShareProjectsHistory']['office_brkg_fp_check'],
                    'payment_release_check'=>$this->request->data['ShareProjectsHistory']['payment_release_check'],
					'brochure_check'=>$this->request->data['ShareProjectsHistory']['brochure_check'],
					'price_list_check'=>$this->request->data['ShareProjectsHistory']['price_list_check'],
					'builder_buyer_agreement_check'=>$this->request->data['ShareProjectsHistory']['builder_buyer_agreement_check'],
					'upload_doc_check'=>$this->request->data['ShareProjectsHistory']['upload_doc_check']
                ),
                array(
                    'id'=>$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']
                ));
			
			//$lastinsertId = $this->ShareProjectsHistory->getLastInsertId();
			$this->Session->setFlash('Now, Please Assign Broker.');
			return $this->redirect('/accounts/projects/share-project-to-brokers/'.$ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		}
		
		$this->request->data = $this->ShareProjectsHistory->findById($ShareProjectsHistoryData[0]['ShareProjectsHistory']['id']);
		//echo '<pre>'; print_r($this->request->data);echo '884';
		
    }
	
	public function shareProjectToBrokers($share_project_info_id){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
		$this->loadModel('SalesUser');
		   $BuilderBrokersList = $this->SalesUser->find('all', array(
					'joins' => array(
						array(
							'table' => 'builder_brokers',
							'type' => 'LEFT',
							'conditions' => array(
								'builder_brokers.broker_id = SalesUser.id'
							)
						)
					),
					'conditions' => array(
						'builder_brokers.builder_id' => $this->Session->read('Auth.Websiteuser.id')
					)
				));
				
		$this->set('BuilderBrokersList', $BuilderBrokersList);
        
		$builder_id = CakeSession::read('Auth.Websiteuser.id');
		$this->set('share_project_info_id', $share_project_info_id);
		$this->set('builder_id', $builder_id);
		$this->loadModel('BuilderBrokerShareProject');
		$this->loadModel('ShareProjectsHistory');
		//$this->set('broker_id', $builder_id);
		//$projectid = 701;
		
		
		$getProjectId = $this->ShareProjectsHistory->find('all', array(					
					'conditions' => array(
						'id' => $share_project_info_id
					)
				));
		//echo '<pre>'; print_r($getProjectId);
		//echo $getProjectId[0]['ShareProjectsHistory']['id'];
		
		if ($this->request->is('post')) {			
		
			
			
			$this->BuilderBrokerShareProject->query("Delete from `builder_broker_share_projects` WHERE `builder_id` = " . $builder_id . " and `project_id` = " . $getProjectId[0]['ShareProjectsHistory']['project_id'] . "");
			
			
			for ($i = 0; $i <= count($this->request->data['shareprojects1'])/2-1; $i++) {
				//$i=0;
				//foreach($this->request->data['shareprojects'] as $data){
				$this->request->data['BuilderBrokerShareProject']['share_project_history_id'] = $share_project_info_id;
				$this->request->data['BuilderBrokerShareProject']['builder_id'] = $builder_id;
				$this->request->data['BuilderBrokerShareProject']['broker_id'] = $this->request->data['shareprojects1']['broker_id'.$i.''];
				$this->request->data['BuilderBrokerShareProject']['project_id'] = $getProjectId[0]['ShareProjectsHistory']['project_id'];
				$this->request->data['BuilderBrokerShareProject']['status'] = $this->request->data['shareprojects1']['broker_check'.$i.''];
			
			$this->BuilderBrokerShareProject->create();
			$this->BuilderBrokerShareProject->save($this->request->data['BuilderBrokerShareProject']);	

			
				
			}
			//print_r(json_encode($this->request->data['shareprojects'], true));
			$brokersJson = json_encode($this->request->data['shareprojects1'], true);
			//echo $share_project_info_id;die();
			$this->ShareProjectsHistory->updateAll(
                array(
                    'brokers_assign'=>"'{$brokersJson}'"
                ),
                array(
                    'id'=>$share_project_info_id
                ));
			
			
		}
		$getArrayForBrokerChecks = $this->ShareProjectsHistory->find('all', array(					
					'conditions' => array(
						'id' => $share_project_info_id
					)
				));
		
		//print_r($getArrayForBrokerChecks);
		$this->request->data['shareprojects1'] = json_decode($getArrayForBrokerChecks[0]['ShareProjectsHistory']['brokers_assign'], true);
		//$this->request->data['shareprojects'] = $this->request->data['BuilderBrokerShareProject'];
		
		
		//$getProjectId11 = $this->ShareProjectsHistory->find('all');
    }
		
		
		//print_r($getArrayForBrokerChecks1);
		//$this->request->data['shareprojects'] = json_decode($getArrayForBrokerChecks1[0]['ShareProjectsHistory']['brokers_assign'], true);

}
