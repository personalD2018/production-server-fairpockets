<?php
class BrokersController extends InventoryManagerAppController {
    public $uses = array(
        'InventoryManager.InvBroker',
        'InventoryManager.InvWebsiteuser',
        'InventoryManager.InvBuilderBroker',
        );
    public $components = array('Session', 'InventoryManager.InventoryCustom');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authorize = array();
        $this->Auth->allow('index', 'getInventories', 'addInventory', 'addInventoryFloor');
    }

    public function index() {
        echo "Broker manager index";
        exit;
    }
    public function searchbrokers(){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            return $this->redirect('/');
        }
        $this->layout = 'account';
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $user = $this->InvWebsiteuser->getUserDetails($builder_id);
        $this->set('user', $user);
        
    }
    public function searchBrokerList(){
        if (!$this->InventoryCustom->isBuilderLogged()) {
            echo json_encode(array("status"=>"0","message"=>"User not authorized")); exit;
        }
        $builder_id = CakeSession::read('Auth.Websiteuser.id');
        $broker_name = $this->request->data['broker_name'];
        $broker_email = $this->request->data['broker_email'];
        $broker_mobile = $this->request->data['broker_mobile'];
        $broker_company = $this->request->data['broker_company'];
        
        $ret = $this->InvBuilderBroker->getAllBrokers($builder_id, $broker_name, $broker_email, $broker_mobile, $broker_company);
        
        echo json_encode($ret); exit;
        
        
        
    }

}
