<?php

App::uses('Component', 'Controller');

class InventoryCustomComponent extends Component {

    public function generate() {
        return "generated string";
    }

    public function isBuilderLogged() {
        $user_id = CakeSession::read('Auth.Websiteuser.id');
        $user_role = CakeSession::read('Auth.Websiteuser.userrole');
        if (isset($user_id) && !empty($user_id) && isset($user_role) && intval($user_role) == 3) {
            $user = ClassRegistry::init('Websiteuser');
            $row = $user->find('first', array('conditions' => array("id" => $user_id)));
            if (isset($row['Websiteuser']['id']) && $row['Websiteuser']['id'] == $user_id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
