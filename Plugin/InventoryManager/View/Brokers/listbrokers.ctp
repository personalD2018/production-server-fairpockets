<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div id="id_top" tabindex="-10" class="center wow fadeInDown no-padding-bottom animated" style="visibility: visible; animation-name: fadeInDown;">
        <div class="row">
            <div class="col-md-8">
                <h2>List Brokers</h2>
            </div>
            <div class="col-md-4">
                <div class="input-group custom-search-form" style="margin-top: 14px;">
                    <input type="text" id="searchbox" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-yellow" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" style="margin-top:20px;">
        <div class="align-right">
            <button class="btn btn-primary" style="margin-right:16px;" onclick="javascript: window.location.href='<?php echo $this->Html->url('/accounts/search-broker')?>'"> Search/Add Broker</button>
        </div>
        <div class="col-md-12">
            <table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Company</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (is_array($brokers) && count($brokers) > 0) {
                            $cntr = 1;
                            foreach($brokers as $row) {
                    ?>
                    <tr>
                        <td><?php echo $cntr;?></td>
                        <td><?php echo isset($row['InvWebsiteuser']['userorgname']) ? trim($row['InvWebsiteuser']['userorgname']) : "N/A";?></td>
                        <td><?php echo isset($row['InvWebsiteuser']['username']) ? trim($row['InvWebsiteuser']['username']) : "N/A";?></td>
                        <td><?php echo isset($row['InvWebsiteuser']['email']) ? trim($row['InvWebsiteuser']['email']) : "N/A";?></td>
                        <td><?php echo isset($row['InvWebsiteuser']['usermobile']) ? trim($row['InvWebsiteuser']['usermobile']) : "N/A";?></td>
                        <td><a href="javascript:;" onclick="deleteBroker('<?php echo $row['InvWebsiteuser']['user_id']; ?>','<?php echo $row['InvBuilderBroker']['builder_id'];?>')">Disable</a></td>
                    </tr>
                    <?php
                                $cntr++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        
        var dataTable = jQuery('#inventory_list').DataTable({
            "columnDefs": [{
                "targets": [0,5], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }],
            "order": [[ 1, "asc" ]]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        
        
        deleteBroker = function(){
            var broker_id = arguments[0];
            var builder_id = arguments[1];
            jQuery.confirm({
                title: 'Confirm!',
                content: 'Want to delete this broker?',
                buttons: {
                    confirm: function () {
                        window.location.href = '<?php echo Router::url('/accounts/brokers/delete/',true);?>' + builder_id + '/' + broker_id;
                    }
                }
            });
        }
        
        
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>
