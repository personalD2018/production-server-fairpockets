<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Listed Inventories</h1>
        </div>
        <div class="col-md-4">
            <div class="input-group custom-search-form">
                <input type="text" id="searchbox" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-yellow" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="align-right">
            <button class="btn btn-primary" style="margin-right:16px;" onclick="javascript: window.location.href='<?php echo $this->Html->url('/accounts/inventory/add')?>'"> Add New Inventory</button>
        </div>
        <div class="col-md-12">
            <table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Project</th>
                        <th>Tower</th>
                        <th>Wing</th>
                        <th>Floors</th>
                        <th>Flats/Floor</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($inventories as $key=>$row):
                    ?>
                    <tr>
                        <td><?php echo isset($row['InvProject']['project_name']) ? $row['InvProject']['project_name'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['tower_name']) ? $row['InvInventory']['tower_name'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['wing']) ? $row['InvInventory']['wing'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['floor_start']) ? $row['InvInventory']['floor_start'] : "G";?> + <?php echo isset($row['InvInventory']['floor_end']) ? intval($row['InvInventory']['floor_end']) : "0";?></td>
                        <td><?php echo isset($row['InvInventory']['flats_per_floor']) ? $row['InvInventory']['flats_per_floor'] : "N/A";?></td>
                        <td style="text-align: center;">
                            <a href="<?php echo $this->webroot.'accounts/inventory/edit/'.$row['InvInventory']['id'];?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            &nbsp;
                            <a href="<?php echo $this->webroot.'accounts/inventory/view_inventory/'.$row['InvInventory']['id'];?>" title="View" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            &nbsp;
                            <a href="javascript:void(0);" onclick="javascript:deleteInventory('<?php echo $row['InvInventory']['id']?>')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        deleteInventory = function(){
            var inventory_id = arguments[0];
            jQuery.confirm({
                title: 'Confirm!',
                content: 'Want to delete this inventory?',
                buttons: {
                    confirm: function () {
                        window.location.href = '<?php echo Router::url('/accounts/inventory/delete/',true);?>' + inventory_id
                    }
                }
            });
        }
        var dataTable = jQuery('#inventory_list').DataTable({
            "columnDefs": [{
                "targets": [5], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        //jQuery( "div#demo99" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>