<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
    .dataTables_filter {
        display: none;
    }
    .next, .prev {
        border: none!important;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Message Board</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('MessageBoardAppusersAlls', array(
			'url' => '/accounts/projects/share-message-appusers-all/',
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));		
	?>
	
	<input type="hidden" name="data[MessageBoardAppusersAlls][builder_id]" value="<?php echo $builder_id; ?>">
    <div class="row">
        <div class="col-md-12" style="border: 1px solid #bdbdbd; padding:25px;">
            
           <div class="row cell-content">
                <div class="col-md-4">Select Project</div>
                <div class="col-md-4">
				<div class="select-style">
				<?php echo $this->Form->input(
                                                'project_id', array(
                                                'id' => 'project_id',
                                                'options' => $getProjects,
                                                'class' => 'selectpicker bs-select-hidden required',
                                                'label' => false,
                                                'empty' => 'Select Project'
                                                    )
                                            ); ?>
											</div>
				</div>
            </div>
			<br>
            <div class="row cell-content">
                <div class="col-md-4">Message</div>
                <div class="col-md-4">
				<?php echo $this->Form->input('message', array(
				'type' => 'textarea', 'div' => false,
				'label' => false, 'hiddenField' => false,
				'title' => 'About Project',
				'class' => 'small required')); ?>
				</div>
            </div>
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_SB]" id="btn_share_SB" value="Send Message To All App Users" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
        </div>
    </div>
	<br> <br>
	<div class="row" style="margin-top:20px;">
        <!--<div class="align-right">
            <button class="btn btn-primary" style="margin-right:16px;" onclick="javascript: window.location.href='<?php echo $this->Html->url('/accounts/inventory/add')?>'"> Add New Inventory</button>
        </div>-->
		<?php //echo '<pre>';print_r($this->Number->getProjectNameByProjectId(701)); ?>
        <div class="col-md-12">
            <table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Message</th>
						<th>Project Name</th>
                        <th>Date/Time</th>
                        <!--<th>Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($MessageBoardAppusersAll as $key=>$row):
                    ?>
                    <tr>
                        <td><?php echo isset($row['MessageBoardAppusersAlls']['message']) ? $row['MessageBoardAppusersAlls']['message'] : "N/A";?></td>
						<td><?php echo isset($row['MessageBoardAppusersAlls']['project_id']) ? $this->Number->getProjectNameByProjectId($row['MessageBoardAppusersAlls']['project_id'])[0]['Project']['project_name'] : "N/A";?></td>
						
						<td><?php echo isset($row['MessageBoardAppusersAlls']['created']) ? $row['MessageBoardAppusersAlls']['created'] : "N/A";?></td>
						<!--<td style="text-align: center;">
                           <a href="javascript:void(0);" onclick="javascript:deleteBuilderMessage('<?php echo $row['MessageBoardAppusersAlls']['id']?>')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>-->
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
	
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        deleteBuilderMessage = function(){
            var inventory_id = arguments[0];
            jQuery.confirm({
                title: 'Confirm!',
                content: 'Want to delete this inventory?',
                buttons: {
                    confirm: function () {
                        window.location.href = '<?php echo Router::url('/accounts/inventory/delete/',true);?>' + inventory_id
                    }
                }
            });
        }
        var dataTable = jQuery('#inventory_list').DataTable({
            "columnDefs": [{
                "targets": [1], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
                "searchable": false
            }]
        });
        $("#searchbox").on("keyup search input paste cut", function() {
            dataTable.search(this.value).draw();
        });
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        //jQuery( "div#demo99" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>