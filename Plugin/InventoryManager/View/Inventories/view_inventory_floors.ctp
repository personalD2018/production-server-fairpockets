<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

			<script src="https://markcell.github.io/jquery-tabledit/assets/js/jquery.min.js"></script>
			<script src="https://markcell.github.io/jquery-tabledit/assets/js/bootstrap.min.js"></script>
			<script src="https://markcell.github.io/jquery-tabledit/assets/js/prettify.min.js"></script>
			<script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>
<style type="text/css">
    
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }
</style>
<?php 
$final_array = array_combine($bhks, $bhks);
//echo '<pre>'; print_r($final_array);
$bhkArr = array();
$bhkArr = json_encode($final_array,true);


$final_array1 = array_combine($areas, $areas);
//echo '<pre>'; print_r($final_array1);

//echo '<pre>'; print_r($final_array);


//die();

//$final_array1 = array('1215','1245','1895','1620');

//$final_array2 = array('1215','1245','1895','1620');
//$final_array3 = array_combine($final_array1, $final_array2);
//echo '<pre>'; print_r($final_array3);

$areasArr = array();
$areasArr = json_encode($final_array1,true);

//echo '<pre>'; print_r($final_array1);
//echo '<pre>'; print_r($areasArr);die();
//print_r($areasArr);
 //die(); 
 //echo '<pre>'; print_r($inventory_property_arr);
 ?>

<div class="page-content">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header">Floor Details</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="account-block">
                <div class="add-title-tab">
                    <h3>Project Info </h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <div class="row">
                        <div class="col-md-4">Project: <?php echo isset($inventory[0]['Project']['project_name']) ? $inventory[0]['Project']['project_name'] : 'N/A';?></div>
                        <div class="col-md-4">Tower: <?php echo isset($inventory[0]['InvInventory']['tower_name']) ? $inventory[0]['InvInventory']['tower_name'] : 'N/A';?></div>
                        <div class="col-md-4">Wing: <?php echo isset($inventory[0]['InvInventory']['wing']) ? $inventory[0]['InvInventory']['wing'] : 'N/A';?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Floors: <?php echo isset($inventory[0]['InvInventory']['floor_start']) ? $inventory[0]['InvInventory']['floor_start'] : '';?> + <?php echo isset($inventory[0]['InvInventory']['floor_end']) ? $inventory[0]['InvInventory']['floor_end'] : 'N/A';?></div>
                        <div class="col-md-4">Flats/Floor: <?php echo isset($inventory[0]['InvInventory']['flats_per_floor']) ? $inventory[0]['InvInventory']['flats_per_floor'] : 'N/A';?></div>
                        <div class="col-md-4">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <?php $cntr=0; foreach($flat_count_by_status as $row_status):?>
                        <div class="col-md-4"><?php echo $row_status['status']?>: <?php echo $row_status['counter'];?></div>
                        <?php $cntr++; if($cntr % 3 == 0){ echo "</div><div class=\"row\">"; } endforeach; ?>
                        <div class="col-md-4">Total Flats: <?php echo $flat_counter;?></div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="account-block">
                <div class="add-title-tab">
                    <h3>Flats </h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bs-example">
                                <div class="panel-group" id="accordion">
                                    <?php $cntr = 1; ?>
                                    <?php foreach($inventory_property_arr as $key=>$row_el): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $key;?>" aria-expanded="true" aria-controls="collapse-<?php echo $key;?>">
                                                <a role="button" style="font-weight: bold;">
                                                    <i class="more-less glyphicon glyphicon-<?php echo $cntr==1 ? "minus":"plus"; ?>"></i>
                                                    Floor - <?php echo $key == '0' ? 'G' : str_pad($key, 3, "0", STR_PAD_LEFT);?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse-<?php echo $key;?>" class="panel-collapse collapse <?php echo $cntr == 1 ? 'in' : ''; ?>">
                                            <div class="panel-body">
                                                
												<table class="table table-striped table-bordered" id="example<?php echo $key;?>">
                                <thead>
                                                        <tr>
                                                            <th style="display:none;">Id</th>
															<th>Flat No</th>
                                                            <th>Area ( Sqfts )</th>
                                                            <th>Config</th>
                                                            <th>Status</th>
															<th>Updated By</th>
                                                            <th>Position</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
                                                            if (is_array($row_el) && count($row_el) > 0) {
                                                                foreach($row_el as $row) {
                                                        ?>
                                                        <tr class="<?php echo isset($row_classes[$row['flat_status']]) ? $row_classes[$row['flat_status']] : ''?>">
                                                           <td style="padding-top:18px; display:none;"><?php echo $row['id']; ?></td>
															<!--<input type="hidden" class="tabledit-input" name="InvPropId" value="<?php //echo $row['id']; ?>">-->
															
															<td style="padding-top:18px;">
															<?php 
															if($inventory[0]['InvInventory']['id'] == 254){
																echo $inventory[0]['InvInventory']['tower_name'].'-'.str_pad($row['flat_no'], 2, "0", STR_PAD_LEFT);
															}else{
																echo $inventory[0]['InvInventory']['tower_name'].'-'.$key.''.str_pad($row['flat_no'], 2, "0", STR_PAD_LEFT);
															}
															
															
															?>
															</td>
                                                            <td style="padding-top:18px;"><?php echo isset($row['flat_area']) ? $row['flat_area'] : "N/A"; ?></td>
															<td style="padding-top:18px;"><?php echo isset($row['flat_bhk']) && strlen($row['flat_bhk']) > 3 ? $row['flat_bhk'] : 'N/A'; ?></td>
                                                            <!--<td style="padding-top:18px;"><?php //echo isset($row['flat_bhk']) && strlen($row['flat_bhk']) > 3 ? $row['flat_bhk'] : 'N/A';?></td>-->
                                                            <td style="padding-top:18px;"><?php echo isset($row['flat_status']) ? $statuses[$row['flat_status']] : "N/A";?></td>
															<td style="padding-top:18px;">
															<?php $updatedby = $this->Number->getUpdatedStatusBy($row['inventory_id'],$key,$row['flat_no']); ?>
															<?php 
															if(isset($updatedby['user_name']) && $updatedby['user_name'] !=''){
																echo $updatedby['user_name'];
															}else{
																echo 'Admin';
															}
															//echo isset($updatedby['user_name']) ? $updatedby : "Admin";?>
															</td>
                                                            <td style="padding-top:18px;"><?php 
                                                                $flat_pos_arr = !empty($row['flat_position']) ? json_decode($row['flat_position'], true) : array();
                                                                $temp_pos_arr = array();
                                                                if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
                                                                    foreach($flat_pos_arr as $pos_el) {
                                                                        $temp_pos_arr[] = $positions[$pos_el];
                                                                    }
                                                                }
                                                                if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
                                                                    echo implode(", ",$temp_pos_arr);
                                                                } else {
                                                                    echo "N/A";
                                                                }
                                                            ?></td>
                                                            
                                                        </tr>
                                                        <?php
                                                                }
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
						
						
						
						
						
						<script type="text/javascript">
			 $('[data-toggle="tooltip"]').tooltip();
			$('#example<?php echo $key;?>').Tabledit({
		url: '/accounts/inventory/updateinv',
		deleteButton: true,
		restoreButton: false,
		columns: {
			identifier: [0, 'id'],
			//editable: [[1, 'flat_no'], [2, 'flat_area', '<?php echo $bhkArr; ?>'], [3, 'flat_bhk'],[4, 'status', '{"1": "Available", "2": "Not Available", "3": "On Hold", "4": "Hold"}']]
			editable: [[2, 'flat_area', '<?php echo $areasArr; ?>'], [3, 'flat_bhk', '<?php echo $bhkArr; ?>'], [4, 'status', '{"1": "Available", "2": "Not Available", "3": "On-Hold", "4": "Internal Hold"}'],[6, 'flat_position[]', '{"": "N/A", "1": "Corner Flat", "2": "Sea Side", "3": "Park Facing", "4": "Road Facing", "5": "Villa facing"}']]
		},
		onDraw: function() {
			console.log('onDraw()');
		},
		onSuccess: function(data, textStatus, jqXHR) {
			console.log('onSuccess(data, textStatus, jqXHR)');
			console.log(data);
			console.log(textStatus);
			console.log(jqXHR);
		},
		onFail: function(jqXHR, textStatus, errorThrown) {
			console.log('onFail(jqXHR, textStatus, errorThrown)');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
		},
		onAlways: function() {
			console.log('onAlways()');
		},
		onAjax: function(action, serialize) {
			console.log('onAjax(action, serialize)');
			console.log(action);
			console.log(serialize);
		}
	}).find('select[name="flat_position[]"]').attr('multiple', 'multiple');
	</script>
												
                                            </div>
                                        </div>
                                    </div>
                                    <?php $cntr++; endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="/accounts/inventory/share_inventory_to_brokers/<?php echo $this->params['inventory_id']; ?>"><input type="submit" value="Share with Brokers" class="btn btn-primary"></a>
                        </div>
						<div class="col-md-4">
                            <a href="/accounts/inventory/share_inventory_to_sales/<?php echo $this->params['inventory_id']; ?>"><input type="submit" value="Share with Sales" class="btn btn-primary"></a>
                        </div>
                    </div>
                </div>
            </div>
<!-- Modal -->
<div class="modal fade" id="floorEditModal" tabindex="-1" role="dialog" aria-labelledby="floorEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="floorEditModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="frmEditFloorDetails" id="frmEditFloorDetails" method="post" action="javascript:void(0)">
                    <input type="hidden" name="data[id]" value="">
                <div class="row">
                    <div class="col-md-4">Flat No. :</div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="input text">
                                <input name="data[flat_no]" id="flat_no" class="form-control" placeholder="Flat No," maxlength="10" type="text" style="width: 60px;" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Flat area :</div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="data[flat_area]" id="flat_area" class="selectpicker bs-select-hidden">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Flat area :</div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="data[flat_bhk]" id="flat_bhk" class="selectpicker bs-select-hidden">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Flat status :</div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="data[flat_status]" id="flat_status" class="selectpicker bs-select-hidden">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Flat position :</div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="data[flat_position][]" id="flat_position" class="js-select2-multiple" multiple="multiple">
                                <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="javascript:saveFloorDetails();">Save changes</button>
            </div>
        </div>
    </div>
</div>


			<script src="https://markcell.github.io/jquery-tabledit/assets/js/jquery.min.js"></script>
			<script src="https://markcell.github.io/jquery-tabledit/assets/js/bootstrap.min.js"></script>
			<script src="https://markcell.github.io/jquery-tabledit/assets/js/prettify.min.js"></script>
			<script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>
			
			



<script type="text/javascript">

function populateFloorDetails1(a){
	statusid = "status"+a;
	txtstatusid = "txtstatus"+a;
	var status = document.getElementById(statusid).innerHTML;
	document.getElementById(statusid).innerHTML = "<input type='text' value='"+status+"' name='"+txtstatusid+"'>";
		
	updateid = "update"+a;
	document.getElementById(a).style.visibility="hidden";
	document.getElementById(updateid).style.visibility="visible";
}

function populateFloorDetails2(b){
	statusid = "status"+a;
	txtstatusid = "txtstatus"+a;
	var status = document.getElementById(statusid).innerHTML;
	document.getElementById(statusid).innerHTML = "<input type='text' value='"+status+"' name='"+txtstatusid+"'>";
		
	updateid = "update"+a;
	document.getElementById(a).style.visibility="hidden";
	document.getElementById(updateid).style.visibility="visible";
}

    jQuery(document).ready(function() {
		//$('#collapse-10').removeClass('panel-collapse collapse').addClass('panel-collapse in');
		
//jQuery("input[type='select'][name='status']").attr('multiple','multiple');
		//jQuery("input:select").val("Click this button to register").css({color:'yellow', background:'blue'});
        function toggleIcon(e) {
            jQuery(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }
        jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
        jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);
        jQuery('.js-select2-multiple').multiselect();
        populateFloorDetails = function(){
            var inv_prop_id = arguments[0];
            jQuery.ajax({
                url: "<?php echo $this->webroot.'accounts/inventory/edit_property_inventory';?>",
                type: "POST",
                data: {"id" : inv_prop_id},
                dataType: "json",
                success: function(resp_data){
                    jQuery("#floorEditModalLabel").html(resp_data.modal_title);
                    jQuery("div.modal-body").html(resp_data.html);
                }
            }).responseText;
        }
        
        saveFloorDetails = function(){
            jQuery.ajax({
                url: "<?php echo $this->webroot.'accounts/inventory/save_property_inventory'; ?>",
                type: "POST",
                data: jQuery('#frmEditFloorDetails').serialize(),
                dataType: "json",
                success: function(resp_data){
                    if(resp_data.status == '1'){
                        jQuery.alert({
                            title: 'Success!',
                            content: 'Inventory updated1111',
                        });
						$('#collapse-10').removeClass('panel-collapse collapse').addClass('panel-collapse in');
                        //window.location.reload();
						
                    } else {
                        jQuery.alert({
                            title: 'Error!',
                            content: 'Inventory could not be updated',
                        });
                    }
                }
            }).responseText;
        }

    });
</script>

<?php
echo $this->Html->scriptBlock("jQuery(document).ready(function() {
        jQuery( 'div#MainMenu a.list-group-item' ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( 'div#MainMenu a.inventory' ).addClass('active');
        jQuery( 'div#MainMenu a.inventory' ).removeClass('collapsed');
        jQuery( 'div#demo100' ).addClass('in');
        jQuery( 'div#MainMenu a.inventory-management' ).addClass('active');
    });");
?>
