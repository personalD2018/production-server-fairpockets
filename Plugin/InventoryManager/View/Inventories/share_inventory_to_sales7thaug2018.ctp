<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Share Inventory To Sales</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('BuilderSalesShareInventory', array(
			'url' => '/accounts/inventory/share_inventory_to_sales/'.$inventory_id,
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		
	?>
	
	<input type="hidden" name="broker_id" value="broker_id">
	<input type="hidden" name="builder_id" value="builder_id">
    <div class="row">
        <div class="col-md-12" style="border: 1px solid #bdbdbd;">
            
            
            
           
           
                        
            <div class="row cell-content">
                <div class="col-md-4"><b>Sales User Name</b></div>
                <div class="col-md-4">
				&nbsp;
				</div>
				<div class="col-md-4">
				<b>Actions</b>
				</div>
			</div>
			<?php if (!empty($BuilderSalesList)) { ?>
			<?php for ($i = 0; $i <= count($BuilderSalesList)-1; $i++) { ?>
            <div class="row cell-content">
                <div class="col-md-4"><?php echo $BuilderSalesList[$i]['SalesUser']['name']; ?></div>
                <div class="col-md-4"><input type="hidden" name="data[shareinv1][sales_id<?php echo $i; ?>]" value="<?php echo $BuilderSalesList[$i]['SalesUser']['id']; ?>"></div>
				<div class="col-md-4">
				<?php echo $this->Form->input('shareinv1.sales_check'.$i, array(
                                    'id' => 'shareinv1.sales_check'.$i,
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
				</div>
            </div>
			<?php } }?>
            
           
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>