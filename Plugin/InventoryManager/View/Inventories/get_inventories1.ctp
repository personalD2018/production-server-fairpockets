	<?php $paginator = $this->Paginator;?>
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-header">List of Sales Employees</h1>
		</div>
		<div class="col-md-4">
			<div class="user_panel_search">
				<form class="search" id="search" method="post"  action="sales_employees_lists" >
						<div class="input-group custom-search-form">
							<input type="text" name="search" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default btn-yellow" type="button" onclick="document.getElementById('search').submit()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="space-x"></div>
		
		<table id="inventory_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Project</th>
                        <th>Tower</th>
                        <th>Wing</th>
                        <th>Floors</th>
                        <th>Flats/Floor</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($inventories as $key=>$row):
                    ?>
                    <tr>
                        <td><?php echo isset($row['InvProject']['project_name']) ? $row['InvProject']['project_name'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['tower_name']) ? $row['InvInventory']['tower_name'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['wing']) ? $row['InvInventory']['wing'] : "N/A";?></td>
                        <td><?php echo isset($row['InvInventory']['floor_start']) ? $row['InvInventory']['floor_start'] : "G";?> + <?php echo isset($row['InvInventory']['floor_end']) ? intval($row['InvInventory']['floor_end']) : "0";?></td>
                        <td><?php echo isset($row['InvInventory']['flats_per_floor']) ? $row['InvInventory']['flats_per_floor'] : "N/A";?></td>
                        <td style="text-align: center;">
                            <!--<a href="<?php //echo $this->webroot.'accounts/inventory/edit/'.$row['InvInventory']['id'];?>" title="Edit" style="font-size: 16px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
                            &nbsp;
                            <a href="<?php echo $this->webroot.'accounts/inventory/view_inventory/'.$row['InvInventory']['id'];?>" title="View/Edit" style="font-size: 16px;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            &nbsp;
                            <a href="javascript:void(0);" onclick="javascript:deleteInventory('<?php echo $row['InvInventory']['id']?>')" title="Delete" style="font-size: 16px; color: red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
		<div class="clear"></div>
		
		<div class="panel-pagination col-sm-12">
			
			<ul class="inner_pagi">
				
				<?php					
					if ($paginator->hasPrev()) {
						echo $this->Paginator->prev(__('<< Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
					echo $paginator->numbers(array('modulus' => 2, 'separator' => '',
					'tag' => 'li',
					'currentClass' => 'active', 'class' => 'pagi_nu'));
					if ($paginator->hasNext()) {
						echo $this->Paginator->next(__('Next >>'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					}
				?>		
				
			</ul>
			
		</div>
		
	</div>
	
		<script type="text/javascript">
jQuery(document).ready(function() 
{	
	jQuery('.ajaxDelete').click( function() 
	{		//alert(this.id);	
		var pid = this.id;
		 if (confirm('Do you want to delete?')) 
		{				
			//if(r==true)
			//{			
				jQuery.ajax(
				{
					type: "POST",					
					url: '<?php echo 'https://www.fairpockets.com/accounts/deleteSalesEmployees/' ?>',
					cache:false,
					data:'pid=' + pid,
					success:function(msg)
					{	
						if(msg == 1)
						{
							alert("Deleted Successfully.");
							jQuery('#row' + pid).slideUp(800,'linear');
						} else
						{
							alert(msg)
							alert("try again");
						}
					}
				});			
			//}
		};			
	});
});
</script>
