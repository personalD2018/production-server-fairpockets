<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>-->



<?php $jsons = '{"flat_no":{"1":"001","2":"002"},"flat_area":{"1":"1215","2":"1620"},"flat_bhk":{"1":"2BHK","2":"2BHK"},"flat_status":{"1":"1","2":"1"},"flat_position":{"1":["1","2"],"2":["2"]}}';
//$json1 = '{"flat_no":{"1":"001","2":"002","3":"003","4":"004","5":"005"},"flat_area":{"1":"1215","2":"1245","3":"1620","4":"1245","5":"1215"},"flat_bhk":{"1":"2BHK","2":"2BHK","3":"3BHK","4":"2BHK","5":"2BHK"},"flat_status":{"1":"1","2":"1","3":"4","4":"1","5":"2"},"flat_position":{"1":["1"],"2":["2"],"5":["1"]}}';
//$decodes = json_decode($jsons,true);
//echo '<pre>'; print_r($decodes);

$flats_per_floor = $inventory1[0]['InvInventory']['flats_per_floor'];
$invjson = $inventory1[0]['InvInventory']['template'];
$decodes = json_decode($invjson,true);
//echo $flats_per_floor;
//echo '<pre>'; print_r($decodes);
 ?>

<div class="page-content">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header">Floor Details</h1>
        </div>
		<div class="col-md-8" style="text-align:right;">
            <?php 
		echo $this->html->link('Print', "javascript:void(0)", array('style'=>'float:left;height:auto;padding: 5px;', "class"=>"no-print", "escape"=>false, "id"=>"print-preview1"));
		?>
	
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="account-block" id="leftPrintBody">
			
			<style type="text/css">    
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
		
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: lightblue;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }
	.table-wrapper-scroll-x {
  display: block;
  max-height: 100px;
  overflow-y: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}
.table-bordered > tbody > tr > td{
	border:none;	
	padding: 0px 0px 0px 0px !important;
}
.tabledsn{border:0.5px dashed; padding:8px;

    
    transition: transform .2s; /* Animation */
    
    margin: 0 auto;
}
.tabledsn1{border:0.5px solid; padding:8px; font-weight:bold;

}
.tabledsn:hover {
    transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
.table-bordered{
	border:1px solid #000000;
}
@leftPrintBody {
        size: A4;
        margin: 0;
    }
    @media print {
		
		#leftPrintBody {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
		
	}
</style>
			
			
			
                <div class="add-title-tab">
                    <h3>Flats </h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bs-example">
                                <div class="panel-group" id="accordion">
                                    <?php $cntr = 1; ?>
									<?php 
									//echo '<pre>'; print_r($inventory);die();
									//echo '<pre>'; print_r($inventory_property_arr);die(); ?>
                                    <?php //foreach($inventory_property_arr as $key=>$row_el): ?>
                                    <div class="panel panel-default">                                        
                                        <div>
										<!--<div id="collapse-<?php echo $key;?>" class="panel-collapse collapse <?php echo $cntr == 1 ? 'in' : ''; ?>">-->
                                            <div class="panel-body" style="width:100%; overflow-x:auto;">	
                                                <table class="table table-bordered">                     
                                                    <tbody>
													<tr>
													<td style="padding:20px;">
														<div class="insidetd" style="width:100px;">
														<div class="tabledsn1">Flat No</div>
														<div class="tabledsn1">Area</div>
														<div class="tabledsn1">Config</div>
														<div class="tabledsn1">Position</div>
														
														<?php foreach($inventory_property_arr as $key=>$row_el): ?>
															<div class="tabledsn">Floors - <?php echo $key; ?></div>
														<?php endforeach; ?>
														</div>
													</td>
                                                        <?php 
														//echo '<pre>'; print_r($inventory_property_arr);
                                                            $row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
                                                           // if (is_array($row_el) && count($row_el) > 0) {
                                                                //foreach($row_el as $row) {
																for($i=1; $i<=$flats_per_floor; $i++){
                                                        ?>
                                                        
                                                            <td style="padding-top:20px;">
															<div class="insidetd" style="width:140px;">
															<div class="tabledsn1">
																<?php echo str_pad($decodes['flat_no'][$i], 3, "0", STR_PAD_LEFT)?>
															</div>
															<div class="tabledsn1">
															<?php echo isset($decodes['flat_area'][$i]) ? $decodes['flat_area'][$i] . ' Sqft.' : "N/A"; ?>
															</div>
															<div class="tabledsn1">
															<?php echo isset($decodes['flat_bhk'][$i]) && strlen($decodes['flat_bhk'][$i]) > 3 ? $decodes['flat_bhk'][$i] : 'N/A';?>
															</div>
															<div class="tabledsn1">
															<?php 
                                                                $flat_pos_arr = $decodes['flat_position'][$i];
                                                                $temp_pos_arr = array();
                                                                if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
                                                                    foreach($flat_pos_arr as $pos_el) {
                                                                        $temp_pos_arr[] = $positions[$pos_el];
                                                                    }
                                                                }
                                                                if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
                                                                    echo implode(", ",$temp_pos_arr);
                                                                } else {
                                                                    echo "N/A";
                                                                }
                                                            ?>
															</div>
															<?php foreach($inventory_property_arr as $key=>$row_el){
															//$row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
                                                            //if (is_array($row_el) && count($row_el) > 0) {
                                                               // foreach($row_el as $row) {
														
														?>
														<?php //echo $decodes['flat_no'][$i];
														$invid = $inventory_id;
														$floor_no = $key;
														$flat_no = $decodes['flat_no'][$i];
														$flat_status_id = $this->Number->getStatusInv($invid,$floor_no,$flat_no);
														
														if($flat_status_id == 1) { $colorcode = '#dff0d8';}
														if($flat_status_id == 2) { $colorcode = '#f2dede';}
														if($flat_status_id == 3) { $colorcode = '#fcf8e3';}
														if($flat_status_id == 4) { $colorcode = '#d9edf7';}
														
														$flat_status =  $statuses[$flat_status_id];
														$row_classes11 = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
														?>
														<div class="tabledsn" style="background-color:<?php echo $colorcode; ?>">
														<?php echo isset($flat_status) ? $flat_status : "N/A";?></div>
															
														<?php //}
														//}
														} ?>
															
															<!--<a href="javascript: void(0);" class="btn btn-info" style="width: 40px;" data-toggle="modal" data-target="#floorEditModal1" onclick="javascript:populateFloorDetails('<?php echo $row['id']; ?>');">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                </a>
                                                                <a href="javascript:;" class="btn btn-danger" style="width: 40px;">
                                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                </a>-->
																</div>
															</td>
                                                            
															
                                                        
                                                        <?php
                                                                }
                                                            //}
                                                        ?>
														
														</tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $cntr++; //endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
</div>
<script>
jQuery(document).ready(function(){
jQuery("#print-preview1").click(/*"click", */function () {
            var divContents = jQuery("#leftPrintBody").html();
            var printWindow = window.open('', '', 'height=500,width=2120');
            printWindow.document.write('<html><head><title>Inventory Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
	});
</script>

<?php
echo $this->Html->scriptBlock("jQuery(document).ready(function() {
        jQuery( 'div#MainMenu a.list-group-item' ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( 'div#MainMenu a.inventory' ).addClass('active');
        jQuery( 'div#MainMenu a.inventory' ).removeClass('collapsed');
        jQuery( 'div#demo100' ).addClass('in');
        jQuery( 'div#MainMenu a.inventory-management' ).addClass('active');
    });");
?>
