<style type="text/css">
    .cell-content{
        margin: 14px 0;
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header">Share Projects To Brokers</h1>
        </div>
    </div>
	 <?php
		echo $this->Form->create('BuilderBrokerShareProject', array(
			'url' => '/accounts/projects/share-project-to-brokers/'.$share_project_info_id,
			'id' => 'frm_inventory_first', 
			'name'=>'frm_inventory_first',
			'method'=>'POST'
		));
		
	?>
	
	<input type="hidden" name="broker_id" value="broker_id">
	<input type="hidden" name="builder_id" value="builder_id">
    <div class="row">
        <div class="col-md-12">
            
            <table id="sales_list" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
						<th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($BuilderBrokersList)) {
                        for ($i = 0; $i < count($BuilderBrokersList); $i++) {
                    ?>
                    <tr>
                        <td><?php echo $BuilderBrokersList[$i]['SalesUser']['name']; ?></td>
                        <td><?php echo $BuilderBrokersList[$i]['SalesUser']['email']; ?></td>
                        <td><?php echo $BuilderBrokersList[$i]['SalesUser']['mobile']; ?></td>
						<td style="text-align: center;">
							<input type="hidden" name="data[shareprojects1][broker_id<?php echo $i; ?>]" value="<?php echo $BuilderBrokersList[$i]['SalesUser']['id']; ?>">
							
							<?php echo $this->Form->input('shareprojects1.broker_check'.$i, array(
                                    'id' => 'shareprojects1.broker_check'.$i,
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );
				?>
							</td>
                    </tr>
					<?php }}else{ ?>
					<tr>
                        <td colspan="4" style="text-align:center;">
							No User available, please add sales employees. <a href="/accounts/add_sales_employees1">Click Here</a>
						</td>
					</tr>
					<?php } ?>
                </tbody>
            </table>
            
           
           
                        
            <!--<div class="row cell-content">
                <div class="col-md-4"><b>Broker Name</b></div>
                <div class="col-md-4">
				&nbsp;
				</div>
				<div class="col-md-4">
				<b>Actions</b>
				</div>
			</div>
			<?php //if (!empty($BuilderBrokersList)) { ?>
			<?php //for ($i = 0; $i <= count($BuilderBrokersList)-1; $i++) { ?>
            <div class="row cell-content">
                <div class="col-md-4"><?php //echo $BuilderBrokersList[$i]['SalesUser']['name']; ?></div>
                <div class="col-md-4"><input type="hidden" name="data[shareprojects1][broker_id<?php ///echo $i; ?>]" value="<?php ///echo $BuilderBrokersList[$i]['SalesUser']['id']; ?>"></div>
				<div class="col-md-4">
				<?php /*echo $this->Form->input('shareprojects1.broker_check'.$i, array(
                                    'id' => 'shareprojects1.broker_check'.$i,
                                    'type' => 'checkbox',
                                    'div' => array('class' => 'col-sm-12'),
                                    'class' => 'checkbox-custom',
                                    'label' => array('class' => 'checkbox-custom-label', 'text' => ''),
                                        )
                                );*/
				?>
				</div>
            </div>-->
			<?php //} }?>
            
           
            <div class="row cell-content">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="submit" name="data[btn_share_brokers]" id="btn_share_brokers" value="Share" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "div#MainMenu a.list-group-item" ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( "div#MainMenu a.inventory" ).addClass('active');
        jQuery( "div#MainMenu a.inventory" ).removeClass('collapsed');
        jQuery( "div#demo100" ).addClass('in');
        jQuery( "div#MainMenu a.inventory-management" ).addClass('active');
    });
</script>