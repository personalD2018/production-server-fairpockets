<?php
	$jsons = '{"flat_no":{"1":"001","2":"002"},"flat_area":{"1":"1215","2":"1620"},"flat_bhk":{"1":"2BHK","2":"2BHK"},"flat_status":{"1":"1","2":"1"},"flat_position":{"1":["1","2"],"2":["2"]}}';
	$flats_per_floor = $inventory[0]['InvInventory']['flats_per_floor'];
	$invjson = $inventory[0]['InvInventory']['template'];
	$decodes = json_decode($invjson,true);
?>
<div class="page-content">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-header">Floor Details</h1>
			
        </div>
		<div class="col-md-8" style="text-align:right;">
            <?php 
				echo $this->html->link('Print Report', "javascript:void(0)", array("class"=>"btn btn-primary no-print", "escape"=>false, "id"=>"print-preview1"));
			?>	
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="account-block" id="leftPrintBody">
			
			
			
			<style type="text/css">    
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
		
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: lightblue;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }
	.table-wrapper-scroll-x {
  display: block;
  max-height: 100px;
  overflow-y: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}
.table-bordered > tbody > tr > td{
	border:none;	
	padding: 0px 0px 0px 0px !important;
}
.tabledsn{border:0.5px dashed; padding:8px;

    
    transition: transform .2s; /* Animation */
    
    margin: 0 auto;
}
.tabledsn1{border:0.5px solid; padding:8px; font-weight:bold;

}
.tabledsn:hover {
    transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
.table-bordered{
	border:1px solid #000000;
}
@leftPrintBody {
        size: A4;
        margin: 0;
    }
    @media print {		
		#leftPrintBody {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
		.my-logo-print{display:none !important;}
		.add-title-tab{display:none !important;}
	}
</style>
			
			<img src="https://www.fairpockets.com/img/front/logo.png" alt="Fairpockets" class="my-logo-print" style="display:none;">
			
                <div class="add-title-tab">
                    <h3>Inventory Details </h3>
                </div>
                <div class="add-tab-content detail-block" id="id_proj_basic_content" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bs-example">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">                                        
                                        <div>
											<div class="panel-body" style="width:100%; overflow-x:auto;">	
                                                <?php //echo '<pre>'; print_r($inventory);//foreach($inventory as $inventoryok){ count
												 //count
													for($j=0;$j<=count($inventory)-1;$j++){
												?>
												<?php $jsonview = $inventory[$j]['InvInventory']['template'];
													$arrr[] =  json_decode($jsonview,true);
													$areaSelectedArr = $inventAreaArr;
													$statusSelectedArr = $statArr;
												?>
												<?php 
												//echo $inventory[$j]['InvInventory']['id'];
												$arrnew = $this->Number->invFloorByInvId($inventory[$j]['InvInventory']['id'],$areaSelectedArr,$statusSelectedArr);
													//echo '<pre>'; print_r($arrnew);die();
												?>
												
												<div class="detail-title" style="text-align:center;">
													<?php if($inventory[$j]['InvInventory']['wing'] != ''){ ?>
														<a>
															<b><?php echo $inventory[$j]['InvInventory']['wing']; ?></b>								
														</a>
													<?php }else{ ?>
														<a>
															<b><?php echo $inventory[$j]['InvInventory']['tower_name']; ?></b>								
														</a>
													<?php } ?>
												</div>
												<br>
												<br>
												
												<?php
												$invid = $inventory[$j]['InvInventory']['id'];
												//echo $invid;
												//$area = 1245;
												
												
												$InvtsByInvIdsArr = $this->Number->getInvByArea($invid,$areaSelectedArr,$statusSelectedArr);
												//echo '<pre>'; print_r($InvtsByInvIdsArr);die();		
												?>
												
												<div class="abovetable" style="width:100%; overflow-x:auto;">
													<table class="table table-bordered">                     
														<tbody>
															<tr>
																<td style="padding-top:20px;">
																	<div class="insidetd" ><!--style="width:140px;"-->
																		<div class="tabledsn1">Flat No</div>
																		<div class="tabledsn1">Area</div>
																		<div class="tabledsn1">Config</div>
																		<div class="tabledsn1">Position</div>														
																		<?php foreach($arrnew as $key=>$row_el): ?>
																			<div class="tabledsn">Floors - <?php echo $key; ?></div>
																		<?php endforeach; ?>
																	</div>
																</td>
																<?php
																
																//$getInvByAreaArr = $this->Number->getInvByArea($invid,$area);
																//echo '<pre>'; print_r($getInvByAreaArr);die();
																
																	$row_classes = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
																	if(isset($InvtsByInvIdsArr) && $InvtsByInvIdsArr != ''){
																		for($i=0; $i<=count($InvtsByInvIdsArr)-1; $i++){
																?>                                                        
																<td style="padding-top:20px;">
																	<div class="insidetd"> <!--style="width:140px;"-->
																		<div class="tabledsn1">
																			<?php //echo $arrr[$j]['flat_no'][$i]; 																
																			 echo str_pad($InvtsByInvIdsArr[$i]['flat_no'], 3, "0", STR_PAD_LEFT) ?>																
																		</div>
																	
																		<div class="tabledsn1">
																		<?php echo isset($InvtsByInvIdsArr[$i]['flat_area']) ? $InvtsByInvIdsArr[$i]['flat_area'] . ' Sqft.' : "N/A"; ?>
																		</div>
																		<div class="tabledsn1">
																		<?php echo isset($InvtsByInvIdsArr[$i]['flat_bhk']) && strlen($InvtsByInvIdsArr[$i]['flat_bhk']) > 3 ? $InvtsByInvIdsArr[$i]['flat_bhk'] : 'N/A';?>
																		</div>
																		<div class="tabledsn1">
																		<?php 
																			$flat_pos_arr = $InvtsByInvIdsArr[$i]['flat_position'];
																			$temp_pos_arr = array();
																			if (is_array($flat_pos_arr) && count($flat_pos_arr) > 0) {
																				foreach($flat_pos_arr as $pos_el) {
																					$temp_pos_arr[] = $positions[$pos_el];
																				}
																			}
																			if(is_array($temp_pos_arr) && count($temp_pos_arr) > 0){
																				echo implode(", ",$temp_pos_arr);
																			} else {
																				echo "N/A";
																			}
																		?>
																		</div>
																	<?php //echo '<pre>'; print_r($arrnew); ?>
																	
																		<?php foreach($arrnew as $key=>$row_el){
																			//echo '<pre>'; print_r($arrnew);
																		?>
																		<?php 
																		$invid = $inventory[$j]['InvInventory']['id'];
																		$floor_no = $key;
																		$flat_no = $InvtsByInvIdsArr[$i]['flat_no'];
																		
																		//echo $invid;
																		//echo '<pre>'; print_r($statArr);
																		$statArrOk = $statArr;
																		$flat_status_id = $this->Number->getStatusInv($invid,$floor_no,$flat_no,$statArrOk);
																		
																		if($flat_status_id == 1) { $colorcode = '#dff0d8';}
																		if($flat_status_id == 2) { $colorcode = '#f2dede';}
																		if($flat_status_id == 3) { $colorcode = '#fcf8e3';}
																		if($flat_status_id == 4) { $colorcode = '#d9edf7';}
																		
																		if($flat_status_id == '') { $colorcode = 'none';}
																		
																		$flat_status =  $statuses[$flat_status_id];
																		$row_classes11 = array("1"=>"success", "2"=>"danger", "3"=>"warning", "4"=>"info");
																		?>
																		<div class="tabledsn" style="background-color:<?php echo $colorcode; ?>; height:37px;">
																		<?php echo $flat_status; ?>
																		<?php //echo isset($flat_status) ? $flat_status : "N/A";?></div>
																			
																		<?php 
																		} ?>
																	
																	
																	</div>
																</td>
																<?php }
																	} else {?>
																	<td>No Data</td>
																<?php } ?>
															</tr>
														</tbody>
													</table>												
												</div>
												<br><br>
												<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
</div>
<script>
jQuery(document).ready(function(){
jQuery("#print-preview1").click(/*"click", */function () {
            var divContents = jQuery("#leftPrintBody").html();
            var printWindow = window.open('', '', 'height=500,width=2120');
            printWindow.document.write('<html><head><title>www.fairpockets.com</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
	});
</script>

<?php
echo $this->Html->scriptBlock("jQuery(document).ready(function() {
        jQuery( 'div#MainMenu a.list-group-item' ).each(function ( index, domEle) {  
            var attr = jQuery(this).attr('id');
            if ( jQuery(this).attr('id') === 'disable') {          
                jQuery(this).removeAttr('id');        
            }      
        });
        jQuery( 'div#MainMenu a.inventory' ).addClass('active');
        jQuery( 'div#MainMenu a.inventory' ).removeClass('collapsed');
        jQuery( 'div#demo100' ).addClass('in');
        jQuery( 'div#MainMenu a.inventory-management' ).addClass('active');
    });");
?>
