<?php
class InvProperty extends InventoryManagerAppModel {
    public $primaryKey 	= 'property_id';
    public $name = 'Property';
    
    public $hasMany = array(
        'PropertyFeature' => array(
            'className' => 'PropertyFeature',
            'conditions' => array('PropertyFeature.features_id' => array('1','34')),
            /*'order' => 'Recipe.created DESC',*/
            'foreignKey'=>'properties_id'
        )
    );
    
    
    public function getPropertyByUser($user_id){
        $area_arr = array();
        $bhk_arr = array();
        $data_arr = $this->find('all', array(
            'fields' => array(''),
            'conditions' => array('InvProperty.user_id = ' . $user_id)
        ));
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                $area_arr[] = $row['PropertyFeature'][0]['features_Value'];
                $bhk_arr[] = $row['PropertyFeature'][1]['features_Value'];
            }
        }
        return array("area"=>$area_arr, "bhk"=>$bhk_arr);
    }
	
	public function getPropertyAreaByUser($user_id,$project_id){
		//echo $user_id.'----'.$project_id;die();
        $area_arr = array();
        //$bhk_arr = array();
        $data_arr = $this->find('all', array(
            'fields' => array(''),
            'conditions' => array('InvProperty.user_id' => $user_id,
			'InvProperty.project_id' => $project_id)
        ));
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                $area_arr[] = $row['PropertyFeature'][0]['features_Value'];
                //$bhk_arr[] = $row['PropertyFeature'][1]['features_Value'];
            }
        }
		//echo '<pre>'; print_r($area_arr);die();
        return array("area"=>$area_arr);
    }
	
	public function getPropertyUnitByUser($user_id,$project_id){
        //$area_arr = array();
        $bhk_arr = array();
        $data_arr = $this->find('all', array(
            'fields' => array(''),
            'conditions' => array('InvProperty.user_id' => $user_id,
			'InvProperty.project_id' => $project_id)
        ));
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach($data_arr as $row) {
                //$area_arr[] = $row['PropertyFeature'][0]['features_Value'];
                $bhk_arr[] = $row['PropertyFeature'][1]['features_Value'];
            }
        }
        return array("bhk"=>$bhk_arr);
    }
	
	
    /*
    $this->Model->find('all', array(
            'fields' => array(),
            'conditions' => array(),
            'joins' => array(
                array(
                    'table' => 'table',
                    'alias' => 'Table',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(' = ')
                )
            )
        ));
    */
}

