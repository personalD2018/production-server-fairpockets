<?php
class InvProject extends InventoryManagerAppModel {
    public $name = 'Project';
    public function getProjectByUser($user_id){
        $data_arr = $this->find('all', array(
            'fields' => array('InvProject.project_id', 'InvProject.project_name'),
            'conditions' => array('InvProject.user_id = ' . $user_id)
        ));
        return $data_arr;
    }
    public function getProjectByUserId($user_id){
        $data_arr = $this->find('all', array(
            'conditions' => array('InvProject.user_id = ' . $user_id)
        ));
        return $data_arr;
    }
    public function getProjectById($project_id){
        $project_office_use = ClassRegistry::init('ProjectOfficeUse');
        
        $ret_arr = array();
        $ret_arr['projects'] = $this->find('first',array(
            'conditions'=>array('InvProject.project_id'=>$project_id)
        ));
        $ret_arr['project_office'] = $project_office_use->find('first', array(
            'conditions'=>array("project_id = '{$project_id}'")
        )); 
        return $ret_arr;
        
    }
    
}

